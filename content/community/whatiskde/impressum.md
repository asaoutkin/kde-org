---
title: Impressum kde.org
hidden: true
---


<p><i>(This page is in German language as it is required by German law. An English translation
can be found <a href="../impressum-en">here</a>)</i>.</p>

<dl>

<dt>Vereinsname:</dt>
  <dd>K Desktop Environment e. V. (KDE e.V.)<br />&nbsp;</dd>

<dt>Sitz:</dt>
  <dd>Berlin, Deutschland <br />&nbsp;</dd>

<dt>Anschrift:</dt>
  <dd>KDE e.V.<br />
  Prinzenstraße 85 F<br />
  10969 Berlin<br />
  Germany<br />&nbsp;</dd>

<dt>Umsatzsteuer-IdNr:</dt>
  <dd>DE278127691<br />&nbsp;</dd>

<dt>Telefon:</dt>
  <dd>+49 30-2023 7305-0<br />&nbsp;</dd>

<dt>Fax:</dt>
  <dd>+49 30-2023 7305-9<br />&nbsp;</dd>

<dt>E-Mail:</dt>
  <dd>Webmaster: <a href="ma&#105;l&#x74;o&#x3a;&#x77;&#101;b&#x6d;&#x61;s&#x74;&#x65;&#114;&#64;&#107;de&#00046;org">&#119;&#0101;bmast&#101;r&#0064;&#107;&#x64;&#101;.o&#114;&#103;</a></dd>
  <dd>Vorstand: <a href="m&#097;&#x69;&#108;&#116;&#111;&#0058;&#x6b;&#100;&#x65;&#0045;&#101;&#x76;-&#118;or&#115;t&#x61;n&#x64;&#64;&#00107;de&#046;&#x6f;&#114;&#103;">&#x6b;de-&#x65;v-&#00118;ors&#116;a&#x6e;d&#x40;k&#100;e.o&#114;&#103;</a> <br />&nbsp;</dd>

<dt>Internet:</dt>
  <dd><a href="http://ev.kde.org/">http://ev.kde.org/</a> <br />&nbsp;</dd>

<dt>Vertretungsberechtigter Vorstand:</dt>
 <dd>Aleix Pol &lt;<a href="mailto:aleixpol@kde.org">aleixpol@kde.org</a>&gt;,<br />
     Lydia Pintscher,<br />
     Eike Hein,<br />
     Neofytos Kolokotronis,<br/>
     Adriaan de Groot,<br />
      (siehe auch <a href="https://ev.kde.org/boards">Der Vorstand</a>) <br />&nbsp;</dd>

<dt>Registergericht:</dt>
  <dd>Amtsgericht Berlin (Charlottenburg) <br />&nbsp;</dd>

<dt>Registernummer:</dt>
  <dd>VR31685</dd>

</dl>

<p>Unsere Website verwendet Piwik, dabei handelt es sich um einen sogenannten Webanalysedienst. Piwik verwendet sog. „Cookies“, das sind Textdateien, die auf Ihrem Computer gespeichert werden und die unsererseits eine Analyse der Benutzung der Webseite ermöglichen. Zu diesem Zweck werden die durch den Cookie erzeugten Nutzungsinformationen (einschließlich Ihrer gekürzten IP-Adresse) an unseren Server übertragen und zu Nutzungsanalysezwecken gespeichert, was der Webseitenoptimierung unsererseits dient. Ihre IP-Adresse wird bei diesem Vorgang umge­hend anony­mi­siert, so dass Sie als Nutzer für uns anonym bleiben. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Webseite werden nicht an Dritte weitergegeben. Sie können die Verwendung der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern, es kann jedoch sein, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können.</p>

<p>Wenn Sie mit der Spei­che­rung und Aus­wer­tung die­ser Daten aus Ihrem Besuch nicht ein­ver­stan­den sind, dann kön­nen Sie der Spei­che­rung und Nut­zung nachfolgend per Maus­klick jederzeit wider­spre­chen. In diesem Fall wird in Ihrem Browser ein sog. Opt-Out-Cookie abgelegt, was zur Folge hat, dass Piwik kei­ner­lei Sit­zungs­da­ten erhebt. Achtung: Wenn Sie Ihre Cookies löschen, so hat dies zur Folge, dass auch das Opt-Out-Cookie gelöscht wird und ggf. von Ihnen erneut aktiviert werden muss.</p>

<iframe width="100%" src="https://stats.kde.org/index.php?module=CoreAdminHome&amp;action=optOut&amp;language=de"></iframe>

<p>
<b>Haftungshinweis:</b> Trotz sorgf&auml;ltiger inhaltlicher
Kontrolle &uuml;bernehmen wir keine Haftung
f&uuml;r die Inhalte externer Links. F&uuml;r den Inhalt der verlinkten Seiten
sind ausschlie&szlig;lich deren Betreiber verantwortlich.
</p>

