---
title: kde.org Impressum
hidden: true
---


<p><i>(The original German "Impressum" can be found <a href="../impressum">here</a>)</i>.</p>

<dl>

<dt>Name:</dt>
  <dd>K Desktop Environment incorporated society (KDE e.V.) <br />&nbsp;</dd>

<dt>Location:</dt>
  <dd>Berlin, Germany <br />&nbsp;</dd>

<dt>Address:</dt>
  <dd>KDE e.V.<br />
  Prinzenstraße 85 F<br />
  10969 Berlin<br />
  Germany<br />&nbsp;</dd>

<dt>VAT Number:</dt>
  <dd>DE278127691<br />&nbsp;</dd>

<dt>Phone:</dt>
  <dd>+49 30-2023 7305-0<br />&nbsp;</dd>

<dt>Fax:</dt>
  <dd>+49 30-2023 7305-9<br />&nbsp;</dd>

<dt>E-Mail:</dt>
  <dd>Webmaster: <a href="ma&#x69;lto&#058;w&#00101;b&#109;&#00097;ste&#x72;&#x40;&#x6b;&#0100;&#101;.&#111;rg">&#x77;ebm&#x61;&#115;te&#x72;&#64;&#107;de.&#111;&#114;g</a></dd>
  <dd>Board: <a href="&#109;&#x61;il&#00116;&#x6f;&#058;kde&#0045;&#x65;v-&#0118;&#x6f;rst&#97;&#x6e;&#x64;&#00064;&#107;d&#101;&#x2e;o&#114;&#00103;">k&#100;&#0101;-ev&#x2d;v&#00111;r&#115;&#116;a&#x6e;&#100;&#64;&#107;de&#x2e;&#x6f;&#114;g</a> <br />&nbsp;</dd>

<dt>Internet:</dt>
  <dd><a href="http://www.kde.org/areas/kde-ev/">www.kde.org/areas/kde-ev</a> <br />&nbsp;</dd>

<dt>Board:</dt>
 <dd>Aleix Pol &lt;<a href="mailto:aleixpol@kde.org">aleixpol@kde.org</a>&gt;,<br />
     Lydia Pintscher,<br />
     Eike Hein,<br />
     Neofytos Kolokotronis,<br/>
     Adriaan de Groot,<br />
     
      (more at: <a href="http://www.kde.org/areas/kde-ev/corporate/board.php">Board</a>) <br />&nbsp;</dd>

<dt>Court of jurisdiction:</dt>
  <dd>Amtsgericht Berlin (Charlottenburg)<br />&nbsp;</dd>

<dt>Reg.-No.:</dt>
  <dd>VR31685</dd>

</dl>

<p>Our website uses piwik, a so called web analyzer service. Piwik uses "cookies", those are text files stored on your computer and enabling us to analyse the usage of the website. For this purpose usage statistics generated from the cookies (including your shortened ip address) are sent to our server and saved for usage analytics, to optimize our websites. During this process your ip address immediately gets anonymized so you as user are completely anonymous to us. The generated information by the cookies are not passed on to a third party. You can prevent the usage of cookies by a corresponding setting in your browser software, but in this case you might not be able to use this website with full functionality.</p>

<p>If you do not agree with the storage and evaluation of your data from your visit, then you can opt-out of storage and usage via a mouse click. In this case your browser will store a so called Opt-Out-Cookie, which tells Piwik to not analyse any session data. Attention: If you delete all your cookies, your Opt-Out-Cookie will be deleted as well, in which case you probably need to activate it again.</p>

<iframe width="100%" src="https://stats.kde.org/index.php?module=CoreAdminHome&amp;action=optOut&amp;language=en"></iframe>

<p>
<b>Disclaimer:</b> The KDE project accepts no liability for the content of external links though we do
aim to review them. Reponsibility for external content remains with the content provider.
</p>

