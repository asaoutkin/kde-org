---
title: Donate and support KDE
description: Help us create software that gives you full freedom, protects your privacy and be a part of a great community
aliases:
- /donate
- /donations
layout: donations
sassFiles:
  - sass/donations.scss
learn: Learn about our activities
activity: Your generous donation funds contributor meetings and infrastructure. It keeps our software safe for the future and raises awareness for Free Software and Free Culture. Support our annual conferences Akademy, Akademy-es, Lakademy and KDE.in.
reports:
  - link: https://ev.kde.org/reports/ev-2017/
    name: 2017 report
    image: https://ev.kde.org/reports/ev-2017/images/slider/1.jpg
  - link: https://ev.kde.org/reports/ev-2018/
    name: 2018 report
    image: https://ev.kde.org/reports/ev-2018/images/slider/1.jpg
  - link: https://ev.kde.org/reports/ev-2019/
    name: 2019 report
    image: https://ev.kde.org/reports/ev-2019/images/slider/1.jpg
support: Support An Engaged Community
support_text: Our community has developed a wide variety of applications for communication, work, education and entertainment. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant, open atmosphere for experimentation.
regulary: Donate regularily
other: Other ways to donate
previous: Previous donations page
kdeev: About KDE e.V.
kde: KDE e.V. is the non-profit organization behind the KDE community. It is based in Germany and thus donations are tax deductible in Germany. If you live in another EU country your donation might also be tax deductible. Please consult your tax advisor for details.
supporting:
  title: Supporting Member Programme for individuals
  text:  Show your love for KDE by donating 100€/year! Get Invited to attend the annual general assembly of KDE e.V. and get regular first hand reports about KDE's activities
supporting_corp:
  title: Supporting Member Programme for corporations
  text:  Are you part of a business or a corporation interested in sponsoring KDE development? In addition to making great software possible, supporting members also have their logos added to the KDE website and many printed promotional materials. 
---
