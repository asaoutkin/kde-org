---
title: Buy hardware with Plasma and KDE Applications
subtitle: "Here you can find a list of devices with KDE Plasma pre-installed that you can buy right now:"
layout: free
sassFiles:
  - /sass/hardware.scss
pinephone:
  title: PinePhone KDE Community Edition
  subtitle: Experience the future of KDE’s open mobile platform
  description: |
    Plasma Mobile and PinePhone provide you with the excitement of experimenting with
    the future of Free and Open Source mobile phones right now.

    The Pinephone is a tinkerer's dream, letting you boot new operating systems with
    ease and tweak, expand, and update the hardware. Plasma Mobile provides you with
    a feature-rich environment with an already well-populated app ecosystem ready for
    convergence. Experiment with software that improves on a daily basis, use calendars,
    sync up with your desktop and other devices using KDE Connect, browse the web,
    read documents, play games, send Instant Messages, and so much more.
  specification: |
    As a regular user, get this phone if you want to experience first hand what the
    future of FLOSS mobile phones will be like. Test it, give us feedback and help
    us push Plasma Mobile to maturity!

    As a developer, get this phone if you want to contribute to Plasma Mobile, one
    of the most active, community-driven projects for mobile phones. It will also
    give you a headstart in creating apps for a budding system that is on the verge
    of taking the mobile arena by storm.
  learn: Learn more about the PinePhone
  alt: Picture of the PinePhone with Plasma Mobile
slimbook:
  title: KDE Slimbook
  subtitle: Powerful Hardware, Sleek Software
  description: |
    The first Linux ultrabook with a Ryzen 4000 processor and KDE's full-featured
    Plasma desktop and hundreds of Open Source programs and utilities.
  alt: Picture of the two slimbook models
  choose: Choose your size
  slim14: KDE Slimbook 14”
  amd: AMD Ryzen 7 4800 H
  screen14: 14-inch IPS LED display with 1920 by 1080 resolution at 60Hz
  ssd: Up to 2TB storage SSD
  ram: Up to 64GB memory
  watt: 47‑watt‑hour battery
  kg: 1 kg
  slim16: KDE Slimbook 15.6”
  screen16: 15.6-inch IPS LED display with 1920 by 1080 resolution at 60Hz
  watt16: 92‑watt‑hour battery
  kg16: 1.5 kg
  configure: Configure
  full: See the full comparison
kfocus:
  title: Kubuntu Focus M2
  subtitle: A powerful laptop for daily use or CPU-intensive work
  description: |
    The Kubuntu Focus laptop is a high-powered, workflow-focused laptop
    which ships with Kubuntu installed. This is the first officially
    Kubuntu-branded computer. It is designed for work that requires
    intense GPU computing, fast NVMe storage and optimal cooling to unleash
    the CPU's maximum potential.
  specification: Specifications
  operating: "Operating system pre-installed:"
  cpu: "CPU:"
  cpu_desc: 10th Generation Intel Core i7-10875H Processor, 8 core / 16 thread. 2.3GHz Base, 5.1GHz Turbo
  gpu: "GPU:"
  gpu_desc:  NVIDIA GeForce RTX 2060/2070/2080 GPU PCIe x 16 AND Intel UHD 630 Graphics with CUDA and cuDNN
  memory: "Memory:"
  memory_desc: "Up to 64GB Dual Channel DDR4 3200 MHz"
  display: "Display:"
  display_desc: Full HD 16.1” matte 1080p IPS 144Hz
  storage: "Storage:"
  storage_desc: "2 x M.2 2280 PCIe Gen3x4 interface featuring Samsung 970 Evo Plus at 3,500MB/s and 2,700MB/s seq. read and write"
  price: "Price:"
  price_desc: starting at $1,795
pinebook:
  title: Pinebook Pro
  description: |
    The Pinebook Pro is meant to deliver solid day-to-day Linux or
    *BSD experience and to be a compelling alternative to mid-ranged
    Chromebooks that people convert into Linux laptops. In contrast to
    most mid-ranged Chromebooks however, the Pinebook Pro comes with
    an IPS 1080p 14″ LCD panel, a premium magnesium alloy shell,
    64/128GB of eMMC storage, a 10,000 mAh capacity battery and the
    modularity / hackability that only an open source project can
    deliver – such as the unpopulated PCIe m.2 NVMe slot. The USB-C
    port on the Pinebook Pro, apart from being able to transmit data
    and charge the unit, is also capable of digital video output
    up-to 4K at 60hz.
  alt: Pinebook pro picture
  specification: Specifications
  operating: "Operating system pre-installed:"
  cpu: "CPU:"
  gpu: "GPU:"
  memory: "Memory:"
  display: "Display:"
  display_desc: 1080p IPS Panel
  storage: "Storage:"
  storage_desc: "64GB of eMMC (Upgradable)"
  price: "Price:"
  price_desc: starting at $199
hardware_option: Hardware sellers with KDE Plasma as an option
hardware_desc: "You can find here a list of hardware sellers who offer Plasma as an option when buying the laptop:"
layout: hardware
---

{{< stop-translate >}}

{{< diagonal-box color="blue" href="https://slimbook.es/en" title="Slimbook" src="/content/hardware/slimbook.jpg" >}}

SLIMBOOK was born in 2015 with the idea of being the best brand in the computer
market with GNU / Linux (although it is also verified the absolute compatibility
with Microsoft Windows). They assemble computers searching for excellent quality
and warranty service, at a reasonable price. So much so, that in 2018 they were
awarded Best Open Source Service / Solution Provider at the OpenExpo Europe 2018.

{{< /diagonal-box >}}

{{< diagonal-box color="yellow" href="https://starlabs.systems/" title="Star Labs" src="/content/hardware/starlabs.png" >}}

Star Labs offers a range of laptops designed and built specifically for Linux.
Two version of their laptop are offered: the Star Lite Mk III 11-inch and the 
Star LabTop Mk IV 13-inch.

{{< /diagonal-box >}}

{{< diagonal-box color="green" href="https://www.tuxedocomputers.com/en" title="Tuxedo" src="/content/hardware/Tuxedo.png" >}}

Tuxedo builds tailor-made hardware and all this with Linux!

TUXEDO Computers are individually built computers/PCs and notebooks which are fully Linux compatible, i. e. Linux hardware in tailor-made suit.

All TUXEDO devices are delivered in such a way that you only have to unpack, connect and switch them on. All the computers and notebooks are assembled and installed in-house, not outsourced.

They provide you with self-programmed driver packages, support, installation scripts and everything around our hardware, so that every hardware component really works.

{{< /diagonal-box >}}