---
title: Press Contact
aliases:
- /contact/representatives
---

## Download the KDE Press Kit

If you would like to learn more about KDE, represent us in the media, or
create content about KDE, our Press Kit can be a valuable resource.

The Press Kit contains a booklet with written guidelines and resources,
and a selection of images that can be freely used in any KDE-related
content.

<a href="https://share.kde.org/s/XgobC4pWK5LxXFt" class="button">
Download the latest KDE Press Kit here.</a>

## Get in touch with KDE's Press Room at <press@kde.org>

The people working in KDE's Press Room can help you or your media
organisation arrange an interview with KDE developers and contributors,
obtain further information about projects and people within KDE, and
officially contact the KDE Project. We can also provide you with
statements, stories, quotes, graphical material and videos for your
outlet.

Unfortunately we cannot provide you with technical support, but we know
who can:

-   If you have problems with the KDE.org website, e-mail
    <kde-www@kde.org>.
-   If you need technical support, get in touch with your operating
    system vendor or subscribe to one of our [mailing
    lists](/mailinglists/) and ask there. You can also check [our
    forums](http://forum.kde.org) for help.
-   If you are a lawyer and need contact information as required by
    German law, go to the
    [Imprint](/community/whatiskde/impressum-en) ([German
    Impressum](/community/whatiskde/impressum)).
