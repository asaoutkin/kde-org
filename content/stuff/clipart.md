---
title: "Press Kit: KDE Clipart"
description: "Here you can find a list of artwork used in the KDE project. Includes the official KDE and Plasma logos, and KDE mascots Konqi and Katie."
hidden: true
aliases:
- /stuff/buttons/
---

<style>

    main.container section {
        padding: 0px;
    }
    
    main.container article + article {
        border-top: solid 1px rgba(0,0,0,.1)
    }
    
    
    main.container p {
        margin: 0px;
        padding: 0px !important;
    }
    
    article {
        padding: 10px 20px;
    }
    
    article small {
        display: block;
        width: 100%;
        margin-bottom: 20px;
        background-color: #e8f1f8;
        border: 1px solid #9cc4e0;
        color: #28618c;
        font-size: 80%;
        border-radius: 4px;
        padding: 10px;
        line-height: 150%;
    }
    
    #logos-current, 
    #logos-legacy,
    #kabel-font,
    #mascots {
        display: flex;
        flex-wrap: wrap;
    }
    
    #kabel-font,
    #mascots {
        justify-content: center;
        align-items: center;
    }
    
    #mascots img {
        border: none;
        padding: 0px;
        background: transparent;
    }
    
    article h2 {
        display: block;
        width: 100% !important;
        padding: 10px 0px;
        text-align: left;
    }
    
    figure img {
        box-sizing: border-box !important;
        margin: 0px 0px 10px 0px !important;
    }
    
    
    #logos-current figure,
    #logos-legacy figure,
    #kabel-font figure,
    #mascots figure {
        width: 50%;
        box-sizing: border-box !important;
        display: block;
        margin: 0px;
        text-align: center;
        padding: 20px;
    }
     
    #mascots figure.left {
        text-align: right;
        padding-right: 0px;
    }
     
    #mascots figure.right {
        text-align: left;
    }
    
    #logos-legacy figure {
        width: 33%;
    }
    
    #kabel-font figure {
        width: 60%;
    }
    
    #logos-current figure > a::after,
    #logos-legacy figure > a::after {
        content: '';
        display: block;
    }
    
    #logos-current img {
        height: 128px;
    }
    
    #logos-legacy img {
        width: 64px;
        height: 64px;
    }
</style>


<article id="kde-store">
    <p>For additional content, images, and media visit <a href="https://store.kde.org/">store.kde.org</a>.</p>
</article>

<article id="logos-current">
    <h2>KDE Logo</h2>
    <small>
        The KDE logo can be used freely as long as it is not used to refer to projects other than KDE itself. There is no formal procedure to use it.
        Copying of the KDE Logo is subject to the LGPL copyright
        license. Trading and branding with the KDE Logo is subject to our
        trademark licence.  For more details on their usage please see <a
        href="http://techbase.kde.org/Development/Guidelines/CIG/KDE_Logo">the KDE CIG
        Logo page</a>.
    </small>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-white-blue-rounded-source.svg" />
        <figcaption>
            White on Blue Rounded
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-white-blue-rounded-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-white-blue-rounded-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-white-blue-rounded-source.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-white-gray-rounded-source.svg" />
        <figcaption>
            White on Gray Rounded
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-white-gray-rounded-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-white-gray-rounded-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-white-gray-rounded-source.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-white-blue-source.svg" />
        <figcaption>
            White on Blue
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-white-blue-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-white-blue-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-white-blue-source.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-white-gray-source.svg" />
        <figcaption>
            White on Gray
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-white-gray-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-white-gray-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-white-gray-source.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-blue-transparent-source.svg" />
        <figcaption>
            Blue Transparent
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-blue-transparent-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-blue-transparent-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-blue-transparent-source.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-gray-transparent-source.svg" />
        <figcaption>
            Gray Transparent
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-gray-transparent-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-gray-transparent-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-gray-transparent-source.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-blue-w-slug-vectorized.svg" />
        <figcaption>
            White on Blue with Slug
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-blue-w-slug-94x128.png" download="">94x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-blue-w-slug-2200x3000.png" download="">2200x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-blue-w-slug-vectorized.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-grey-w-slug-vectorized.svg" />
        <figcaption>
            White on Grey with Slug
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-grey-w-slug-94x128.png" download="">94x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-grey-w-slug-2200x3000.png" download="">2200x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-grey-w-slug-vectorized.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/kde-logo-bluegradient-w-slug-vectorized.svg" />
        <figcaption>
            White on Blue Gradient with Slug
        </figcaption>
        <a href="/stuff/clipart/logo/kde-logo-bluegradient-w-slug-94x128.png" download="">94x128 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-bluegradient-w-slug-2200x3000.png" download="">2200x3000 PNG</a>
        <a href="/stuff/clipart/logo/kde-logo-bluegradient-w-slug-vectorized.svg" download="">Scalable SVG</a>
    </figure>
    
</article>
<article id="logos-current">
    <h2>Plasma Logo</h2>  
    <figure>
        <img src="/stuff/clipart/logo/plasma-logo-colorful.svg" />
        <figcaption>
            Colorful
        </figcaption>
        <a href="/stuff/clipart/logo/plasma-logo-colorful-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/logo/plasma-logo-colorful-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/logo/plasma-logo-colorful.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/logo/plasma-logo-monochrome.svg" />
        <figcaption>
            Monochrome
        </figcaption>
        <a href="/stuff/clipart/logo/plasma-logo-monochrome-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/logo/plasma-logo-monochrome-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/logo/plasma-logo-monochrome.svg" download="">Scalable SVG</a>
    </figure>
</article>
<article id="mascots">
<h2>Mascots</h2>
    <figure class="left">
        <img src="/stuff/clipart/konqi-mascot.png" style="width: 100%; max-width: 400px; height: auto;" />
    </figure>
    <figure class="right">
        <figcaption>
            Konqi and Katie are the primary KDE mascots. There is a wide variety of
            contextual images and media available on our community wiki along with
            usage guidelines.
        </figcaption>
        <a href="https://community.kde.org/Promo/Material/Mascots">Community Wiki</a>
    </figure>
</article>

<article id="logos-legacy">
<h2>Legacy Logos</h2>  
    <small>
        These logos are available for archival and historic reasons, and should not be
        used in the creation of new KDE-related content.
    </small>
    <figure>
        <img src="/stuff/clipart/klogo-official-oxygen-128x128.png" />
        <figcaption>
            Oxygen
        </figcaption>
        <a href="/stuff/clipart/klogo-official-oxygen-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/klogo-official-oxygen-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/klogo-official-oxygen.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/klogo-official-lineart_simple-128x128.png" />
        <figcaption>
            Blue Lineart
        </figcaption>
        <a href="/stuff/clipart/klogo-official-lineart_simple-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/klogo-official-lineart_simple-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/klogo-official-lineart_simple.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/klogo-official-lineart_detailed-128x128.png" />
        <figcaption>
            Blue Detailed Lineart
        </figcaption>
        <a href="/stuff/clipart/klogo-official-lineart_detailed-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/klogo-official-lineart_detailed-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/klogo-official-lineart_detailed.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/klogo-official-lineart_simple_bw-128x128.png" />
        <figcaption>
            Grey Lineart
        </figcaption>
        <a href="/stuff/clipart/klogo-official-lineart_simple_bw-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/klogo-official-lineart_simple_bw-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/klogo-official-lineart_simple_bw.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/klogo-official-lineart_detailed_bw-128x128.png" />
        <figcaption>
            Grey Detailed Lineart
        </figcaption>
        <a href="/stuff/clipart/klogo-official-lineart_detailed_bw-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/klogo-official-lineart_detailed_bw-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/klogo-official-lineart_detailed_bw.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/klogo-official-crystal-128x128.png" />
        <figcaption>
            Blue Crystal
        </figcaption>
        <a href="/stuff/clipart/klogo-official-crystal-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/klogo-official-crystal-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/klogo-official-crystal.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/klogo-crystal-128x128.png" />
        <figcaption>
            Crystal
        </figcaption>
        <a href="/stuff/clipart/klogo-crystal-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/klogo-crystal-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/klogo-crystal.svg" download="">Scalable SVG</a>
    </figure>
    <figure>
        <img src="/stuff/clipart/klogo-classic-128x128.png" />
        <figcaption>
            Classic
        </figcaption>
        <a href="/stuff/clipart/klogo-classic-128x128.png" download="">128x128 PNG</a>
        <a href="/stuff/clipart/klogo-classic-3000x3000.png" download="">3000x3000 PNG</a>
        <a href="/stuff/clipart/klogo-classic.svg" download="">Scalable SVG</a>
    </figure>
</article>

