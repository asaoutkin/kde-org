---
version: "5.21.0"
title: "KDE Plasma 5.21.0, feature Release"
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
draft: false
---

This is a feature release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.21.0 announcement](/announcements/plasma-5.21.0).
