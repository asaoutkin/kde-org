<ul>
    <!--
    <li>
        An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
        shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
    -->

    <!--
    <li>
        <strong>Debian</strong> KDE 4.7.1 packages are available in the unstable repository.
    </li>
    -->

    <!--
    <li>
        <strong>Fedora</strong> KDE 4.7.1 packages are available:
        <ul>
          <li><a href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> development repository</li>
          <li><a href="http://admin.fedoraproject.org/updates/F15/FEDORA-2011-5221">Fedora 15</a></li>
          <li><a href="http://admin.fedoraproject.org/updates/F14/FEDORA-2011-5200">Fedora 14</a></li>
        </ul>
    </li>
    -->

    <!--

    <li>
       <strong>FreeBSD</strong> KDE 4.7.1 ports are available in area51.
       Please check the <a href="http://freebsd.kde.org/area51.php">KDE on FreeBSD</a> website for more information
       on how to use ports from area51.
    </li>
    -->

    <li>
        <strong>Gentoo Linux</strong> provides KDE SC 4.7.1 packages in main repository.<br/>
        More details can be found on <a href="http://www.gentoo.org/proj/en/desktop/kde/kde4-guide.xml">Gentoo KDE Guide</a>.
    </li>

    <li>
        <strong>Kubuntu</strong> packages are available for 11.04 and Oneiric.
        More details can be found in the <a href="http://www.kubuntu.org/news/kde-4.7.1">
        announcement on Kubuntu.org</a>.
    </li>

    <!--
    <li>
        <strong>Mandriva</strong> provide packages for
        <a href="http://download.kde.org/binarydownload.html?url=/stable/4.7.1/Mandriva/2010.1/i586">2010.1 i586</a>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/4.7.1/Mandriva/2010.1/x86_64">2010.1 x86_64</a>
    </li>
    -->

    <li>
        <strong>openSUSE</strong> packages <a href="http://en.opensuse.org/Portal:KDE">are available</a> for openSUSE 11.4
        (<a href="http://download.opensuse.org/repositories/KDE:/Release:/47/openSUSE_11.4/KDE4-BASIS.ymp">one-click install</a>),
        for openSUSE 11.3
        (<a href="http://download.opensuse.org/repositories/KDE:/Release:/47/openSUSE_11.3/KDE4-BASIS.ymp">one-click install</a>),
        and openSUSE Factory
        (<a href="http://download.opensuse.org/repositories/KDE:/Release:/47/openSUSE_Factory/KDE4-BASIS.ymp">one-click install</a>).
      <!--  A <a href="http://home.kde.org/~kdelive/">KDE Four Live CD</a> with these packages is also available. -->
    </li>

    <!--

    <li>
        <strong>Solaris</strong> KDE 4.7.1 packages for Solaris 11 Express and OpenIndiana are available from the <a href="http://solaris.bionicmutton.org/pkg/4.7.1/">repository of the kde-solaris project</a>. See also the <a href="http://blogs.fsfe.org/adridg/?p=1067">announcement</a>.
    </li>
    -->

    <!--
    <li>
        <strong>Pardus</strong> KDE 4.2 Beta 2 packages are available for Pardus 2008 in <a href="http://paketler.pardus.org.tr/pardus-2008-test/">Pardus test repository</a>. Also all source PiSi packages are in <a href="http://svn.pardus.org.tr/pardus/devel/desktop/kde4/base/">Pardus SVN</a>.
    </li>
    -->

    <!--
    <li>
        <strong>Magic Linux</strong> KDE 4.3.85 packages are available for Magic Linux 2.5.
        See <a href="http://www.linuxfans.org/bbs/thread-182132-1-1.html">the release notes</a>
        for detailed information and
        <a href="http://apt.magiclinux.org/magic/2.5/unstable/RPMS.all/">the FTP tree</a> for
        packages.
    </li>
    -->

    <!--
    <li>
        <strong>Windows</strong> KDE 4.3.85 packages for Microsoft Windows.
        See <a href="http://windows.kde.org">windows.kde.org</a> for details. Download installer and get your kde apps on your windows desktop. Note: KDE on Windows is not in the final state, so many applications can be unsuitable for day to day use yet.
    </li>
    -->

</ul>
