KDE Project Security Advisory
=============================

Title:          kinit: World readable X11 Cookie key logger
Risk Rating:    Important
CVE:            CVE-2016-3100
Platforms:      X11
Versions:       kinit < 5.23
Author:         Siddharth Sharma siddharth.kde@gmail.com
Date:           21 June 2016

Overview
========

An authorized user can log key events of other user by accessing
world-readable X11 cookie


Impact
======

Pre-authenticated attacker can read all key events by the users logged on
to the system.

Workaround
==========

None

Solution
========

For kinit apply the following patches:
https://quickgit.kde.org/?p=kinit.git&a=commitdiff&h=dece8fd89979cd1a86c03bcaceef6e9221e8d8cd
https://quickgit.kde.org/?p=kinit.git&a=commitdiff&h=72f3702dbe6cf15c06dc13da2c99c864e9022a58

References
==========

https://bugs.kde.org/show_bug.cgi?id=358593
https://bugs.kde.org/show_bug.cgi?id=363140

Credits
=======

Thanks to David Rumley for finding the issue and Albert Astals Cid for fixing the issue.

