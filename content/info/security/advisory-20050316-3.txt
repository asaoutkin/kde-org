-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Insecure temporary file creation by dcopidlng
Original Release Date: 20050316
URL: http://www.kde.org/info/security/advisory-20050316-3.txt

0. References
        http://bugs.kde.org/show_bug.cgi?id=97608
        http://www.gentoo.org/security/en/glsa/glsa-200503-14.xml 
        http://bugs.gentoo.org/attachment.cgi?id=51120&action=view
        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-0365

1. Systems affected:

        All KDE versions in the KDE 3.2.x and KDE 3.3.x series.
        This problem only affects users who compile KDE or KDE applications
        themselves.

2. Overview:

        The dcopidlng script is vulnerable to symlink attacks, potentially
        allowing a local user to overwrite arbitrary files of a user when
        the script is run on behalf of that user.

        The dcopidlng script is run as part of the build process of KDE
        itself and may be used by the build process of third party KDE
        applications.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2005-0365 to this issue.

      
3. Impact:

        The dcopidlng script is vulnerable to symlink attacks, potentially
        allowing a local user to overwrite arbitrary files of a user when
        that user compiles KDE or third party KDE applications that use the
        dcopidlng script as part of their build process.


4. Solution:

        Upgrade to KDE 3.4.

        For older versions of KDE Source code patches have been made
        available which fix these vulnerabilities.

        Installed versions of dcopidlng can be patched manually as follows:

            cd $(kde-config --expandvars --install exe)
            patch < ~/post-3.2.3-kdelibs-dcopidlng.patch

5. Patch:

        A patch for KDE 3.2.x is available from
        ftp://ftp.kde.org/pub/kde/security_patches

        43213bb9876704041af622ed2a6903ae  post-3.2.3-kdelibs-dcopidlng.patch

        A patch for KDE 3.3.x is available from
        ftp://ftp.kde.org/pub/kde/security_patches

        43213bb9876704041af622ed2a6903ae  post-3.3.2-kdelibs-dcopidlng.patch


6. Time line and credits:

        21/01/2005 Problem reported to bugs.kde.org by Davide Madrisan
        21/01/2005 Patches applied to KDE CVS.
        16/03/2005 KDE Security Advisory released.
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iD8DBQFCOBcDN4pvrENfboIRAp0CAJ9hVKorTEzaih0Kriqe/K42C/hTyACeLWzH
Jdng6kwtzYt+bQD70ckUMPk=
=HVnd
-----END PGP SIGNATURE-----
