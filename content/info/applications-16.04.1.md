---
version: "16.04.1"
title: "KDE Applications 16.04.1 Info Page"
announcement: /announcements/announce-applications-16.04.1
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
