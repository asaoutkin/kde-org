---
title : "KDE 3.0.5 Info Page"
publishDate: 2002-11-08 00:01:00
unmaintained: true
---

<p>
KDE 3.0.5 was released on November 18th, 2002.
Read the <a href="/announcements/announce-3.0.5">official announcement</a>.
</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a> and sound related
questions are answered in the
<a href="http://www.arts-project.org/doc/handbook/faq.html">FAQ of the aRts Project</a>

<h2>Download and Installation</h2>

<u>Source code</u>

<a href="/info/sources/source-3.0.5">Source 3.0.5</a>

<p>
  The translation package has been split into individual language
  packages so you can
  <a href="http://download.kde.org/stable/3.0.5/src/kde-i18n/">download</a> only the
  translations you need.
</p>

<u>Binary packages</u>

<p>
Binary packages have meanwhile be removed from our FTP.</p>

<h2>Updates</h2>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="&#x6d;a&#x69;&#x6c;&#116;&#111;:&#115;&#x65;cur&#0105;&#x74;&#x79;&#x40;&#x6b;d&#x65;&#0046;org">s&#101;c&#x75;&#x72;&#0105;ty&#x40;&#107;&#00100;e&#x2e;o&#114;g</a>.</p>

<ul>
<li>
<p>Several shell escaping vulnerabilities have been found throughout KDE which allow a remote attacker to execute commands as the local user.
   Read the <a href="security/advisory-20021220-1.txt">detailed
   advisory</a>. It is strongly recommended to update to KDE 3.0.5a.
</p>
</li>
<li>
Several problems with KDE's use of Ghostscript where discovered that allow the execution of
arbitrary commands contained in PostScript (PS) or PDF files with the privileges of the victim.
Read the <a href="security/advisory-20030409-1.txt">detailed advisory</a>.
It is strongly recommended to update to <a href="3.0.5b.php">KDE 3.0.5b</a>
</li>
<li>
A HTTP authentication credentials leak via the a "Referrer" was discovered by George Staikos
in Konqueror. If the HTTP authentication credentials were part of the URL they would be possibly sent
in the referer header to a 3rd party web site.
Read the <a href="security/advisory-20030729-1.txt">detailed advisory</a>. KDE 3.1.3 and newer
are not vulnerable.
</li>
</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release date:</p>

<ul>
<li>currently none known.</li>
</ul>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.0-api/classref/index.html">
programming interface of KDE 3.0</a>.
</p>

