---
version: "18.11.80"
title: "KDE Applications 18.11.80 Info Page"
signer: Albert Astals Cid
signing_fingerprint: 0x8692A42FB1A8B666C51053919D17D97FD8224750
announcement: /announcements/announce-applications-18.12-beta
type: info/application-v1
---
