<?php
  $page_title = "KDE 3.90.1 Info Page";
  $site_root = "../";
  include "header.inc";
?>

<p>
KDE 3.90.1 was released on May 11th, 2007.
</p>

<p>This page will be updated to reflect changes in the status of 3.90.1
release cycle so check back for new information.</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<ul>
<li>None known yet</li>

</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release was packaged:</p>

<ul>
<li>None known yet</li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a>.

<h2>Download and Installation</h2>

<p>
<u>Build instructions</u>.
 <a href="http://techbase.kde.org/Getting_Started/Build/KDE4_Alpha_1">Build instructions</a>
 are separately maintained.
</p>
<p>
  The complete source code for KDE 3.90.1 is available for download:
</p>

<?php
include "source-3.90.1.inc"
?>

<u><a name="binary">Binary packages</a></u>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.90.1 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.90.1/">http or FTP mirrors</a>.
</p>

<p>
  Currently pre-compiled packages are available for:

<ul>
	<li><a href="http://kubuntu.org/announcements/kde4-alpha1.php">Kubuntu</a> </li>
	<li><a href="http://en.opensuse.org/KDE4">openSUSE</a> </li>
	<li><a href="http://ranger.users.finkproject.org/kde/index.php/Home">OS X</a> </li>
	<li><a href="http://overlays.gentoo.org/proj/kde">Gentoo</a> </li>
</ul>
</p>


<h2>Developer Info</h2>

<p>
If you need help porting your application to KDE 4 see the <a
href="http://websvn.kde.org/*checkout*/trunk/KDE/kdelibs/KDE4PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.
<p>

<?php
  include "footer.inc";
?>
