<ul>
<!--
       <li>
       An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
       shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
-->
       <li>
       <strong>Debian</strong> KDE 4.0.98 packages are available in the experimental
	   repository.  The KDE Development Platform is in unstable. More
	   details can be found in the <a href="http://pkg-kde.alioth.debian.org/experimental.html">
	   installation instructions</a>.
       </li>

       <li>
       <strong>Fedora</strong> KDE 4.0.98 packages are in the experimental
       <a href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a>
       development repository, as well as in the unstable repository hosted
       at the <a href="http://kde-redhat.sourceforge.net/">kde-redhat project</a>.
       </li>
<!--
       <li>
       <strong>Gentoo Linux</strong> provides KDE 4.0.1 builds on
       <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
       </li>
-->
       <li>
                <strong>Kubuntu</strong> packages are included in the upcoming "Intrepid Ibex"
                (8.10) and also made available as updates for the stable 8.04 ("Hardy Heron").
                More details can be found in  the <a href="http://kubuntu.org/news/kde-4.1rc1">
                announcement on Kubuntu.org</a>.
        </li>

	<!--
        <li>
                <strong>Mandriva</strong> provide packages for 
                <a
					 href="http://download.kde.org/binarydownload.html?url=/stable/4.0.98/Mandriva/">2008.1
					 i586 ( Mandriva Spring )</a>. 
					 Please refer to README to more information and how to instal
					 debug packages to provide upstream developers proper information
					 in case you desire help KDE improving.
					 For Mandriva cooker ( development ) users, 4.0.98 is fully available at cooker repositories.
        </li>
	-->

        <li>
                <strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE/KDE4">are available</a> 
                for openSUSE 11.0
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_11.0/KDE4-BASIS.ymp">one-click 
                install</a>), 
                for openSUSE 10.3 
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
                install</a>) and	
		openSUSE Factory 
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_Factory/KDE4-BASIS.ymp">one-click 
                install</a>).
                A <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
                Four Live CD</a> with these packages is also available.
        </li>
        <li>
                <strong>Magic Linux</strong> KDE 4.0.98 packages are available for Magic Linux 2.1.
                See <a href="http://www.linuxfans.org/bbs/thread-182132-1-1.html">the release notes</a>
                for detailed information and
                <a href="http://ftp.magiclinux.org.cn/nihui/kde4/RPMS.kde41x/">the FTP tree</a> for
                packages.
        </li>
</ul>
