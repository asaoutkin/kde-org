<ul>
<!--
       <li>
       An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
       shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
-->
<!--
       <li>
       <strong>Debian</strong> KDE 4.0.71 packages are available in the experimental branch.
       The KDE Development Platform is in <em>Sid</em>. Watch for
       announcements by the <a href="http://pkg-kde.alioth.debian.org/">Debian KDE Team</a>.
       </li>
-->
       <li>
       <strong>Fedora</strong> KDE 4.0.80 packages are in the experimental
       <a href="https://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> development repository.
       </li>
<!--
       <li>
       <strong>Gentoo Linux</strong> provides KDE 4.0.1 builds on
       <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
       </li>
-->
<!--
       <li>
                <strong>Kubuntu</strong> packages are included in the upcoming "Hardy Heron"
                (8.04) and also made available as updates for the stable "Gutsy Gibbon" (7.10).
                More details can be found in  the <a href="http://kubuntu.org/announcements/kde-4.0.71.php">
                announcement on Kubuntu.org</a>.
        </li>
-->
<!--
        <li>
                <strong>Mandriva</strong> will provide packages for 
                <a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.1/Mandriva/">2008.0</a> and aims
                at producing a Live CD with the latest snapshot of 2008.1.
        </li>a
-->
        <li>
                <strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE/KDE4">are available</a> 
                for openSUSE 11.0
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/Desktop/openSUSE_11.0/KDE4-BASIS.ymp">one-click 
                install</a>), 
                for openSUSE 10.3 
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
                install</a>), 
		
		openSUSE Factory 
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/Desktop/openSUSE_Factory/KDE4-BASIS.ymp">one-click 
                install</a>) and openSUSE 10.2.
                A <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
                Four Live CD</a> with these packages is also available.
        </li>
        <li>
                <strong>Magic Linux</strong> KDE 4.0.80 packages are available for Magic Linux 2.1.
                See <a href="http://www.linuxfans.org/bbs/thread-182132-1-1.html">the release notes</a>
                for detailed information and
                <a href="http://ftp.magiclinux.org.cn/nihui/kde4/RPMS.kde41x/">the FTP tree</a> for
                packages.
        </li>
</ul>
