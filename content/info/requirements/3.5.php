<?php
  $page_title = "KDE 3.5 Compilation Requirements";
  $site_root = "../../";
  include "header.inc";
?>

<h4>Compilers</h4>
<p align="justify">
  <em>Compiler Requirements</em>.
  KDE is designed to be portable and hence to compile with a large
  variety of GNU/Linux and UNIX compilers. However, KDE is advancing very rapidly
  and the ability of native compilers on various UNIX systems to compile
  KDE depends on users of those systems
  <a href="http://bugs.kde.org/">reporting</a> compile problems to
  the responsible developers.
</p>

<p align="justify">
  In addition, the C++ implementation in <a href="http://gcc.gnu.org/">gcc/egcs</a>,
  the most popular KDE compiler, has been advancing rapidly, and has also
  recently undergone a major redesign.  As a result, KDE will not compile
  properly with older versions of gcc or most newer releases.
</p>

<p align="justify">
  In particular, gcc versions earlier than gcc-2.95, such as egcs-1.1.2
  or gcc-2.7.2, may not properly compile some components of KDE 3.5.
  While there have been reports of successful KDE compilations with
  the so-called gcc-2.96 and gcc-3.4 (cvs), the KDE project at this time
  recommends the use of gcc-3.3.1 or a version of gcc
  which shipped with a stable Linux distribution and which was used
  successfully to compile a stable KDE for that distribution.
</p>

    <div align="center">
      <a href="#Basic">Basic</a>&nbsp;&nbsp;<a href="#Help">Help</a>&nbsp;&nbsp;<a href="#Hardware">Hardware</a>&nbsp;&nbsp;<a href="#Networking">Networking</a>&nbsp;&nbsp;<a href="#Browsing">Browsing</a>&nbsp;&nbsp;<a href="#Security">Security</a>&nbsp;&nbsp;<a href="#Graphics">Graphics</a>&nbsp;&nbsp;<a href="#Multimedia">Multimedia</a>&nbsp;&nbsp;<a href="#Development">Development</a>
    </div>
    <br />

    <table border="1" cellpadding="4" cellspacing="0">
      <tr>
        <td align="center" colspan="5">Basic</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center" width="25%"><strong>Description</strong></td>

        <td align="center" width="25%"><strong>Explanation</strong></td>

        <td align="center" width="25%"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.trolltech.com/">Qt &gt;= 3.3.2 &amp; &lt; 4.0</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>Qt is the C++ cross-platform GUI toolkit upon which the great majority of KDE is built.</td>

        <td>Qt is required by all KDE modules.</td>

        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td>X&nbsp;Server</td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>An X&nbsp;Server provides the underlying display technology on UNIX systems. The KDE Project recommends the <a href="http://www.x.org/">X.org</a> or <a href="http://www.xfree86.org/">XFree86</a> servers.</td>

        <td>An X server is required by all KDE modules.</td>

        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td>X Render Extension</td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>The X Render Extension adds support for alpha-blended icons, anti-aliased fonts and drawing primitives, as well as true-color animated mouse cursors to the X window system.
            This extension is available in recent versions of <a href="http://www.x.org/">X.org</a> and <a href="http://www.xfree86.org/">XFree86</a>.</td>

        <td>Nearly all KDE applications benefit from alpha blended icons and anti-aliased fonts.</td>

        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td>X Fixes Extension</td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The X Fixes extension was created to address a number of shortcomings in the core X window system that prevented applications from doing some things efficiently. The X Fixes extension is available in version 6.8 or later of <a href="http://www.x.org/">X.org</a>.</td>

        <td>Klipper benefits from the XFixes extension, in the form of reduced CPU utilization in both Klipper and the X server. This extension is also needed by applications that use the Composite and Damage extensions.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td>X Composite Extension</td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The X Composite extension adds support for translucent windows and non-rectangular windows with anti-aliased edges to the X window system. It also makes the content of obscured windows available to for example desktop pagers, and provides backing store which eliminates flicker. It also makes it possible for a composition manager to draw dynamic effects on the screen, such as window drop shadows. The X Composite Extension is available in version 6.8 or later of <a href="http://www.x.org/">X.org</a>.</td>

        <td>All KDE applications benefit from reduced flicker, and effects drawn by the composition manager. Kicker uses the composite extension for creating thumbnails of windows and displaying them in the pager.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td>X Damage Extension</td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The X Damage Extension provides notifications to applications when the content of the screen or individual windows changes as a result of applications drawing on them. The X Damage Extension is available in version 6.8 or later of <a href="http://www.x.org/">X.org</a>.</td>

	<td>The Damage extension is needed by the composition manager to be able to know when to update dynamic effects.</td>

	<td>kdebase</td>
      </tr>

      <tr valign="top">
        <td>X DPMS Extension</td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>The DPMS extension provides energy-saving power management for most modern monitors.
            This extension is included by default in recent X&nbsp;Servers from <a href="http://www.x.org/">X.org</a> and <a href="http://www.xfree86.org/">XFree86</a>.</td>

        <td>KDE enables you to configure your display&apos;s DPMS settings.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td>X Video Extension</td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The X Video Extension (xv) provides hardware-accelerated video playback for X&nbsp;Servers.
            This extension is included by default in recent X&nbsp;Servers from <a href="http://www.x.org/">X.org</a> and <a href="http://www.xfree86.org/">XFree86</a>.</td>

        <td>The video subsytem will perform much better if the X Video Extension is available.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td>Xft &gt;=2.0</td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>The Xft library works in combination with the fontconfig and freetype2 libraries to provide an interface for applications to draw anti-aliased text using the X render extension. This library in combination with fontconfig, freetype2 and the X render extension is required for anti-aliased fonts in KDE applications.</td>

        <td>Nearly all KDE applications benefit from anti-aliased fonts.</td>

        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>

      </tr>

      <tr valign="top">
        <td><a href="http://www.fontconfig.org">fontconfig</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Fontconfig is a library that provides system wide font customization and configuration, and enables applications to discover which fonts are installed on the system. This library in combination with Xft, freetype2 and the X render extension is required for anti-aliased fonts in KDE applications.</td>

        <td>Nearly all KDE applications benefit from anti-aliased fonts.</td>
        
        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.cs.wisc.edu/~ghost/index.html">Ghostscript</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Ghostscript provides PostScript&reg; and PDF (Portable Document Format) rendering and manipulation for KDE. This functionality is neeeded by print previews and filters.</td>

        <td>Almost all of KDE benefits from Ghostscript due to the rendering and filtering it provides. Version 6.53 or later is recommended.</td>

        <td>kdebase, kdeaddons, kdeedu, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://">Database Server</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Databases provide a scalable method for organizing, accessing and modifying data quickly.</td>

        <td>Various applications either require or work better when a database is used to keep their data. While the database requirements vary from application to application, the most common requirements are <a href="http://www.mysql.com/">MySQL</a> and/or <a href="http://www.postgresql.org/">PostgreSQL</a> and/or <a href="http://genix.net/unixODBC/">unixODBC</a>.</td>

        <td>kdepim</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.sleepycat.com/">Berkeley DB 4</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Berkeley DB 4 provides a fast, scalable, in-process database engine.</td>

        <td>KDevelop, the KDE development environment, requires it. KBabel, the KDE translation tool, can use it.</td>

        <td>kdevelop, kdesdk</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.python.org/">Python</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Python is a modern, object-oriented programming language.</td>

        <td>Some KOffice components use Python as a scripting engine. In addition, Python bindings to KDE are available for KDE development using Python.</td>

        <td>kdebindings, koffice</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.perl.com/">Perl</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>Perl is a modern programming language.</td>

        <td>The info page KIO needs Perl to convert info pages to HTML. KSirc can be scripted in Perl. Several modules require Perl for source code manipulation during their compilation.</td>

        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.digistar.com/bzip2/">bzip2</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>Bzip2 is a patent-free compression and decompression library. Compared to gzip, it provides greater compression at the cost of being substantially slower.</td>

        <td>Some KDE applications enable compressing or decompressing bzip'd files.</td>

        <td>kdelibs, kdeadmin, kdeutils</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.dante.de/">TeX</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>TeX, together with LaTeX, provide a standard, high-quality publishing system.</td>

        <td>Some KDE applications can import or export TeX documents.</td>

        <td>kdegraphics, koffice</td>
      </tr>

      <tr valign="top">
        <td>X Xinerama Extension</td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The X Xinerama extension provides support for multi-headed (multi-monitor) setups.
            This extension is included by default in recent X&nbsp;Servers from <a href="http://www.x.org/">X.org</a> and <a href="http://www.xfree86.org/">XFree86</a>.</td>

        <td>The KDE window manager supports the use of multiple monitors.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td>Xcursor library</td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The Xcursor library provides support for colored and animated cursor themes.
            This library is included by default in recent versions of <a href="http://www.x.org/">X.org</a> and <a href="http://www.xfree86.org/">XFree86</a>, but can also be installed as a standalone library and used with any X server. For full functionality the X server must support Xrender version 0.8 and Xfixes version 2.0.</td>

        <td>The KDE Control Center supports configuring the cursor themes.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gzip.org/zlib/">zlib &gt;=1.1</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>A compression/decompression library.</td>

        <td>A system zlib library is preferred by Qt, though Qt provides its own copy to fall back on. Various KDE applications make use of zlib to compress or decompress data.</td>

        <td>kdelibs, kdebase, arts, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://acl.bestbits.at/">ACL and Attr</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>File permission access control.</td>

        <td>KDE can use libacl for file permission Access Control Lists.</td>

        <td>kdelibs</td>
      </tr>

      <tr>
        <td align="center" colspan="5">Help</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xmlsoft.org/">libxml2 &gt;= 2.4.8</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libxml provides an XML parser and toolkit. XML is a metalanguage to design markup languages, such as HTML.</td>

        <td>Libxml is used for reading KDE documentation. Note that even though earlier versions of libxml2 may work,  versions 2.4.26 through 2.4.28 have a bug which prevents their use in KDE.</td>

        <td>kdelibs, kdebase, arts, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gtk.org/">GLib &gt;= 2.4</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>GLib is the low-level core library that forms the basis of GTK+.</td>

        <td>GLib is used by 3rd party libraries in arts and kopete</td>

        <td>arts, kdenetwork</td>
      </tr>

      <tr valign="top">
        <td><a href="http://xmlsoft.org/XSLT/">libxslt &gt;= 1.0.7</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libxslt provides XSLT processing. XSLT is itself an XML language used to define XML transformations (<em>i.e.</em>, from one XML language to another).</td>

        <td>Libxslt is needed to read KDE documentation.</td>

        <td>kdelibs, kdebase, arts, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.htdig.org/">ht dig</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>ht://dig is a document indexing and search facility.</td>

        <td>KDevelop uses ht://dig to index and search documentation.</td>

        <td>kdevelop</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Hardware</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.cups.org/">CUPS &gt;= 1.1.9</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>CUPS (the Common Unix Printing System) is a modern printing architecture for UNIX systems.</td>

        <td>CUPS provides enhanced printing administration, printing options and usability to all KDE applications. KDE provides tools to configure CUPS.</td>

        <td>kdelibs, kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gphoto.org/">gPhoto 2 &gt;= 2.0.1</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>GPhoto is a set of digital camera software applications for Unix-like systems.</td>

        <td>Some KDE applications and services which can work with images on digital cameras require gPhoto 2.</td>

        <td>kdeaddons, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.mostang.com/sane/">SANE</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>SANE is an application programming interface (API) that provides standardized access to raster image scanner hardware.</td>

        <td>KDE applications which work with scanners require SANE.</td>

        <td>kdelibs, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://secure.netroedge.com/~lm78/">lm_sensors</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Lm_sensors provides Linux monitoring tools for systems with hardware health monitoring, such as temperature sensors.</td>

        <td>KDE applications which enable you to monitor your system's hardware health require lm_sensors to perform the monitoring function.</td>

        <td>kdeadmin</td>
      </tr>

      <tr valign="top">
        <!-- mtools 3.9.9 is known to work but earlier version could work too, but not much earlier versions -->
        <td><a href="http://mtools.linux.lu/">mtools &gt;= 3.9.9</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Mtools is a collection of utilities to access MS-DOS disks from Unix without mounting them. It supports Win'95 style long file names, OS/2 Xdf disks and 2m disks (store up to 1992k on a high density 3 1/2 disk).</td>

        <td>Using mtools you can access a floppy drive from Konqueror without first mounting the device.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="ftp://ftp.penguinppc.org/projects/hfsplus">hfsplus</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Hfsplus is a collection of utilities to access Macintosh disks from Unix without mounting them.</td>

        <td>Using hfsplus you can access a Macintosh floppy from Konqueror.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="https://www.jlogday.com/code/libmal/">libmal &gt;=0.20</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Provides the functionality to synchronize PalmOS handhelds with MAL servers like AvantGo.</td>

        <td>The AvantGo conduit of KPilot depends on this library to synchronize with AvantGo. If the library or the header files are not available during compilation, the AvantGo conduit (malconduit) will not be available.</td>

        <td>kdepim</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Networking</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="ftp://cs.anu.edu.au/pub/software/ppp/">pppd</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Pppd is a PPP (Point-to-Point Protocol) daemon. PPP is a common protocol for connecting to the Internet via a modem.</td>

        <td>Kppp uses pppd to connect to the Internet using a computer POTS modem.</td>

        <td>kdenetwork</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.samba.org/">libsmbclient &gt;=3.0.4</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libsmbclient is a part of Samba and provides access to Windows SMB shares. It provides client (both download and upload) services but not server services.</td>

        <td>Some KDE applications use libsmb to access Windows shares.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.openldap.org/">libldap</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Libldap implements LDAP (the Lightweight Directory Access Protocol). LDAP provides access to X.500 directory services, both stand-alone and as part of a distributed directory service.</td>

        <td>The KDE Address Book and some PIM applications can access directory and user information from an LDAP server with libldap.</td>

        <td>kdelibs, kdepim</td>
      </tr>

      <tr valign="top">
        <td><a href="http://hal.freedesktop.org/wiki/Software/hal">HAL &gt;= 0.4.x</a>,
        <a href="http://hal.freedesktop.org/wiki/Software/dbus">DBus &gt;= 0.2x</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>HAL is a hardware abstraction layer that exposes hardware properties and makes them available through a D-Bus service.</td>

        <td>The media kioslave can use HAL to list media and to listen to hotplug events. If HAL is not running, media:/ will fall back on fstab tracking.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://oss.sgi.com/projects/fam/">FAM</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>FAM is a server which tracks changes to the filesystem and relays these changes to interested applications. Its efficiency comes from consolidating the polling for all applications or, with kernel support, from obtaining kernel notifications of file system changes.</td>

        <td>The KDE subsytem relies on monitoring changes in the filesystem so that the GUI provides an up-to-date view. FAM makes this process more efficient.</td>

        <td>kdelibs, kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.openslp.org/">OpenSLP</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>SLP (Service Location Protocol) implementation. Allows browsing and searching for network services.</td>

        <td>Used by the Desktop Sharing server and the Remote Desktop Connection client to find servers. For the client an installation of the library is sufficient, on servers the slpd daemon must run.</td>

        <td>kdenetwork</td>
      </tr>
      <tr valign="top">
        <td><a href="http://www.pilot-link.org/">Pilot-Link</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>pilot-link is a library that provides means to communicate with
        Palm OS (tm) devices such as the Palm Pilot, Handspring Visor, and Sony Clie.
        </td>

        <td>
        pilot-link is needed by KPilot, the KDE-to-Palm synchronization
	tool. For KDE 3.5.6, pilot-link >= 0.12.0 is required for KPilot.
        </td>

        <td>kdepim</td>
      </tr>
      <tr valign="top">
        <td><a href="http://asg.web.cmu.edu/sasl/sasl-library.html">Cyrus SASL</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Cyrus SASL provides authentication mechanisms. It comes with a library and plugins
            (one plugin for each type of authentication, e.g. plain, gssapi, sasldb etc.) </td>

        <td>The POP3 and SMTP support (kioslaves) use the Cyrus SASL library when available</td>

        <td>kdebase</td>
      </tr>
      <tr valign="top">
        <td><a href="http://www.opensource.apple.com/darwinsource/tarballs/apsl/mDNSResponder-87.tar.gz">mDNS</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>mDNS is a daemon needed for KDE's service discovery feature. It is compatible with ZeroConf implementations such as Apple's Rendevouz technology.</td> 

        <td>Applications that use mDNS include: 
          <ul>
            <li>Konqueror and any application capable of using KDE's file dialog via the dnssd:/ ioslave for discovering ftp, nfs, ssh, telnet and ldap servers, shared desktops and application specific services.</li>
            <li>krfb and krdc for discovering remote desktop connections.</li>
            <li>Games like lskat, KWin4 and KBattleship for discovering game servers.</li>
          </ul>
        </td>
        <td>kdelibs, kdenetwork, kdegames</td>
      </tr>


      <tr>
        <td align="center" colspan="5">Browsing</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://java.sun.com/">Java &gt;= 1.3</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Java is a programming language designed for use in the distributed environment of the Internet. Since Netscape integrated Java into its browser, Java has become a popular language for websites.</td>

        <td>Some websites require the use of Java for some or all of their services. In addition, Java bindings exist for KDE which enables writing KDE applications in the Java language.</td>

        <td>kdebase, kdebindings</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.winehq.org/">WINE</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>WINE is an implementation of the Windows 32 and 16 bit APIs on top of X and UNIX. It can be used to execute an ever-increasing number of applications written for Windows on GNU/Linux and UNIX systems.</td>

        <td>The KDE web browser Konqueror can use WINE to run some ActiveX controls, such as the Quicktime video player.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.codeweavers.com/products/cxoffice/">Crossover Plugin</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>CrossOver Plugin, an extension of WINE, lets you use many Windows ActiveX plugins, such as QuickTime, ShockWave Director and Windows Media Player 6.4, directly from within your Linux browser.</td>

        <td>The KDE web browser Konqueror can use CrossOver Plugin to support some proprietary but common web technologies.</td>

        <td>kdebase</td>
      </tr>
      <tr valign="top">
        <td><a href="http://www.pcre.org/">PCRE</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>The PCRE (Perl Compatible Regular Expressions) library is a set of functions that implement regular expression pattern
        matching using the same syntax and semantics as Perl 5.</td>

        <td>The regular-expression support in the KDE JavaScript engine (KJS) uses PCRE.</td>

        <td>kdelibs</td>
      </tr>
      <tr valign="top">
        <td><a href="http://www.gnu.org/software/libidn/">IDN</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libidn provides support for IETF Internationalized Domain Names (IDN)</td>

        <td>Domain name lookup throughout all of KDE uses libidn when available.
	    For Kopete's Jabber plugin this is a requirement.</td>

	<td>kdelibs</td>
      </tr>
      <tr valign="top">
        <td><a href="http://josefsson.org/libntlm/">NTLM</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Libntlm provides support for NTLM authentication</td>

        <td>Microsoft webservers on intranets often require NTLM authentication</td>

        <td>kdelibs</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Security</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.openssl.org/">OpenSSL &gt;= 0.9.6</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>OpenSSL is a robust, commercial-grade, full-featured and Open Source toolkit implementing the Secure Sockets Layer (SSL v2/v3) and Transport Layer Security (TLS v1) protocols as well as a full-strength general purpose cryptography library.</td>

        <td>KDE uses OpenSSL for the bulk of secure communications, including secure web browsing via HTTPS.</td>

        <td>kdelibs, kdebase, kdenetwork, kdepim</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gnupg.org/">GnuPG &gt;= 1.2.5</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>GnuPG is a complete and free replacement for PGP (Pretty Good Privacy). PGP uses a popular technique for encrypting and decrypting e-mail as well as for sending an encrypted digital signature that lets the receiver verify the sender's identity and know that the message was not modified en route.</td>

        <td>The KDE mail client KMail can use GnuPG to encrypt and decrypt emails as well as to authenticate the identify of the author of an email. Version 1.2.2 or higher is strongly recommended.</td>

        <td>kdepim</td>
      </tr>

      <tr valign="top">
        <td><a href="http://java.sun.com/products/jsse/">JSSE</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>JSSE (Java Secure Socket Extension) is a set of Java packages that enable secure Internet communications by Java applets.</td>

        <td>Some Java-enabled websites, particularly banking sites, require JSSE for full access to their services. The KDE web browser Konqueror will use JSSE if it is available on the system and properly configured. If you do not do online banking JSSE is optional.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.kernel.org/pub/linux/libs/pam/">PAM</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>PAM (Pluggable Authentication Modules) is a flexible system for authenticating users.</td>

        <td>Various KDE applications can use the PAM modules for user authentication.</td>

        <td>kdebase</td>
      </tr>

      <tr>
        <td align="center" colspan="5">Graphics</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://mesa3d.sourceforge.net/">OpenGL</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>OpenGL is an industry-standard API for developing portable, interactive 2D and 3D graphics applications.</td>

        <td>The 3D hardware acceleration available through the OpenGL API is used in applications ranging from graphics and modellers to screensavers and video players.</td>

        <td>kdelibs, kdebase, kdeaddons, kdegraphics, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.libsdl.org/index.php">SDL</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>SDL (Simple DirectMedia Layer) is a cross-platform multimedia library designed to provide fast access to the graphics framebuffer and audio device.</td>

        <td>Some <a href="http://noatun.kde.org/">Noatun</a> plugins require SDL.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.libtiff.org/">libtiff</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Libtiff provides support for the Tag Image File Format (TIFF), a widely used format for storing image data, also used in facsimile transmissions.</td>

        <td>Libtiff is needed for KDE applications which send, receive or view faxes.</td>

        <td>kdebase, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.libpng.org/pub/png/libpng.html">libpng</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>Libpng is the official PNG (Portable Network Graphics) reference library.</td>

        <td>PNG images are the standard image format for KDE. Libpng is required to view and save such images. Note that most image handling is done by Qt, which will provide its own copy of libpng if one is not installed on the system.</td>

        <td>kdelibs, kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="ftp://ftp.uu.net/graphics/jpeg/">libjpeg</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libjpeg is a library which implements JPEG image compression and decompression. JPEG is a standardized lossy compression method for full-color and gray-scale images, such as photographs, and is commonly used on the Internet.</td>

        <td>KDE applications can view and save JPEG images using libjpeg.</td>

        <td>kdelibs, kdebase, kdegraphics, kdemultimedia, koffice</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.libmng.com/">libmng</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Libmng is the standard implementation of MNG (Multiple-image Network Graphics). MNG, built on PNG, provides animated images, similar to animated GIF images.</td>

        <td>While not nearly as ubiquitous as animated GIFs, MNG is catching on as an animated image technology for the Internet.</td>

        <td>kdelibs, kdebase, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.freetype.org/">freetype &gt;= 2.0.0</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>FreeType 2 is a software font rendering engine.</td>

        <td>Freetype 2 provides enhanced anti-aliased font handling and manipulation for the entire KDE desktop.</td>

        <td>kdelibs, kdebase, kdeadmin, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://poppler.freedesktop.org">Poppler &gt;= 0.3.1</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Poppler is a PDF rendering library.</td>

        <td>Some KDE applications can provide enhanced information about PDF files using Poppler.</td>

        <td>kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="ftp://ftp.gnu.org/pub/gnu/gift">GIFT &gt;= 0.1.9</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The GNU Image Finding Tool allows for searching images by example. E.g. right-click on an image in the filemanager and select "Search for similar Images". See <a href="http://www.fer-de-lance.org/">http://www.fer-de-lance.org/</a> for more information.</td>

        <td>Implemented as a KPart and kioslave, it is typically used with Konqueror, but it can also be used by other applications.</td>

        <td>kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.levien.com/libart/">libart &gt;= 2.3.8</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libart is a library for high-performance, anti-aliased 2D graphics.</td>

        <td>Libart is required for KDE's SVG icon engine.</td>

        <td>kdelibs</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Multimedia</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.alsa-project.org/">ALSA</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>ALSA (Advanced Linux Sound Architecture) provides audio and MIDI functionality for Linux.</td>

        <td>ALSA provides advanced audio support for various KDE multimedia and audio applications.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.fokus.gmd.de/research/cc/glone/employees/joerg.schilling/private/cdrecord.html">cdrtools</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>cdrtools (f/k/a cdrecord) burns CDs using a CDR/CDRW recorder.</td>

        <td>Cdrtools is commonly required for burning CDs.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xiph.org/paranoia/">cdparanoia</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Cdparanoia directly extracts digital audio from compact discs (audio CDs). There's no intermediate analog step. The digital audio data is converted to various standard formats.</td>

        <td>Cdparanoia is used by most KDE CD "rippers", inclding the audiocd:// IO slave.</td>

        <td>kdebase, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://lame.sourceforge.net/">L.A.M.E.</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>L.A.M.E. (LAME Ain't an Mp3 Encoder) is an MP3 encoder.</td>

        <td>L.A.M.E. is needed to save digital music in the MP3 format.</td>

        <td>kdebase, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xiph.org/ogg/index.html">Ogg Vorbis</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Ogg Vorbis is a patent-free, fully open general purpose audio encoding standard that rivals or surpasses the 'upcoming' generation of proprietary coders (AAC and TwinVQ, also known as VQF).</td>

        <td>Ogg Vorbis is needed to listen to, and to save digital music in the excellent Ogg Vorbis format.</td>

        <td>kdebase, arts, kdeaddons, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://flac.sourceforge.net/index.html">FLAC &gt;= 1.0.3</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>FLAC stands for Free Lossless Audio Codec. Grossly oversimplified, FLAC is similar to MP3, but lossless, meaning that audio is compressed in FLAC without any loss in quality.</td>

        <td>FLAC (libFLAC and libOggFLAC) is needed to listen to digital music in the FLAC or Ogg/FLAC formats.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.speex.org/index.html">Speex</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Speex is an Open Source/Free Software patent-free audio compression format designed for speech.</td>

        <td>Speex (libspeex) is needed to listen to digital files and streaming audio in the Speex format.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.underbit.com/products/mad/">MAD</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>MAD is a high-quality MPEG audio decoder. It currently supports MPEG-1 and the MPEG-2 extension to lower sampling frequencies, as well as the de facto MPEG 2.5 format.</td>

        <td>While MAD (libmad) is not required to play MP3s, it is recommended for its superior playback quality.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://freeware.sgi.com/source/audiofile/">libaudiofile</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libaudio is a library for accessing digital audio in the .WAV format.</td>

        <td>Libaudio is needed to play or save .WAV audio files.</td>

        <td>kdelibs, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://xinehq.de/">XINE</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>XINE is a multimedia system for playback of CDs, DVDs and VCDs and for decoding multimedia streams or files such as .AVI, .MOV, .WMV and .MP3.</td>

        <!-- This read that KDE 3.1 was supposed to switch to XINE for basic video support. It has happened, right? -->
        <td>The KDE multimedia system uses XINE for basic video support.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gstreamer.org/">gstreamer 0.8</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>gstreamer is a multimedia framework with support for a variety of file types and sound outputs.</td>

        <td>JuK can use gstreamer. External KDE applications such as amaroK work better with gstreamer.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.musicbrainz.org/products/client/download.html">libmusicbrainz</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>libmusicbrainz is used to interface with the <a href="http://www.musicbrainz.org/">MusicBrainz</a> server to help guess information about your music, such as the album and artist.</td>

        <td>JuK can use libmusicbrainz in conjuction with libtunepimp to guess the tags for music in your library.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.musicbrainz.org/products/tunepimp/download.html">libtunepimp</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>libtunepimp is a library designed to aid in the creation of <a href="http://www.musicbrainz.org/">MusicBrainz</a>-enabled applications.</td>

        <td>JuK can use libtunepimp in conjunction with libmusicbrainz to guess the tags for music in your library.</td>

        <td>kdemultimedia</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Development</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://subversion.tigris.org/">Subversion</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Subversion is a revision control system like CVS.</td>

        <td>KDevelop 3.0 can use Subversion to maintain a revision
	control repository for projects.  There is also a kioslave for
	Subversion.</td>

        <td>kdesdk, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.cvshome.org/">CVS</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The Concurrent Versioning System is a very popular revision control system, typically used in software project development.</td>

        <td>CVS is needed by KDevelop and certain other applications to access CVS repositories.</td>

        <td>kdesdk, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.perforce.com/">Perforce</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Perforce is a revision control system like CVS.</td>

        <td>KDevelop 3.0 can use the <code>p4</code> command line tool to enable Perforce as the backend revision control repository for projects.</td>

        <td>kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://ctags.sourceforge.net/">Exuberant Ctags</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Ctags indexes source code files, allowing text editors and other utilities to quickly find identifiers and other elements of the source.</td>

        <td>KDevelop uses ctags, if available, to provide improved source code navigation.</td>

        <td>kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.doxygen.org/">doxygen</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Doxygen is a cross-platform, <a href="http://java.sun.com/products/jdk/javadoc/">JavaDoc</a>-like documentation system for C, C++ and IDL (Corba, Microsoft&reg; and KDE DCOP flavors).</td>

        <td>Doxygen is used to generate the kdelibs API documentation. If you want to develop software using KDE you should install doxygen.</td>

        <td>kdebindings, kdesdk, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.recherche.enac.fr/opti/facile/">libfacile</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>A Functional Constraint Library</td>

        <td>libfacile is used to implement a chemical equation solver in kalzium</td>

        <td>kdeedu</td>
      </tr>

      <tr valign="top">
        <td><a href="http://caml.inria.fr/ocaml/">ocaml</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Objective Caml is the most popular variant of the Caml language.</td>

        <td>ocaml is used to implement a chemical equation solver in kalzium</td>

        <td>kdeedu</td>
      </tr>

    </table>

<?php
  include "footer.inc"
?>
