---
title: "International Sites"
description: "Support channels for KDE are also available in other languages."
---

<style>
.app-category,.international-site {
   float: left;
   display: table-cell;
   max-width: 240px;
   width: 240px;
   height: 60px;
   max-height: 60px!important;
   text-align: left;
}

.app-category img,.international-site img {
   float: left;
   margin: 0 10px 0 0!important;
}

.app-category a,.international-site a {
   font-size: 12pt;
   font-weight: 700;
   text-decoration: none;
}

.international-site {
   height: auto!important;
}
</style>

<p>This page provides access to a number of KDE web pages in different
languages, countries or regional areas. The idea is to collect useful
information concerning the usage of KDE for specific communities, status of
translations, suggested settings for such users, and similar issues.
</p>
<p>
These pages also are not necessarily involved in the KDE translation
or localization efforts.  Please see the
<b><a href="https://l10n.kde.org">l10n.kde.org</a></b> website for
more information on this topic.
</p>
<p>
If you would like to contribute pages tailored to your language, country or regional
area please contact the KDE.org <a href="&#x6d;ailt&#111;:w&#x65;b&#x6d;&#00097;&#115;&#x74;&#101;&#00114;&#x40;kde.org">webmaster</a>.
</p>

<p class="international-site"><img src="/images/flags/br.png" width="22" height="14" alt="[Brasil Flag]" /> <a href="https://br.kde.org/">Brasil</a></p>
<p class="international-site"><img src="/images/flags/cn.png" width="22" height="14" alt="[China Flag]" /> <a href="https://kde-china.org">China</a></p>
<p class="international-site"><img src="/images/flags/fr.png" width="22" height="14" alt="[France Flag]" /> <a href="https://fr.kde.org/">France</a></p>
<!-- <p class="international-site"><img src="/images/flags/ie.png" width="22" height="14" alt="[Ireland Flag]" /> <a href="https://www.kde.ie/">Ireland</a></p> -->
<p class="international-site"><img src="/images/flags/il.png" width="22" height="14" alt="[Israel Flag]" /> <a href="https://il.kde.org/">Israel</a></p>
<p class="international-site"><img src="/images/flags/it.png" width="22" height="14" alt="[Italy Flag]" /> <a href="https://kdeitalia.it">Italy</a></p>
<p class="international-site"><img src="/images/flags/jp.png" width="22" height="14" alt="[Japan Flag]" /> <a href="https://jp.kde.org/">Japan</a></p>
<p class="international-site"><img src="/images/flags/nl.png" width="22" height="14" alt="[Netherlands Flag]" /> <a href="https://kde.nl/">Netherlands</a></p>
<p class="international-site"><img src="/images/flags/ro.png" width="22" height="14" alt="[Romania Flag]" /> <a href="https://ro.kde.org">Romania</a></p>
<p class="international-site"><img src="/images/flags/ru.png" width="22" height="14" alt="[Russia Flag]" /> <a href="https://kde.ru">Russia</a></p>
<p class="international-site"><img src="/images/flags/es.png" width="22" height="14" alt="[Spain Flag]" /> <a href="https://www.kde-espana.org">Spain</a></p>

<div style="clear:left;"></div>

<p style="font-size: smaller;">
<b>Note:</b><br />

Our policy regarding the sites listed above is simple: If somebody goes through the
trouble of creating a KDE website, then we will include it in this
list. Furthermore, we will <em>not</em> override the site creator's
preference for what to call their geographical area (e.g., calling the
Taiwan site "Taiwan" instead of "China").

</p>
