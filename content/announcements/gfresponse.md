---
title: KDE Official Response to GNOME Foundation
date: "2000-08-31"
description: The recent announcements regarding the formation of a GNOME Foundation coupled with the Sun/Hewlett Packard decision to use GNOME as their standard desktop has resulted in a deluge of requests to the KDE Core Team asking what our "position" is. Well, this is it. We offer this position paper in the hope that we can put this behind us and get back to coding
---

The recent announcements regarding the formation of a GNOME Foundation
coupled with the Sun/Hewlett Packard decision to use GNOME as their
standard desktop has resulted in a deluge of requests to the KDE Core
Team asking what our "position" is. Well, this is it. We offer this
position paper in the hope that we can put this behind us and get back
to coding.

We should caution all readers that the responses are all in more
detail than we really think is necessary. We would like to answer the
question like this:

Q: "How much does creation of GNOME Foundation affect KDE development?" <br>
A: "As much as the birth of the last baby polar bear at the Quebec City
Zoo" (i.e., not at all)

But these days, that doesn't seem enough. So read on.

## Why None of it Matters

We would like to start, though, by saying that none of this really has
anything at all to do with KDE. The KDE Project has always been made
up of and controlled by developers working to make KDE the best
desktop and application development platform in existence. We program
for the sheer joy and fun that comes out of putting out such an
awesome product. This has nothing to do with market share, mind
share, corporate attention, or any other outside influence. No matter
what happens, we will always work on KDE and make sure that it is the
best system we can possibly make it.

The fact is that KDE is currently very popular for both developers and
users. Nearly all major BSD and Linux distributions ship KDE as their
default desktop and all but one (hopefully soon to change) include
KDE. More to the point, not only do distributions like us, but most
strongly back KDE with paid developers, hardware, and bandwidth. A
large survey done by Borland/Inprise showed that nearly half of all
developers prefer to program with KDE and over half use it. A newer
survey done by Evans Data Corp has KDE usage at over 70%. We win
nearly all "users choice" awards whenever given out. The most recent
was the "Show Favorites" award at the recent LinuxWorld Expo.
"Third-party" developers are churning out hundreds of KDE applications
(apps.kde.com lists nearly 600 of them)

All this popularity is due to the simple fact that KDE is very well
done from both a developer and a user point of view. We have put all
of our love of programming into this software and it shows.

We are especially excited because of the upcoming release of KDE 2.0.
All of the popularity, all the awards, all of the usage of KDE has all
been for the KDE 1.x series... and as anybody who has used or
developed for the beta versions of KDE 2.0 can attest, KDE 2.0 will be
better than all previous and existing desktops. If you liked what
we've done in the past (and the stats above show that you do), you'll
really like what we're doing now.

But we still get asked about the GNOME Foundation (and related topics)
so we'll answer them anyway.

## The Formation and Impact of the GNOME Foundation

The actual formation of the GNOME Foundation doesn't affect us at all.
They are simply changing the way that their own project is governed
and controlled. That is, the Foundation defines when GNOME will be
released, what it will look like, what constitutes a GNOME standard,
etc. It also will take care of GNOME public relations as well as
accepting donations (as a non-profit organization). We have a similar
mechanism for accepting donations (our KDE e.V. non-profit) and we
already have a system for releases and standards. So again, what the
GNOME Project does here doesn't affect us at all.

Now we have been asked "Will KDE ever create a KDE Foundation in the
same sense as the GNOME Foundation?" The answer to this is no,
absolutely not. KDE has always been and always will be controlled by
the developers that work on it and are willing to do the code. We
will resist any and all attempts to change this.

Looking at the issue from another standpoint, the KDE Project is
possibly the only large Open Source project that has neither a
"benevolent dictator" nor an elected governing board (or any voting at
all). In a sense, we are the only large "pure" Bazaar-style project
out there... and it works. We have no intention of changing an
obviously winning formula.

Back to the GNOME Foundation. While the charter for the Foundation
only specifies controlling GNOME standards, we realize that there is a
temptation to think that they might also try to control and define the
mythical "Linux Desktop Standard". The lack of an "Official Standard"
is a hot-topic and when journalists see large corporations backing an
existing project, claims of GNOME becoming the standard are never far
behind.

This reasoning is flawed and here's why (in no particular order):

<ol>
<li>As mentioned earlier, KDE has a very deep-rooted popularity for
   solid reasons.  Any attempt to proclaim "we are THE standard"
   without our involvement is just silly.

<li>BSD/Linux users tend to resent "standards" being force-fed on them
   by either corporations or the media.  We'll use what we want, thank
   you.

<li>True desktop standards can only be hacked out between the actual
   developers of the two projects working on a specific issue.  Things
   like drag-n-drop, cut-n-paste, session management, window manager
   hints, and dotdesktop file format are all examples of this
   happening.
</ol>

We are also hearing lately that maybe developers will gravitate
towards GNOME instead of KDE now that they have large corporate
backers. Note that we have yet to hear this from any <em>developers</em>
themselves. This is also unlikely for several reasons (again, in no
particular order):

<ol>
<li>GNOME and KDE have very markedly different design philosophies and
   methods.  Since nearly all developers in both projects are doing it
   for fun, they naturally go to whichever one fits their style of
   programming.

<li>With KDE, it is <em>very</em> easy to program large and complex
   applications with a minimum of code.  Again, since most developers
   are in this for fun, they want to maximize their free-time during
   coding.  KDE makes this possible.

<li>Open Source developers very rarely work on projects because <em>of</em>
   corporate backing and sometimes will <em>not</em> work on them due to it.
   The size of the wallets of a project's corporate backers mean
   nothing to somebody who is not getting paid and is working for the
   pure joy of programming.
</ol>

Of course, companies that use GNOME or KDE to develop their
applications aren't in it for the fun. In that case, we simply remind
them that KDE is currently very very popular and has an extremely
powerful infrastructure to develop on. If you need to put out a fully
working product as fast as possible, then KDE is the right platform to
work on -- it's as simple as that.

## The Impact of the Sun/HP Announcement

The Sun/HP announcement that they will be adopting GNOME as their
desktop standard and replacing CDE has caused quite a bit of buzz on
it's own and is often linked with the GNOME Foundation announcement.
In many ways, our responses or reaction to their announcement is
identical to our reaction to the GNOME announcement. There are some
differences, though.

First, the decision to use GNOME on Sun and HP desktops doesn't change
much in itself. Sun may be well known for its servers, but it doesn't
seem to be doing that great as a desktop. There are likely many many
more BSD and GNU/Linux computers running KDE than there are new Solaris
workstations running CDE. We don't anticipate that this will change
very much in the future.

The second issue is the fact that Sun will be assigning developers
(the figure "50 developers" seems to be in vogue) to work on GNOME.
We honestly don't know how this will affect GNOME as we have no
fortune tellers working with KDE. While it has never been shown that
the simple task of adding more developers to a project will make it
better AND Sun hasn't impressed us much with all the developers and
money they've thrown at CDE, we realize that the past is no guarantee
for the future. Perhaps the addition of extra paid developers really
<em>will</em> rejuvenate the GNOME Project and they will catch up to us
in both a technical and usability sense.

In any case, it still won't affect the KDE Project. We will still put
out an excellent product that is among the best in the world.

## Summary

In summary, the KDE Project has always existed and thrived regardless
of competition by either other Open Source projects or profit-minded
companies. We have done so by putting out a quality product that we
love to work on and people love to use. This isn't going to change.

To our users, we promise to always ensure that KDE is as intuitive and
easy to use as possible and that we will always be there for you to
use. To Open Source developers, we promise that KDE will always have
the most powerful development infrastructure out there while still
being the easiest to program for. To companies, we can assure you
that your KDE-based product will have a huge market and a reduced
"idea-to-release" time. And to the media, as much as you may want to
pronounce either KDE or GNOME the "Winner of the Desktop Wars", we're
just going to have to disappoint you. KDE (and GNOME) are in for the
long haul and we're not about to go away.
