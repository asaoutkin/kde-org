---
title: KDE Announces Support Of KDE Technology
date: 1998-07-16
---


&nbsp;<I>Caldera hosts official U.S. FTP site for KDE</I>&nbsp;

&nbsp;OREM, UT - July 16, 1998 - Caldera&reg;, Inc. today announced
the adoption and support of KDE technology. Caldera will include the K
Desktop Environment in the OpenLinux 1.2.2 maintenance release due out
the end of September. KDE will be the default desktop in the Caldera OpenLinux
2.0 product, scheduled for release the fourth quarter of this year.&nbsp;

&nbsp;"Caldera is proud to be the first Linux vendor to fully adopt
KDE technology. Based on the open-source model, KDE offers the customers
of our commercial Linux products an internationally developed, leading-edge
desktop environment," said Ransom Love, General Manager of the OpenLinux
Division at Caldera. "With this partnership, Caldera continues its commitment
to our customers success by providing easy-to-use management and productivity
tools."&nbsp;

&nbsp;Caldera is supporting KDE technology by hosting the official KDE
U.S. FTP site at <a href="ftp://ftp.us.kde.org/pub/kde/">ftp.us.kde.org</A>.
Provided by Caldera, KDE 1.0 binary and source rpms for OpenLinux 1.2 are
available for download from the site.&nbsp;

&nbsp;"Caldera's adoption of the KDE technology helps to get not only
the KDE desktop into corporate settings, but also means a big leap forward
for the Linux operating system," said Kalle Dalheimer, member of the KDE
core team. "KDE provides users with the most feature-rich, easy-to-use
desktop available for Unix systems."&nbsp;

&nbsp;The completely new Internet enabled desktop, incorporating a large
suite of applications for Unix workstations and the upcoming Compound Document
Framework based K Office Suite, currently supports more than 25 languages.
The strength of this exceptional environment lies in the interoperability
of its components and the functionality of its applications.&nbsp;

&nbsp;<b>Caldera Information</b>&nbsp;
<br>Caldera, Inc. develops and markets a line of operating system and networking
technologies including OpenLinux and DR-DOS. For more information about
Caldera products and technologies, please <b>visit our web site at </b><a href="http://www.caldera.com/">www.caldera.com</A>,
or call 1-888-GO-LINUX or 1-801-765-4888.&nbsp;

&nbsp;<b>KDE Information</b>&nbsp;
<br>The K Desktop Environment is an international Internet based volunteer
project which develops the freely available graphical desktop environment
for the UNIX&reg; platform. For more information about KDE and its technology,
please visit the web site at <a href="/">www.kde.org</A>
or contact <a href="m&#97;il&#116;o:&#x70;re&#115;&#0115;&#064;kde.&#x6f;&#114;g">press@kde.org.</A>&nbsp;

&nbsp;<b>Media Contacts Only:</b>&nbsp;
<br>Laura Kenner&nbsp;
<br>Caldera, Inc.&nbsp;
<br>(801) 765-4999 x238&nbsp;
<br><a href="mailto:laurak@caldera.com">laurak@caldera.com</A>&nbsp;

&nbsp;Kalle Dalheimer and Martin Konold&nbsp;
<br>K Desktop Environment e.V.&nbsp;
<br>+49-7071-29-78653&nbsp;




