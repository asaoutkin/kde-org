---
aliases:
- ../announce-3.1alpha1
date: '2002-07-11'
description: A first, experimental snapshot of the CVS tree of the upcoming KDE 3.1
  release. Many new features, and probably many new bugs as well. Have fun ;-)
title: KDE Project Ships Development Release
---

FOR IMMEDIATE RELEASE

<h3 align="center">
   KDE Project Adds Popular New Features, Ships Development Release of
   Leading Open Source Desktop
</h3>

The KDE Project Ships a New Feature Release for the Third-Generation of the Leading Desktop for Linux/UNIX, Offering Users a Glimpse of Progress and an Opportunity to Provide Timely Feedback

The <a href="http://www.kde.org/">KDE
Project</a> today announced the immediate availability of KDE 3.1 alpha1,
the first development release of a significant feature upgrade for KDE 3
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/">screenshots</a>).
KDE 3 is the third generation of KDE's free, powerful and easy-to-use
Internet-enabled desktop for Linux and other UNIXes.
KDE 3.1, scheduled for final release in October 2002, will provide
<a href="http://developer.kde.org/development-versions/kde-3.1-features.html">substantial
improvements</a> to the KDE desktop experience.
As the KDE 3 API is frozen for binary compatibility, KDE 3.1 will be
binary compatible with KDE 3.0.

KDE 3.1 alpha1 ships with the core KDE libraries, the base desktop
environment, and hundreds of applications and other desktop enhancements
from the other KDE base packages (PIM, administration, network,
edutainment, development, utilities, multimedia, games, artwork, and
others).

The primary goal of the 3.1alpha1 release is to engage experimental KDE
users who would like to comment on the current progress or who simply
prefer to enjoy some of the many great new KDE 3.1 features now.
Instructions for maintaining a KDE 3.1 alpha installation side-by-side
with a stable KDE release are
<a href="http://developer.kde.org/build/build2ver.html">available here</a>.

KDE, including all its libraries and its applications, is
available <em>for free</em> under Open Source licenses.
KDE can be obtained in source and, for stable releases, in numerous binary
formats from the KDE
<a href="http://download.kde.org/unstable/kde-3.1-alpha1/">http</a> or
<a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors,
and can also be obtained on
<a href="http://www.kde.org/cdrom.html">CD-ROM</a> or with any
of the major Linux/UNIX systems shipping today.

For more information about the KDE 3 series, please view the KDE 3.1 alpha1
<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/">screenshots</a>
(!), or read the
<a href="http://www.kde.org/announcements/announce-3.0.php">KDE 3.0
announcement</a>, the list of KDE 3.1
<a href="http://developer.kde.org/development-versions/kde-3.1-features.html">planned
features</a> and the KDE 3.1
<a href="http://developer.kde.org/development-versions/kde-3.1-release-plan.html">release
plan</a>.

<a id="changes"></a>

#### New Features / Enhancements

KDE 3.1 promises a number of welcome improvements and additions
(<sup>\*</sup> indicates a work in progress):

<u>Web browser</u>.
By popular request, the web browser
(<a href="http://konqueror.kde.org/"><em>Konqueror</em></a>) offers
tabbed browsing!
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s1.png">screenshot</a>).<sup>_</sup>
SSL certificate policies can now be defined on a domain name
basis.<sup>_</sup>

<u>Email client</u>.
The email client (<a href="http://kmail.kde.org/"><em>KMail</em></a>)
has merged in several privacy and
security enhancements - namely S/MIME, PGP/MIME and X.509v3 support -
in collaboration with the
<a href="http://www.gnupg.org/aegypten/">Aegypten project</a>,
an IT security project
<a href="http://newsforge.com/article.pl?sid=01/10/05/1611215&amp;mode=thread">sponsored</a>
by the German government
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s19.png">screenshot</a>).<sup>\*</sup>

<u>File browser</u>.
The file browser (<em>Konqueror</em>) has gained a number of
new features:

<div align="justify">
  <ul>
    <li>
      folder icons which reflect a folder's contents;
    </li>
    <li>
      a new video thumbnail generator, in the multimedia
      package (<em>kdemultimedia</em>);<sup>*</sup>
    </li>
    <li>
      an optional 256x256 image preview in an image tooltip;
    </li>
    <li>
      use of a pre-generated thumbnail by the image thumbnail generator
      if available within an image;
    </li>
    <li>
      a number of <em>KFile</em> plugins now provided enhanced
      information about various file types (such as .diff, .cpp,
      .rpm, .deb, .html and .tiff files)
      (<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s2.png">screenshot</a>);
    </li>
    <li>
      a detailed list view with meta-information about
      files in the side-bar;<sup>*</sup> and
    </li>
    <li>
      integration of file meta-information and multimedia files 
      searches in file search utility<sup>*</sup>
    </li>
  </ul>
</div>

<u>Desktop sharing</u>.
A new <a href="http://www.uk.research.att.com/vnc/">VNC</a>-compatible
desktop sharing framework allows KDE users to share a KDE desktop across
multiple machines
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s6.png">screenshot</a>).<sup>\*</sup>
Both a desktop-sharing server (<em>KRfb</em>) and a client
(<em>KRdc</em>)
have joined the network package (<em>kdenetwork</em>).

<u>New games</u>.
A number of new games have been added to the games package
(<em>kdegames</em>), including
a golf game (<a href="http://www.katzbrown.com/kolf/"><em>Kolf</em></a>)
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s4.png">screenshot</a>),
an Atlantik and Monopoly-type game
(<a href="http://www.unixcode.org/atlantik/"><em>Atlantik</em></a>),
a Blackjack game
(<a href="http://www.freekde.org/neil/megami/"><em>Megami</em></a>).
and
a Same-like game
(<a href="http://klickety.sourceforge.net/"><em>Klickery</em></a>).

<u>Download manager</u>.
A new download manager (<em>KGET</em>), which integrates fully
into Konqueror, has joined the network package (<em>kdenetwork</em>).
It manages any number of downloads in one window, where transfers
can be added, removed, paused, resumed, queued or scheduled.
A dialog displays transfer status, including progress, size, speed and
estimated time to completion.

<u>Multimedia</u>.
The multimedia framework (<em>kdemultimedia</em>) includes a
new framework for video.<sup>_</sup>
In addition, a new multimedia player
(<a href="http://noatun.kde.org/"><em>Noatun</em></a>) plugin
for searching lyrics (<em>Lyrics Plugin</em>) has joined the
addons package (<em>kdeaddons</em>).<sup>_</sup>

<u>Address book</u>.
The address book (<em>KAddressbook</em>) has gained the ability to
fetch contact information from one or more LDAP servers.
It can also print contact information and import
industry-standard vCards.<sup>\*</sup>

<u>Desktop panel</u>.
The panel (<em>Kicker</em>) supports fully customized menus, such
as may be desirable in a kiosk or office environment.<sup>\*</sup>
Moreover, the panel can now be set to hide underneath other windows
and raise itself when the mouse pointer covers the panel's area.
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s3.png">screenshot</a>).

<u>Theme manager</u>.
The theme manager provides improved theme style and color decoration
previews
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s13.png">screenshot</a>).

<u>Terminal emulator</u>.
The terminal emulator
(<a href="http://konsole.kde.org/"><em>Konsole</em></a>)
can now bookmark commands which
start a new shell, such as <code>ssh://user@host.org</code>. It
also displays an information dialog now when the user presses
<code>Ctrl-s</code>, as some users do not realize this action
can activate scroll locking. The emulator's copy-to-clipboard behavior
has been modified to conform to the KDE standard
(now one must select the <em>Copy</em> entry in the <em>Edit</em> menu
to copy to the secondary "Windows" clipboard, while highlighting
continues to copy to the primary "X11" clipboard)
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s5.png">screenshot</a>).

<u>Advanced editor</u>.
The advanced text / programmer's editor
(<a href="http://kate.kde.org/"><em>Kate</em></a>) provides
improved code folding and translucent menus
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/s12.png">screenshot</a>).

<u>Application finder</u>.
The application finder (<em>KAppfinder</em>) provides a nice tree view
for selecting the applications to include in the KDE desktop menu
hierarchy.

<u>Japanese reference</u>.
(<a href="http://www.katzbrown.com/kiten/"><em>Kiten</em></a>)
has joined the edutainment package (<em>kdeedu</em>).<sup>\*</sup>

<u>User notifications</u>.
The desktop now has two new user notification methods to provide
non-obtrusive informational messages: a passive popup widget
(<em>KPassivePopup</em>), which pops up next to the application's taskbar
window entry, without stealing the focus, as well as a window
information class (<em>KWindowInfo</em>), for displaying messages
in a window's title [ and icon ]. Moreover, a new notification dialog
(<em>KNotifyDialog</em>) provides an easy method for users to
select for which events an application should provide
notifications.<sup>\*</sup>

<u>KDE usability</u>.
The <a href="http://usability.kde.org/">KDE Usability Project</a> has
made some usability enhancements to
the panel (<em>Kicker</em>), the desktop calculator (<em>KCalc</em>)
and the screenshot utility (<em>KSnapshot</em>).

<u>Eye candy</u>.
On the glitzy side, KDE 3.1 will ship with a new default theme style,
Keramik. And, for something to look forward to,
the post-KDE 3.1 alpha1 CVS sports attractive
drop shadows for menus
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/2329-1.png">screenshot</a>).

<a id="binary"></a>

#### Installing Binary Packages

<u>Binary Packages</u>.
Due to the experimental nature of this release, binary packages are
not being made available via the KDE mirrors. Some distributions
may provide binary packages on their website, however, so please check
there if you are looking for binary packages.

#### Compiling Source Code

<a id="source_code-library_requirements"></a><u>Library
Requirements / Options</u>.
KDE 3.1 alpha1 requires
<a href="http://www.trolltech.com/developer/download/qt-x11.html">Qt
3.0.3 for X11</a>.
In addition, KDE can take advantage of a number of other
<a href="../3.0/#source_code-library_requirements">optional
libraries and applications</a> to improve your desktop experience and
capabilities.

<u>Compiler Requirements</u>.
KDE is designed to be cross-platform and hence to compile with a large
variety of Linux/UNIX compilers. However, KDE is advancing very rapidly
and the ability of native compilers on various UNIX systems to compile
KDE depends on users of those systems
<a href="http://bugs.kde.org/">reporting</a> compilation and other
build problems to the responsible maintainers.

With respect to the most popular KDE compiler,
<a href="http://gcc.gnu.org/">gcc/egcs</a>, please note that some
components of KDE will not compile properly with gcc versions earlier
than gcc-2.95, such as egcs-1.1.2 or gcc-2.7.2, or with unpatched
versions of <a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a> 3.0.x.
However, KDE should compile properly with gcc 3.1.

<a id="source_code"></a><u>Source Code</u>.
The complete source code for KDE 3.1 alpha1 is available for free
<a href="http://download.kde.org/unstable/kde-3.1-alpha1/src/">download</a>.
These source packages have been digitally signed with
<a href="http://www.gnupg.org/">GnuPG</a> using the KDE Archives PGP Key
(available from the
<a href="http://www.kde.org/signature.html">KDE Signature page</a>
or public key servers), and their respective MD5 sums are listed on the
<a href="http://www.kde.org/info/3.1.html">KDE 3.1 Info Page</a>.

<u>Further Information</u>. For further
instructions on compiling and installing KDE 3.1 alpha1, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</a> and, if you should encounter compilation difficulties, the
<a href="http://developer.kde.org/build/compilationfaq.html">KDE Compilation FAQ</a>.

#### KDE Sponsorship

Besides the superb and invaluable efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>. In addition,
the members of the <a href="http://www.kdeleague.org/">KDE
League</a> provide significant support for KDE promotion, and the
<a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
provides most of the Internet bandwidth for the KDE project.
Thanks!

#### About KDE

KDE is an independent project of hundreds of developers, translators,
artists and other professionals worldwide collaborating over the Internet
to create and freely distribute a sophisticated, customizable and stable
desktop and office environment employing a flexible, component-based,
network-transparent architecture and offering an outstanding development
platform. KDE provides a stable, mature desktop, a full, component-based
office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools and utilities, and an
efficient, intuitive development environment featuring the excellent IDE
<a href="http://www.kdevelop.org/">KDevelop</a>. KDE is working proof
that the Open Source "Bazaar-style" software development model can yield
first-rate technologies on par with and superior to even the most complex
commercial software.

<hr/>

  <font size="2">
  <em>Trademarks Notices.</em>
  KDE, K Desktop Environment, KDevelop and KOffice are trademarks of KDE e.V.

Linux is a registered trademark of Linus Torvalds.

Monopoly is a trademark of Hasbro, Inc.

Trolltech and Qt are trademarks of Trolltech AS.

UNIX is a registered trademark of The Open Group.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

<hr />

<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
Andreas Pour<br />
KDE League, Inc.<br />

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 917 312 3122

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(33) 4 3250 1445

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
      (49) 2421 502758
  </td>
</tr>
<tr Valign="top">
  <td >
    Europe (English):
  </td>
  <td>
   Jono Bacon <br>
    
  [jono@kde.org](mailto:jono@kde.org) <br>
  </td>
</tr>
<tr Valign="top">
  <td >
      Southeast Asia (English and Indonesian):
  </td>
  <td>
   Ariya Hidayat <br>
    
  [ariya@kde.org](mailto:ariya@kde.org) <br>
  (62) 815 8703177

  </td>
</tr>
</table>
