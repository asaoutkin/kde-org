---
aliases:
- ../announce-3.5.8
date: '2007-10-16'
title: An&uacute;ncio de Lan&ccedil;amento do KDE 3.5.8
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   O Projecto KDE Lan&ccedil;a a Oitava Vers&atilde;o de Tradu&ccedil;&otilde;es e Servi&ccedil;os para o Ambiente
   de Trabalho Livre
</h3>

<p align="justify">
  O KDE 3.5.8 oferece tradu&ccedil;&otilde;es em 65 l&iacute;nguas, melhorias no pacote KDE PIM
  e em outras aplica&ccedil;&otilde;es.
</p>

<p align="justify">
  O <a href="http://www.kde.org/">Projecto
  KDE</a> anunciou hoje a disponibilidade imediata do KDE 3.5.8,
  uma vers&atilde;o de manuten&ccedil;&atilde;o para a &uacute;ltima
  gera&ccedil;&atilde;o do ambiente de trabalho poderoso, avan&ccedil;ado e
  <em>livre</em> para o GNU/Linux e outros UNIXes que &eacute;  o KDE. Este
  suporta agora <a href="http://l10n.kde.org/stats/gui/stable/">65 
  l&iacute;nguas</a>, tornando-o dispon&iacute;vel para mais pessoas que a
  maioria do <em>software</em> n&atilde;o-livre e poder&aacute; ser extendido
  facilmente para suportar outras, atrav&eacute;s das comunidades que desejem
  contribuir para o projecto de c&oacute;digo aberto.
  </p>
  <p align="justify">
  Embora o foco principal dos programadores resida na finaliza&ccedil;&atilde;o do KDE 4.0,
  a s&eacute;rie est&aacute;vel 3.5 continua a ser o ambiente de escolha para o tempo actual.
  Est&aacute; provado como est&aacute;vel e bem suportado. A vers&atilde;o 3.5.8, com cerca de
  centenas de correc&ccedil;&otilde;es de erros, melhorou mais uma vez a experi&ecirc;ncia dos
  utilizadores. O foco principal das melhorias para o KDE 3.5.8 &eacute;
  <ul>
        <li>
        Melhoramentos no Konqueror e no seu componente de navega&ccedil;&atilde;o Web KHTML.
        Foram corrigidos erros no tratamento das liga&ccedil;&otilde;es de HTTP. O KHTML viu
	ser melhorado o suporte para mais algumas funcionalidades do CSS, para
	ter uma melhor compatibilidade com as normas.
        </li>
        <li>
        No m&oacute;dulo <em>kdegraphics</em>, ocorreram diversas correc&ccedil;&otilde;es no
	visualizador de PDF's do KDE e no Kolourpaint, um programa de pintura,
	nesta vers&atilde;o.
        </li>
        <li>
        O pacote KDE PIM tem, como de costume, aplicadas diversas correc&ccedil;&otilde;es
	de estabilidade, cobrindo o cliente de e-mail do KDE KMail, a
	aplica&ccedil;&atilde;o de organiza&ccedil;&atilde;o KOrganizer e outros pontos diversos.
	</li>
      </ul>	
</p>
<p align="justify">
  Para uma lista mais detalhada das melhorias desde a 
  <a href="http://www.kde.org/announcements/announce-3.5.7">vers&atilde;o 3.5.7 do KDE</a>,
  a 25 de Janeiro de 2007, consulte por favor o
  <a href="http://www.kde.org/announcements/changelogs/changelog3.5.7to3.5.8">Registo de Altera&ccedil;&otilde;es do KDE 3.5.8</a>.
</p>

<p align="justify">
  O KDE 3.5.8 vem com um ambiente de trabalho b&aacute;sico e &eacute; acompanhado de quinze 
  pacotes (PIM, administra&ccedil;&atilde;o, redes, educa&ccedil;&atilde;o/entretenimento, utilit&aacute;rios,
	   multim&eacute;dia, jogos, itens art&iacute;sticos, programa&ccedil;&atilde;o Web, entre outros). As
  ferramentas e aplica&ccedil;&otilde;es premiadas do KDE est&atilde;o dispon&iacute;veis em 
 <strong>65 l&iacute;nguas</strong>.
</p>

<h4>
  Distribui&ccedil;&otilde;es que Oferecem o KDE
</h4>
<p align="justify">
  A maior parte das distribui&ccedil;&otilde;es de Linux e UNIX n&atilde;o incorporam imediatamente
  as novas vers&otilde;es do KDE, mas ir&atilde;o integrar os pacotes do KDE 3.5.8 nas suas
  pr&oacute;ximas vers&otilde;es. Veja
  <a href="http://www.kde.org/download/distributions">esta lista</a> para
  ver as distribui&ccedil;&otilde;es que oferecinclude("../contact/press_contacts.inc");em o KDE.
</p>

<p align="justify">
  <em>Criadores de Pacotes</em>.
   Alguns vendedores de sistemas operativos fornecem gentilmente pacotes
   bin&aacute;rios do KDE 3.5.8 para algumas vers&otilde;es das suas distribui&ccedil;&otilde;es e, 
   noutros casos, existem comunidades volunt&aacute;rias que tamb&eacute;m o fazem.
   Alguns desses pacotes bin&aacute;rios est&atilde;o dispon&iacute;veis gratuitamente para obter
   do servidor de transfer&ecirc;ncias do KDE em 
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.8/">http://download.kde.org</a>.
  Os pacotes bin&aacute;rios adicionais, assim como as actualiza&ccedil;&otilde;es aos pacotes
  dispon&iacute;veis neste momento, poder&atilde;o vir a ficar dispon&iacute;veis nas pr&oacute;ximas
  semanas.
</p>

<p align="justify">
  <a id="package_locations"><em>Localiza&ccedil;&otilde;es dos Pacotes</em></a>.
  Para uma lista actualizada dos pacotes bin&aacute;rios dispon&iacute;veis, cuja notifica&ccedil;&atilde;o foi prestada ao Projecto KDE, consulte por favor a 
  <a href="/info/3.5.8">P&aacute;gina de Informa&ccedil;&otilde;es do KDE 3.5.8</a>.
</p>

<h4>
  Compilar o KDE 3.5.8
</h4>
<p align="justify">
  <a id="source_code"></a><em>C&oacute;digo-Fonte</em>.
  O c&oacute;digo-fonte completo do KDE 3.5.8 poder&aacute; ser
  <a href="http://download.kde.org/stable/3.5.8/src/">obtido
  de forma livre</a>.  As instru&ccedil;&otilde;es de compila&ccedil;&atilde;o e instala&ccedil;&atilde;o do KDE 3.5.8
  est&atilde;o dispon&iacute;veis na <a href="/info/3.5.8">P&aacute;gina de Informa&ccedil;&otilde;es
  do KDE 3.5.8</a>.
</p>

<h4>
  Suportar o KDE
</h4>
<p align="justify">
O KDE &eacute; um projecto de 
<a href="http://www.gnu.org/philosophy/free-sw.html">'Software' Livre</a> que
existe e cresce apenas devido &agrave; ajuda dos muitos volunt&aacute;rios que doam o seu
tempo e esfor&ccedil;o. O KDE est&aacute; sempre &agrave; procura de novos volunt&aacute;rios e 
contribui&ccedil;&otilde;es, quer seja na programa&ccedil;&atilde;o, correc&ccedil;&atilde;o ou relato de erros,
quer na escrita de documenta&ccedil;&atilde;o, tradu&ccedil;&otilde;es, promo&ccedil;&atilde;o, dinheiro, etc. Todas
as contribui&ccedil;&otilde;es s&atilde;o gratamente recebidas e aceites com todo o gosto. Leia
por favor a  <a href="/community/donations/">p&aacute;gina de Suporte do KDE</a> para mais
informa&ccedil;&otilde;es. </p>

<p align="justify">
Ficamos &agrave; espera de not&iacute;cias suas em breve!
</p>

<h4>
  Acerca do KDE
</h4>
<p align="justify">
  O KDE &eacute; um projecto <a href="/awards/">premiado</a>, independente e com 
  <a href="/people/">centenas</a> de programadores, tradutores, artistas
  e outros profissionais em todo o mundo que colaboram atrav&eacute;s da Internet
  para criar e distribuir de forma livre um ambiente sofisticado, personalizado
  e est&aacute;vel para o seu trabalho, empregando uma arquitectura flex&iacute;vel,
  baseada em componentes e transparente na rede, oferecendo tamb&eacute;m uma
  plataforma de desenvolvimento espantosa.</p>

<p align="justify">
  O KDE oferece um ambiente de trabalho est&aacute;vel, maduro e que inclui um
  navegador avan&ccedil;ado
  (o <a href="http://konqueror.kde.org/">Konqueror</a>), um pacote de
  gest&atilde;o de informa&ccedil;&otilde;es pessoais (o <a href="http://kontact.org/">Kontact</a>),
  um pacote de escrit&oacute;rio completo (o 
  <a href="http://www.koffice.org/">KOffice</a>), um grande conjunto de
  aplica&ccedil;&otilde;es e utilit&aacute;rios de rede e um ambiente de desenvolvimento intuitivo
  e eficiente, o excelente IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  O KDE &eacute; uma prova que o modelo de desenvolvimento "estilo Bazaar"
  poder&aacute; garantir tecnologias de primeira linha a par ou superiores
  &agrave;s aplica&ccedil;&otilde;es comerciais mais complexas.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Avisos de Marcas Registadas.</em>
  O KDE<sup>&#174;</sup> e o log&oacute;tipo do Ambiente de Trabalho 
  K<sup>&#174;</sup> s&atilde;o marcas registadas do KDE e.V.

O Linux &eacute; uma marca registada de Linus Torvalds.

O UNIX &eacute; uma marca registada do The Open Group nos Estados Unidos e noutros
pa&iacute;ses.

Todas as outras marcas registadas e direitos de c&oacute;pia referidos neste
an&uacute;ncio s&atilde;o da propriedade dos seus respectivos donos.
</font>

</p>

<hr />

{{% include "content/includes/press_contacts.html" %}}
