---
aliases:
- ../announce-1.92
date: 2000-07-25
title: KDE 1.92 Release Announcement
---

<STRONG>Third Beta Preview of Leading Desktop for Linux<SUP>&reg;</SUP> and Other Unixes<SUP>&reg;</SUP></STRONG>
July 25, 2000 (The INTERNET).  The <a href="/">KDE
Team</A> today announced the release of KDE 1.92, codenamed "Korner",
the third beta preview of KDE's
next-generation, powerful, modular desktop.  Following on the heels of the
release of KDE 1.91 (Kleopatra) on June 24, 2000, Korner is based on
a snapshot of
<a href="http://www.trolltech.com/">Trolltech's</A><SUP>tm</SUP>
upcoming Qt<SUP>&reg;</SUP> 2.2.0 and will include the core libraries,
the core desktop environment, the KOffice suite, as well
as the over 100 applications from the other standard base KDE packages: 
Administration, Games,
Graphics, Multimedia, Network, Personal Information Management (PIM),
Toys and Utilities. 
Korner is targeted at users who would like to help the
KDE team make usability and feature enhancements and fix the remaining
set of bugs before the release of KDE 2.0, scheduled
for early September 2000.


Korner benefitted greatly from a productive
<a href="k3b-announce.php">10-day conference</A> of many core KDE developers in Norway last week.
"A lot has happened since the last KDE release", said Matthias Kalle Dalheimer,
President of Klar�lvdalens Datakonsult AB and long-time core KDE developer. 
"Besides fixing many of the remaining bugs, we also achieved numerous
improvements in useability, functionality and appearance. I am especially
impressed by the remarkable advances in HTML display and in KOffice."


Korner offers a quite stable desktop suitable for a non-critical
environment.  The principal benefits lie in
the cutting-edge technologies provided by
<a href="http://konqueror.kde.org/">Konqueror</A> and
the <a href="http://koffice.kde.org/">KOffice suite</A>, in
KDE's enhanced customizability and in KDE's continued improvements in
ease of use.


- <a id="Konqueror">Konqueror</A> reigns as the next-generation
  web browser, file manager and
  document viewer for KDE 2.0. Widely acclaimed as a technological
  break-through for the Linux desktop, Konqueror has a component-based
  architecture which combines the features and functionality of Internet
  Explorer<SUP>&reg;</SUP>/Netscape Communicator<SUP>&reg;</SUP> and
  Windows Explorer<SUP>&reg;</SUP>. Konqueror will support
  the full gamut of current Internet technologies, including
  JavaScript<SUP>TM</SUP>, Java<SUP>&reg;</SUP>, HTML 4.0, CSS-2
  (Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
  and Netscape Communicator<SUP>&reg;</SUP> plugins (for
  viewing Flash<SUP>TM</SUP>,
  RealAudio<SUP>TM</SUP>, RealVideo<SUP>TM</SUP> and similar technologies).
  In addition,
  Konqueror's network transparency offers seamless support for browsing
  Linux<SUP>&reg;</SUP> NFS shares, Windows<SUP>&reg;</SUP> SMB shares,
  HTTP pages, FTP directories as well as any other protocol for which
  a plug-in is available.

<BR> <BR>

- <a id="KOffice">The KOffice suite</A> is one of the most-anticipated
  Open Source projects. The suite consists
  of a spreadsheet application (KSpread), a vector drawing application
  (KIllustrator), a frame-based
  word-processing application (KWord), a presentation program (KPresenter),
  and a chart and diagram application
  (KChart). Native file formats will use XML, and work on
  filters for proprietary binary file formats is progressing.
  Combined with a powerful scripting language and the
  ability to embed individuals components within each other using KDE's
  KParts technology, the KOffice
  suite will provide all the necessary functionality to all but the most
  demanding power users, at an unbeatable price -- free.

<BR> <BR>

- KDE's customizability touches every aspect of this next-generation
  desktop. <a id="Style engine">Korner benefits from Qt's
  style engine, which permits developers and artists to create their
  own widget designs down to the precise appearance of a scrollbar,
  a button, a menu and more, combined with development tools which will
  largely automate the creation of these widget sets. Just to
  mention a few of the legion configuration options,
  users can choose among: numerous types of menu effects; a menu
  bar atop the display (Macintosh<SUP>&reg;</SUP>-style) or atop each individual
  window (Windows-style); <a href="http://www.mosfet.org/themeapi">Native
  KDE2-Themes</A> and <a href="k3c-announce.php">
  GTK-Themes</A>; icon-effects and -themes; system sounds; key bindings;
  languages; toolbar and menu composition; and much much more.

<BR> <BR>

#### Downloading and Compiling Korner

The source packages for Korner are available at
<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta3/tar/src/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta3/tar/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</A>.  Korner requires
Trolltech's Qt 2.1.9-korner, which is a snapshot only available
from the above locations under the name qt-copy-1.92.  Please
note that Qt 2.1.9-korner is not an official Trolltech release and is also not
part of KDE's beta testing.


Please be advised that Korner will <STRONG>not</STRONG> work with older
versions of Qt, including Trolltech's latest stable release, Qt 2.1.1,
which formed the basis of the Kleopatra release.


For further instructions on compiling and installing Korner, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</A> and, if you encounter problems, the
<a href="http://developer.kde.org/build/index.html">compilation FAQ</A>.

#### Installing Binary Packages of Korner

The binary packages for Korner are available under
<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta3/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta3/</A> or under the
equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</A>. Korner requires
Trolltech's Qt 2.1.9-korner, which is a snapshot only available
from the above locations under the name qt-copy-1.92.  Please
note that Qt 2.1.9-korner is not an official Trolltech release and is also not
part of KDE's beta testing.


Please be advised that Korner will <STRONG>not</STRONG> work with older
versions of Qt, including Trolltech's latest stable release, Qt 2.1.1,
which formed the basis of the Kleopatra release.


At the time of this release, precompiled packages are available for:


- <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta3/rpm/Caldera-2.4">Caldera-2.4</A>
- <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta3/rpm/Mandrake">Mandrake</A>
- <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta3/rpm/Redhat">Redhat</A>
- <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta3/rpm/SuSE">SuSE</A>


Check the ftp servers periodically for pre-compiled packages for other
distributions.


#### About KDE
KDE is a collaborative project by hundreds of developers worldwide to
create a sophisticated, customizable and stable desktop environment
employing a network-transparent, intuitive user interface.  Currently
development is focused on KDE 2, which will for the first time offer a
free, Open Source, fully-featured office suite and which promises to
make the Linux desktop as easy to use as Windows<SUP>&reg;</SUP> and
the Macintosh<SUP>&reg;</SUP>
while remaining loyal to open standards and empowering developers and users
with Open Source software.  KDE is working proof of how the Open Source
software development model can create technologies on par with and superior
to even the most complex commercial software.

For more information about KDE, please visit KDE's <a href="/whatiskde">web site</A>.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#x67;&#x72;&#x61;&#110;r&#x6f;t&#0104;&#64;&#x6b;&#100;e&#46;&#x6f;rg<BR>
(1) 480 732 1752
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
&#0102;a&#x75;&#114;&#x65;&#64;&#107;de.&#x6f;&#x72;&#103;<BR>
(44) 1225 471 300
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
&#x6b;o&#x6e;&#x6f;l&#100;&#0064;&#107;de&#0046;&#0111;rg<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
