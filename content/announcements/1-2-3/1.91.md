---
aliases:
- ../announce-1.91
date: 2000-06-14
title: KDE 1.91 Release Announcement
---

<b>Second Beta Preview of Advanced Linux<sup>&reg;</sup> Desktop</b>
June 14, 2000 (The Internet). The <a href="http://www.kde.org">KDE Team</a>
today announced the release of KDE 1.91, codenamed "Kleopatra", the second
beta preview of KDE's next-generation, powerful, modular desktop. Following
on the heels of the release of KDE 1.90 (Konfucious) on May 11, 2000, Kleopatra
is based on
<a href="http://www.trolltech.com">Qt 2.1</a> and will include
the core libraries, the core desktop environment, the KOffice suite, as
well as most of the other standard base KDE packages: kdeadmin, kdegames,
kdegraphics, kdenetwork, kdemultimedia, kdetoys and kdeutils. Kleopatra
is targeted at
<a href="#developer">developers</a> and interested
<a href="#user">users</a>.
For those compiling from source, please consult the <a href="http://developer.kde.org/build/index.html">compilation
instructions</a>.
"Kleopatra fixes many of the bugs which helpful users reported in Konfucious,
the previous release," stated David Faure, a core KDE developer. "The core
libraries are now frozen except for critical changes, so developers of
KDE 1 applications can now safely port to KDE 2 without fearing any major
changes in the core libraries."
<a id="developer"></a><b>For the developer</b>, KDE 1.91 provides
a stable API which will enable developers to commence serious development
of their application so they may time the release of their software to
coincide with the release of KDE 2.0, scheduled for September 2000. It
is anticipated that with the exception of aRts, the budding KDE 2.0 real
time multimedia engine, and the <a href="#Style engine">KDE style engine</a>,
there will be few binary incompatible and fewer, if any, source incompatible
changes in the core libraries through the 2.0 release. Further development
will focus on finalizing aRts, fixing bugs, complying with the
<a href="http://developer.kde.org/documentation/standards/">KDE
Standards and Style Guides</a> and maximizing performance.
Kleopatra offers a large number of major technological improvements
to developers compared to the critically acclaimed KDE 1.x series. Chief
among these are the Desktop COmmunication Protocol (DCOP), the i/o libraries
(KIO), the component object model (KParts), an XML-based GUI class, and
the standards-compliant HTML rendering engine (KHTML).

- <a id="KIO"></a>DCOP is a client-to-client communications protocol intermediated
  by a server. The protocol supports both message passing and remote procedure
  calls. The technology is used in KDE 2.0, for example, to direct application
  requests to instances of the application which are already running, thereby
  preventing multiple occurrences of the same application from running concurrently.

<br>&nbsp;

- <a id="KIO"></a>KIO implements i/o in a separate process to permit a
  non-blocking GUI. The class is network transparent and hence can be used
  seamlessly to access HTTP, FTP, Gopher, POP, IMAP, NFS, SMB, LDAP and local
  files. Moreover, its modular and extensible design permits developers to
  "drop in" additional protocols, such as WebDAV, which will then automatically
  be available to all KDE applications. KIO also implements a trader which
  can locate handlers for specified mimetypes; these handlers can then be
  embedded within the requesting application using the KParts technology
  (described
  <a href="#KParts">below</a>).

<br>&nbsp;

- <a id="KParts"></a>KParts, the KDE component object model, allows one
  process to embed another within itself. The technology handles all aspects
  of the embedding, such as positioning toolbars and inserting the proper
  menus when the embedded component is activated or deactivated. KParts can
  also interface with the <a href="#KIO">KIO</a> trader to locate available
  handlers for specific mimetypes or services/protocols. This technology
  is used extensively by the
  <a href="#KOffice">KOffice suite</a> and
  <a href="#Konqueror">Konqueror</a>.

<br>&nbsp;

- <a id="KIO"></a>The XML GUI employs XML to create and position menus,
  toolbars and possibly other aspects of the GUI. This technology offers
  developers and users the advantage of simplified configurability of these
  user interface elements across applications and automatic compliance with
  the
  <a href="http://developer.kde.org/documentation/standards/">KDE Standards
  and Style Guide</a> irrespective of modifications to the standards.

<br>&nbsp;

- <a id="KHTML"></a>KHTML is an HTML 4.0 compliant rendering and drawing
  engine. The class supports the full gamut of current Internet technologies,
  including JavaScript<sup>TM</sup>, Java<sup>&reg;</sup>, HTML 4.0, CSS-2
  (Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
  and Netscape Communicator<sup>&reg;</sup> plugins (for viewing Flash<sup>TM</sup>,
  RealAudio<sup>TM</sup>, RealVideo<sup>TM</sup> and similar technologies).
  The KHTML class can easily be used by an application as either a widget
  (using normal X Window parenting) or as a component (using the <a href="#KParts">KParts</a>
  technology). KHTML, in turn, has the capacity to embed components within
  itself using the KParts technology.

<a id="user"></a><b>For the interested user</b>, KDE 1.91 offers a fairly
stable desktop suitable for a non-critical environment. Users who would
like the opportunity to contribute to the further development of KDE can
use this release as a basis for offering suggestions and bug reports, or
those who are curious can evaluate the new frontier of the \*nix desktop.
The attractions of Kleopatra to users are manifold and impressive. The
principal benefits to users lie in the cutting-edge technologies provided
by <a href="http://www.konqueror.org">Konqueror</a>, the <a href="http://koffice.kde.org">KOffice
suite</a>, KDE's enhanced customizability, and full Unicode support.

- <a id="Konqueror"></a>Konqueror stands tall as the next-generation web
  browser, file manager and document viewer for KDE 2.0. Widely acclaimed
  as a technological break-through for the Linux desktop, Konqueror has a
  component-based architecture which combines the features and functionality
  of Internet Explorer<sup>&reg;</sup>/Netscape Communicator<sup>&reg;</sup>
  and Windows Explorer<sup>&reg;</sup>. Konqueror supports all major Internet
  technologies supported by <a href="#KHTML">KHTML</a>. In addition, Konqueror's
  network transparency offers seamless support for browsing Linux<sup>&reg;</sup>
  NFS shares, Windows<sup>&reg;</sup> SMB shares, HTTP pages, FTP directories
  as well as any other protocol for which a <a href="#KIO">KIO</a> plug-in
  is available.

<br>&nbsp;

- <a id="KOffice"></a>The KOffice suite, long ago heralded as a
  <a href="http://www.mieterra.com/article/koffice.html">"killer
  app"</a>, is one of the most-anticipated Open Source projects. The suite
  consists of a spreadsheet application (KSpread), a vector drawing application
  (KIllustrator), a bitmap drawing application (KImageShop), a frame-based
  word-processing application (KWord), a chart and diagram application (KChart).
  Native file formats use XML, and work on filters for proprietary binary
  file formats is progressing. Combined with a powerful scripting language
  and the ability to embed individuals components within each other using
  the
  <a href="#KParts">KParts</a> technology, the KOffice suite will provide
  all the necessary functionality to all but the most demanding power users,
  at an unbeatable price -- free.

<br>&nbsp;

- <a id="KIO"></a>KDE's customizability touches every aspect of this next-generation
  desktop.&nbsp;<a id="Style engine"></a>Kleopatra benefits from Qt's style
  engine, which permits developers and artists to create their own widget
  designs down to the precise appearance of a scrollbar, a button, a menu
  and more, combined with development tools which will largely automate the
  creation of these widget sets (note that the configuration files for the
  style engine will change in an incompatible way prior to the next KDE release
  scheduled for June 2000). Just to mention a few of the legion configuration
  options, users can choose among: numerous types of menu effects; a menu
  bar atop the display (Macintosh<sup>&reg;</sup>-style) or atop each individual
  window (Windows-style); icon styles; system sounds; key bindings; languages;
  toolbar and menu composition; and much much more.

<br>&nbsp;

- <a id="Unicode"></a>KDE 2.0 supports Unicode at its very core, the outstanding
  Qt toolkit. In addition, KHTML support includes bidirectional scripts,
  such as Arabic and Hebrew, and Far Eastern languages (Chinese/Japanese/Korean).
  Combined with the 45 separate teams actively translating KDE into other
  languages, KDE 2.0 will truly be an international desktop.

#### WHERE TO DOWNLOAD SOURCE PACKAGES

Kleopatra source packages can be downloaded from: <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta2/tar/src/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta2/tar/src/</a>

Or one of its <a href="/mirrors">mirror</A> sites.

Kleopatra requires the recently released version 2.1.1 of the <a href="http://www.trolltech.com">Qt</A> toolkit.

The source package of Qt 2.1.1 is available at this <a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.1.1.tar.gz">location</A> for your <br>
convenience. Please note that this stable version of Qt has been officially released by Trolltech and is not part of the beta testing of KDE 1.91.<br>

Kleopatra does NOT work with older Qt 1.x or Qt 2.0.

In case you have problems downloading the source packages you may have a look <a href="http://www.kde.org/announcements/rename-1.91.html">here</A>.

If your compilation of the sources does fail at some point please have a look at the <a href="http://www.kde.org/compilationfaq.html">Compilation FAQ</A>.

#### WHERE TO DOWNLOAD BINARY PACKAGES

Kleopatra binary packages will are available from:

<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta2/">
ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta2/</a>

Currently you can find there rpms for
<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta2/rpm/Caldera-2.4/RPMS">
Caldera 2.4</A> and <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta2/rpm/SuSE/">SuSE 6.4</A>.

Or one of its <a href="/mirrors">mirror</A> sites.

#### About KDE

KDE is a collaborative project by hundreds of developers worldwide to create
a sophisticated, customizable and stable desktop environment employing
a network-transparent, intuitive user interface. Currently development
is focused on KDE 2, which will for the first time offer a free, Open Source,
fully-featured office suite and which promises to make the Linux desktop
as easy to use as Windows<sup>&reg;</sup> and the Macintosh<sup>&reg;</sup>
while remaining loyal to open standards and empowering developers and users
with Open Source software. KDE is working proof of how the Open Source
software development model can create technologies on par with and superior
to even the most complex commercial software.
For more information about KDE, please visit KDE's <a href="/whatiskde">web
site</a>.
<br>

<hr NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<table BORDER=0 CELLSPACING=0 CELLPADDING=8 >
<tr>
<th ALIGN=LEFT COLSPAN="2">Press Contacts:</th>
</tr>

<tr VALIGN=TOP>
<td ALIGN=RIGHT NOWRAP>United States:</td>

<td NOWRAP>Kurt Granroth
<br>&#x67;r&#97;&#110;&#114;&#00111;t&#x68;&#64;kde.or&#00103;
<br>(1) 480 732 1752</td>
</tr>

<tr VALIGN=TOP>
<td ALIGN=RIGHT NOWRAP>Europe (French and English):</td>

<td NOWRAP>David Faure
<br>&#x66;aure&#x40;kd&#0101;.&#111;rg
<br>(44) 1225 471 300</td>
</tr>

<tr VALIGN=TOP>
<td ALIGN=RIGHT NOWRAP>Europe (German and English):</td>

<td NOWRAP>Martin Konold
<br>ko&#110;&#00111;l&#x64;&#x40;k&#x64;&#00101;&#00046;or&#00103;
<br>(49) 179 22 5 22 49&nbsp;</td>
</tr>
</table>
