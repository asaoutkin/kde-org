---
aliases:
- ../announce-2.2.2
date: '2001-11-21'
description: This page tries to present as much as possible of the very nummerous
  problem corrections and feature additions occurred in KDE between version 1.1 and
  the current, 1.1.1. The changes descriptions were kept brief. The main characteristic
  of the present release is stability, rock solid stability. Thanks to all involved.
  Thanks to the users for their great moral support
title: KDE 2.2.2 Release Announcement
---

FOR IMMEDIATE RELEASE

<h3 align="center">KDE Service/Security Release Available</h3>

KDE Ships Leading Desktop with Advanced Web Browser and Development
Environment for Linux and Other UNIXes

November 21, 2001 (The INTERNET).
The <a href="http://www.kde.org/">KDE
Project</a> today announced the immediate release of KDE 2.2.2,
a powerful and easy-to-use Internet-enabled desktop for Linux.
KDE 2.2.2 is available in
<a href="http://i18n.kde.org/teams/distributed.html">42
languages</a>
and ships with the core KDE libraries, the core
desktop environment, and over 100 applications from the other
base KDE packages (administration, multimedia, network, PIM,
development, etc.).
The KDE Project encourages all users of the
<a href="/community/awards">award-winning</a>
KDE, and strongly encourages all users of KDE
in multi-user environments, to upgrade to KDE 2.2.2.

KDE 2.2.2 is a security and service release. It
marks the last scheduled release of the KDE 2 series, though further
releases may occur. Code development is
currently focused on KDE 3.0,
<a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">scheduled</a>
for its first beta release
next month and for final release in the first quarter of 2002.

The principal improvements over KDE 2.2.1, release two months ago, include:

<ul>
<li>security-related</li>
<ul>
<li>SSL certificate loading</li>
<li>symlink vulnerability in .wmrc access by KDM introduced in 2.2</li>
<li>security problem with eFax (used by klprfax)</li>
<li>potential problem in PAM invocation by KDM</li>
<li>potential harmful side-effect of failed KDM session starts</li>
</ul>
<li>new features</li>
<ul>
<li>added support for
<a href="http://www.codeweavers.com/home/">CodeWeavers</a>'
<a href="http://www.codeweavers.com/products/crossover/">CrossOver</a>
plug-in (provides support for
<a href="http://www.apple.com/quicktime/">QuickTime</a>, etc.)
(<a href="http://www.codeweavers.com/products/crossover/images/quicktime.jpg">screenshot</a>)</li>
<li>added support for the wheelmouse for scrolling through the KGhostview
PS/PDF viewer component</li>
<li>ability to search for multiple patterns at a time in the file search
dialog</li>
<li>debugging multi-threaded applications with
<a href="http://www.kdevelop.org/">KDevelop</a></li>
</ul>
<li>improvements/fixes</li>
<ul>
<li>handling of HTTP links that redirect to FTP</li>
<li>POST using SSL through a proxy and sending headers through proxies</li>
<li>saving of recently-selected files in the file dialog</li>
<li>handling of non-ASCII characters over SMB</li>
<li>toolbar button captions with certain styles</li>
<li>selecting items with the mouse in
<a href="http://konqueror.kde.org/">Konqueror</a></li>
<li>sorting in Konqueror's textview</li>
<li>saving current settings as a theme in the theme manager</li>
<li>crashes in <a href="http://devel-home.kde.org/~kmail/index.html">KMail</a>
 with certain mails</li>
<li>crash on invoking the KDM chooser</li>
<li>non-Latin languages with KDevelop</li>
</ul>
<li>performance</li>
<ul>
<li>icon loading optimized</li>
<li>file dialog speedups</li>
<li>stop spinning SMB client processes</li>
<li>handling of large files in Kate</li>
</ul>
</ul>

A more complete
<a href="/announcements/changelogs/changelog2_2_1to2_2_2">list of
changes</a> and a <a href="/info/2.2.2">FAQ about
the release</a> are available at the KDE
<a href="http://www.kde.org/">web site</a>.

KDE 2.2.2 complements the release of
<a href="http://www.koffice.org/">KOffice</a> 1.1 in August, 2001.
KOffice is a comprehensive, modular, component-based
suite of office productivity applications. KDE is the only Open Source
project to provide a complete desktop and productivity environment for
Linux/UNIX.

KDE and all its components (including KOffice) are available
<em>for free</em> under Open Source licenses from the KDE
<a href="http://download.kde.org/stable/2.2.2/src/">server</a>
and its <a href="/mirrors">mirrors</a> and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.

For more information about the KDE 2.2 series, please see the
<a href="/announcements/announce-2.2">KDE 2.2
press release</a> and the <a href="/info/2.2">KDE
2.2 Info Page</a>, which is an evolving FAQ about the series.

#### Installing KDE 2.2.2 Binary Packages

<em>Binary Packages</em>.
All major Linux distributors and some Unix distributors have provided
binary packages of KDE 2.2.2 for recent versions of their distribution. Some
of these binary packages are available for free download under
<a href="http://download.kde.org/stable/2.2.2/">http://download.kde.org/stable/2.2.2/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>, and additional
binary packages, as well as updates to the packages now available, will
become available over the next few week.

Please note that the KDE team makes these packages available from the
KDE web site as a convenience to KDE users. The KDE project is not
responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution. If you cannot find a binary package for your distribution,
please read the <a href="http://dot.kde.org/986933826/">KDE Binary Package
Policy</a>.

<em>Library Requirements</em>.
The library requirements for a particular binary package vary with the
system on which the package was compiled. Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was included with the applicable distribution (e.g., LinuxDistro 8.0
may have shipped with Qt-2.2.3 but the packages below may require
Qt-2.3.x). For general library requirements for KDE, please see the text at
<a href="#source_code-library_requirements">Source Code - Library
Requirements</a> below.

<a name="package_locations"><em>Package Locations</em></a>.
At the time of this release, pre-compiled packages are available for:

<ul>
  <li><a href="http://www.conectiva.com/">Conectiva Linux</a> (<a href="http://download.kde.org/stable/2.2.2/Conectiva/README">README</a>)</li>
  <ul>
    <li>7.0:  <a href="http://download.kde.org/stable/2.2.2/Conectiva/7.0/RPMS.kde/">Intel i386</a></li>
  </ul>
  <li><a href="http://www.linux-mandrake.com/en/">Mandrake Linux</a> (<a href="http://download.kde.org/stable/2.2.2/Mandrake/README">README</a>)</li>
  <ul>
    <li>8.1:  <a href="http://download.kde.org/stable/2.2.2/Mandrake/8.1/i586/">Intel i586</a> (see also the <a href="http://download.kde.org/stable/2.2.2/Mandrake/8.1/noarch/">noarch</a> directory) and <a href="http://download.kde.org/stable/2.2.2/Mandrake/ia64/ia64/">HP/Intel IA-64</a> (see also the <a href="http://download.kde.org/stable/2.2.2/Mandrake/ia64/noarch/">noarch</a> directory)</li>
    <li>8.0:  <a href="http://download.kde.org/stable/2.2.2/Mandrake/8.0/i586/">Intel i586</a> (see also the <a href="http://download.kde.org/stable/2.2.2/Mandrake/8.0/noarch/">noarch</a> directory) and <a href="http://download.kde.org/stable/2.2.2/Mandrake/ppc/ppc/">PowerPC</a> (see also the <a href="http://download.kde.org/stable/2.2.2/Mandrake/ppc/noarch/">noarch</a> directory)</li>
    <li>7.2:  <a href="http://download.kde.org/stable/2.2.2/Mandrake/7.2/i586/">Intel i586</a></li>
  </ul>
  <li><a href="http://www.redhat.com/">RedHat Linux</a>:
  <ul>
    <li>7.2:  <a href="http://download.kde.org/stable/2.2.2/RedHat/7.2/i386/">Intel i386</a>, <a href="http://download.kde.org/stable/2.2.2/RedHat/7.2/ia64/">HP/Intel IA-64</a> and <a href="http://download.kde.org/stable/2.2.2/RedHat/7.2/alpha/">Alpha</a> (see also the <a href="http://download.kde.org/stable/2.2.2/RedHat/7.2/noarch/">noarch</a> directory)</li>
  </ul>
  <li><a href="http://www.suse.com/">SuSE Linux</a> (<a href="http://download.kde.org/stable/2.2.2/SuSE/README">README</a>):
  <ul>
    <li>7.3:  <a href="http://download.kde.org/stable/2.2.2/SuSE/i386/7.3/">Intel i386</a> (see also the <a href="http://download.kde.org/stable/2.2.2/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.2:  <a href="http://download.kde.org/stable/2.2.2/SuSE/i386/7.2/">Intel i386</a> and <a href="http://download.kde.org/stable/2.2.2/SuSE/ia64/7.2/">HP/Intel IA-64</a> (see also the <a href="http://download.kde.org/stable/2.2.2/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.1:  <a href="http://download.kde.org/stable/2.2.2/SuSE/i386/7.1/">Intel i386</a>, <a href="http://download.kde.org/stable/2.2.2/SuSE/ppc/7.1/">PowerPC</a>, <a href="http://download.kde.org/stable/2.2.2/SuSE/sparc/7.1/">Sun Sparc</a> and <a href="http://download.kde.org/stable/2.2.2/SuSE/axp/7.1/">Alpha</a> (see also the <a href="http://download.kde.org/stable/2.2.2/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.0:  <a href="http://download.kde.org/stable/2.2.2/SuSE/i386/7.0/">Intel i386</a>, <a href="http://download.kde.org/stable/2.2.2/SuSE/ppc/7.0/">PowerPC</a> and <a href="http://download.kde.org/stable/2.2.2/SuSE/s390/7.0/">IBM S390</a> (see also the <a href="http://download.kde.org/stable/2.2.2/SuSE/noarch/">noarch</a> directory)</li>
    <li>6.4:  <a href="http://download.kde.org/stable/2.2.2/SuSE/i386/6.4/">Intel i386</a> (see also the <a href="http://download.kde.org/stable/2.2.2/SuSE/noarch/">noarch</a> directory)</li>
  </ul>
  <li><a href="http://www.tru64unix.compaq.com/">Tru64 Systems</a> (<a href="http://download.kde.org/stable/2.2.2/Tru64/README.Tru64">README</a>)</li>
  <ul>
    <li>Tru64 4.0d, e, f and g and 5.x:  <a href="http://download.kde.org/stable/2.2.2/Tru64/">Alpha</a></li>
  </ul>
</ul>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days and weeks.

#### Downloading and Compiling KDE 2.2.2

<em>Library
Requirements</em>.
KDE 2.2.2 requires the following libraries:

<ul>
<li>Qt-2.2.4, which is available in source code from Trolltech as
<a href="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</a>, though
<a href="ftp://ftp.trolltech.com/qt/source/qt-x11-2.3.1.tar.gz">qt-x11-2.3.1.tar.gz</a> (rather than Qt-2.3.2) is recommended;</li>
<li>for reading help pages and other KDE documentation,
<a href="http://xmlsoft.org/">libxml2</a> &gt;= 2.4.9 and
<a href="http://xmlsoft.org/XSLT/">libxslt</a> &gt;= 1.0.7;</li>
<li>for JavaScript regular expression support,
<a href="http://www.pcre.org/">PCRE</a> &gt;= 3.5;</li>
<li>for SSL support,
<a href="http://www.openssl.org/">OpenSSL</a> &gt;= 0.9.6x
(versions 0.9.5x are no longer supported);</li>
<li>for Java support, a JVM &gt;= 1.3;</li>
<li>for Netscape Communicator plugin support, KDE requires a recent version of
<a href="http://www.lesstif.org/">Lesstif</a> or Motif;</li>
<li>for searching local documentation,
<a href="http://www.htdig.org/">ht://dig</a>; and</li>
<li>for other special features, such as drag'n'drop audio CD ripping,
certain other packages.</li>
</ul>

<em>Compiler Requirements</em>.
Please note that some components of
KDE 2.2.2 will not compile with older versions of
<a href="http://gcc.gnu.org/">gcc/egcs</a>, such as egcs-1.1.2 or
gcc-2.7.2. At a minimum gcc-2.95-\* is required. In addition, some
components of KDE 2.2.2 (such as the multimedia backbone of KDE,
<a href="http://www.arts-project.org/">aRts</a>) will not compile with
<a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0.x</a> (the
problems are being corrected by the KDE and GCC teams and should be
resolved by the first stable release of KDE 3.0).

<em>Source Code/SRPMs</em>.
The complete source code for KDE 2.2.2 is available for free download at
<a href="http://download.kde.org/stable/2.2.2/src/">http://download.kde.org/stable/2.2.2/src/</a>
or in the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>.
Additionally, source rpms are available for the following distributions:

<ul>
  <li><a href="http://download.kde.org/stable/2.2.2/Mandrake/SRPMS/">Mandrake Linux</a></li>
  <li><a href="http://download.kde.org/stable/2.2.2/RedHat/7.2/SRPMS/">RedHat Linux</a></li>
  <li><a href="http://download.kde.org/stable/2.2.2/SuSE/SRPMS/">SuSE Linux</a></li>
</ul>

<em>Further Information</em>. For further
instructions on compiling and installing KDE 2.2.2, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you should encounter compilation problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>. For
problems with SRPMs, please contact the person listed in the applicable
.spec file.

#### About KDE

KDE is an independent, project by hundreds of developers worldwide
collaborating over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture. KDE is working proof of the power of
the Open Source "Bazaar-style" software development model to create
first-rate technologies on par with and superior to even the most complex
commercial software.

Please visit the KDE family of web sites for the
<a href="http://www.kde.org/faq.html">KDE FAQ</a>,
<a href="http://www.kde.org/screenshots/kde2shots.html">screenshots</a>,
<a href="http://www.koffice.org/">KOffice information</a> and
<a href="http://developer.kde.org/documentation/kde2arch.html">developer
information</a>.
Much more <a href="http://www.kde.org/whatiskde/">information</a>
about KDE is available from KDE's
<a href="http://www.kde.org/family.html">family of web sites</a>.

#### Corporate KDE Sponsors

Besides the valuable and excellent efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>. In addition, the members of
the <a href="http://www.kdeleague.org/">KDE League</a> provide significant
support for promoting KDE. Thanks!

<hr /><font size=2>
<em>Trademarks Notices.</em>
KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
UNIX and Motif are registered trademarks of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
Netscape Communicator is a trademark or registered trademark of
Netscape Communications Corporation in the United States and other countries.
Java is a trademark of Sun Microsystems, Inc.
QuickTime is a trademark of Apple Computer, Inc., registered in the U.S.
and other countries.
All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.</font>
<br />
<hr />
<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
  Eunice Kim<br>
  The Terpin Group<br>

[ekim@terpin.com](mailto:ekim@terpin.com)<br>
(1) 650 344 4944 ext. 105<br>&nbsp;<br>
Kurt Granroth <br>

[granroth@kde.org](mailto:granroth@kde.org)
<br>
(1) 480 732 1752<br>&nbsp;<br>
Andreas Pour<br>

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 718-456-1165

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(44) 1225 837409

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
  (49) 2421 502758
  </td>
</tr>
</table>
