---
title: "The Developer's Perspective: Developing With KDE"
hidden: true
---

Once you've decided to start developing with KDE, you may be looking for a starting point.
A good first step is going through the <a href="http://doc.trolltech.com/3.3/tutorial.html">tutorials</a> included
with Qt as well as the documentation and articles at the <a href="http://developer.kde.org">KDE Developer's Corner</a>.

Don't forget to subscribe to the various <a href="https://www.kde.org/mailinglists/#development">KDE development
email lists</a> where you can interact with KDE developers of all skill levels, from
core developers to those brand new to the platform. Learning to take full advantage of the <a href="../developers-whykde#tools">development tools</a> is also worth your time as that will help ensure
your time spent developing with KDE is enjoyable and effective.

If you wish to contribute to the KDE project itself, the <a href="http://bugs.kde.org">KDE Bug Tracking System</a> and the <a href="https://www.kde.org/jobs/">KDE Jobs</a> page are good places to start to find ideas. Of course, the tried-and-true method of scratching an itch always works too. There's also an <a href="http://developer.kde.org/documentation/tutorials/howto/index.html">excellent tutorial</a> available at the <a href="http://developer.kde.org">KDE Developer's Corner</a> that is geared toward new developers, occasional developers, and anyone considering contributing to KDE.

<table style="border: solid 1px;" align="center" cellpadding="6" cellspacing="0">
<tr>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Users</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Developers</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">About KDE</th>
</tr>
<tr>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../users-whykde">Why KDE?</a><br/>
<a href="../users-deploying">Deploying KDE</a><br/>
<a href="../users-supporting">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../developers-whykde">Why KDE?</a><br/>
<a href="../developers-developing">Developing With KDE</a><br/>
<a href="../developers-supporting">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap"><a href="http://www.kde.org">The KDE Project</a><br/>
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a><br/>

</td>
</tr>
</table>
