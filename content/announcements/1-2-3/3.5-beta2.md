---
aliases:
- ../announce-3.5beta2
date: 2005-10-18
title: Announcing KDE 3.5 Beta 2 (\"Koalition\")
---

DATELINE October 18, 2005

### KDE Project Ships Second Beta of Next Major Release

October 18, 2005 (The Internet) - The <a href="http://www.kde.org">KDE Project</a> is pleased to announce the immediate availability of KDE 3.5 Beta 2, dubbed "Koalition".

#### Getting Koalition

KDE 3.5 Beta 2 can be downloaded over the Internet by visiting <a href="http://download.kde.org/unstable/3.5-beta2/src">download.kde.org</a>. Source code and vendor supplied binary packages are available. For additional information on package availability and to read further release notes, please visit the <a href="http://www.kde.org/info/3.5beta2.php">KDE 3.5 Beta 2 information page</a>.

The KDE team asks everyone to try the version and give feedback through <a href="http://bugs.kde.org">the bug tracking system</a>.

#### Supporting KDE

KDE is supported through voluntary contributions of time, money and resources by individuals
and companies from around the world. To discover how you or your company can join in and help
support KDE please visit the <a href="http://www.kde.org/community/donations/">Supporting KDE</a> web page. There
may be more ways to support KDE than you imagine, and every bit of support helps make KDE
a better project and a better product for everyone. Communicate your support today with a monetary donation,
new hardware or a few hours of your time!

Especially for beta releases, we can need any helping hand that is offered us to categorize and fix problem reports

#### KDE Sponsorship

Besides the superb and invaluable efforts by the
<a href="http://www.kde.org/people/">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandriva.com/">Mandriva</a>,
<a href="http://www.trolltech.com/">Trolltech</a> and
<a href="http://www.suse.com/">SUSE/Novell</a>.
<a href="http://www.ibm.com/">IBM</a> has donated significant hardware
to the KDE Project, and the
<a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
provide most of the Internet bandwidth for the KDE project. Thanks!

#### About KDE

KDE is an independent project of hundreds of developers, translators,
artists and other professionals worldwide collaborating over the Internet
to create and freely distribute a sophisticated, customizable and stable
desktop and office environment employing a flexible, component-based,
network-transparent architecture and offering an outstanding development
platform. KDE provides a stable, mature desktop, a full, component-based
office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools and utilities, and an
efficient, intuitive development environment featuring the excellent IDE
<a href="http://www.kdevelop.org/">KDevelop</a>. KDE is working proof
that the Open Source "Bazaar-style" software development model can yield
first-rate technologies on par with and superior to even the most complex
commercial software.

<hr noshade="noshade" size="1" width="98%" align="center" />

<font size="2">
<em>Trademark Notices.</em>
KDE and K Desktop Environment are trademarks of KDE e.V.

Linux is a registered trademark of Linus Torvalds.

UNIX is a registered trademark of The Open Group in the United States and
other countries.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

<hr noshade="noshade" size="1" width="98%" align="center" />

#### Press Contacts

<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="mai&#x6c;&#00116;o&#x3a;in&#x66;&#x6f;&#00045;&#97;fric&#97;&#x40;&#0107;&#100;&#x65;.&#111;&#114;g">&#x69;n&#0102;&#111;&#00045;afri&#x63;&#097;&#064;&#107;d&#00101;&#0046;or&#103;</a><br />

</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="&#x6d;ai&#x6c;&#00116;o:&#105;&#x6e;&#102;o-a&#115;&#105;&#97;&#x40;&#107;d&#x65;.o&#x72;&#x67;">&#105;&#0110;f&#0111;-&#0097;&#x73;i&#x61;&#0064;kde&#046;&#x6f;rg</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;ilt&#111;:&#x69;nf&#111;&#x2d;&#x65;&#117;r&#x6f;&#x70;&#101;&#00064;&#x6b;d&#x65;&#046;o&#x72;&#x67;">inf&#00111;-e&#117;&#114;&#x6f;&#112;e&#064;kde&#0046;&#111;rg</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="m&#0097;&#0105;&#x6c;t&#x6f;&#58;i&#x6e;f&#111;-n&#111;rt&#104;a&#109;e&#114;&#x69;&#99;&#x61;&#00064;&#x6b;&#x64;&#101;&#46;o&#00114;&#103;">&#x69;&#110;f&#111;&#x2d;no&#114;&#116;hamer&#x69;c&#97;&#x40;&#0107;&#100;&#101;&#0046;&#x6f;r&#103;</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#00109;a&#0105;&#108;&#0116;&#111;:i&#x6e;&#00102;&#00111;&#045;o&#x63;ea&#x6e;&#x69;a&#64;k&#100;e&#x2e;org">inf&#x6f;-oce&#x61;&#110;&#0105;a&#x40;kde&#x2e;o&#x72;g</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#00105;lto&#058;&#0105;&#110;f&#x6f;-&#0115;outha&#109;&#x65;&#114;&#0105;ca&#x40;k&#100;&#x65;.or&#103;">inf&#111;&#45;southa&#x6d;&#0101;&#x72;&#105;ca&#x40;k&#100;e.&#x6f;rg</a><br />
</td>

</tr></table>