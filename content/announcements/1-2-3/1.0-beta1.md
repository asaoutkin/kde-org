---
aliases:
- ../beta1announce
date: 1997-10-20
title: KDE 1.0-beta1 Release Announcement
---

```




              T H E   K   D E S K T O P   E N V I R O N M E N T

                              http://www.kde.org

                                    Beta 1
                               October 20, 1997


       An integrated Desktop Environment for the Unix Operating System


  We  are  pleased  to  announce  the  availability of the first public beta
  "Arnsberg" of the K Desktop Environment.

  KDE is a powerful graphical desktop  environment  for  Unix  workstations.
  The  KDE  desktop  aims to combine ease of use, contemporary functionality
  and outstanding graphical design with the technological superiority of the
  Unix operating system.

  KDE  is  a completely new desktop, incorporating a large suite of applica-
  tions for Unix workstations. While KDE includes  a  window  manager,  file
  manager,  panel,  control  center and many other components that one would
  expect to be part of a contemporary desktop environment, the true strength
  of this exceptional environment lies in the interoperability of its compo-
  nents.


  Key KDE features:


        o Transparent Network Access:

          Most KDE components are inherently Internet  aware.  KDE's  folder
          windows  are in fact web browsers. You can open URLs and view HTML
          pages, download files by dragging them  from  the  folder  windows
          onto your desktop or even edit remote documents by simply dragging
          them from a remote site opened in a folder onto KDE's editor.


        o Session management:

          KDE supports elaborate session management. You can leave documents
          and  applications  open  when  you  log out and session management
          aware applications will be restored to  their  previous  state  on
          your  next log in. KDE offers pseudo session management for legacy
          X11 applications.


        o Configurability:

          Most, if not all settings are configurable through  intuitive  GUI
          dialogs  rather  then the traditional cryptic configuration files.
          The configuration files are of course human readable.


        o Vast pool of KDE applications:

          A great number of applications have already been written  for  the
          KDE  project.  Some  are supplied with the base packages available
          for download from http://www.kde.org, others can be found  in  the
          'apps'  directory on ftp.kde.org and its many mirrors.  We proudly
          offer applications ranging  from  a  postscript  viewer  to  a  CD
          player,  a calculator to a sound system, and even a finite element
          analysis application. We are confident you will be pleasantly sur-
          prised by the broad spectrum of applications available today.


        o Internationalization:

          Most of KDE's core components and many of the KDE applications are
          internationalized. That is, they will operate in  many  languages.
          We  consider this a key feature in gaining acceptance among a wide
          audience. Please consider helping us translate more of our  appli-
          cations.  Visit  http://www.kde.org/i18n.html for more information
          on the internationalization project.



  Screen Shots:

          Screen shots of a typical K desktop and some of  its  core  compo-
          nents can be found at  http://www.kde.org/kscreenshots.html We are
          certain you will like what you see.


  Where to get KDE:

          You    can    download    the    KDE    base     packages     from
          ftp://ftp.kde.org/pub/kde or one of its many mirrors. Please visit
          our web site at http://www.kde.org  for  more  info.  The  precise
          locations of the beta 1 packages are:

            ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/tgz/binary
            ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/tgz/source
            ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/rpm/binary
            ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/rpm/source



  Supported platforms:

          KDE  was  primarily  developed under the GNU/Linux variant of the Unix
          operating system. However it is known to compile without, or  with
          very few, problems on most Unix variants.

          At the moment we explicitely support  GNU/Linux (Intel , Alpha, Sparc)
          and Solaris (Sparc) and we  have  success  reports  for   FreeBSD,
          IRIX, OSF, SunOS, HP-UX. and others.


  KDE Development:

          KDE  was  developed  by a world-wide network of programmers during
          the past 12 months. In fact the release of the  first  beta  coin-
          cides roughly with KDE's first birthday, October 16th 1997.

          KDE  development  is open to everyone. Please consider joining the
          KDE project. If you are a programmer,  consider  writing  applica-
          tions  for  the  KDE project. If you are a graphical design artist
          consider creating icon sets, color schemes, sound schemes,  logo's
          and other supporting art. If you enjoy writing, you are invited to
          join our documentation project and help creating  help  documenta-
          tion,  tutorials and other supporting documentation in the K docu-
          mentation project (KDP). Please visit our  web  site  for  further
          information  on  how you can help make KDE an even better environ-
          ment than it already is.

          To the expert:

          KDE's development model differs deliberately from  that  of  other
          known  desktop  projects in that it does not try to follow precon-
          ceived handed-down specifications. We have recognized  the  avail-
          ablilty  of an existing and usable desktop at all stages of devel-
          opment as paramount to the success of the project.  Only  in  this
          fashion we are able to attract the large number of developers nec-
          essary for a project of this enormous scale. KDE  improves  on  an
          almost  daily basis in an iterative cycle of design, coding, test-
          ing, revision, redesign, leading  to constant excitement and  con-
          crete tangible results at any given time. We are convinced that we
          have chosen the right development model prerequisite to running  a
          successful  Internet  project, and accommodating  the special cir-
          cumstances arising from the fact that all development is done on a
          strictly  volunteer  basis. Please give KDE a try, we believe that
          the results speak for themselves.


  Licensing:

          The KDE libraries are licensed under the LGPL,  and  the  applica-
          tions  are  licensed  under  the  GPL.  KDE is based on the Qt GUI
          library which is free for the X Window System, provided the appli-
          cations  developed  with it are released under a free license such
          as  the  GPL.   More  information  about  Qt  may  be   found   at
          http://www.troll.no


  What does the 'K' stand for?

          Nothing.  It is simply named the "K Desktop Environment".


  Lastly: KDE is NOT a Window Manager

          KDE  is not just another window manager trying to imitate the look
          of an existing desktop environment, but rather an integrated envi-
          ronment  of  which  the window manager provided with KDE is only a
          small part. In fact if you so desire you can continue to use  your
          favorite  window  manger, however we strongly encourage you to use
          the KDE window manager and enjoy the functionality afforded by the
          interoperability of the KDE components. The KDE window manger pro-
          vides all the functionality you need and more.

          Additionally, KDE is not a CDE, Windows or OS/2 clone.  While some
          ideas may have originated from these systems, KDE is unique and we
          have no intentions of cloning another system.


  Bug Reports:

          Please submit your bug reports to kde-bugs@kde.org. Consider  sub-
          scribing to our mailing lists depending on your interests.


  Mailing Lists:

           kde-announce   (announcements)
           kde            (general discussion)
           kde-devel      (development issues)
           kde-look       (look and feel issues)
           kde-licensing  (licensing issues)
           kde-user       (end user discussion)
           kde-patches    (daily patches)

          To subscribe (unsubscribe), send mail to

                     [list]-request@fiwi02.wiwi.uni-tuebingen.de
          with:

                     subscribe (unsubscribe) [your-email-address]

          in  the  Subject:  line  of the message. A mailing list archive is
          provided at:

                        http://www.itm.mu-luebeck.de/~coolo/kde/

  Thanks:

          We would like to thank Troll Tech AS http://www.troll.no for  cre-
          ating    Qt    and    our    sponsors    for   making    KDE   ONE
          http://www.kde.org/kde-one.html in Arnsberg, Germany possible.

  Epilogue:

          We sincerely believe that we have initiated a new era for the Unix
          operating  system  and hope you will share our excitement. We hope
          that KDE will allow you to get your  work  done  faster  and  more
          efficiently  than ever. Please consider joining and supporting the
          project.



  The KDE Core Team



  APPENDIX:


  Mirrors:

          We expect ftp.kde.org to be rather busy in the future. Please take
          note of the following list of official mirrors of ftp.kde.org.

               ftp://ftp.net.lut.ac.uk/kde
               ftp://ftp.nvg.unit.no/pub/linux/kde
               ftp://linux.unipv.it/pub/linux/kde
               ftp://ftp.chialpha.org/pub/kde
               ftp://ftp.blaze.net.au/pub/kde
               ftp://belief.cslab.tuwien.ac.at/pub/kde
               ftp://ftp.gwdg.de/pub/x11/kde/
               ftp://ops.linux.co.za/pub/Linux/kde
               ftp://ftp.uni-erlangen.de/pub/Linux/MIRROR.KDE
               ftp://ftp.dataplus.se/pub/linux/kde
               ftp://ftp.klammeraffe.org/pub/mirror/kde
               ftp://ftp.tvnet.hu/pub/kde

          The packages will also be uploaded to:

               ftp://sunsite.unc.edu/pub/Linux/Incoming/
               ftp://ftp.redhat.com/pub/Incoming

          Expect  the  usual delays before all packages are available at all
          ftp sites.

```