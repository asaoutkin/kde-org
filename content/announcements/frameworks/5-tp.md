---
aliases:
- ../frameworks5TP
date: '2014-01-07'
description: KDE Ships Frameworks 5 Technology Preview.
title: Frameworks 5 Technology Preview
---

{{<figure src="/announcements/frameworks/5-tp/KDE_QT.jpg" class="text-center" caption="Collaboration between Qt and KDE" >}}
<br />

January 7, 2014. The KDE Community is proud to announce a Tech Preview of KDE Frameworks 5. Frameworks 5 is the result of almost three years of work to plan, modularize, review and port the set of libraries previously known as KDElibs or KDE Platform 4 into a set of Qt Addons, separate libraries with well-defined dependencies and abilities, ready for Qt 5. This gives the Qt ecosystem a powerful set of drop-in libraries providing additional functionality for a wide variety of tasks and platforms, based on over 15 years of KDE experience in building applications. Today, all the Frameworks are available in Tech Preview mode; a final release is planned for the first half of 2014. Some Tech Preview addons (notably KArchive and Threadweaver) are more mature than others at this time.

## What is Frameworks 5?

The KDE libraries are currently the common code base for (almost) all KDE applications. They provide high-level functionality such as toolbars and menus, spell checking and file access. Currently, 'kdelibs' is distributed as a single set of interconnected libraries. Through KDE Frameworks efforts, these libraries have been methodically reworked into a set of independent, cross platform libraries that will be readily available to all Qt developers.

The KDE Frameworks—designed as drop-in Qt Addons—will enrich Qt as a development environment with libraries providing functions that simplify, accelerate and reduce the cost of Qt development. Frameworks eliminate the need to reinvent key functionality.

The transition from Platform to Frameworks has been underway for almost three years and is being implemented by a team of about 20 (paid and volunteer) developers and actively supported by four companies. Frameworks 5 consists of 57 modules: 19 independent libraries (Qt Addons) not requiring any dependencies; 9 that require libraries which themselves are independent; and 29 with more significant dependency chains. Frameworks are developed following the <a href='http://community.kde.org/Frameworks/Policies'>Frameworks Policies</a>, in a vendor neutral, open process.

<a href='http://dot.kde.org/2013/09/25/frameworks-5'>This KDE News article</a> has more background on Frameworks 5.

## Available today

The tech preview made available today contains all 57 modules that are part of Frameworks 5. Of these, two have a maturity level that shows the direction of Frameworks: ThreadWeaver and KArchive. Developers are invited to take all of the modules for a spin and provide feedback (and patches) to help bring them to the same level of maturity.

KArchive offers support for many popular compression codecs in a self-contained, featureful and easy-to-use file archiving and extracting library. Just feed it files; there is no need to reinvent an archiving function in your Qt-based application! ThreadWeaver offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread execution by specifying dependencies between the threads and executing them while satisfying these dependencies, greatly simplifying the use of multiple threads. These are available for production use now.

There is a <a href='http://community.kde.org/Frameworks/List'>full list of the Frameworks</a>; tarballs with the current code can be <a href='http://download.kde.org/unstable/frameworks/4.95.0/'>downloaded</a>. <a href='http://community.kde.org/Frameworks/Binary_Packages'>Binaries</a> are available as well.

{{<figure src="/announcements/frameworks/5-tp/kf5_small.png" class="text-center" alt="Overview of the KDE Frameworks (a work in progress!)" >}}
<br />

The team is currently working on providing a detailed listing of all Frameworks and third party libraries at <a href='http://inqlude.org'>inqlude.org</a>, the curated archive of Qt libraries. Each entry includes a dependency tree view. Dependency diagrams can also be found <a href='http://agateau.com/tmp/kf5/'>here</a>.

## Working towards a final release

The team will do monthly releases with a beta planned for the first week of April and a final release in the beginning of June.

Plans for this period include tidying up the infrastructure, integration with QMake and pkg-config for non-CMake users, getting CMake contributions upstream, and a final round of API cleanups and reviews. Frameworks 5 will be open for API changes until the beta in April.

Those interested in following progress can check out the <a href='https://projects.kde.org/projects/frameworks'>git repositories</a>, follow the discussions on the <a href='https://mail.kde.org/mailman/listinfo/kde-frameworks-devel'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='https://git.reviewboard.kde.org/groups/kdeframeworks/'>review board</a>. Policies and the current state of the project and plans are available at the <a href='http://community.kde.org/Frameworks'>Frameworks wiki</a>. Real-time discussions take place on the <a href='irc://#kde-devel@freenode.net'>#kde-devel IRC channel on freenode.net</a>.

## Discuss, Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.

You can discuss this news story <a href='http://dot.kde.org/2014/01/07/frameworks-5-tech-preview'>on the Dot</a>, KDE's news site.

#### Support KDE

<a href="http://jointhegame.kde.org/"><img src="/announcements/frameworks/5-tp/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game" /> </a>
KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.

&nbsp;
