---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
description: KDE Ships Frameworks 5.15.0
layout: framework
title: Release of KDE Frameworks 5.15.0
version: 5.15.0
---

{{<figure src="/announcements/frameworks/5-tp/KDE_QT.jpg" class="text-center" caption="Collaboration between Qt and KDE" >}}

October 10, 2015. KDE today announces the release of KDE Frameworks 5.15.0.

KDE Frameworks are 60 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Baloo

- Fix limit/offset handling in SearchStore::exec
- Recreate the baloo index
- balooctl config: add options to set/view onlyBasicIndexing
- Port balooctl check to work with new architecture (bug 353011)
- FileContentIndexer: fix emitting filePath twice
- UnindexedFileIterator: mtime is quint32 not quint64
- Transaction: fix another Dbi typo
- Transaction: Fix documentMTime() and documentCTime() using wrong Dbis.
- Transaction::checkPostingDbInTermsDb: Optimize code
- Fix dbus warnings
- Balooctl: Add checkDb command
- balooctl config: Add "exclude filter"
- KF5Baloo: Make sure D-Bus interfaces are generated before they are used. (bug 353308)
- Avoid using QByteArray::fromRawData
- Remove baloo-monitor from baloo
- TagListJob: Emit error when failed to open database
- Do not ignore subterms if not found
- Cleaner code for failing Baloo::File::load() on DB open fail.
- Make balooctl use IndexerConfig instead of manipulating baloofilerc directly
- Improve i18n for balooshow
- Make balooshow fail gracefully if database cannot be opened.
- Fail Baloo::File::load() if the Database is not open. (bug 353049)
- IndexerConfig: add refresh() method
- inotify: Do not simulate a closedWrite event after move without cookie
- ExtractorProcess: Remove the extra n at the end of the filePath
- baloo_file_extractor: call QProcess::close before destroying the QProcess
- baloomonitorplugin/balooctl: i18nize indexer state.
- BalooCtl: Add a 'config' option
- Make baloosearch more presentable
- Remove empty EventMonitor files
- BalooShow: Show more information when the ids do not match
- BalooShow: When called with an id check if the id is correct
- Add a FileInfo class
- Add error checking in various bits so that Baloo doesn't crash when disabled. (bug 352454)
- Fix Baloo not respecting "basic indexing only" config option
- Monitor: Fetch remaining time on startup
- Use actual method calls in MainAdaptor instead of QMetaObject::invokeMethod
- Add org.kde.baloo interface to root object for backward compatibility
- Fix date string displayed in address bar due to porting to QDate
- Add delay after each file instead of each batch
- Remove Qt::Widgets dependency from baloo_file
- Remove unused code from baloo_file_extractor
- Add baloo monitor or experimental qml plugin
- Make "querying for remaining time" thread safe
- kioslaves: Add missing override for virtual functions
- Extractor: Set the applicationData after constructing the app
- Query: Implement support for 'offset'
- Balooctl: Add --version and --help (bug 351645)
- Remove KAuth support to increase max inotify watches if count too low (bug 351602)

### BluezQt

- Fix fakebluez crash in obexmanagertest with ASAN
- Forward declare all exported classes in types.h
- ObexTransfer: Set error when transfer session is removed
- Utils: Hold pointers to managers instances
- ObexTransfer: Set error when org.bluez.obex crashes

### Extra CMake Modules

- Update GTK icon cache when installing icons.
- Remove workaround to delay execution on Android
- ECMEnableSanitizers: The undefined sanitizer is supported by gcc 4.9
- Disable X11,XCB etc. detection on OS X
- Look for the files in the installed prefix rather the prefix path
- Use Qt5 to specify what's Qt5 installation prefix
- Add definition ANDROID as needed in qsystemdetection.h.

### Framework Integration

- Fix random file dialog not showing up problem. (bug 350758)

### KActivities

- Using a custom matching function instead of sqlite's glob. (bug 352574)
- Fixed problem with adding a new resource to the model

### KCodecs

- Fix crash in UnicodeGroupProber::HandleData with short strings

### KConfig

- Mark kconfig-compiler as non-gui tool

### KCoreAddons

- KShell::splitArgs: only ASCII space is a separator, not unicode space U+3000 (bug 345140)
- KDirWatch: fix crash when a global static destructor uses KDirWatch::self() (bug 353080)
- Fix crash when KDirWatch is used in Q_GLOBAL_STATIC.
- KDirWatch: fix thread safety
- Clarify how to set KAboutData constructor arguments.

### KCrash

- KCrash: pass cwd to kdeinit when auto-restarting the app via kdeinit. (bug 337760)
- Add KCrash::initialize() so that apps and the platform plugin can explicitly enable KCrash.
- Disable ASAN if enabled

### KDeclarative

- Small improvements in ColumnProxyModel
- Make it possible for applications to know path to homeDir
- move EventForge from the desktop containment
- Provide enabled property for QIconItem.

### KDED

- kded: simplify logic around sycoca; just call ensureCacheValid.

### KDELibs 4 Support

- Call newInstance from the child on first invocation
- Use kdewin defines.
- Don't try to find X11 on WIN32
- cmake: Fix taglib version check in FindTaglib.cmake.

### KDesignerPlugin

- Qt moc can't handle macros (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- implement windows usermetadata

### KDE GUI Addons

- Not looking for X11/XCB makes sense also for WIN32

### KHTML

- Replace std::auto_ptr with std::unique_ptr
- khtml-filter: Discard rules containing special adblock features that we do not handle yet.
- khtml-filter: Code reorder, no functional changes.
- khtml-filter: Ignore regexp with options as we do not support them.
- khtml-filter: Fix detection of adblock options delimiter.
- khtml-filter: Clean up from trailing white spaces.
- khtml-filter: Do not discard lines starting with '&amp;' as it is not a special adblock char.

### KI18n

- remove strict iterators for msvc to make ki18n build

### KIO

- KFileWidget: parent argument should default to 0 like in all widgets.
- Make sure the size of the byte array we just dumped into the struct is big enough before calculating the targetInfo, otherwise we're accessing memory that doesn't belong to us
- Fix Qurl usage when calling QFileDialog::getExistingDirectory()
- Refresh Solid's device list before querying in kio_trash
- Allow trash: in addition to trash:/ as url for listDir (calls listRoot) (bug 353181)
- KProtocolManager: fix deadlock when using EnvVarProxy. (bug 350890)
- Don't try to find X11 on WIN32
- KBuildSycocaProgressDialog: use Qt's builtin busy indicator. (bug 158672)
- KBuildSycocaProgressDialog: run kbuildsycoca5 with QProcess.
- KPropertiesDialog: fix for ~/.local being a symlink, compare canonical paths
- Add support for network shares in kio_trash (bug 177023)
- Connect to the signals of QDialogButtonBox, not QDialog (bug 352770)
- Cookies KCM: update DBus names for kded5
- Use JSON files directly instead of kcoreaddons_desktop_to_json()

### KNotification

- Don't send notification update signal twice
- Reparse notification config only when it changed
- Don't try to find X11 on WIN32

### KNotifyConfig

- Change method for loading defaults
- Send the appname whose config was updated along with the DBus signal
- Add method to revert kconfigwidget to defaults
- Don't sync the config n times when saving

### KService

- Use largest timestamp in subdirectory as resource directory timestamp.
- KSycoca: store mtime for every source dir, to detect changes. (bug 353036)
- KServiceTypeProfile: remove unnecessary factory creation. (bug 353360)
- Simplify and speed up KServiceTest::initTestCase.
- make install name of applications.menu file a cached cmake variable
- KSycoca: ensureCacheValid() should create the db if it doesn't exist
- KSycoca: make global database work after the recent timestamp check code
- KSycoca: change DB filename to include language and sha1 of the dirs it's built from.
- KSycoca: make ensureCacheValid() part of the public API.
- KSycoca: add a q pointer to remove more singleton usage
- KSycoca: remove all self() methods for factories, store them in KSycoca instead.
- KBuildSycoca: remove writing of the ksycoca5stamp file.
- KBuildSycoca: use qCWarning rather than fprintf(stderr, ...) or qWarning
- KSycoca: rebuild ksycoca in process rather than executing kbuildsycoca5
- KSycoca: move all of the kbuildsycoca code into the lib, except for main().
- KSycoca optimization: only watch the file if the app connects to databaseChanged()
- Fix memory leaks in the KBuildSycoca class
- KSycoca: replace DBus notification with file watching using KDirWatch.
- kbuildsycoca: deprecate option --nosignal.
- KBuildSycoca: replace dbus-based locking with a lock file.
- Do not crash when encountering invalid plugin info.
- Rename headers to _p.h in preparation for move to kservice library.
- Move checkGlobalHeader() within KBuildSycoca::recreate().
- Remove code for --checkstamps and --nocheckfiles.

### KTextEditor

- validate more regexp
- fix regexps in HL files (bug 352662)
- sync ocaml HL with state of https://code.google.com/p/vincent-hugot-projects/ before google code is down, some small bugfixes
- add word-break (bug 352258)
- validate line before calling folding stuff (bug 339894)
- Fix Kate word count issues by listening to DocumentPrivate instead of Document (bug 353258)
- Update Kconfig syntax highlighting: add new operators from Linux 4.2
- sync w/ KDE/4.14 kate branch
- minimap: Fix scrollbar handle not being drawn with scrollmarks off. (bug 352641)
- syntax: Add git-user option for kdesrc-buildrc

### KWallet Framework

- No longer automatically close on last use

### KWidgetsAddons

- Fix warning C4138 (MSVC): '*/' found outside of comment

### KWindowSystem

- Perform deep copy of QByteArray get_stringlist_reply
- Allow interacting with multiple X servers in the NETWM classes.
- [xcb] Consider mods in KKeyServer as initialized on platform != x11
- Change KKeyserver (x11) to categorized logging

### KXMLGUI

- Make it possible to import/export shortcut schemes symmetrically

### NetworkManagerQt

- Fix introspections, LastSeen should be in AccessPoint and not in ActiveConnection

### Plasma Framework

- Make tooltip dialog hidden on the cursor entering the inactive ToolTipArea
- if the desktop file has Icon=/foo.svgz use that file from package
- add a "screenshot" file type in packages
- consider devicepixelration in standalone scrollbar
- no hover effect on touchscreen+mobile
- Use lineedit svg margins in sizeHint calculation
- Don't fade animate icon in plasma tooltips
- Fix eliding button text
- Context menus of applets within a panel no longer overlap the applet
- Simplify getting associated apps list in AssociatedApplicationManager

### Sonnet

- Fix hunspell plugin ID for proper loading
- support static compilation on windows, add windows libreoffice hunspell dict path
- Do not assume UTF-8 encoded Hunspell dictionaries. (bug 353133)
- fix Highlighter::setCurrentLanguage() for the case when previous language was invalid (bug 349151)
- support /usr/share/hunspell as dict location
- NSSpellChecker-based plugin

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.