---
aliases:
- ../../kde-frameworks-5.73.0
date: 2020-08-01
layout: framework
title: Release of KDE Frameworks 5.73.0
version: 5.73.0
---

August 01, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.73.0.

KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Breeze Icons

+ Center 16px help-about and help-whatsthis
+ Add kirigami-gallery icon
+ Add Kontrast icon
+ Add applications/pkcs12 mime type icon
+ Make breeze-dark audio-volume icons match breeze
+ Make microphone style more consistent with other audio icons and pixel aligned
+ Use 35% opacity for faded waves in audio-volume icons and make mute waves faded
+ Make audio-off and audio-volume-muted the same
+ Add snap-angle icon
+ Fix 22px application-x-ms-shortcut linking to a 16px icon
+ Fix inconsistencies in mimetype icons between light/dark versions
+ Fix invisible light/dark differences in vscode and wayland icons
+ Add document-replace icon (Like for Overwrite action)
+ Add template for making python scripts that edit SVGs
+ Add SMART status icon (bug 423997)
+ Add "task-recurring" and "appointment-recurring" icons (bug 397996)

### Extra CMake Modules

+ Add ecm_generate_dbus_service_file
+ Add ecm_install_configured_file
+ Export Wayland_DATADIR

### KActivitiesStats

+ Ignore BoostConfig.cmake if present (bug 424799)

### KCMUtils

+ Support highlight indicator for QWidget and QtQuick based module
+ Add method to clear plugin selector (bug 382136)

### KConfig

+ Update sGlobalFileName when QStandardPaths TestMode is toggled
+ API dox: state explicitly expected encoding for KConfig key &amp; group names

### KConfigWidgets

+ KCModule: Indicate when a setting has been changed from the default value
+ When resetting to system default do not use the standard palette of the style

### KCoreAddons

+ Introduce KRandom::shuffle(container)

### KDeclarative

+ SettingStateBinding : expose whether non default highlight is enabled
+ Make sure KF5CoreAddons is installed before using KF5Declarative
+ Add KF5::CoreAddons to public interface for KF5::QuickAddons
+ Introduce SettingState* elements to ease KCM writing
+ support config notifications in configpropertymap

### KDE GUI Addons

+ Move KCursorSaver from libkdepim, improved

### KImageFormats

+ Adapt license to LGPL-2.0-or-later

### KIO

+ KFilterCombo: application/octet-stream is also hasAllFilesFilter
+ Introduce OpenOrExecuteFileInterface for handling opening executables
+ RenameDialog: Show if files are identical (bug 412662)
+ [rename dialog] Port Overwrite button to KStandardGuiItem::Overwrite (bug 424414)
+ Show up to three file item actions inline, not just one (bug 424281)
+ KFileFilterCombo: Show extensions if there's repeated mime comment
+ [KUrlCompletion] Don't append / to completed folders
+ [Properties] Add SHA512 algorithm to checksums widget (bug 423739)
+ [WebDav] Fix copies that include overwrites for the webdav slave (bug 422238)

### Kirigami

+ Support action visibility in GlobalDrawer
+ Introduce FlexColumn component
+ Mobile layout optimizations
+ A layer must cover the tabbar as well
+ PageRouter: actually use preloaded pages when pushing
+ Improve (Abstract)ApplicationItem docs
+ Fix segfault on PageRouter teardown
+ Make ScrollablePage scrollable with keyboard
+ Improve accessibility of the Kirigami input fields
+ Fix the static build
+ PageRouter: Add convenience APIs for otherwise manual tasks
+ Introduce PageRouter lifecycle APIs
+ Force a low Z order of ShadowedRectangle's software fallback

### KItemModels

+ KSelectionProxyModel: allow using the model with new-style connect
+ KRearrangeColumnsProxyModel: fix hasChildren() when no columns are set up yet

### KNewStuff

+ [QtQuick dialog] use update icon that exists (bug 424892)
+ [QtQuick GHNS dialog] Improve combobox labels
+ Don't show the downloaditem selector for only one downloadable item
+ Rejig the layout a little, for a more balanced look
+ Fix layout for the EntryDetails status card
+ Only link to Core, not the quick plugin (because that won't work)
+ Some prettification of the welcome screen (and don't use qml dialogs)
+ Use the dialog when a knsrc file is passed, otherwise not
+ Remove the button from the main window
+ Add a dialog (for showing the dialog directly when passed a file)
+ Add the new bits to the main application
+ Add a simple (at least for now) model for showing knsrc files
+ Move the main qml file into a qrc
+ Turn the KNewStuff Dialog test tool into a proper tool
+ Allow to delete updatable entries (bug 260836)
+ Do not focus first element automatically (bug 417843)
+ Fix display of Details and Uninstall button in Tiles view mode (bug 418034)
+ Fix moving buttons when search text is inserted (bug 406993)
+ Fix missing parameter for translated string
+ Fix details install dropdown in QWidgets dialog (bug 369561)
+ Add tooltips for different view modes in QML dialog
+ Set entry to uninstalled if installation fails (bug 422864)

### KQuickCharts

+ Also take model properties into account when using ModelHistorySource

### KRunner

+ Implement KConfig watcher for enabled plugins and runner KCMs (bug 421426)
+ Do not remove virtual method from build (bug 423003)
+ Deprecate AbstractRunner::dataEngine(...)
+ Fix disabled runners and runner config for plasmoid
+ Delay emitting metadata porting warnings until KF 5.75

### KService

+ Add overload to invoke terminal with ENV variables (bug 409107)

### KTextEditor

+ Add icons to all buttons of file modified message (bug 423061)
+ Use the canonical docs.kde.org URLs

### KWallet Framework

+ use better Name and follow HIG
+ Mark API as deprecated also in D-Bus interface description
+ Add copy of org.kde.KWallet.xml without deprecated API

### KWayland

+ plasma-window-management:  Adapt to changes in the protocol
+ PlasmaWindowManagement: adopt changes in the protocol

### KWidgetsAddons

+ KMultiTabBar: paint tab icons active on mouseover
+ Fix KMultiTabBar to paint the icon shifted on down/checked by style design
+ Use new overwrite icon for Overwrite GUI Item (bug 406563)

### KXMLGUI

+ Make KXmlGuiVersionHandler::findVersionNumber public in KXMLGUIClient

### NetworkManagerQt

+ Remove secrets logging

### Plasma Framework

+ Set cpp ownership on the units instance
+ Add some missing PlasmaCore imports
+ Remove usage of context properties for theme and units
+ PC3: Improve look of the Menu
+ Adjust audio icons again to match breeze-icons
+ Make showTooltip() invokable from QML
+ [PlasmaComponents3] Honor icon.[name|source] property
+ Filter on formfactors if set
+ Update mute icon style to match breeze-icons
+ Remove broken metatype registration for types missing the "*" in the name
+ Use 35% opacity for faded elements in the network icons
+ Don't show Plasma dialogs in task switchers (bug 419239)
+ Correct QT Bug URL for font rendering hack
+ Don't use hand cursor because it is not consistent
+ [ExpandableListItem] use standard button sizes
+ [PlasmaComponents3] Show ToolButton focus effect and omit shadow when flat
+ Unify height of PC3 buttons, TextFields, and Comboboxes
+ [PlasmaComponents3] Add missing features to TextField
+ [ExpandableListItem] Fix silly error
+ Rewrite button.svg to make it easier to understand
+ Copy DataEngine relays before iterating (bug 423081)
+ Make signal strength in network icons more visible (bug 423843)

### QQC2StyleBridge

+ Use "raised" style for non-flat toolbuttons
+ Only reserve space for the icon in toolbutton if we actually have an icon
+ Support showing a menu arrow on tool buttons
+ Really fix menu separator height on high DPI
+ Update Mainpage.dox
+ Set height of MenuSeparator properly (bug 423653)

### Solid

+ Clear m_deviceCache before introspecting again (bug 416495)

### Syntax Highlighting

+ Convert DetectChar to proper Detect2Chars
+ Mathematica: some improvements
+ Doxygen: fix some errors ; DoxygenLua: starts only with --- or --!
+ AutoHotkey: complete rewrite
+ Nim: fix comments
+ Add syntax highlighting for Nim
+ Scheme: fix identifier
+ Scheme: add datum comment, nested-comment and other improvements
+ Replace some RegExp by AnyChar, DetectChar, Detect2Chars or StringDetect
+ language.xsd: remove HlCFloat and introduce char type
+ KConfig: fix $(...) and operators + some improvements
+ ISO C++: fix performance in highlighting numbers
+ Lua: attribute with Lua54 and some other improvements
+ replace RegExpr=[.]{1,1} by DetectChar
+ Add PureScript highlighting based on Haskell rules. It gives pretty good approximation and starting point for PureScript
+ README.md: use the canonical docs.kde.org URLs

### Security information


The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB