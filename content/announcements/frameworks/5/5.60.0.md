---
aliases:
- ../../kde-frameworks-5.60.0
date: 2019-07-13
description: KDE Ships Frameworks 5.60.0.
layout: framework
title: Release of KDE Frameworks 5.60.0
version: 5.60.0
---

July 13, 2019. KDE today announces the release of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.60.0.

KDE Frameworks are over 70 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see the <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### General

- Qt &gt;= 5.11 is now required, now that Qt 5.13 has been released.

### Baloo

- [QueryTest] Test if independent phrases are really independent
- [TermGenerator] Insert an empty position between independent terms
- [QueryTest] Restructure tests to allow easier extension
- [TermGenerator] Leave single term phrases out of the PositionDB
- [TermGenerator] Do Term truncation prior to UTF-8 conversion
- [PostingIterator] Move positions() method to VectorPositionInfoIterator
- [TermGenerator] Use UTF-8 ByteArray for termList
- [WriteTransactionTest] Clear mixup of QString and QByteArray
- [experimental/BalooDB] Fix trivial 0 / nullptr warning
- [PositionDbTest] Fix trivial memleak in test
- [PendingFileQueueTest] Verify create + delete do not emit extra events
- [PendingFileQueueTest] Verify delete + create actually works
- [PendingFileQueue] Avoid delete + create / create + delete race
- [PendingFileQueueTest] Use synthetic timer events to speedup test
- [XAttrIndexer] Update DocumentTime when XAttrs are updated
- [PendingFileQueueTest] Shorten timeouts, verify tracking time
- [PendingFileQueue] Use more accurate calculation of remaining time
- [ModifiedFileIndexer] Use correct mimetype for folders, delay until needed
- [NewFileIndexer] Omit symlinks from the index
- [ModifiedFileIndexer] Avoid shadowing XAttr changes by content changes
- [NewFileIndexer] Use correct mimetype for folders, check excludeFolders
- [UnindexedFileIndexer] Pick up comment, tags and rating changes
- [UnindexedFileIndexer] Skip filetime checks for new files
- [DocumentUrlDB] Avoid manipulation of the whole tree on trivial rename
- [DocumentUrlDB] Catch invalid URLs early
- [DocumentUrlDB] Remove unused 'rename' method
- [balooctl] Streamline indexer control commands
- [Transaction] Replace template for functor with std::function
- [FirstRunIndexer] Use correct mimetype for folders
- Move invariant IndexingLevel out of the loop
- [BasicIndexingJob] Skip lookup of baloo document type for directories
- [FileIndexScheduler] Ensure indexer is not run in suspended state
- [PowerStateMonitor] Be conservative when determining power state
- [FileIndexScheduler] Stop the indexer when quit() is called via DBus
- Avoid container detach in a few places
- Do not try to append to QLatin1String
- Disable valgrind detection when compiling with MSVC
- [FilteredDirIterator] Combine all suffixes into one large RegExp
- [FilteredDirIterator] Avoid RegExp overhead for exact matches
- [UnindexedFileIterator] Delay mimetype determination until it is needed
- [UnindexedFileIndexer] Do not try to add nonexistent file to index
- Detect valgrind, avoid database removal when using valgrind
- [UnindexedFileIndexer] Loop optimizations (avoid detach, invariants)
- Delay running UnindexedFileIndexer and IndexCleaner
- [FileIndexScheduler] Add new state for Idle on battery
- [FileIndexScheduler] Postpone housekeeping tasks while on battery
- [FileIndexScheduler] Avoid emitting state changes multiple times
- [balooctl] Clarify and extend status output

### BluezQt

- Add MediaTransport API
- Add LE Advertising and GATT APIs

### Breeze Icons

- Add id="current-color-scheme" to collapse-all icons (bug 409546)
- Add disk-quota icons (bug 389311)
- Symlink install to edit-download
- Change joystick settings icon to game controller (bug 406679)
- Add edit-select-text, make 16px draw-text like 22px
- Update KBruch icon
- Add help-donate-[currency] icons
- Make Breeze Dark use same Kolourpaint icon as Breeze
- Add 22px notifications icons

### KActivitiesStats

- Fix a crash in KactivityTestApp when Result has strings with non-ASCII

### KArchive

- Do not crash if the inner file wants to be bigger than QByteArray max size

### KCoreAddons

- KPluginMetaData: use Q_DECLARE_METATYPE

### KDeclarative

- [GridDelegate] Fix gaps in corners of thumbnailArea highlight
- get rid of blockSignals
- [KCM GridDelegate] Silence warning
- [KCM GridDelegate] Take into account implicitCellHeight for inner delegate height
- Fix GridDelegate icon
- Fix fragile comparison to i18n("None") and describe behavior in docs (bug 407999)

### KDE WebKit

- Downgrade KDEWebKit from Tier 3 to Porting Aids

### KDocTools

- Update pt-BR user.entities

### KFileMetaData

- Fix extracting of some properties to match what was written (bug 408532)
- Use debugging category in taglib extractor/writer
- Format photo exposure bias value (bug 343273)
- fix property name
- Remove photo prefix from every exif property name (bug 343273)
- Rename ImageMake and ImageModel properties (bug 343273)
- [UserMetaData] Add method to query which attributes are set
- Format focal length as millimeter
- Format photo exposure time as rational when applicable (bug 343273)
- Enable usermetadatawritertest for all UNIXes, not only Linux
- Format the aperture values as F numbers (bug 343273)

### KDE GUI Addons

- KModifierKeyInfo: we are sharing the internal implementation
- Remove double look-ups
- Move to runtime the decision to use x11 or not

### KHolidays

- Update UK Early May bank holiday for 2020 (bug 409189)
- Fix ISO code for Hesse / Germany

### KImageFormats

- QImage::byteCount -&gt; QImage::sizeInByes

### KIO

- Fix KFileItemTest::testIconNameForUrl test to reflect different icon name
- Fix i18n number-of-arguments error in knewfilemenu warning message
- [ftp] Fix wrong access time in Ftp::ftpCopyGet() (bug 374420)
- [CopyJob] Batch reporting processed amount
- [CopyJob] Report results after finishing copy (bug 407656)
- Move redundant logic in KIO::iconNameForUrl() into KFileItem::iconName() (bug 356045)
- Install KFileCustomDialog
- [Places panel] Don't show Root by default
- Downgrade "Could not change permissions" dialog box to a qWarning
- O_PATH is only available on linux. To prevent the compiler from throwing an error
- Show feedback inline when creating new files or folders
- Auth Support: Drop privileges if target is not owned by root
- [copyjob] Only set modification time if the kio-slave provided it (bug 374420)
- Cancel privilege operation for read-only target with the current user as owner
- Add KProtocolInfo::defaultMimetype
- Always save view settings when switching from one view mode to another
- Restore exclusive group for sorting menu items
- Dolphin-style view modes in the file dialog (bug 86838)
- kio_ftp: improve error handling when copying to FTP fails
- kioexec: change the scary debug messages for delayed deletion

### Kirigami

- [ActionTextField] Make action glow on press
- support text mode and position
- mouseover effect for breadcrumb on desktop
- enforce a minimum height of 2 gridunits
- Set SwipeListItem implicitHeight to be the maximum of content and actions
- Hide tooltip when PrivateActionToolButton is pressed
- Remove accidentally slipped back traces of cmake code for Plasma style
- ColumnView::itemAt
- force breeze-internal if no theme is specified
- correct navigation on left pinned page
- keep track of the space covered by pinned pages
- show a separator when in left sidebar mode
- in single column mode, pin has no effect
- first semi working prototype of pinning

### KJobWidgets

- [KUiServerJobTracker] Handle ownership change

### KNewStuff

- [kmoretools] Add icons to download and install actions

### KNotification

- Don't search for phonon on Android

### KParts

- Add profile support interface for TerminalInterface

### KRunner

- Don't delay emission of matchesChanged indefinitely

### KService

- Add X-Flatpak-RenamedFrom as recognized key

### KTextEditor

- fix goto line centering (bug 408418)
- Fix bookmark icon display on icon border with low dpi
- Fix action "Show Icon Border" to toggle border again
- Fix empty pages in print preview and lines printed twice (bug 348598)
- remove no longer used header
- fix autoscrolling down speed (bug 408874)
- Add default variables for variables interface
- Make automatic spellcheck work after reloading a document (bug 408291)
- raise default line length limit to 10000
- WIP:Disable highlighting after 512 characters on a line
- KateModeMenuList: move to QListView

### KWayland

- Include a description
- Proof of concept of a wayland protocol to allow the keystate dataengine to work

### KWidgetsAddons

- KPasswordLineEdit now correctly inherits its QLineEdit's focusPolicy (bug 398275)
- Replace "Details" button with KCollapsibleGroupBox

### Plasma Framework

- [Svg] Fix porting error from QRegExp::exactMatch
- ContainmentAction: Fix loading from KPlugin
- [TabBar] Remove exterior margins
- Applet, DataEngine and containment listing methods inPlasma::PluginLoader no longer filters the plugins with X-KDE-ParentAppprovided when empty string is passed
- Make pinch in calendar work again
- Add disk-quota icons (bug 403506)
- Make Plasma::Svg::elementRect a bit leaner
- Automatically set version of desktopthemes packages to KF5_VERSION
- Don't notify about changing to the same state it was at
- Fix the alignment of the label of the toolbutton
- [PlasmaComponents3] Vertically center button text as well

### Purpose

- Change initial size of the config dialog
- Improve Job Dialog buttons' icons and text
- Fix translation of actiondisplay
- Don't show error message if sharing is cancelled by the user
- Fix warning when reading plugin metadata
- Redesign config pages
- ECMPackageConfigHelpers -&gt; CMakePackageConfigHelpers
- phabricator: Fix fallthrough in switch

### QQC2StyleBridge

- Show shortcut in menu item when specified (bug 405541)
- Add MenuSeparator
- Fix ToolButton remaining in a pressed state after press
- [ToolButton] Pass custom icon size to StyleItem
- honor visibility policy (bug 407014)

### Solid

- [Fstab] Select appropriate icon for home or root directory
- [Fstab] Show mounted "overlay" filesystems
- [UDev Backend] Narrow device queried for

### Syntax Highlighting

- Fortran: relicense to MIT
- Improve the Fortran fixed format syntax highlighting
- Fortran: implement free &amp; fixed formats
- Fix CMake COMMAND nested paren highlighting
- Add more keywords and also support rr in gdb highlighter
- Detect comment lines early in GDB highlighter
- AppArmor: update syntax
- Julia: update syntax and add constants keywords (bug 403901)
- CMake: Highlight the standard CMake environment variables
- Add syntax definition for ninja build
- CMake: Support for 3.15 features
- Jam: various improvements and fixes
- Lua: update for Lua54 and end of function as Keyword rather than Control
- C++: update for C++20
- debchangelog: add Eoan Ermine

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB