---
aliases:
- ../../kde-frameworks-5.31.0
date: 2017-02-11
description: KDE Ships Frameworks 5.31.0
layout: framework
title: Release of KDE Frameworks 5.31.0
version: 5.31.0
---

{{<figure src="/announcements/frameworks/5-tp/KDE_QT.jpg" >}}

February 11, 2017. KDE today announces the release of KDE Frameworks 5.31.0.

KDE Frameworks are 70 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### General

Many modules now have python bindings.

### Attica

- add support for display_name in categories

### Breeze Icons

- too many icon changes to list them here

### Extra CMake Modules

- Enable -Wsuggest-override for g++ &gt;= 5.0.0
- Pass -fno-operator-names when supported
- ecm_add_app_icon : ignore SVG files silently when unsupported
- Bindings: Many fixes and improvements

### Framework Integration

- Support some of the KNSCore questions using notifications

### KArchive

- Fix KCompressionDevice (seeking) to work with Qt &gt;= 5.7

### KAuth

- Update most of the examples, drop outdated ones

### KConfig

- Fix linking on Windows: don't link kentrymaptest to KConfigCore

### KConfigWidgets

- Do nothing in ShowMenubarActionFilter::updateAction if there are no menubars

### KCoreAddons

- Fix Bug 363427 - unsafe characters incorrectly parsed as part of URL (bug 363427)
- kformat: Make it possible to properly translate relative dates (bug 335106)
- KAboutData: Document that bug email address can also be a URL

### KDeclarative

- [IconDialog] Set proper icons group
- [QuickViewSharedEngine] Use setSize instead of setWidth/setHeight

### KDELibs 4 Support

- Sync KDE4Defaults.cmake from kdelibs
- Fix HAVE_TRUNC cmake check

### KEmoticons

- KEmoticons: use DBus to notify running processes of changes made in the KCM
- KEmoticons: major performance improvement

### KIconThemes

- KIconEngine: Center icon in requested rect

### KIO

- Add KUrlRequester::setMimeTypeFilters
- Fix parsing of directories listing on a specific ftp server (bug 375610)
- preserve group/owner on file copy (bug 103331)
- KRun: deprecate runUrl() in favor of runUrl() with RunFlags
- kssl: Ensure user certificate directory has been created before use (bug 342958)

### KItemViews

- Apply filter to proxy immediately

### KNewStuff

- Make it possible to adopt resources, mostly for system-wide settings
- Don't fail when moving to the temp directory when installing
- Deprecate the security class
- Don't block when running the post-install command (bug 375287)
- [KNS] Take into account the distribution type
- Don't ask if we're getting the file in /tmp

### KNotification

- Re-add logging notifications to files (bug 363138)
- Mark non-persistent notifications as transient
- Support "default actions"

### KPackage Framework

- Don't generate appdata if it's marked as NoDisplay
- Fix listing when the requested path is absolute
- fix handling of archives with a folder in them (bug 374782)

### KTextEditor

- fix minimap rendering for HiDPI envs

### KWidgetsAddons

- Add methods to hide the reveal password action
- KToolTipWidget: don't take ownership of the content widget
- KToolTipWidget: hide immediately if content gets destroyed
- Fix focus override in KCollapsibleGroupBox
- Fix warning when destructing a KPixmapSequenceWidget
- Install also CamelCase forward headers for classes from multi-class headers
- KFontRequester: Find the nearest match for a missing font (bug 286260)

### KWindowSystem

- Allow Tab as being modified by Shift (bug 368581)

### KXMLGUI

- Bug reporter: Allow a URL (not just an email address) for custom reporting
- Skip empty shortcuts on ambiguity check

### Plasma Framework

- [Containment Interface] No need for values() as contains() looks up keys
- Dialog: Hide when focus changes to ConfigView with hideOnWindowDeactivate
- [PlasmaComponents Menu] Add maximumWidth property
- Missing icon when connected to openvpn via bluetooth network (bug 366165)
- Make sure we display enabled ListItem on hover
- make all heights in the calendar header to be even (bug 375318)
- fix color styling in network plasma icon (bug 373172)
- Set wrapMode to Text.WrapAnywhere (bug 375141)
- update kalarm icon (bug 362631)
- correctly forward status from applets to containment (bug 372062)
- Use KPlugin to load Calendar plugins
- use the highlight color for selected text (bug 374140)
- [Icon Item] Round size we want to load a pixmap in
- portrait prop is not relevant when there is no text (bug 374815)
- Fix the renderType properties for various components

### Solid

- Work round DBus property fetching bug (bug 345871)
- Treat no passphrase as Solid::UserCanceled error

### Sonnet

- Added Greek trigram data file
- Fix segfault in trigrams generation and expose MAXGRAMS constant in the header
- Look for non-versioned libhunspell.so, should be more future proof

### Syntax Highlighting

- C++ highlighting: update to Qt 5.8

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.