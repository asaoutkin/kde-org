---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
description: KDE Ships Frameworks 5.18.0
layout: framework
title: Release of KDE Frameworks 5.18.0
version: 5.18.0
---

January 09, 2016. KDE today announces the release of KDE Frameworks 5.18.0.

KDE Frameworks are 60 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Baloo

- Fix several issue of mtime related search
- PostingDB Iter: Do not assert on MDB_NOTFOUND
- Balooctl status: Avoid showing 'Content indexing' about folders
- StatusCommand: Show the correct status for folders
- SearchStore: Gracefully handle empty term values (bug 356176)

### Breeze Icons

- icon updates and additions
- 22px size status icons for 32px too as you need it in the system tray
- Changed Fixed to Scalable value to 32px folders in Breeze Dark

### Extra CMake Modules

- Make the KAppTemplate CMake module global
- Silence CMP0063 warnings with KDECompilerSettings
- ECMQtDeclareLoggingCategory: Include &lt;QDebug&gt; with the generated file
- Fix CMP0054 warnings

### KActivities

- Streamlined the QML loading for KCM (bug 356832)
- Work-around for the Qt SQL bug that does not clean up connections properly (bug 348194)
- Merged a plugin that executes applications on activity state change
- Port from KService to KPluginLoader
- Port plugins to use kcoreaddons_desktop_to_json()

### KBookmarks

- Fully initialize DynMenuInfo in return value

### KCMUtils

- KPluginSelector::addPlugins: fix assert if 'config' parameter is default (bug 352471)

### KCodecs

- Avoid deliberately overflowing a full buffer

### KConfig

- Ensure group is unescaped properly in kconf_update

### KCoreAddons

- Add KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)
- Add KPluginMetaData::copyrightText(), extraInformation() and otherContributors()
- Add KPluginMetaData::translators() and KAboutPerson::fromJson()
- Fix use-after-free in desktop file parser
- Make KPluginMetaData constructible from a json path
- desktoptojson: make missing service type file an error for the binary
- make calling kcoreaddons_add_plugin without SOURCES an error

### KDBusAddons

- Adapt to Qt 5.6's dbus-in-secondary-thread

### KDeclarative

- [DragArea] Add dragActive property
- [KQuickControlsAddons MimeDatabase] Expose QMimeType comment

### KDED

- kded: adapt to Qt 5.6's threaded dbus: messageFilter must trigger module loading in the main thread

### KDELibs 4 Support

- kdelibs4support requires kded (for kdedmodule.desktop)
- Fix CMP0064 warning by setting policy CMP0054 to NEW
- Don't export symbols that also exist in KWidgetsAddons

### KDESU

- Don't leak fd when creating socket

### KHTML

- Windows: remove kdewin dependency

### KI18n

- Document the first argument rule for plurals in QML
- Reduce unwanted type changes
- Make it possible to use doubles as index for i18np*() calls in QML

### KIO

- Fix kiod for Qt 5.6's threaded dbus: messageFilter must wait until the module is loaded before returning
- Change the error code when pasting/moving into a subdirectory
- Fix emptyTrash blocked issue
- Fix wrong button in KUrlNavigator for remote URLs
- KUrlComboBox: fix returning an absolute path from urls()
- kiod: disable session management
- Add autocompletion for '.' input which offers all hidden files/folders* (bug 354981)
- ktelnetservice: fix off by one in argc check, patch by Steven Bromley

### KNotification

- [Notify By Popup] Send along event ID
- Set default non-empty reason for screen saver inhibition; (bug 334525)
- Add a hint to skip notifications grouping (bug 356653)

### KNotifyConfig

- [KNotifyConfigWidget] Allow selecting a specific event

### Package Framework

- Make it possible to provide the metadata in json

### KPeople

- Fix possible double deletion in DeclarativePersonData

### KTextEditor

- Syntax h/l for pli: builtin functions added, expandable regions added

### KWallet Framework

- kwalletd: Fix FILE* leak

### KWindowSystem

- Add xcb variant for static KStartupInfo::sendFoo methods

### NetworkManagerQt

- make it work with older NM versions

### Plasma Framework

- [ToolButtonStyle] Always indicate activeFocus
- Use the SkipGrouping flag for the "widget deleted" notification (bug 356653)
- Deal properly with symlinks in path to packages
- Add HiddenStatus for plasmoid self-hiding
- Stop redirecting windows when item is disabled or hidden. (bug 356938)
- Don't emit statusChanged if it hasn't changed
- Fix element ids for east orientation
- Containment: Don't emit appletCreated with null applet (bug 356428)
- [Containment Interface] Fix erratic high precision scrolling
- Read KPluginMetada's property X-Plasma-ComponentTypes as a stringlist
- [Window Thumbnails] Don't crash if Composite is disabled
- Let containments override CompactApplet.qml

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.