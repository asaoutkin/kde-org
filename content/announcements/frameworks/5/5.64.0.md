---
aliases:
- ../../kde-frameworks-5.64.0
date: 2019-11-10
description: KDE Ships Frameworks 5.64.0.
layout: framework
title: Release of KDE Frameworks 5.64.0
version: 5.64.0
---

November 10, 2019. KDE today announces the release of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.64.0.

KDE Frameworks are over 70 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see the <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Attica

- Add some std::move in setter functions

### Baloo

- Make it compile against qt5.15
- Use propertymap to store properties in Baloo::Result
- Add standalone conversion functions for PropertyMap to Json and vice versa
- [Database] Rework handling environment flags
- Replace recursion in FilteredDirIterator with loop iteration

### Breeze Icons

- Center-align the non-square 64px audio mimetype icons (bug 393550)
- Delete remnants of nepomuk icon
- Move colorful 32px help-about icon into actions (bug 396626)
- Improve draw icons (bug 399665)
- Delete nepomuk icon
- Fill middle mouse button area
- Add folder-recent, extend hand of clock in folder-temp
- Use a more correct and appropriate visual metaphor for "get hot new stuff" icon (bug 400500)
- Update elisa icon
- Use css instead of scss as output format
- Fix incorrect rendering of 22px edit-opacity icon
- Add edit-opacity icons (bug 408283)
- Icons for windy weather (bug 412718)
- Fix incorrect margins in 16/22px media icons
- Use the text rather than highlight color for rating/star emblem
- Add draw-arrow icons (bug 408283)
- Add draw-highlight action icons (bug 408283)
- Add PATH/LD_LIBRARY_PATH to qrcAlias invocation
- Add applications-network icon for renaming Internet category to Network
- Add edit-line-width icons (bug 408283)

### Extra CMake Modules

- Don't set C/C++ standards if already set
- Use modern way to set the C/CXX standard
- Raise CMake requirements to 3.5
- ECMAddQch: support PREDEFINED_MACROS/BLANK_MACROS with blanks &amp; quotes

### Framework Integration

- Add standard icons to support to all entries in QDialogButtonBox (bug 398973)
- ensure winId() not called on non-native widgets (bug 412675)

### KActivitiesStats

- tests: fix macos build failure
- Windows MSVC compile fix
- Add a utility accessor to get a QUrl from a ResultSet::Result

### KArchive

- Fix memory leak in KXzFilter::init
- Fix null pointer reference when extraction fails
- decodeBCJ2: Fix assert with broken files
- KXzFilter::Private: remove unused props
- K7Zip: Fix memory use in readAndDecodePackedStreams

### KCalendarCore

- Add libical version too
- Explicitly define the Journal copy ctor

### KCMUtils

- Conditionally show navigation buttons in the header for multi-page KCMs
- don't use a custom header height (bug 404396)
- add extra include
- Fix memory leak of KQuickAddons::ConfigModule objects (bug 412998)
- [KCModuleLoader] Show error when QML fails to load

### KConfig

- kconfig_compiler: Move the KSharedConfig::Ptr when using them
- Make it compile against qt5.15 without deprecated method
- Expose isImmutable to introspection (e.g. QML)
- Add convenience for defaults/dirty states to KCoreConfigSkeleton
- Make kconfig_compiler generate ctors with the optional parent arg
- Make preferences() a public function

### KConfigWidgets

- Avoid overloading KCModule::changed

### KContacts

- Install translations

### KCoreAddons

- KProcessInfoList -- add proclist backend for FreeBSD

### KDeclarative

- Use compile time checked connect
- Make the settingChanged() slot protected
- Get KQuickAddons::ConfigModule to expose if we're in the defaults state
- Grab the keyboard when KeySequenceItem is recording
- Add ManagedConfigModule
- Remove outdated comment about [$e] expansion
- [ConfigModule] Expose mainUi component status and error string

### KDELibs 4 Support

- KLocale api docs: make it easier to find how to port code away from it

### KDocTools

- man: use &lt;arg&gt; instead of &lt;group&gt;

### KFileMetaData

- Fix crash in writer collection and cleanup

### KHTML

- Extend KHtmlView::print() to use a predefined QPrinter instance (bug 405011)

### KI18n

- Add KLocalizedString::untranslatedText
- Replace all qWarning and related calls with categorised logging

### KIconThemes

- Fix usage of the new deprecation macros for assignIconsToContextMenu
- Deprecate KIconTheme::assignIconsToContextMenu

### KIO

- Const &amp; signature of new introduced SlaveBase::configValue
- Port to the QSslError variant of KSslInfoDialog
- Port KSSLD internals from KSslError to QSslError
- Make non-ignorable SSL errors explicit
- auto-enable KIO_ASSERT_SLAVE_STATES also for from-git builds
- Port (most of) KSslInfoDialog from KSslError to QSslError
- kio_http: avoid double Content-Type and Depth when used by KDAV
- Port the KSSLD D-Bus interface from KSslError to QSslError
- Replace usage of SlaveBase::config()-&gt;readEntry by SlaveBase::configValue
- Remove two unused member variables using KSslError
- Avoid sending KDirNotify::emitFilesAdded when the emptytrashjob finishes
- Deprecate the KTcpSocket-based variant of SslUi::askIgnoreSslErrors
- Treat "application/x-ms-dos-executable" as executable on all platforms (bug 412694)
- Replace usage of SlaveBase::config() by SlaveBase::mapConfig()
- ftptest: replace logger-colors with logger
- [SlaveBase] Use QMap instead of KConfig to store ioslave config
- Port KSslErrorUiData to QSslError
- exclude ioslaves directory from api docs
- ftptest: mark overwrite without overwrite flag no longer failing
- ftptest: refactor the daemon startup into its own helper function
- [SslUi] Add api docs for askIgnoreSslErrors()
- consider ftpd not required for the time being
- port ftp slave to new error reporting system
- fix proxy setting loading
- Implement KSslCertificateRule with QSslError instead of KSslError
- Port (most of) the interface of KSslCertificateRule to QSslError
- Port KSslCertificateManager to QSslError
- Update test kfileplacesviewtest following D7446

### Kirigami

- Ensure that GlobalDrawer topContent always stays on top (bug 389533)
- highlight on mouseover only when mode than one page (bug 410673)
- Rename Okular Active to Okular Mobile
- items have active focus on tab when they aren't in a view (bug 407524)
- Allow contextualActions to flow into the header toolbar
- Fix incorrect Credits model for Kirigami.AboutPage
- Don't show context drawer if all actions are invisible
- Fix Kirigami template image
- keep containers devoid of deleted items
- limit size of the drag margins
- Fix showing menu toolbutton when no drawer is available
- Disable dragging global drawer when in menu mode
- Show menu items tooltip text
- Do not warn about LayoutDirection in SearchField
- Properly check enabled state of Action for ActionToolBar buttons
- Use MenuItem's action property directly in ActionMenuItem
- Allow the global drawer to become a menu if desired
- Be more explicit about action property types

### KItemViews

- [RFC] Unify style of new Kirigami.ListSectionHeader and CategoryDrawer

### KJS

- Better message for String.prototype.repeat(count) range errors
- Simplify parsing of numeric literals
- Parse JS binary literals
- Detect truncated hex and octal literals
- Support new standard way of specifying octal literals
- Collection of regression tests taken from khtmltests repository

### KNewStuff

- Ensure that the changedEntries property is correctly propagated
- Fix KNSCore::Cache fetching when initialising Engine (bug 408716)

### KNotification

- [KStatusNotifierItem] Allow left click when menu is null (bug 365105)
- Remove Growl support
- Add and enable Notification Center support in macOS

### KPeople

- Unbreak build: limit DISABLE_DEPRECATED for KService to &lt; 5.0

### KService

- Make it compile against qt5.15 without deprecated method

### KTextEditor

- KateModeMenuList: improve word wrap
- fix crash (bug 413474)
- more ok's arrived, more v2+
- more ok's arrived, more v2+
- add hint to copyright header
- no non-trivial code lines remain here beside for people that agreed to v2+ =&gt; v2+
- no non-trivial code lines remain here of authors no longer responding, v2+
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- more files v2+, as got OK by authors, see kwrite-devel@kde.org
- katedocument.h is v2+ already
- ok'd by loh.tar, sars, lgplv2+
- relicense to lgplv2+, got ok of sven + michal
- relicense to lgplv2+, got ok of sven + michal
- all files with SPDX-License-Identifier are lgplv2+
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- update license, dh is in lgplv2+ section of relicensecheck.pl
- update license, dh is in lgplv2+ section of relicensecheck.pl
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- lgplv2.1+ =&gt; lgplv2+
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- this header contains no gpl v2 only logic since long
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, add SPDX-License-Identifier
- clarify license, 'michalhumpula' =&gt; ['gplv23', 'lgplv23', 'gplv2+', 'lgplv2+', '+eV' ]
- add missing s (bug 413158)
- KateModeMenuList: force the vertical position above the button
- better: self-contained headers
- group includes for semantics
- sort includes

### KTextWidgets

- Remove call to no longer needed KIconTheme::assignIconsToContextMenu

### KWayland

- FakeInput: add support for keyboard key press and release
- Fix non-integer scale copy on creation of OutputChangeSet

### KXMLGUI

- fix default shortcut detection

### NetworkManagerQt

- Add support for SAE authentication used by WPA3

### Plasma Framework

- map disabledTextColor to ColorScope
- add DisabledTextColor to Theme
- [PC3/button] Elide text always
- Improve panel options menu entries
- [icons/media.svg] Add 16 &amp; 32px icons, update style
- [PlasmaComponents3] Fix checkable toolbutton background

### Prison

- Fix memory handling in datamatrix

### Purpose

- i18n: Add ellipsis to action items (X-Purpose-ActionDisplay)

### QQC2StyleBridge

- Do not assign combobox currentIndex as it breaks binding
- Listen to the application style changing

### Solid

- Don't build static library when BUILD_TESTING=OFF

### Syntax Highlighting

- VHDL: all keywords are insensitive (bug 413409)
- Add string escape characters to PowerShell syntax
- Modelines: fix end of comment
- Meson: more built-in functions and add built-in member functions
- debchangelog: add Focal Fossa
- Updates from CMake 3.16
- Meson: Add a comment section for comment/uncomment with Kate
- TypeScript: update grammar and fixes

### ThreadWeaver

- Make it compile against qt5.15

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB