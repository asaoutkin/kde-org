---
aliases:
- ../../kde-frameworks-5.49.0
date: 2018-08-11
description: KDE Ships Frameworks 5.49.0
layout: framework
title: Release of KDE Frameworks 5.49.0
version: 5.49.0
---

{{<figure src="/announcements/frameworks/5-tp/KDE_QT.jpg" >}}

August 11, 2018. KDE today announces the release of KDE Frameworks 5.49.0.

KDE Frameworks are 70 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Attica

- Don't instantiate a QStringRef into a QString only to search in a QStringList
- Define elements when they're declared

### Baloo

- [tags_kio] Fix multiple filename copies
- Revert "[tags_kio] Use UDS_URL instead of UDS_TARGET_URL."
- [tags_kio] Use UDS_URL instead of UDS_TARGET_URL
- [tags_kio] Query target filepaths instead of appending paths to the file UDS entry
- Support special URLs for finding files of a certain type
- Avoid manipulation of lists with quadratic complexity
- Use non deprecated fastInsert in baloo

### Breeze Icons

- Add <code>drive-optical</code> icon (bug 396432)

### Extra CMake Modules

- Android: Don't hardcode a random version of the Android SDK
- ECMOptionalAddSubdirectory: Provide a bit more detail
- Fix variable definition check
- Change the 'since' version
- Improve ECMAddAppIconMacro

### Framework Integration

- Honor BUILD_TESTING

### KActivities

- Honor BUILD_TESTING

### KArchive

- Honor BUILD_TESTING

### KAuth

- Avoid warnings for PolkitQt5-1 headers
- Honor BUILD_TESTING

### KBookmarks

- Honor BUILD_TESTING

### KCodecs

- Honor BUILD_TESTING

### KCompletion

- Honor BUILD_TESTING

### KConfig

- Honor BUILD_TESTING

### KConfigWidgets

- Honor BUILD_TESTING

### KCoreAddons

- Fix overflow in rounding code (bug 397008)
- API dox: remove not-to-be-there ":"s behind "@note"
- API dox: talk about nullptr, not 0
- KFormat: Replace unicode literal with unicode codepoint to fix MSVC build
- KFormat: correct @since tag for new KFormat::formatValue
- KFormat: Allow usage of quantities beyond bytes and seconds
- Correct KFormat::formatBytes examples
- Honor BUILD_TESTING

### KCrash

- Honor BUILD_TESTING

### KDBusAddons

- Don't block forever in ensureKdeinitRunning
- Honor BUILD_TESTING

### KDeclarative

- ensure we are always writing in the engine's root context
- better readability
- Improve API docs a bit
- Honor BUILD_TESTING

### KDELibs 4 Support

- Fix qtplugins in KStandardDirs

### KDocTools

- Honor BUILD_TESTING

### KEmoticons

- Honor BUILD_TESTING

### KFileMetaData

- API dox: add @file to functions-only header to have doxygen cover those

### KGlobalAccel

- Honor BUILD_TESTING

### KDE GUI Addons

- Honor BUILD_TESTING

### KHolidays

- Install the sunrise/sunset computation header
- Added leap year day as (cultural) holiday for Norway
- Added ‘name’ entry for Norwegian holiday files
- Added descriptions for Norwegian holiday files
- more Japanese holiday updates from phanect
- holiday_jp_ja, holiday_jp-en_us - updated (bug 365241)

### KI18n

- Reuse function that already does the same
- Fix the catalog handling and locale detection on Android
- Readability, skip no-op statements
- Fix KCatalog::translate when translation is same as original text
- a file has been renamed
- Let ki18n macro file name follow style of other find_package related files
- Fix the configure check for _nl_msg_cat_cntr
- Don't generate files in the source directory
- libintl: Determine if _nl_msg_cat_cntr exists before use (bug 365917)
- Fix the binary-factory builds

### KIconThemes

- Honor BUILD_TESTING

### KIO

- Install kio related kdebugsettings category file
- rename private header to _p.h
- Remove custom icon selection for trash (bug 391200)
- Top-align labels in properties dialog
- Present error dialog when user tries to create directory named "." or ".." (bug 387449)
- API dox: talk about nullptr, not 0
- kcoredirlister lstItems benchmark
- [KSambaShare] Check file that's changed before reloading
- [KDirOperator] Use alternating background colors for multi-column views
- avoid memory leak in slave jobs (bug 396651)
- SlaveInterface: deprecate setConnection/connection, nobody can use them anyway
- Slightly faster UDS constructor
- [KFilePlacesModel] Support pretty baloosearch URLs
- Remove projects.kde.org web shortcut
- Switch KIO::convertSize() to KFormat::formatByteSize()
- Use non deprecated fastInsert in file.cpp (first of many to come)
- Replace Gitorious web shortcut by GitLab
- Don't show confirmation dialog for Trash action by default (bug 385492)
- Don't ask for passwords in kfilewidgettest

### Kirigami

- support dynamically adding and removing title (bug 396417)
- introduce actionsVisible (bug 396413)
- adapt margins when scrollbar appears/disappear
- better management of the size (bug 396983)
- Optimise setting up the palette
- AbstractApplciationItem shouldn't have its own size, only implicit
- new signals pagePushed/pageRemoved
- fix logic
- add ScenePosition element (bug 396877)
- No need to emit the intermediary palette for every state
- hide-&gt;show
- Collapsible Sidebar Mode
- kirigami_package_breeze_icons: don't treat lists as elements (bug 396626)
- fix search/replace regexp (bug 396294)
- animating a color produces a rather unpleasant effect (bug 389534)
- color focused item for keyboard navigation
- remove quit shortcut
- Remove long-time deprecated Encoding=UTF-8 from desktop format file
- fix toolbar size (bug 396521)
- fix handle sizing
- Honor BUILD_TESTING
- Show icons for actions that have an icon source rather than an icon name

### KItemViews

- Honor BUILD_TESTING

### KJobWidgets

- Honor BUILD_TESTING

### KJS

- Honor BUILD_TESTING

### KMediaPlayer

- Honor BUILD_TESTING

### KNewStuff

- Remove long-time deprecated Encoding=UTF-8 from desktop format files
- Change default sort order in the download dialog to Rating
- Fix DownloadDialog window margins to meet general theme margins
- Restore accidentally removed qCDebug
- Use the right QSharedPointer API
- Handle empty preview lists

### KNotification

- Honor BUILD_TESTING

### KPackage Framework

- Honor BUILD_TESTING

### KParts

- API dox: talk about nullptr, not 0

### KPeople

- Honor BUILD_TESTING

### KPlotting

- Honor BUILD_TESTING

### KPty

- Honor BUILD_TESTING

### KRunner

- Honor BUILD_TESTING

### KService

- API dox: talk about nullptr, not 0
- Require out-of-source build
- Add subseq operator to match sub-sequences

### KTextEditor

- proper fix for the raw string indenting auto-quoting
- fix indenter to cope with new syntax file in syntaxhighlighting framework
- adjust test to new state in syntax-highlighting repository
- Show "Search wrapped" message in center of view for better visibility
- fix warning, just use isNull()
- Extend Scripting API
- fix segfault on rare cases where empty vector occurs for word count
- enforce clear of scrollbar preview on document clear (bug 374630)

### KTextWidgets

- API docs: partial revert of earlier commit, didn't really work
- KFindDialog: give the lineedit focus when showing a reused dialog
- KFind: reset count when changing the pattern (e.g. in the find dialog)
- Honor BUILD_TESTING

### KUnitConversion

- Honor BUILD_TESTING

### KWallet Framework

- Honor BUILD_TESTING

### KWayland

- Cleanup RemoteAccess buffers on aboutToBeUnbound instead of object destruction
- Support cursor hints on locked pointer
- Reduce unnecessary long wait times on failing signal spies
- Fix selection and seat auto tests
- Replace remaining V5 compat global includes
- Add XDG WM Base support to our XDGShell API
- Make XDGShellV5 co-compilable with XDGWMBase

### KWidgetsAddons

- Fix KTimeComboBox input mask for AM/PM times (bug 361764)
- Honor BUILD_TESTING

### KWindowSystem

- Honor BUILD_TESTING

### KXMLGUI

- Fix KMainWindow saving incorrect widget settings (bug 395988)
- Honor BUILD_TESTING

### KXmlRpcClient

- Honor BUILD_TESTING

### Plasma Framework

- if an applet is invalid, it has immediately UiReadyConstraint
- [Plasma PluginLoader] Cache plugins during startup
- Fix fading node when one textured is atlassed
- [Containment] Don't load containment actions with plasma/containment_actions KIOSK restriction
- Honor BUILD_TESTING

### Prison

- Fix Mixed to Upper mode latching in Aztec code generation

### Purpose

- Make sure debugging for kf5.kio.core.copyjob is disabled for the test
- Revert "test: A more "atomic" way of checking for the signal to happen"
- test: A more "atomic" way of checking for the signal to happen
- Add bluetooth plugin
- [Telegram] Don't wait for Telegram to be closed
- Prepare to use Arc's status colours in the revision drop-down list
- Honor BUILD_TESTING

### QQC2StyleBridge

- Improve sizing of menus (bug 396841)
- Remove double comparison
- Port away from string-based connects
- check for valid icon

### Solid

- Honor BUILD_TESTING

### Sonnet

- Sonnet: setLanguage should schedule a rehighlight if highlight is enabled
- Use the current hunspell API

### Syntax Highlighting

- CoffeeScript: fix templates in embedded JavaScript code &amp; add escapes
- Exclude this in Definition::includedDefinitions()
- Use in-class member initialization where possible
- add functions to access keywords
- Add Definition::::formats()
- Add QVector&lt;Definition&gt; Definition::includedDefinitions() const
- Add Theme::TextStyle Format::textStyle() const;
- C++: fix standard floating-point literals (bug 389693)
- CSS: update syntax and fix some errors
- C++: update for c++20 and fix some syntax errors
- CoffeeScript &amp; JavaScript: fix member objects. Add .ts extension in JS (bug 366797)
- Lua: fix multi-line string (bug 395515)
- RPM Spec: add MIME type
- Python: fix escapes in quoted-comments (bug 386685)
- haskell.xml: don't highlight Prelude data constructors differently from others
- haskell.xml: remove types from "prelude function" section
- haskell.xml: highlight promoted data constructors
- haskell.xml: add keywords family, forall, pattern
- Honor BUILD_TESTING

### ThreadWeaver

- Honor BUILD_TESTING

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.