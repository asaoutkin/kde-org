---
aliases:
- ../../kde-frameworks-5.66.0
date: 2020-01-11
description: KDE Ships Frameworks 5.66.0.
layout: framework
title: Release of KDE Frameworks 5.66.0
version: 5.66.0
---

January 11, 2020. KDE today announces the release of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.66.0.

KDE Frameworks are over 70 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see the <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### All frameworks

- Port from QRegExp to QRegularExpression
- Port from qrand to QRandomGenerator
- Fix compilation with Qt 5.15 (e.g. endl is now Qt::endl, QHash insertMulti now requires using QMultiHash...)

### Attica

- Don't use a verified nullptr as a data source
- Support multiple children elements in comment elements
- Set a proper agent string for Attica requests

### Baloo

- Correctly report if baloo_file is unavailable
- Check cursor_open return value
- Initialise QML monitor values
- Move URL parsing methods from kioslave to query object

### BluezQt

- Add Battery1 interface

### Breeze Icons

- Change XHTML icon to be a purple HTML icon
- Merge headphones and zigzag in the center
- Add application/x-audacity-project icon (bug 415722)
- Add 32px preferences-system
- Add application/vnd.apple.pkpass icon (bug 397987)
- icon for ktimetracker using the PNG in the app repo, to be replaced with real breeze SVG
- add kipi icon, needs redone as a breeze theme svg [or just kill off kipi]

### Extra CMake Modules

- [android] Fix apk install target
- Support PyQt5 compiled with SIP 5

### Framework Integration

- Remove ColorSchemeFilter from KStyle

### KDE Doxygen Tools

- Display fully qualified class/namespace name as page header (bug 406588)

### KCalendarCore

- Improve README.md to have an Introduction section
- Make incidence geographic coordinate also accessible as a property
- Fix RRULE generation for timezones

### KCMUtils

- Deprecate KCModuleContainer

### KCodecs

- Fix invalid cast to enum by changing the type to int rather than enum

### KCompletion

- Deprecate KPixmapProvider
- [KHistoryComboBox] Add method to set an icon provider

### KConfig

- kconfig EBN transport protocol cleanup
- Expose getter to KConfigWatcher's config
- Fix writeFlags with KConfigCompilerSignallingItem
- Add a comment pointing to the history of Cut and Delete sharing a shortcut

### KConfigWidgets

- Rename "Configure Shortcuts" to "Configure Keyboard Shortcuts" (bug 39488)

### KContacts

- Align ECM and Qt setup with Frameworks conventions
- Specify ECM dependency version as in any other framework

### KCoreAddons

- Add KPluginMetaData::supportsMimeType
- [KAutoSaveFile] Use QUrl::path() instead of toLocalFile()
- Unbreak build w/ PROCSTAT: add missing impl. of KProcessList::processInfo
- [KProcessList] Optimize KProcessList::processInfo (bug 410945)
- [KAutoSaveFile] Improve the comment in tempFileName()
- Fix KAutoSaveFile broken on long path (bug 412519)

### KDeclarative

- [KeySequenceHelper] Grab actual window when embedded
- Add optional subtitle to grid delegate
- [QImageItem/QPixmapItem] Don't lose precision during calculation

### KFileMetaData

- Partial fix for accentuated characters in file name on Windows
- Remove unrequired private declarations for taglibextractor
- Partial solution to accept accentuated characters on windows
- xattr: fix crash on dangling symlinks (bug 414227)

### KIconThemes

- Set breeze as default theme when reading from configuration file
- Deprecate the top-level IconSize() function
- Fix centering scaled icons on high dpi pixmaps

### KImageFormats

- pic: Fix Invalid-enum-value undefined behaviour

### KIO

- [KFilePlacesModel] Fix supported scheme check for devices
- Embed protocol data also for Windows version of trash ioslave
- Adding support for mounting KIOFuse URLs for applications that don't use KIO (bug 398446)
- Add truncation support to FileJob
- Deprecate KUrlPixmapProvider
- Deprecate KFileWidget::toolBar
- [KUrlNavigator] Add RPM support to krarc: (bug 408082)
- KFilePlaceEditDialog: fix crash when editing the Trash place (bug 415676)
- Add button to open the folder in filelight to view more details
- Show more details in warning dialog shown before starting a privileged operation
- KDirOperator: Use a fixed line height for scroll speed
- Additional fields such as deletion time and original path are now shown in the file properties dialog
- KFilePlacesModel: properly parent tagsLister to avoid memleak. Introduced with D7700
- HTTP ioslave: call correct base class in virtual_hook(). The base of HTTP ioslave is TCPSlaveBase, not SlaveBase
- Ftp ioslave: fix 4 character time interpreted as year
- Re-add KDirOperator::keyPressEvent to preserve BC
- Use QStyle for determining icon sizes

### Kirigami

- ActionToolBar: Only show the overflow button if there are visible items in the menu (bug 415412)
- Don't build and install app templates on android
- Don't hardcode the margin of the CardsListView
- Add support for custom display components to Action
- Let the other components grow if there's more things on the header
- Remove dynamic item creation in DefaultListItemBackground
- reintroduce the collapse button (bug 415074)
- Show application window icon on AboutPage

### KItemModels

- Add KColumnHeadersModel

### KJS

- Added tests for Math.exp()
- Added tests for various assignment operators
- Test special cases of multiplicate operators (*, / and %)

### KNewStuff

- Ensure the dialog title is correct with an uninitialised engine
- Don't show the info icon on the big preview delegate (bug 413436)
- Support archive installs with adoption commands (bug 407687)
- Send along the config name with requests

### KPeople

- Expose enum to the metaobject compiler

### KQuickCharts

- Also correct the shader header files
- Correct license headers for shaders

### KService

- Deprecate KServiceTypeProfile

### KTextEditor

- Add "line-count" property to the ConfigInterface
- Avoid unwanted horizontal scrolling (bug 415096)

### KWayland

- [plasmashell] Update docs for panelTakesFocus to make it generic
- [plasmashell] Add signal for panelTakesFocus changing

### KXMLGUI

- KActionCollection: provide a changed() signal as a replacement for removed()
- Adjust keyboard shortcut configuration window's title

### NetworkManagerQt

- Manager: add support for AddAndActivateConnection2
- cmake: Consider NM headers as system includes
- Sync Utils::securityIsValid with NetworkManager (bug 415670)

### Plasma Framework

- [ToolTip] Round position
- Enable wheel events on Slider {}
- Sync QWindow flag WindowDoesNotAcceptFocus to wayland plasmashell interface (bug 401172)
- [calendar] Check out of bounds array access in QLocale lookup
- [Plasma Dialog] Use QXcbWindowFunctions for setting window types Qt WindowFlags doesn't know
- [PC3] Complete plasma progress bar animation
- [PC3] Only show progress bar indicator when the ends won't overlap
- [RFC] Fix Display Configuration icon margins (bug 400087)
- [ColorScope] Work with plain QObjects again
- [Breeze Desktop Theme] Add monochrome user-desktop icon
- Remove default width from PlasmaComponents3.Button
- [PC3 ToolButton] Have the label take into account complementary color schemes (bug 414929)
- Added background colors to active and inactive icon view (bug 370465)

### Purpose

- Use standard ECMQMLModules

### QQC2StyleBridge

- [ToolTip] Round position
- Update size hint when font changes

### Solid

- Display first / in mounted storage access description
- Ensure mounted nfs filesystems matches their fstab declared counterpart (bug 390691)

### Sonnet

- The signal done is deprecated in favour of spellCheckDone, now correctly emitted

### Syntax Highlighting

- LaTeX: fix brackets in some commands (bug 415384)
- TypeScript: add "bigint" primitive type
- Python: improve numbers, add octals, binaries and "breakpoint" keyword (bug 414996)
- SELinux: add "glblub" keyword and update permissions list
- Several enhancements to gitolite syntax definition

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB