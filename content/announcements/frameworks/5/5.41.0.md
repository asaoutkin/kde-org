---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
description: KDE Ships Frameworks 5.41.0
layout: framework
title: Release of KDE Frameworks 5.41.0
version: 5.41.0
---

{{<figure src="/announcements/frameworks/5-tp/KDE_QT.jpg" >}}

December 10, 2017. KDE today announces the release of KDE Frameworks 5.41.0.

KDE Frameworks are 70 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Baloo

- Strip down and re-write the baloo tags KIO slave (bug 340099)

### BluezQt

- Do not leak rfkill file descriptors (bug 386886)

### Breeze Icons

- Add missing icon sizes (bug 384473)
- add install and uninstall icons for discover

### Extra CMake Modules

- Add the description tag to the generated pkgconfig files
- ecm_add_test: Use proper path sep on Windows
- Add FindSasl2.cmake to ECM
- Only pass the ARGS thing when doing Makefiles
- Add FindGLIB2.cmake and FindPulseAudio.cmake
- ECMAddTests: set QT_PLUGIN_PATH so locally built plugins can be found
- KDECMakeSettings: more docu about the layout of the build dir

### Framework Integration

- Support downloading the 2nd or 3rd download link from a KNS product (bug 385429)

### KActivitiesStats

- Start fixing libKActivitiesStats.pc: (bug 386933)

### KActivities

- Fix race that starts kactivitymanagerd multiple times

### KAuth

- Allow to only build the kauth-policy-gen code generator
- Add a note about calling the helper from multithreaded applications

### KBookmarks

- Do not show edit bookmarks action if keditbookmarks is not installed
- Port from deprecated KAuthorized::authorizeKAction to authorizeAction

### KCMUtils

- keyboard navigation in and out QML kcms

### KCompletion

- Do not crash when setting new line edit on an editable combo box
- KComboBox: Return early when setting editable to previous value
- KComboBox: Reuse the existing completion object on new line edit

### KConfig

- Don't look for /etc/kderc every single time

### KConfigWidgets

- Update default colors to match new colors in D7424

### KCoreAddons

- Input validation of SubJobs
- Warn about errors when parsing json files
- Install mimetype definitions for kcfg/kcfgc/ui.rc/knotify &amp; qrc files
- Add a new function to measure the length by text
- Fix KAutoSave bug on file with white space in it

### KDeclarative

- Make it compile on windows
- make it compile with QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY
- [MouseEventListener] Allow accepting mouse event
- use a single QML engine

### KDED

- kded: remove dbus calls to ksplash

### KDocTools

- Update Brasilian Portuguese translation
- Update Russian translation
- Update Russian translation
- Update customization/xsl/ru.xml (nav-home was missing)

### KEmoticons

- KEmoticons: port plugins to JSON and add support for loading with KPluginMetaData
- Do not leak symbols of pimpl classes, protect with Q_DECL_HIDDEN

### KFileMetaData

- The usermetadatawritertest requires Taglib
- If the property value is null, remove the user.xdg.tag attribute (bug 376117)
- Open files in TagLib extractor readonly

### KGlobalAccel

- Group some blocking dbus calls
- kglobalacceld: Avoid loading an icon loader for no reason
- generate correct shortcut strings

### KIO

- KUriFilter: filter out duplicate plugins
- KUriFilter: simplify data structures, fix memory leak
- [CopyJob] Don't start all over after having removed a file
- Fix creating a directory via KNewFileMenu+KIO::mkpath on Qt 5.9.3+ (bug 387073)
- Created an auxiliary function 'KFilePlacesModel::movePlace'
- Expose KFilePlacesModel 'iconName' role
- KFilePlacesModel: Avoid unnecessary 'dataChanged' signal
- Return a valid bookmark object for any entry in KFilePlacesModel
- Create a 'KFilePlacesModel::refresh' function
- Create 'KFilePlacesModel::convertedUrl' static function
- KFilePlaces: Created 'remote' section
- KFilePlaces: Add a section for removable devices
- Added baloo urls into places model
- Fix KIO::mkpath with qtbase 5.10 beta 4
- [KDirModel] Emit change for HasJobRole when jobs change
- Change label "Advanced options" &gt; "Terminal options"

### Kirigami

- Offset the scrollbar by the header size (bug 387098)
- bottom margin based on actionbutton presence
- don't assume applicationWidnow() to be available
- Don't notify about value changes if we are still in the constructor
- Replace the library name in the source
- support colors in more places
- color icons in toolbars if needed
- consider icon colors in the main action buttons
- start for an "icon" grouped property

### KNewStuff

- Revert "Detach before setting the d pointer" (bug 386156)
- do not install development tool to aggregate desktop files
- [knewstuff] Do not leak ImageLoader on error

### KPackage Framework

- Properly do strings in the kpackage framework
- Don't try to generate metadata.json if there's no metadata.desktop
- fix kpluginindex caching
- Improve error output

### KTextEditor

- Fix VI-Mode buffer commands
- prevent accidental zooming

### KUnitConversion

- Port from QDom to QXmlStreamReader
- Use https for downloading currency exchange rates

### KWayland

- Expose wl_display_set_global_filter as a virtual method
- Fix kwayland-testXdgShellV6
- Add support for zwp_idle_inhibit_manager_v1 (bug 385956)
- [server] Support inhibiting the IdleInterface

### KWidgetsAddons

- Avoid inconsistent passworddialog
- Set enable_blur_behind hint on demand
- KPageListView: Update width on font change

### KWindowSystem

- [KWindowEffectsPrivateX11] Add reserve() call

### KXMLGUI

- Fix translation of toolbar name when it has i18n context

### Plasma Framework

- The #warning directive is not universal and in particular is NOT supported by MSVC
- [IconItem] Use ItemSceneHasChanged rather than connect on windowChanged
- [Icon Item] Explicitly emit overlaysChanged in the setter rather than connecting to it
- [Dialog] Use KWindowSystem::isPlatformX11()
- Reduce the amount of spurious property changes on ColorScope
- [Icon Item] Emit validChanged only if it actually changed
- Suppress unnecessary scroll indicators if the flickable is a ListView with known orientation
- [AppletInterface] Emit change signals for configurationRequired and -Reason
- Use setSize() instead of setProperty width and height
- Fixed an issue where PlasmaComponents Menu would appear with broken corners (bug 381799)
- Fixed an issue where context menus would appear with broken corners (bug 381799)
- API docs: add deprecation notice found in the git log
- Synchronize the component with the one in Kirigami
- Search all KF5 components as such instead as separate frameworks
- Reduce spurious signal emissions (bug 382233)
- Add signals indicating if a screen was added or removed
- install Switch stuff
- Don't rely in includes of includes
- Optimize SortFilterModel role names
- Remove DataModel::roleNameToId

### Prison

- Add Aztec code generator

### QQC2StyleBridge

- determine QQC2 version at build time (bug 386289)
- by default, keep the background invisible
- add a background in ScrollView

### Solid

- Faster UDevManager::devicesFromQuery

### Sonnet

- Make it possible to crosscompile sonnet

### Syntax Highlighting

- Add PKGUILD to bash syntax
- JavaScript: include standard mime types
- debchangelog: add Bionic Beaver
- Update SQL (Oracle) syntax file (bug 386221)
- SQL: move detecting comments before operators
- crk.xml: added &lt;?xml&gt; header line

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.