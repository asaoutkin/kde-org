---
aliases:
- ../../kde-frameworks-5.76.0
date: 2020-11-07
description: KDE today announces the release of KDE Frameworks 5.76.0.
directory: 5.76
layout: framework
qtversion: 5.12
title: KDE Ships Frameworks 5.76.0
version: 5.76.0
---

November 07, 2020. KDE today announces the release of KDE Frameworks 5.76.0.

KDE Frameworks are 70 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see [the KDE Frameworks release announcement](/announcements/kde-frameworks-5.0).

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Baloo

* Split CJK terms at punctuation characters, optimize code
* Refactor the code in preparation for ICU integration

### Breeze Icons

* Add 48px dialog-warning icon
* Change style of media-repeat-single to use number 1
* Add more git ignored files
* check if file exists before removing
* Always remove destination file first when generating symlinks
* Add some color mode icons for Okular
* Add task-complete icons (bug 397996)
* Add network-limited icon
* Copy 32px kup symlink to apps/48 to fix scalable test failure
* Add meeting-organizer icons (bug 397996)
* Add fingerprint icon
* Add task-recurring and appointment-recurring icons (bug 392533)
* Temporarily disable generating icons on Windows
* Symlink kup.svg to preferences-system-backup.svg

### Extra CMake Modules

* Make androiddeployqt find libraries and QML plugins without installation
* find-modules/FindReuseTool.cmake - Fix finding reuse tool
* improve default formatting options
* Include option to use LLVM for users with Qt < 5.14
* add missing minimum version for RENAME parameter
* Document when FindGradle has been added
* Add FindGradle from KNotification

### KAuth

* Convert the backend name to uppercase sooner
* Add helper to obtain the caller's uid

### KCalendarCore

* Raise ambiguity in ICalFormat::toString() in tests
* Add COLOR property serialization from RFC7986
* Make MemoryCalendar::rawEvents(QDate, QDate) works for open bounds

### KCMUtils

* Port from QStandardPaths::DataLocation to QStandardPaths::AppDataLocation
* Add namespace support to KCModuleData CMake macro
* Deprecate KSettings::PluginPage
* remove reference to undefined header
* Move the KCMUtilsGenerateModuleData to correct place
* Push all pre-created sub pages of a KCM
* Add CMake function to generate basics module data
* Improve legibility of inline QML in kcmoduleqml.cpp
* proper header height with a placeholder
* [kcmoduleqml] Fix top margin for QML KCMs

### KConfig

* Find missing Qt5DBus dependency
* kconf_update: Allow repeated tests in --testmode by ignoring kconf_updaterc

### KConfigWidgets

* Change http: to https:

### KContacts

* Fix Bug 428276 - KContacts cannot be used in qmake project (bug 428276)

### KCoreAddons

* KJob: add setProgressUnit(), to choose how percent is calculated
* Fix potential memory leak in KAboutData::registerPluginData
* Split suggestName(); the split method doesn't check if file exists
* KAboutData: deprecate pluginData() & registerPluginData()
* Don't quit the event loop in KJobTest::slotResult()
* Use functor-based singleShot() overload in TestJob::start()

### KDeclarative

* [abstractkcm] Set explicit padding
* [simplekcm] Remove custom padding handling
* [kcmcontrols] Remove duplicated code
* Add source to KDeclarativeMouseEvent
* reparent overlaysheets to the root
* Make GridViewKCM and ScrollVieKCM inherit from AbstractKCM
* Add getter method for subPages

### KDocTools

* Fix xml formatting in contributor.entities
* Korean update: reformat HTML files of GPL, FDL and add LGPL

### KFileMetaData

* [ExtractionResult] Restore binary compatibility
* [TaglibWriter|Extractor] Remove raw speex mimetype
* [TaglibWriter] Open the read-write also on Windows
* [Extractor|WriterCollection] Filter out non-library files
* [EmbeddedImageData] Try to work around MSVC stupidity
* [ExtractionResult] Deprecate ExtractEverything, fixup since
* [EmbeddedImageData] Read test ground truth image just once
* [EmbeddedImageData] Remove private cover writing implementation
* [EmbeddedImageData] Move write implementation to taglib writer plugin
* [EmbeddedImageData] Remove private cover extraction implementation
* [EmbeddedImageData] Move implementation to taglib extractor plugin

### KGlobalAccel

* systemd dbus activation

### KIconThemes

* Keep aspect ration when upscaling

### KIdleTime

* Deprecate single-arg signal KIdleTime::timeoutReached(int identifier)

### KImageFormats

* Add support for RLE-compressed, 16 bits per channel PSD files
* Return unsupported when reading 16bit RLE compressed PSD files
* feat: add psd color depth == 16 format support

### KIO

* conditionally compare with blank QUrl instead of / on Windows for mkpathjob
* KDirModel: two fixes for QAbstractItemModelTester
* CopyJob: Include skipped files in progress calculation when renaming
* CopyJob: don't count skipped files in the notification (bug 417034)
* In file dialogs, select an existing dir when trying to create it
* CopyJob: fix total number of files/dirs in progress dialog (when moving)
* Make FileJob::write() behave consistently
* Support for xattrs on kio copy/move
* CopyJob: don't count dir sizes into the total size
* KNewFileMenu: Fix crash by using m_text rather than m_lineEdit->text()
* FileWidget: Show Selected file preview on mouse leave (bug 418655)
* expose user context help field in kpasswdserver
* KNewFileMenu: use NameFinderJob to get a "New Folder" name
* Introduce NameFinderJob that suggests new "New Folder" names
* Do not explicitly define Exec lines for KCMs (bug 398803)
* KNewFileMenu: Split the dialog creation code to a separate method
* KNewFileMenu: check file does not already exists with delay for improved usability
* [PreviewJob] Allocate sufficient memory for SHM segment (bug 427865)
* Use versioning mechanism to add the new places for existing users
* Add bookmarks for pictures, music and videos (bug 427876)
* kfilewidget: keep the text in the Name box when navigating (bug 418711)
* Handle KCMs in OpenUrlJob with KService API
* Canonicalize file path when fetching and creating thumbnails
* KFilePlacesItem: hide kdeconnect sshfs mounts
* OpenFileManagerWindowJob: pick window from main job correctly
* Avoid pointless probing for nonexisting thumbnail images
* [BUG] Fixing regression on selecting files that contain `#`
* KFileWidget: make icon zoom buttons jump to the nearest standard size
* put minimumkeepsize actually in the netpref KCM (bug 419987)
* KDirOperator: simplify the icons zoom slider logic
* UDSEntry: document the expected time format for time keys
* kurlnavigatortest: remove the desktop:, needs desktop.protocol to work
* KFilePlacesViewTest: don't show a window, not needed
* OpenFileManagerWindowJob: Fix crash when falling back to KRun strategy (bug 426282)
* Internet keywords: fix crash and failed tests if delimiter is space
* Prefer DuckDuckGo bangs over other delimiters
* KFilePlacesModel: ignore hidden places when computing closestItem (bug 426690)
* SlaveBase: document ERR_FILE_ALREADY_EXIST behavior with copy()
* kio_trash: fix the logic when no size limit is set (bug 426704)
* In file dialogs, creating a dir that already exists should select it
* KFileItemActions: Add property for min/max count of Urls

### Kirigami

* [avatar]: Make numbers invalid names
* [avatar]: Expose cache property of image
* Also set a maximumWidth for icons in global drawer (bug 428658)
* Make quit shortcut an action and expose it as a readonly property
* Use hand cursors on ListItemDragHandle (bug 421544)
* [controls/avatar]: Support CJK names for initials
* Improve look of the FormLayout on mobile
* Fix menus in contextualActions
* Don't alter Item in code called from Item's destructor (bug 428481)
* don't modify the other layout reversetwins
* Set/unset focus to overlay sheet on open/close
* Only drag window by the global toolbar when pressed & dragged
* Close OverlaySheet when pressing Esc key
* Page: Make padding, horizontalPadding and verticalPadding properties work
* ApplicationItem: Use background property
* AbstractApplicationItem: add missing properties & behavior from QQC2 ApplicationWindow
* limit items width to layout width
* Fix back button not showing on layered page headers on mobile
* Silence warning about "checkable" binding loop in ActionToolBar
* swap the order of columns on rtl layouts
* workaround to make sure ungrabmouse is called every time
* check for startSystemMove existence
* fix separator on mirrored layouts
* drag window by clicking on empty areas
* don't scroll by drag with mouse
* Fix cases when the reply is null
* Fix faulty refactor of Forward/BackButton.qml
* Ensure empty icon is Ready and doesn't get painted as previous icon
* Constrain height of back/forward button in PageRowGlobalToolBarUI
* Silence console spam from ContextDrawer
* Silence console spam from ApplicationHeader
* Silence console spam from back/forwardbutton
* Prevent mouse dragging from dragging an OverlaySheet
* Drop lib prefix when building for Windows
* fix twinformlayouts alignment management
* Improve legibility of embedded QML in C++ code

### KItemModels

* KRearrangeColumnsProxyModel: fix crash with no source model
* KRearrangeColumnsProxyModel: only column 0 has children

### KNewStuff

* Fix erroneous logic introduced in e1917b6a
* Fix double-delete crash in kpackagejob (bug 427910)
* Deprecate Button::setButtonText() and fix API docs, nothing is prepended
* Postpone all on-disk cache writes until we've had a quiet second
* Fix crash when list of installed files is empty

### KNotification

* Mark KNotification::activated() as deprecated
* Apply some sanity checking to action keys (bug 427717)
* Use FindGradle from ECM
* Fix condition to use dbus
* Fix: enable legacy tray on platforms without dbus
* rewrite notifybysnore to provide more reliable support for Windows
* Add comments to describe DesktopEntry field in notifyrc file

### KPackage Framework

* Make "no metadata" warning a debug-only thing

### KPty

* Rip out AIX, Tru64, Solaris, Irix support

### KRunner

* Deprecate obsolete RunnerSyntax methods
* Deprecate ignoreTypes and RunnerContext::Type
* Do not set the type to File/Directory if it does not exist (bug 342876)
* Update maintainer as discussed in the mailing list
* Deprecate unused constructor for RunnerManager
* Deprecate categories feature
* Remove unnecessary check if runner is suspended
* Deprecate defaultSyntax and setDefaultSyntax methods
* Cleanup defunct usage of RunnerSyntax

### KService

* Allow NotShowIn=KDE apps, listed in mimeapps.list, to be used (bug 427469)
* Write fallback value for KCM Exec lines with appropriate executable (bug 398803)

### KTextEditor

* [EmulatedCommandBar::switchToMode] Do nothing when the old and new modes are the same (bug 368130 as follows:)
* KateModeMenuList: remove special margins for Windows
* Fix memory leak in KateMessageLayout
* try to avoid to erase custom-styles for highlightings we didn't touch at all (bug 427654)

### KWayland

* Provide convenience methods around wl_data_offet_accept()
* Mark enums in a Q_OBJECT, Q_ENUM

### KWidgetsAddons

* new setUsernameContextHelp on KPasswordDialog
* KFontRequester: remove, the now redundant, nearestExistingFont helper

### KWindowSystem

* xcb: Fix detection of screen sizes for High-DPI

### NetworkManagerQt

* Add enum and declarations to allow passing capabilities in the registration process to NetworkManager

### Plasma Framework

* BasicPlasmoidHeading component
* Always show ExpandableListitem buttons, not just on hover (bug 428624)
* [PlasmoidHeading]: Set implicit sizing properly
* Lock the header colours of Breeze Dark and Breeze Light (bug 427864)
* Unify aspect ratio of 32px and 22px battery icons
* Add margin hints to toolbar.svg and refactor PC3 ToolBar
* Add AbstractButton and Pane to PC3
* support exclusive action groups in the contextual actions
* Fix BusyIndicator rotating even when invisible, again
* Fix colours not applying to mobile task switcher icon
* Add plasma mobile task switcher and close app icons (for taskpanel)
* Better Menu in PlasmaComponents3
* Remove unnecessary anchors in the ComboBox.contentItem
* Round slider handle position
* [ExpandableListItem] Load expanded view on demand
* Add missing `PlasmaCore.ColorScope.inherit: false`
* Set PlasmoidHeading colorGroup in root element
* [ExpandableListItem] Make colored text 100% opaque (bug 427171)
* BusyIndicator: Do not rotate when invisible (bug 426746)
* ComboBox3.contentItem must be a QQuickTextInput to fix autocomplete (bug 424076)
* FrameSvg: Don't reset the cache when resizing
* Toggle plasmoids when shortcut is activated (bug 400278)
* TextField 3: Add missing import
* Fix IDs in plasmavault_error icon
* PC3: fix color of TabButton label
* Use a hint instead of a bool
* Allow plasmoids to ignore the margins

### Purpose

* Add description to kaccounts youtube provider

### QQC2StyleBridge

* Fix ToolBar contentWidth binding loop
* Reference shortcut label directly by id instead of implicitly
* ComboBox.contentItem must be a QQuickTextInput to fix autocomplete (bug 425865)
* Simplify conditional clauses in Connections
* Fix Connections warning on ComboBox
* Add support for qrc icons to StyleItem (bug 427449)
* Properly indicate focus state of ToolButton
* Add TextFieldContextMenu for right click context menus on TextField and TextArea
* Set background color to ComboBox's ScrollView

### Solid

* Add support for sshfs to the fstab backend
* CMake: Use pkg_search_module when looking for plist
* Fix imobiledevice backend: Check API version for DEVICE_PAIRED
* Fix imobiledevice backend build
* Add Solid backend using libimobiledevice for finding iOS devices
* Use QHash for mapping where order is not needed

### Sonnet

* Use modern signal-slot connection syntax

### Syntax Highlighting

* The "compact" core function is missing
* comment out the check, add comment why this no longer works here
* The position:sticky value is missing
* fix php/* generation for new Comments hl
* Feature: Replace Alerts w/ Special-Comments syntax and remove Modelines
* Feature: Add the `comments.xml` as an umbrella syntax for various comment kinds
* Fix: CMake syntax now mark `1` and `0` as special boolean values
* Improvement: Include Modelines rules in files where Alerts has been added
* Improvement: Add some more boolean values to `cmake.xml`
* Solarized themes: improve separator
* Improvement: Updates for CMake 3.19
* Add support for systemd unit files
* debchangelog: add Hirsute Hippo
* Feature: Allow multiple `-s` options for `kateschema2theme` tool
* Improvement: Add various tests to the converter
* move more utility scripts to better location
* move update-kate-editor-org.pl script to better place
* kateschema2theme: Add a Python tool to convert old schema files
* Decrease opacity in separator of Breeze & Dracula themes
* Update README with "Color themes files" section
* Fix: Use `KDE_INSTALL_DATADIR` when install syntax files
* fix rendering of --syntax-trace=region with multiple graphics on the same offset
* fix some issues of fish shell
* replace StringDetect by DetectChar / Detect2Chars
* replace some RegExpr by StringDetect
* replace RegExpr="." + lookAhead by fallthroughContext
* replace \s* with DetectSpaces

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB