---
title: KDE's January 2020 Apps Update

publishDate: 2020-01-09 12:00:00

layout: page # don't translate

summary: "What Happened in KDE's Applications This Month"

type: announcement # don't translate
---

The year is 2020, we are living in the future, let's see what KDE apps has brought us in the last month!

## KTimeTracker ported to KDE Frameworks 5

The long-awaited modernized version of KTimeTracker is finally released.
The application is a personal time tracker for busy people which is now
available on Linux, FreeBSD and Windows. Over the course of 2019 it had been
ported to Qt5 and KDE Frameworks after being unmaintained since around 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

The new version is also polished and slightly modernised with the most
noticeable new features being the new Task Time Editing dialog and
live preview in the Export dialog as seen in the picture below.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Export dialog in KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/
It is available through your Linux distro or as a [Windows installer](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) and there's even [untested MacOS builds](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) built nightly.

[The release is announced in the blog of the maintainer Alexander Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

Astronomy program [KStars got new features in 3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Images can have fine changes in Shadows, Midtones, and Highlights allowing the faintest of stars to be seen.

Alternative constellations from the Western Sky Culture which is fascinating to [read about](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Alternative constellations" alt="Western Sky Culture" >}}

[KStars is available for](https://edu.kde.org/kstars/) Android, Windows, MacOS, Snap store and from your Linux distro.

## Common Frameworks - KNewStuff

Here on the Apps Update we focus on the apps rather than coding libraries.  But new features in the common libraries will mean new features in all your apps :)

This month saw a redesigned UI for KNewStuff, the framework to download addons for your applications.  The browse and download dialog was redesigned and the comments section can now be filtered.  It'll be appearing in all your apps and settings modules shortly starting with the Global Theme module in System Settings.

{{< img src="knewstuff-comments.png" caption="Filters on Comments" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Redesigned Browse Dialog" alt="Redesigned Browse Dialog" style="width: 800px">}}

## Bugfixes

[KDevelop's monthly bugfix update 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) fixed a longstanding problem where the GPL and LGPL headers were mixed up, grab it from your distro or Appimage to make sure your licencing is correct.

[Latte Dock 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) fixed some features with Qt 5.14 and removed some crashes.

Dolphin Plugins 19.12.1 fixed a broken SVN Commit dialog.

There was improved file indexing in Elisa.  It also fixed some compilation issues on Android and without Baloo.

The new release of KPat was declaired to have no [OARS](https://hughsie.github.io/oars/index.html) relevant age restrictions.

Okular fixed a crash when closing the print preview dialog.

This month's release of Kdenlive video editor had an impressive number of fixes best of all was updating the [screenshots](https://kde.org/applications/multimedia/org.kde.kdenlive) used in the meta info. It also has dozens of improvements and fixes in timeline and preview handling.

New JavaScript support is now in Kate's [LSP](https://langserver.org/) client.

## Enjoy KDE on the Flathub Store

KDE is embracing all the app stores.  We can now deliver more and more of our programs directly to you the user.  One of the leading app stores on Linux is [Flathub](https://flathub.org/home) which uses the FlatPak format.

You may well already have Flatpak and Flathub configured on your system and ready to use in Discover or other app installers.  For example KDE neon has set it up by default on installs for over a year now.  If not it's a quick [setup process](https://flatpak.org/setup/) for all the major distros.

If you're interested in the techy details you can browse [KDE's Flatpak packaging repo](https://phabricator.kde.org/source/flatpak-kde-applications/) and read the [KDE Flatpak guide](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

But probably what you're interested in is the apps so take a look at what the [Flathub store has under KDE](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## LabPlot now on Chocolatey

[Chocolatey](https://chocolatey.org/) is a package manager for Windows.  If you want full control over what software is installed on your Windows machine or whole office of machines then Chocolatey gives you easy control over that just like you are used to on Linux.

[LabPlot](https://chocolatey.org/packages/labplot) is KDE's app for interactive graphing and analysis of scientific data and it is now available through Chocolatey.  Give it a try!

[LabPlot blog](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Website Updates

The recently revived KDE Web Team has been updating a bunch of our older themed sites. The newly relaunched [KPhotoAlbum](https://www.kphotoalbum.org/) website is a lovely example, updated and refreshed for our photo storage and search app.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

And if you want to show off a simple to use but full featured local music player but were ashamed by the old looking website, [JuK](https://juk.kde.org/) has just had an updated website too.

## Releases 19.12.1

Some of our projects release on their own timescale and some get released en-masse. The 19.12.1 bundle of projects was released today and should be available through app stores and distros soon.  See the [19.12.1 releases page](https://www.kde.org/info/releases-19.12.1.php).  This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because it is dozens of different products rather than a single whole.
