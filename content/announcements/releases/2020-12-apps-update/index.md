---
title: KDE's December 2020 Apps Update
publishDate: "2020-12-10T12:00:00+00:00" # todo change me
layout: page
type: announcement
---

KDE apps are always being improved and enhanced. But not only that, KDE developers are also constantly working on new applications to cover users' needs. Today's release is no different: the new bundle updates email, calendar and contact suite, Kontact; adds new travel assistant apps Itinerary and KPublicTransport to the list of available programs; and, for the first time, includes Partition Manager, the Konversation chat client, and KTorrent into our regular release service.

## First Itinerary release

KDE Itinerary is an app that provides you with all the information you need while on the road. This digital travel assistant supplies you with timetables and locations for trains, airplanes and long-distance bus itineraries, as well as information about hotel or event bookings. KDE Itinerary can import data from several sources, such as your email client, and update you with real-time delay/disruption information, the weather forecast at your destination and suggestions for local public transport to get to the train station or airport on time. It can also provide you with train and coach station layout maps.

You can read more about KDE Itinerary on all its features on [Volker Krause's blog](https://www.volkerkrause.eu/).

## Kontact

[Kontact](https://kontact.kde.org/) is your all-in-one personal information manager, allowing you to read your mails, store your contacts, manage your calendar and so much more. In this release, Kontact saw significant improvements.

First, the user experience of Google synchronization has been improved and now doesn't use an embedded browser to log in, but instead opens your default web browser. You also now get notifications when the Google account needs to be re-authenticated.

There is also a new plugin that allows you to configure the settings of multiple mail folders at the same time. This is particularly helpful if you have dozens of folders and want to change the configuration for just a group of them.

*Kleopatra*, the certificate manager for Kontact, now supports more types of smart cards. It previously supported OpenPGP and NetKey cards but now also covers PIV cards (which are used in governmental and corporate environments). The user interface has also been simplified and it is now easier to switch between the main views.

*Akonadi*, the underlying data storage framework for Kontact, also saw some significant improvements. Akonadi now supports the LZMA compression algorithm to store the data, thus reducing the disk usage by up to 30%. The KIMAP library that handles fetching your mails now supports the QRESYNC extension ([RFC 5162](https://tools.ietf.org/html/rfc5162)). This extension allows for even faster and more reliable email synchronization with less bandwidth usage when syncing emails with IMAP servers that support this extension.

Kontact also now supports a new service: [EteSync](https://www.etesync.com/). EteSync is a secure, end-to-end encrypted, and privacy respecting sync service for your contacts, calendars, tasks and notes. You can read more about EteSync at the [GSoC project report](https://thejollyblog.netlify.app/posts/KDE/gsoc-part-6-user-guide).

## Dolphin

{{< img class="text-center img-fluid" src="dolphin.png" caption="Dolphin's address bar is now in the toolbar" style="width: 600px">}}

[Dolphin](https://apps.kde.org/en/dolphin) is KDE's file and folder manager. Several changes to Dolphin's interface make it easier for you to navigate your storage and browse your documents, photos, songs, and directories.

Dolphin's address bar is now in the toolbar, for example, and, in *Detail View* mode, you can set the size to include everything in the folder including the nested folders inside the parent one.

A change in Frameworks means that the *Places* panel in Dolphin, the file dialogs, and various other places now include entries for your *Music*, *Pictures*, and *Videos* folders by default. Related to that, screenshots taken with [Spectacle](https://apps.kde.org/en/spectacle) now appear in the list of recent documents in the *Places Panel*. The *Places Panel* is visible in Dolphin, the file dialogs, and various other pieces of software.

Finally, for those looking to use Dolphin on touchscreens, Dolphin now has full touch support!

## Konsole

{{< img class="text-center img-fluid" src="konsole-html.jpeg" caption="Konsole now with a toolbar, showing off HTML colour preview, a vertical line to mark column 80 and it can open URLs and files" style="width: 600px">}}

Konsole now has a configurable toolbar (which you can of course hide if you don’t like it). It shows HTML colour previews as tooltips, can show a vertical line to mark column 80 or any other column, and it can open URLs and files. If you're running a command which uses the full terminal such as ```less``` or ```vim``` and wants to peek back at your command line there's now a setting for a shortcut to do this.

## Konversation

{{< img class="text-center img-fluid" src="konversation.png" caption="Nick icon themes can now be browsed and selected" style="width: 600px">}}

IRC chat app [Konversation](https://konversation.kde.org/) joins the regular release service and adds a bunch of new features:

* Konversation now supports downloading nick icon themes from store.kde.org
* The channel history can be deleted for privacy
* The available server capabilities get shown on connect

And there's a bunch of technical protocol enhancements:

* The IRCv3 protocol has been improved with added support for capability negotiation v3.2
* Konversation now uses SASL authentication mechanisms in CAP LS
* A date marker gets added to the first line on new date if the date is not in the timestamp
* Konversation follows the standard for string handling when sending PASS command
* The znc.in/self-message capability has been added 
* WHO requests for twitch.tv servers have been disabled

## Spectacle

{{< img class="text-center img-fluid" src="spectacle.png" caption="Spectacle with annotations" style="width: 600px">}}

KDE's screenshot app, [Spectacle](https://apps.kde.org/en/spectacle), now has an editor that lets you add arrows, smilies, text and freehand drawing to your screenshots.

## KDE Connect

[KDE Connect](https://kdeconnect.kde.org/) is an app that lets you synchronize multiple services across your mobile devices and desktop computers. The new version released today lets you download conversations part-by-part to load more efficiently and quickly; adds a thumbnail preview to the list of conversations when the most-recent message contains an attachment; and improves the placeholder message when the most-recent message in a conversation has an attachment but no plain-text body.

## Other New Features

There are too many new features to give them all justice. Here's a flavour of some you might come across in the new app versions out today:

{{< img class="text-center img-fluid" src="filelight.jpeg" caption="Filelight can save as SVG" style="width: 600px">}}

* [Elisa](https://elisa.kde.org/), a compact music player, now lets you change the color scheme of the app independent of the system wide color scheme. It also now lets you choose which view to display when the app launches 
* [Ark](https://apps.kde.org/en/ark), KDE's compression utility, now supports archives with zstd compression 
* [Gwenview](https://apps.kde.org/en/gwenview), an advanced image and video-clip viewer, now has an option to not auto-play videos in browse mode
* [Kate](https://apps.kde.org/en/kate) is a feature-rich text editor. Kate’s file browser now has an *Open with* menu item in its context menu
* [Filelight](https://apps.kde.org/en/filelight) shows you visually how much space each folder and file takes up on your disks. Filelight now comes with a feature to save the current view as an SVG file
* KDE's Nextcloud and Owncloud wizards now boast overhauled visuals in *System Settings*' *Online Accounts* page
* [KAlarm](https://apps.kde.org/en/kalarm), a personal alarm message, command and email scheduler, now has the option to use the notification system to
display alarm messages. It also has the option to give alarms names for easier
identification.

# Incoming

Some new apps arrived in *Playground*, which is where we work on them before they are ready for release.

{{< img class="text-center img-fluid" src="totalreqall.png" caption="TotalReqall" style="width: 600px">}}

Such is the case of **TotalReqall**, a program for memorizing the Bible and other works that uses the Sword library.

# Releases 20.12.0

[20.12 release notes](https://community.kde.org/Releases/20.12_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull; [20.12.0 source info page](https://kde.org/info/releases-20.12.0) &bull; [20.12.0 full changelog](https://kde.org/announcements/fulllog_releases-20.12.0/)

# Contributing

If you would like to help, [get involved](https://community.kde.org/Get_Involved)! We are a friendly and accepting community and we need developers, writers, translators, artists, documenters, testers and evangelists. You can also visit our [welcome chat channel](https://webchat.kde.org/#/room/#kde-welcome:kde.org) and talk live to active KDE contributors.

Another way to help KDE is by [donating to KDE](https://kde.org/community/donations/) and helping the Community keep things running.
