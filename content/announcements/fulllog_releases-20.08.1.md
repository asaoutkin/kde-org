---
release: true
releaseDate: 2020-09
title: Release Service 20.08.1 Full Log Page
type: fulllog
version: 20.08.1
hidden: true
---
<h3><a name='akonadi-calendar-tools' href='https://cgit.kde.org/akonadi-calendar-tools.git'>akonadi-calendar-tools</a> <a href='#akonadi-calendar-tools' onclick='toggle("ulakonadi-calendar-tools", this)'>[Hide]</a></h3>
<ul id='ulakonadi-calendar-tools' style='display: block'>
<li>Cmake: explicitly require KF5I18n. <a href='http://commits.kde.org/akonadi-calendar-tools/99bdf7c476332126fe35d1ff1ce9f14563982ecd'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>Fix Bug 425732 - "Show QR Codes" do not working, no QR Codes in the preview of contact. <a href='http://commits.kde.org/akonadi-contacts/7c3ff797637e57204c46b03b84772298c8c1f763'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425732'>#425732</a></li>
</ul>
<h3><a name='akonadiconsole' href='https://cgit.kde.org/akonadiconsole.git'>akonadiconsole</a> <a href='#akonadiconsole' onclick='toggle("ulakonadiconsole", this)'>[Hide]</a></h3>
<ul id='ulakonadiconsole' style='display: block'>
<li>Don't export QT_NO_CPU_FEATURE=sse4.2. <a href='http://commits.kde.org/akonadiconsole/4d2cc20623fbcc4e5f3e98558a9cc9d8725fa0dd'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Use no deprecated enum Qt::MiddleButton. <a href='http://commits.kde.org/akregator/a2d0c30dea3a03b95ad08278e01621f44e901e31'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Pass the ARCHIVE_EXTRACT_SECURE_SYMLINKS flag to libarchive. <a href='http://commits.kde.org/ark/8bf8c5ef07b0ac5e914d752681e470dea403a5bd'>Commit.</a> </li>
<li>Appdata: use canonical help URL. <a href='http://commits.kde.org/ark/ddd3641eecce3e441d38c80c831266538db13a40'>Commit.</a> </li>
</ul>
<h3><a name='audiocd-kio' href='https://cgit.kde.org/audiocd-kio.git'>audiocd-kio</a> <a href='#audiocd-kio' onclick='toggle("ulaudiocd-kio", this)'>[Hide]</a></h3>
<ul id='ulaudiocd-kio' style='display: block'>
<li>Appdata: use canonical help URL. <a href='http://commits.kde.org/audiocd-kio/4188636213791a08004d6e103c0d50a3ef8649ef'>Commit.</a> </li>
</ul>
<h3><a name='baloo-widgets' href='https://cgit.kde.org/baloo-widgets.git'>baloo-widgets</a> <a href='#baloo-widgets' onclick='toggle("ulbaloo-widgets", this)'>[Hide]</a></h3>
<ul id='ulbaloo-widgets' style='display: block'>
<li>Increase min KF5 version to reflect reality. <a href='http://commits.kde.org/baloo-widgets/e774fe5a7c8194efca14e186a6e46950b8a08c18'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix file preview for desktop files with absolute icon paths. <a href='http://commits.kde.org/dolphin/004734e8321778a22a648e73b5d1743b64ca2bda'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423326'>#423326</a></li>
<li>Fix nullptr crash on Windows. <a href='http://commits.kde.org/dolphin/e42f42fa276533e1de4810b4461a084132cbdc70'>Commit.</a> </li>
<li>Fix 'show space' setting not being re-applied. <a href='http://commits.kde.org/dolphin/771b9c8fc25edf9848841d5315ac36d5f09836cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425542'>#425542</a></li>
<li>Fix i18n. <a href='http://commits.kde.org/dolphin/df85f24ecbf3dfb6431f357929328daaf7a59cfb'>Commit.</a> </li>
</ul>
<h3><a name='dolphin-plugins' href='https://cgit.kde.org/dolphin-plugins.git'>dolphin-plugins</a> <a href='#dolphin-plugins' onclick='toggle("uldolphin-plugins", this)'>[Hide]</a></h3>
<ul id='uldolphin-plugins' style='display: block'>
<li>Fix compile with Clang 10.0.1. <a href='http://commits.kde.org/dolphin-plugins/18f486cca90ad71f5e04216c75116331620ebda8'>Commit.</a> </li>
</ul>
<h3><a name='elisa' href='https://cgit.kde.org/elisa.git'>elisa</a> <a href='#elisa' onclick='toggle("ulelisa", this)'>[Hide]</a></h3>
<ul id='ulelisa' style='display: block'>
<li>Fix incorrect mpris2 volume display. <a href='http://commits.kde.org/elisa/755dfe4e8d4c2e727466d49a7d927daf16b764de'>Commit.</a> </li>
<li>When navigating with Elisa's file browser display current directory. <a href='http://commits.kde.org/elisa/ad479bb5e2e893931fa58ad089771923aef7eaa0'>Commit.</a> </li>
<li>Fix clickable zones in header bar to be always maximized. <a href='http://commits.kde.org/elisa/917396e878c0be520c612f87c97a3d46e1edf802'>Commit.</a> </li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Hide]</a></h3>
<ul id='uleventviews' style='display: block'>
<li>Use no deprecated enum Qt::MiddleButton. <a href='http://commits.kde.org/eventviews/4ab534f648eed33cd335e816c39e230b8d386497'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Make "Sort By" menu work properly. <a href='http://commits.kde.org/gwenview/5bcfd5b83ca86cebceb9cc08d5dded00ca6819cb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425739'>#425739</a></li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Show only collection that accept task/todo/note mimetype. <a href='http://commits.kde.org/incidenceeditor/4516624015f351a85bcf898e62d2c0bbd6b8c90e'>Commit.</a> </li>
</ul>
<h3><a name='kaccounts-providers' href='https://cgit.kde.org/kaccounts-providers.git'>kaccounts-providers</a> <a href='#kaccounts-providers' onclick='toggle("ulkaccounts-providers", this)'>[Hide]</a></h3>
<ul id='ulkaccounts-providers' style='display: block'>
<li>Nextcloud: use https for kde.org. <a href='http://commits.kde.org/kaccounts-providers/bbd4de1acdf08e437f88d7413825e26611e70196'>Commit.</a> </li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Move data files into their own directory. <a href='http://commits.kde.org/kalarm/f5a9324390906788f8452f8a6e59776fdb5ee271'>Commit.</a> </li>
<li>Ignore double click on a disabled template type. <a href='http://commits.kde.org/kalarm/8040581f445632862261d9c79afb8080e5bbe28e'>Commit.</a> </li>
<li>Prevent double click selecting a disabled template type. <a href='http://commits.kde.org/kalarm/492353ec7c8263295a4d46a764bd7f9da5e36fb4'>Commit.</a> </li>
<li>Bug 425751: Fix inability to create alarms from templates, or load templates in edit dialog. <a href='http://commits.kde.org/kalarm/36b27c3663b88220dc65aec1062674f53e004c2c'>Commit.</a> </li>
<li>Bug 425751: Don't show spurious error message when deleting alarm template. <a href='http://commits.kde.org/kalarm/9b0ceed34a6772e7b69292e7e9252b1f9078bec1'>Commit.</a> </li>
<li>Set correct version for KCalendarCore library. <a href='http://commits.kde.org/kalarm/93635786b123cb69909fc0da5f6ca8586c5bd28e'>Commit.</a> </li>
<li>Update from KDE4. <a href='http://commits.kde.org/kalarm/15236d052862e387b95b17f57e3e45f816657fb9'>Commit.</a> </li>
</ul>
<h3><a name='kalarmcal' href='https://cgit.kde.org/kalarmcal.git'>kalarmcal</a> <a href='#kalarmcal' onclick='toggle("ulkalarmcal", this)'>[Hide]</a></h3>
<ul id='ulkalarmcal' style='display: block'>
<li>Add missing event attribute. <a href='http://commits.kde.org/kalarmcal/d8945e70ff7f0905fa058a8399201c3b76824428'>Commit.</a> </li>
</ul>
<h3><a name='kamoso' href='https://cgit.kde.org/kamoso.git'>kamoso</a> <a href='#kamoso' onclick='toggle("ulkamoso", this)'>[Hide]</a></h3>
<ul id='ulkamoso' style='display: block'>
<li>Add set attribute "AA_UseHighDpiPixmaps". <a href='http://commits.kde.org/kamoso/b696cd17fc30d8bb8f86c233d41c0b2b3f2e59e3'>Commit.</a> </li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Hide]</a></h3>
<ul id='ulkdebugsettings' style='display: block'>
<li>Don't export QT_HASH_SEED and QT_NO_CPU_FEATURE anymore. <a href='http://commits.kde.org/kdebugsettings/d9016ba12ca30e8248d4c36979042a38dffc1aab'>Commit.</a> </li>
</ul>
<h3><a name='kdeconnect-kde' href='https://cgit.kde.org/kdeconnect-kde.git'>kdeconnect-kde</a> <a href='#kdeconnect-kde' onclick='toggle("ulkdeconnect-kde", this)'>[Hide]</a></h3>
<ul id='ulkdeconnect-kde' style='display: block'>
<li>[kio] Mark Class as local. <a href='http://commits.kde.org/kdeconnect-kde/56d40292470bb25253405b99d94792b72436a4d7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413477'>#413477</a>. See bug <a href='https://bugs.kde.org/425948'>#425948</a></li>
<li>Reload remote commands model when commands change. <a href='http://commits.kde.org/kdeconnect-kde/2be7b3eff1c40b8dc8369cb9c4873ad410f544e2'>Commit.</a> </li>
<li>Define content margins for settings app. <a href='http://commits.kde.org/kdeconnect-kde/17ea9a49d43085f5c5324f5ef023a75023f61ffa'>Commit.</a> </li>
<li>Appdata: use canonical help URL. <a href='http://commits.kde.org/kdeconnect-kde/76fefacc8d560566d92ad4eb53e140fc7d564e9b'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Keep titlebars state (hidden/shown) when switching between layouts. <a href='http://commits.kde.org/kdenlive/813d6ae6cb73fed2dd97a7f429101857723e14f7'>Commit.</a> </li>
<li>Fix insert broken in some configurations (video disabled). <a href='http://commits.kde.org/kdenlive/a508e86cfe93b2628dcaf4c11f90a63375c331b2'>Commit.</a> </li>
<li>Fix compilation. <a href='http://commits.kde.org/kdenlive/c4966aac902bf17a1b97ffa254f122a01471d2a5'>Commit.</a> </li>
<li>Don't resize mixer widget if tabbed. <a href='http://commits.kde.org/kdenlive/d43328bfadce75d84015c46eeabd334e598d9bd6'>Commit.</a> </li>
<li>Fix incorrect stream or no audio on insert. <a href='http://commits.kde.org/kdenlive/5b4a2b800646a9737bfd107c9f543196f68a6402'>Commit.</a> </li>
<li>Update default editing layout. <a href='http://commits.kde.org/kdenlive/055abf4d93880ff852c09272ef3db47ed68dfac4'>Commit.</a> </li>
<li>Fix audio only insert broken. <a href='http://commits.kde.org/kdenlive/89d6efe48b2a58963194af0e8a512f9d253083a1'>Commit.</a> </li>
<li>Correctly update project duration on group move. <a href='http://commits.kde.org/kdenlive/6289435ad6f2893fc55d6e6e6899a5b4a3514211'>Commit.</a> </li>
<li>Raise Project Bin when a clip is dropped in timeline or created through the menu. <a href='http://commits.kde.org/kdenlive/0edce07385fa67cb2aa68754d609a2a79ae3eda0'>Commit.</a> </li>
<li>Correctly replace slideshow folder if moved. <a href='http://commits.kde.org/kdenlive/9de3b4dc714e358dbebb7bffc114b0ac9ec42c66'>Commit.</a> </li>
<li>Fix a spelling error. <a href='http://commits.kde.org/kdenlive/16e0cc2934cf315205b28c5f40028edf5ec4e497'>Commit.</a> </li>
<li>Fix shift click for multiple selection broken in Bin. <a href='http://commits.kde.org/kdenlive/dd5fcbf66befbb9f6506d1f7266d8ffad6ebec9a'>Commit.</a> </li>
</ul>
<h3><a name='kget' href='https://cgit.kde.org/kget.git'>kget</a> <a href='#kget' onclick='toggle("ulkget", this)'>[Hide]</a></h3>
<ul id='ulkget' style='display: block'>
<li>Port to ecm_add_test to actually run the tests. <a href='http://commits.kde.org/kget/45735cfae102a1ba616cf3385101606e5c9effc9'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Don't specify an image viewer when opening a key's photo. <a href='http://commits.kde.org/kgpg/a9f09321ed0244c623fc18ec0264eda1964cd512'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400324'>#400324</a></li>
</ul>
<h3><a name='kio-gdrive' href='https://cgit.kde.org/kio-gdrive.git'>kio-gdrive</a> <a href='#kio-gdrive' onclick='toggle("ulkio-gdrive", this)'>[Hide]</a></h3>
<ul id='ulkio-gdrive' style='display: block'>
<li>Appstream: use canonical help URL. <a href='http://commits.kde.org/kio-gdrive/4c548da9b4625df17452ec901cb43f24ec99b179'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix crash when we call "save attachments". <a href='http://commits.kde.org/kmail/0b8cfef8b5407cb8d1f7afcf4384d1f0decbf09d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425532'>#425532</a></li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Hide]</a></h3>
<ul id='ulknotes' style='display: block'>
<li>Use no deprecated enum Qt::MiddleButton. <a href='http://commits.kde.org/knotes/597ed63eef4df3b3a38d6d4f95842b70fd01d1b0'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Add OARS. <a href='http://commits.kde.org/konsole/c7cf6e3f640075ebcdcd1ddeef213b60cce6783c'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>I18n: rename mimetype catalog to kpat_xml_mimetypes.pot. <a href='http://commits.kde.org/kpat/3673c4dd5aaa453897795bf375af2ccc6d0e24af'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>[kontact] [Bug 425842] New: kpimtextedit crash in KMail. <a href='http://commits.kde.org/kpimtextedit/cb381de91a9bbc676b180ce4a33c7211ea176a25'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425842'>#425842</a></li>
<li>EmoticonTextEditItem: avoid emitting dataChanged. <a href='http://commits.kde.org/kpimtextedit/63d1d905af0999541390275c95ad35a9700a76ad'>Commit.</a> </li>
</ul>
<h3><a name='ktuberling' href='https://cgit.kde.org/ktuberling.git'>ktuberling</a> <a href='#ktuberling' onclick='toggle("ulktuberling", this)'>[Hide]</a></h3>
<ul id='ulktuberling' style='display: block'>
<li>Fix --help. <a href='http://commits.kde.org/ktuberling/994ac0e756f4905b4723f71d050b2c27e1fc57ea'>Commit.</a> </li>
</ul>
<h3><a name='kwalletmanager' href='https://cgit.kde.org/kwalletmanager.git'>kwalletmanager</a> <a href='#kwalletmanager' onclick='toggle("ulkwalletmanager", this)'>[Hide]</a></h3>
<ul id='ulkwalletmanager' style='display: block'>
<li>Fix memory leak. <a href='http://commits.kde.org/kwalletmanager/848ee2643e84fa9340a7dfe4946967945efad50d'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Allow non-existing file to be set as glossary. <a href='http://commits.kde.org/lokalize/8041c76b5ee762c121dea1f40b90a0190b646518'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Use no deprecated enum Qt::MiddleButton. <a href='http://commits.kde.org/mailcommon/02d9b70249eebcb68324c36135fd79ca321f2264'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix Bug 425219 - suspicious URL warning system gives false positive for URL with port number. <a href='http://commits.kde.org/messagelib/7f2505d60629a2998518e5cc36e04e4ca6529bda'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425219'>#425219</a></li>
<li>Use no deprecated enum Qt::MiddleButton. <a href='http://commits.kde.org/messagelib/1ad12372745be1da977d706ca58422a5b04b39b3'>Commit.</a> </li>
<li>Add name when we forward as attachment. <a href='http://commits.kde.org/messagelib/681a44e3acf0820ecfb6daa0737b1bb1a6f8da2e'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Using no deprecated enum Qt::MiddleButton. <a href='http://commits.kde.org/okular/69776168e2fc8144684fda9c8917dbc583feb906'>Commit.</a> </li>
<li>Improve robustness of code against corrupted configurations. <a href='http://commits.kde.org/okular/bc2d22910ac3da9fbc4bad8ea3925a4e195252f1'>Commit.</a> </li>
<li>Store the state of the builtin annotations in a new config key. <a href='http://commits.kde.org/okular/10d92fbeda39d3b177b0681e68ff1f5ea6b99270'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425354'>#425354</a></li>
<li>BuildPen: honor annotation opacity. <a href='http://commits.kde.org/okular/1c7c7275e1fc38f304d03589e034a550e45e4c3f'>Commit.</a> </li>
<li>PagePainter: Don't draw Arrow/Line annotations with multipyl. <a href='http://commits.kde.org/okular/389c7b0235fe762cda799ad4ce8bfbe710bef210'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425075'>#425075</a></li>
</ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Hide]</a></h3>
<ul id='ulpimcommon' style='display: block'>
<li>It marks as deprecated in qt5.15 branch. <a href='http://commits.kde.org/pimcommon/e1f1093d982e52206872b4874d8b4a4614b3d648'>Commit.</a> </li>
<li>Modernize code. <a href='http://commits.kde.org/pimcommon/e91ddcc839dd235fc061760d4b3c062802a53a48'>Commit.</a> </li>
<li>Optimization: don't call cleanupEmailList each time that we add a new. <a href='http://commits.kde.org/pimcommon/4e6ca05959ef0d26b93150d4645d257c1c47d672'>Commit.</a> </li>
<li>Optimization: Don't calling cleanupEmailList twice when we loadcontacts. <a href='http://commits.kde.org/pimcommon/f8492c6da02ca234049860164be57e055b22fde5'>Commit.</a> </li>
<li>Fix create addr. dont reuse same addr otherwise we will create a. <a href='http://commits.kde.org/pimcommon/d492cab67bfc5f83b1360bb7e1e99547e2c5ce27'>Commit.</a> </li>
<li>Add missing Q_REQUIRED_RESULT. <a href='http://commits.kde.org/pimcommon/28f1c516ae014ba7555b908f5d68def6ddb4794e'>Commit.</a> </li>
<li>Optimization: don't recreate on stack PimCommon::BalooCompletionEmail. <a href='http://commits.kde.org/pimcommon/a479eca2345e02bcb8ad17424549335a288a20a2'>Commit.</a> </li>
<li>Add missing Q_REQUIRED_RESULT. <a href='http://commits.kde.org/pimcommon/4fc0bfc75423524397dbc3e3f685825cf5284f39'>Commit.</a> </li>
<li>Const'ify variable. <a href='http://commits.kde.org/pimcommon/927d9c4e9301c1215547e65da804b0768b3539a2'>Commit.</a> </li>
<li>Minor clean up code. <a href='http://commits.kde.org/pimcommon/9050dc5757d55f72b2da35bdd8b91ecad3cb54f8'>Commit.</a> </li>
<li>Continue to modernize code. <a href='http://commits.kde.org/pimcommon/2cb0b035753833f4a147e4f9dfcbbfc44106a15e'>Commit.</a> </li>
<li>Clean up code. <a href='http://commits.kde.org/pimcommon/54275adb8d9659e9f427af6a945d261528ffde0f'>Commit.</a> </li>
<li>Don't export QT_NO_CPU_FEATURE=sse4.2. <a href='http://commits.kde.org/pimcommon/7164f97530628d8037665be4842342de00658b37'>Commit.</a> </li>
</ul>
<h3><a name='rocs' href='https://cgit.kde.org/rocs.git'>rocs</a> <a href='#rocs' onclick='toggle("ulrocs", this)'>[Hide]</a></h3>
<ul id='ulrocs' style='display: block'>
<li>Adapt test to default edge type. <a href='http://commits.kde.org/rocs/f2bf4a85f38f60bc2f35b9b83419e302eca5bc18'>Commit.</a> </li>
<li>Fix Rocs2 graph document import of edge types. <a href='http://commits.kde.org/rocs/6c0b205e70b4945647174d0664012932526c27c0'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Compile fix. <a href='http://commits.kde.org/umbrello/0724d67ff8157eca8e51d3baef0b8e9711282107'>Commit.</a> See bug <a href='https://bugs.kde.org/425281'>#425281</a></li>
<li>Fix 'No display of foreign key constraints when opening a file'. <a href='http://commits.kde.org/umbrello/61536141ba6247ce05f7ce85fd44584e405439bc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425285'>#425285</a></li>
<li>Fix 'Double association in the diagram after renaming a foreign key constraint'. <a href='http://commits.kde.org/umbrello/952091eba84977f1aad3c39be3d08f98a3abb2fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425281'>#425281</a></li>
</ul>
