---
title: KDE 2 Launch Pad
date: "2000-07-29"
description: In a modest attempt to assist new users as well as devoted supporters, the developers of KDE Team prepared a series of documents presenting the most interesting technologies and the most important improvements that the KDE code acquired.
---

Immediatly after <a href="../announce-1.0">releasing KDE-1.0</a>, the community of K Desktop developers started to work hard on the new code base, that is now, after two years, in preparation for release.

Much of the central code has been rewritten, many exciting features have been added. The number and quality of changes is dramatic. Users of the new, best computer desktop software will need time and patience to discover all the wealth of interesting features and functionalities.

In a modest attempt to assist new users as well as devoted supporters, the developers of KDE Team prepared a series of documents presenting the most interesting technologies and the most important improvements that the KDE code acquired.

First, an <a href="../k3b-announce">overall view</a>, as gathered during the Third KDE Developers Forum in Trysil, Norway, is available.

More details about <a href="../k3c-announce">graphical enhancements and look-and-feel improvements</a> is kindly provided by the KDE Artist Team and the Styles And Themes special interest group members.

The KMail developers are now making public a <a href="http://devel-home.kde.org/~kmail/kmailreview">review</a> of what will be the greatest stable release of KDE's mail client.

Enjoy. And if you're a beta tester, liked something about the KDE Beta releases and want to review this something, please contact [KDE's How To Help Office](mailto:howtohelp@kde.org) :-).

