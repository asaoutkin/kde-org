---
title: Release Service 20.12.2 Full Log Page
type: fulllog
version: 20.12.2
release: true
releaseDate: 2021-02
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Fix Bug 432307 - Cancel drag'n drop does not work. [Commit.](http://commits.kde.org/akonadi/b73096c979ffc8410aa484c657d936ac00bbd1f5) Fixes bug [#432307](https://bugs.kde.org/432307)
+ Add autotest for Item::setTags when creating instances. [Commit.](http://commits.kde.org/akonadi/f52bccec1424eb52dbac09f6050acb359f3ce9e5) 
+ When tags are overwriien, get them from tags(). [Commit.](http://commits.kde.org/akonadi/4d3b2a588f561cd099f19c1166b2ca884f43e9c3) 
+ Remove unsused method in Config. [Commit.](http://commits.kde.org/akonadi/a90b536ebc2d04f61032a613ac878ae60a5a1d5c) 
+ Use lazy global initialization of Config singleton. [Commit.](http://commits.kde.org/akonadi/33976aa6c365bb948c1f9413151d1c9a0f8240b7) 
+ Remove unsused method in Config. [Commit.](http://commits.kde.org/akonadi/ac598cbfe0203f30091236d42e21e82b98818e81) 
+ Use lazy global initialization of Config singleton. [Commit.](http://commits.kde.org/akonadi/a9409d6bfe482bf21c61493c40b037fa8cc47f42) 
+ When tags are overwriien, get them from tags(). [Commit.](http://commits.kde.org/akonadi/e23117dbe9d59d2e770f374f1ed8b5e5f291da65) 
+ Fix bug when deleting the last tag. [Commit.](http://commits.kde.org/akonadi/b71307a40fb0c3919191e11bfcfc5cc34a40bf57) Fixes bug [#431297](https://bugs.kde.org/431297)
+ Fix Bug 430787 - When moving an email into a subfolder on KDE Plasma Wayland the context menu does not appear at the target subfolder. [Commit.](http://commits.kde.org/akonadi/6cfce924ed79d8860e99e53291a95e4bb21adbe6) Fixes bug [#430787](https://bugs.kde.org/430787)
{{< /details >}}
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Allow instance creation using an item. [Commit.](http://commits.kde.org/akonadi-calendar/46bc0e61ee08a7f506385e33dab3d563b1135597) 
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Show 'Compress' menu on multiple selected archives of the same type. [Commit.](http://commits.kde.org/ark/f3055fa41e6eea735a0f9c4d82c80c57b7a43e53) Fixes bug [#430086](https://bugs.kde.org/430086)
+ Fix crash after closing the window while loading a TAR archive. [Commit.](http://commits.kde.org/ark/a41e69b30cc07dd758849f8685d322150459e4f1) Fixes bug [#410092](https://bugs.kde.org/410092)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Don't crash when building without baloo. [Commit.](http://commits.kde.org/dolphin/5ed12ed44a7169855d46486d17c3e18697c3c4ae) Fixes bug [#431730](https://bugs.kde.org/431730)
+ Show button to open knetattach inline with URL nav on remove:// view. [Commit.](http://commits.kde.org/dolphin/33270dd4423eea329b8f20db17f8d70ab7b108da) Fixes bug [#431626](https://bugs.kde.org/431626)
+ Fix folder size calculation on FUSE and network file systems. [Commit.](http://commits.kde.org/dolphin/a6d095fa0450237f89e240da99877f6c79b46ecc) Fixes bug [#430778](https://bugs.kde.org/430778). Fixes bug [#431106](https://bugs.kde.org/431106)
+ Change copy location shortcut in dolphin so as to not conflict with Konsole panel. [Commit.](http://commits.kde.org/dolphin/764861af51ec0a4f124886567725f9950961f6e6) Fixes bug [#426461](https://bugs.kde.org/426461)
{{< /details >}}
{{< details id="dolphin-plugins" title="dolphin-plugins" link="https://commits.kde.org/dolphin-plugins" >}}
+ [Mount ISO Action] Optimize getDeviceFromBackingFile. [Commit.](http://commits.kde.org/dolphin-plugins/94e2efd5ce2f0bc0e12abb6b8f7390f2be83b064) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Avoid concurrent access when fetching lyrics. [Commit.](http://commits.kde.org/elisa/e2845b589170de36851ff37d36d76697adde7440) Fixes bug [#431422](https://bugs.kde.org/431422)
+ Fix "show current track" button. [Commit.](http://commits.kde.org/elisa/afaf25d815763728ac842922dd422377f66079dc) Fixes bug [#431602](https://bugs.kde.org/431602)
+ Ensure the configuration dialog is not dirty when just open. [Commit.](http://commits.kde.org/elisa/d825da3dd869dc2cec45a80ecd6351bc241fafcc) Fixes bug [#430743](https://bugs.kde.org/430743)
+ Avoid printing useless logs on console when power management is working. [Commit.](http://commits.kde.org/elisa/c2b3d0d312ac20c0aa1a4e7a34a765484c420cc3) Fixes bug [#421660](https://bugs.kde.org/421660)
+ Fix enqueue from files explorer. [Commit.](http://commits.kde.org/elisa/6951479520ac241072ac3f03a310fff522579453) Fixes bug [#428218](https://bugs.kde.org/428218)
+ Fix Radio streams. [Commit.](http://commits.kde.org/elisa/7b0cf1da0cfd916c5425e36c7f51bdf41c0322b5) 
+ Use a QFlags to handle entities to watch for file system changes. [Commit.](http://commits.kde.org/elisa/662f88b78a7ee3bef27e584b00ad3ca3c300cf62) See bug [#409587](https://bugs.kde.org/409587)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Fix visual artifacts when panning zoomed in image on HiDPI. [Commit.](http://commits.kde.org/gwenview/955df5addac77b49c8349e6477806dbf27bde238) Fixes bug [#417342](https://bugs.kde.org/417342). Fixes bug [#428397](https://bugs.kde.org/428397). Fixes bug [#414776](https://bugs.kde.org/414776)
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Large directories were not being added to data projects with all their contents. [Commit.](http://commits.kde.org/k3b/38f5449b8ba2b4b106bab691a578e2a66c3aa0ad) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Don't preset alarm name when creating new alarm from a template. [Commit.](http://commits.kde.org/kalarm/1204787729ebcb2619325455c329cc8d08c8aaa2) 
+ Set version requirement for KAlarmCal. [Commit.](http://commits.kde.org/kalarm/65708c91bcc80c7f9e582a3aebdb0d6d5d835fb9) 
+ Fix build. [Commit.](http://commits.kde.org/kalarm/3c9863276d1f9b8e4c60f68e50cfa62e09b26175) 
+ Fix hang when an alarm is triggered in a read-only resource. [Commit.](http://commits.kde.org/kalarm/589d8dcfe9691d650d31d34e6eef3b800e7b2ebf) 
+ Default to using alarm names. [Commit.](http://commits.kde.org/kalarm/eba9ae0946c5df4163064c1d1264f934efddc444) 
{{< /details >}}
{{< details id="kalarmcal" title="kalarmcal" link="https://commits.kde.org/kalarmcal" >}}
+ Add setName() method. [Commit.](http://commits.kde.org/kalarmcal/eee4ee046153eb53d49eb9c1c7f92499234a52d2) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Fix notifications in the KDE Connect plasmoid. [Commit.](http://commits.kde.org/kdeconnect-kde/fbc827e373a6f1520abe4fc8f7cedc1dbc07551c) Fixes bug [#432337](https://bugs.kde.org/432337)
+ UseHighDpiPixmaps in kdeconnectd and kdeconnect-indicator. [Commit.](http://commits.kde.org/kdeconnect-kde/dd2cb98e4e8c65eb589611e2ab3dcf3bba392dbf) 
+ Remove OnlyShowIn from daemon desktop file. [Commit.](http://commits.kde.org/kdeconnect-kde/f582871d80dc6cb99f33af6e2ca64036e27fc3d3) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Allow resizing unselected subtitles. [Commit.](http://commits.kde.org/kdenlive/d4b9ee6528f7de95a77427a52f010ba2808cee96) 
+ Remove env variable breaking UI translation. [Commit.](http://commits.kde.org/kdenlive/9619b3f5108ec99aded66d3102e844e4debcc6bf) 
+ Fix clip with mix transition cannot be cut in some circumstances. [Commit.](http://commits.kde.org/kdenlive/c3bae8e416054abc92b965c554166fadde4b0424) 
+ Ensure all track tags have the same width if more than 10 tracks. [Commit.](http://commits.kde.org/kdenlive/5adf0dcb309353a9c2e8b58c1009c05f6460ec5a) 
+ Fix rendering uses wrong locale, resulting in broken slowmotion in render and possibly other issues on some locales. [Commit.](http://commits.kde.org/kdenlive/b9aa046df30a15315f7871971c95dffb3d0863ca) 
+ Expose proxy info in playlist clip properties (to allow delete, etc). [Commit.](http://commits.kde.org/kdenlive/0b60b1e999627ecd8015b83c78320af7b467d2fe) 
+ Fix proxied playlists rendering blank and missing sound. [Commit.](http://commits.kde.org/kdenlive/617c3acbbb8252a50a90c0c90f14f94ea63f7fb8) 
+ Fix playlist proxies broken. [Commit.](http://commits.kde.org/kdenlive/c05f98274fabc748cb3a0da28196fcaeeb8a716e) 
+ Fixed issue where changing speed resets audio channel of clip to channel 1. [Commit.](http://commits.kde.org/kdenlive/7c85faf53e42ad2a33f7ab9ec58eabefe881e37e) 
+ Ensure color/image/title clips parent producer always has out set as the longest duration of its timeline clips. [Commit.](http://commits.kde.org/kdenlive/84aea416f80568e85fb1003e1278eade782fe91d) 
+ Ensure clips have an "unnamed" label if name is empty. [Commit.](http://commits.kde.org/kdenlive/b331578c0935b45b166d25eab9125044e7d77423) 
+ Effect keyframe minor fixes (improve hover color and allow pasting param to keyframe 0). [Commit.](http://commits.kde.org/kdenlive/3316f377a26c6bec3792cc53b88b2897d1aab585) 
+ Re-enable audio playback on reverse speed. [Commit.](http://commits.kde.org/kdenlive/9d4018a64f81ef36d8ace73cad55585eeec1c73c) 
+ Fix changing speed breaks timeline focus. [Commit.](http://commits.kde.org/kdenlive/6a4be05821365ed4d126dc75739864155e50e69f) 
+ Ensure a group/ungroup operation cannot be performed while dragging / resizing a group. [Commit.](http://commits.kde.org/kdenlive/cb7ee64327b215ef96aea3450c0f87ca2440ffd7) 
+ Cleanup monitor overlay toolbars and switch to QtQuick2 only. [Commit.](http://commits.kde.org/kdenlive/dadd06f64026feb0f2878ac37121e94f4eab7432) 
+ Improve show/hide monitor toolbar (ensure it doesn't stay visible when mouse exits monitor). [Commit.](http://commits.kde.org/kdenlive/cb9cba9d84628a8543f311da6a5476a5a77fc954) 
+ Correctly disable subtitle widget buttons when no subtitle is selected, add button tooltips. [Commit.](http://commits.kde.org/kdenlive/4291b1113ac37b8f014a7b680e5f9247313227ea) 
+ Fix lift value incorrect on click. [Commit.](http://commits.kde.org/kdenlive/fb0e09bad2cc3e7a65bcc704f82050a346609677) Fixes bug [#431676](https://bugs.kde.org/431676)
+ Update render target when saving project under a new name. [Commit.](http://commits.kde.org/kdenlive/3b5b1d4fb6121003678ab36b382c2432f04ffe39) 
+ Improve and fix ressource manager, add option to add license attribution. [Commit.](http://commits.kde.org/kdenlive/ed5c91d5ac307c28c84b322fcf9a394a83eb466f) 
+ Fix some crashes on subtitle track action. [Commit.](http://commits.kde.org/kdenlive/f104d8aca641a48b21f19d165face861a7953e37) 
+ Set range for zoome of avfilter.zoompan to 1-10 (effect doesn’t support. [Commit.](http://commits.kde.org/kdenlive/15a2ce8149bdb6759f74d003e999b2b547ea2de9) 
+ Improve subtitle track integration: add context menu, highlight on active. [Commit.](http://commits.kde.org/kdenlive/b8afadbaba53444f5d57353dbeafd3461de81daa) 
+ Fix incorrect arguments parsing on app restart. [Commit.](http://commits.kde.org/kdenlive/30ec7007055af454ecba3e72c04e65aac28f49f9) 
+ Fix build. [Commit.](http://commits.kde.org/kdenlive/92a64e79f3efe8805c24a742a934119818080f27) 
+ Attempt to fix subtitle encoding issue. [Commit.](http://commits.kde.org/kdenlive/6d185ecae7edf61281dd3b69767c57107ee6491e) 
+ Fix recent regression (crash moving clip in timeline). [Commit.](http://commits.kde.org/kdenlive/ab66afb851339bb002ea104fb2a821282582d8c0) 
+ Fix subtitles not displayed on project opening. [Commit.](http://commits.kde.org/kdenlive/6d5bd03dc5d96748f0d01fef6e98f435445adf8d) 
+ Update copyright year to 2021. [Commit.](http://commits.kde.org/kdenlive/77a5a5cee103b917b2864759116ea06fb7059e78) 
+ Fix disabled clip regression (color and opacity changes were not applied anymore). [Commit.](http://commits.kde.org/kdenlive/ffb46fdef81bbdce33e2278d7be874993148e807) 
+ Fix compilation. [Commit.](http://commits.kde.org/kdenlive/ad01afc905c0c14fc3a4039b40f39b19ee07ae93) 
+ Delete equalizer.xml. [Commit.](http://commits.kde.org/kdenlive/ded6dbf92d990e487cff7578692af6db92210794) 
+ Delete eq.xml. [Commit.](http://commits.kde.org/kdenlive/81d5acce0758385d427754013a5fa96146c05e54) 
+ Delete selectivecolor.xml. [Commit.](http://commits.kde.org/kdenlive/90b8665a8ffa86674563c230d7556e1a6cdc478a) 
+ Delete unsharp.xml. [Commit.](http://commits.kde.org/kdenlive/84968140418dde1d54fb70c0b6a2e77a510e7622) 
+ Dragging an effect from a track to another should properly activate icon and create an undo entry. [Commit.](http://commits.kde.org/kdenlive/e5780eee961b51938ad205bda65e38d6024decb6) 
+ Always keep timeline cursor visible when seeking with keyboard, not only when "follow playhead when playing is enabled". [Commit.](http://commits.kde.org/kdenlive/c25fa35a4cf6fa64af9da021bce152cdc9e61554) 
+ Fix broken Freesound login and import. [Commit.](http://commits.kde.org/kdenlive/ed9daa2089fc11b820dedff26e8f23ab649c32a1) 
+ Fix regression in subtitle resize. [Commit.](http://commits.kde.org/kdenlive/3cfaf1a514980571cd9a932d49fcfd7b6ee4eba8) 
+ Fix clips incorrectly resized on move with mix. [Commit.](http://commits.kde.org/kdenlive/def2096267ef1569de0cc10703043e27c98704ff) 
+ Fix grouped clips independently resized when resizing the group. [Commit.](http://commits.kde.org/kdenlive/a7d819551b7d77109498ce570b260f962c3f230a) 
+ Implement missing subtitle copy/paste. [Commit.](http://commits.kde.org/kdenlive/fcb241737316cdec7997c1aba761e21b656d2dcd) Fixes bug [#430843](https://bugs.kde.org/430843)
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix rendering problem. [Commit.](http://commits.kde.org/kdepim-addons/1ee37513d82ce40561792f42cdae8ddbd6b498d0) 
{{< /details >}}
{{< details id="kget" title="kget" link="https://commits.kde.org/kget" >}}
+ The New Download button had an empty layout. [Commit.](http://commits.kde.org/kget/e9ec5649d40ab31c949fb1ec1fcc83df385e7e82) 
+ Don't create dangling iterators in TransferMultiSegKio. [Commit.](http://commits.kde.org/kget/08b7a4dffe460db5e1482f4f6e7313ffaa56141a) 
+ Initialize Transfer::m_runningSeconds. [Commit.](http://commits.kde.org/kget/61e5c803c19218a3f1862d050f3374de9271dba7) 
{{< /details >}}
{{< details id="kig" title="kig" link="https://commits.kde.org/kig" >}}
+ Prevent crash during construction of circle by three points. [Commit.](http://commits.kde.org/kig/fcabe991f637150921d15c5aa2ba6d0068a909b8) Fixes bug [#430884](https://bugs.kde.org/430884)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Don't log passwords. [Commit.](http://commits.kde.org/kio-extras/6c099045f6f67069ad7b5f4deb6873cf5f34a4f4) 
{{< /details >}}
{{< details id="kiten" title="kiten" link="https://commits.kde.org/kiten" >}}
+ Fix addItem() group name in ConfigDictionarySelector::updateWidgets(). [Commit.](http://commits.kde.org/kiten/47c688fa73badf22248580b5e9d3ef7e10493216) Fixes bug [#418108](https://bugs.kde.org/418108)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Handle Accor reservation without a hotel phone number. [Commit.](http://commits.kde.org/kitinerary/14ff53bd615c8b6efc6db68e5d58d609b5f5452e) 
{{< /details >}}
{{< details id="knotes" title="knotes" link="https://commits.kde.org/knotes" >}}
+ Clean up CMakeLists.txt (remove duplicate entries). [Commit.](http://commits.kde.org/knotes/641a3dcbd451b20e52f7d5d763bb6d7fc5091c7d) 
+ Fix Bug 431126 - KNotes cannot be configured using the GUI. [Commit.](http://commits.kde.org/knotes/7be8013bed25c6a55ad1641da3c1ab35ba5b289d) Fixes bug [#431126](https://bugs.kde.org/431126)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix the handbook URL in the maintainer README. [Commit.](http://commits.kde.org/konsole/c584fed88896b0f91986943fcf7423e8371cf740) 
+ Fix crash exiting all tabs at the same time. [Commit.](http://commits.kde.org/konsole/fc7850c3e396400e82ad99bacaf9e4f825c083a9) Fixes bug [#431827](https://bugs.kde.org/431827)
+ Fix "Remember window size" not working when unchecked. [Commit.](http://commits.kde.org/konsole/8fa1c4b508b080f958a16d52a1e03bcf9e7e4495) Fixes bug [#427610](https://bugs.kde.org/427610)
{{< /details >}}
{{< details id="kontact" title="kontact" link="https://commits.kde.org/kontact" >}}
+ Some appdata checkers complain if caption ends with . [Commit.](http://commits.kde.org/kontact/eb15e5665e0cabf662bc7649b74db22db2ace012) 
+ Add OARS content rating. [Commit.](http://commits.kde.org/kontact/803e3499752b89885413c504ef4d7de2609a39ab) 
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Fix out of bounds read when parsing fstab. [Commit.](http://commits.kde.org/kpmcore/dc38cb119608d0e48bc1492573aa732019e2b6a5) Fixes bug [#429191](https://bugs.kde.org/429191)
{{< /details >}}
{{< details id="krfb" title="krfb" link="https://commits.kde.org/krfb" >}}
+ Pipewire: Support BGRA. [Commit.](http://commits.kde.org/krfb/f041cdf09509c99686eb819f76c5dd81e7d57d74) 
{{< /details >}}
{{< details id="kwave" title="kwave" link="https://commits.kde.org/kwave" >}}
+ Unbreak translations. [Commit.](http://commits.kde.org/kwave/1836dfc6ca93c55712229c946e1fde6435936a4d) Fixes bug [#431281](https://bugs.kde.org/431281)
{{< /details >}}
{{< details id="libkgapi" title="libkgapi" link="https://commits.kde.org/libkgapi" >}}
+ Don't reset account scopes if not necessary. [Commit.](http://commits.kde.org/libkgapi/d81d247026ac993ac5717ca471c7fcf1778951f2) Fixes bug [#429406](https://bugs.kde.org/429406)
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Connect before calling code. [Commit.](http://commits.kde.org/messagelib/bfff80558f9f5e51ee34c14559bfd85204890faf) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Reload xml to update shortcuts when they change in other parts. [Commit.](http://commits.kde.org/okular/2d69bd6e3a8a59642be58f1d285e55c3df8b6a67) 
+ Fix an unexpected file type being the default on open (non Plasma desktops). [Commit.](http://commits.kde.org/okular/23af5aae7f9e90a138053ac5bdd797cb728218c6) Fixes bug [#430728](https://bugs.kde.org/430728)
+ We're using clang format 11 on CI now. [Commit.](http://commits.kde.org/okular/d0f4c3120e4268cdaf3e95860718e41224a0895c) 
+ CI: Switch to new clang-format. [Commit.](http://commits.kde.org/okular/4880e609af65d3cf0a10d71eec72d3013945fcc9) 
{{< /details >}}
{{< details id="partitionmanager" title="partitionmanager" link="https://commits.kde.org/partitionmanager" >}}
+ README.md: use the official screenshot. [Commit.](http://commits.kde.org/partitionmanager/78ebf9d91b19599a68a40056bbb5028110f0ac63) 
{{< /details >}}
{{< details id="pim-data-exporter" title="pim-data-exporter" link="https://commits.kde.org/pim-data-exporter" >}}
+ Don't copy empty exporteddata information file. [Commit.](http://commits.kde.org/pim-data-exporter/620d7227f3257faffd5ffcc32cdcc28612d0fe76) Fixes bug [#429895](https://bugs.kde.org/429895)
+ Fix store archive info. [Commit.](http://commits.kde.org/pim-data-exporter/30fc703e7310d372a03ea622e0f397e19c43223e) See bug [#429895](https://bugs.kde.org/429895)
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ If region capture is cancelled, close Spectacle. [Commit.](http://commits.kde.org/spectacle/a232723b35a1c2c2d8f27f6bf4a59f2ff3775863) Fixes bug [#432006](https://bugs.kde.org/432006)
+ Fix lookup of ComparableQPoint keys in QMap. [Commit.](http://commits.kde.org/spectacle/2ca0ba662673928598776b65ab5d62ad00c244fd) Fixes bug [#430942](https://bugs.kde.org/430942)
+ Don't translate property name. [Commit.](http://commits.kde.org/spectacle/ed60ec35b32b1860dc9722008a9a846ac0201847) Fixes bug [#431557](https://bugs.kde.org/431557)
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Umbrello/umlscene.cpp: Fix crash on exit with widget selected in diagram. [Commit.](http://commits.kde.org/umbrello/d5b467cbafd94e91196f7667d858f9a2c4e04c34) 
{{< /details >}}
