---
aliases:
- ../../plasma-5.12.1
changelog: 5.12.0-5.12.1
date: 2018-02-13
layout: plasma
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma/5/5.12.0/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

{{% i18n_date %}}

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.12.1" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in February with many feature refinements and new modules to complete the desktop experience." "5.12" >}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- System Settings: Fix crash when searching. <a href="https://commits.kde.org/systemsettings/d314bce549f63735e1746101aaae8880011b6704">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D10272">D10272</a>
- Fixed mouse settings module crashing on Wayland. <a href="https://commits.kde.org/plasma-desktop/483565374f7992a087585bbf5af55ab05b60d212">Commit.</a> Fixes bug <a href="https://bugs.kde.org/389978">#389978</a>. Phabricator Code review <a href="https://phabricator.kde.org/D10359">D10359</a>
- Show a beautiful disabled icon for updates. <a href="https://commits.kde.org/discover/d7d7904b5a8e8cca03216907f1b3ee0707aa0f08">Commit.</a> Fixes bug <a href="https://bugs.kde.org/390076">#390076</a>