---
aliases:
- ../../plasma-5.9.5
changelog: 5.9.4-5.9.5
date: 2017-04-25
layout: plasma
---

{{%youtube id="lm0sqqVcotA"%}}

{{<figure src="/announcements/plasma/5/5.9.0/plasma-5.9.png" alt="KDE Plasma 5.9 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.9">}}

{{% i18n_date %}}

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.9.5" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in January with many feature refinements and new modules to complete the desktop experience." "5.9" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Plastik window decoration now supports global menu. <a href="https://commits.kde.org/kwin/3e0ddba683ca68cb50b403cb3893fa2fc9d2d737">Commit.</a> See bug <a href="https://bugs.kde.org/375862">#375862</a>. Phabricator Code review <a href="https://phabricator.kde.org/D5131">D5131</a>
- Media Controller can now properly handle and seek long tracks (&gt; 30 minutes). <a href="https://commits.kde.org/plasma-workspace/550860f6366cc99d3f0ff19f74fd3fc3d1bfc0ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/377623">#377623</a>
- Sort the themes in decoration KCM. <a href="https://commits.kde.org/kwin/f5a43877a9ea6ddad9eaa8d7498c8ea518c29c81">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D5407">D5407</a>