---
title: "Plasma 5.21"
subtitle: "We made something pretty"
desc: Plasma 5.21 is all about upgrading the looks and usability of Plasma.
description: "Plasma 5.21 is a looker! A new wallpaper, a new theme, and especially a new app launcher
  make Plasma not only easier on the eyes, but also easier to use and much more accessible.

  Read on to discover everything that make 5.21 the most stunning Plasma yet!"
version: "5.21.0"
release: "plasma-5.21.0"
date: "2021-02-16T12:00:00Z"
images:
 - /announcements/plasma/5/5.21.0/breeze-twilight.webp
layout: plasma-5-21
sassFiles:
  - /sass/plasma-5-21.scss
features:
  - title: "Discover"
    content: "Support unattended updates"
    logo: https://apps.kde.org/app-icons/org.kde.discover.svg
  - title: "KRunner"
    content: "Pin KRunner (doesn't close automatically)"
    logo: /breeze-icons/krunner.svg
  - title: "Digital Clock"
    content: "Better support for timezones"
    logo: /breeze-icons/kalarm.svg
  - title: "Sound Applet"
    content: "The sound applet now displays the live microphone volume"
    logo: /breeze-icons/sound.svg
licenses:
  - SPDX-FileCopyrightText: 2021 Carl Schwan <carlschawn@kde.org>
  - SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
---

{{< container >}}

{{< youtube "ahEWG4JCA1w" >}}

{{< feature src="../5.21.0/kickoff.webp" class="round-top-right" >}}

## New Application Launcher

Find, reach and run your apps faster and easier than ever with Plasma's
new app launcher.

The new launcher features two panes to make it simple to locate your programs and
comes with improved keyboard, mouse, and touch input, boosting accessibility across
the board.

Support for languages with right-to-left writing (such as Japanese and Arabic) has also
improved. We have also included an alphabetical "All Applications" view, a grid view
for your favorite tools, and placed all the power actions ("Sleep", "Restart", "Shut Down",
etc.) at the bottom of the launcher pop-up so they are always visible and available.

Last but not least, we have [fixed most of the bugs reported by users](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/258), guaranteeing a smoother access to all your stuff.

If you prefer the old Kickoff app launcher however, it is only few clicks away in
[KDE's software store](https://store.kde.org).

{{< /feature >}}

{{< feature src="../5.21.0/header1.webp" class="round" >}}

## Application theme improvements

Continuing with improvements to the look and feel, applications using Plasma's default
theme now have a refreshed color scheme and sport a brand new unified headerbar style
with a clean, cool new look.

{{< /feature >}}

{{< /container >}}

{{< twilight class="text-center " >}}

## Breeze Twilight

{{< laptop src="../5.21.0/breeze-twilight.webp" class="mb-4" caption="Screenshot showing Breeze Twilight with a dark panel and light applications" >}}

Meet Breeze Twilight: a combination of a dark theme for Plasma and a light theme
for applications, so you can enjoy the best of both worlds. Find it in the
Global Theme section of your Settings

{{< /twilight >}}

{{< container class="monitor" >}}

![System monitor logo](https://apps.kde.org/app-icons/org.kde.plasma-systemmonitor.svg)

## Plasma System Monitor

Plasma System Monitor is a brand new app for monitoring system resources
and is now an integral part of Plasma.

Plasma System Monitor provides a wide variety of different views, offering an
overview page with information on important core resources, such as memory,
disk space, network and CPU usage. It also provides a quick view of the applications
consuming the most resources. This lets you easily locate and zap applications
or processes that are slowing your computer down.

If you need more details, the Applications page shows you all the running
applications along with detailed statistics and graphs. A process page is
also available for per-process information, and History shows the evolution of the
use of your machine's resources over time.

Finally, you can also create your own new customized pages using the page editor. This
lets you tailor the information you get from your system to your needs.

{{< /container >}}

{{< boxes >}}

{{< box src="../5.21.0/systemmonitor.webp" alt="Overview page" >}}

{{< box src="../5.21.0/systemmonitor2.webp" alt="History page" >}}

{{< box src="../5.21.0/systemmonitor3.webp" alt="Application resource usage overview" >}}

{{< box src="../5.21.0/systemmonitor4.webp" alt="New page editor" >}}

{{< /boxes >}}

{{< diagonal-box color="purple" logo="/images/wayland.png" alt="Wayland logo" >}}

## KWin and Wayland 

KDE is pushing to have first class support for Wayland, and Plasma 5.21 makes
massive progress towards reaching that goal.

We have extensively refactores the compositing code in KWin and the changes should
reduce latency throughout all compositing operations. We have also added a control
in the compositing settings so you can choose whether you prefer lower latency or
smoother animations.

In addition, we have also added support for mixed-refresh-rate display setups
on Wayland, e.g. you can have one screen refreshing at 144Hz and another at
60Hz, which is ideal for improving work-stations with multiple monitors. Preliminary
support for multiple GPUs was also added on Wayland.

The virtual keyboard in Wayland has been improved and now supports GTK
applications using the `text-input-v3` protocol. The support for graphical tablets has
also been improved and now includes all the controls that were missing in the
previous version, such as pad ring and pad buttons.

Apart from the numerous improvements in stability, there are quite a few Plasma
components that are getting much better support in Wayland. KRunner, for example,
is now able to list all open windows in Wayland, a new component in the panel's system
tray informs you of the keyboard layout, and we now support features required
for GTK 4, so all GTK 4 applications will now work.

{{< /diagonal-box >}}

{{< container class="text-center monitor" >}}

![System Settings](/breeze-icons/systemsettings.svg)

## System Settings

Plasma 5.21 brings a new page to System Settings: the Plasma Firewall settings.
This configuration module lets you set up and configure a firewall for your system.

A firewall can help you protect your system from intrusion from outside or block information
you want to keep private from leaving your machine. Plasma 5.21 provides a simplified way of
configuring both UFW and firewalld, two of the most used firewall systems in Linux.

Multiple pre-existing configuration pages have been completely rewritten and are now cleaner and easier
to use. This has been the case for the Accessibility, Desktop Session and SDDM configuration
modules.

{{< /container >}}

{{< boxes >}}

{{< box src="../5.21.0/kcm_access.webp" alt="New Accessibility KCM" >}}

{{< box src="../5.21.0/kcmserver.webp" alt="Desktop Session KCM" >}}

{{< box src="../5.21.0/sddm.webp" alt="New SDDM KCM" >}}

{{< box src="../5.21.0/firewall.webp" alt="New Firewall KCM" >}}

{{< /boxes >}}

{{< diagonal-box color="blue" class="text-center applets" >}}

## Applets

The Media Player widget's layout has been improved and now includes the
list of applications currently playing music in the header as a tab bar.
Another upgrade is that the album cover now takes up the whole
width of the widget.

![Media Player Applets](../5.21.0/media.webp)

{{< /diagonal-box >}}

{{< container class="text-center monitor" >}}

![Plasma Mobile](https://www.plasma-mobile.org/img/logo.svg)

## Plasma Mobile

Plasma has always been designed to adapt to all types of screens.
It can work on a desktop but it's also easily adaptable to work on a mobile, tablet
or anything in between. In Plasma 5.21 we are adding two new components for mobile
in the official release.

* The Plasma Phone Components contains the mobile shell but also specific
Plasma widgets adapted for Plasma Mobile.

* QQC2 Breeze Style is a pure Qt Quick Controls 2 style. It visually fits
in with the widget based desktop Breeze theme and was optimized for lower
RAM and GPU usage.

{{< /container >}}

{{< feature-grid title="Other Updates" >}}

{{< container >}}

If you would like to learn more, [check out the the full changelog for Plasma 5.21](/announcements/changelogs/plasma/5/5.20.5-5.21.0).

## In Memoriam Gustavo Carneiro

{{<figure src="https://cdn.kde.org/promo/Announcements/Plasma/5.21/gustavo.png" alt="Gustavo Carneiro" caption="Gustavo doing what he liked to do best: tinkering with tech." width="600px" >}}

Plasma 5.21 is dedicated to Gustavo Carneiro, a KDE contributor from Brazil that left us in January victim of COVID-19.

KDE was Gustavo's first free software experience, and he embraced it. Although he did not start out as a C++ developer, he worked hard to learn and improve the systems that he was using.

Among many other things, Gustavo became a major contributor to KDE's terminal emulator, Konsole, and went on to develop technologies some of which are so new, they have not even been released yet. 

Now his legacy lives on within Konsole.

Gustavo, thank you for your help.

{{< /container >}}
