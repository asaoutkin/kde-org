---
aliases:
- ../../plasma-5.16.0
changelog: 5.15.5-5.16.0
date: 2019-06-11
layout: plasma
title: 'KDE Plasma 5.16: Now Smoother and More Fun'
---

{{% youtube id="T-29hJUxoFQ" %}}

{{<figure src="/announcements/plasma/5/5.16.0/plasma_5.16.png" alt="Plasma 5.16" class="text-center" width="600px" caption="KDE Plasma 5.16">}}

{{% i18n_date %}}

Today KDE launches the latest version of its desktop environment, Plasma 5.16.

For this release, KDE developers have worked hard to polish Plasma to a high gloss. The results of their efforts provide a more consistent experience and bring new features to all Plasma users.

One of the most obvious changes is the completely rewritten notification system that comes with a <i>Do Not Disturb</i> mode, a more intelligent history which groups notifications together, and critical notifications in fullscreen apps. Besides many other things, it features better notifications for file transfer jobs, and a much more usable <i>System Settings</i> page to configure all notification-related things.

The system and widget settings have been refined and improved by porting code to newer Kirigami and Qt technologies, while at the same time polishing the user interface. The Visual Design Group and the Plasma team continue their efforts towards the usability and productivity goal, getting feedback and removing all the papercuts in our software so that you find Plasma smoother, as well as more intuitive and consistent to use.

For the first time, the default wallpaper of Plasma 5.16 has been decided by a contest where everyone could participate and submit their original art. The winning wallpaper - the work of a talented Argentinian artist - is as sleek and cool as Plasma itself, and will keep your desktop looking fresh during the summer.

We hope you enjoy using Plasma 5.16 as much as we did making it.

## Desktop Management

- Plasma 5.16 comes with a completely rewritten notification system. You can mute notifications with the <i>Do Not Disturb</i> mode, and the list of past notifications now displays them grouped by app. Critical notifications show up even when apps are in fullscreen mode and we have also improved notifications for file transfer jobs. Another thing users will appreciate is that the <i>System Settings</i> page for notifications is much clearer and more usable.

{{<figure src="/announcements/plasma/5/5.16.0/notifications_2.png" alt="New notifications" caption="New Notifications" width="600px" >}}

* Plasma's themes system has also been greatly improved. For one, when you select a new theme, it now gets correctly applied to panels. We have some great news for theme designers: you now have more control over customizing widgets. You can tweak the look of the analog clock, for example, by adjusting the offset of its hands and toggling the blur behind them.

{{<figure src="/announcements/plasma/5/5.16.0/plasma-theme-fixes.png" alt="Theme Engine Fixes for Clock Hands!" caption="Theme Engine Fixes for Clock Hands!" width="600px" >}}

- We have modernized all widget configuration settings and improved the panels to make them clearer and easier to use. The <i>Color Picker</i> widget has also been improved, and lets you drag colors from the plasmoid to text editors, or the palette of a photo editor. The <i>Show Desktop</i> icon is now also present in the panel by default.

* Plasma 5.16 protects your privacy, too. When any app is recording audio, a microphone icon will appear in the <i>System Tray</i> warning you of the fact. You can then raise or lower the volume using the wheel on your mouse, or mute and unmute the mic with a middle click.

* The look and feel of the lock, login and logout screens has been improved with new icons, labels, hover behavior, and by adjusting the login button layout.

{{<figure src="/announcements/plasma/5/5.16.0/lock_screen.png" alt="Login Screen Theme Improved" caption="Login Screen Theme Improved" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.16.0/sddm-theme-1.png" alt="SDDM Theme Improved" caption="SDDM Theme Improved" width="600px" >}}

- The settings window of the <i>Wallpaper Slideshow</i> displays the images in the folders you selected, and lets you select only the ones you want to display in the slideshow.

{{<figure src="/announcements/plasma/5/5.16.0/wallpaper.png" alt="Wallpaper Slideshow settings window" caption="Wallpaper Slideshow settings window" width="600px" >}}

- The <i>Task Manager</i> has better organized context menus, and you can configure it to move a window from a different virtual desktop to the current one with a middle click.

- The default <i>Breeze</i> window and menu shadow colors are back to pure black. This improves the visibility of many things, especially when using a dark color scheme.

- The <i>Show Alternatives</i> button is visible in panel edit mode, and you can use it to quickly swap one widget for another with similar functionalities. For example, you can use it to replace the analog clock with the digital or binary clock.

{{<figure src="/announcements/plasma/5/5.16.0/alternatives_menu-1.png" alt="Panel Edition Offers Alternatives" caption="Panel Editing Offers Alternatives" width="600px" >}}

* In Plasma 5.16, you can lock and unlock Plasma Vaults directly from Dolphin.

## Settings

- We have polished all pages and refined the entire <i>Appearance</i> section. The <i>Look and Feel</i> page has moved to the top level, and we added improved icons to many of the pages.

{{<figure src="/announcements/plasma/5/5.16.0/application-style.png" alt="Application Style and Appearance Settings" caption="Application Style and Appearance Settings" width="600px" >}}

* The <i>Color Scheme</i> and <i>Window Decorations</i> pages have been redesigned with a more consistent grid view, and the <i>Color Scheme</i> page now supports filtering by light and dark themes. It also lets you drag-and-drop to install themes, and undo if you accidentally delete a theme. To apply a theme, you just have to double-click on it.

{{<figure src="/announcements/plasma/5/5.16.0/colors_kcm-2.png" alt="Color Scheme" caption="Color Scheme" width="600px" >}}

* The theme preview of the <i>Login Screen</i> page has been overhauled, and the <i>Desktop Session</i> page now features a <i>Reboot to UEFI Setup</i> option.

* Plasma 5.16 supports configuring touchpads using the Libinput driver on X11.

## Window Management

- Initial support for using Wayland with proprietary Nvidia drivers has been added. When using Qt 5.13 with this driver, graphics are also no longer distorted after waking the computer from sleep.

- Wayland now features drag-and-drop between XWayland and Wayland native windows. The <i>System Settings</i> libinput touchpad page lets you configure the click method, switching between <i>areas</i> or <i>clickfinger</i>.

- KWin's blur effect looks more natural and correct as it doesn't unnecessarily darken the area between blurred colors anymore. GTK windows now apply correct active and inactive color schemes.

{{<figure src="/announcements/plasma/5/5.16.0/blur_konsole.png" alt="Lock screen in Plasma 5.16" caption="Window Management" width="600px" >}}

* We have added two new default shortcuts: <kbd>Meta</kbd> + <kbd>L</kbd> lets you lock the screen and <kbd>Meta</kbd> + <kbd>D</kbd> offers a quick way to show and hide the desktop.

## Plasma Network Manager

- The <i>Networks</i> widget is now faster to refresh Wi-Fi networks and more reliable in doing so. It also has a button to display a search field that helps you find a particular network in the list of available choices. Right-clicking on any network will show a <i>Configure…</i> action.

- WireGuard, the simple and secure VPN, is compatible with NetworkManager 1.16. We have also added One Time Password (OTP) support in the Openconnect VPN plugin.

## Discover

- Discover is Plasma's software manager, and the latest version available in Plasma 5.16 comes with improvements that make it easier to use. In the new <i>Update</i> page, for example, apps and packages now come with distinct <i>downloading</i> and <i>installing</i> sections. When an item has finished installing, it disappears from the view. You can also force-quit while an installation or an update operation is in progress.

{{<figure src="/announcements/plasma/5/5.16.0/discover-update.png" alt="Updates in Discover" caption="Updates in Discover" width="600px" >}}

* The task completion indicator now looks better, featuring a real progress bar. Discover also displays a busy indicator when checking for updates, and has improved support and reliability for AppImages and other apps that come from <a href='https://store.kde.org'>store.kde.org</a>.

* The <i>Sources</i> menu now shows the version number for each different source of an app.