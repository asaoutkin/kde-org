---
aliases:
- ../../plasma-5.5.95
changelog: 5.5.5-5.5.95
date: 2016-03-02
layout: plasma
---

{{<figure src="/announcements/plasma/5/5.6.0/plasma-5.6.png" alt="KDE Plasma 5.6" class="text-center" width="600px" caption="KDE Plasma 5.6">}}

{{% i18n_date %}}

Today KDE releases a beta update to its desktop software, Plasma 5.6.

This release of Plasma brings many improvements to the task manager, KRunner, activities, and Wayland support as well as a much more refined look and feel.

### Slicker Plasma Theme

{{<figure src="/announcements/plasma/5/5.6.0/colour-schemes.png" alt="Breeze Color Scheme Support" class="text-center" width="600px" caption="Breeze Color Scheme Support">}}

The default Plasma theme, Breeze, now follows the application color scheme allowing for a more personalized experience. A new 'Breeze Light' together with 'Breeze Dark' theme can be used to bring back the previous behavior. Additionally, tooltip animations have become more subtle.

### Supercharged Task Manager

{{<figure src="/announcements/plasma/5/5.6.0/download.png" alt="Copy Progress" class="text-center" width="600px" caption="Copy Progress">}}

Multitasking has just become easier. The much improved task manager in Plasma 5.6 now displays progress of tasks, such as downloading or copying files.

{{<figure src="/announcements/plasma/5/5.6.0/media-player.png" alt="Media Controls" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.6.0/media-player-tooltips.png" alt="Media Controls" class="text-center" width="600px" caption="Media Controls in Panel and Tooltips">}}

Moreover, hovering a music or video player shows beautiful album art and media controls, so you never have to leave the application you're currently working with. Our media controller applet that shows up during playback also received some updates, notably support for multiple players running simultaneously.

Not only did we improve interacting with running applications, starting applications gets in your way less, too. Using Jump Lists you can launch an application and jump, hence the name, to a specific task right away. This feature is also present in the application launchers.

### Smoother Widgets

{{<figure src="/announcements/plasma/5/5.6.0/krunner.png" alt="KRunner" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.6.0/media-player-tooltips.png" alt="Folderview in Panel" class="text-center" width="600px" caption="KRunner's Smoother look and Folderview in Panel">}}

There are many refinements to the overall visuals of Plasma in this release. KRunner gained support for drag and drop and lost separator lines to look smoother while icons on the desktop traded the solid label background for a chic drop shadow. Users that place a folder applet in their panel can enjoy improved drag and drop, support for the back button on a mouse as well as choosing between list and icon view. On the more technical side, many small fixes to hi-dpi scaling have found their way into this release.

### Weather

{{<figure src="/announcements/plasma/5/5.6.0/weather.png" alt="Weather Widget" class="text-center" width="600px" caption="Weather Widget">}}

Another feature returns from the old days, the weather widget.

### On the road to Wayland

{{<figure src="/announcements/plasma/5/5.6.0/wayland.png" alt="Plasma using Wayland" class="text-center" width="600px" caption="Plasma using Wayland">}}

With Plasma 5.5 for the first time we shipped a Wayland session for you to try out. While we still do not recommend using Wayland as a daily driver, we've made some significant advances:

- Window decorations are now supported for Wayland clients giving you a beautiful and unified user experience
  - Input handling gained all features you've come to know and love from the X11 world, including 'Focus follows mouse', Alt + mouse button to move and resize windows, etc
  - Different keyboard layouts and layout switching

### Tech Preview System Integration Themes

We are trialing a tech preview of Breeze themes for Plymouth and Grub, so Plasma can give you a complete system experience from the moment you turn your computer on.
