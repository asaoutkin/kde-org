---
aliases:
- ../../plasma-5.7.0
changelog: 5.6.5-5.7.0
date: 2016-07-05
layout: plasma
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma/5/5.7.0/plasma-5.7.png" alt="KDE Plasma 5.7 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.7">}}

{{% i18n_date %}}

Today KDE releases an update to its desktop software, Plasma 5.7.

### Improved Workflows

{{<figure src="/announcements/plasma/5/5.7.0/krunner-jump-actions.png" alt="Jump List Actions in KRunner " class="text-center" width="600px" caption="Jump List Actions in KRunner">}}

In our previous release we added Jump List Actions for quicker access to certain tasks within an application. This feature has been extended and those actions are also found through KRunner now.

{{<figure src="/announcements/plasma/5/5.7.0/pim-events.png" alt="Agenda Items in Calendar " class="text-center" width="600px" caption="Agenda Items in Calendar">}}

Plasma 5.7 marks the return of the agenda view in the calendar, which provides a quick and easily accessible overview of upcoming appointments and holidays.

{{<figure src="/announcements/plasma/5/5.7.0/plasma-pa-drag.png" alt="Dragging Application to Audio Device " class="text-center" width="600px" caption="Dragging Application to Audio Device">}}

Many improvements have been added to the Volume Control applet: it gained the ability to control volume on a per-application basis and allows you to move application output between devices using drag and drop. Also implemented is the ability to raise the volume above 100%.

{{<figure src="/announcements/plasma/5/5.7.0/icons-invert.png" alt="Icons Tint to Match Highlight " class="text-center" width="600px" caption="Icons Tint to Match Highlight">}}

For improved accessibility, Breeze icons within applications are now tinted depending on the color scheme, similarly to how it's done within Plasma. This resolves situations where our default dark icons might show up on dark surfaces.

### Better Kiosk Support

The <a href="https://userbase.kde.org/KDE_System_Administration/Kiosk/Introduction">Kiosk Framework</a> provides means of restricting the customazibility of the workspace, in order to keep users in an enterprise or public environment from performing unwanted actions or modifications. Plasma 5.7 brings many corrections about enforcing such restrictions. Notably, the Application Launcher will become read-only if widgets are locked through Kiosk policies, i.e. favorites are locked in place and applications can no longer be edited. Also, the Run Command restriction will prevent KRunner from even starting in the first place.

### New System Tray and Task Manager

The System Tray has been rewritten from scratch to allow for a simpler and more maintainable codebase. While its user interface has only seen some minor fixes and polishing, many issues caused by the complex nature of the applet housing applets and application icons within have been resolved.

Similarly, the task bar has gained a completely revamped backend, replacing the old one that has already been around in the early days of our workspace. While the old backend got many features added over the period of time it was used, the new one has a remarkably better performance and could be engineered more cleanly and straight-forward as the requirements were known beforehand. All of this will ensure a greatly increased reliability and it also adds <a href="https://blog.martin-graesslin.com/blog/2016/06/a-task-manager-for-the-plasma-wayland-session/">support for Wayland</a> which was one of the most visible omissions in our Wayland tech previews.

### Huge Steps Towards Wayland

{{<figure src="https://c2.staticflickr.com/8/7418/26639077964_445c1761e3_b.jpg" alt="Betty the Fuzzpig Tests Plasma Wayland " class="text-center" width="600px" caption="Betty the Fuzzpig Tests Plasma Wayland">}}

This release brings Plasma closer to the new windowing system Wayland. Wayland is the successor of the decades-old X11 windowing system and brings many improvements, especially when it comes to tear-free and flicker-free rendering as well as security. The development of Plasma 5.7 for Wayland focused on quality in the Wayland compositor KWin. Over 5,000 lines of auto tests were added to KWin and another 5,000 lines were added to KWayland which is now released as part of KDE Frameworks 5.

The already implemented workflows got stabilized and are ensured to work correctly, with basic workflows now fully functional. More complex workflows are not yet fully implemented and might not provide the same experience as on X11. To aid debugging a new debug console got added, which can be launched through KRunner using the keyword “KWin” and integrates functionality known from the xprop, xwininfo, xev and xinput tools.

Other improvements include:

- When no hardware keyboard is connected, a <a href="https://blog.martin-graesslin.com/blog/2016/05/virtual-keyboard-support-in-kwinwayland-5-7/">virtual keyboard</a> is shown instead, bringing a smooth converged experience to tablets and convertibles
- The sub-surface protocol is now supported which means that System Settings works correctly and no longer errorneously opens multiple windows.
- Mouse settings, such as pointer acceleration, are honored and the touchpad can be enabled/disabled through a global shortcut. Touchpad configuration is still missing.

{{%youtube id="_29BYcm8xKk"%}}

Virtual Keyboard Support in Plasma Wayland