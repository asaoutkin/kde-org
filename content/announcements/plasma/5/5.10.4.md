---
aliases:
- ../../plasma-5.10.4
changelog: 5.10.3-5.10.4
date: 2017-07-18
layout: plasma
---

{{% youtube id="VtdTC2Mh070" %}}

{{<figure src="/announcements/plasma/5/5.10.0/plasma-5.10.png" alt="KDE Plasma 5.10 " class="text-center" width="600px" caption="KDE Plasma 5.10">}}

{{% i18n_date %}}

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.10.4" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in May with many feature refinements and new modules to complete the desktop experience." "5.10" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- [Windowed Widgets Runner] Fix launching widget. <a href="https://commits.kde.org/plasma-workspace/8c5a75341849a621462c41bb685bb46dfef129e1">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D6602">D6602</a>
- [Notifications] Check for corona to avoid crash. <a href="https://commits.kde.org/plasma-workspace/8a05294e5b3ef1df86f099edde837b8c8d28ccaf">Commit.</a> Fixes bug <a href="https://bugs.kde.org/378508">#378508</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6653">D6653</a>
- System Setting: Honour the NoDisplay attribute of KServices. <a href="https://commits.kde.org/systemsettings/85ed16cd422804971345bc492757fa0050b4b61d">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D6612">D6612</a>