---
aliases:
- ../../plasma-5.10.0
changelog: 5.9.5-5.10.0
date: 2017-05-30
layout: plasma
title: KDE Plasma 5.10, Simple by Default, Powerful when Needed
---

{{%youtube id="VtdTC2Mh070"%}}

{{<figure src="/announcements/plasma/5/5.10.0/plasma-5.10.png" alt="KDE Plasma 5.10 " class="text-center" width="600px" caption="KDE Plasma 5.10">}}

{{% i18n_date %}}

Today KDE has made a new feature release of our desktop Plasma 5.10 with new features across the suite to give users an experience which lives up to our tagline: simple by default, powerful when needed.

## Panel Task Manager

{{< video src-webm="/announcements/plasma/5/5.10.0/middle-click-task-bar.webm" caption="Middle Mouse Click to Group" >}}

Task Manager, the list of applications in the panel, has gained options for middle mouse click such as grouping and ungrouping applications.

Several other improvements here include:

- Places jump list actions in File manager launchers (e.g. pinned Dolphin in Task Manager now lists user places)
- The icon size in vertical Task Managers is now configurable to support more common vertical panel usage patterns
- Improved app identification and pinning in Task Manager for apps that rely on StartupWMClass, perl-SDL-based apps and more

## Folder View Is the New Default Desktop

{{<figure src="/announcements/plasma/5/5.10.0/spring_loading.gif" alt="Folder on the Desktop by Default " class="text-center" width="600px" caption="Folder on the Desktop by Default">}}

After some years shunning icons on the desktop we have accepted the inevitable and changed to Folder View as the default desktop which brings some icons by default and allows users to put whatever files or folders they want easy access to. Many other improvements have been made to the Folder View include:

- <a href="https://blogs.kde.org/2017/01/31/plasma-510-spring-loading-folder-view-performance-work">Spring Loading</a> in Folder View making drag and drop of files powerful and quick
- More space-saving/tighter icon grid in Folder View based on much user feedback
- Improved mouse behavior / ergonomics in Folder View for icon dnd (less surprising drop/insert location), rectangle selection (easier, less fiddly) and hover (same)
- Revamped rename user interface in Folder View (better keyboard and mouse behavior e.g. closing the editor by clicking outside, RTL fixed, etc.)
- <em>Massively</em> improved performance in Folder View for initial listing and scrolling large folders, reduced memory usage
- Many other bug fixes and UI improvements in Folder View, e.g. better back button history, Undo shortcut support, clickable location in the headings, etc.
- Unified drop menu in Folder View, showing both file (Copy/Move/Link) and widget (creating a Picture widget from an image drop, etc.) drop actions
- It is now possible to resize widgets in the desktop by dragging on their edges and moving them with Alt+left-click, just like regular windows

## New Features Everywhere

{{<figure src="/announcements/plasma/5/5.10.0/lock-music.png" alt="Lock Screen Now Has Music Controls " class="text-center" width="600px" caption="Lock Screen Now Has Music Controls">}}

{{<figure src="/announcements/plasma/5/5.10.0/software-centre-krunner.png" alt="Software Centre Plasma Search" class="text-center" width="600px" caption="Software Centre Plasma Search offers to install apps">}}

{{<figure src="/announcements/plasma/5/5.10.0/plasma-pa.png" alt="Audio Volume Device Menu " class="text-center" width="600px" caption="Audio Volume Device Menu">}}

There are so many other improvements throughout the desktop, here's a sample:

- Media controls on lock screen
- Pause music on suspend
- Software Centre Plasma Search (KRunner) suggests to install non-installed apps
- File copying notifications have a context menu on previews giving access to actions such as open containing folder, copy, open with etc
- Improved plasma-windowed (enforces applet default/minimum sizes etc)
- 'desktop edit mode', when opening toolbox reveals applet handles
- Performance optimizations in Pager and Task Manager
- 'Often used' docs and apps in app launchers in addition to 'Recently used'
- Panel icons (buttons for popup applets, launcher applets) now follow the Icons -> Advanced -> Panel size setting in System Settings again, so they won't take up too much space, particularly useful for wide vertical panels
- Revamped password dialogs for network authentication
- The security of the lock screen architecture got reworked and simplified to ensure that your system is secured when the screen is locked. On Linux systems the lock screen is put into a sandbox through the seccomp technology.
- Plasma's window manager support for hung processes got improved. When a window is not responding any more it gets darkened to indicate that one cannot interact with it any more.
- Support for locking and unlocking the shell from the startup script, useful especially for distributions and enterprise setups
- Audio Volume applet has a handy menu on each device which you can use to set is as default or switch output to headphones.

## Improved touch screen support

{{<figure src="/announcements/plasma/5/5.10.0/lock-screen-virtual-keyboard.png" alt="Virtual keyboard on Log In and Lock Screen " class="text-center" width="600px" caption="Virtual keyboard on Log In and Lock Screen">}}

Touch Screen Support has improved in several ways:

- Virtual Keyboard in lock screen
- Virtual Keyboard in the login screen
- Touch screen edge swipe gestures
- Left screen edge defaults to window switching
- Show auto-hiding panels through edge swipe gesture

## Working for the Future with Wayland

We have put a lot of work into porting to new graphics layer Wayland, the switch is coming but we won't recommend it until it is completely transparent to the user. There will be improved features too such as KWin now supports scaling displays by different levels if you have a HiDPI monitor and a normal DPI screen.
Keyboard layout support in Wayland now has all the features of X11:

- Layout switcher in the system tray
- Per layout global shortcut
- Switch layout based on a policy, either global, virtual desktop, application or per window
- IPC interface added, so that other applications can change layout.

## Plymouth Boot Splash Selection

{{<figure src="/announcements/plasma/5/5.10.0/plymouth-kcm.png" alt="Plymouth KControl Module " class="text-center" width="600px" caption="Plymouth KControl Module">}}

A new System Settings module lets you download and select boot time splashes.

## Bundle Packages

{{<figure src="/announcements/plasma/5/5.10.0/xdg-portal.png" alt="Flatpak integration with xdg-desktop-portal-kde: selecting a file using file chooser portal, invoking openURI portal and notification portal " class="text-center" width="600px" caption="Flatpak integration with xdg-desktop-portal-kde: selecting a file using file chooser portal, invoking openURI portal and notification portal">}}

Experimental support for forthcoming new bundle package formats has been implemented. Discover software centre has gained provisional backends for Flatpak and Snappy. New plugin xdg-desktop-portal-kde has added KDE integration into Flatpak packaged applications.

Support for GNOME’s <a href='https://odrs.gnome.org/'>Open Desktop Ratings</a>, replacing old Ubuntu popularity contest with tons of already existing reviews and comments.
