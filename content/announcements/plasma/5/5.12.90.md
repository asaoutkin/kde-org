---
aliases:
- ../../plasma-5.12.90
changelog: 5.12.5-5.12.90
date: 2018-05-17
layout: plasma
title: 'KDE Plasma 5.13 Beta: Fast, Lightweight and Full Featured.'
---

{{<figure src="/announcements/plasma/5/5.13.0/plasma-5.13.png" alt="Plasma 5.13 Beta" class="text-center" width="600px" caption="KDE Plasma 5.13 Beta">}}

{{% i18n_date %}}

{{% i18n_var "Today KDE unveils a %[1]s release of Plasma %[2]s." "beta" "5.13" %}}

Members of the Plasma team have been working hard to continue making Plasma a lightweight and responsive desktop which loads and runs quickly, but remains full-featured with a polished look and feel. We have spent the last four months optimising startup and minimising memory usage, yielding faster time-to-desktop, better runtime performance and less memory consumption. Basic features like panel popups were optimised to make sure they run smoothly even on the lowest-end hardware. Our design teams have not rested either, producing beautiful new integrated lock and login screen graphics.

## New in Plasma 5.13

### Plasma Browser Integration

Plasma Browser Integration is a suite of new features which make Firefox and Chrome, and Chromium-based browsers work with your desktop. Downloads are now displayed in the Plasma notification popup just as when transferring files with Dolphin. The Media Controls Plasmoid can mute and skip videos and music playing from within the browser. You can send a link to your phone with KDE Connect. Browser tabs can be opened directly using KRunner via the Alt-Space keyboard shortcut. To enable Plasma Browser Integration, add the relevant plugin from the addon store of your favourite browser.

{{<figure src="/announcements/plasma/5/5.13.0/pbi-download-integration.png" alt="Plasma Browser Integration for Downloads" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.13.0/pbi-video-controls.png" alt="Plasma Browser Integration for Media Controls" class="text-center" width="600px" caption="Plasma Browser Integration for Downloads and Media Controls">}}

### System Settings Redesigns

Our settings pages are being redesigned. The KDE Visual Design Group has reviewed many of the tools in System Settings and we are now implementing those redesigns. KDE's Kirigami framework gives the pages a slick new look. We started off with the theming tools, comprising the icons, desktop themes, and cursor themes pages. The splash screen page can now download new splashscreens from the KDE Store. The fonts page can now display previews for the sub-pixel anti-aliasing settings.

{{<figure src="/announcements/plasma/5/5.13.0/kcm-desktop-theme.png" alt="Desktop Theme" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.13.0/kcm-fonts-hint-preview.png" alt="Font Settings" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.13.0/kcm-icons.png" alt="Icon Themes" caption="Redesigned System Settings Pages"  class="text-center" width="600px">}}

### New Look for Lock and Login Screens

Our login and lock screens have a fresh new design, displaying the wallpaper of the current Plasma release by default. The lock screen now incorporates a slick fade-to-blur transition to show the controls, allowing it to be easily used like a screensaver.

{{<figure src="/announcements/plasma/5/5.13.0/lockscreen.png" alt="Lock Screen" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.13.0/login.png" alt="Login Screen" caption="Lock and Login Screen new Look"  class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.13.0/kwin-blur-dash.png" alt="Improved Blur Effect in the Dash Menu" caption="Improved Blur Effect in the Dash Menu"  class="text-center" width="600px">}}

### Graphics Compositor

Our compositor KWin gained much-improved effects for blur and desktop switching. Wayland work continued, with the return of window rules, the use of high priority EGL Contexts, and initial support for screencasts and desktop sharing.

{{<figure src="/announcements/plasma/5/5.13.0/discover.png" alt="Discover's Lists with Ratings, Themed Icons, and Sorting Options" caption="Discover's Lists with Ratings, Themed Icons, and Sorting Options"  class="text-center" width="600px">}}

### Discover

Discover, our software and addon installer, has more features and sports improvements to the look and feel.

Using our Kirigami UI framework we improved the appearance of lists and category pages, which now use toolbars instead of big banner images. Lists can now be sorted, and use the new Kirigami Cards widget. Star ratings are shown on lists and app pages. App icons use your local icon theme better match your desktop settings. All AppStream metadata is now shown on the application page, including all URL types. And for users of Arch Linux, the Pacman log is now displayed after software updates.

Work has continued on bundled app formats. Snap support now allows user control of app permissions, and it's possible to install Snaps that use classic mode. And the 'snap://' URL format is now supported. Flatpak support gains the ability to choose the preferred repository to install from when more than one is set up.

### Much More

Other changes include:

- A tech preview of <a href='http://blog.broulik.de/2018/03/gtk-global-menu/'>GTK global menu integration</a>.
- Redesigned Media Player Widget.
- Plasma Calendar plugin for astronomical events, currently showing: lunar phases & astronomical seasons (equinox, solstices).
- xdg-desktop-portal-kde, used to give desktop integration for Flatpak and Snap applications, gained support for screenshot and screencast portals.
- The Digital Clock widget allows copying the current date and time to the clipboard.
- The notification popup has a button to clear the history.
- More KRunner plugins to provide easy access to Konsole profiles and the character picker.
- The Mouse System Settings page has been rewritten for libinput support on X and Wayland.
- Plasma Vault has a new CryFS backend, commands to remotely close open vaults with KDE Connect, offline vaults, a more polished interface and better error reporting.
- A new dialog pops up when you first plug in an external monitor so you can easily configure how it should be positioned.
- Plasma gained the ability to fall back to a software rendering if OpenGL drivers unexpectedly fail.

{{<figure src="/announcements/plasma/5/5.13.0/gedit-global-menu.png" alt="GEdit with Title Bar Menu" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.13.0/media-plasmoid.png" alt="Redesigned Media Player Widget" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.13.0/connect-external-monitor.png" alt="Connect an External Monitor" caption="GEdit with Title Bar Menu. Redesigned Media Player Widget.  Connect an External Monitor Dialog."  class="text-center" width="600px">}}