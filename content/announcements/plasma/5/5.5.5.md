---
aliases:
- ../../plasma-5.5.5
changelog: 5.5.4-5.5.5
date: 2016-03-01
layout: plasma
---

{{<figure src="/announcements/plasma/5/5.5.0/plasma-5.5.png" alt="Plasma 5.5 " class="text-center" width="600px" caption="Plasma 5.5">}}

{{% i18n_date %}}

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.5.5" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in December with many feature refinements and new modules to complete the desktop experience." "5.5" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix Turning all screens off while the lock screen is shown can result in the screen being unlocked when turning a screen on again. <a href='https://www.kde.org/info/security/advisory-20160209-1.txt'>CVE-2016-2312</a>.
- [User Switcher] Fix session switching when automatic screen locking is enabled. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7a0096ba99d7a71ae9f45d7c0011d0ebb1eae23d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356945'>#356945</a>
- Fix entries staying highlighted after context menu closes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9e0a7e991dbfc862a72f21f4662e280aff8ab317'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356018'>#356018</a>