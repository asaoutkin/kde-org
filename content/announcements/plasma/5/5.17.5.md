---
aliases:
- ../../plasma-5.17.5
changelog: 5.17.4-5.17.5
date: 2020-01-07
layout: plasma
---

{{< peertube "https://peertube.mastodon.host/videos/embed/5a315252-2790-42b4-8177-94680a1c78fc" >}}

{{<figure src="/announcements/plasma/5/5.17.0/plasma-5.17.png" alt="Plasma 5.17" class="text-center" width="600px" caption="KDE Plasma 5.17">}}

{{% i18n_date %}}

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.17.5" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in October with many feature refinements and new modules to complete the desktop experience." "5.17" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix for KDecoration crash in systemsettings. <a href="https://commits.kde.org/kwin/1a13015d2d1de3ffb9450143480e729057992c45">Commit.</a> Fixes bug <a href="https://bugs.kde.org/411166">#411166</a>. Phabricator Code review <a href="https://phabricator.kde.org/D25913">D25913</a>
- Fix regression in "Port the pager applet away from QtWidgets". <a href="https://commits.kde.org/plasma-desktop/2b5e86323f180f0c51ef9af898a69a522bc379ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/414849">#414849</a>
- Revert "[sddm-theme] Fix initial focus after SDDM QQC2 Port". <a href="https://commits.kde.org/plasma-workspace/c2bc5243d460c306f995130880494eec6f54b18a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/414875">#414875</a>