---
aliases:
- ../../plasma-5.10.5
changelog: 5.10.4-5.10.5
date: 2017-08-22
layout: plasma
---

{{%youtube id="VtdTC2Mh070"%}}

{{<figure src="/announcements/plasma/5/5.10.0/plasma-5.10.png" alt="KDE Plasma 5.10 " class="text-center" width="600px" caption="KDE Plasma 5.10">}}

{{% i18n_date %}}

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.10.5" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in May with many feature refinements and new modules to complete the desktop experience." "5.10" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- A Plasma crash when certain taskbar applications group in a specific way
- Excluding OSD's from the desktop grid kwin effect
- Discover handling URL links to packages