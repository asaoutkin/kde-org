---
aliases:
- ../announce-4.12.2
date: '2014-02-04'
description: KDE Ships Applications and Platform 4.12.2.
title: KDE Ships February Updates to Applications, Platform and Plasma Workspaces
---

February 4, 2014. Today KDE released updates for its Applications and Development Platform, the second in a series of monthly stabilization updates to the 4.12 series. This release also includes an updated Plasma Workspaces 4.11.6. This release contains only bugfixes and translation updates; it will be a safe and pleasant update for everyone.
<br /><br />
More than 20 recorded bugfixes include improvements to the personal information management suite Kontact, the UML tool Umbrello, the document viewer Okular, the web browser Konqueror, the file manager Dolphin, and others.
<br /><br />

{{% i18n_var "A more complete <a href='%[1]s'>list of changes</a> can be found in KDE's issue tracker. Browse the Git logs for a detailed list of changes in %[2]s." "https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=%[2]s&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=" "4.12.2" %}}

{{% i18n_var "To download source code or packages to install go to the <a href='http://www.kde.org/info/%[1]s.php'>%[1]s Info Page</a>. If you want to find out more about the 4.12 versions of KDE Applications and Development Platform, please refer to the <a href='http://www.kde.org/announcements/4.12/'>4.12 release notes</a>." "4.12.0" %}}

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

{{% i18n_var "KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/%[1]s/'>http://download.kde.org</a> or from any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today." "4.12.0" %}}

#### {{% i18n_var "Installing %[1]s Binary Packages" "4.12.0" %}}

<em>Packages</em>.

{{% i18n_var "Some Linux/UNIX OS vendors have kindly provided binary packages of %[1]s for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks." "4.12.0" %}}

<em>Package Locations</em>.

{{% i18n_var "For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='/info/%[1]s.php#binary'>%[1]s Info Page</a>." "4.12.0" %}}

#### {{% i18n_var "Compiling %[1]s" "4.12.0" %}}

{{% i18n_var "The complete source code for %[1]s may be <a href='http://download.kde.org/stable/%[1]s/src/'>freely downloaded</a>. Instructions on compiling and installing %[1]s are available from the <a href='/info/%[1]s.php'>%[1]s Info Page</a>." "4.12.0" %}}

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative.

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
