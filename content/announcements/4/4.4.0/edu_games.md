---
title: Applications Leap Forward
hidden: true
---

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../pim_network">
                <img src="/announcements/4/4.4.0/images/internet-32.png" />
                Previous Page: Network and Personal Information Management
                </a>
        </td>
        <td align="right" width="50%">
                <a href="../platform">Next page: Development Platform
                <img src="/announcements/4/4.4.0/images/platform-32.png" />\
		</a>
        </td>
    </tr>
</table>

<h2>Learn and Discover with the KDE Educational applications</h2>

<p>
The KDE team working on educational applications ("KDE Edu") focuses primarily on schoolchildren between age of 3 and 18. For this audience, a large variety of applications has been developed for mathematics, language, chemistry and physics, touch typing, and even basic programming skills. The Edu team has also created a few applications for a more mature audience. For example, KStars is a perfect fit for the semi-professional astronomer, and has been spotted in professional observatories. Marble, a desktop globe application, can not only display a variety of maps of earth, including satellite images and <a href="http://openstreetmap.org">Open Streetmap</a>, but also features images from Flickr and information from Wikipedia articles.
</p>
<h3>Gaze at the Kstars</h3>
<p>
Prakash Mohan created an Observation Planner for KStars, which can make the life of any astronomer easy by assisting in planning, scheduling and executing observation sessions. The planner can automatically add the night's visible objects to the observation list. It can download images from the Web and has a nice way of storing your observation logs.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_parley.png">
	<img src="/announcements/4/4.4.0/thumbs/44_parley.png" class="img-fluid">
	</a> <br/>
	<em>Parley getting new language lessons</em>
</div>
<br/>

<h3>Learn with Parley</h3>
<p>
Parley is a program to help you memorize things using the spaced repetition (or flash card) technique. Particularly suited to learning new languages, Parley offers several different test types to learn new words, including anagrams, multiple choice, written tests, conjugations, synonyms/antonyms, article training, and "fill in the blank" tests. These tests can be quickly and easily set up for over 200 languages. Vocabulary files are shared with other KDE-Edu applications and can be easily edited by hand. Parley's preexisting vocabulary files can be easily obtained through its integrated KNewStuff framework.
</p>
<h3>Explore our Marble</h3>
<p>
Bastian Holst worked on a weather plugin (among others) for Marble. This included work on Marble itself, such as the plugin dialog and the backend of all online services like Wikipedia and photo plugin. He made it easier to display items in Marble; his work will let Marble introduce many new features in the future.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_marble.png">
	<img src="/announcements/4/4.4.0/thumbs/44_marble.png" class="img-fluid">
	</a> <br/>
	<em>Icy Marble</em>
</div>
<br/>

<p>
Andrew Manson worked with the Marble crew to implement a nice Open Street Map annotation framework that makes life easier for mappers all around the world.
</p>
<h3>Step by Step physics</h3>
<p>
Christopher Ing helped improve Step, KDE's physics simulator. He implemented a smoothed particle hydrodynamics algorithm in KDE-Edu's Step for fast fluid simulation. This will allow science educators to quantitatively measure and qualitatively observe interesting properties of fluids. Physics students will love this - the improvements to Step make it easy to visualise and experiment with complex fluid physics. Read his <a href="http://www.jacksofscience.com/physics/three-aspects-of-a-great-fluid-simulation/">blog</a> for more information.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_step.png">
	<img src="/announcements/4/4.4.0/thumbs/44_step.png" class="img-fluid">
	</a> <br/>
	<em>Step physics simulation</em>
</div>
<br/>

<h3>Calculate Cantor</h3>
<p>
Cantor is a new educational application that makes its debut in KDE SC 4.4. Cantor, developed by Alexander Rieder, acts as a front-end to a series of powerful mathematics and statistics applications: the (<a href="http://www.r-project.org">R programming language</a>, the <a href="http://sagemath.org">SAGE mathematics software</a>, and the <a href="http://maxima.sourceforge.net">Maxima computer algebra system</a>). It wraps them with a graphical user interface without hampering their functionality. Thanks to its graphical user interface, Cantor provides the capabilities of these applications to teachers and students while making the learning curve less steep.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_cantor.png">
	<img src="/announcements/4/4.4.0/thumbs/44_cantor.png" class="img-fluid">
	</a> <br/>
	<em>Cantor is a very capable application</em>
</div>
<br/>

<p>
Cantor can work with multiple applications at the same time. It offers syntax highlighting of commands being entered and convenient access to the documentation of the currently used application. Additionally, Cantor offers a GUI for for building matrices, for solving equations, calculus derivatives and integrals. It also has support for plots and figures, which are displayed inline, and can be copied or saved.
</p>
<p>
Example sheets can be downloaded through KNewStuff. While sheets produced by Cantor can easily be shared with the community using the upload feature from KNewStuff.
</p>
<h3>Graphical Rocs</h3>
<p>
Another newcomer from the edu team is <a href="http://edu.kde.org/rocs">Rocs</a>, developed by Tomaz Canabrava. Rocs is a Graph Theory Integrated Development Environment (IDE) to help teachers demonstrate the results of graph algorithms and to help students play with them. Rocs has a scripting module that interacts with drawn graphs; every graph script change is reflected on the drawn version.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_rocs.png">
	<img src="/announcements/4/4.4.0/thumbs/44_rocs.png" class="img-fluid">
	</a> <br/>
	<em>Rocs in action</em>
</div>
<br/>

<h3>Kalzium molecules</h3>
<p>
Kashyap Puranik added a series of calculators to Kalzium. These include a calculator for molecular mass, gasses (temperature, volume, pressure etc), solution concentration (nice for helping with your chemistry homework), and nuclear degradation (if you want to build a nuclear reactor at home). These calculators are also available as separate Plasma widgets.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kalzium.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kalzium.png" class="img-fluid">
	</a> <br/>
	<em>Kalzium introduces advanced calculators</em>
</div>
<br/>

<h3>KAlgebra</h3>
<p>
KAlgebra has got lots of new features and improvements from Aleix Pol. These includes KAlgebra backend for Cantor, support for lists, improved MathML presentation support, improved jump detections and more usable lambda expressions. A new 2D parametric functions plotting capability has also been added.
</p>
<h3>KTurtle</h3>
<p>
KTurtle is an educational programming environment for the KDE Desktop. Cies Breijs has been working with KTurtle to add features like F2 context help, SVG export for the canvas, HTML export of the code and printing support for the canvas.
</p>
<h2>Play with the KDE games</h2>
<p>
The KDE Software Compilation has a number of applications meant for gaming and entertainment. These include some of the old classics like Pacman and Minesweeper (Kapman and KMines, respectively). A few popular board games and mind twisters have also been brought along: one of them KsirK, which is based on the strategy game Risk. KSudoku is popular with those who enjoy solving Sudoku puzzles.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/games.png">
	<img src="/announcements/4/4.4.0/thumbs/games.png" class="img-fluid">
	</a> <br/>
	<em>A variety of KDE Games</em>
</div>
<br/>

<h3>Granatier</h3>
<p>
Mathias Kraus has written a game called Granatier, which is inspired by the work of ClanBomber, a Bomberman clone.The object of the game is to run through an arena, using bombs to clear out blocks and eliminate your opponents. Bonuses and handicaps are hidden under blocks, which can help or hinder your progress. The game also allows users to import their own ClanBomber arenas for use in the game.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_granatier.png">
	<img src="/announcements/4/4.4.0/thumbs/44_granatier.png" class="img-fluid">
	</a> <br/>
	<em>Playing Granatier</em>
</div>
<br/>

<h3>Puzzling Palapeli</h3>
<p>
Palapeli is a new jigsaw-puzzle game by Stefan Majewsky, which allows you to make and solve your own jigsaws.  You can make simple puzzles with very few pieces or more complex puzzles with a hundred or more pieces.  Either way, it's rewarding to see your own creation come together!
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_palapeli.png">
	<img src="/announcements/4/4.4.0/thumbs/44_palapeli.png" class="img-fluid">
	</a> <br/>
	<em>Solving a puzzle in Palapeli</em>
</div>
<br/>

<h3>Kigo</h3>
<p>
Another new masterpiece game, done by Sascha Peilicke, is Kigo, an implementation of the ancient Chinese game of Go.  Go is easy to learn and difficult to master.  In Kigo, your computer opponent is the GnuGo engine, who is not highly skilled but can help you learn the game and develop your skill to the point where you can venture online and challenge human players at the various Go websites.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kigo.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kigo.png" class="img-fluid">
	</a> <br/>
	<em>Going against the computer</em>
</div>
<br/>

<h3>Bovo</h3>
<p>
Bovo is a game where two players take turns placing markers on the board, the winner being the first to complete a line of five markers. Pelladi Gabor is working on Bovo to highlight the last move to be easier to spot and enable undo after the game has ended, to be able to correct a fatal mis-click.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_bovo.png">
	<img src="/announcements/4/4.4.0/thumbs/44_bovo.png" class="img-fluid">
	</a> <br/>
	<em>You lose!</em>
</div>
<br/>

<h3>KBreakOut</h3>
<p>
KBreakout is a single player falling blocks puzzle arcade game. For KDE SC 4.4, Fela Winkelmolen has added new features in KBreakOut like better keyboard and fullscreen support.
</p>
<h3>KDiamond</h3>
<p>
KDiamond is a three-in-a-row game like Bejeweled. It features unlimited fun with randomly generated games and five difficulty levels with varying number of diamond colors and board sizes.  Stefan Majewsky has added improved time display for KDiamond.
</p>
<h3>KGoldrunner</h3>
<p>
KGoldrunner is a fast-paced platform game where the player must navigate a maze while collecting gold nuggets and avoiding enemies.  A variety of level packs are included, as well as an editor to create new levels. Ian Wadham has added mazes game (37 levels), contributed by Steve Mann to KGoldRunner.
</p>
<h3>Killbots</h3>
<p>
Killbots is a simple game of evading killer robots. The robots are numerous and their sole objective is to destroy you.  Fortunately for you, their creator has focused on quantity rather than quality and as a result the robots are severely lacking in intelligence. Your superior wit and a fancy teleportation device are your only weapons against the never-ending stream of mindless automatons. Parker Coates added a new game type parameter to make fast enemies a bit more intelligent.
</p>

<p>More information about these and other games can be found on the <a
href="http://games.kde.org">KDE Games website</a></p>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../pim_network">
                <img src="/announcements/4/4.4.0/images/internet-32.png" />
                Previous Page: Network and Personal Information Management
                </a>
        </td>
        <td align="right" width="50%">
                <a href="../platform">Next page: Development Platform
                <img src="/announcements/4/4.4.0/images/platform-32.png" />\
		</a>
        </td>
    </tr>
</table>
