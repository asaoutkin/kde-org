---
title: KDE Software Compilation 4.4 Feature Guide
hidden: true
---

<p>
Over the last 14 years the KDE community has grown from a humble few developers
to a community numbering in the thousands. In 2006 the community started a huge
effort to redesign the foundations on which it was building its software. On
January 11th 2008 the 4th major release of the KDE software compilation saw the
light of day. This ambitious release had its share of issues but did also show
the great potential our products have, resulting in a large number of new
contributors.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop_thumb.png" class="img-fluid" alt="The KDE Plasma Desktop Workspace">
	</a> <br/>
	<em>The KDE Plasma Desktop Workspace</em>
</div>
<br/>

<p>
KDE Software Compilation 4.3 improved on the experience provided by its predecessors
by finishing many features, while enhancing performance and stability. In the 4.4
release, the KDE community emphasis has been on introducing and integrating major
new functionality.
</p>

<p align="justify">
Over 670 contributors, of which 102 new to the KDE community, have added their
code to the main branch in the 6 months of development of KDE SC 4.4. implemented
over 1400 feature requests, while more than 7200 bugs have
been fixed. The latter was of course also the result of the hard work of the
many people tirelessly triaging, testing and reporting bugs - well over 20.000
bugs have been reported in the last 6 months, while more than 21.000
have been closed. Special credit for this sometimes thankless work goes to the
top three bug triagers Darío Andrés R., Myriam Schweingruber and Leonardo
Finetti.
</p>

<p>
Like with the previous releases, version 4.4 is not the end station but only another
milestone along the road of KDE development. The platform is designed and intended
to keep on growing far into the future, and the KDE Team would like to invite you to
join us in this fantastic journey.
</p>

<p>
This visual feature guide provides an overview of what is new and improved in this
latest incarnation of the Plasma workspaces and KDE Applications. It also includes
a glimpse at the additions to the KDE Platform. For a short list of improvements,
see the <a href="https://www.kde.org/announcements/4.4/">Release Announcement</a>. While
this feature guide credits people who created, finished or worked extensively on
each feature, many others also contributed whose names are not listed. Properly
crediting everyone involved would be difficult, so we have relied on
<a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">the feature
plan</a> as the primary source of information about contributions.
</p>

<h2>Screenshots and Screencasts</h2>
<p>
The KDE community has been busy putting media online to show what our latest software can do. You can find movies on the <a href="http://www.youtube.com/user/kdepromo">KDE promo youtube channel</a> and on <a href="http://blip.tv/search?q=kde44">blip.tv</a> and by searching with the KDE44 tag on <a href="http://www.flickr.com/search/?q=kde44">flickr</a> and other social media sites. You are of course invited to add media yourself, tagging it with the KDE44 tag so everyone can find it! Blogging, tweeting and denting is also appreciated.
</p>
<p>
And if you're up to it, you can join the fun at any of the <a href="http://community.kde.org/Promo/ReleaseParties/4.4">release parties</a> around the world! Everyone is invited and there will be more than 25 parties all around the world.
</p>

<table width="400" border="0" cellspacing="20" cellpadding="8">
<tr>
    <td width="32">
        <a href="../plasma"><img src="/announcements/4/4.4.0/images/desktop-32.png" /></a>
    </td>
    <td>
        <a href="../plasma"><strong>The KDE Workspaces</strong>: Plasma,
KWin, KRunner and the Social Desktop</a>
    </td>
</tr>
<tr>
    <td>
        <a href="../applications"><img src="/announcements/4/4.4.0/images/applications-32.png" /></a>
    </td>
    <td>
        <a href="../applications"><strong>The KDE Applications</strong>: Nepomuk, printing, Dolphin and Gwenview</a>
    </td>
</tr>
<tr>
    <td>
        <a href="../pim_network"><img src="/announcements/4/4.4.0/images/internet-32.png" /></a>
    </td>
    <td>
        <a href="../pim_network"><strong>The KDE Personal Information Management and
Network applications:</strong> Kaddressbook, Blogilo, KGet and Kopete</a>
    </td>
</tr>
<tr>
    <td>
        <a href="../edu_games"><img src="/announcements/4/4.4.0/images/education-32.png" /></a>
    </td>
    <td>
        <a href="../edu_games"><strong>The KDE Educational Applications and
Games:</strong> Kstars, Parley, Marble, Cantor and Rocs</a>
    </td>
</tr>
<tr>
    <td>
        <a href="../platform"><img src="/announcements/4/4.4.0/images/development-32.png" /></a>
    </td>
    <td>
        <a href="../platform"><strong>The KDE Development Platform</strong>: Nepomuk, Akonadi, KAuth and KNewStuff3</a>
    </td>
</tr>
</table>
<br/>
<p>
<strong>Credits</strong> for this Visual Guide go to Jos Poortvliet, Luca Beltrame, Justin Kirby, Aron Asor, Sebastian Kuegler, Vivek Prakash, Ricky Laishram, Carl Symons and Stuart Jarvis, the KDE Promo Team and several KDE developers who have all helped making this overview of new and "old" features in KDE SC 4.4 possible.
</p>

