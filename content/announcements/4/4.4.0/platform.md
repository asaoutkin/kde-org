---
title: Development Platform
hidden: true
---

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../edu_games">
                <img src="/announcements/4/4.4.0/images/education-32.png" />
                Previous page: Educational Applications and Games
                </a>
        </td>
        <td align="right" width="50%">
		<a href="../guide">Back to Overview
                <img src="/announcements/4/4.4.0/images/star-32.png" />
		</a>
        </td>
    </tr>
</table>

<h2>Discover the Powerful KDE Development Platform</h2>

<p>
Over the years, the KDE community has build a comprehensive set of applications. While doing so, we have put a strong focus on sharing technology. Shared functionality in libraries makes our applications simpler and spreads the workload. It also allows developers to focus on doing new, unique things - the common functionality is readily available. For users, the common framework ensures that applications work well, are light on memory and stable. All KDE libraries are fully LGPL or BSD compatible - making them ideal to be used where and how you want. We also have a great suite of tools to make it easy to develop using the KDE Platform.
</p>

<h2>Nepomuk for developers</h2>
<p>
Nepomuk's new Virtuoso storage backend provides the performance to make Nepomuk really shine. For application developers, this means that you can enhance your application with metadata from files in the index, or integrate search capabilities using all kinds of criteria. This can be especially interesting if you want to offer related files in the context of your UI. It can also make it easier to offer files to open on a splash screen for example. All of this functionality is contained in kdelibs, so you will get all those features with a standard KDE installation. There's no need to compile or install extra dependencies, so you can be confident that your users will be able to use the new features you build.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_timeline-october.png">
	<img src="/announcements/4/4.4.0/thumbs/44_timeline-october.png" class="img-fluid">
	</a> <br/>
	<em>The Nepomuk based timeline view</em>
</div>
<br/>

<p>
Nepomuk offers an easy to use <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/nepomuk/html/namespaceNepomuk_1_1Query.html">API for generating queries</a>, for querying the index, for displaying search results and metadata, and for manipulating resources in its index. These tasks have been reduced in complexity, while keeping Nepomuk's flexible concepts in place. Learning how use Nepomuk search in your applications takes a few minutes, while it offers huge potential to introduce your applications to a new level of file or resource management. <a href="http://trueg.wordpress.com/2010/01/26/what-we-did-last-summer-and-the-rest-of-2009-a-look-back-onto-the-nepomuk-development-year-with-an-obscenely-long-title/">Read more</a> on the blog of core Nepomuk developer Sebastian Trueg.
</p>

<h2>Akonadi Groupware Cache</h2>
<p>
KDE SC 4.4 is the first KDE SC release to make use of Akonadi, the powerful framework for PIM applications. Akonadi provides a unified access and caching mechanism for all kinds of PIM information -- emails, contacts or calendar information. KDE SC 4.4 marks the beginning of the Akonadi era in KDE. KAddressbook is a complete rewrite of the "old" KAddressBook, and the first application using Akonadi. More applications, such as KDE's mail client KMail will follow suit with the release of SC 4.5 this summer. Akonadi tears down the walls between applications, and makes it easy to use PIM data throughout your desktop and applications. In addition, Akonadi integrates well with Nepomuk Desktop Search, as all data entered into Akonadi is automatically indexed by Nepomuk, enabling "One Search to Rule Them All".
</p>
<p>
On a platform level, as Akonadi has matured, it has gained some nice new features as well. Szymon Tomasz Stefanek joined the Akonadi crowd to implement a filtering framework, that is, the new filtering system for the Akonadi platform. Filtering is what is used to discard spam, to move the messages from your boss to a special folder, or to play a sound when mail from your fiancée arrives. Most of the work is at the API level; only the top is visible in the UI.
</p>

<h2>Sounding Phonon </h2>
<p>
Martin Sandsmark worked on Phonon, allowing it to export audio data. This makes it possible to create visualizations in applications like Amarok, Juk or Dragon player. Colin Guthrie developed PulseAudio integration for Phonon, including the needed changes in KMix. He still has a lot to do for the next release. But the basics have been implemented, and KDE applications can now work properly with Pulseaudio.
</p>

<h2>Secure Authorization</h2>

<p>
Dario Freddi and Nicola Gigante worked on secure authorization. Their aim was to provide fine-grained user privilege elevation using the FreeDesktop.org PolicyKit technology. PolicyKit provides a D-Bus interface offering control for actions that can be run in a different user context (such as running certain commands as root). Freddi readied the KAuth library for inclusion into kdelibs, and integrated it with various elements of the user interface, such as the Date and Time configuration module in KDE's System Settings. He also added a specialized version of a KPushButton that can be attached to certain actions to be run with elevated privileges. This makes it almost trivial to provide direct manipulation of system administration tasks in a secure way, well integrated in the user interface and with policies the underlying operating system provides. KAuth currently works on Linux and Mac systems, support for the Windows platform will be added soon. More work is needed on integrating KAuth into System Settings and other parts of KDE. But the work completed paves the way to a more secure and, at the same time, more user-friendly integration of KDE's desktop and applications with the underlying OS.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/kauth-datetime.png">
	<img src="/announcements/4/4.4.0/thumbs/kauth-datetime.png" class="img-fluid">
	</a> <br/>
	<em>The new authentication framework in action changing the date and time</em>
</div>
<br/>

<h2>Lockdown Kiosk</h2>
<p>
The KDE Plasma team has been talking with people working on several large deployments in Europe about lock down and deployment management. Some efforts have already been put into that for the Plasma 4.4 release, and more can be expected for 4.5. Find more <a href="http://techbase.kde.org/Projects/Plasma/4.4-Kiosk">info about Kiosk in Plasma on Techbase</a>.
</p>

<h2>Share KNewStuff3</h2>
<p>
The GHNS (GetHotNewStuff) developers have written the libattica library which accesses the <a href="http://www.freedesktop.org/wiki/Specifications/open-collaboration-services">Open Collaboration Services</a> (a <a href="http://freedesktop.org">freedesktop.org</a> specification). Currently, only <a href="http://opendesktop.org">OpenDesktop.org</a> supports these services but at <a href="http://maemo.org">maemo.org</a> people are working on a second provider and the <a href="forum.kde.org ">KDE forum team</a> will also introduce open collaboration services.
</p>
<p>
Additionally to the specification on freedesktop.org the OCS serverside PHP libary will soon be available with the AGPL license, enabling other websites to easily become Open Collaboration Service providers.
</p>
<p>
The new KNewStuff3 framework in the KDE libraries is building upon this technology. It now supports serverside search and social features, allowing users to become fan of an item and rate it. Bigger previews are possible, there is a basically working upload dialog to share creativity with others and there is configuration in System Settings. While this 'stuff' is still very new, several applications already use it, including the Plasma image wallpaper, the Comic plasma applet, Parley and Kate. Find more information in the api documentation <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/knewstuff/html/classKNS3_1_1DownloadDialog.html">here</a> and <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/knewstuff/html/classKNS3_1_1UploadDialog.html">here</a> and <a href="http://techbase.kde.org/Development/Tutorials/Collaboration/HotNewStuff/Introduction">here</a> on techbase. Of course, feel free to contact the developer Frederik Gladhorn in #kde-devel on IRC (freenode), his nick is fregl. He also wrote <a href="http://blogs.fsfe.org/gladhorn/2010/01/10/new-stuff-again/">a blogpost for developers</a>.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_gethotnewstuff_list.png">
	<img src="/announcements/4/4.4.0/thumbs/44_gethotnewstuff_list.png" class="img-fluid">
	</a> <br/>
	<em>KNewStuff details dialog</em>
</div>
<br/>

<h2>Bind your Languages</h2>
<p>
On the front of the bindings for interpreted languages, such as Ruby, Python and C#, a lot of work has been done. Simon Edwards updated the PyKDE4 bindings to support the new Software Compilation features, and also added support for Python 3 (the new, non backwards-compatible Python version). For his GSOC 2009 project Arno Rehn rewrote the bindings generator for the Smoke libraries that are used as a basis for several Qt and KDE language bindings. It features an industrial strength compiler derived from the QtJambi and KDevelop C++ parsers called 'smokegen'. This tool can be used by all KDE developers, not just language bindings specialists, to create 'Smoke' libraries for the apis of their libraries and applications.
</p>
<p>
Once a Smoke library has been created, it is straightforward to implement plugins or extensions to use with it for JavaScript, Ruby and other languages. We have split up the Smoke libraries so that there is now a 1:1 mapping between each C++ library wrapped and a corresponding smoke libary. A new command line tool called 'smokeapi' is provided to introspect the Smoke libraries and query the methods and classes in them. We will shortly be adding documentation to the TechBase Wiki to explain how to use these new tools.
</p>
<p>
Upcoming will be improvements to the ECMA script bindings for Plasma, especially in the documentation area. Keep an eye on the <a href="http://dot.kde.org">dot</a> for news about this!
</p>

<h2>Remote control KDElibs</h2>
<p>
Michael Zanetti developed a solid API for remote controls. It supports backends but currently only a LIRC backend is implemented. This API makes KDElirc independent of the underlying backend. In time, a backend for Bluetooth or ZigBee would be possible, as would be a Windows, Mac or any other platform backend. Another advantage is the easy usage of the Solid API - any KDE application can easily make use of remote control technologies this way. Alex Fiestas worked on the KDE Solid based Bluetooth stack, making it more stable and robust.
</p>

<h3>Tools and applications</h3>
<p>
Besides the extensive development platform, the KDE community has created a plethora of development tools. These range from simple convenience scripts to a lightweight IDE (Kate) and a fully equipped, state of the art development environment (KDevelop). The latter is not part of the official KDE Software Compilation releases but a release is imminent and worth watching.
</p>

<h2>Hexing Okteta</h2>

<p>
Okteta, KDE's hex editor, received not only a little brush-up for all its little tools, but also gained new functionality from its author Friedrich Kossebau.
</p>
<p>
The main view can now be split, so the same byte array can be viewed with different ranges and settings at the same time. Some tool dialogs are shown embedded at the bottom of the current view, like the "Go to Offset" dialog and the new “Select range” tool, which enables direct entry of the range of the selected bytes. The dialogs for searching and replacing are still floating, but now also accept UTF-8 input (though only case-sensitive currently). Selected data can be exported or copied to the clipboard in the Base64 format. The new concept of data generators is used both for creating new byte arrays or for data to be inserted into existing ones. Generators available are "Pattern", "Random Data", and "Sequence". Values in the Decoding table can now be edited and are synched back to the byte array (not with UTF-8, handling of possibly changing byte count not yet done).
</p>
<p>
A useful external contribution has been made with the initial version of the Structures tool from Alex Richardson. This is for analyzing and editing of byte arrays based on user-creatable structure definitions. In this version, these definitions can be built from arrays, unions, primitive types and enum values. Users are encouraged to share their definitions at <a href="http://kde-files.org/index.php?xcontentmode=691">KDE-Files.org</a>. In future versions of the Structures tool, the sharing will also be supported using GHNS.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44-okteta.png">
	<img src="/announcements/4/4.4.0/thumbs/44-okteta.png" class="img-fluid">
	</a> <br/>
	<em>Overview of Oktetas new features</em>
</div>
<br/>

<p>
Okteta's KPart, useful for embedded viewing of a file's raw data in Konqueror and KDevelop, finally shares most of the UI code with the Okteta program, so the menu entries are consistent.
</p>

<h2>Advanced text editing with Kate</h2>
<p>
Joseph Wenninger has been working on the snippets plugin in Kate. Snippets are a nice and powerful way of creating or expanding large texts, and very powerful thanks to the included variable support. This functionality is shared with the powerful KDE IDE, KDevelop, and allows users to quickly add small pieces of pre-made text in a document. Snippets can be shared and downloaded conveniently throug the GetNewStuff interface and inserted in several ways like code-completion or from a snippet list. Snippets support variables which automatically get expanded upon, like date or email address, and they will all be updated if one is changed. With the tab key you can quickly jump through the custom variables, editing them as you see fit. You can even insert snippets in other snippets.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kate-snippets.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kate-snippets.png" class="img-fluid">
	</a> <br/>
	<em>Adding a snippet by auto-completion</em>
</div>
<br/>

<p>
More information about using the KDE Development Platform can be found on <a href="http://techbase.kde.org">Techbase</a>, KDE's knowledge base. Of course, documentation is never perfect, so this site is a wiki and you are all invited to improve or add to the documentation!
</p>

<h2>The end</h2>
<p>
This also marks the end of this guide through the new features in the KDE Software Compilation. We hope you have enjoyed it and would again like to extend our thanks to all the contributors for their tireless efforts to bring great software to users all around the world! If you feel like it, you're more than welcome to join the ranks of KDE contributors by <a href="https://www.kde.org/community/getinvolved/">getting involved</a> or <a href="https://www.kde.org/community/donations/">doing a donation</a> so others can continue this amazing work!
</p>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../edu_games">
                <img src="/announcements/4/4.4.0/images/education-32.png" />
                Previous page: Educational Applications and Games
                </a>
        </td>
        <td align="right" width="50%">
		<a href="../guide">Back to Overview
                <img src="/announcements/4/4.4.0/images/star-32.png" />
		</a>
        </td>
    </tr>
</table>
