---
title: Applications Leap Forward
hidden: true
---

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../plasma">
                <img src="/announcements/4/4.4.0/images/desktop-32.png" />
                Previous Page: KDE Workspaces
                </a>
        </td>
        <td align="right" width="50%">
                <a href="../pim_network">Next page: Network and Personal Information Management
                <img src="/announcements/4/4.4.0/images/internet-32.png" />
		</a>
        </td>
    </tr>
</table>

<h2>KDE Applications Grow New Features</h2>
<p>
The KDE community has developed a wide range of applications. Some of those are meant to fulfill basic needs like file and image management or playing music; others offer more advanced functionality like helping you to learn languages or playing a game. The following section highlights some general improvements and changes in various applications; further pages focus on the networking and communcation applications, the KDE educational applications and the KDE games.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_4apps.png">
	<img src="/announcements/4/4.4.0/thumbs/44_4apps.png" class="img-fluid">
	</a> <br/>
	<em>KDE Software Compilation provides many basic applications and utilities</em>
</div>
<br/>

<h3>All Applications</h3>
<p>
Some improvements in the underlying infrastructure of the KDE Platform affect all KDE Applications. The shared functionality between KDE applications makes it possible for KDE developers to improve all of the KDE Software Distribution in one go. For the 4.4 release, the Oxygen team introduced a variety of animations in the default Oxygen theme, giving the interfaces a more natural feel. This was implemented by Hugo Pereira Da Costa who also worked on integrating the default Oxygen window decoration with his own highly configurable <a href="http://kde-look.org/content/show?content=99551">Nitrogen decoration</a>. The KIOslaves provide KDE applications with access to local and remote file systems. Thanks to Carlo Segato, the Samba KIOslave stores passwords in KWallet, saving you the trouble of remembering them all. Jacopo De Simoi and Harsh J improved the thumbnail support in the KIOslaves, so now your images are properly rotated and previews of comic book files are shown.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kwrite_openremote.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kwrite_openremote.png" class="img-fluid">
	</a> <br/>
	<em>Applications built on top of the KDE Platform can access remote files using the KIO technology</em>
</div>
<br/>

<h3>Finding Nepomuk</h3>
<p>
Nepomuk is the semantic search framework in the KDE Software Compilation. For users, it provides integrated and pervasive search options in all applications built on the KDE platform. You can bookmark queries to the search index, and use them like real folders from all your applications. Tagging files allows you to group certain files under more than one topic, adding flexibility to your filesystem hierarchy. Nepomuk doesn't replace the traditional view on the filesystem, but augments it with metadata and linking between files and other, more abstract concepts, called "Resources". An example of this is the newly introduced timeline. In any location bar within a KDE application (like in Dolphin and Konqueror, but also in File Open and Save dialogs), you can access this timeline view by typing in "timeline:/" as location. This will show you a view of files you have accessed grouped by date, making it easier to find what you've been working on. You can add this timeline view to your Places sidebar in Dolphin and in file dialogs either by dragging the location or by clicking with your right mouse button in the sidebar and choosing "add".
</p>

<p>
KDE SC 4.4 delivers a much faster version of Nepomuk, making it a useful way to find your data. The full-text indexing provided by Strigi's libstreams collects metadata for files based on their content. This makes it easy to search, for example, the contents of OpenDocument and PDF files, so you can find files based on keywords contained in them. Libstreams provides support for a large (and growing) number of file formats. Great performance improvements come from the new Virtuoso storage backend that is used for Nepomuk in 4.4. Virtuoso provides both performance and features Nepomuk greatly benefits from today, and even more in future releases.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_systemsettings_nepomuk.png">
	<img src="/announcements/4/4.4.0/thumbs/44_systemsettings_nepomuk.png" class="img-fluid">
	</a> <br/>
	<em>Switching on semantic search features and file indexing can be done from system settings</em>
</div>
<br/>

<p>
In order to use the Desktop Search functionality throughout KDE, go to System Setting's Advanced Tab, choose Desktop Search and enable the Nepomuk Semantic Desktop and the Strigi file indexing. Soon after that, Nepomuk will start indexing your folders. Indexing will only happen when the system is otherwise idle, so as not to disturb your workflow and make the machine less responsive while you are using it. File indexing will also be suspended when you run on battery. KDE's hardware framework Solid provides these features that are used to save battery power by delaying energy-consuming disk access and CPU cycles to a later point. Nepomuk automatically picks up changed files. After initial indexing, it only incrementally updates its index.
</p>

<h3>Printing</h3>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_okular_printing.png">
	<img src="/announcements/4/4.4.0/thumbs/44_okular_printing.png" class="img-fluid">
	</a> <br/>
	<em>Printing odd and even pages is now possible, along with many more printing features</em>
</div>
<br/>

<p>
The KDE print subsystem has been further improved by John Layt for this Software Compilation release. In particular, John added some requested features with regards to page selection when printing. Support for printing only odd or even pages has been added to the print dialog. Certain applications (such as Kate or Konqueror, or the ones that make use of them) can now print page ranges as well.
</p>

<h3>Speech support</h3>
Jeremy Whiting has been improving speech support - an area untouched for almost 2 years. While a gui to configure speech hasn't been finished yet, the automatic configuration has been improved and there is a commandline configuration tool named spd-conf.

<h2>The Social Desktop, Open Desktop and Silk Initiatives</h2>

<h3>Social Desktop</h3>
<p>
The social desktop initiative by KDE aims to integrate a variety of social web features right into your desktop. While the first fruits of this work appeared in the KDE SC 4.3 release, development continues. Among others, Eckhart Wörner, Marco Martin and Frederik Gladhorn worked on this, introducing new widgets and improvements to the current offerings. With the new Knowledge Base widget, you can access an online database of questions and answers about KDE applications, and browse it easily from your desktop. The Community widget (previously OpenDesktop Widget) lets you interact with your friends directly from the desktop. Add the widget, supply your OpenDesktop username and password, and follow activities of your friends. You can easily update your status and connect to others. The widget also allows you to update your location conveniently using Plasma's geolocation features. The Social News widget notifies you about events of possible interest, such as a new version of a favorite application, a nearby KDE event or a knowledge base reply to a question you asked
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_social_desktop1.png">
	<img src="/announcements/4/4.4.0/thumbs/44_social_desktop1.png" class="img-fluid">
	</a> <br/>
	<em>Social Desktop widgets in Plasma</em>
</div>
<br/>

<h3>OpenDesktop</h3>

<p>
Started a few years ago by Frank Karlitschek, the OpenDesktop project aims to free your online data. Bringing online content to your desktop decreases reliance on a few large providers and allows you to connect data seamlessly, no matter where it comes from. You have a friend who's on a different social networking site, and you don't feel like joining it? You have a huge contact list on a social site where you can send messages, but you would like to send an ordinary email? These are the kind of issues OpenDesktop will help to resolve.
</p>
<p>
Right now, the infrastructure has been built. There are standard APIs and libraries. Currently, only <a href="http://opendesktop.org">OpenDesktop.org</a> supports these services, but at <a href="http://maemo.org">maemo.org</a>, people are working becoming the second provider <!--(jos: asked Henri Bergius about progress on this by mail)--> and the <a href="forum.kde.org ">KDE forum team</a> will also introduce open collaboration services. In the future, we can expect more service providers to appear.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/desktop.png" class="img-fluid">
	</a> <br/>
	<em>Integration of online communities through Open Collaboration Services</em>
</div>
<br/>

<p>
For you as an end user, this means you will have to join a new social network - but this one is open. This time, no single provider will be able to lock you in and control your data - it is yours. You want to download your contact list? Done. You want to download all your images and the comments & tags on them, and migrate it to another website? No problem. Currently the small number of providers and users limits the usefulness of the Open Network being created. In time, we hope other providers will join and open up their data - so everybody will profit from our work. Using plugins, developers could create connectivity to Facebook, Linkedin and other well-known networks. For now, we hope you will join OpenDesktop and help us gain mass!
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_social_desktop2.png">
	<img src="/announcements/4/4.4.0/thumbs/44_social_desktop2.png" class="img-fluid">
	</a> <br/>
	<em>Various online services as first class citizens on your workspace - desktop or netbook</em>
</div>
<br/>

<h3>Downloading KNewStuff</h3>
<p>
In addition to the social features of the OpenDesktop work used by the Plasma widgets, another feature of online content integration has been improved. The KNewStuff technology, introduced by KDE over 5 years ago, has seen a major update. KNewStuff is technology that allows applications to offer you an easy dialog to download additional content. You don't have to search the web for new Parley languages or new desktop wallpapers, and then go through a process of downloading and installing them - KNewStuff brings them to you faster and easier.
</p>
<p>
Social features are new in KNewStuff - you can become a fan and vote for your favorites. Unlike in previous versions, all available content can be downloaded from the server, so you can continue to scroll through the results until you've seen everything. There is also a tool that searches content on the server. The most interesting feature, while still a bit rough, is the ability to upload content. You can share your work with the world without having to know the technical details of packaging or uploading.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_ghns_wallpapers.png">
	<img src="/announcements/4/4.4.0/thumbs/44_ghns_wallpapers.png" class="img-fluid">
	</a> <br/>
	<em>Downloading additional content is interactive now</em>
</div>
<br/>

<p>
Like its predecessor, KNewStuff supports updating, showing a new version of a resource (puzzle, language file, widget) which you can click to install. Viewing and adding comments is in the making. While this 'NewStuff' is still new, several applications already use it, including the Plasma image wallpaper, the Comic Plasma applet, the Parley language training application, and the Kate text editor.
</p>

<h3>Project Silk</h3>
<p>
A third web team is working on Project Silk, aimed at full integration of data into the desktop and applications. While Silk is a young project, the first Silk components are already integrated in KDE SC 4.4, especially Plasma. The Webslice widget lets you show a part of a webpage on your desktop. Do you have a favorite website showing the weather, but also "less interesting content"? You can put the interesting slice on your desktop -- fully interactive.
</p>
<p>
The MediaWiki Runner is a plugin to KRunner, the versatile mini-commandline that makes your desktop life so easy. The MediaWiki Runner provides search access to various MediaWiki instances. It enables searching in Wikipedia, Wikitravel, and KDE's UserBase and TechBase. The KDE team doesn't want to put too much demand on the servers of these providers, so the MediaWiki Runners are switched off by default. They are easy to enable using KRunner's configuration under the wrench icon. If you want to add support for another MediaWiki installation, maybe your the one of your company, that is now as easy as adding a .desktop file with the URL to your MediaWiki installation.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/krunner-mediawiki.png">
	<img src="/announcements/4/4.4.0/thumbs/krunner-mediawiki.png" class="img-fluid">
	</a> <br/>
	<em>KRunner searches in Wikipedia and Wikitravel</em>
</div>
<br/>

<p>
Those are the two components of KDE Silk entering stable state; there are more interesting things to come.
</p>
<h3>Manage Your Files With Dolphin</h3>
<p>
Dolphin is the default KDE file manager. It is both easy to use and full-featured, with an optional dual-pane interface and tabbed file browsing. The 4.4 release brings an improved search interface which allows you to select a series of attributes so you can quickly find the files you are looking for, thanks to the work by Adam Kidder. You can save your searches and they will be kept updated, so you'll always have live results without the need to re-run your query manually.
</p>
<p>
When selecting a file, Dolphin shows you data about the file in the metadata sidebar. You can add and edit tags, comments and ratings. Clicking on a tag now shows you all other files with the same tag. Tab behavior has become more consistent; Dolphin now opens a location in a tab if you middle click a browsing button (back, forward or up), just as it does when you middle click on a folder. A final feature added by Peter Penz, the maintainer of Dolphin, is the addition of version control support. If you enter a folder which is under version control support by Subversion, it will show appropriate options in the file context menu like update and commit.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_dolphin_search.png">
	<img src="/announcements/4/4.4.0/thumbs/44_dolphin_search.png" class="img-fluid">
	</a> <br/>
	<em>Desktop Search using Nepomuk is now integrated into the Dolphin File Manager</em>
</div>
<br/>

<h3>Gwenview images and videos</h3>
<p>
Aurelien Gateau has once again added a variety of incremental improvements to Gwenview. Folders now show thumbnails of their contents, the start page has been improved and <a href="http://agateau.wordpress.com/2009/11/06/gwenview-importer/">a new image importer has been developed</a>. The latter makes it easy to quickly get your photos from your camera or your phone onto your computer, including capabilities like renaming the images. As usual, Aurelien has worked hard to make the whole process as intuitive as possible, something you will certainly notice when using it.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_gwenview_import.png">
	<img src="/announcements/4/4.4.0/thumbs/44_gwenview_import.png" class="img-fluid">
	</a> <br/>
	<em>Importing images made easy using Gwenview</em>
</div>
<br/>

<h3>Look at Your Documents Through Okular</h3>
<p>
Okular developer Albert Astals Cid worked with Jochen Trumpf to provide Okular with the capability to do forward and backward search linking. This means you can shift-rightclick a line in a pdf document generated by a LaTeX source file, and Okular will start your favorite LaTeX editor and show the corresponding source line. While working with LaTeX markup source, it is convenient to be able to view the formatted results. Therefore the improved forward and back search linking in Okular makes it easier to identify the sources of formatting problems or see the effects of changing the markup. KDE produces an easy-to-use LaTeX editor (Kile) and KDE's founder, Matthias Ettrich, is also well known for starting the LyX project, a popular cross-platform Qt-based LaTeX editor.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_okular_latex.png">
	<img src="/announcements/4/4.4.0/thumbs/44_okular_latex.png" class="img-fluid">
	</a> <br/>
	<em>Okular now supports jumping to the correct place in your editor -- very handy for LaTeX users</em>
</div>
<br/>

<h3>Listen to JuK</h3>
<p>
The default audio player in the KDE Software Compilation, JuK, has been improved to simplify its use, with the ability to disable cross-fading and support for MP4 and ASF files (as long as the system has this support in Taglib). Michael Pyne and Jeff Mitchell implemented these improvements, taking advantage of some Kubuntu patches.
</p>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../plasma">
                <img src="/announcements/4/4.4.0/images/desktop-32.png" />
                Previous Page: KDE Workspaces
                </a>
        </td>
        <td align="right" width="50%">
                <a href="../pim_network">Next page: Network and Personal Information Management
                <img src="/announcements/4/4.4.0/images/internet-32.png" />
		</a>
        </td>
    </tr>
</table>
