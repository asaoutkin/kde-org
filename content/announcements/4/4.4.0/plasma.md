---
title: Workspace Improves User Experience
hidden: true
---

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../guide">
                <img src="/announcements/4/4.4.0/images/star-32.png" />
                Previous Page: Overview
                </a>
        </td>
        <td align="right" width="50%">
                <a href="../applications">Next page: Basic applications
                <img src="/announcements/4/4.4.0/images/applications-32.png" />
		</a>
        </td>
    </tr>
</table>

<h2>Plasma introduces new workspaces and more freedom</h2>

<div align="right">
<img src="/announcements/4/4.4.0/images/plasma-logo.jpg" align="right" hspace=10 />
</div>

<p>
The KDE Plasma Workspaces provide a set of modern interfaces designed for a variety of devices. The 4.4 release introduces the Plasma Netbook workspace, an interface specifically optimized for mobile devices with smaller screens. The existing Plasma Desktop workspace received numerous improvements, including the integration of on-line and social media content, as well as the ability to share widgets with others over the network.
</p>

<h3>Improved window management</h3>
<p>
Martin Gräßlin made many enhancements to window switching, making it easier than ever to find and control your windows. The Present Windows effect, which shows all windows from either the current (CTRL-F9) or all desktops (CTRL-F10) scaled on the screen, can now also show only the windows belonging to a specific application when you press CTRL-F7. The desktop grid, which provides you with an overview of the virtual desktops, now uses a Present Windows effect on each desktop so that all the windows on each desktop are visible, giving instant access to every application. In addition, it now possible to configure what happens when you click on windows - for example, you can close a window with a middle-click on a scaled preview.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/kwin.png">
	<img src="/announcements/4/4.4.0/thumbs/kwin.png" class="img-fluid">
	</a> <br/>
	<em>The Cube Desktop Switcher in Plasma</em>
</div>
<br/>

<p>
The venerable ALT-TAB application switching was also enhanced by Martin. You can now navigate the list of windows with the cursor keys and use middle click to close an application. There is a configuration window added to System Settings to configure the way ALT-TAB shows items. You can have the windows show horizontally, vertically, compact or more verbose. You can also have a second shortcut configured for a different window switcher. For example, you could activate the Present Windows switcher with META-TAB, and keep ALT-TAB for the traditional list-like application switch view.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_krunner_window_management1.png">
	<img src="/announcements/4/4.4.0/thumbs/44_krunner_window_management1.png" class="img-fluid">
	</a> <br/>
	<em>KRunner can also be used to switch to a certain opened application or document</em>
</div>
<br/>

<p>
To aid in handling windows, Martin Gräßlin and Lucas Murray added the ability to maximize and tile windows by dragging them to an edge of the screen. You can grab a window and drag it to the top of the screen to maximize it, or drag two windows to opposite screen edges to put them side by side. You can see this in the following screencast:
</p>

<div class="text-center">
	<em>Managing Windows Made Easier </em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">Download</a></div>
<br/>

<p>
Jorge Emilio Mata took window management to the next level with window tabbing. This feature makes it possible to group windows with a set of tabs where the upper window border would normally be. With the middle mouse button, you simply drag a window to the decoration bar of another window and they will group. You can also right click on the upper window border ("window decoration") and choose "Move window to group". The window group can be easily moved or closed. With many open windows, this will certainly reduce clutter.
</p>

<h3>New and improved Plasma widgets and behavior</h3>

<p>
Despite differences in behavior and look, all Plasma workspaces (currently, Desktop and Netbook) support all Plasma widgets. The KDE Software Compilation ships with a number of default widgets; many more are available from the community. Widgets can be added to the workspace from a new "Add widget" interface designed by Ana Cecília Martins Barbosa. It is now easier to browse available widgets. The new interface attaches to the user's main panel, instead of obscuring the desktop with a separate window.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_add_widget.png">
	<img src="/announcements/4/4.4.0/thumbs/44_add_widget.png" class="img-fluid">
	</a> <br/>
	<em>Plasma's new interface for adding widgets to the desktop or panel</em>
</div>
<br/>

<p>

Anselmo Lacerda S. de Melo and Petri Damsten further developed an interface to aid in installing new widgets either from the online Plasma widgets database or from downloaded files like Google Gadgets.

</p>

<p>
Plasma developer Rob Scheepmaker has made it possible to share Plasma widgets over a network. It is now possible for widgets to be interacted with remotely and simultaneously by several people - such as sharing notes and files, or drawing on a blackboard together! For example, a train station could use a remote broadcasting widget to allow travelers to check up-to-date departure times quickly from home. Note that security plays an important part in Plasma widget sharing and all messages are signed. Access can be controlled by limiting remote machines to particular services, by granting access using a Bluetooth-like password pairing mechanism (requiring the same password to be entered on both machines), or by disallowing remote access entirely.
</p>

<p>
Lukas Appelhans made the Quicklaunch Plasma widget more flexible, so icon size and the number of rows are now configurable. Bruno Bigras and Marco Martin enhanced Fredrik Höglund's Folderview widget by providing selection icons when hovering files or directories with the mouse (like Dolphin does). Michal Dutkiewicz created a widget that shows a list of open windows. The various Plasma weather widgets now have access to wetter.com data, besides the existing NOAA, BBC and Environment Canada providers thanks to Thilo-Alexander Ginkel. Shafqat Bhuiyan made it possible to use a variety of gradients as a desktop background.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_folderview_hover.png">
	<img src="/announcements/4/4.4.0/thumbs/44_folderview_hover.png" class="img-fluid">
	</a> <br/>
	<em>The Folderview widgets lets you drill down into directories by hovering with the mouse</em>
</div>
<br/>

<p>
Jacopo De Simoi and Giulio Camuffo worked on the Device Notifier, the widget that shows plugged-in devices and media such as USB sticks and DVD's. The widget now allows users to choose actions (such as Open, Play, etc.) within the widget, rather than bringing up a separate options dialog, so it's quicker and easier to handle devices. Sebastian (Sebas) Kügler further improved the battery applet by simplifying the layout, making it more usable and less cluttered. The Network Manager applet received attention from a whole team including Sebas, Peder Osevoll Midthjell, Sveinung Dalatun, Anders Sandven, Darío Freddi and Frederik Gladhorn and of course Will Stephenson. Network Management is now more dependable and supports more types of connections. This was all made possible by the earlier work of Will who has put considerable effort into network management over the years.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_device_notifier.png">
	<img src="/announcements/4/4.4.0/thumbs/44_device_notifier.png" class="img-fluid">
	</a> <br/>
	<em>Plasma's Device Notifier makes interaction with your removable media easy</em>
</div>
<br/>

<p>
Plasma developer Marco Martin has improved the system tray applet. In particular, the system tray can now embed Plasma widgets in addition to normal application status icons. The Device Notifier and Weather widgets are examples of widgets that can now be embedded into the system tray. Notifications have also been improved. After being displayed, they are grouped according to the application that generated them; the user can review them later. This reduces visual clutter and improves the overall experience, with no loss of information.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_weather_widget.png">
	<img src="/announcements/4/4.4.0/thumbs/44_weather_widget.png" class="img-fluid">
	</a> <br/>
	<em>The Weather Widget supports various online weather sources</em>
</div>
<br/>

<p>
Marco Martin also built upon the effects implemented by the KWin developers. Using KWin effects to replace some animations in Plasma makes them smoother and less resource-intensive. In addition to the sliding animations in popups and application launchers, he made it possible to quickly activate a window from a taskbar group by clicking the thumbnail. Improvements to the Present Window effect by Martin Gräßlin were leveraged by Marco to allow a ctrl-click on a taskbar group to trigger the Present Windows effect, giving you a fullscreen choice of the windows in the group.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_applets_in_systray2.png">
	<img src="/announcements/4/4.4.0/thumbs/44_applets_in_systray2.png" class="img-fluid">
	</a> <br/>
	<em>Plasma Widgets can now also enhance the system notification area</em>
</div>
<br/>

<p>
Marco also integrated Plasma configuration directly into System Settings. The feature to couple activities to desktops, previously somewhat hidden in Plasma, can now be found there. And you can configure the shortcuts directly when changing the number of desktops. Plasma adds new shortcuts automatically, and you can select the desktop switching animation as well as the on-screen display for switching desktops.
</p>
<p>
Chani Armitage implemented support for mouse actions in Plasma, so the behavior of mouse actions over the workspace can be completely configured. Such functionality can be extended through the use of plugins. Björn Ruberg developed a virtual keyboard widget for touchscreen users and Matteo Agostinelli made a new advanced calculator widget. Michal Dutkiewicz introduced the spell check widget; Sandro S Andrade developed a widget to follow KDE development through information on commits, bugs and build quality.
</p>

<h3>Plasma Desktop Workspace</h3>
<p>
The Plasma Desktop Workspace is designed for normal desktop computers and notebooks. By default it sports a panel on the bottom of the screen with an application launcher on the left, a taskbar in the center and a systemtray and a clock on the right. The desktop itself offers a wallpaper and a folderview showing the contents of your Desktop folder.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop.png" class="img-fluid">
	</a> <br/>
	<em>Plasma Desktop for normal computers and notebooks</em>
</div>
<br/>

<p>
Plasma Desktop 4.4 is the latest iteration of what has been KDE's core software product since the start. Building on a mature and usable base, it has received fewer specific new features than newer interfaces such as Plasma Netbook. However, there is still plenty new for Plasma Desktop users as it benefits from all the above-described enhancements in KWin window management and effects, as well as new and improved Plasma widgets (including remote sharing, enhanced device manager and new widget browser). The flexibility of the Plasma infrastructure also allows users to build their own desktop experiences to suit their own particular workcases. For example, a student could use multiple Folder View widgets for quick access to their files, a Remember the Milk widget to keep track of homework, and a timer widget to know when to take breaks or switch subjects.  Someone collaborating internationally with others might use a World Clock widget to check the time in different places.  For more ideas, see the <a href="http://www.flickr.com/groups/plasma-desktop/">Plasma Desktop Flickr group</a>, and feel free to add your own desktop!
</p>

<div class="text-center">
	<em>Improved Interaction with the Plasma Desktop Shell</em><br/>
<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">Download</a></div>
<br/>

<p>
Davide Bettio has overhauled the structure of Plasma's configuration dialogs in an effort to reflect the mental model of the user closer, rather than the technological structure of these components. You can now easily set your virtual desktops to each have its own plasma activity with it's own widgets and background.
</p>

<h3>Plasma Netbook Workspace</h3>

Plasma Netbook screenshot

<p>
The Plasma Netbook Workspace offers a user interface optimized for mobile use. It is built around the constraints of devices with smaller screens and alternative input devices like touch screens but also focuses on use cases related to the Web. The special widget page page provides direct access to a variety of information on the Web, and a special search and launch interface makes selecting applications and data easier. It does not feature a taskbar, relying on a direct display of active applications instead. It does have a panel on top, which integrates shortcuts to the active applications, the Search and Launch (SAL) interface, one or more "pages" that can contain various widgets. and some system information like a clock, network and battery status.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_netbook_newspaper.png">
	<img src="/announcements/4/4.4.0/thumbs/44_netbook_newspaper.png" class="img-fluid">
	</a> <br/>
	<em>Plasma Netbook's widget page</em>
</div>
<br/>

<p>
Aside from its tailored interface, the Plasma Netbook Workspace has some extra features that have been developed to ease working on devices where screen space is usually limited. As noted above, application switching does not use a taskbar like in Plasma Desktop.  Instead, clicking an "application list" button displays all the windows currently open (using KWin's Present Windows effect). Following the same philosophy, the main panel auto-hides every time applications have focus. Finally, the windows are maximized by default and have minimal borders.
</p>

<h3>Start applications and find data</h3>
<p>
Plasma Netbook's application launcher page is centered on search instead of menu browsing, and leverages the KRunner plugins to find what you are looking for in a full-screen view. Application switching, access to your files, Bookmarks, Wikipedia search, a quick calculator -- you name it, the new application launcher provides it in an easy and appealing way. The central place in the Search and Launch page is the search bar where you can query your netbook and the Net just by typing the appropriate words; results are presented in a matter of seconds. When you find what you are looking for, you can mark it as a favorite and its icon will appear on the upper area below the panel, to permit easy and convenient access.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_plasma_netbook2.png">
	<img src="/announcements/4/4.4.0/thumbs/44_plasma_netbook2.png" class="img-fluid">
	</a> <br/>
	<em>Plasma Netbook's Search and Launch interface</em>
</div>
<br/>

<p>
You do not need to know the exact name of the resource or application you want to find since the search also looks at application descriptions and provides icons indicating different categories (applications, office, and so on), which can be used to browse the installed applications by category. A back button allows you to go back to the initial view.
</p>
<p>
Besides the Search and Launch interface, the Plasma Netbook Workspace provides a set of pages to display widgets, in a slightly different fashion from its Desktop counterpart. Since the netbook initiative has a strong focus on the Web, you will find widgets that are useful for a network-oriented activity: the weather widget, the RSS news reader widget, the openDesktop widget and the OpenDesktop Knowledge base. You can add as many widgets as you want, using the scrollbar to browse through them. You can also create new pages to organize the widgets in sets. Widgets are being developed that show contacts, emails and more social media information. Plasma Netbook widgets are grouped in columns using a scrollbar to optimize the use of available space.
</p>
<p>
The Plasma Netbook Workspace is mainly the work of Marco Martin, with important contributions from Artur Souza, Aaron Seigo and the rest of the Plasma team.
</p>

<h3>Changes to workspace tools</h3>

<p>
John Layt modified the configuration tool System Settings to support new locales, currencies and date formats, including a variety of new calendars like Indian National, Ethiopian, Coptic, and Julian. He didn't stop there - John extended this support to the Plasma Calendar widget which can now show multiple holiday regions so you can track the parties around the world. Font selection was improved by Craig Drummond and Christoph Feck, who also worked on the icon and time configuration screens. Ben Cooksley worked on the layout of System Settings with the KDE usability team and implemented the changes, making it easier to find the configuration setting you are looking for. Fredrik Höglund added <a href="http://who-t.blogspot.com/search/label/xi2">XInput2</a> support to the mouse configuration and  Martin Gräßlin, while working on the improvements in KWin, changed the Virtual Desktop configuration to incorporate the new KWin features.
</p>
<p>
Trever Fischer has written a configuration module to handle automatic mounting of removable drives. Through its simple interface, a user can quickly set the removable media that can be automatically mounted, as well as automatic mounting conditions. The user can also remove devices already flagged for automounting.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_krunner.png">
	<img src="/announcements/4/4.4.0/thumbs/44_krunner.png" class="img-fluid">
	</a> <br/>
	<em>KRunner showing different kind of options for a search term</em>
</div>
<br/>

<p>
Jacopo De Simoi developed a few new runners for KRunner, the quick launcher accessible by ALT-F2. Devices can now show up in the search. You can manage actions in the default interface, and there is a single runner mode to have only one runner respond to queries. Martin Gräßlin integrated window management functionality in KRunner. Jan Gerrit Marker added support for Firefox bookmarks so you can simply type (part of) a bookmark from Firefox and open it.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kmix_OSD.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kmix_OSD.png" class="img-fluid">
	</a> <br/>
	<em>KMix's on-screen display reacts to your multimedia buttons</em>
</div>
<br/>

<p>
Christian Esken updated several features in KMix. You can now organize and understand the KMix audio controls better by customizing the order of the sound controls. The new descriptive "What's this?" tooltips help explain some of the controls available for your sound card. KMix also now has improved multimedia keyboard support. If an application takes away the multimedia volume keys, KMix will retrieve them automatically the next time you start it. Users can disable the multimedia volume keys through the KMix GUI if they wish. Thanks to the work by Colin Guthrie, basic PulseAudio integration is available in KMix.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kmix.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kmix.png" class="img-fluid">
	</a> <br/>
	<em>KMix is the mixer to control your audio</em>
</div>
<br/>

<h3>There's more...</h3>
<p>
These are only some of the new features you find in KDE Plasma 4.4. The best way to see this all in action, and to discover how Plasma can facilitate your personal workflow is to experience it yourself. So make sure you give Plasma Desktop or Plasma Netbook a whirl!
</p>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../guide">
                <img src="/announcements/4/4.4.0/images/star-32.png" />
                Previous Page: Overview
                </a>
        </td>
        <td align="right" width="50%">
                <a href="../applications">Next page: Basic applications
                <img src="/announcements/4/4.4.0/images/applications-32.png" />
		</a>
        </td>
    </tr>
</table>
