---
title: KDE Platform 4.10 Opens Up More APIs to Qt Quick
date: "2013-02-06"
hidden: true
---

## Plasma SDK

This release of the KDE Development Platform sees more work on a
comprehensive SDK for Plasma. Previously separate and distinct
components, such as plasmoidviewer, plasamengineexplorer and
plasmawallpaperviewer, are now part of <a href='http://techbase.kde.org/Projects/Plasma/PlasMate'>PlasMate</a>,
the toolset for developing Plasma widgets.

{{<figure src="/announcements/4/4.10.0/plasmate.png" caption="Plasmate forms the heart of the Plasma SDK"width="600px" >}}

## Qt Quick Support

The use of <a href='http://doc.qt.digia.com/qt/qtquick.html'>Qt
Quick</a> within Plasma continues to expand. Many components have been
updated to use Qt Quick exclusively for the user interface; this also
makes it easy to extend and customize Plasma Workspaces. Plasma now
also allows Containments (which are responsible for presenting widgets
on the desktop and in panels) to be written using only Qt Quick's
easy-to-learn language. This gives developers the ability to produce
custom Containments for experimentation or special use cases. With
this capability, Plasma is a valuable, universal user interface
toolkit.

## Scripting Desktop Effects

Scripting interfaces for window effects, behavior and management make
KWin Window Management a useful tool for developers wanting to address
a particular use case. In addition, this modular approach minimizes
the size of the core of KWin. It also improves maintainability by
moving specialized code into external scripts Compared to the C++ code
they replace, scripts make it easier to write, maintain and ensure
quality of code.

#### Installing the KDE Development Platform

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

{{% i18n_var "KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/%[1]s'>http://download.kde.org</a> and can also be obtained on <a href='http://www.kde.org/download/cdrom.php'>CD-ROM</a> or with any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today." "4.10.0" %}}

##### Packages

{{% i18n_var "Some Linux/UNIX OS vendors have kindly provided binary packages of %[1]s for some versions of their distribution, and in other cases community volunteers have done so." "4.10.0"%}}

#### Package Locations

{{% i18n_var "For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_%[1]s'>Community Wiki</a>." "4.10.0"%}}

{{% i18n_var "The complete source code for %[1]s may be <a href='/info/%[1]s.php'>freely downloaded</a>. Instructions on compiling and installing KDE software %[1]s are available from the <a href='/info/%[1]s.php#binary'>%[1]s Info Page</a>." "4.10.0"%}}

#### System Requirements

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Also Announced Today:

## <a href="./plasma"><img src="/announcements/4/4.10.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.10" width="64" height="64" /> Plasma Workspaces 4.10 Improve Mobile Device Support and Receive Visual Refinement</a>

Several components of Plasma Workspaces have been ported to Qt Quick/QML framework. Stability and usability have been improved. A new print manager and Color Management support have been introduced.

## <a href="./applications"><img src="/announcements/4/4.10.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.10"/> KDE Applications Improve Usability, Performance and Take You to Mars</a>

KDE Applications gained feature enhancements to Kate, KMail and Konsole. KDE-Edu applications saw a complete overhaul of KTouch and many other changes. KDE Games introduced the new Picmi game and improvements throughout.

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
