---
title: "KDE platvorm: täiustatud multimeedia- ja semantilised võimalused"
date: "2011-07-27"
hidden: true
---


<p>
KDE annab uhkelt teada KDE platvormi 4.7 väljalaskmisest. Plasma töötsoonide ja KDE rakenduste alus on saanud paljudes valdkondades olulisi täiendusi. Lisaks uue tehnoloogia juurutamisele on nähtud palju vaeva alusstruktuuri jõudluse ja stabiilsuse parandamisel.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/systemsettings.png">
	<img src="/announcements/4/4.7.0/thumbs/systemsettings.png" class="img-fluid" alt="KDE Platform 4.7">
	</a> <br/>
	<em>KDE Platform 4.7</em>
</div>
<br/>

<p>
Meie meediaraamistiku Phonon uusim versioon pakub hulga uusi võimalusi, näiteks Zeitgeisti toetus. Samuti on vaeva nähtud taustaprogrammide kallal: VLC-põhist taustaprogrammi võib nüüd pidada stabiilseks ning see on eelistatud taustaprogramm mitmeplatvormses keskkonnas, GStreamerile põhinevat taustaprogrammi aga võib lugeda stabiilseks Linuxi platvormil. Xine taustaprogrammi enam ei arendata.
</p><p>
KDE platvormi semantilise töölaua komponentide funktsionaalsus ja stabiilsus on paranenud. Nepomuk elas üle võimsaid sisemisi muutusi, mille taga oli soov muuta see stabiilsemaks ja kiiremaks ning pakkuda rakendustele rikkalikumat API-t. Strigi analüsaatorid loevad nüüd metaandmeid omaette protsessis, millega leidis lahenduse üle 35 krahhiga seotud Dolphini ja Konquerori veateate.
</p><p>
KWin pakub nüüd rakenduste arendajatele võimalust lasta komposiit peatada, kui seda soovib täisekraanirežiimis rakendus. See peaks parandama OpenGL mängude ja GPU kiirendatud videotaasesituse jõudlust.
</p><p>
Kuvahaldur <strong>KDM</strong> toetab nüüd <a href="http://ksmanis.wordpress.com/2011/04/21/hello-planet-and-grub2-support-for-kdm/">GRUB2</a>. Kasutajad, kelle GRUB2 menüüs leidub mitmeid operatsioonisüsteeme, saavad nüüd seiskamisdialoogis valida, millises neist alglaadimine sooritada, hoides taaskäivitamise valikule klõpsates hiirenuppu pikemalt all.

<div class="text-center">
	<a href="/announcements/4/4.7.0/kdm.png">
	<img src="/announcements/4/4.7.0/thumbs/kdm.png" class="img-fluid" alt="KDM supports the GRUB2 boot manager">
	</a> <br/>
	<em>KDM toetab GRUB2 alglaadurit</em>
</div>
<br/>


</p><p>
KDE süsteemset puhverserverit toetav <strong>KIO Proxy</strong> on saanud rohkelt parandusi ja uusi omadusi, sealhulgas on toetatud SOCKS-puhverserverid, võimalik on hankida mitme puhverserveri URL-aadress korraga ning toetatud on ka süsteemse puhverserveri teabe hankimine Windowsi ja Maci platvormil.
</p><p>
KwebkitPart, mis aitab lõimida WebKiti KDE rakendustesse, on saanud täiustusi reklaamide blokeerimise osas, mis muudab veebilehitsemise KDE WebKiti brauseriga veelgi meeldivamaks kogemuseks.
</p>



<h4>KDE arendusplatvormi paigaldamine</h4>
<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral, operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.7.0/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.7.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.7.0/">http://download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4.7.0">4.7infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.7.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.7.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.7.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4.7.0#binary">4.7.0infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.4. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Täna ilmusid veel:</h2>

<h3>
<a href="./plasma">
Plasma töötsoonid: KWin aitab neid kõikjal kasutada
</a>
</h3>

<p>
<a href="./plasma">
<img src="/announcements/4/4.7.0/images/plasma.png" class="app-icon float-left m-3" alt="KDE Plasma töötsoonid 4.7" />
</a>

Plasma töötsoonidele on kasuks tulnud ulatuslik arendustegevus KDE komposiit-aknahalduri KWin kallal ning selliste uute Qt tehnoloogiate nagu Qt Quick kasutamise võimalus. Täpsemalt kõneleb kõigest <a href="./plasma">Plasma töölaua ja Plasma Netbook 4.7 väljalasketeade</a>.
<br />

</p>

<h3>
<a href="../applications">
Uuendatud KDE rakendused pakuvad rohkelt vaimustavaid omadusi
</a>
</h3>

<p>
<a href="../applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="KDE rakendused 4.7"/>
</a>

Paljud KDE rakendused läbisid uuenduskuuri. Eriti tasub välja tuua KDE grupitöölahenduse Kontact taasühinemist KDE reeglipärase väljalasketsükliga pärast seda, kui kõik selle tähtsamad komponendid porditi Akonadi peale. Samal ajal näeb ilmavalgust KDE võimalusterohke fotohalduri Digikam uus väljalase. Täpsemalt kõneleb kõigest <a href="../applications">KDE rakenduste 4.7 väljalasketeade</a>.
<br />

</p>