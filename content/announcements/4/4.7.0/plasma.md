---
title: Plasma Workspaces become more portable thanks to KWin
date: "2011-07-27"
hidden: true
---

<p>
KDE is happy to announce the immediate availability of version 4.7 of both the Plasma Desktop and Plasma Netbook Workspaces. The Plasma Workspaces have seen improvements to existing functionality, as well as the introduction of significant new features. In particular, these include new interface design methods better suited to touchscreen and mobile devices.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/plasma.png">
	<img src="/announcements/4/4.7.0/thumbs/plasma.png" class="img-fluid" alt="Plasma Desktop 4.7">
	</a> <br/>
	<em>Plasma Desktop 4.7</em>
</div>
<br/>

<p>
Lots of visual polishing took place with an update to the <strong>Oxygen icons</strong>, and improved consistency between panel items such as clock and notification areas. Recognizing the modular nature of KDE software and the ability to mix and match applications from many different sources, KDE has also improved the Oxygen GTK themes, making applications from GNOME (and other applications using GTK+) blend seamlessly with KDE applications in your Plasma Workspace.

<div class="text-center">
	<a href="/announcements/4/4.7.0/oxygen-icons.png">
	<img src="/announcements/4/4.7.0/thumbs/oxygen-icons.png" class="img-fluid" alt="Oxygen Icons have been touched up">
	</a> <br/>
	<em>Oxygen Icons have been touched up</em>
</div>
<br/>

</p><p>
The Plasma Workspaces window manager, KWin, has received extensive cleanup of its code and can now run on <strong>OpenGL ES</strong> supporting hardware, making it better suited for mobile devices, and also improvements for desktop users. KWin has a new shadow system, improving support for users of older hardware or hardware for which driver support for OpenGL is limited  (Xrender backend). KWin's performance improves noticably thanks to numerous optimizations in certain painting operations.
</p><p>
Plasma's Activities have seen many improvements, the Activity Manager now takes a more prominent place in the default panel in Plasma Desktop. Activities enhance the users' workflows by providing smart ways of grouping applications, widgets and documents.

<div class="text-center">
	<a href="/announcements/4/4.7.0/plasma-activities.png">
	<img src="/announcements/4/4.7.0/thumbs/plasma-activities.png" class="img-fluid" alt="Plasma's Activities take a more prominent role in 4.7">
	</a> <br/>
	<em>Plasma's Activities take a more prominent role in 4.7</em>
</div>
<br/>

</p><p>
The Plasma Workspaces now also offer much improved network management, including experimental support for NetworkManager 0.9 as well as Bluetooth tethering, 3G, VPN, MAC spoofing and other advanced networking options.

<div class="text-center">
	<a href="/announcements/4/4.7.0/plasma-networkmanagement.png">
	<img src="/announcements/4/4.7.0/thumbs/plasma-networkmanagement.png" class="img-fluid" alt="Network Management in Plasma 4.7">
	</a> <br/>
	<em>Network Management in Plasma 4.7</em>
</div>
<br/>

</p><p>
Navigating through applications and recent files is easier with the addition of breadcrumbs to the Kickoff application launcher, helping users to see where they are and quickly back up to higher menu levels.
Many other usability and functionality improvements have been made to the Workspaces. For example, Konsole no longer blocks the removal of USB storage devices and KMix has improved PulseAudio support.

<div class="text-center">
	<a href="/announcements/4/4.7.0/kickoff.png">
	<img src="/announcements/4/4.7.0/thumbs/kickoff.png" class="img-fluid" alt="Kickoff's new breadcrumbs ease navigation">
	</a> <br/>
	<em>Kickoff's new breadcrumbs ease navigation</em>
</div>
<br/>

</p>

<h4>Installing Plasma</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.7.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.7.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.7.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.7.0">4.7 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.7.0 may be <a href="/info/4.7.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.7.0
  are available from the <a href="/info/4.7.0#binary">4.7.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.7.4. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Announced Today:</h2>

<h3>
<a href="../applications">
Updated KDE Applications Bring Many Exciting Features
</a>
</h3>

<p>
<a href="../applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.7"/>
</a>
Many KDE applications are updated. In particular, KDE's groupware solution, Kontact, rejoins the main KDE release cycle with all major components ported to Akonadi. The Digikam Software Collection, KDE's feature-rich photo management tools, come with a major new version. For full details, read the <a href="../applications">KDE Applications 4.7 release announcement</a>.
<br />
</p>

<h3>
<a href="../platform">
Improved Multimedia, Instant Messaging and Semantic Capabilities in the KDE Platform
</a>
</h3>

<p>
<a href="../platform">
<img src="/announcements/4/4.7.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.7"/>
</a>

A wide range of KDE and third party software will be able to take advantage of extensive work in Phonon and major improvements to the semantic desktop components, with enriched APIs and improved stability. The new KDE Telepathy framework offers integration of instant messaging directly into workspaces and applications. Performance and stability improvements in nearly all components lead to a smoother user experience and a reduced footprint of applications using the KDE Platform 4.7. For full details, read the <a href="../platform">KDE Platform 4.7 release announcement</a>.
<br />

</p>
