---
title: "Plasma töötsoonid: KWin aitab neid kõikjal kasutada"
date: "2011-07-27"
hidden: true
---

<p>
KDE teatab rõõmuga nii Plasma töölaua kui ka Plasma Netbooki töötsooni versiooni 4.7 väljalaskmisest. Plasma töötsoonide senist funktsionaalsust on tublisti parandatud ning lisandunud on ka mitmeid märkimisväärseid võimalusi. Viimaste seast tasub esile tuua uusi liidese loomise viise, mis sobivad paremini puuteekraaniga ja mobiilsetele seadmetele.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/plasma.png">
	<img src="/announcements/4/4.7.0/thumbs/plasma.png" class="img-fluid" alt="Plasma töölaud 4.7">
	</a> <br/>
	<em>Plasma töölaud 4.7</em>
</div>
<br/>

<p>
Rohke visuaalne viimistlus avaldub <strong>Oxygeni ikoonide</strong> uuendustes ning tunduvalt on ühtlustunud ka paneeli elementide, näiteks kella ja märguanneteala, väljanägemine. KDE tarkvara modulaarse iseloomu ning võime tõttu kombineerida ja ühendada paljudest eri allikatest pärit rakendusi on KDE täiustanud ka Oxygeni GTK teemasid, mis lubab GNOME rakendustel (ja teistel GTK+ kasutavatel rakendustel) sujuvalt sulanduda Plasma töötsooni KDE rakendustega.

<div class="text-center">
	<a href="/announcements/4/4.7.0/oxygen-icons.png">
	<img src="/announcements/4/4.7.0/thumbs/oxygen-icons.png" class="img-fluid" alt="Oxygeni ikoonid elasid üle viimistluskuuri">
	</a> <br/>
	<em>Oxygeni ikoonid elasid üle viimistluskuuri</em>
</div>
<br/>

</p><p>
Plasma töötsoonide aknahalduri KWin koodi on ulatuslikult puhastatud ning see suudab nüüd töötada <strong>OpenGL ES</strong> toetusega riistvaral, mis muudab aknahalduri senisest sobivamaks mobiilsetele seadmetele, aga pakub paremat elamust ka lauaarvutite kasutajatele. KWin kasutab uut varjutussüsteemi, mis aitab paremini toetada kasutajaid, kel on vanem riistvara või selline riistvara, mille draiveri OpenGL toetus on piiratud (Xrenderi taustaprogramm). KWini jõudlus on tänu rohkele optimeerimisele märgatavalt paranenud.
</p><p>
Ohtralt täiustusi on saanud ka Plasma tegevused. Tegevustehalduril on nüüd märksa silmatorkavam asukoht Plasma töölaua vaikimisi paneelil. Tegevused aitavad edendada kasutajate töövoogu, pakkudes nutikaid võimalusi rakendusi, vidinaid ja dokumente rühmitada.

<div class="text-center">
	<a href="/announcements/4/4.7.0/plasma-activities.png">
	<img src="/announcements/4/4.7.0/thumbs/plasma-activities.png" class="img-fluid" alt="Plasma tegevustel on 4.7-s silmatorkavam koht">
	</a> <br/>
	<em>Plasma tegevustel on 4.7-s silmatorkavam koht</em>
</div>
<br/>

</p><p>
Samuti pakuvad Plasma töötsoonid nüüd palju etemaid võrguhalduse võimalusi, sealhulgas on eksperimentaalselt toetatud nii NetworkManager 0.9 kui ka interneti kasutamine üle Bluetoothi, 3G, VPN, MAC-i muutmine ja veel mitmed peened võrguhalduse võimalused.

<div class="text-center">
	<a href="/announcements/4/4.7.0/plasma-networkmanagement.png">
	<img src="/announcements/4/4.7.0/thumbs/plasma-networkmanagement.png" class="img-fluid" alt="Plasma 4.7 võrguhaldus">
	</a> <br/>
	<em>Plasma 4.7 võrguhaldus</em>
</div>
<br/>

</p><p>
Liikumine rakenduste ja viimati kasutatud failide vahel on nüüd lihtsam tänu rakenduste käivitajale Kickoff lisatud asukohanäitajale, mis aitab kasutajal täpselt aru saada, kus ta parajasti viibib, ning kiiresti tagasi jõuda menüü kõrgematesse tasanditesse.
Töötsoonid on saanud veel palju muid kasutamist lihtsustavaid ja funktsionaalsust täiendavaid uuendusi. Nii näiteks ei blokeeri Konsool enam USB-pulkade eemaldamist ning KMix toetab tunduvalt paremini PulseAudiot.

<div class="text-center">
	<a href="/announcements/4/4.7.0/kickoff.png">
	<img src="/announcements/4/4.7.0/thumbs/kickoff.png" class="img-fluid" alt="Kickoffi uus asukohanäitaja lihtsustab liikumist">
	</a> <br/>
	<em>Kickoffi uus asukohanäitaja lihtsustab liikumist</em>
</div>
<br/>

</p>

<h4>Plasma paigaldamine</h4>
<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral, operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.7.0/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.7.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.7.0/">http://download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4.7.0">4.7infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.7.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.7.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.7.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4.7.0#binary">4.7.0infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.4. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Täna ilmusid veel:</h2>

<h3>
<a href="../applications">
Uuendatud KDE rakendused pakuvad rohkelt vaimustavaid omadusi
</a>
</h3>

<p>
<a href="../applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="KDE rakendused 4.7"/>
</a>

Paljud KDE rakendused läbisid uuenduskuuri. Eriti tasub välja tuua KDE grupitöölahenduse Kontact taasühinemist KDE reeglipärase väljalasketsükliga pärast seda, kui kõik selle tähtsamad komponendid porditi Akonadi peale. Samal ajal näeb ilmavalgust KDE võimalusterohke fotohalduri Digikam uus väljalase. Täpsemalt kõneleb kõigest <a href="../applications">KDE rakenduste 4.7 väljalasketeade</a>.
<br />

</p>

<h3>
<a href="../platform">
KDE platvorm: täiustatud multimeedia-, kiirsuhtlus- ja semantilised võimalused
</a>
</h3>

<p>
<a href="../platform">
<img src="/announcements/4/4.7.0/images/platform.png" class="app-icon float-left m-3" alt="KDE arendusplatvorm 4.7.0"/>
</a>

Suur hulk KDE ja kolmanda poole tarkvara saab tulu ulatuslikust arendustööst Phononi kallal ning olulistest täiustustest semantilise töölaua komponentide seas, mille API on tublisti täienenud ning stabiilsus suurenenud. Uus KDE Telepathy raamistik võimaldab lõimida kiirsuhtluse otse töötsoonidesse ja rakendustesse. Jõudluse ja stabiilsuse paranemine peaaegu kõigis komponentides tagab etema kasutajakogemuse ning vähendab KDE platvormi 4.7 kasutavate rakenduste koormust süsteemile. Täpsemalt kõneleb kõigest <a href="../platform">KDE platvorm 4.7 väljalasketeade</a>.
<br />

</p>
