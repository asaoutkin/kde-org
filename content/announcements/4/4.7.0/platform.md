---
title: Improved Multimedia and Semantic Capabilities in KDE Platform
date: "2011-07-27"
hidden: true
---

<p>
KDE proudly announces the release of KDE Platform 4.7. The foundation for the Plasma Workspaces and the KDE Applications has seen significant improvements in many areas. Besides the introduction of new technology, a lot of work has been done on improving the performance and stability of the underlying structure.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/systemsettings.png">
	<img src="/announcements/4/4.7.0/thumbs/systemsettings.png" class="img-fluid" alt="KDE Platform 4.7">
	</a> <br/>
	<em>KDE Platform 4.7</em>
</div>
<br/>

<p>
The latest version of Phonon, our media framework, now comes with many new features such as Zeitgeist support. We have also worked on back-ends: The VLC-based back-end is now considered stable and is the preferred back-end for multiplatform use, while the back-end based on GStreamer is now also considered stable on Linux platforms. The xine back-end is no longer maintained.
</p><p>
The semantic desktop components of the KDE Frameworks have improved functionality and stability. Nepomuk has undergone massive internal changes making it more stable and faster, with richer APIs for applications. Strigi analyzers now read meta-data in their own process, resolving over 35 crash-related bugs in Dolphin and Konqueror.
</p><p>
KWin now features an option for application developers to suspend compositing when a fullscreen application calls for it. This should increase performance for OpenGL games and GPU-accelerated video playback.
</p><p>
<strong>KDM</strong>, the display manager, has gained <a href="http://ksmanis.wordpress.com/2011/04/21/hello-planet-and-grub2-support-for-kdm/">GRUB2 support</a>. A user with multiple operating systems in their GRUB2 menu can now choose which one to reboot to in the shutdown dialog by holding down the mouse button on their reboot choice.

<div class="text-center">
	<a href="/announcements/4/4.7.0/kdm.png">
	<img src="/announcements/4/4.7.0/thumbs/kdm.png" class="img-fluid" alt="KDM supports the GRUB2 boot manager">
	</a> <br/>
	<em>KDM supports the GRUB2 boot manager</em>
</div>
<br/>

</p><p>
<strong>KIO Proxy</strong>, KDE's system-wide proxy support continues to be overhauled with fixes and features including SOCKS proxy support, functions for obtaining multiple proxy URL addresses and support for obtaining system proxy information on the Windows and Mac platforms.
</p><p>
KwebkitPart, integrating WebKit into KDE applications, has improved ad-blocking support, making browsing with a KDE WebKit browser a more pleasant experience.
</p>

<h4>Installing the KDE Development Platform</h4>

<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.7.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.7.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.7.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.7.0">4.7 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.7.0 may be <a href="/info/4.7.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.7.0
  are available from the <a href="/info/4.7.0#binary">4.7.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.7.4. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Announced Today:</h2>

<h3>
<a href="../plasma">
Plasma Workspaces Become More Portable Thanks to KWin
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/4/4.7.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.7" />
</a>

The Plasma Workspaces gain from extensive work to KDE’s compositing window manager, KWin, and from the leveraging of new Qt technologies such as Qt Quick. For full details, read the <a href="../plasma">Plasma Desktop and Plasma Netbook 4.7 release announcement</a>.
<br />
<br/>

</p>

<h3>
<a href="../applications">
Updated KDE Applications Bring Many Exciting Features
</a>
</h3>

<p>
<a href="../applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.7"/>
</a>
Many KDE applications are updated. In particular, KDE's groupware solution, Kontact, rejoins the main KDE release cycle with all major components ported to Akonadi. The Digikam Software Collection, KDE's feature-rich photo management tools, come with a major new version. For full details, read the <a href="../applications">KDE Applications 4.7 release announcement</a>.
<br />
</p>
