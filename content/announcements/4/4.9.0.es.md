---
aliases:
- ../4.9
date: '2012-12-05'
title: KDE 4.9 – en memoria de Claire Lotion
---

<p>
<img src="/stuff/clipart/klogo-official-oxygen-128x128.png" class="float-left m-2" />KDE tiene el gusto de anunciar su último conjunto de lanzamientos, que proporciona actualizaciones mayores para los <a href="./plasma">Espacios de trabajo Plasma de KDE</a>, las <a href="./applications">Aplicaciones de KDE</a> y la <a href="./platform">Plataforma de KDE</a>. La versión 4.9 proporciona muchas funciones nuevas, además de una estabilidad y rendimiento mejorados.
</p>
<p>
Este lanzamiento está dedicado a la memoria de la colaboradora de KDE <a href="http://dot.kde.org/2012/05/20/remembering-claire-lotion">Claire Lotion</a>. La personalidad y el entusiasmo vibrantes de Claire han sido una fuente de inspiración para muchos miembros de nuestra comunidad, y su trabajo pionero en el formato, alcance y frecuencia de nuestros encuentros de desarrolladores ha cambiado el modo en el que llevamos a cabo nuestro trabajo en la actualidad. A través de esta y de otras actividades, dejó una marca notable en el software que hoy podemos proporcionarle a usted, por lo que le estamos profundamente agradecidos.</p>
<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-desktop.png">
	<img src="/announcements/4/4.9.0/kde49-desktop-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>
<p>
El Equipo de Control de Calidad de KDE se formó a principios de este año con el objetivo de mejorar los niveles generales de calidad y estabilidad del software de KDE. Se ha puesto especial atención en identificar y solucionar las regresiones de lanzamientos anteriores. Esta ha sido una de las principales prioridades, ya que asegura que cada lanzamiento es mejor que el anterior.
</p>
<p>
Este equipo tambiénha fijado un proceso de prueba más riguroso para los lanzamientos que comienza con versiones beta. Los nuevos voluntarios que prueban el software han recibido entrenamiento y se han llevado a cabo varios días de pruebas intensivas. En lugar de realizar las tradicionales pruebas exploratorias, se han asignado probadores para que se centren en áreas específicas que han cambiado desde los lanzamientos anteriores. Para algunos componentes críticos se han creado y usado completas listas de comprobaciones. El equipo ha encontrado muchos problemas importantes a tiempo y ha trabajado con los desarrolladores para asegurar su corrección. Este mismo equipo ha notificado alrededor de 160 problemas en las versiones betas y candidatas, muchos de los cuales ya están solucionados. Otros usuarios de las versiones betas y candidatas han incrementado considerablemente el número de fallos informados. Estos esfuerzos son importantes porque permiten que los desarrolladores se concentren en solucionar los 
problemas.
</p>
<p>
Como resultado de los esfuerzos del Equipo de Control de Calidad de KDE, la versión 4.9 es la mejor de todas.
</p>
<p>
La solución de un error particular merece especial atención. Se trata de un problema de Okular notificado en 2007 y que ha obtenido cerca de 1.100 votos: era importante para muchos usuarios. Estos se quejaban de que escribían notas y no podían guardarlas ni imprimirlas. Con la asistencia de muchos comentarios y personas en el canal de IRC de Okular, Fabio D'Urso implementó una solución que permite a Okular guardar e impirmir las notas hechas en documentos PDF. La solucion ha necesitado trabajo en las bibliotecas de KDE y atención al diseño general para asegurar que los documentos distintos a PDF funcionaban bien. Esta ha sido la primera experiencia como desarrollador de Fabio, que llegó cuando encontró el problema y decidió hacer algo para solucionarlo.
</p>
<h2><a href="./plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Espacios de trabajo Plasma 4.9 – Mejoras globales</a></h2>
<p>
Lo más destacado de los espacios de trabajo Plasma incluye mejoras sustanciales en el gestor de archivos Dolphin, el emulador de terminal de X Konsole, las Actividades y el gestor de ventanas KWin. Lea el <a href="./plasma">«Anuncio de los espacios de trabajo Plasma»</a>.
</p>
<h2><a href="./applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Aplicaciones de KDE 4.9 nuevas y mejoradas</a></h2>
<p>
Hoy se han liberado aplicaciones de KDE nuevas y mejoradas que incluyen Okular, Kopete, KDE PIM y aplicaciones y juegos educativos. Lea el <a href="./applications">«Anuncio de aplicaciones de KDE»</a>.
</p>
<h2><a href="./platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>Plataforma de KDE 4.9</a></h2>
<p>
El lanzamiento de la plataforma de KDE de hoy incluye corrección de errores, otras mejoras de calidad, redes y preparación para Frameworks 5.
</p>
<h2>Corra la voz y vea qué está ocurriendo: etiquete como «KDE»</h2>
<p>
KDE anima a todo el mundo a correr la voz en la Web Social. Envíe artículos a los sitios de noticias, use canales como delicious, digg, reddit, twitter o identi.ca. Suba capturas de pantallas a servicios como Facebook, Flickr, ipernity o Picasa, y publíquelas en los grupos apropiados. Cree vídeos y súbalos a YouTube, Blip.tv o Vimeo. Tenga la amabilidad de etiquetar las publicaciones y el material enviado con la palabra «KDE». Esto hace que sea más fácil de localizar y le proporciona al Equipo de Promoción de KDE un modo de analizar la cobertura del lanzamiento del software KDE 4.9.
</p>
<p>
Siga lo que está ocurriendo en las fuentes de datos en vivo de KDE. Este sitio agrega actividad en tiempo real a identi.ca, twitter, youtube, flickr, picasaweb, blogs y otros sitios de redes sociales. Puede encontrar la fuente de datos en <a href="http://buzz.kde.org">buzz.kde.org</a>.
</p>
<h2>Fiestas de lanzamiento</h2>
<p>
Como viene siendo habitual, los miembros de la comunidad de KDE organizan fiestas de lanzamiento por todo el mundo. Algunas ya han sido programadas y otras se celebrarán en el futuro. Encuentre <a href="http://community.kde.org/Promo/Events/Release_Parties/4.9">aquí la lista de las fiestas</a>. ¡Todo el mundo está invitado a participar! En ellas existirá una combinación de compañía interesante y conversaciones inspiradoras, así como comida y bebida. Es una buena oportunidad de aprender más sobre lo que está ocurriendo en KDE, de introducirse o de conocer a otros usuarios y colaboradores.
</p>
<p>
Animamos a todos a organizar sus propias fiestas. ¡Son divertidas de organizar y están abiertas a todo el mundo! Consulte los <a href="http://community.kde.org/Promo/Events/Release_Parties">consejos sobre cómo organizar una fiesta</a>.
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.9/&amp;title=KDE%20releases%20version%204.9%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.9/" data-text="#KDE releases version 4.9 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.9/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.9%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.9/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde49"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde49"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde49"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde49"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2>Sobre estos anuncios de lanzamiento</h2>
<p>
Estos anuncios de lanzamiento han sido preparados por Jos Poortvliet, Carl Symons, Valorie Zimmerman, Jure Repinc, David Edmundson y otros miembros del Equipo de Promoción de KDE, así como la comunidad de KDE. Cubren lo más destacado de los numerosos cambios realizados en el software de KDE durante los últimos seis meses.
</p>

<h4>Apoyo de KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify"> El nuevo <a
href="http://jointhegame.kde.org/">programa de apoyo de miembros</a> de KDE e.V. está
ahora abierto.  Por 25&euro; cada tres meses puede asegurar que la comunidad internacional
de KDE continúa creciendo para hacer Software Libre de calidad mundial.</p>

<p>&nbsp;</p>