---
aliases:
- ../4.1
- ../4.1.mr
date: '2008-07-29'
title: KDE 4.1 Release Announcement
---

<h3 align="center">
KDE प्रकल्प KDE 4.1.0 चे प्रकाशन करीत आहे
</h3>

<p align="justify">
  <strong>
    KDE ने Uwe Thiem समर्पीत डेस्कटॉप व अनुप्रयोग यांचा विकास केला आहे
  </strong>
</p>

<p align="justify">
<a href="http://www.kde.org/">KDE प्रकल्प</a> ने आज KDE 4.1.0 चे प्रकाशन केले. हे प्रकाशन
KDE 4 श्रृंखला अंतर्गत दुसरे विशेष प्रकाशन आहे, ज्यात नविन अनुप्रयोग व KDE4 मधिल नाविन्य तऱ्हेने विकसीत गुणविशेष समाविष्ठीत आहे. KDE 4.1 हे KDE4 चे प्रथम प्रकाशन आहे ज्यात व्यक्तिगत माहिती व्यवस्थापक संकुल KDE-PIM सह इ-मेल क्लाऐंट KMail, मदत सहाय्यक KOrganizer, Akregator, RSS फीड वाचक, KNode, समाचारगट वाचक व Kontact शेल मध्ये एकत्रीत केलेले अनेक विभाग समाविष्ठीत आहे. याच्या व्यतिरीक्त, KDE 4.0 मधिल, नविन डेस्कटॉप शेल, आता साधारण वापरकर्त्यांकरीता KDE 3 शेलशी बदलविण्याजोगी स्थितीपर्यंत विकसीत केले गेले आहे. पूर्वीच्या प्रकाशन प्रमाणेच यावेळी सुद्धा अधिक वेळ फ्रेमवर्क व KDE लायब्ररी करीता दिला गेला आहे.
<br />
Dirk M&uuml;ller, KDE प्रकाशन व्यवस्थापक खालिल नुरूप आकडे प्रविष्ट करतात: <em>"KDE 4.0 पासून ते KDE 4.1 पर्यंत एकूण 20803 कमीट्स् 15432
भाषांतरन चेकइनसह केले गेले. जवळपास 35000 कमीट्स् कार्यरत शाखां मध्ये केले गेले,
काहिक KDE 4.1 सह एकत्र केले गेले, त्यामुळे त्यांस मोजले जात नाही."</em>
M&uuml;ller पुढे सांगतात की KDE च्या sysadmin गटाने KDE च्या SVN सर्वर वर 166 नविन खाते बनविले.

<div class="text-center">
	<a href="/announcements/4/4.1.0/desktop.png">
	<img src="/announcements/4/4.1.0/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The KDE 4.1 desktop</em>
</div>
<br/>

</p>
<p>
<strong>KDE 4.1 मधिल महत्वाची सुधारणा:</strong>
<ul>
    <li>KDE-PIM संकुलचे पुनरागमन</li>
    <li>Plasma आणखी सुधारीत</li>
    <li>बरेचशे नविन व विकसीत अनुप्रयोग व फ्रेमवर्क</li>
</ul>

</p>

<h3>
  <a id="changes">Uwe ThiemIn: च्या आठवणी करीता समर्पीत</a>
</h3>
<p align="justify">
KDE समूह हे प्रकाशन Uwe Thiem च्या आठवणीस समर्पीत केले आहे, जे KDE चे दिर्घकालीन सहभागी होते, त्यांचे निधन मूत्रपिंड वीकारामुळे झाले. Uwe यांच्या अपेक्षीत निधानामुळे इतर सहभाग्यांना दु:खद धक्का बसला आहे. Uwe ने जिवनाच्या शेवटपर्यंत प्रोग्रमींगच्या व्यतिरिक्त, KDE करीता मह्तवपूर्ण योगदान केले. Uwe यांनी वापरकर्त्यांना Free Software च्या तत्वांशी अगत करण्यास मोठी भूमिका भजावली. Uwe यांच्या अकस्मात निधानामुळे, KDE ने समुहातील अनमोल हीरा व खरा मित्र गमावला आहे. आम्ही त्यांचे परिवार व मित्र मंडळासह या दु:खद घटनेत सोबत आहोत.
</p>

<h3>
  <a id="changes">भूतकाळ, वर्तमान व भविष्य</a>
</h3>
<p align="justify">
जरी KDE 4.1 वापरकर्तांच्या वापर करीता पहिले प्रकाशन ठरले असले, तरी KDE 3.5 मधिल, काहिक गुणविशेष अजूनही लागू केले गेले नाही.
KDE समुह त्यावर कार्य करीत आहे व पुढिल प्रकाशन मध्ये उपलब्ध करून देण्यास प्रयत्नशील आहे. KDE 3.5 पासून प्रत्येक गुणविशेष कार्यरत केले जाईल अशी हमी नसल्यावरही, KDE 4.1 आधिपासून शक्तिशाली व गुणविशेष-युक्त कार्यरत वातावरण पुरविते.<br />
लक्षात ठेवा UI मधिल काहिक पर्याय ठराविक संदर्भ करीता स्थानांतरीत केले गेले आहे, त्यामुळे काही न आढळल्यास योग्यरित्या चौकशी करण्याची खात्री करा.<p />
KDE 4.1 KDE4 श्रृंखलातील व भविष्यातील विकास करीता एक म्हत्वाचे पाऊल आहे. KDE 4.2 चे प्रकाशन जानेवारी 2009 पर्यंत अपेक्षीत आहे.
</p>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kdepim-screenie.png">
	<img src="/announcements/4/4.1.0/kdepim-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KDE PIM is back</em>
</div>
<br/>

<h3>
  <a id="changes">सुधारणा</a>
</h3>
<p align="justify">
KDE 4.1 मध्ये नविन फ्रेमवर्क स्थपीत करतेवेळी, वापरकर्त्यांसाठी दृश्यास्पद भागांकडे जास्त महत्व दिले गेले आहे. KDE 4.1 मधिल सुधारणा करीता खालिल वाचा. अधिक माहिती
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">KDE 4.1 प्रकाशन हेतु</a>
पान व अधिक विस्तृत माहिती करीता <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">4.1 गुणविशेष आराखडा</a> पान पाहू शकता.
</p>

<h4>
  वापरकर्त्यांकरीता
</h4>
<p align="justify">

<ul>
    <li>
        <strong>KDE-PIM</strong> चे 4.1 मध्ये पुनरागमन, जे आपली व्यक्तिगत माहिती व संपर्क आवश्यक अनुप्रयोगात समाविष्ठीत करतो. KMail मेल क्लाऐंट, KOrganizer व्यवस्थापन घटक, Akregator RSS फीड वाचक
        व इतर आता KDE 4 मध्ये उपलब्ध केले गेले आहेत.
    </li>
    <li>
        <strong>Dragon प्लेयर</strong>, वापरणी करीता सोपे विडीओ प्लेयर
    </li>
    <li>
        <strong>Okteta</strong> नविन सुलभरित्या-एकत्रीत व गुणविशेष-युक्त हेक्ससंपादक
    </li>
    <li>
        <strong>Step</strong>, भौतिकशास्त्र इम्यूलेटर ज्यामुळे भौतिकशास्त्र शिकणे आता सुलभ व सोपे झाले आहे
    </li>
    <li>
        <strong>KSystemLog</strong>, प्रणालीवरील सर्व घडामोडींवर नियंत्रण ठेवण्यास मदत करतो
    </li>
    <li>
        <strong>नविन खेळ</strong> जसे की KDiamond (एक रचलेले क्लोन), Kollision, KBreakOut
        व Kubrick तुम्हाला कामातून विश्रांती पुरविते
    </li>
    <li>
        <strong>Lokalize</strong>,  KDE4 भाषांतरनकर्त्यांना स्वतःच्या भाषेत भाषांतरन करीता साधन उपलब्ध करून देते
        (50-भाषां करीता KDE4 समर्थन आधिच पुरविले गेले आहे)
    </li>
    <li>
        <strong>KSCD</strong>, डेस्कटॉप CD प्लेयरचे पुनरागमन
    </li>
</ul>

<a href="http://www.kde.org/users/faq">KDE4 वापरकर्ता FAQ</a> मधिल समाविष्ठीत सर्व प्रश्नांचे उत्तर देतो, व KDE4 विषयी अधिक माहिती पुरवितो.

</p>

<p align="justify">

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-screenie.png">
	<img src="/announcements/4/4.1.0/dolphin-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Dolphin's new selection mechanism</em>
</div>
<br/>

<ul>
    <li>
        <strong>Dolphin</strong>, KDE चे फाइल व्यवस्थापक मुख्य दृश्यात वृक्ष नुरूप प्रदर्शन, व टॅब समर्थन पुरवितो. नविन व एकदा-क्लिक निवड करीता अधिक स्थिर वापरकर्ता अनुभव पुरवितो, व येथे-प्रत बनवा आणि येथे-हलवा या संदर्भ क्रिया सोपे करतो. तसेच
 Konqueror हे Dolphin करीता एक विकल्प आहे, जे वरील गुणविशेषांचा फायदा घेण्यास सक्षम आहे. [<a href="#screenshots-dolphin">Dolphin स्क्रिनशॉट</a>]
    </li>
    <li>
        <strong>Konqueror</strong>, आधिपासूनच बंद चौकट व टॅब पुन्ह-उघडण्याकरीता KDE वेबब्राऊजर समर्थन पुरवितो, वेबपानातून सहजपणे स्क्रॉल करण्याची सुविधा सुद्धा पुरवितो.
    </li>
    <li>
        <strong>Gwenview</strong>, KDE च्या प्रतिमा प्रदर्शकास नविन पूर्णपडदा दर्शविण्याचे गुणविशेष प्राप्त झाले आहे, इतर चित्र पहाण्याकरीता थंबनेल
        पट्टी पुरविली जाते, व प्रतिमा श्रेणीकृत्त करण्याकरीता समर्थन पुरवितो. [<a href="#screenshots-gwenview">Gwenview स्क्रिनशॉट</a>]
    </li>
    <li>
        <strong>KRDC</strong>, KDE चे दूरस्थ डेस्कटॉप क्लाऐंट आता स्थानीय संजाळावर ZeroConf शिष्टाचारचा वापर करून दूरस्थ डेस्कटॉप शोधण्यास मदत करतो.
    </li>
    <li>
        <strong>Marble</strong>, KDE डेस्कटॉप आता <a
        href="http://www.openstreetmap.org/">OpenStreetMap</a> शी जोडण्यात आले आहे ज्यामुळे तुम्ही Free Maps च्या सहायाने रस्त्यावरील मार्ग शोधू शकता. [<a href="#screenshots-marble">Marble स्क्रिनशॉट</a>]
    </li>
    <li>
        <strong>KSysGuard</strong>, आता क्रिया नियंत्रण आधारीत निरीक्षण समर्थन किंवा कार्यरत अनुप्रयोगांस समर्थन पुरवितो ज्यामुळे वर्तमान स्थिती माहित करूण घेण्याकरीता अनुप्रयोगांना टर्मिनल पासून पुन्हा सुरू करण्याची आवश्यकता नाही.
    </li>
    <li>
        <strong>KWin</strong> चे चौकट व्यवस्थापक गुणविशेष आता आणखी स्थीर व विस्तारीत करण्यात आले आहे. नविन प्रभाव जसे की कवरस्वीच् चौकट स्वीचर व
        प्रचलीत "डगमगणारे चौकट" सुद्धा जोडले गेले आहे. [<a href="#screenshots-kwin">KWin
        स्क्रिनशॉट</a>]
    </li>
    <li>
        <strong>Plasma</strong> चौकट संयोजना पटल वाढविले गेले आहे. नविन पटल नियंत्रक पटल इच्छिक करण्याकरीता प्रत्यक्ष दृश्यास्पद प्रतिसाद पुरविते. तुम्ही पटलही जोडू शकता व त्यास पडद्याच्या विविध किणाऱ्यास हलवूही शकता. नविन संचयीकादृश्य ऍपलेट
        तुम्हाला डेस्कटॉप वरील फाइल संचयन करीता सहमती देतो (प्रणालीवर संचयीकाचे दृश्य पुरवितो. तुम्ही शून्य, एक किंवा त्यापेक्षा जास्त संचयीका दृश्य डेस्कटॉपवर पाहू शकता, व कार्यरत फाइल करीता सोपे व इच्छिक प्रवेशही पुरविले गेले आहे.
        [<a href="#screenshots-plasma">Plasma स्क्रिनशॉट</a>]
    </li>
</ul>
</p>

<h4>
  डेव्हलपर करीता
</h4>
<p align="justify">

<ul>
    <li>
         <strong>Akonadi</strong> PIM संचयन फ्रेमवर्क अनुप्रयोगसाठी इमेल व संपर्क माहिती उत्तम मार्ग द्वारे पुरवितो. <a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> माहिती शोध करीता समर्थन पुरवितो व अनुप्रयोगातील बदल स्वीकारण्यास सूचीत करतो.
    </li>
    <li>
        KDE अनुप्रयोग Python व Ruby मध्ये लिहीले जाऊ शकते. <strong>भाषा बाइंडींग</strong> अनुप्रयोग डेव्हलपर करीता स्थीर, परिपक्व व सज्ज <a href="http://techbase.kde.org/Development/Languages">मानले</a> जाते.
    </li>
    <li>
        <strong>Libksane</strong> नविन स्कॅनींग अनुप्रयोग Skanlite करीता सोपे प्रतिमा स्कॅनींग पुरविले गेले आहे.
    </li>
    <li>
        KMail व Kopete द्वारे वापरले जाणारे सहभागीय <strong>emoticons</strong> प्रणाली जोडण्यात आली आहे.
    </li>
    <li>
         GStreamer, QuickTime व DirectShow9 करीता नविन <strong>Phonon</strong> मल्टिमिडीया बॅकएन्ड पुरविले गेली आहे, व त्याचबरोबर KDE
         Windows व Mac OS वरील मल्टिमिडीया समर्थन वाढविला गेला आहे.
    </li>

</ul>
</p>

<h3>
  नविन प्लॅटफार्म
</h3>
<p align="justify">
<ul>
    <li>
        KDE चा <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a> करीता समर्थन नुकताच वाढला आहे. काहिक अनिर्धारीत त्रुटी उर्वरीत असल्यामुळे, KDE सहसा OSOL वर चालविले जाते.
    </li>
    <li>
        <strong>Windows</strong> डेव्हलपर आता ठराविक प्लॅटफार्म करीता KDE अनुप्रयोग पूर्वदृश्य थेथून <a href="http://windows.kde.org">डाऊनलोड</a> करू शकता. लायब्ररी आधिपासूनच स्थीर आहे. Windows वरील kdelibs चे सर्व गुणविशेष उपलब्ध नसल्यावरही, लायब्ररी सुस्थीत आहे. काहिक अनुप्रयोग आधिपासूनच Windows वर कार्यरत असल्यामुळे, इतर कार्य करण्यास पात्र ठरत नाही.
    </li>
    <li>
        <strong>Mac OSX</strong> हे आणखी एक नविन प्लॅटफॉर्म वापरले जात आहे.
        <a href="http://mac.kde.org">Mac वर KDE</a> वापरणी करीता अजूनही सज्ज नाही. मल्टिमिडीया आधार Phonon द्वारे आधिपासूनच उपलब्ध असले तरी, हार्डवेअर व शोध एकत्रीकरण अजूनही अपूरे आहे.
    </li>
</ul>
</p>

<a id="screenshots-dolphin"></a>

<h3>
  स्क्रिनशॉट
</h3>
<p align="justify">

<a id="screenshots-dolphin"></a>

<h4>Dolphin</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-treeview.png">
	<img src="/announcements/4/4.1.0/dolphin-treeview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Dolphin's new treeview gives you quicker access across directories. Note that it's disabled in the default setting.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-tagging.png">
	<img src="/announcements/4/4.1.0/dolphin-tagging_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nepomuk provides tagging and rating in KDE -- and thus in Dolphin.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-icons.png">
	<img src="/announcements/4/4.1.0/dolphin-icons_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Icon preview and information bars provide visual feedback and overview.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-filterbar.png">
	<img src="/announcements/4/4.1.0/dolphin-filterbar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Find your files easier with the filter bar.</em>
</div>
<br/>

<a id="screenshots-gwenview"></a>

<h4>Gwenview</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-browse.png">
	<img src="/announcements/4/4.1.0/gwenview-browse_thumb.png" class="img-fluid">
	</a> <br/>
	<em>You can browse directories with images with Gwenview. Hover actions put common tasks at your fingertips.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-open.png">
	<img src="/announcements/4/4.1.0/gwenview-open_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Opening files from your harddisk or the network is just as easy, thanks to KDE's infrastructure.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-thumbnailbar.png">
	<img src="/announcements/4/4.1.0/gwenview-thumbnailbar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The new thumbnail bar lets you switch between images easily. It is also available in full screen mode.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-sidebar.png">
	<img src="/announcements/4/4.1.0/gwenview-sidebar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Gwenview's sidebar provides access to additional information and image manipulation options.</em>
</div>
<br/>

<a id="screenshots-marble"></a>

<h4>Marble</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/marble-globe.png">
	<img src="/announcements/4/4.1.0/marble-globe_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The Marble desktop globe.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/marble-osm.png">
	<img src="/announcements/4/4.1.0/marble-osm_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Marble's new OpenStreetMap integration also features public transport information.</em>
</div>
<br/>

<a id="screenshots-kwin"></a>

<h4>KWin</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-desktopgrid.png">
	<img src="/announcements/4/4.1.0/kwin-desktopgrid_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KWin's desktopgrid visualizes the concept of virtual desktops and makes it easier to remember where you left that window you're looking for.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-coverswitch.png">
	<img src="/announcements/4/4.1.0/kwin-coverswitch_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The Coverswitcher makes switching applications with Alt+Tab a real eye-catcher. You can choose it in KWin's desktop effects settings.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-wobbly.png">
	<img src="/announcements/4/4.1.0/kwin-wobbly_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KWin now also has the mandatory wobbly windows (disabled by default).</em>
</div>
<br/>

<a id="screenshots-plasma"></a>

<h4>Plasma</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/plasma-folderview.png">
	<img src="/announcements/4/4.1.0/plasma-folderview_thumb.png" class="img-fluid">
	</a> <br/> 
	<em>The new folderview applet lets you display the content of arbitrary
      directories on your desktop. Drop a directory onto your unlocked
      desktop to create a new folderview. A folderview can not only display local
      directories, but can also cope with locations on the network.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/panel-controller.png">
	<img src="/announcements/4/4.1.0/panel-controller_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The new panel controller lets you easily resize and reposition panels.
      You can also change the position of applets on the panel by dragging them
      to their new position.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/krunner-screenie.png">
	<img src="/announcements/4/4.1.0/krunner-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>With KRunner, you can start applications, directly email your friends
      and accomplish various other small tasks.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/plasma-kickoff.png">
	<img src="/announcements/4/4.1.0/plasma-kickoff_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Plasma's Kickoff application launcher has had a facelift.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/switch-menu.png">
	<img src="/announcements/4/4.1.0/switch-menu_thumb.png" class="img-fluid">
	</a> <br/>
	<em>You can choose between the Kickoff application launcher and the classic
      menu style.</em>
</div>
<br/>
</p>

<h4>
  परिचीत अडचणी
</h4>
<p align="justify">
<ul>
    <li> NVidia द्वारे पुरविलेल्या बायनरी ड्राइवरचा वापर करणाऱ्या <strong>NVidia</strong> कार्ड वापरकर्त्यांना चौकट बदलाव व पुन्हकार नुरूप कार्यक्षमता त्रुटीस सामोरे जावे लागेल. या त्रुटींविषयी NVidia इंजीनीयर यांना अवगत केले गेले आहे. तरी, ठराविक NVidia ड्राइवर अजूनही प्रकाशीत केले गेले नाही. चित्रलेखीय कार्यक्षमता वाढविण्यासाठी अधिक माहितीकरीता
<a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a> येथे भेट द्या, ड्राइवर कधी ठिक करायचे, हे NVidia वर अवलंबून आहे.</li>
</ul>

</p>

<h4>
    मिळवा, चालवा, तपासणी करा
</h4>
<p align="justify">
  मंडळातील प्रतिनिधी व Linux/UNIX OS विक्रेता यांनी विनम्रपणे अनेक Linux वितरण करीता व Mac OS X आणि Windows करीता KDE 4.1.0 चे बायनरी संकुल पुरविले आहे.  हे संकुल वापरणी करीता तयार नसल्याने यांचा वापर करतेवेळी खबरदारी बाळगा. त्याकरीता कार्यप्रणाली वरील सॉफ्टवेअर व्यवस्थापन प्रणालीची तपासणी करा.
</p>
<h4>
  KDE 4.1.0 कंपाईल पध्दती
</h4>
<p align="justify">
  <a id="source_code"></a><em>सोअर्स कोड</em>.
  KDE 4.1.0 चे पूर्ण सोअर्स कोड <a
  href="http://www.kde.org/info/4.1.0"> येथून मोफत डाऊनलोड केले जाऊ शकते</a>.
  KDE 4.1.0 चे कंपाईल व प्रतिष्ठापन सूचनांकरीता
 <a href="/info/4.1.0">KDE 4.1.0 माहिती पान</a>, किंवा
 <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a> पहा.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
