---
title: KDE Development Platform 4.5.0 gains performance, stability, new high-speed cache and support for WebKit
hidden: true
---

<p align="justify">
KDE, an international Free Software community, proudly announces the release of the KDE Platform 4.5. This basis for the Plasma Workspaces and Applications developed by KDE has seen significant improvements in many areas. Besides the introduction of new technology, a lot of work has been spent on improving the performance and stability of the underlying pillars of KDE development. KDE Platform has progressed in many areas, both in terms of infrastructure and of development tools.
</p>

<p align="justify">
<ul>
    <li>After years of work, the integration for <b>WebKit</b> has become part of our libraries. WebKit now provides a integration with password storage and other features in Konqueror, the KDE web browser. Other applications can make use of WebKit to display content with the same level of integration as KHTML.</li>
    <li>KHTML continues to develop, gaining support for XPath queries.</li>
    <li>Web content performance has been increased using a new scheduling policy, allowing for more parallel downloads of content from distinct servers, in turn enabling pages to be rendered faster as more of their components are loaded together. The page-rendering speeds of the KHTML and WebKit engines benefit from these changes.</li>
    <li><b>Plasma Workspaces</b> can now be configured using JavaScript templates which can be distributed as small, separate packages. This makes it possible for system administrators and integrators to easily set up custom Plasma configurations for users, or to change the default setup of a standard Plasma Desktop.</li>
    <li><b>KSharedDataCache</b> is a caching mechanism for applications. This new memory-mapped cache is already used for icons and demonstrates noticeable speed-ups there.</li>
    <li>Application developers making use of <b>Kate’s SmartRange interface</b> for their editor components now have several new interfaces to use, details for which can be found in <a href="http://kate-editor.org/2010/07/13/kde-4-5-smartrange-movingrange/">this blog post</a>.</li>
    <li>As KDE Games welcome their first full Python-written application, the KDE bindings developers have added Perl bindings to the officially supported language set. Ruby support in the KDE Platform has also been <a href="http://www.arnorehn.de/blog/2010/07/and-the-bindings-keep-rocking-writing-ruby-kio-slaves/">significantly improved</a>.</li>
    <li>The Phonon Multimedia library can now optionally use PulseAudio.</li>

</ul>
These changes form the underpinnings for many improvements in <a href="../plasma">Plasma Desktop and Plasma Netbook</a> and in the <a href="../applications">KDE Applications 4.5</a> that have been released in tandem with the KDE Development Platform 4.5.0.
</p>

<h2>Plans and Work in Progress</h2>
<p align="justify">
The KDE platform team is working on making the KDE Platform more suitable for mobile devices. With the introduction of KDE's platform profiles, it is possible to compile KDE with limited feature sets in order to minimise their footprint. These profiles are still being elaborated to meet mobile apps' needs. The plans include using it for the mobile versions of Kontact, KOffice and the Plasma Mobile workspace. The BlueDevil framework will add better support for many Bluetooth devices in upcoming versions. BlueDevil, which is based on BlueZ, found on many Linux systems, is maturing quickly and is currently going through a series of beta releases. Its developers are aiming at a release included with the KDE Platform 4.6 in January 2011.
</p>

<h4>Installing the KDE Development Platform</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.5.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
<a id="packages"><em>Packages</em></a>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.0
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.5.0.">4.5 Info
Page</a>.
</p>

<h4>
  Compiling 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.5.0 may be <a
href="http://download.kde.org/stable/4.5.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.5.0
  are available from the <a href="/info/4.5.0.#binary">4.5.0Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.6.3. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Released Today:</h2>

<a href="../plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.5.0" />
</a>

<h2>Plasma Desktop and Netbook 4.5.0: improved user experience</h2>
<p align="justify">
 KDE today releases the Plasma Desktop and Plasma Netbook Workspaces 4.5.0. Plasma Desktop received many usability refinements. The Notification and Job handling workflows have been streamlined. The notification area has been cleaned up visually, and its input handling across applications is now made more consistent by the extended use of the Freedesktop.org notification protocol first introduced in Plasma's previous version. <a href="../plasma"><b>Read The Full Announcement</b></a>
</p>

<a href="../applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.5.0"/>
</a>
<h2>KDE Applications 4.5.0 enhance usability and bring routing</h2>
<p align="justify">
The KDE team today releases a new version of the KDE Applications. Many educational titles, tools, games and graphical utilities have seen further enhancement and usability improvements. Routing backed by OpenRouteService has made its entry in Marble, the virtual globe. Konqueror, KDE's webbrowser can now be configured to use WebKit with the KWebKit component available in the Extragear repository. <a href="../applications"><b>Read The Full Announcement</b></a>
</p>
