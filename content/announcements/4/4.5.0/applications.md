---
title: KDE Applications 4.5.0 enhance usability and bring map routing
hidden: true
---

<p>
KDE, an international Free Software community, is happy to announce the immediate availability of the KDE Applications 4.5. Be it the high-quality games, educational and productivity software or the useful tools, these applications have become more powerful, yet easier to use.
</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/apps-overview.png">
	<img src="/announcements/4/4.5.0/thumbs/apps-overview.png" class="img-fluid" >
	</a> <br/>
	<em>Mathematical, text-editing and entertainment applications</em>
</div>
<br/>

<p>
The KDE Applications have been improved in many different areas. Let us highlight a few of the changes which were made.
</p>
    <ul>

   <li>The KDE Games team has worked on new features and introduced a new game: <b>Kajongg</b>, the <a href="http://en.wikipedia.org/wiki/Mahjong">original four-player Mahjongg</a>. This game is written entirely in Python, making it the first major Python KDE Application as part of the KDE Games family. The game <a href="http://www.kde.org/applications/games/konquest/">Konquest</a>, in which the player is tasked to conquer the galaxy, now lets users create and customize their own maps. Other improvements are easier configuration in the <a href="http://www.kde.org/applications/games/palapeli">jigsaw puzzle game Palapeli</a>, a new KGoldRunner level set (“Demolition”), and better <a href="http://en.wikipedia.org/wiki/Smart_Game_Format">SGF</a> file integration for <a href="http://www.kde.org/applications/games/kigo/">Kigo</a>.
    </li>
    <li>The <b>educational applications</b> have also seen several improvements. The biggest were for the learning aid, <a href="http://www.kde.org/applications/education/parley/">Parley</a>, which has gained a new practice interface and support for conjugations.
    </li>
    <li><a href="http://www.kde.org/applications/education/marble/"><b>Marble</b></a>, our virtual globe application, received a route planning feature and the option to download map data before you go on a trip so you do not need access to the Internet to make use of Marble and its mapping features based on OpenStreetmap and OpenRouteService data. Read more about <a href="http://nienhueser.de/blog/?p=137">worldwide and offline map routing in Marble</a>.
    </li>

</ul>

<div class="text-center">
	<a href="/announcements/4/4.5.0/marble-routing.png">
	<img src="/announcements/4/4.5.0/thumbs/marble-routing.png" class="img-fluid" >
	</a> <br/>
	<em>The Marble Desktop Globe can now be used for routing as well</em>
</div>
<br/>

<p align="justify">

In Konqueror, Adblock lists can now be automatically updated, making for a cleaner web-browsing experience. Users who prefer to use WebKit as the rendering engine can now rely on the KDE-WebKit KPart. Gwenview now runs smoothly without blocking even while applying processor-intensive effects. Its image editing operations now properly retain all EXIF metadata. Gwenview also sees many usability improvements and increased configurability. Finally, KInfoCenter, which gives the user an overview of the hardware and software configuration of his or her system, has seen some major work this release, with usability improvements to its main window and better stability.

</p>

<h3>More Screenshots...</h3>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-infocenter.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-infocenter.png" class="img-fluid">
	</a> <br/>
	<em>KInfoCenter, which shows information about your system has been visually revamped</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/dolphin-metadata.png">
	<img src="/announcements/4/4.5.0/thumbs/dolphin-metadata.png" class="img-fluid">
	</a> <br/>
	<em>Semantic Desktop features in Dolphin provide useful metadata</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/gwenview-flickrexport.png">
	<img src="/announcements/4/4.5.0/thumbs/gwenview-flickrexport.png" class="img-fluid">
	</a> <br/>
	<em>With Gwenview you can easily share your pictures on the web using your favourite picture service</em>
</div>
<br/>

<h4>Installing KDE Applications</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.5.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
<a id="packages"><em>Packages</em></a>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.0
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.5.0.">4.5 Info
Page</a>.
</p>

<h4>
  Compiling 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.5.0 may be <a
href="http://download.kde.org/stable/4.5.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.5.0
  are available from the <a href="/info/4.5.0.#binary">4.5.0Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.6.3. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Released Today:</h2>

<a href="../platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.5.0"/>
</a>

<h2>KDE Development Platform 4.5.0 gains performance, stability, new high-speed cache and support for WebKit</h2>
<p align="justify">
 KDE today releases the KDE Development Platform 4.5.0. This release brings many performance  and stability improvements. The new <b>KSharedDataCache</b> is optimized for fast access to resources stored on disk, such as icons. The new <b>WebKit</b> library provides integration with network settings, password-storage and many other features found in Konqueror. <a href="../platform"><b>Read The Full Announcement</b></a>
</p>

<a href="../plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.5.0" />
</a>

<h2>Plasma Desktop and Netbook 4.5.0: improved user experience</h2>
<p align="justify">
 KDE today releases the Plasma Desktop and Plasma Netbook Workspaces 4.5.0. Plasma Desktop received many usability refinements. The Notification and Job handling workflows have been streamlined. The notification area has been cleaned up visually, and its input handling across applications is now made more consistent by the extended use of the Freedesktop.org notification protocol first introduced in Plasma's previous version. <a href="../plasma"><b>Read The Full Announcement</b></a>
</p>
