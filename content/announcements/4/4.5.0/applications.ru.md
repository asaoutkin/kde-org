---
title: "Приложения KDE 4.5.0: улучшено удобство работы, появилась поддержка прокладывания маршрутов"
hidden: true
---

<p>
KDE, международное сообщество свободного программного обеспечения, 
объявляет о выпуске приложений KDE версии 4.5. Высококачественные игры, 
образовательное и офисное программное обеспечение и просто полезные 
программы — все они обзавелись новыми функциями, в то же время став 
проще в использовании.
</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/apps-overview.png">
	<img src="/announcements/4/4.5.0/thumbs/apps-overview.png" class="img-fluid" >
	</a> <br/>
	<em>Приложения для выполнения математических расчётов, работы с текстом и развлечения</em>
</div>
<br/>

<p>
Приложения KDE обросли улучшениями со всех сторон. Рассмотрим 
некоторые из сделанных улучшений.
</p>

<ul>

   <li>Команда разработчиков игр для KDE всё это время работала 
    над рядом нововведений в играх, и представила одну новую игру:
    <b>Kajongg</b>, 
    <a href="http://ru.wikipedia.org/wiki/Маджонг">настоящую 
    игру Маджонг для четверых игроков</a>. 
    Игра полностью написана на Python, и, таким образом, 
    является первым крупным приложением KDE, написанным на Python. 
    Игра 
    <a href="http://www.kde.org/applications/games/konquest/">Konquest</a>, 
    в которой игрок должен покорить галактику, теперь имеет 
    встроенный редактор карт. Среди прочих улучшений нужно 
    отметить упрощение настроек 
    <a href="http://www.kde.org/applications/games/palapeli">в мозаике Palapeli</a>, 
    новый набор уровней («Demolition») для KGoldRunner, 
    и лучшую интеграцию файлов 
    <a href="http://ru.wikipedia.org/wiki/Smart_Game_Format">SGF</a> 
    в <a href="http://www.kde.org/applications/games/kigo/">Kigo</a>.
    </li>
    <li>Появились улучшения и в <b>образовательных приложениях</b>. 
    Самые значительные были сделаны в приложении 
    <a href="http://www.kde.org/applications/education/parley/">Parley</a>,
    которое призвано помочь в процессе заучивания иностранной 
    лексики. Здесь появился новый пользовательский интерфейс и 
    поддержка спряжений глаголов.
    </li>
    <li>В 
    <a href="http://www.kde.org/applications/education/marble/"><b>Marble</b></a>, 
    виртуальном глобусе для KDE, появилась возможность 
    прокладывать маршруты (на базе интернет-сервисов 
    OpenStreetMap и OpenRouteService) и загружать карты 
    на компьютер (полезно в случае, если отправляетесь куда-то, 
    где не будет доступа в Интернет). Больше о прокладывании 
    маршрутов в Marble можно прочитать 
    <a href="http://nienhueser.de/blog/?p=137">здесь</a>. <!-- knotes -->
    </li>

</ul>

<div class="text-center">
	<a href="/announcements/4/4.5.0/marble-routing.png">
	<img src="/announcements/4/4.5.0/thumbs/marble-routing.png" class="img-fluid" >
	</a> <br/>
	<em>Виртуальный глобус Marble теперь можно использовать для прокладывания маршрутов</em>
</div>
<br/>

<p align="justify">

Списки фильтров Adblock в Konqueror теперь обновляются
автоматически, что позволяет более эффективно очищать
просматриваемые веб-страницы от навязчивой рекламы.
Любители WebKit наконец-то получили возможность использовать
компонент веб-браузера KDE-Webkit.
Gwenview теперь работает более стабильно, без блокировки
окна даже когда применяемые эффекты интенсивно используют ресурсы ЦП.
При редактировании изображений корректно сохраняются
метаданные EXIF. Gwenview стало намного легче использовать,
а также появилась возможность более гибкой настройки.
И, наконец, KInfoCenter, приложение которое показывает
общие данные об используемом программном и аппаратном
обеспечении, стало более удобным в использовании и
теперь работает ещё стабильнее.

</p>

<h3>Другие снимки экрана...</h3>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-infocenter.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-infocenter.png" class="img-fluid">
	</a> <br/>
	<em>На KInfoCenter, программу, показывающую информацию о системе, теперь стало значительно приятнее смотреть</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/dolphin-metadata.png">
	<img src="/announcements/4/4.5.0/thumbs/dolphin-metadata.png" class="img-fluid">
	</a> <br/>
	<em>Dolphin позволяет просматривать метаданные файла</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/gwenview-flickrexport.png">
	<img src="/announcements/4/4.5.0/thumbs/gwenview-flickrexport.png" class="img-fluid">
	</a> <br/>
	<em>С помощью Gwenview можно с лёгкостью загружать изображения на многочисленные интернет-сервисы</em>
</div>
<br/>

<h4>Установка приложений KDE</h4>

<p align="justify">
Пакет программ KDE, включая все приложения и библиотеки, 
доступен бесплатно, на условиях лицензий, предусматривающих открытость исходного кода. 
Программный пакет KDE работает на целом ряде аппаратного 
обеспечения и операционных систем и может работать с 
любым диспетчером окон и окружением рабочего стола. 
Кроме Linux и других UNIX-подобных систем, приложения KDE 
доступны также в версиях для Microsoft Windows на сайте 
<a href="http://windows.kde.org">«Программный  пакет KDE в Windows»</a> 
и для Apple Mac OS X на сайте 
<a href="http://mac.kde.org/">«Программный пакет KDE для Mac»</a>. 
В сети также можно найти экспериментальные сборки KDE для 
мобильных платформ (MeeGo, MS Windows Mobile, Symbian), 
но в настоящее время их поддержка не осуществляется. 
<br /> KDE можно получить в скомпилированном и в виде 
исходных текстов на сайте 
<a
href="http://download.kde.org/stable/4.5.0/">
http://download.kde.org</a>, на 
<a href="http://www.kde.org/download/cdrom">CD-ROM</a> 
либо как часть одного из 
<a href="http://www.kde.org/download/distributions">
популярных дистрибутивов GNU/Linux и UNIX</a>.
</p>
<p align="justify">
  <a id="packages"><em>Пакеты</em></a>. 
Некоторые поставщики систем Linux/UNIX создали скомпилированные 
пакеты 4.5.0 для своих дистрибутивов. 
В других случаях этим занимались члены сообщества 
пользователей дистрибутива. <br /> Некоторые из них доступны 
на сайте 
<a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">
http://download.kde.org</a>. 
Дополнительные пакеты скомпилированного программного 
обеспечения и обновления к пакетам будут 
доступны через несколько недель.
</p>

<p align="justify">
  <a id="package_locations"><em>Расположение пакетов</em></a>. 
  Список доступных на данный момент пакетов KDE находится на 
  <a href="/info/4.5.0">4.5
  информационной странице выпуска</a>.
</p>

<h4>
  Сборка из исходных кодов 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  Полные исходные коды 4.5.0 находятся 
  в свободном доступе для 
  <a href="http://download.kde.org/stable/4.5.0/src/">
  загрузки</a>. 
  Инструкции по сборке из исходных кодов и установке 
  программного обеспечения KDE 4.5.0 находятся на 
  <a href="/info/4.5.0#binary">информационной странице выпуска 4.5.0</a>.
</p>

<h4>
Системные требования
</h4>
<p align="justify">
Для того, чтобы выжать максимум из данного выпуска, 
рекомендуется использовать самую свежую версию библиотеки 
Qt (на сегодняшний день — 4.6.3). Это необходимо для 
обеспечения стабильной работы, так как некоторые 
усовершенствования, представленные в данном выпуске 
программного обеспечения KDE, 
на самом деле были сделаны в самой библиотеке Qt.<br />
Графические драйверы в некоторых ситуациях могут использовать XRender, 
а не OpenGL для обработки графических эффектов. 
Если наблюдается падение производительности системы при 
включении графических эффектов рабочего стола, их 
отключение скорее всего решит проблему, в зависимости 
от используемого графического драйвера и аппаратной 
конфигурации. Для того, чтобы в полной мере использовать 
возможности программного обеспечения KDE, также рекомендуется 
установить самую свежую версию графических драйверов, 
так как это позволит повысить удобство работы, как 
на уровне доступности функций, так и на уровне общего 
быстродействия и стабильности. 
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Также сегодня вышли:</h2>

<a href="../platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="Платформа разработки KDE 4.5.0"/>
</a>

<h2>Платформа разработки KDE версии 4.5.0 получила прирост 
стабильности и производительности, новый высокоскоростной 
кэш и поддержку WebKit.</h2>
<p align="justify">
Сегодня команда разработчиков KDE 
объявляет о выходе платформы разработки KDE версии 4.5.0. 
В этом выпуске были значительно улучшены стабильность и 
производительность работы. Новый кэш <b>KSharedDataCache</b> 
был оптимизирован для осуществления быстрого доступа к 
файлам, хранящимся на диске (например, к значкам). 
Новая библиотека <b>WebKit</b> была тесно интегрирована 
с Konqueror и теперь поддерживает общие настройки сети, 
хранение паролей и многое другое. 
<a href="../platform"><b>Прочитать анонс полностью...</b></a>
</p>

<a href="../plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="Оболочки рабочего стола KDE Plasma 4.5.0" />
</a>

<h2>Оболочки рабочего стола Plasma Desktop и Plasma Netbook 4.5.0: 
работать стало удобнее</h2>
<p align="justify">
Сегодня команда разработчиков 
KDE объявляет о выходе оболочек рабочего стола Plasma Desktop 
и Plasma Netbook версии 4.5.0. 
Теперь рабочий стол Plasma стало ещё удобнее использовать. 
Была улучшена обработка задач и уведомлений. 
Область уведомления получила обновлённый, визуально более 
приятный внешний вид. Также, благодаря интенсивному 
использованию протокола уведомлений Freedesktop.org 
(впервые был использован в предыдущей версии Plasma), 
было улучшено взаимодействие с ней различных приложений. 
<a href="../plasma"><b>Прочитать анонс полностью...</b></a>
</p>
