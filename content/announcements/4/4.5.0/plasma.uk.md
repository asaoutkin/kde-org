---
title: KDE Plasma Workspaces Improve User Experience
hidden: true
---

<h2>Спільнотою KDE випущено Плазму для стаціонарних та портативних комп’ютерів версії 4.5.0</h2>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-netbook-sal.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-netbook-sal.png" class="img-fluid" >
	</a> <br/>
	<em>У інтерфейсі пошуку і запуску Плазми для нетбуків передбачено доступ до програм, записів контактів та пошуку у стільниці</em>
</div>
<br/>

<p>
KDE, міжнародна спільнота з розробки вільного програмного забезпечення, щаслива повідомити про випуск робочих просторів Плазми версії 4.5 для стаціонарних та портативних комп’ютерів. У обох робочих просторах передбачено подальші покращення та кроки у напрямку створення семантичної стільниці з орієнтацією на виконання завдань користувача. Окрім виправлення тисяч вад, які заважали користувачам попередніх версій, стільниця стала зручнішою та привабливішою. Серед цікавинок цього випуску:
</p>
<ul>
    <li>У <b>область сповіщення</b> на панелі було очищено від зайвих візуальних елементів. Монохроматичні піктограми зроблять панель візуально простішою та зручнішою для користувача. Позначки стеження за звантаженням та позначки виконання інших тривалих дій, які концентруються у області сповіщення, тепер обробляються у самому віджеті показу поступу. Крім того, перероблено механізми впорядкування та обробки черги сповіщень від програм. У новій версії сповіщення від віджетів спільного використання на віддалених системах може бути показано разом з віджетами сповіщень про локальні події.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-notification.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-notification.png" class="img-fluid" >
	</a> <br/>
	<em>Покращена обробка сповіщень та кращий вигляд стільниці Плазми</em>
</div>
<br/>

<br />
    <li><b>KWin</b>, компонент керування вікнами робочого простору Плазми, тепер здатен розташовувати вікна без перекриття на основі алгоритмів побудови мозаїки. Відповідно до визначених користувачем наборів правил, KWin виконує розташування вікон без втручання користувача. Крім того, у новій версії компонування вікон ви зможете <b>без проблем пересувати вікна екраном простим перетягуванням їх за вільне місце</b> у певному вікні. Ця нова можливість покращує ергономіку керування вікнами: тепер ви можете використовувати довільне порожнє місце вікна для його перетягування. Можливість реалізовано лише для програм на основі бібліотеки Qt. Для інтеграції з іншими бібліотеками потрібно реалізувати інтеграцію додатково. Додавання і вилучення віртуальних стільниць тепер можна виконати безпосередньо з пейджера стільниць. Реалізовано ґратку віртуальних стільниць. Окрім цих змін, у багатьох системах збільшено швидкодію ефектів стільниці.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-desktopgrid.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-desktopgrid.png" class="img-fluid" >
	</a> <br/>
	<em>Тепер вам зручно буде додавати і вилучати віртуальні стільниці з ґратки стільниць</em>
</div>
<br/>
    <li>Робочий простір Плазми <b>Простори дій</b>: масштабований інтерфейс користувача (Zooming User Interface або ZUI) попередніх версій стільниці Плазми було замінено механізмом «Керування просторами дій», подібним до діалогового вікна додавання віджетів, реалізованого у випуску 4.4. За допомогою нового керування просторами дій ви зможете додавати, вилучати, зберігати та відновлювати простори дій, а також перемикатися між ними. Просторами дій тепер простіше керувати, вони допоможуть користувачам виконувати звичні для них завдання з роботи на комп’ютері шляхом якіснішого поділу між просторами виконання різних завдань. Нове керування просторами дій є першою видимою частиною <em>прив’язки до контексту</em>, яку запроваджено до Плазми шляхом використання можливостей <em>Семантичної стільниці</em>, робота яких забезпечується Nepomuk.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-netbook-pageone.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-netbook-pageone.png" class="img-fluid" >
	</a> <br/>
	<em>На першій сторінці Плазми для нетбуків ви побачите вкладки соціальних мереж та набір ваших улюблених віджетів плазми</em>
</div>
<br/>
    <li><b>Плазма для нетбуків</b>, робочий простір KDE для невеликих ноутбуків та нетбуків, було значно покращено у цьому випуску. Значна частина змін залишається майже непомітною на поверхні, але забезпечить вам зручності у роботі на нетбуці, кращу швидкодію та якіснішу роботу за допомогою сенсорних екранів.
    </li>
    <li>Новий інструмент налаштування теми Oxygen надасть змогу досвідченим користувачам налаштувати стиль Oxygen бажаним для них чином. Нарешті, було значно покращено рушій тем Aurorae, додано ефект розмивання, який можна вимкнути, якщо його використання призводить до зниження швидкодії через недостатню реалізацію потужностей графічної картки у драйверах.</li>
    <li>Оскільки програми KDE та вільне програмне забезпечення загалом призначено для користувачів з усього світу, ви зможете працювати у системі, використовуючи тайський, тайванський та японський календарі, а також підтримку нових систем запису чисел. У контекстному вікні дат календаря буде наведено дані щодо свят.</li>
</ul>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-calendar.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-calendar.png" class="img-fluid" >
	</a> <br/>
	<em>Клацання на годиннику відкриває віджет календаря, у якому буде показано свята місцевості, у якій ви мешкаєте</em>
</div>
<br/>

<p align="justify">
Серед інших покращень, зміни у показі KRunner результатів пошуку, можливість зміни розмірів піктограм панелі, просте налаштування панелі за допомогою шаблонів JavaScript, тіні, кращі можливості перетягування зі скиданням у віджеті швидкого запуску, за допомогою яких ви зможете розподілити вас список улюблених програм на стовпчики та рядки, покращення у Dolphin, Konsole тощо. У новій версії аплети Плзами можна запускати як окремі програми за допомогою програми <em>plasma-windowed</em>. Нова версія програми сповіщення про портативні пристрої показує зрозуміліші повідомлення про помилки.
</p>

<h4>Зміни у впорядкування налаштувань робочого простору</h4>
<p align="justify">
У новій версії було змінено структуру налаштування робочого простору на основі нових категорій у «Системних параметрах». За допомогою нового розділу «Робочий простір» ви зможете змінити вигляд і поведінку вашого робочого простору Плазми. Ви зможете змінити тему, увімкнути або вимкнути можливості семантичної стільниці та стільничного пошуку, а також увімкнути можливості, які здатні змінити робочий простір потрібним вам чином: налаштувати поведінку стільниці у відповідь на наведення вказівника миші на кут екрана, візуальні допоміжні елементи для керування вікнами, нову можливість мозаїчного розташування вікон тощо.
</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-systemsettings.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-systemsettings.png" class="img-fluid" >
	</a> <br/>
	<em>У розділі «Робочий простір» «Системних параметрів» ви зможете виконати довільне налаштування вигляду та поведінки вашого робочого простору.</em>
</div>
<br/>

<p align="justify">
Ефекти робочого простору здатні зробити роботу на стільниці зручною у декілька способів. Зокрема, ви можете скористатися показом мініатюр вікон за допомогою OpenGL на панелі задач та ефектом стільничної ґратки, за допомогою якого вам буде зручніше орієнтуватися у вашому наборі віртуальних стільниць. Якщо апаратна частина вашого комп’ютера не здатна належним чином обробляти дані графічних ефектів, Плазма зможе працювати і без просторових ефектів у режимі традиційного двовимірного показу. Рекомендуємо вам скористатися найсучаснішою версією драйверів для графічної картки та набором бібліотек XOrg, щоб забезпечити найкращу швидкодію. У версії 4.5 адміністратори та поширювачі програмного забезпечення отримали інструменти для точнішого керування графічним обладнанням та драйверами за допомогою <a href="http://blog.martin-graesslin.com/blog/2010/07/blacklisting-drivers-for-some-kwin-effects/">списків блокування ефектів</a>. За допомогою цих списків простіше вмикати та вимикати певні можливості на основі даних щодо можливості їх використання та стабільності роботи у певних системах. Крім того, ця можливість забезпечить безпроблемну роботу без додаткового налаштування для багатьох користувачів.

</p>
<h3>Інші знімки вікон…</h3>
<p align="justify">

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-desktopsettings.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-desktopsettings.png" class="img-fluid" >
	</a> <br/>
	<em>Інтерфейс параметрів стільниці став значно зручнішим</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-jobs.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-jobs.png" class="img-fluid" >
	</a> <br/>
	<em>Візуалізація запущених завдань у Плазмі, ...</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-filecopyjobs.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-filecopyjobs.png" class="img-fluid" >
	</a> <br/>
	<em>...завдання групуються, а сповіщення запам’ятовуються.</em>
</div>
<br/>

</p>

<h3>Плани та поточні розробки</h3>
<p align="justify">
Команда розробників Плазми працює над подальшим покращенням швидкодії керування вікнами. Крім того, у списку завдань розширення концепції <b>Простори дій</b>, групування відповідних вікон вручну або автоматично, підтримка додаткових можливостей комп’ютера, що зроблять роботу робочого простору та програм краще пов’язаною з контекстом роботи, зокрема можливостей геопозиціювання. Крім того, передбачено реалізацію прозорого розмивання тла вікон, що покращить ергономіку та вигляд стільниці шляхом підвищення зручності читання тексту та написів на елементах інтерфейсі користувача.<br/>
Нові робочі простори Плазми для <b>медіа-центру</b> та мобільних телефонів також перебувають у розробці. Під час нещодавньої конференції Akademy було продемонстровано перший телефонний дзвінок, виконаний за допомогою нової оболонки <b>Мобільна Плазма</b>.
</p>

<h4>Встановлення Плазми</h4>
<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-desktopsettings.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-desktopsettings.png" class="img-fluid" >
	</a> <br/>
	<em>The Desktop Settings user interface has seen some nice usability enhancements</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-jobs.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-jobs.png" class="img-fluid" >
	</a> <br/>
	<em>Running jobs are visualized by Plasma, ...</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-filecopyjobs.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-filecopyjobs.png" class="img-fluid" >
	</a> <br/>
	<em>... multiple jobs are grouped and notifications are remembered for a while.</em>
</div>
<br/>
</p>

<h3>Plans and Work in Progress</h3>
<p align="justify">
The Plasma team is working on further performance improvements in window management. Also on the agenda is extending the concepts of <b>Activities</b>, the intentional and automatic grouping of related windows and supporting desktop furniture, making the workspace and applications more context-aware, including the use of geolocation. Effects such as blurring transparent window backgrounds are being employed to increase ergonomics while looking good, by making text and user interface elements more readable.<br/>
New Plasma Workspaces for <b>Media Center</b> use-cases and mobile phones are also on their way. During the recent Akademy conference, the first phone call was made using the new <b>Plasma Mobile</b> shell.
</p>

<h4>Installing Plasma</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.5.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
<a id="packages"><em>Packages</em></a>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.0
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.5.0.">4.5 Info
Page</a>.
</p>

<h4>
  Compiling 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.5.0 may be <a
href="http://download.kde.org/stable/4.5.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.5.0
  are available from the <a href="/info/4.5.0.#binary">4.5.0Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.6.3. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Крім того, сьогодні випущено:</h2>

<a href="../platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="Платформа розробки KDE 4.5.0"/>
</a>

<h2>У новій Платформі розробки KDE 4.5.0 ви зможете скористатися покращеною швидкодією, стабільністю, новим високошвидкісним кешем та підтримкою WebKit</h2>
<p align="justify">
 Сьогодні командою KDE було випущено Платформу розробки KDE версії 4.5.0. Цей випуск характеризується покращеннями у швидкодії та стабільності. Нову версію <b>KSharedDataCache</b> оптимізовано для пришвидшення доступу до даних, що зберігаються на диску, зокрема піктограм. У новій версії бібліотеки <b>WebKit</b> передбачено інтеграцію з параметрами мережі, системою зберігання паролів та іншими можливостями Konqueror. <a href="../platform"><b>Ознайомитися з повним текстом повідомлення</b></a>
</p>

<a href="../applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="Програми KDE 4.5.0"/>
</a>

<h2>Набір програм KDE 4.5.0: зручність у користуванні і маршрутизація</h2>
<p align="justify">
10 серпня 2010 року. Сьогодні командою KDE було випущено нову версію збірки програм KDE. У цій збірці покращено роботу освітніх програм, інструментів та графічних програм. У віртуальному глобусі Marble реалізовано пошук маршрутів за допомогою служби OpenRouteService. Konqueror, переглядач Інтернету KDE, тепер можна налаштувати на використання WebKit за допомогою компонента KWebKit, встановити який можна зі сховища додаткових програм KDE. <a href="../applications"><b>Ознайомитися з повним текстом повідомлення</b></a>
</p>
