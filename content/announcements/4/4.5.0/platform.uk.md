---
title: У платформі розробки KDE 4.5.0 покращено швидкодію, стабільність, реалізовано високошвидкісне кешування та підтримку WebKit
hidden: true
---

<p align="justify">
KDE, міжнародна спільнота з розробки вільного програмного забезпечення оголошує про випуск Платформи KDE 4.5. Ця платформа є основою робочих просторів Плазми та Програми, розроблених KDE. У новому випуску Платформи ви знайдете значні покращення у багатьох сферах. Окрім реалізації нової технології, значних зусиль було докладено до покращення швидкодії та стабільності основних компонентів розробки KDE. Розвиток Платформи KDE було продовжено у багатьох напрямках у сенсі інфраструктури та інструментів розробки.
</p>

<p align="justify">
<ul>
    <li>Після років розробки <b>KPart WebKit</b> стала частиною наших бібліотек. Таким чином, користувачі зможуть зробити вибір між рушієм показу даних WebKit та його попередником, KHTML. Нову версію WebKit значно краще інтегровано до Konqueror, програми для перегляду Інтернету KDE, та інших програм, які здатні використовувати можливості Інтернету за допомогою бібліотек KDE.</li>
    <li>Розробка KHTML триває, реалізовано підтримку запитів XPath</li>
    <li>Швидкодію обробки інтернет-даних покращено за допомогою нових правил планування, які забезпечують збільшення кількості потоків паралельного звантаження даних з декількох серверів. Це, у свою чергу, призвело до пришвидшення показу сторінок інтернету, оскільки значну частину їх елементів можна завантажувати паралельно. Внесені зміни одночасно покращили швидкодію рушіїв KHTML і WebKit.</li>
    <li><b>Робочі простори Плазми</b> тепер можна налаштовувати за допомогою шаблонів JavaScript, які можна поширювати у невеличких окремих пакунках. Це спрощує виконання завдань адміністраторів системи та інтеграторів зі створення нетипових налаштувань Плазми для користувачів, а також зміну типових налаштувань стільниці Плазми.</li>
    <li><b>KSharedDataCache</b> — механізм кешування для програм. Новий кеш з встановленням відповідності областей пам’яті даним вже використовується для піктограм і показує значне зростання швидкодії.</li>
    <li>Розробники програмного забезпечення, які користуються <b>інтерфейсом SmartRange Kate</b> для компонентів своїх редакторів, можуть скористатися одним з декількох нових інтерфейсів. Докладніше про це у <a href="http://kate-editor.org/2010/07/13/kde-4-5-smartrange-movingrange/">відповідному дописі блогу</a>.</li>
    <li>У той час, коли набір програм KDE поповнився новою програмою, повністю написаною мовою Python, до набору прив’язок бібліотек KDE додано прив’язки для мови Perl. Крім того, підтримку Ruby у Платформі KDE <a href="http://www.arnorehn.de/blog/2010/07/and-the-bindings-keep-rocking-writing-ruby-kio-slaves/">було значно покращено</a>.</li>
    <li>До складу нової версії Платформи KDE включено нові <b>бібліотеки прив’язки до Perl</b>, що розширить діапазон мов, якими можна користуватися для розробки програмного забезпечення, що використовує компоненти Платформи KDE.</li>
    <li>Мультимедійна бібліотека Phonon нової версії може використовувати звуковий сервер PulseAudio.</li>

</ul>
Описані вище зміни є основою багатьох покращень у роботі <a href="../plasma">Плазми для стаціонарних та портативних комп’ютерів</a> та <a href="../applications">Програм KDE 4.5</a>, які було випущено разом з Платформою розробки KDE 4.5.0.
</p>

<h2>Плани та поточні розробки</h2>
<p align="justify">
Команда з розробки платформи KDE працює над покращенням сумісності Платформи KDE з мобільними пристроями. З реалізацією профілів платформи стало можливим збирання KDE з обмеженим набором можливостей з метою зменшення впливу цих можливостей на швидкодію. Розробка для мобільних програм триває. У планах використання профілів для мобільних версій Kontact, KOffice та робочого простору Мобільної Плазми. Оболонка BlueDevil значно покращить підтримку багатьох пристроїв Bluetooth у наступних версіях. BlueDevil, заснована на бібліотеці BlueZ, яка є частиною багатьох систем Linux швидко набуває остаточних обрисів. Розробники намагаються зробити все від них залежне для включення цієї системи до Платформи KDE 4.6, яку буде випущено у січні 2011 року.
</p>

<h4>Встановлення Платформи розробки KDE</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.5.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
<a id="packages"><em>Packages</em></a>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.0
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.5.0.">4.5 Info
Page</a>.
</p>

<h4>
  Compiling 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.5.0 may be <a
href="http://download.kde.org/stable/4.5.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.5.0
  are available from the <a href="/info/4.5.0.#binary">4.5.0Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.6.3. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Крім того, сьогодні випущено:</h2>

<a href="../plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="Робочі простори Плазми KDE 4.5.0" />
</a>

<h2>Плазма для стаціонарних та портативних комп’ютерів, версія 4.5.0: зручніше користування</h2>
<p align="justify">
 Сьогодні командою KDE випущено робочі простори Плазми для стаціонарних та портативних комп’ютерів версії 4.5.0. Користування Плазмою для стаціонарних комп’ютерів стало набагато зручнішим. Спрощено прийоми роботи з сповіщеннями та завданнями. З області сповіщень було усунуто зайві візуальні елементи, обробка сповіщень від програм стала одноріднішою за рахунок ширшого використання протоколу сповіщення Freedesktop.org, вперше реалізованого у попередній версії Плазми. <a href="../plasma"><b>Ознайомитися з повним текстом повідомлення</b></a>
</p>

<a href="../applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="Програмиe KDE 4.5.0"/>
</a>

<h2>Набір програм KDE 4.5.0: зручність у користуванні і маршрутизація</h2>
<p align="justify">
10 серпня 2010 року. Сьогодні командою KDE було випущено нову версію збірки програм KDE. У цій збірці покращено роботу освітніх програм, інструментів та графічних програм. У віртуальному глобусі Marble реалізовано пошук маршрутів за допомогою служби OpenRouteService. Konqueror, переглядач Інтернету KDE, тепер можна налаштувати на використання WebKit за допомогою компонента KWebKit, встановити який можна зі сховища додаткових програм KDE. <a href="../applications"><b>Ознайомитися з повним текстом повідомлення</b></a>
</p>
