---
title: KDE Plasma Workspaces Improve User Experience
hidden: true
---

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-netbook-sal.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-netbook-sal.png" class="img-fluid" >
	</a> <br/>
	<em>Plasma Netbook's Search and Launch interface offers applications, contacts and desktop search</em>
</div>
<br/>

<p>
KDE, an international Free Software community, is happy to announce the immediate availability of the Plasma Desktop and Netbook workspaces in version 4.5. The Plasma Workspaces see both refinement and innovation towards semantic, task-driven workflows. In addition to thousands of bugs that have been fixed in order to make the user experience more complete, more usable and more attractive, some interesting changes are included in this release:
</p>
<ul>
    <li>The <b>notification area</b> in the panel has been cleaned up visually. Monochromatic icons give visual clarity, and more consistent user interactions improve usability. Download tracking and indicators for other long-running operations, which are centralized in the notification area, are now handled by a visual progress indication in the widget itself. Likewise, the display, categorization and queueing of application notifications has been reworked. Notifications from shared Plasma widgets on remote systems may now be displayed as well as local events.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-notification.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-notification.png" class="img-fluid" >
	</a> <br/>
	<em>Plasma Desktop's notifications have received polish in handling, display and appearance</em>
</div>
<br/>

<br />
    <li><b>KWin</b>, the window manager component of the Plasma Workspace, can now lay-out windows on the screen without overlapping using tiling algorithms. Following a defined set of rules, KWin takes care of placing windows for the user. It is now also possible to <b>easily move windows around by dragging an empty area</b> inside the window. This new feature improves window handling ergonomics by allowing empty space to be used as drag handles. This feature will only work with Qt-based applications, as it requires some integration with the underlying toolkit to work properly. Adding and removing virtual desktops can now be done directly from the pager and desktop grid effect. Alongside these changes, many systems will enjoy improved window effects performance.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-desktopgrid.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-desktopgrid.png" class="img-fluid" >
	</a> <br/>
	<em>You can now add and remove virtual desktops conveniently from the Desktop Grid</em>
</div>
<br/>
    <li>Plasma Workspace <b>Activities</b>: the Zooming User Interface (ZUI)  of the previous Plasma Desktop releases has been replaced by an "Activity Manager", similar to the Add Widgets dialog introduced with the 4.4 release. The new Activity Manager provides adding, removing, saving and restoring of Activities, and allows to switch between them. The visual concept of an Activity has been extended as well. Activities are now easier to  manage and can aid users in their daily computing needs, providing them  with a clearer separation between different tasks. The new Activity Manager is the first visible piece of <em>context-awareness</em> brought into Plasma through the use of <em>Semantic Desktop</em> features provided by Nepomuk.
    </li>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-netbook-pageone.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-netbook-pageone.png" class="img-fluid" >
	</a> <br/>
	<em>Plasma Netbook's Page One is designed to keep tabs on the social web, it can hold any Plasma widget</em>
</div>
<br/>
    <li><b>Plasma Netbook</b>, the KDE Workspace for small notebooks and netbooks, has received substantial improvements with this release. Many of the changes are not immediately visible, but do provide a more pleasant netbook experience with higher performance and better touchscreen capabilities.
    </li>
    <li>The new "oxygen-settings" tool allows power users to configure the default Oxygen style to their liking. Finally, the theme engine Aurorae has seen many improvements and the blur effect has been reintroduced but may be disabled depending on the capabilities of your graphics driver.</li>
    <li>In a move to support Free Software around the world, all KDE applications now can work with the Thai, Taiwanese and Japanese calendars, and more digit sets are supported. Local holidays are now displayed in detail in the calendar popup.</li>
</ul>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-calendar.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-calendar.png" class="img-fluid" >
	</a> <br/>
	<em>Clicking on the clock reveals the calendar, which displays holidays in your region</em>
</div>
<br/>

<p align="justify">
A variety of other improvements span changes to KRunner’s display of results, configurable panel icon sizes, easy panel configuration using JavaScript templates and shadows, better drag and drop in the Quick Launch widget, which now allows favorite applications to be arranged in multiple columns and rows, improvements in Dolphin, Konsole, and much more. It has also been made possible to start Plasma applets as separate applications through the <em>plasma-windowed</em> application. The Removable Device Notifier now shows errors more clearly.
</p>

<h4>Workspace Setup Restructured</h4>
<p align="justify">
The configuration for the workspace has been restructured and is now offered through a category in System Settings. The new Workspace section provides control over the Look and Feel of your Plasma workspace. Here you can set the theming to your liking, enable or disable the Semantic Desktop &amp; Desktop Search features,  and enable many settings that make your workspace work the way you want it to, be it hot corners, visual helpers used for managing windows, the new window tiling feature, and many more.
</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-systemsettings.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-systemsettings.png" class="img-fluid" >
	</a> <br/>
	<em>The Workspace section in System Settings gives you full control over the look and feel of your workspace.</em>
</div>
<br/>

<p align="justify">
Workspace effects enhance the user experience in many ways.  These include the usage of OpenGL-rendered window previews in the taskbar, and a desktop grid effect which enables you to zoom in and out of virtual desktops. If your hardware or graphics driver does not support these graphical effects properly, Plasma falls back gracefully to traditional 2D rendering. We recommend using the latest version of the graphics driver and underlying XOrg stack for the best performance. In 4.5, it is now possible for administrators and distributors to gain finer control over graphics hardware and drivers used through the use of <a href="http://blog.martin-graesslin.com/blog/2010/07/blacklisting-drivers-for-some-kwin-effects/">per-effect blacklists</a>. This makes it easier to enable and disable certain features based on their availability and stability on certain systems. It also makes it easier to provide a smoother experience out of the box to many users.

</p>
<h3>More Screenshots...</h3>
<p align="justify">

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-desktopsettings.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-desktopsettings.png" class="img-fluid" >
	</a> <br/>
	<em>The Desktop Settings user interface has seen some nice usability enhancements</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-jobs.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-jobs.png" class="img-fluid" >
	</a> <br/>
	<em>Running jobs are visualized by Plasma, ...</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-filecopyjobs.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-filecopyjobs.png" class="img-fluid" >
	</a> <br/>
	<em>... multiple jobs are grouped and notifications are remembered for a while.</em>
</div>
<br/>
</p>

<h3>Plans and Work in Progress</h3>
<p align="justify">
The Plasma team is working on further performance improvements in window management. Also on the agenda is extending the concepts of <b>Activities</b>, the intentional and automatic grouping of related windows and supporting desktop furniture, making the workspace and applications more context-aware, including the use of geolocation. Effects such as blurring transparent window backgrounds are being employed to increase ergonomics while looking good, by making text and user interface elements more readable.<br/>
New Plasma Workspaces for <b>Media Center</b> use-cases and mobile phones are also on their way. During the recent Akademy conference, the first phone call was made using the new <b>Plasma Mobile</b> shell.
</p>

<h4>Installing Plasma</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.5.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
<a id="packages"><em>Packages</em></a>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.0
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.5.0.">4.5 Info
Page</a>.
</p>

<h4>
  Compiling 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.5.0 may be <a
href="http://download.kde.org/stable/4.5.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.5.0
  are available from the <a href="/info/4.5.0.#binary">4.5.0Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.6.3. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Released Today:</h2>

<a href="../platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.5.0"/>
</a>

<h2>KDE Development Platform 4.5.0 gains performance, stability, new high-speed cache and support for WebKit</h2>
<p align="justify">
 KDE today releases the KDE Development Platform 4.5.0. This release brings many performance  and stability improvements. The new <b>KSharedDataCache</b> is optimized for fast access to resources stored on disk, such as icons. The new <b>WebKit</b> library provides integration with network settings, password-storage and many other features found in Konqueror. <a href="../platform"><b>Read The Full Announcement</b></a>
</p>

<a href="../applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.5.0"/>
</a>
<h2>KDE Applications 4.5.0 enhance usability and bring routing</h2>
<p align="justify">
The KDE team today releases a new version of the KDE Applications. Many educational titles, tools, games and graphical utilities have seen further enhancement and usability improvements. Routing backed by OpenRouteService has made its entry in Marble, the virtual globe. Konqueror, KDE's webbrowser can now be configured to use WebKit with the KWebKit component available in the Extragear repository. <a href="../applications"><b>Read The Full Announcement</b></a>
</p>
