---
title: La plataforma de desarrollo de KDE 4.5.0 mejora su rendimiento, su estabilidad e introduce una nueva cach&eacute; de alta velocidad y el uso de WebKit
hidden: true
---

<p align="justify">
KDE, una comunidad internacional de software libre, tiene el orgullo de publicar la plataforma KDE 4.5. Esta base desarrollada por KDE para el espacio de trabajo Plasma y para las aplicaciones ha experimentado mejoras significativas en muchas &aacute;reas. Adem&aacute;s de la introducci&oacute;n de nueva tecnolog&iacute;a, se ha trabajado mucho para mejorar el rendimiento y la estabilidad de los pilares fundamentales del desarrollo de KDE. La plataforma KDE ha progresado en muchas &aacute;reas, tanto en t&eacute;rminos de infraestructura como de herramientas de desarrollo.
</p>

<p align="justify">
<ul>
    <li>Tras a&ntilde;os de trabajo, la integraci&oacute;n de <b>WebKit</b> ya forma parte de nuestras bibliotecas. Ahora, WebKit proporciona una integraci&oacute;n con el almacenamiento de contrase&ntilde;as y otras caracter&iacute;sticas de Konqueror, el navegador de KDE. Otras aplicaciones pueden hacer uso de WebKit para mostrar contenidos con el mismo nivel de integraci&oacute;n que KHTML.</li>
    <li>KHTML contin&uacute;a su desarrollo con la compatibilidad con las consultas XPath.</li>
    <li>El rendimiento de los contenidos web se ha mejorado mediante una nueva pol&iacute;tica de planificaci&oacute;n que admite mayor n&uacute;mero de descargas de contenidos en paralelo de diferentes servidores, permitiendo que las p&aacute;ginas se generen m&aacute;s r&aacute;pidamente ya que sus componentes se cargan juntos. La velocidad de generaci&oacute;n de p&aacute;ginas de los motores de KHTML y de WebKit tambi&eacute;n se benefician de estos cambios.</li>
    <li>Ahora, <b>el espacio de trabajo Plasma</b> se puede configurar mediante plantillas de JavaScript que se pueden distribuir en forma de peque&ntilde;os paquetes aparte. Esto permite que los administradores del sistema y los integradores preparen configuraciones personalizadas de Plasma para los usuarios o que cambien la configuraci&oacute;n predeterminada de un escritorio Plasma est&aacute;ndar.</li>
    <li><b>KSharedDataCache</b> es un mecanismo de cach&eacute; para aplicaciones. Esta nueva cach&eacute; en memoria ya se utiliza para los iconos y ha demostrado notables aumentos de velocidad en esa &aacute;rea.</li>
    <li>Ahora, los desarrolladores de aplicaciones que hagan uso de la <b>interfaz SmartRange de Kate</b> para sus componentes de editor tienen asu disposici&oacute;n varias interfaces nuevas, de las cuales se pueden consultar m&aacute;s detalles en <a href="http://kate-editor.org/2010/07/13/kde-4-5-smartrange-movingrange/">esta entrada de blog.</a>.</li>
    <li>Como KDE Games da la bienvenida a su primera aplicaci&oacute;n completamente escrita en Python, los desarrolladores de bindings de KDE han a&ntilde;adido bindings de Perl al conjunto del lenguaje implementado oficialmente. Tambi&eacute;n se ha <a href="http://www.arnorehn.de/blog/2010/07/and-the-bindings-keep-rocking-writing-ruby-kio-slaves/">mejorado significativamente</a>. el uso de Ruby en la plataforma KDE.</li>
    <li>Ahora, la biblioteca Phonon Multimedia puede utilizar PulseAudio de manera opcional.</li>

</ul>
Estos cambios constiyuyen los fundamentos para muchas de las mejoras en <a href="../plasma">el escritorio Plasma y en Plasma Netbook</a> as&iacute; como en las<a href="../applications">aplicaciones de KDE 4.5</a> que se han publicado de manera conjunta con la plataforma de desarrollo KDE 4.5.0.
</p>

<h2>Planes y trabajo en procceso</h2>
<p align="justify">
El equipo de la plataforma KDE est&aacute; trabajando para hacerla m&aacute;s adecuada para dispositivos port&aacute;tiles. Con la introducci&oacute;n de los perfiles de la plataforma KDE, es posible compilar KDE con configuraciones de caracter&iacute;sticas limitadas con el fin de minimizar su huella. Dichos perfiles tambi&eacute;n se est&aacute;n desarrollando para satisfacer las necesidades de las aplicaciones m&oacute;viles. Los planes incluyen su uso para las versiones m&oacute;viles de Kontact, KOffice y el espacio de trabajo Plasma Mobile. El marco de trabajo BlueDevil a&ntilde;adir&aacute; una mejor compatibilidad para numerosos dispositivos Bluetooth en versiones posteriores. BlueDevil, que est&aacute; basado en BlueZ y que se encuentra en muchos sistemas Linux, est&aacute; madurando r&aacute;pidamente y en la actualidad est&aacute; pasando por una serie de versiones beta. Sus desarrolladores planean publicar una nueva versi&oacute;n con la plataforma KDE 4.6 en enero de 2011.
</p>

<h4>Instalaci&oacute;n de la plataforma de desarrollo de KDE</h4>
<p align="justify">
El software de KDE, incluyendo todas sus bibliotecas y aplicaciones, est&aacute; disponible bajo licencias de c&oacute;digo abierto. El software de KDE se puede ejecutar sobre varias configuraciones de hardware, sistemas operativos y cualquier gestor de ventanas o entorno de escritorio. Adem&aacute;s de Linux y otros sistemas operativos basados en UNIX, en el sitio web del <a href="http://windows.kde.org">KDE para Windows</a> dispone de versiones para Microsoft Windows de la mayor&iacute;a de aplicaciones, y versiones para Mac OS X en el sitio de <a href="http://mac.kde.org/">KDE on Mac</a>. Tambi&eacute;n puede encontrar compilaciones experimentales de las aplicaciones de KDE para plataformas m&oacute;viles como MeeGo, MS Windows Mobile y Symbian en la web, pero no est&aacute;n soportadas. 
KDE se puede obtener en c&oacute;digo fuente y varios formatos binarios en <a
href="http://download.kde.org/stable/4.5.0/">http://download.kde.org</a> y tambi&eacute;n se puede obtener en <a href="http://www.kde.org/download/cdrom">CD-ROM</a> o con cualquiera de las <a href="http://www.kde.org/download/distributions">principales distribuciones de GNU/Linux y sistemas UNIX</a> actuales.
</p>
<p align="justify">
  <a id="packages"><em>Paquetes</em></a>.
  Algunos proveedores de sistemas operativos Linux/UNIX han tenido la amabilidad de proporcionar paquetes binarios de 4.5.0 para algunas versiones de su distribuci&oacute;n, y en otros casos lo han hecho voluntarios de la comunidad. <br />
  Algunos de estos paquetes binarios est&aacute;n disponibles para su descarga gratuita desde <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">http://download.kde.org</a>.
  En las pr&oacute;ximas semanas estar&aacute;n disponibles paquetes binarios adicionales, as&iacute; como actualizaciones de los ya disponibles.
</p>

<p align="justify">
  <a id="package_locations"><em>Ubicaciones de los paquetes</em></a>.
  Visite la <a href="/info/4.5.0">p&aacute;gina de informaci&oacute;n de 4.5</a> para obtener una lista actualizada de los paquetes binarios disponibles de los que ha sido informado el Equipo de Publicaci&oacute;n de KDE.
</p>

<h4>
  Compilaci&oacute;n de 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  El c&oacute;digo fuente completo de 4.5.0 se puede <a
href="http://download.kde.org/stable/4.5.0/src/">descargar libremente</a>. Las instrucciones para compilar e instalar KDE 4.5.0 est&aacute;n disponibles en la <a href="/info/4.5.0">p&aacute;gina de informaci&oacute;n de 4.5</a>.
</p>

<h4>
Requisitos del sistema
</h4>
<p align="justify">
Para obtener lo m&aacute;ximo posible de nuestras publicaciones, le recomendamos encarecidamente el uso de la &uacute;ltima versi&oacute;n de Qt, que a d&iacute;a de hoy es la 4.6.3. Es necesaria para asegurarle una experiencia estable, as&iacute; como algunas mejoras del software KDE que se basan en el framework Qt subyacente.<br />
Algunos controladores gr&aacute;ficos pueden, bajo ciertas condiciones, utilizar XRender en lugar de OpenGL para la composici&oacute;n. Si experimenta una notable lentitud en cuanto al rendimiento gr&aacute;fico, puede que le ayude desactivar los efectos de escritorio, dependiendo del controlador gr&aacute;fico y de la configuraci&oacute;n utilizados. Para poder aprovechar todas las posibilidades del software KDE, le recomendamos que utilice los &uacute;ltimos controladores disponibles para su sistema, puesto que pueden mejorar su experiencia notablemente, tanto en cuesti&oacute;n de funciones opcionales como en rendimiento y estabilidad.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Tambi&eacute;n publicados hoy:</h2>

<a href="../plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.5.0" />
</a>

<h2>Plasma para escritorio y netbooks 4.5.0: experiencia de usuario mejorada</h2>
<p align="justify">
Hoy, KDE publica los espacios de trabajo del escritorio Plasma y Plasma para netbooks 4.5.0. El escritorio Plasma ha refinado mucho su usabilidad. Se ha mejorado la eficiencia de los flujos de gesti&oacute;n de notificaciones y trabajos, el &aacute;rea de notificaciones se ha "limpiado" a nivel visual, y la gesti&oacute;n de entrada de datos entre aplicaciones es m&aacute;s consistente al utilizar el protocolo de notificaciones de Freedesktop.org ya introducido en la versi&oacute;n anterior de Plasma. <a href="../plasma"><b>Leer el anuncio completo</b></a>
</p>

<a href="../applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.5.0"/>
</a>
</a>
<h2>Las aplicaciones de KDE 4.5.0 mejoran su usabilidad e incluyen rutas de mapas</h2>
<p align="justify">
10 de agosto de 2010. Hoy, el equipo de KDE publica una nueva versi&oacute;n de las aplicaciones de KDE. Se han mejorado muchos t&iacute;tulos educativos, herramientas, juegos y utilidades gr&aacute;ficas a nivel funcional y de usabilidad. Marble, el globo virtual, incluye rutas bas&aacute;ndose en OpenRouteService. Konqueror, el navegador web de KDE se puede configurar para utilizar WebKit mediante el componente KWebKit disponible en el repositorio Extragear. <a href="../applications"><b>Leer el anuncio completo</b></a>
</p>
