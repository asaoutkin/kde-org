---
title: "Slikovni vodič po KDE 4.0: Igre"
hidden: true
---

<p>
<img src="/announcements/4/4.0/images/games.png" align="right"  hspace="10"/>
Skupnost KDE Games je vložila ogromno truda v prenos in prenovo mnogih iger iz KDE 3
in ustvarjanje novih razburljivih iger za KDE 4.0.<br />
Igre so bolj intuitivne za igranje, imajo dodelano grafiko in so bolj neodvisne od
ločljivosti zaslona. Igre lahko igrate razpete čez ves zaslon ali pa v majhnem oknu.<br />
Večina grafike je ustvarjene povsem na novo, kar da igram v KDE 4.0 zelo dodelan izgled in modern videz.
</p>

<h2>KGoldrunner: arkadna igra</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kgoldrunner.png">
<img src="/announcements/4/4.0/kgoldrunner_thumb.png" class="img-fluid">
</a> <br/>
<em>Zberite zlate kepe v KGoldrunnerju</em>
</div>
<br/>

<p>
KGoldrunner je retro arkadna igra s priokusom reševanja ugank.
Vsebuje stotine stopenj, kjer morate zbrati kepe zlata, pri
tem pa vas neutrudno lovijo sovražniki.
</p>

<h2>KFourInLine: štiri v vrsto</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kfourinline.png">
<img src="/announcements/4/4.0/kfourinline_thumb.png" class="img-fluid">
</a> <br/>
<em>Postavite štiri žetone v vrsto v KFourInLine</em>
</div>
<br/>

<p>
KFourInLine je namizna igra za dva igralca, ki je znana pod
imenom »Štiri v vrsto«. S strateško postavitvijo žetonov vsak
igralec poskuša postaviti štiri žetone v ravno vrsto.
</p>

<h2>LSkat: igra s kartami</h2>

<div class="text-center">
<a href="/announcements/4/4.0/lskat.png">
<img src="/announcements/4/4.0/lskat_thumb.png" class="img-fluid">
</a> <br/>
<em>Igrajte Skat</em>
</div>
<br/>

<p>
Poročnik Skat (nemško »Offiziersskat«) je zabavna in nalezljiva igra
s kartami za dva igralca. Drugi igralec je lahko živa oseba ali pa
računalnik.
</p>

<h2>KJumpingCube: igra s kockami</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kjumpingcube.png">
<img src="/announcements/4/4.0/kjumpingcube_thumb.png" class="img-fluid">
</a> <br/>
<em>Kockajte s KJumpingCube</em>
</div>
<br/>

<p>
KJumpingCube je preprosta strateška igra, ki temelji na igralnih kockah.
Igralna površina je sestavljena iz kvadratov s pikami. Ko je igralec na potezi,
klikne na prost kvadrat ali pa na svojega.
</p>

<h2>KSudoku: logična igra</h2>

<div class="text-center">
<a href="/announcements/4/4.0/ksudoku.png">
<img src="/announcements/4/4.0/ksudoku_thumb.png" class="img-fluid">
</a> <br/>
<em>Razmigajte možgane s KSudoku</em>
</div>
<br/>

<p>
KSudoku je miselna igra postavljanja simbolov. Igralec mora zapolniti
mrežo na tak način, da je v eni vrstici, v enem stolpcu in v enem
bloku samo po en izvod vsakega simbola.
</p>

<h2>Konquest: strategija</h2>

<div class="text-center">
<a href="/announcements/4/4.0/konquest.png">
<img src="/announcements/4/4.0/konquest_thumb.png" class="img-fluid">
</a> <br/>
<em>Osvojite planete v Konquest</em>
</div>
<br/>

<p>
Igralec osvaja druge planete s pošiljanjem svojih ladij nanje.
Cilj je zgraditi galaktični imperij, s tem da osvojite vse planete.
</p>

<p>
Več o zgoraj omenjenih in dodatnih igrah izveste na osveženi strani
skupnosti <a href="http://games.kde.org">KDE Games</a>.
</p>
<table width="100%">
	<tr>
		<td width="50%">
				<a href="../education">
				<img src="/announcements/4/4.0/images/education-32.png" />
				Prejšnja stran: Izobraževalni programi
				</a>		
		</td>
	<td align="right" width="50%">
				<a href="../guide">Pregled
				<img src="/announcements/4/4.0/images/star-32.png" /></a>
		</td>
	</tr>
</table>
