---
title: "KDE 4.0 Visual Guide: Applications"
hidden: true
---


<h2>The Dolphin file manager</h2>
<p>
Dolphin is KDE4's new file manager. You can browse, locate, open, copy and move files with it. Dolphin concentrates on ease of use and replaces Konqueror's 
filemanagement component, which was used in KDE 3 and earlier. While Konqueror can still be used as file manager, and in fact shares the fileview functionality with Dolphin, the KDE team has decided to introduce an application that is optimized for filemanagement: Dolphin.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-splitview.png">
<img src="/announcements/4/4.0/dolphin-splitview_thumb.png" class="img-fluid">
</a> <br/>
<em>Copy files using Dolphin</em>
</div>
<br/>


<p>
For directories containing lots of images, press the preview button in Dolphin's 
toolbar and get previews of the files located in the current directory. To quickly move between directories, click on the breadcrumbs located right above the fileview. Clicking on the arrow next to one of the breadcrumbs lets you move swiftly to different subdirectories. To 
quickly move between directories, directly above the fileview, click on a directory name.  Clicking on the arrow next to one of the directory names lets 
you move swiftly to different subdirectories. For a side-by-side view that 
makes copying files between directories easy, hit the "Split View" button. While 
Dolphin remembers settings for a specific directory, you can also set the 
defaults to your personal taste via the "Settings" | "Configure Dolphin" menu.
</p>

<p>
On the left-hand side, Dolphin's sidebar provides quick access to your most 
often used locations, called "Places". Just drag a folder to the sidebar and be able 
to quickly access it, from not only Dolphin itself, but also from the KickOff Places tab and from the 
"File Open" dialog of all your applications.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-groups.png">
<img src="/announcements/4/4.0/dolphin-groups_thumb.png" class="img-fluid">
</a> <br/>
<em>Show files in groups with Dolphin</em>
</div>
<br/>

<p>
The Information sidebar provides a preview with some additional information 
about the selected file. You can also use it to add comments to your files and 
tag them for easier sorting. Enable the option "View" | "Show in Groups" to group 
files sorted by size, type or other characteristics.
</p>

<h2>Okular and Gwenview: View your documents and images</h2>

<div class="text-center">
<a href="/announcements/4/4.0/gwenview.png">
<img src="/announcements/4/4.0/gwenview_thumb.png" class="img-fluid">
</a> <br/>
<em>View your images with Gwenview</em>
</div>
<br/>


<p>
<strong>Gwenview</strong> is KDE's image viewer. While it was available 
in KDE3, the KDE4 version has a simplified user interface, making
it more suitable for quickly browsing through your collection of images. Gwenview
is also used to display images. It provides a nice fullscreen interface that 
can be used to display your images as a slideshow.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/okular.png">
<img src="/announcements/4/4.0/okular_thumb.png" class="img-fluid">
</a> <br/>
<em>Okular is KDE 4.0's fast and versatile document reader</em>
</div>
<br/>

<p>
<strong>Okular</strong> is KDE4's document viewer. It supports a multitude of 
formats, ranging from PDF files to OpenDocument files. Okular is not limited to 
only reading files, however. The new "Review" feature allows you to scribble 
annotations into documents. Press F6, choose a pen and then start marking text in your 
documents, add notes to them, or comment on sections. Okular is
based on KPdf, KDE3's PDF viewer. Okular focuses on usability and support for a 
broad range of document file formats.
</p>


<h2>System Settings</h2>
<p>
System Settings is KDE 4.0's control center. Here you can change the look and feel 
of your applications, configure personal settings, change network settings 
and administer your computer.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/systemsettings-appearance.png">
<img src="/announcements/4/4.0/systemsettings-appearance_thumb.png" class="img-fluid">
</a> <br/>
<em>Change the appearance the desktop in System Settings</em>
</div>
<br/>


<p>
Open "Appearance" to change the color scheme of all applications to provide more 
contrast, or match your personal taste better. Here you can also change the size 
and faces of fonts used in your applications. Although KDE 4.0 comes with well-designed and sensible default artwork, it might not fit everybody's 
taste. The Appearance sheets in System Settings let you easily completely change the look 
of the desktop.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/solid.png">
<img src="/announcements/4/4.0/solid_thumb.png" class="img-fluid">
</a> <br/>
<em>Hardware integration through the Solid framework</em>
</div>
<br/>


<p>
System Settings also provides tools to control the underlying operating system. Solid, 
takes care of things such as power management, device hotplugging and network connectivity 
internally, and uses proven infrastructure that can be set up according to your system.
</p>

<h2>Konsole</h2>
<p>
An example for an application that has seen a major overhaul for KDE 4.0 is the terminal 
emulation program Konsole. The configuration dialog has been made easier to use
while not cutting down on Konsole's featureset. Other improvements include:
</p>

<div class="text-center">
<a href="/announcements/4/4.0/konsole.png">
<img src="/announcements/4/4.0/konsole_thumb.png" class="img-fluid">
</a> <br/>
<em>Konsole, KDE's terminal emulator</em>
</div>
<br/>

<p>
<ul>
	<li>
		The user interface has been reorganized and cleaned up, many keyboard shortcuts
		have been added to make the use of konsole more efficient.
	</li>
	<li>
		A split view has been introduced so the user can divide konsole windows into
		different areas. This makes for easy scanning of past output and monitoring.
	</li>
	<li>
		Tab titles are now automatically changed to make it easier to identify them.
	</li>
	<li>
		Incremental search results are highlighted and a search bar makes for a
		smoother user experience. Press CTRL+SHIFT+F in a terminal window to search
		through the buffer.
	</li>
	<li>
		Performance has been improved in scrolling big terminal displays and searching large
		amounts of output 
	</li>
	<li>
		Terminal windows have real transparency that can be enabled in the Appearance tab.
		(Note that you might need to start the first Konsole window running with 
		"konsole --enable-transparency".)
	</li>
</ul>
</p>
<p>
More new features in Konsole can be found in its 
<a href="http://websvn.kde.org/branches/KDE/4.0/kdebase/apps/konsole/CHANGES-4.0?view=markup">
changelog</a>.
</p>

<h2>More applications</h2>
<p>
The release of KDE 4.0 also brings good news for users of extragear applications. Applications 
in the extragear modules normally take care of their own releases. From now on it is also 
possible to follow the main KDE Release Schedule. The release team therefore has extended its 
duties and will provide tarballs for applications which 
<a href="http://techbase.kde.org/Projects/extragearReleases">want to join</a> this for every KDE 
release. This first set of tarballs already contain some well known extragear applications, 
like: <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Projects/KAider">Kaider</a>, 
<a href="http://ktorrent.org/">KTorrent[3]</a>, 
<a href="http://ktown.kde.org/kphotoalbum/">KPhotoAlbum</a> and 
<a href="http://rsibreak.org/">RSIBreak</a>. 
The KDE4 port might not be complete for these applications, but they are happy to receive bug 
reports now.
</p>

<table width="100%">
	<tr>
		<td width="50%">
				<a href="../desktop">
				<img src="/announcements/4/4.0/images/desktop-32.png" />
				Previous page: Desktop
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="../education">Next page: Educational Applications
				<img src="/announcements/4/4.0/images/education-32.png" /></a>
		</td>
	</tr>
</table>
