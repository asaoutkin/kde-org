---
title: KDE 4.0 ਵਿਜ਼ੁਅਲ ਗਾਈਡ
hidden: true
---

<p>
ਇੱਕ ਟੂਰ ਲਵੋ, KDE 4.0 ਬਾਰੇ ਜਾਣੋ
FIXME:Obvious.
</p>
<div align="left">
<table width="400" border="0" cellspacing="20" cellpadding="8">
<tr>
	<td>
		<a href="../desktop.pa"><img src="/announcements/4/4.0/images/desktop-32.png" /></a>
	</td>
	<td align="center">
			<a href="../desktop.pa"><strong>ਡੈਸਕਟਾਪ</strong>:</a>
	</td>
</tr>
<tr>
	<td>
		<a href="../applications.pa"><img src="/announcements/4/4.0/images/applications-32.png" /></a>
	</td>
	<td align="center">
		<a href="../applications.pa">ਐਪਲੀਕੇਸ਼ਨ</a>
	</td>
</tr>
<tr>
	<td>
		<a href="../education"><img src="/announcements/4/4.0/images/education-32.png" /></a>
	</td>
	<td align="center">
		<a href="../education">ਵਿਦਿਅਕ</a>
	</td>
</tr>
<tr>
	<td>
		<a href="../games"><img src="/announcements/4/4.0/images/games-32.png" /></a>
	</td>
	<td align="center">
		<a href="../games">ਖੇਡਾਂ</a>
	</td>
</tr>
</table>
</div>
