---
title: KDE 4.0 Visual Guide
hidden: true
---

<p>
The KDE 4.0 Desktop and applications deserve a closer look. The pages below provide
an overview of KDE 4.0 and give some examples of its associated applications. Screenshots of many components are included. Be aware that this is just a small sample of what KDE 4.0
offers you.
</p>
<div align="left">
<table width="400" border="0" cellspacing="20" cellpadding="8">
<tr>
	<td width="32">
		<a href="../desktop"><img src="/announcements/4/4.0/images/desktop-32.png" /></a>
	</td>
	<td>
		<a href="../desktop"><strong>The Desktop</strong>: Plasma, KRunner, KickOff and KWin</a>
	</td>
</tr>
<tr>
	<td>
		<a href="../applications"><img src="/announcements/4/4.0/images/applications-32.png" /></a>
	</td>
	<td>
		<a href="../applications"><strong>Applications</strong>: Dolphin, Okular, Gwenview, System Settings and Konsole</a>
	</td>
</tr>
<tr>
	<td>
		<a href="../education"><img src="/announcements/4/4.0/images/education-32.png" /></a>
	</td>
	<td>
		<a href="../education"><strong>Educational Applications:</strong> Kalzium, Parley, Marble, Blinken, KStars and KTouch</a>
	</td>
</tr>
<tr>
	<td>
		<a href="../games"><img src="/announcements/4/4.0/images/games-32.png" /></a>
	</td>
	<td>
		<a href="../games"><strong>Games</strong>: KGoldrunner, KFourInLine, LSkat, KJumpingCube, KSudoku and Konquest</a>
	</td>
</tr>
</table>
</div>

<p>
<br /><br /><br />
<em>The Visual Guide has been written by Sebastian K&uuml;gler and Jos Poortvliet.</em>
</p>
