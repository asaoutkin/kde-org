---
title: "Applicazioni educative di KDE 4.0"
hidden: true
---

<p>
Il team KDE Edu sviluppa delle ottime applicazioni educative. Le età alle quali queste applicazioni sono dedicate variano dai 3 ai 99 anni, dagli alunni delle scuole elementari agli studenti universitari sino ad arrivare agli insegnanti, ma generalmente sono indirizzate ai primi.
</p>

<h2>Kalzium</h2>
<p>
Kalzium è un'applicazione che mostra informazioni a proposito degli elementi chimici. Gioca con la barra della temperatura per vedere lo stato delgli elementi alle varie temperature. La linea del tempo mostr, alla stessa maniera di quella della temperatura, l'anno in cui un determinato elemento è stato scoperto. Nella tab «Calcola» si possono immetere formulechimiche, per esempio: C2H5Br. Kalzium mostrerà di quali atomi questa molecola è costituita e la massa molecolare.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kalzium-stateofmatter.png">
<img src="/announcements/4/4.0/kalzium-stateofmatter_thumb.png" class="img-fluid">
</a> <br/>
<em>Kalzium mostra lo stato degli elementi</em>
</div>
<br/>

<p>
Fai click sul risolutore di equazioni, inserisci una equazione chimica e lascia che Kalzium ti dia il risultatos. Troverai una equazione di esempio già inserita, fai click su Calcola per vedere il risultato.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kalzium-molview.png">
<img src="/announcements/4/4.0/kalzium-molview_thumb.png" class="img-fluid"><br/></a>
<em>Check out molecules in 3d in Kalzium </em>
</div>
<br/>

<p>
Fai click sul visualizzatore molecolare per vedere i modelli delle molecole. Molti esempi sono installati con Kalzium. Fai click su «carica molecola» e scegline una, potrai ruotare la molecola mantenendo premuto il pulsante sinistro del mouse e muovendo il puntatore. La rotellina del mouse effettua lo zoom e il pulsante sinistro gira la molecola. Le opzioni sotto «Display» permettono di migliorare la qualità e la maniera in cui le molecole vengono mostrate. Per modificare la visualizzazione della molecola seleziona i diversi parametri sotto il menu «Balls and sticks»  e scegli «Van der Waals» per vedere la «Van der Waals force». Kalzium, tramite <a href="http://openbabel.sourceforge.net/wiki/Main_Page">OpenBabel</a>, ha la capacità di leggere molti formati per la visualizzazione delle molecole.
</p>

<h2> Parley Vocabulary Trainer</h2>

<div class="text-center">
<a href="/announcements/4/4.0/parley.png">
<img src="/announcements/4/4.0/parley_thumb.png" class="img-fluid">
</a> <br/>
<em>Learn foreign languages with Parley</em>
</div>
<br/>

<p>
Parley è un'applicazione studiata per facilitare l'apprendimento di lingue straniere o di un qualsiasi altro insieme di parole ed oggetti. Fai click sul menu File, Scarica un nuovo vocabolario e inizia ad imparare nuove parole! Fai click su «inizia pratica» per iniziare la lezione. Rispondi alle domande e verifica le risposte. Quando «stop» viene premuto Parley mostrerà i risultati del test. Nel menu delle statistiche Parley mostrerà i tuoi progressi.
</p>

<h2>Marble, il mappamondo per il Desktop</h2>

<div class="text-center">
<a href="/announcements/4/4.0/marble.png">
<img src="/announcements/4/4.0/marble_thumb.png" class="img-fluid">
</a> <br/>
<em>Il mappamondo per il vostro Desktop</em>
</div>
<br/>

<p>
Marble è un'applicazione che mostra un mappamondo tridimensionale. Puoi ruotare il globo terrestre o fare zoom su una determinata area. Scegli una città e ti verranno mostrate le informazioni tratte da wikipedia, comprese foto e collegamenti per approfondire. Selezionando la visualizzazione della mappa, sulla sinistra, potrai scegliere altri modi di rappresentare la superfice terrestre, inclusa una visuale appiattita come una normale mappa oppure una visualizzazione notturna. Mantenendo il puntatore del mouse sul tipo di mappa scelto vengono mostrate importanti informazioni sulla stessa. Si possono scaricare nuove visualizzazioni della terra selezionando «Scarica nuovi dati» dal menu File. Scegliendo la mappa «Crustage», per esempio, viene mostrata la crosta terrestre sottomarina in base alla usa età. </p>

<h2>Allena la memoria con Blinken!</h2>

<div class="text-center">
<a href="/announcements/4/4.0/blinken.png">
<img src="/announcements/4/4.0/blinken_thumb.png" class="img-fluid">
</a> <br/>
<em>Allena memoria e riflessi con Blinken!</em>
</div>
<br/>

<p>
Blinken è una piccola applicazione per l'allenamento della memoria. Avviato Blinken puoi scegliere il livello di difficoltà e iniziare l'allenamento. Blinken inizierà a fare il suo lavoro indicandoti dove fare click con una luce. Subito dopo le luci in sequenza illumineranno, a caso, due dei quattro lati di Blinken, poi tre, poi quattro... e così via! Prova ad arrivare più lontano che puoi!
</p>

<h2> KStars, la mappa celeste</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kstars.png">
<img src="/announcements/4/4.0/kstars_thumb.png" class="img-fluid">
</a> <br/>
<em>Naviga fra le stelle con KStars</em>
</div>
<br/>

<p>
KStars è un'applicazione che puoi usare per vedere il cielo notturno. Può essere usata per controllare un telescopio digitale, anche tramite internet. Al primo avvio l'applicazione chiede dove ci si trova e sposta la vista in modo da rappresentare il cielo come dovrebbe essere visto dal luogo che è stato indicato. Puoi acchiappare il cielo e farlo ruotare tenendo premuto il pulsante sinistro del mouse, gestire l'ingrandimento con la rotellina e ottenere maggiori informazioni sugli oggetti utilizzando il pulsante destro. Fai click sul menu «Strumenti»e seleziona «Che si vede stanotte» per avere una lista degli oggetti osservabili dalla posizione configurata. Seleziona un oggetto dalla lista e scegli «Centra oggetto» per averlo centrato nella visualizzazione del cielo. Fai click su «Dettagli oggetto» per vedere una lista dettagliata dei dati sull'oggetto come l'orbita, la magnitudine e le coordinate o anche per centrarlo con il telescopio. Nella scheda «Collegamenti» puoi trovare una lista di risorse online relative all'oggetto. Nella scheda «Log» puoi prendere i tuoi appunti sulle osservazioni effettuate. Se possiedi un telescopio adatto, puoi configurarlo per essere pilotato da KStars nel menu «Periferiche», alla voce «Configura telescopio». Puoi anche viaggiare nel tempo scegliendo una data e un'ora diverse da quella attuale, e vedere come il cielo e disposto a quella data. Nella visualizzazione normale il cielo scorre in tempo reale, ma puoi accelerare o diminuire lo scorrere del tempo utilizzando i relativi bottoni nella barra degli strumenti.
</p>

<h2>KTouch, l'istruttore di dattilografia</h2>

<div class="text-center">
<a href="/announcements/4/4.0/ktouch.png">
<img src="/announcements/4/4.0/ktouch_thumb.png" class="img-fluid">
</a> <br/>
<em>Allenati con la videoscrittura con KTouch</em>
</div>
<br/>

<p>
KTouch è un programma che ti aiuta ad allenarti a scrivere sulla tastiera. All'avvio dell'applicazione, scegli una delle «Lezioni predefinite» dal menu «Allenamento». Il tuo compito sarà quello di digitare correttamente le lettere, che compaiono all'interno della prima riga bianca, nella seconda riga. L'immagine della tastiera ti suggerirà quali lettere premere successivamente. Se commetti un errore la barra dove stai digitando diventerà rossa, e non potrai andare avanti con la lezione finchè non correggi l'errore. KTouch ti mostrerà quanti errori hai commesso e la tua velocità di digitazione. Se vorrai approfondire le statistiche delle lezioni potrai farlo alla voce «Statistiche lezione» nel menu «Allenamento».
</p>
<table width="100%">
	<tr>
		<td width="50%">
			<a href="../applications">
				<img src="/announcements/4/4.0/images/applications-32.png" />
				Nella pagina Precedente: Applicazioni
				</a>		
		</td>
		<td align="right" width="50%">
	      <a href="../games">Nella prossima pagina: Giochi
				<img src="/announcements/4/4.0/images/games-32.png" /></a>
		</td>
	</tr>
</table>
