---
title: "Slikovni vodič po KDE 4.0: Izobraževalni programi"
hidden: true
---

<p>
Ekipa KDE Education ustvarja visoko-kakovostno izobraževalno programsko opremo.
Namenjena je otrokom in odraslim od 3 do 99 leta starosti. V pomoč je tako
osnovnošolcem, srednješolcem, študentom, kot tudi staršem in učiteljem. Glavna
pozornost je namenjena otrokom v osnovni in srednji šoli.
</p>

<h2>Kalzium</h2>
<p>
Kalzium prikazuje podatke o kemijskih elementih. Da bi videli agregatno stanje
elementa pri določeni temperaturi, na levi kliknite na zavihek »Agregatno stanje«
in nastavite drsnik. Časovnica na podoben način prikazuje leto odkritja elementa.
Na zavihku Izračunaj lahko vnesete kemijsko formulo, npr. C2H5Br, Kalzium pa bo
prikazal atome, ki sestavljajo molekulo, in molekulsko maso.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kalzium-stateofmatter.png">
<img src="/announcements/4/4.0/kalzium-stateofmatter_thumb.png" class="img-fluid">
</a> <br/>
<em>Kalzium prikazuje agregatno stanje</em>
</div>
<br/>

<p>
Kliknite na gumb »Reševalec enačb«, vnesite kemijsko enačbo in Kalzium bo
izračunal rezultat. Privzeto je že vnesena vzorčna enačba, kliknite na Izračunaj
in dobili boste rezultat.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kalzium-molview.png">
<img src="/announcements/4/4.0/kalzium-molview_thumb.png" class="img-fluid"><br/></a>
<em>Kalzium prikazuje molekulo v 3D pogledu </em>
</div>
<br/>

<p>
Da vidite 3D modele molekul, kliknite na gumb »Prikaz molekule«. S Kalziumom že
prihajajo nekateri primeri. Kliknite na »Naloži molekulo« in izberite eno izmed
datotek. Molekulo lahko vrtite s pritiskom desnega gumba miške in premikom miške.
Z miškinim koleščkom lahko spreminjate povečavo, z levim gumbom pa molekulo
lahko premikate naokoli. Z možnostmi v skupini Prikaz lahko nastavljate način in
kvaliteto prikaza molekule. Kliknite na spustni meni z besedilom »Krogljice in
palčke« ter izberite »Van der Waals«, da bi videli Van der Waalsovo silo. Kalzium
podpira mnogo oblik zapisa podatkov o molekulah. zahvaljujoč
<a href="http://openbabel.sourceforge.net/wiki/Main_Page">OpenBabel</a>.
</p>

<h2> Parley: učenje besednega zaklada</h2>

<div class="text-center">
<a href="/announcements/4/4.0/parley.png">
<img src="/announcements/4/4.0/parley_thumb.png" class="img-fluid">
</a> <br/>
<em>Naučite se tujih jezikov s pomočjo Parley</em>
</div>
<br/>

<p>
Parley je program za učenje besednjaka tujih jezikov, oziroma katerega koli seznama besed ali
predmetov. Za pričetek učenja iz menija Datoteka izberite ukaz »Dobi nove besednjake«. Ko se
prenos zaključi, kliknite na gumb »Začni vadbo«. Odgovorite na vprašanje in kliknite
Preveri, da vidite, ali ste imeli prav. Ko ustavite vadbo, vam bo Parley pokazal, kako
dobro vam je šlo. Na meniju Vadba, lahko najdete možnost Statistika, ki prikaže podatke
o vašem napredku.
</p>

<h2> Marble: namizni globus</h2>

<div class="text-center">
<a href="/announcements/4/4.0/marble.png">
<img src="/announcements/4/4.0/marble_thumb.png" class="img-fluid">
</a> <br/>
<em>Namizni globus Marble</em>
</div>
<br/>

<p>
Marble je program, ki vam prikazuje 3D karto sveta, podobno kot globus. Karto
lahko vrtite, se ji približate, ali se od nje oddaljite. Povečavo lahko nastavljate
s koleščkom na miški. Kliknite na mesto in v informacijskem oknu pojdite na
zavihek Wikipedija, kjer lahko vidite dodatne podatke o tem mestu, vključno s
slikami in povezavami do dodatnih informacij. Če na levi strani kliknete zavihek
»Videz karte«, lahko svet vidite predstavljen na druge načine. Med njimi sta
ravna karta in pa Zemlja ponoči. Če miškin kazalec podržite nad vrsto karte,
boste videli njen opis. Dodatne teme lahko dobite z izbiro ukaza »Prenesi nove
podatke« iz menija Datoteka.
</p>

<h2> Blinken: urjenje spomina</h2>

<div class="text-center">
<a href="/announcements/4/4.0/blinken.png">
<img src="/announcements/4/4.0/blinken_thumb.png" class="img-fluid">
</a> <br/>
<em>Blinken za vadbo spomina</em>
</div>
<br/>

<p>
Blinken je programček za urjenje spomina. Za začetek treninga kliknite ba gumb Start. Nato izberite
težavnostno stopnjo in počakajte da Blinken začne. Osvetlil se bo en izmed štirih obarvanih delov.
Kliknite na ta del. Čez nekaj časa se bosta osvetlila dva dela zapovrstjo. Ponovno kliknite nanju,
v istem vrstnem redu. Poskusite priti čim dlje.
</p>

<h2>KStars: namizni planetarij</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kstars.png">
<img src="/announcements/4/4.0/kstars_thumb.png" class="img-fluid">
</a> <br/>
<em>Oglejte si nočno nebo s KStars</em>
</div>
<br/>

<p>
S programom KStars lahko vidite, kaj je na nebu ponoči. Uporabite ga lahko tudi za
krmiljenje daljnogledov, tudi preko interneta. Ko prvič zaženete KStars, boste morali
vnesti svojo lokacijo. Odtlej bo KStars prikazoval nočno nebo, kot se dejansko vidi
s te lokacije. Karto nočnega neba lahko premikate, tako da jo zagrabite z levim gumbom
miške in jo premaknete. Povečavo spreminjate s koleščkom na miški. Da bi videli
podatke o nebesnem telesu, nanj kliknite z desnim gumbom miške. Če iz menija Orodja izberete
»Kaj je na nebu«, dobite seznam nebesnih teles, ki so vidni z vaše lokacije. Sedaj lahko
izberete objekt in kliknete gumb »Usredišči objekt«, da se to telo premakne v sredino
zaslona. Kliknite na »Podrobnosti o objektu«, da vidite podrobne podatke o planetu,
kometu ali drugem objektu, ki ga opazujete. Na zavihku Položaj lahko vidite natančne
koordinate objekta. Svoja opazovanja lahko beležite na zavihku Dnevnik. Če imate
ustrezen teleskop z digitalno povezavo, lahko iz menija Naprave zaženete »Čarovnika
za daljnogled« in nato upravljate z daljnogledom kar iz KStars. Da izberete določen
datum in čas za prikaz zvezdne karte, se poslužite menija Čas. Z gumbi v orodjarni
lahko pospešite, upočasnite ali ustavite uro.
</p>

<h2>KTouch: učenje tipkanja</h2>

<div class="text-center">
<a href="/announcements/4/4.0/ktouch.png">
<img src="/announcements/4/4.0/ktouch_thumb.png" class="img-fluid">
</a> <br/>
<em>Vadite slepo tipkanje s KTouch</em>
</div>
<br/>

<p>
KTouch vam pomaga pri učenju slepega tipkanja. Ko začnete vajo, morate pravilno
prepisati besedilo iz zgornjega belega pasu. Slika tipkovnice vam kaže, kje se nahaja
tipka, ki jo morate pritisniti naslednjo. Če se zmotite, se pas, v katerega tipkate,
obarva rdeče, dokler ne odpravite napake. KTouch vam prikazuje hitrost in pravilnost
v glavnem oknu. Na voljo je tudi podrobnejša statistika.
</p>
<p>
Zgoraj so omenjeni le nekateri programi iz kolekcije projekta KDE Education. Mnogo
več jih je na voljo na <a href="http://edu.kde.org/">spletni strani projekta</a>.
</p>

<table width="100%">
	<tr>
		<td width="50%">
		<a href="../applications">
				<img src="/announcements/4/4.0/images/applications-32.png" />
				Predhodna stran: Osnovni programi
				</a>		
		</td>
		<td align="right" width="50%">
        <a href="../games">Naslednja stran: Igre
				<img src="/announcements/4/4.0/images/games-32.png" /></a>
		</td>
	</tr>
</table>
