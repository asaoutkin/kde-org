---
title: "KDE 4.0 Visual Guide: Desktop"
hidden: true
---

<p>FOR IMMEDIATE RELEASE</p>

<h2>Plasma</h2>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>The KDE 4.0 desktop</em>
</div>
<br/>

<p>
Plasma is KDE's new desktop shell which provides new tools to start applications, 
presents the main KDE user interface, and offers new ways of interacting with your 
desktop.
</p>
<p>
Plasma's dashboard view replaces the old "Show Desktop" feature. Enabling the 
dashboard hides all windows and puts the widgets in front of them. Press 
CONTROL+F12 to show the dashboard view, for a quick glance at your plasmoids, read 
the desktop notes, check for new RSS feeds, have a look at the current weather 
situation, all that can be done with Plasma.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/dashboard.png">
<img src="/announcements/4/4.0/dashboard_thumb.png" class="img-fluid">
</a> <br/>
<em>Plasma's dashboard</em>
</div>
<br/>

<h3>Launch applications, search and open webpages with KRunner</h3>
<p>
<strong>KRunner</strong> lets you quickly start applications. Press ALT+F2 to 
show the KRunner dialog. You can then start typing. While typing, KRunner shows 
options that match what you type:

<div class="text-center">
<a href="/announcements/4/4.0/krunner-desktop.png">
<img src="/announcements/4/4.0/krunner-desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Starting applications with KRunner</em>
</div>
<br/>

<ul>
  <li>
    Type the name of an application, and KRunner finds applications matching your 
query. Choose the application from the list shown below the text input line, or 
just hit enter to launch the top-most action which is also marked as "Default". For a 
quick look at processes that are currently being active, just click on the 
"ShowSystem Activity" button, or press ALT+S. For a graphical view, start 
KSysguard.
  </li>
  <li>
    KRunner also acts as a simple pocket calculator. Just type a mathematical 
equation in the form of "=1337*2" and KRunner instantly shows the result.

<div class="text-center">
<a href="/announcements/4/4.0/krunner-calculator.png">
<img src="/announcements/4/4.0/krunner-calculator.png" class="img-fluid">
</a> <br/>
<em>KRunner used as calculator</em>
</div>
<br/>

  </li>
  <li>
    Open your bookmarks in a webbrowser. Bookmarks can be accessed via 
shortcuts. Type "gg:kde4 visual guide" to search Google for "kde4 visual guide". 
Type "wp:kde" to open Wikipedia's page about KDE. You will find that a wealth of
preconfigured shortcuts offer a very easy way to interact with the web. A 
longer list of webshortcuts that are accessible from KRunner (and of course also 
from Konqueror, KDE's webbrowser) can be found in Konqueror's settings. Open 
Konqueror, choose "Settings" from the menu. "Configure Konqueror" opens the 
settings dialog, and webshortcuts can be found in the bar on the left of this dialog.
  </li>
</ul>
</p>

<h3>KickOff</h3>
<p>
KickOff is the new KDE application launcher, or "start menu". 
Click on the KDE logo located in the bottom left of your screen. The KickOff 
menu opens, providing easy access to installed applications, recently used files
and applications and more. Via the Leave tab, you can logout, shutdown and suspend
your computer. The design philosophy of KickOff is that frequently used applications 
and search are quicker to access than navigating a hierarchical tree every time the 
menu is used.

<div class="text-center">
<a href="/announcements/4/4.0/kickoff-favorites.png">
<img src="/announcements/4/4.0/kickoff-favorites_thumb.png" class="img-fluid">
</a> <br/>
<em>Launch your favorite applications easily with KickOff</em>
</div>
<br/>

<ul>
  <li>
    Your <strong>Favorites</strong>. There are a number of applications or 
	documents you need to access very often, therefore they are shown first. You can 
	easily add or remove items from your Favorites menu by right clicking on them, 
	and then either choosing "Add to Favorites" or "Remove from Favorites".
  </li>
  <li>
    The <strong>Applications</strong> tab shows a categorized list of 
	applications. Move your mouse over the buttons at KickOff's bottom, and within the 
	blink of an eye, KickOff switches to the Applications view. Browse through the 
	menu to find out what tools are installed on your system. You can always get one 
	level to the left by clicking the large button on the side. Hint: You can just 
	move the mouse cursor to the left until you hit the screen edge. This makes 
	it much easier to hit.
  </li>
  <li>
	The <strong>Computer</strong> tab gives you access to your storage media, such as
	harddrives or removable media such as USB sticks. It also knows about your
	favorite Places and has a button to open System Settings.
  </li>
  <li>
	The <strong>Recently Used</strong> tab shows applications and documents that you 
	have used before. From there it is easy to pick up the documents and applications
	you've worked with before.
  </li>
  <li>
	The <strong>Leave</strong> tab offers you to shutdown your computer, and log out.
	If you want to suspend or hibernate your machine, press shutdown and then keep the
	"Turn off" button from the log out menu pressed. You then get the options
	to suspend and hibernate if your system supports it.
  </li>
</ul>
A traditional (KDE3-style) menu is available for those who prefer it.
</p>

<h3>The Panel</h3>

<div class="text-center">
<a href="/announcements/4/4.0/panel.png">
<img src="/announcements/4/4.0/panel_thumb.png" class="img-fluid">
</a> <br/>
<em>The Panel holds KickOff, the taskbar, pager, clock, systray and more applets</em>
</div>
<br/>

<p>
If you are looking for a specific tool, click on the K Button in the lower left
corner to open Kickoff and then type what you're looking for. 
There is an automatic filter which takes a list of applications and attempts to find those that match 
your criteria. So entering "cd" finds applications to play an audio CD, burn a CD 
or DVD, or encode an audio CD. Likewise, typing "viewer" gives you a list of 
aplications that can be used to open and view all sorts of file formats.
</p>
<p>
The Plasma <strong>Panel</strong> holds the menu, the systray and a list of 
tasks. The taskbar, located in the panel, can show live thumbnails of windows that 
are currently hidden, the Taskbar Thumbnails.
<br />
</p>
<p>
On the panel, you can also find the Pager applet. Use this applet to navigate between
your workspaces, also called "virtual desktops". By right clicking on the Pager, you
can configure the number and arrangement of those workspaces. If you have the Desktop
Effects enabled, press CTRL+F8 to show a full-screen overview of your workspaces.
</p>

<p>
Hint: To open KickOff, you can 'blindly' move the mouse cursor to the 
bottom-left corner and hit the left mouse button. As KickOff's button also covers 
the screen edges, it is extremely easy to access.
</p>
<p>
If you want to learn more about Plasma, check out the 
<a href="http://techbase.kde.org/Projects/Plasma/FAQ">Plasma FAQ</a> on TechBase.
</p>

<h2>KWin - The KDE window manager</h2>
<p>
KWin, KDE's proven to-be-stable window manager has been enhanced to use the 
capabilities of modern graphics hardware, easing interaction with your windows. 
Right clicking on the top bar of a window gets you, via "Configure Window 
Behavior", to the Window Manager settings. Here you can learn and configure the 
advanced features of KWin. In the Action configuration sheet, you can, for example
choose to maximize the window when you double-click the window decoration.
</p>
<p>
Hint: KWin lets you easily move windows by pressing the ALT button. You can then 
just click on a window's content. While you hold the left mouse button pressed, 
windows will move. Hold ALT pressed and click on the right mouse button to easily
resize a window. No need to aim precisely, just keep ALT pressed and manipulate your 
windows easily!
</p>

<h3>Desktop Effects</h3>
<p>
Enable the Desktop Effects to see new ways KWin offers for interacting with 
your windows. Right-click on the window decoration, choose "Configure Window 
Behavior" and go to the "Desktop Effects" sheet. Check "Enable desktop effects" 
and accept the new settings by clicking "OK", "Apply" or just hitting the Enter key. 
Now KWin's advanced window handling is enabled.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kwin-presentwindows.png">
<img src="/announcements/4/4.0/kwin-presentwindows_thumb.png" class="img-fluid">
</a> <br/>
<em>Switch applications using the Present Windows effect</em>
</div>
<br/>

<p>
    <strong>Present Windows</strong> gives you an overview of your open windows. 
Push the mouse cursor to the top-left corner of the screen and press to arrange 
your windows side-by-side. You can click on a window to get it focused in front of 
you. Start typing a word from the label on a window and matching 
applicatons will be filtered. Hit Enter to switch to the chosen application. You 
can activate the Present Windows effect also with the keyboard through CONTROL+F9 or 
CONTROL+F10 to show windows from all virtual desktops.
</p>
<p>
Hint: With the Present Windows effect enabled, just start typing a word from the title
of the application you want to choose and the grid of applications gets filtered. Just hit
enter once you've picked the application you want and it will be zoomed in.
</p>
<p>
The <strong>Desktop Grid</strong> zooms your desktop out to show a grid of 
your virtual desktops or workspaces. Drag windows between the virtual desktops to move them 
between your workspaces. Click on one of the virtual desktops to zoom into the 
workspace. 
</p>
<p>
Hint: You can press the number of a virtual desktop to switch to that 
workspace. Enable the Desktop Grid with <em>CONTROL+F8</em>. <br />
Click with the right mouse button on a window to show it on all desktops.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktopgrid.png">
<img src="/announcements/4/4.0/desktopgrid_thumb.png" class="img-fluid">
</a> <br/>
<em>The Desktopgrid Effect offers the same functionality as the Pager</em>
</div>
<br/>

<p> 
The Panel holds an applet that provides similar functionality and is also 
available when Desktop Effects have been disabled. Right click on the "Pager"in 
the panel to configure the number and arrangement of your virtual desktops. Drag 
the pager applet from the appletbrowser to the panel or to the desktop. The 
screenshots illustrates this nicely.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/pager.png">
<img src="/announcements/4/4.0/pager.png" class="img-fluid">
</a> <br/>
<em>The Pager allows you to switch between your workspaces</em>
</div>
<br/>

<p>
    The effect <strong>Taskbar Thumbnails</strong> enables live previews of windows when you 
move the mouse over their entry in the taskbar. This makes for easy monitoring of 
activity in hidden windows and applicaton activity. Taskbar Thumbnails also provide 
visual hints to ease locating the application you want to switch to.

<div class="text-center">
<a href="/announcements/4/4.0/kwin-taskbarthumbnails.png">
<img src="/announcements/4/4.0/kwin-taskbarthumbnails_thumb.png" class="img-fluid">
</a> <br/>
<em>Task-switching enhanced with live thumbnails</em>
</div>
<br/>

</p>
<p>
The desktop effects, or more technically, the compositing features of KWin 
enable <strong>transparency effects</strong> in various applications. As an example, 
Konsole, KDE's terminal emulator, can use a transparent background, so applications 
lying behind it can still be seen. It also provides a pleasurable visual effect while you work.
You can also change the opacity of a window by right clicking on the window 
decoration and choosing an Opacity level.</p>

<div class="text-center">
<a href="/announcements/4/4.0/kwin-transparency.png">
<img src="/announcements/4/4.0/kwin-transparency_thumb.png" class="img-fluid">
</a> <br/>
<em>Change the opacity of individual windows and applications</em>
</div>
<br/>

<p>
In the "All Effects" sheet, you are provided with more detailed control over 
various effects. Often, you will find those effects to be configurable so you can 
fit how they work better to your personal taste. KWin tries to enable the effect 
automatically, based on the capabilities of your graphics card, so it is likely 
that the effects may be already enabled on your system.<br /><br />
If you want to find out more about KWin, especially its compositing features,
refer to 
<a href="http://techbase.kde.org/Projects/KWin/4.0-release-notes">KWin's 
release notes</a> on <a href="http://techbase.kde.org/Projects/KWin">its TechBase pages</a>.
</p>

<h2>Sweet Spots</h2>
<p>
KDE makes extensive use of the 'sweet spots' of the screen -- edges and corners, 
which are easier to aim at, make reaching the buttons of the window decorations 
more convenient. Slap the mouse to the top-right corner, click, and the window is being 
closed. The "Close Window" button has some space left of it, so you don't hit it 
accidentally when maximizing a window. The window button ordering can easily 
be changed to comfort your style of working. Right click on the decoration, the 
top frame of the window, choose "Configure Window Behavior".
<br />
Hint: To grab a scrollbar, the easiest way is to move the mouse to the edge of 
the screen, this way aiming and grabbing the scrollbar can be done quickly and 
conveniently.
</p>
<p>

<table width="100%">
	<tr>
		<td width="50%">
				<a href="../guide">
				<img src="/announcements/4/4.0/images/star-32.png" />
				Overview
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="../applications">Next page: Basic applications
				<img src="/announcements/4/4.0/images/applications-32.png" /></a>
		</td>
	</tr>
</table>
