---
aliases:
- ../announce-4.4-beta2
date: '2009-12-21'
description: KDE Community Ships Second Preview for KDE SC 4.4 Series.
title: Software Compilation 4.4 Beta 2 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Software Compilation 4.4-beta2 Out Now: Codename "Claus"
</h3>

<p align="justify">
  <strong>
KDE Ships Second Test Version of New 4.4 Desktop, Applications and Development Platform
</strong>
</p>

<p align="justify">
 Today, KDE has released a second preview the KDE Software Compilation (KDE SC), 4.4 Beta 2. <a href="/announcements/4/4.4-beta2">The second beta version of KDE SC 4.4</a> provides a preview and base for helping to stabilize the next version of the KDE Plasma Workspaces, Applications and Development Platform.<br />
The list of changes this time around is especially long. Important changes can be observed all over the place:
<p>
<ul>
    <li>The <strong>Nepomuk Semantic Search</strong> framework has made leaps: A new storage backend makes it a lot faster, new user interfaces to interact with the Nepomuk database are first delivered with KDE 4.4.0 and a timeline view of your files makes finding files used in the past easier.</li>
    <li>The <strong>Plasma Desktop</strong> has been further polished. Many user interface elements have received attention by developers and designers. The new widget explorer provides a richer experience for managing desktop widgets. Plasma widgets can now be shared with other users over the network and the handling of storage devices in the desktop shell has been streamlined. Also, in 4.4 Plasma's little sibling, the Netbook shell debuts as a technology preview. </li>
    <li><strong>New applications</strong> on the horizon range from Blogilo, a rich-client blogging tool to <a href="http://edu.kde.org/cantor/">Cantor</a> and <a href="http://edu.kde.org/rocs/">Rocs</a>, two scientific applications for advanced math and graph theory needs. Many other applications, such as the Gwenview image viewer and the Dolpin file manager have been further improved.</li>
    <li>The KDE <strong>Development Platform</strong> adds the new KAuth authorization framework for easy and secure privilege escalation, printing of odd and even pages, scanner support for the Windows platform and the first pieces of integration of the popular webkit rendering engine.</li>
</ul>
These are only some of the changes one can expect from the new KDE Software Compilation 4.4, there is also a longer <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Release_Goals">list of the changes</a>.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4-beta1/dolphin-systemsettings.png">
	<img src="/announcements/4/4.4-beta1/dolphin-systemsettings_thumb.png" class="img-fluid" alt="KDE SC 4.4 Beta 2">
	</a> <br/>
	<em>Dolphin and System Settings in 4.4</em>
</div>
<br/>

<p align="justify">
<p />
To find out more about the KDE 4.4 desktop and applications, please also refer to the
<a href="/announcements/4.3/">KDE 4.3.0</a>,
<a href="/announcements/4.2/">KDE 4.2.0</a>,
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes, as KDE SC 4.4 is the next evolutionary step on top of the releases.
<strong>KDE SC 4.4 Beta 2 is not stable software, as such, it is not suitable for everyday or production use.</strong> KDE SC 4.4 Beta 2 is aimed at testers and those who would like to have an early look at the next KDE SC release.
<p />

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.3.85/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4-beta1/systemtray-settings.png">
	<img src="/announcements/4/4.4-beta1/systemtray-settings_thumb.png" class="img-fluid" alt="KDE SC 4.4 Beta 2">
	</a> <br/>
	<em>Improved System Tray 4.4</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.4-beta1/blogilo.png">
	<img src="/announcements/4/4.4-beta1/blogilo_thumb.png" class="img-fluid" alt="KDE SC 4.4 Beta 2">
	</a> <br/>
	<em>New in 4.4: Blogilo Blogging Client</em>
</div>
<br/>

<h4>
  Installing KDE SC 4.4 Beta 2 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.4 Beta 2
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/unstable/4.3.85/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.3.85">KDE SC 4.4 Beta 2 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.4 Beta 2 (a.k.a 4.3.85)
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE SC 4.4 Beta 2 may be <a
href="http://download.kde.org/unstable/4.3.85/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.4 Beta 2
  are available from the <a href="/info/4.3.85#binary">KDE SC 4.3.85 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}