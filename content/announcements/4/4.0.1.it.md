---
aliases:
- ../announce-4.0.1
date: '2008-02-05'
title: KDE 4.0.1 Release Announcement
---

<h3 align="center">
   Il progetto KDE rilascia la prima versione di servizio ed internazionalizzazione per la serie 4.0 dell'ambiente Desktop libero più popolare
</h3>

<p align="justify">
	La comunità KDE rilascia la prima versione di servizio ed internazionalizzazione del Desktop libero per utenti, aziende e pubblica amministrazione
</p>

<p align="justify">
 Il <a href="http://www.kde.org/">Progetto KDE</a> ha oggi annunciato la disponibilità immediata della versione 4.0.1 del Desktop KDE, la prima versione di manutenzione del più avanzato e potente ambiente desktop libero per GNU/Linux e altri UNIX. KDE 4.0.1 viene rilasciato con i pacchetti di base, amministrazione, educazione, utilità, multimedia, giochi, grafica, sviluppo web e altro. I riconosciuti applicativi di KDE sono disponibili approssimativamente in 50 lingue.
</p>

<p align="justify">
 KDE, incluse le librerie e le applicazioni, è disponibile liberamente sotto i termini di licenze Open Source. KDE può essere scaricato come codice sorgente e in numerosi formati binari da  <a href="http://download.kde.org/stable/4.0.1/">http://download.kde.org</a> e può anche essere ottenuto su <a href="http://www.kde.org/download/cdrom">CD-ROM</a> alternativamente alle molte <a href="http://www.kde.org/download/distributions">distribuzioni GNU/Linux e UNIX</a> disponibili.
</p>

<h4>
  <a id="changes">Miglioramenti</a>
</h4>

<p align="justify"> 
 KDE 4.0.1 è una versione di servizio che include molte migliorie e correzioni ad alcuni problemi che sono stati riconosciuti con l'aiuto del <a href="http://bugs.kde.org/">KDE bug tracking system</a>. Sono state migliorate le traduzioni esistenti e ne sono state aggiunte di nuove. Tra i miglioramenti apportati possiamo sottolineare del lavoro sulla stabilità di KHTML, il motore di rendering per il web di KDE. Migliorie e piccole funzionalità sono state aggiunte a Plasma, il gestore del Desktop di KDE 4. KWin, il gestore delle finestre, è stato migliorato su alcuni effetti grafici quando attivati. Di pari passo a questi miglioramenti sui pacchetti di base, altro lavoro è stato fatto su applicazioni come Okular, Impostazioni di Sistema e Kstars. Nuove traduzioni come Danese, Frisone, Ceco e Kazako.
 </p>

<p align="justify">
 Per una lista più dettagliata dei miglioramenti dal rilascio di KDE 4.0 del Gennaio 2008 fare riferimento al <a href="/announcements/changelogs/changelog4_0to4_0_1">KDE 4.0.1 Changelog</a>.
</p>

<p align="justify">
 Informazioni aggiuntive a proposito dei miglioramenti che avverranno nella serie dei rilasci 4.0.x possono essere trovate nell'<a href="../4.0/">Annuncio di KDE 4.0</a>.
</p>
<h4>
  Installare KDE 4.0.1 con in pacchetti binari
</h4>
<p align="justify">
  <em>Fornitori di Pacchetti</em>.
  Alcuni fornitori di distribuzioni Linux/UNIX hanno gentilmente messo a disposizione i pacchetti binari per KDE 4.0.1, in altri casi dei volontari hanno provveduto a creare dei pacchetti. Alcuni dei pacchetti disponibili sono liberamente scaricabili da  <a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.1/">http://download.kde.org</a>. Altri pacchetti binari, come aggiornamenti ai pacchetti già disponibili, possono essere rilasciati nelle prossime settimane.
</p>

<p align="justify">
  <a id="package_locations"><em>Dove trovare i pacchetti</em></a>.
  Per una lista aggiornata dei pacchetti disponibili dei quali il progetti KDE è stato informato, visitare la <a href="/info/4.0.1">KDE 4.0.1 Info Page</a>.
</p>

<h4>
  Compilare KDE 4.0.1
</h4>
<p align="justify">
  <a id="source_code"></a><em>Codice Sorgente</em>.
  Il codice sorgente completo di KDE 4.0.1 può essere <a href="http://download.kde.org/stable/4.0.1/src/">liberamente scaricato</a>. Istruzioni su come compilare e installare KDE 4.0.1 sono disponibili nella <a href="/info/4.0.1#binary">KDE 4.0.1 Info Page</a>.
</p>

<h4>
 Supporta KDE
</h4>
<p align="justify">
 KDE è un progetto <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> che esiste e cresce solamente grazie all'aiuto di molti volontari che donano il loro tempo e impegno. KDE è sempre alla ricerca di nuovi volontari contribuenti che possano dare una mano in settori come la programmazione, la ricerca di bachi, la scrittura della documentazione, la traduzione, la promozione, con donazioni, etc. Tutti i contributi sono apprezzati e accettati. Per favore leggi la pagina<a href="/community/donations/">Supporting KDE</a> per informazioni. </p>

<p align="justify">
Non vediamo l'ora di avere tue notizie!
</p>

<h2>A proposito di KDE 4</h2>
<p>
KDE 4.0 è un innovativo desktop Free Software che contiene molte applicazioni per un utilizzo quotidiano come anche per applicazioni specifiche. Plasma è un nuovo gestore per il desktop sviluppato per KDE 4, fornisce una intuitiva interfaccia per l'interazione con il desktop e con le applicazioni. Konqueror, il web browser, integra il web con il Desktop. Dolphin, il gestore file, Okular, il visualizzatore dei documenti, e le Impostazioni di Sistema forniscono un desktop di base.
<br />
KDE è sviluppato sulle omonime librerie, che forniscono facilità di accesso alle risorse sulla rete con KIO e avanzate capacità grafiche grazie alle Qt4. Phonon e Solid, che sono ugualmente parte delle librerie di KDE, aggiungono una infrastruttura multimediale e una migliore integrazione dell'hardware con le applicazioni scritte per KDE.
</p>

<h4>A proposito di KDE</h4>
<p align="justify">
KDE è costituito da una squadra internazionale dedicata allo sviluppo di software libero e Open Source per il Desktop. Tra i prodotti della squadra KDE ci sono un moderno Desktop per Linux e UNIX, una completa suite da Ufficio, tutti gli strumenti per la collaborazione in team, centinaia di applicazioni in varie categorie, tra le quali Internet, multimedia, intrattenimento, educazione, grafica e sviluppo software. I software KDE sono tradotti in più di 60 lingue e pensato per la facilità d'uso e una moderna accessibilità. Le applicazioni per KDE 4 possono essere nativamente eseguite su Linux, BSD, Solaris, Windows e Mac OS X.
</p>


{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
