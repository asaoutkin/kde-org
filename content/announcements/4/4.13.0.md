---
aliases:
- ../4.13
date: '2014-04-16'
description: KDE Ships Applications and Platform 4.13.
title: KDE Software Compilation 4.13
---

{{<figure src="/announcements/4/4.13.0/screenshots/plasma-4.13.png" class="text-center" width="600px">}}

April 16, 2014

The KDE Community proudly announces the latest major updates to KDE Applications delivering new features and fixes. Major improvements are made to KDE's Semantic Search technology, benefiting many applications. With Plasma Workspaces and the KDE Development Platform frozen and receiving only long term support, those teams are focusing on the transition to Frameworks 5. This release is translated into 53 languages; more languages are expected to be added in subsequent monthly minor bugfix releases.<br />

## <a href="./applications"><img src="/announcements/4/4.13.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.13"/> KDE Applications 4.13 Benefit From The New Semantic Search, Introduce New Features</a>

The latest major updates to the KDE Applications are delivering new features and fixes. Kontact (the personal information manager) has been the subject of intense activity, benefiting from the improvements to KDE's Semantic Search technology and bringing new features. Document viewer Okular and advanced text editor Kate have gotten interface-related and feature improvements. In the education and game areas, we introduce the new foreign speech trainer Artikulate; Marble (the desktop globe) gets support for Sun, Moon, planets, bicycle routing and nautical miles. Palapeli (the jigsaw puzzle application) has leaped to unprecedented new dimensions and capabilities. <a href='./applications'>read the announcement</a>.

## <img src="/announcements/4/4.13.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.13"/> KDE Development Platform 4.13 Introduces Improved Semantic Search

The KDE Development Platform libraries are frozen and receive only bugfixes and minor improvements. The upgrade in the version number for the Development Platform is only for packaging convenience. All bug fixes and minor features developed since the release of Applications and Development Platform 4.11 have been included. The only major change in this release is the introduction of an improved Semantic Search, which brings better performance and reliability to searching on the Linux Desktop.

Development of the next generation KDE Development Platform—called KDE Frameworks 5—is in beta stage. Read <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article</a> to find out what is coming and <a href='https://www.kde.org/announcements/'>see here</a> for the latest announcements.

### Improved Semantic Search

The major new addition to the KDE Development Platform is the <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>next generation Semantic Search</a>. To maintain compatibility, this is included as a new component rather than a replacement for the previous Semantic Search. Applications need to be ported to the new search component; most KDE Applications have already been ported. Downstream distributions can decide whether or not to ship the deprecated Semantic Search alongside the new version.

The improvements to search bring significant benefits in terms of faster, more relevant results, greater stability, lower resource usage and less data storage. The upgrade requires a one-time database migration that will take a few minutes of increased processing power.

## Support KDE

<a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5"><img src="/announcements/4/4.13.0/images/join-the-game.png" width="231" height="120" alt="Join the Game" align="left" class="mr-3"/> </a>

KDE e.V.'s new <a href='https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5'>Supporting Member program</a> is
now open. For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.

&nbsp;

## Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.13 releases of KDE software.

You can discuss this release on <a href='http://dot.kde.org/2014/04/16/kde-releases-applications-and-development-platform-413'>our news site</a>.

## About these release announcements

These release announcements were prepared by the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past four months.
