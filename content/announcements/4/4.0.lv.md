---
aliases:
- ../4.0
- ../4.0.lv
date: '2008-01-11'
layout: single
title: KDE 4.0 izlaists
---

<h3 align="center">
   KDE projekts piedāvā ceturto nozīmīgo brīvās programmatūras darbvirsmas versiju
</h3>
<p align="justify">
  <strong>
    Ar šo ceturto nozīmīgo versiju KDE kopiena iezīmē KDE 4 ēras sākumu.
  </strong>
</p>

<p>
KDE kopiena ir priecīga paziņot tūlītēju <a href="/announcements/4.0/">KDE 4.0.0</a> pieejamību.
Šis nozīmīgais laidiens iezīmē ne tikai ilga un intensīva izstrādes cikla beigas, kas novedis
līdz KDE 4.0, bet arī jaunas KDE 4 ēras sākumu.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>KDE 4.0 darbvirsma</em>
</div>
<br/>

<p>
KDE 4 <strong>bibliotēkām</strong> ir veikti nozīmīgi uzlabojumi gandrīz visās jomās.
Phonon multimediju bibliotēka piedāvā platformu neatkarīgu multimediju atbalstu visām
KDE lietojumprogrammām, Solid aparatūras integrācijas bibliotēka ļauj mijiedarboties ar
(noņemamām) ierīcēm daudz vienkāršāk, kā arī piedāvā rīkus elektroenerģijas taupīšanai.
<p />
KDE 4 <strong>darbvirsmai</strong> ir nākušas klāt dažas jaunas un nozīmīgas iespējas.
Plasma darbvirsmas čaula piedāvā jaunu darbvirsmas saskarni, ieskaitot paneli, izvēlni
un logdaļas (widgets) uz darba virsmas, kā arī ciparvadības paneļa (dashboard) funkciju.
KWin - KDE logu pārvaldītājs tagad atbalsta daudz plašāku grafisko efektu klāstu, lai
atvieglotu darbu ar logiem.
<p />
Arī ļoti daudzām KDE <strong>lietojumprogrammām</strong> ir veikti dažādi uzlabojumi. Vizuāli
uzlabojumi, izmantojot vektorgrafikas attēlus, izmaiņas izmantojamajās bibliotēkās,
lietotāja saskarnes uzlabojumi, jaunas iespējas, pat jaunas aplikācijas - viss, ko vien
var iedomāties ir KDE 4.0. Okular, jaunais dokumentu skatītājs, un Dolphin, jaunais failu
pārvaldnieks, ir tikai divas lietojumprogrammas, kas pielieto KDE 4.0 jaunās tehnoloģijas.
<p />
Oxygen <strong>mākslas darbu</strong> komanda nodrošina darbvirsmu ar svaiga gaisa vēsmu.
Gandrīz visām lietotājam redzamajām KDE darbvirsmas daļām un lietojumprogrammām ir veikta
reorganizācija. Skaistums un saskaņa ir Oxygen divi galvenie pamatjēdzieni.
</p>

<h3>Darbvirsma</h3>
<ul>
	<li>Plasma ir jauna darbvirsmas čaula. Plasma piedāvā paneli, izvēlni un citus 
	  intuitīvus veidus kā strādāt ar darba virsmu un lietojumprogrammām.
	</li>
	<li>Kwin, KDE logu pārvaldītājs tagad atbalsta plašas vizuālas iespējas.
	  Aparatūras paātrināta grafikas attēlošana nodrošina vieglāku un vairāk intuitīvu
	  darbu ar logiem.
	</li>
	<li>Oxygen ir jaunais KDE 4.0 izskats. Oxygen nodrošina saskaņotu, viegli uztveramu
	    un skaistu lietotāja saskarni.
	</li>
</ul>
Apgūsti vairāk par jauno KDE darbvirsmas lietotāja saskarni <a href="./desktop">KDE 4.0 
vizuālajā ceļvedī</a>.

<h3>Lietojumprogrammas</h3>
<ul>
	<li>Konqueror ir sevi pierādījis KDE tīmekļa pārlūks. Konqueror ir mazs, labi integrēts,
	    kā arī atbalsta jaunākos tīmekļa standartus kā CSS 3.</li>
	<li>Dolphin ir KDE jaunais failu pārvaldnieks. Dolphin ir izstrādāts domājot par lietojamību
	        un ir viegli lietojams, bet spēcīgs rīks.
	</li>
	<li>Sistēmas iestatījumi, kontroles centra lietojumprogramma, ir piedzīvojusi jaunu lietotāja
	    saskarni. Izmantojot KSysGuard sistēmas pārraugu, ir vienkārši novērot un kontrolēt sistēmas
	    resursus un aktivitātes.
	</li>
	<li>Okular, KDE 4 dokumentu skatītājs, atbalsta daudz dažādus failu formātus. Okular ir viena
	    no daudzām KDE 4 lietojumprogrammām, kas ir uzlabota sadarbībā ar
	    <a href="http://openusability.org">OpenUsability</a> projektu.
	</li>
	<li>Izglītojošās lietojumprogrammas ir starp pirmajām, kas tika pārnestas un izstrādātas izmantojot
	    KDE 4 tehnoloģiju. Kalzium, grafiska ķīmisko elementu periodiskā tabula, un Marble pasaules globuss
	    ir tikai divi no daudzajiem dārgumiem starp izglītojošajām lietojumprogrammām. Uzzini vairāk par
	    izglītojošajām lietojumprogrammām <a href="./education">vizuālajā ceļvedī</a>.
	</li>
	<li>Ir uzlabotas daudzas KDE spēles, kā piemēram, KMines, mīnu meklēšanas spēle, un KPat, pasjansa spēle.
	    Pateicoties vektorgrafikai un grafikas spējām, spēles ir padarītas vairāk neatkarīgas no ekrāna
	    izšķirtspējas.
	</li>
</ul>
Dažas lietojumprogrammas ir plašāk aprakstītas <a href="./applications">KDE 4.0 vizuālajā ceļvedī</a>.

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/4/4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>failu pārvaldnieks, sistēmas iestatījumi un izvēlne darbībā</em>
</div>
<br/>

<h3>Bibliotēkas</h3>
<p>
<ul>
	<li>Phonon piedāvā lietojumprogrammām multimediju iespējas, kā audio un video atskaņošanu.
	    Iekšēji Phonon izmanto dažādas bibliotēkas, kuras ir iespējams mainīt. 
		Pēc noklusējuma KDE 4.0 izmantos Xine bibliotēku, kas piedāvā izcilu atbalstu dažādiem
		formātiem. Phonon arī ļauj lietotājam izvēlēties izejas iekārtas atkarībā no multimedija
		veida.
	</li>
	<li>Solid aparatūras integrācijas bibliotēka integrē gan nenoņemamās, gan noņemamās iekārtas
	    KDE lietojumprogrammās. Tāpat Solid koordinē sistēmas enerģijas pārvaldības iespējas,
	    vada tīkla savienojumus un Bluetooth iekārtu integrāciju. Iekšēji Solid kompinē HAL
	        NetworkManager un Bluez bluetooth steku, bet šīs komponentes ir iespējams aizvietot,
		bez lietojumprogrammatūras izmaiņām, lai piedāvātu maksimālu pārnesamību.
	</li>
	<li>KHTML ir tīmekļa lapu renderēšanas dzinējs, ko izmanto Konqueror, KDE tīmekļa pārlūks. KHTML ir mazs,
	    labi integrēts, kā arī atbalsta jaunākos tīmekļa standartus kā CSS 3. KHTML bija pirmais dzinējs,
	    kas izgāja slaveno Acid 2 testu.
	</li>
	<li>ThreadWeaver bibliotēka, kas nāk kopā ar kdelibs, piedāvā augsta līmeņa saskarni, lai efektīvāk izmantotu
	    vairākkodolu sistēmas, kā rezultātā KDE lietojumprogrammas darbojas vienveidīgāk un labāk izmanto pieejamos
	    sistēmas resursus.
	</li>
	<li>Tā kā KDE 4.0 ir bāzēts uz Trolltech Qt 4 bibliotēkas, tad ir iespējams izmantot uzlabotas vizuālās iespējas
	    un mazāku atmiņas apjomu. Kdelibs nodrošina ievērojamu Qt bibliotēku paplašinājumu, piedāvājot plašu augsta
	        līmeņa funkcionalitāti un ērtumu izstrādātājam.
	</li>
</ul>
</p>
<p>KDE <a href="http://techbase.kde.org">TechBase</a> ir zināšanu krātuve, kur ir iespējams atrast plašāku informāciju
par KDE bibliotēkām.</p>

<h4>Apskaties vizuālo ceļvedi...</h4>
<p>
<a href="./guide">KDE 4.0 vizuālais ceļvedis</a> nodrošina ātru, dažādu jauno un uzlaboto
KDE 4.0 tehnoloģiju, apskatu. Ilustrēts ar daudziem ekrānattēliem, tas Jums palīdzes iepazīt
dažādas KDE 4.0 daļas un parādīs dažas no jaunajām un aizraujošajām tehnoloģijām un uzlabojumiem.
Ir iespējams uzzināt par jaunajām <a href="./desktop">darbvirsmas</a> iespējam, kā arī par
<a href="./applications">lietojumprogrammām</a> kā sistēmas iestatījumi, Okular, dokumentu skatītājs,
un Dolphin, failu pārvaldnieks. Ir iespējams uzzināt kā par 
<a href="./education">izglītojošajām lietojumprogrammām</a>, tā arī par 
<a href="./games">spēlēm</a>.
</p>

<h4>Izmēģini...</h4>
<p>
Tiem, kas ir ieinteresēti izmēģināt un darboties līdzi, vairāki distributīvi ir informējuši
par pieejamām vai drīzumā pieejamām KDE 4.0 pakām. Pilns pašreizējais saraksts ir atrodams
<a href="http://www.kde.org/info/4.0">KDE 4.0 informācijas lapā</a>, kur ir iespējams atrast arī
norādes uz pirmkodu, informāciju par pirmkoda kompilēšanu, drošību un citiem jautājumiem.
</p>
<p>
Sekojoši distributīvi ir informējuši par KDE 4.0 paku vai Live CD pieejamību:

<ul>
	<li>
		Ir paredzēta KDE4-bāzēta distributīva <strong>Arklinux 2008.1</strong> testa
		versijas iznākšana tuvākajā laikā. Stabilo versiju paredzēts izlaist 3 vai 4 nedēļu laikā.
	</li>
	<li>
		<strong>Fedora</strong> piedāvās KDE 4.0 Fedora 9 versijā, kas <a
		href="http://fedoraproject.org/wiki/Releases/9">tiks izlaista</a>
		šī gada aprīlī, ar pieejamām testa versijām sākot no 24. janvāra.
		KDE 4.0 pakas ir pieejamas pirms-alfa <a
		href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> repozitorijā.
	</li>
	<li>
		<strong>Gentoo Linux</strong> piedāvā KDE 4.0 vietnē
		<a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
	</li>
	<li>
		<strong>Mandriva</strong> nodrošinās pakas versijai 2008.0, kā arī tiks
		veidots Live CD, kas būs bāzēts uz 2008.1 versijas.
	</li>
	<li>
		<strong>Ubuntu</strong> pakas ir iekļautas drīzumā iznākošajā "Hardy Heron"
		(8.04), kā arī ir piedāvā modernizāciju stabilajai "Gutsy Gibbon" (7.10) versijai.
		Lai izmēģinātu KDE 4.0, ir pieejams Live CD.
		Vairāk informācijas var atrast <a href="http://kubuntu.org/announcements/kde-4.0">
		paziņojumā</a> Ubuntu.org vietnē.
	</li>
	<li>
		<strong>openSUSE</strong> pakas <a href="http://en.opensuse.org/KDE4">ir pieejamas</a> 
		openSUSE 10.3 (
		<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">viena klišķa
		instalēšana</a>) un openSUSE 10.2 versijām. Ir arī pieejama <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
		Four Live CD</a> versija. KDE 4.0 tiks iekļauts arī drīzumā iznākošajā openSUSE 11.0 versijā.
	</li>
</ul>
</p>

<h2>Par KDE 4</h2>
<p>
KDE 4.0 ir novatoriska brīvās programmatūras darbvirsma, kas sevī iekļauj daudzas lietojumprogrammas,
kā ikdienas izmantošanai, tā arī specifiskiem gadījumiem. Plasma ir jauna darbvirsmas čaula, kas ir
izstrādāta tieši priekš KDE 4 un piedāvā intuitīvu lietotāja saskarni, lai strādātu ar darbvirsmu un lietojumprogrammām.
Konqueror tīmekļa pārlūks integrē darbvirsmu ar vispasaules tīmekli. Dolphin failu pārvaldnieks,
Okular dokumentu skatītājs un sistēmas uzstādījumu kontroles centrs veido darbvirsmas pamata komplektu.
<br />
KDE ir veidots balstoties uz KDE bibliotēkām, kas nodrošina vieglu piekļuvu tīkla resursiem, izmantojot KIO, un
modernas vizuālas iespējas, izmantojot Qt3. Phonon un Solid, kas arī ir daļa no KDE bibliotēkām nodrošina multimediju un
labāku aparatūras integrāciju visām KDE lietojumprogrammām.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
