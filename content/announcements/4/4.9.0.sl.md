---
aliases:
- ../4.9
- ../4.9.sl
date: '2012-12-05'
title: Izid programske zbirke KDE Software Compilation 4.9 – v spomin na Claire Lotion
---

<p>
<img src="/stuff/clipart/klogo-official-oxygen-128x128.png" class="float-left m-2" />KDE z veseljem najavlja najnovejši izid svoje zbirke programov, ki prinaša večje posodobitve <a href="./plasma">delovnih okolij KDE Plasma</a>, <a href="./applications">KDE-jevih programov</a> in <a href="./platform">KDE-jeve platforme</a>. Različica 4.9 prinaša več novih zmožnosti, izboljšano stabilnost in pohitritve.
</p>
<p>Ta izid je posvečen spominu na KDE-jevo prispevkarico <a href="http://dot.kde.org/2012/05/20/remembering-claire-lotion">Claire Lotion</a>. Njena živahnost in zavzetost sta navduševali mnoge v naši skupnosti. S svojim pionirskim delom na obliki, obsegu ter pogostosti srečanj KDE-jevih razvijalcev je spremenila izvajanje naše misije. S temi in drugimi dejavnostmi je na programski opremi, ki vam jo lahko podarimo danes, pustila pomemben pečat in iz srca se ji zahvaljujemo za ves njen trud.</p>
<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-desktop.png">
	<img src="/announcements/4/4.9.0/kde49-desktop-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>
<p>
Pred nekaj meseci se je zbrala KDE-jeva ekipa za kakovost, ki dela na izboljševanju stopnje kakovosti in stabilnosti KDE-jeve programske opreme. Posebno pozornost so namenili odkrivanju in odpravljanju napak, ki jih v prejšnjih različicah ni bilo. Odpravljanje teh napak je imelo prednost, ker to zagotavlja izboljšanje z vsakim izidom.
</p>
<p>
Ekipa je vzpostavila tudi zanesljivejši postopek preizkušanja izidov, ki se je pričel z beta različicami. Novi prostovoljni preizkuševalci so opravili trening in se udeležili več intenzivnih dnevov preizkušanja. Namesto bolj običajnega raziskovalnega preizkušanja so bili preizkuševalcem dodeljeni določeni deli, ki so bili glede na prejšnje različice spremenjeni. Za več kritičnih komponent so bili razviti in uporabljeni seznami za preverjanje. Ekipa je tako v zgodnji fazi odkrila nekaj pomembnih težav in sodelovala z razvijalci, ki so težave odpravili. Skupina je v razvojnih različicah odkrila in zabeležila več kot 160 napak. Večina od teh napak je odpravljenih. Pri preizkušanju razvojnih različic so pomagali tudi mnogi drugi uporabniki, ki niso člani uradne ekipe za kakovost. Vsa ta prizadevanja so pomembna, saj se tako razvijalci lahko osredotočijo na odpravljanje težav.
</p>
<p>
Rezultat vsega tega truda KDE-jeve ekipe za kakovost so najboljše izdaje KDE-jeve programske opreme do sedaj.
</p>
<p>
En prispevek si zasluži še posebno pozornost. Napaka v programu Okular je bila sporočena leta 2007 in prejela skoraj 1100 glasov. Uporabnikom ni bilo všeč, da svojih zabeležk v datotekah PDF niso mogli shraniti ali natisniti. S pomočjo komentatorjev in uporabnikov na kanalu IRC programa Okular je Fabio D’Urso izvedel rešitev, ki omogoča shranjevanje in tiskanje zabeležk. Prispevek je zahteval nekaj dela na KDE-jevih knjižnicah in pozornost pri splošni zasnovi, zato da bi dokumenti, ki niso PDF, še naprej pravilno delovali. To je bila Fabijeva prva izkušnja pri razvoju KDE-jevega programa, do katere je prišlo zato, ker je Fabio naletel na težavo in se odločil, da glede nje nekaj stori.
</p>

<h2><a href="./plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Delovna okolja KDE Plasma 4.9 – osnovne izboljšave</a></h2>

<p>
Med poudarke glede delovnih okolij Plasma spadajo precejšnje izboljšave upravljalnika datotek Dolphin, posnemovalnika terminala Konsole, dejavnosti in upravljalnika oken KWin. Za podrobnosti si oglejte <a href="./plasma">najavo za delovna okolja Plasma</a>.
</p>

<h2><a href="./applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Novi in izboljšani KDE-jevi programi 4.9</a></h2>

<p>
Med nove ali izboljšane KDE-jeve programe  spadajo, Okular, Kopete, Kontact, izobraževalni programi in igre. Za podrobnosti si oglejte <a href="./applications">najavo za KDE-jeve programe</a>.
</p>

<h2><a href="./platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>KDE-jeva razvojna platforma 4.9</a></h2>
<p>
Današnja izdaja KDE-jeve razvojne platforme vsebuje popravke, druge izboljšave kakovosti in omrežnih zmožnosti ter priprave na ogrodje KDE Frameworks 5. Za podrobnosti si oglejte <a href="./platform">najavo za KDE-jevo razvojno platformo</a>.
</p>
<h2>Razširite besedo in spremljajte dogajanje: uporabite oznako »KDE«</h2>
<p>
Pozivamo vas, da novico razširite po družbenih omrežjih. Objavljajte zgodbe na novičarskih spletnih straneh, uporabite Reddit, Delicious, Twitter, Identi.ca in podobno. Pošljite zaslonske posnetke na spletne storitve Facebook, Google+, Flickr, deviantART, Ipernity in Picasa. Pri tem uporabite ustrezno skupino in ustrezne oznake. Objavite video posnetke na YouTube, Blip.tv in Vimeo. Za objavljeno priporočamo oznako »KDE«. To drugim ljudem omogoča, da laže najdejo vaše objave.
</p>
<p>
Dogajanje lahko spremljate na spletni strani <a href="http://buzz.kde.org/">buzz.kde.org</a>, ki v živo zbira objave na identi.ca, twitter, youtube, flickr, picasaweb in spletnih dnevnikih.
</p>
<h2>Zabave ob izidu</h2>
<p>
Kot je običaj, člani skupnosti KDE po vsem svetu ob izidih novih različic organizirajo zabave in srečanja. Precej jih je že načrtovanih, še več pa jih pride kasneje. Sebi najbližjo zabavo poiščite na <a href="http://community.kde.org/Promo/Events/Release_Parties/4.9">seznamu vseh zabav</a>. Vsakdo je dobrodošel! Čaka vas kombinacija zanimive družbe, živahnih pogovorov in morda tudi pijače in jedače. To je odlična priložnost, da izveste, kaj se dogaja v skupnosti KDE, se vključite ali pa le srečate druge uporabnike in prispevkarje.
</p>
<p>
Ljudi pozivamo, da organizirajo zabave in srečanja v svojih krajih. Oglejte si <a href="http://community.kde.org/Promo/Events/Release_Parties">nasvete za organizacijo zabave</a>.
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.9/&amp;title=KDE%20releases%20version%204.9%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.9/" data-text="#KDE releases version 4.9 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.9/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.9%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.9/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde49"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde49"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde49"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde49"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2>O teh najavah izidov</h2>
<p>
Te najave izidov so pripravili Jos Poortvliet, Carl Symons, Valorie Zimmerman, Jure Repinc, David Edmundson ter drugi člani KDE-jeve promocijske ekipe in širše skupnosti KDE. Obsegajo le del mnogih sprememb, ki so v preteklih šestih mesecih doletele KDE-jevo programsko opremo.
</p>

<h4>Podprite KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify"> KDE e.V.-jev nov <a
href="http://jointhegame.kde.org/">program za podporne člane</a> je
sedaj odprt. Za samo 25 € na četrtletje lahko mednarodni skupnosti KDE
zagotovite rast in naprejšnje ustvarjanje odlične svobodne in odprtokodne
programske opreme.</p>

<p>&nbsp;</p>
