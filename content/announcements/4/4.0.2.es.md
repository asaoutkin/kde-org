---
aliases:
- ../announce-4.0.2
date: '2008-03-05'
title: Anuncio de lanzamiento de KDE 4.0.2
---

<h3 align="center">
El proyecto KDE lanza la segunda versión que mejora las traducciones y servicios del escritorio líder de software libre.

</h3>

<p align="justify">
  <strong>
La Comunidad de KDE lanza la la segunda versión que mejora las traducciones y servicios
del Escritorio Libre 4.0, incluyendo nuevas funcionalidades en el Escritorio Plasma
y numerosas correcciones de fallos, mejoras de rendimiento y actualizaciones de
traducción.
</strong>
</p>

<p align="justify">
La <a href="http://www.kde.org/">Comunidad de KDE</a> 
ha anunciado hoy la inmediata disponibilidad de KDE 4.0.2, la segunda versión de mantenimiento
para la última generación del más avanzado y potente escritorio libre. KDE 4.0.2 incorpora
un escritorio básico y muchos otros paquetes como administración, redes, educación,
utilidades, multimedia, juegos, recursos gráficos, desarrollo web y demás. Las
premiadas herramientas y aplicaciones de KDE están disponibles en casi 49 idiomas.
</p>
<p align="justify">
KDE, incluyendo todas sus bibliotecas y aplicaciones, está disponible gratuitamente
bajo licencias de Código Abierto. Se puede conseguir KDE en código fuente y varios
formatos binarios desde <a
href="http://download.kde.org/stable/4.0.2/">http://download.kde.org</a> y además
en <a href="http://www.kde.org/download/cdrom">CD-ROM</a> o junto con alguno de los
<a href="http://www.kde.org/download/distributions">principales sistemas GNU/Linux
y UNIX</a> existentes hoy en día.
</p>

<h4>
  <a id="changes">Mejoras</a>
</h4>
<p align="justify">
KDE 4.0.2 es una versión de mantenimiento que proporciona correcciones a problemas
reportados usando el <a href="http://bugs.kde.org/">sistema de seguimiento de bugs
de KDE</a> y mejora el soporte para las nuevas traducciones y las ya existentes.
Para KDE 4.0.2, los equipos de Documentación y Traducción han hecho una excepción
a la política habitual de no cambiar textos visibles al usuario, de modo que los
desarrolladores de Plasma pudieran introducir algunas nuevas funcionalidades
para esta versión de KDE.
<br />
Las mejoras de esta versión incluyen, no exclusivamente:
</p>
<ul>
    <li>
	Nuevas funciones en Plasma. El tamaño y posición del panel ahora puede ser configurado.
	La visibilidad de varias opciones se ha mejorado para hacer más fácil a los nuevos
	usuarios el descubrir cómo funciona Plasma y qué proporciona.
    </li>
    <li>
	Las versiones de KDE 4.0.2 en Farsi e Islandés mejoran aún más la localización, 
	como también lo han hecho otras traducciones. KDE 4.0.2 está disponible en 49 idiomas.
    </li>
    <li>
	Los problemas de redibujado se han solucionado en KHTML, el motor de navegación web.
	KHTML ahora también soporta más sitios web, ya que acepta más documentos no
	compatibles con HTML 4.
    </li>
</ul>

<p align="justify">
Varios problemas de estabilidad se han solucionado en Kopete, el cliente de mensajería
instantánea de KDE y Okular, el lector de documentos.
</p>

<p align="justify">
Para ver una lista más detallada de las mejoras realizadas desde el lanzamiento de KDE 4.0.1 el mes pasado, por favor visite el <a href="/announcements/changelogs/changelog4_0_1to4_0_2">registro de cambios de KDE 4.0.2</a>.
</a>.
</p>

<p align="justify">
Se puede encontrar información adicional acerca de las mejoras de la serie de KDE 4.0.x
en el <a href="../4.0/">anuncio de KDE 4.0</a> y en el
<a href="/announcements/4/4.0.1">anuncio de KDE 4.0.1</a>.
</p>

<h4>
  Cómo instalar los paquetes binarios de KDE 4.0.2
</h4>
<p align="justify">
  <em>Creadores de paquetes</em>.
Algunos proveedores de Linux/UNIX han proporcionado generosamente
paquetes binarios de KDE 4.0.2 para algunas versiones de su distribución, y en
otros casos comunidades de voluntarios lo han hecho.
Algunos de estos paquetes binarios están disponibles para su libre descarga
en el servidor de descargas de KDE en 
<a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.2/">http://download.kde.org</a>.
Paquetes binarios adicionales, así como actualizaciones de los paquetes ahora
disponibles, pueden estar disponibles en las próximas semanas.

</p>

<p align="justify">
<a id="package_locations"><em>Localizaciones de paquetes</em></a>.
Para ver una lista actualizada de paquetes binarios disponibles de los que el Proyecto
KDE ha sido informado, por favor visite la <a href="/info/4.0.2">página
de información de KDE 4.0.2</a>.
</p>

<h4>
  Cómo compilar KDE 4.0.2
</h4>
<p align="justify">
  <a id="source_code"></a><em>Código fuente</em>.
  El código fuente completo de KDE 4.0.2 puede ser
  <a href="http://download.kde.org/stable/4.0.2/src/">descargado libremente</a>.
  Hay disponibles instrucciones acerca de compilar e instalar KDE 4.0.2
  en la <a href="/info/4.0.2">página de información de KDE 4.0.2</a>.

</p>
<h4>
Ayudar a KDE
</h4>
<p align="justify">
KDE es un proyecto de <a href="http://www.gnu.org/philosophy/free-sw.html">Software Libre</a>
que existe y crece sólo mediante la ayuda de muchos voluntarios que donan su tiempo y esfuerzo. KDE siempre está buscando nuevos voluntarios y colaboradores, bien para ayudar programando, arreglando fallos o informando de ellos, escribiendo documentación, traducciones, promocionándolo, donando dinero, etc. Todos los colaboradores son gratamente apreciados y esperados con mucho entusiasmo. Por favor, lea la página <a href="/community/donations/">Ayudar a KDE</a> para más información.
</p>

<p align="justify">
¡Esperamos noticias suyas pronto!
</p>
<h2>Acerca de KDE 4</h2>
<p>
KDE 4.0 es un innovador escritorio de Software Libre que contiene muchas aplicaciones
de uso diario, así como para propósitos específicos. Plasma es un nuevo interfaz de escritorio
desarrollado para KDE 4, proporcionando una forma intuitiva de interactuar con el escritorio y
las aplicaciones. El navegador web Konqueror integra la web en el escritorio. El gestor de
archivos Dolphin, el lector de documentos Okular y el centro de control System Settings completan
el conjunto de escritorio básico.

<br />
KDE está construido sobre las Bibliotecas KDE, las cuales proporcionan un acceso sencillo a los recursos
de la red mediante KIO y capacidades visuales avanzadas mediante Qt4. Phonon y Solid, que también son
parte de las Bibliotecas KDE, añaden un <i>framework</i> multimedia y mejor integración de hardware a
todas las aplicaciones KDE.

</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
