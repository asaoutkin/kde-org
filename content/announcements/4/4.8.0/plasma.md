---
title: KDE Plasma Workspaces 4.8 Gain Adaptive Power Management
date: "2012-01-25"
hidden: true
---

<p>
KDE is happy to announce the immediate availability of version 4.8 of both the Plasma Desktop and Plasma Netbook Workspaces. The Plasma Workspaces have seen improvements to existing functionality, as well as the introduction of significant new features.
</p><p>

The <strong>Window Switcher</strong> that is commonly assigned to the Alt+Tab shortcut now has six possible layouts. These choices are especially useful for systems that don't use Desktop Effects. Task Switcher choices can be found at the Window Behavior module inside System Settings. Another round of significant <a href="http://philipp.knechtges.com/?p=10">performance improvements</a> in KWin, Plasma's window manager and compositor further improve the user experience.

</p><p>

<div class="text-center">
	<a href="/announcements/4/4.8.0/window-switcher-layout.png">
	<img src="/announcements/4/4.8.0/thumbs/window-switcher-layout.png" class="img-fluid" alt="You can choose the layout in Plasma's Alt-Tab window switcher">
	</a> <br/>
	<em>You can choose the layout in Plasma's Alt-Tab window switcher</em>
</div>
<br/>

</p><p>
<strong>KDE Power Management System Settings</strong> have been <a href="http://drfav.wordpress.com/2011/10/04/power-management-a-new-screencast/">redesigned</a>. The user interface has been simplified and the layout improved. Power management no longer relies on user created profiles. Instead, it provides three presets: On AC Power, On Battery and On Low Battery. In addition, power management is now multi-screen aware and adapts to the user's current activity. This means, for example, that power settings can be set to never dim or never turn off the screen in a Photos or Video Activity. If needed, power management can be deactivated quickly via the battery desktop applet.

<div class="text-center">
	<a href="/announcements/4/4.8.0/power-management.png">
	<img src="/announcements/4/4.8.0/thumbs/power-management.png" class="img-fluid" alt="Power Management adapts to the user's activity">
	</a> <br/>
	<em>Power Management adapts to the user's activity</em>
</div>
<br/>

</p><p>

KSecretService offers shared password storage, making saved passwords available to other applications. This provides a more secure system and better integration of non-KDE apps with the Plasma Workspaces. With this feature, it is no longer necessary to contend with different password storage systems when using non-KDE applications.

</p><p>
A new splash screen implementation uses QtQuick. This release debuts a visually and feature-wise re-implementation of the device notifier widget as the first default widget of Plasma Desktop that moves to QtQuick for easier maintainability, a smoother user experience and more touch-friendliness.  The on-screen keyboard has seen numerous improvements in terms of bug fixes and performance. Taskbars and docks work better thanks to improvements from Craig Drummond's Icon Tasks Plasmoid. This includes nicer context menus, improved support for launchers, and a number of bug fixes. A new picture-of-the-day wallpaper plugin allows new astronomy, flickr, Wikipedia or other picture on your screen every day. Performance improvements, usability fixes round up the overall user experience.
</p>

<div class="text-center">
	<a href="/announcements/4/4.8.0/tasks.png">
	<img src="/announcements/4/4.8.0/thumbs/tasks.png" class="img-fluid" alt="Icon Tasks provides an alternative way to the more traditional task bar">
	</a> <br/>
	<em>Icon Tasks provides an alternative way to the more traditional task bar</em>
</div>
<br/>

<h4>Installing Plasma</h4>

<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href="http://plasma-active.org">Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.8.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.8.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.8.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.8.0">4.8 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.8.0 may be <a href="/info/4.8.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.8.0
  are available from the <a href="/info/4.8.0#binary">4.8.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we recommend to use a recent version of Qt, either 4.7.4 or 4.8.0. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Announced Today:</h2>

<h3>
<a href="../applications">
KDE Applications 4.8 Offer Faster, More Scalable File Management
</a>
</h3>

<p>

<a href="../applications">
<img src="/announcements/4/4.8.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.8"/>
</a>
KDE applications released today include <a href="http://userbase.kde.org/Dolphin">Dolphin</a> with its new display engine, new <a href="http://userbase.kde.org/Kate">Kate</a> features and improvements, <a href="http://userbase.kde.org/Gwenview">Gwenview</a> with functional and visual improvements. KDE Telepathy reaches first beta milestone. <a href="http://userbase.kde.org/Marble/en">Marble's</a> new features keep arriving, among which are: Elevation Profile, satellite tracking and <a href="http://userbase.kde.org/Plasma/Krunner">Krunner</a> integration. Read the complete <a href="../applications">'KDE Applications Announcement'</a>.
<br />
</p>

<h3>
<a href="../platform">
KDE Platform 4.8 Enhances Interoperability, Introduces Touch-Friendly Components
</a>
</h3>

<p>
<a href="../platform">
<img src="/announcements/4/4.8.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.8"/>
</a>

KDE Platform provides the foundation for KDE software. KDE software is more stable than ever before. In addition to stability improvements and bugfixes, Platform 4.8 provides better tools for building fluid and touch-friendly user interfaces, integrates with other systems' password saving mechanisms and lays the base for more powerful interaction with other people using the new KDE Telepathy framework. For full details, read the <a href="../platform">KDE Platform 4.8 release announcement</a>.
<br />

</p>
