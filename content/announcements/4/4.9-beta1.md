---
aliases:
- ../announce-4.9-beta1
date: '2012-06-04'
description: KDE Provides Test Release for 4.9 Beta1 Workspaces, Applications and
  Platform.
title: KDE Announces 4.9 Beta1 and Testing Initiative
---

<p align="justify">

Today KDE released the first beta for its renewed Workspaces, Applications, and Development
Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing
bugs and further polishing new and old functionality. Highlights of 4.9 include, but are not limited to:

<ul>
    <li>
    Qt Quick in Plasma Workspaces -- Qt Quick is continuing to make its way into the Plasma Workspaces, the Qt Quick Plasma Components, which have been introduced with 4.8, mature further. Additional integration of various KDE APIs was added through new modules. More parts of Plasma Desktop have been ported to QML. While preserving their functionality, the Qt Quick replacements of common plasmoids are visually more attractive, easier to enhance and extend and behave better on touchscreens.
    </li>
    <li>The Dolphin file manager has improved its display and sorting and searching based on metadata. Files can now be renamed inline, giving a smoother user experience.
    <li>
    Deeper integration of Activities for files, windows and other resources: Users can now more easily associate files and windows with an Activity, and enjoy a more properly organized workspace. Folderview can now show files related to an Activity, making it easier to organize files into meaningful contexts.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making the KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.9_Feature_Plan">4.9 Feature Plan</a>. As with any large number of changes, we need to give 4.9 a good testing in order to maintain and improve the quality and our user's user experience when they get the update. Therefore, in tandem with this first 4.9 beta, we also announce a testing initiative lead by KDE's Testing Team.

<h4>Thorough and Rigorous Beta Testing</h4>
The KDE Community is committed to improving quality substantially with a new program that starts with the 4.9 releases. The 4.9 beta releases of Plasma Workspaces, KDE Applications, and KDE Platform are the first phase of a testing process that involves volunteers called “Beta Testers”. They will receive training, test the two beta releases and report issues through <a href="http://bugs.kde.org">KDE Bugzilla</a>.

<h5>The Beta Testing Team</h5>
The Beta Testing Team is a group of people who will do a 'Testapalooza' on the beta releases, catching as many bugs as possible. This is organized in a structured process so that developers know clearly what to fix before the final release. Anyone can help, even people without programming skills.

<h5>How It Works</h5>
There are two groups of beta testers:
<ul>
<li>informal testers install the beta from their distribution and use it as they normally would. They focus on familiar applications and functions, and report any bugs they can reproduce reliably.</li>
<li>selected functional testing involves a list of applets, new programs and applications that require full testing. Volunteers will choose from the list and test according to <a href="http://community.kde.org/Get_Involved/Testing/Beta">established beta testing guidelines</a>.</li>
</ul>
Testing is coordinated to make sure that people's testing time and effort are used as effectively as possible. Coordination gets the most value out of the overall testing program.

<h5>Get Involved</h5>
This is an excellent way for anyone to make a difference for KDE. Continuous quality improvement sets KDE apart. Assisting developers by identifying legitimate bugs leverages their time and skills. This means higher quality software, more features, happier developers. The Beta Testing Program is structured so that any KDE user can give back to KDE, regardless of their skill level.

If you want to be part of this quality improvement program, please contact the Team on the IRC channel #kde-quality on freenode.net. The Team Leaders want to need to know ahead of time who is involved in order to coordinate all of the testing activities. They are also committed to having this project be fun and rewarding.

After checking in, you can install the beta through your distribution package manager. The <a href="http://community.kde.org/Get_Involved/Testing/Beta/InstallingBeta1">KDE Community wiki</a> has instructions. This page will be updated as beta packages for other distributions become available. With the beta installed, you can proceed with testing. Please contact the Team on IRC #kde-quality if you need help getting started.

<h5>Training</h5>
Effective bug reporting is critical to the success of the Program. Bugzilla is a valuable tool for developers and increases their efficiency at closing bugs. Reporting works best when done properly, so the Program includes Bugzilla training. An IRC bugzilla training will be offered in #kde-bugs (freenode) on the weekend of June 9th and 10th. Please visit the #kde-quality IRC channel for information about the Program and the Bugzilla training, or if you have any questions.

Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining in.

<h3>KDE Software Compilation 4.9 Beta1</h3>
<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.8.80/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.9 Beta1 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.9 Beta1 (internally 4.8.80)
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.8.80#binary">4.8.80 Info
Page</a>.
</p>

<h4>
  Compiling 4.9 Beta1
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.9 Beta1 may be <a
href="http://download.kde.org/unstable/4.8.80/src/">freely downloaded</a>.
Instructions on compiling and installing 4.8.80
  are available from the <a href="/info/4.8.80">4.8.80 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or
become a KDE e.V. supporting member through our new
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}