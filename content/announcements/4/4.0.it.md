---
aliases:
- ../4.0
date: '2008-01-11'
layout: single
title: Rilasciato KDE 4.0
---

<h3 align="center">Il progetto KDE rilascia la quarta versione del più diffuso ambiente Desktop Free Software</h3>
<p align="justify">
	<strong>Con la quarta versione la comunità KDE da il via all'era di KDE 4</strong>
</p>

<p>
La comunità KDE è in fibrillazione nell'annunciare l'immediata disponibilità  di <a href="/announcements/4.0/">KDE 4.0.0</a>. Questo significativo rilascio rappresenta la fine dell'intensivo ciclo di sviluppo che ha portato a KDE 4.0 e contemporaneamente l'inizio dell'era di KDE 4.</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Il Desktop di KDE 4.0</em>
</div>

<p>
Le <strong>Librerie</strong> sono state migliorate molto, all'incirca in tutte le aree.
l'infrastruttura multimediale Phonon offre un'infrastruttura indipendente dalla piattaforma per tutte le applicazioni di KDE. Solid, l'infrastruttura per l'integrazione hardware, facilita l'interazione con i vari dispositivi e fornisce gli strumenti per una migliore gesione del risparmio energetico.
<p />
Il <strong>Desktop</strong> KDE 4 gode di alcune nuove funzionalità  di primo piano. Plasma, il gestore del desktop, offre una interfaccia rinnovata ed integra panello, menu e widget sul desktop come anche la funzione dashboard. KWin, il gestore delle finestre di KDE, ora supporta avanzati effetti grafici che permettono una facile interazione con le finestre.
<p />
Anche molte <strong>applicazioni</strong> sono state migliorate. Aggiornamento delle immagini tramite grafica vettoriale, cambiamenti nelle librerie di base, interfaccia utente migliorata, nuove funzionalità, nuove applicazioni... pensa a qualcosa, KDE 4.0 cel'ha!
Okular, il nuovo visualizzatore di documenti e Dolphin, il nuovo gestore file sono solo due delle applicazioni che rappresentano le nuove tecnologie di KDE 4.0.
<p />
<img src="/announcements/4/4.0/images/oxybann.png" align="right" hspace="10"/>
Il team artistico <strong>Oxygen</strong> ha donato una ventata di aria fresca al desktop.
Pressochè tutte le parti del desktop KDE e le sue applicazioni sono interessate dalla nuova grafica. Bellezza e coerenza sono due dei concetti alla base di Oxygen.
</p>

<h3>Desktop</h3>
<ul>
	<li>Plasma è il nuovo gestore del Desktop. Plasma fornisce un pannello, un menu e altre intuitive opzioni per l'interazione con il desktop e le applicazioni.
	</li>
	<li>KWin, il gestore di finestre di KDE, ora ha avanzate funzioni grafiche. La resa grafica accelerata pensata per una migliore e più intuitiva interazione con le finestre.
	</li>
	<li>Oxygen è il nuovo tema grafico di KDE 4.0. Oxygen regala una coerente, bella ed intuitiva esperienza visiva.
	</li>
</ul>
Per saperne di più sulla nuova interfaccia grafica di KDE dai un'occhiata alla <a href="./desktop">visita guidata a KDE 4.0 </a>.

<h3>Applicazioni</h3>
<ul>
	<li>Konqueror è il famoso browser di KDE. Konqueror è leggero, ben integrato e supporta i nuovi standard come i CSS 3.</li>
	<li>Dolphin è il nuovo gestore file. Dolphin è stato sviluppato per essere uno strumento facile e allo stesso tempo potente.
	</li>
	<li>La nuova disposizione delle impostazioni di sistema ed il nuovo KSysGuard rendono più facili le operazioni di controllo e gestione delle risorse e delle attività.
	</li>
	<li>Okular, il visualizzatore di documenti di KDE 4, supporta molti formati. Okular è una delle molte applicazioni che per KDE 4 sono state migliorate con l'aiuto del progetto <a href="http://openusability.org">OpenUsability Project</a>.
	</li>
	<li>Le applicazioni orientate all'educazione sono state tra le prime ad essere convertite e sviluppate con le nuove tecnologie di KDE 4. Kalzium, una tavola periodica grafica, e Marble, il mappamondo, sono solo due delle molte gemme fra le applicazioni educative. Per avere più dettagli riguardo le applicazioni educative dai uno sguardo alla <a href="./education">visita guidata</a>.
	</li>
	<li>Molti dei giochi sono stati aggiornati. Giochi di KDE come KMines, il campo minato, e KPat, il solitario, patience game sono stati ridisegnati. Grazie alla nuova grafica vettoriale e alle nuove capacità grafiche i giochi sono stati resi indipendenti dalla risoluzione dello schermo.
	</li>
</ul>
Alcune applicazioni sono presentate più in dettaglio nella <a href="./applications">visita guidata</a>.

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/4/4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>Gestione dei file, Impostazioni Di sistema e Menu in azione</em>
</div>
<br/>

<h3>Librerie</h3>
<p>
<ul>
	<li>Phonon offre alle applicazioni capacità multimediali come la riproduzione audio e video. Internamente, Phonon utilizza vari motori facilmente intercambiabili. Il motore predefinito sarà Xine, che offre un notevole supporto per vari formati. Phonon permette anche di scegliere il dispositivo di riproduzione in base al tipo di contenuto da riprodurre.
	</li>
	<li>Solid, l'infrastruttura di integrazione dell'hardware, facilita la gestione dei dispositivi, rimovibili e non, nelle applicazioni di KDE. Solid si interfaccia anche con le capacità di risparmio energetico del sistema operativo, gestisce le connessioni di rete e i dispositivi Bluetooth. Internamente, Solid combia la potenza di HAL, NetworkManager e Bluez (per la gestione del Bluetooth), ma questi componenti sono sostituibili senza conseguenze per le applicazioni, garantendo la massima portabilità.
	</li>
	<li>KHTML è il motore di visualizzazione delle pagine web usato da Konqueror, il browser di KDE. KHTML è leggero e supporta i moderni standard come i CSS 3. KHTML è stato anche il primo motore a passare il famoso test Acid 2. 
	</li>
	<li>La libreria ThreadWeaver, inclusa nelle kdelibs, fornische un'interfaccia di alto livello per la migliore gestione dei moderni sistemi multi-core, rendendo le applicazioni per KDE più performanti utilizzando in maniera efficace le risorse disponibili sul sistema.
	</li>
	<li>Essendo sviluppato sulle librerie Qt della Trolltech KDE 4.0 fa uso delle avanzate capacità visive e del minor ingombro di memoria delle stesse. Le kdelibs costituiscono una eccellente estensione alle librerie Qt, aggiungendo molte funzionalità di alto livello e facilitazioni per gli sviluppatori.
	</li>
</ul>
</p>
<p>La biblioteca tecnica di KDE, <a href="http://techbase.kde.org">TechBase</a>, contiene molte informazioni sulle librerie di KDE.</p>

<h4>Visita guidata...</h4>
<p>
La <a href="./guide">visita guidata a KDE 4.0</a> riassume velocemente le varie novità e miglioramenti di KDE 4.0. Con una serie di screenshot, ti guida attraverso le differenti parti di KDE 4.0 e mostra alcune delle nuove entusiasmanti tecnologie disponibili per gli utenti. Iniziare dal <a href="./desktop">desktop</a> è consigliabile. Successivamente sono presentate le nuove <a href="./applications">applicazioni</a> come Okular, il visualizzatore di documenti, e Dolphin, il gestore file. Vi è un'introduzione anche alle <a href="./education">applicazioni educative</a> e ai <a href="./games">giochi</a>.
</p>

<h4>Facci un giro...</h4>
<p>
Per quelli interessati a provare e a contribuire, molte distribuzioni ci hanno fatto sapere che forniranno i pacchetti per KDE 4.0 dopo il rilascio odierno. La lista completa può essere trovata nella <a href="http://www.kde.org/info/4.0">KDE 4.0 Info Page</a>, dove possono essere trovati anche i collegamenti al codice sorgente, informazioni sulla compilazione, informazioni sulla sicurezza e su altri problemi.
</p>
<p>
I seguenti progetti ci hanno notificato la disponibilità delle seguenti distribuzioni o LiveCD per KDE 4.0:

<ul>
	<li>
		Una versione Alpha di <strong>Arklinux 2008.1</strong> che contiene KDE 4.0 è attesa a breve dopo il rilascio odierno, con una versione finale attesa nel giro di 3 o 4 settimane.
	</li>
	<li>
		<strong>Fedora</strong> fornirà KDE 4.0 in Fedora 9, con <a
		href="http://fedoraproject.org/wiki/Releases/9">rilascio</a>
		atteso per Aprile, mentre versioni Alpha saranno disponibili dal 24 di Gennaio. I pacchetti per KDE 4.0 si trovano nel deposito pre-alpha <a
		href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a>.
	</li>
	<li>
		I pacchetti di KDE 4.0 per <strong>Debian</strong> sono disponibili nel deposito "experimental", la piattaforma di sviluppo KDE sarà inclusa in <em>Lenny</em>. Tenete d'occhio gli aggiornamenti del <a href="http://pkg-kde.alioth.debian.org/">Debian KDE Team</a>.
	</li>
	<li>
		<strong>Gentoo Linux</strong> distribuisce KDE 4.0 su <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
	</li>
	<li>
		Pacchetti per <strong>Kubuntu</strong> e Ubuntu saranno inclusi nella futuro rilascio "Hardy Heron" (8.04) e sono disponibili anche per "Gutsy Gibbon" (7.10). Un Live CD è disponibile per provare KDE 4.0. Maggiori dettagli su <a href="http://kubuntu.org/announcements/kde-4.0">Ubuntu.org</a>.
	</li>
	<li>
		<strong>openSUSE</strong> rende <a href="http://en.opensuse.org/KDE4">disponibile</a> KDE 4.0 per openSUSE 10.3 (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">installazione in un clic</a>)
		e openSUSE 10.2. <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
		Four Live CD</a> è comunque disponibile con gli stessi pacchetti. KDE 4.0 sarà parte della prossima openSUSE 11.0.
	</li>
</ul>
</p>

<h2>A proposito di KDE 4</h2>
<p>
KDE 4.0 è un innovativo desktop Free Software che contiene molte applicazioni per un utilizzo quotidiano come anche per applicazioni specifiche. Plasma è un nuovo gestore per il desktop sviluppato per KDE 4, fornisce una intuitiva interfaccia per l'interazione con il desktop e con le applicazioni. Konqueror, il web browser, integra il web con il Desktop. Dolphin, il gestore file, Okular, il visualizzatore dei documenti, e le Impostazioni di Sistema forniscono un desktop di base.
<br />
KDE è sviluppato sulle omonime librerie, che forniscono facilità di accesso alle risorse sulla rete con KIO e avanzate capacità grafiche grazie alle Qt4. Phonon e Solid, che sono ugualmente parte delle librerie di KDE, aggiungono una infrastruttura multimediale e una migliore integrazione dell'hardware con le applicazioni scritte per KDE.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}