---
aliases:
- ../announce-4.1-beta2
date: '2008-06-24'
title: Annuncio del rilascio di KDE 4.1 beta 2
---

<h3 align="center">
  Disponibile la seconda beta di KDE 4.1
</h3>

<p align="justify">
  <strong>
La communità di KDE è lieta di annunciare il secondo rilascio beta per KDE 4.1</strong>
</p>

<p align="justify">
La <a href="http://www.kde.org/">Comunit&agrave; KDE</a> &egrave; fiera di annunciare il
rilascio della seconda beta di KDE 4.1. Questa Beta 2 è indirizzata a coloro i quali desiderino provare
in anteprima KDE 4.1 e a chi volesse contribuire aiutando a scovare bug, regressioni e altri problemi in
modo che la versione 4.1 possa finalmente sostituire la 3.5 sui Desktop degli utenti. KDE 4.1 Beta 2 è
disponibile in formato binario per una gran numero di piattaforme e, naturalmente anche sotto forma di
codice sorgente. La versione finale di KDE 4.1 è attesa per fine Luglio 2008.
</p>

<h4>
  <a id="changes">I dettagli di KDE 4.1 Beta 2</a>
</h4>

<p align="justify">
A un mese dal "feature freeze" (ndr. non si possono aggiungere più nuove funzionalità)
sul ramo KDE 4.1, gli sviluppatori di KDE sono impegnati in una lavoro di pulizia
focalizzato sulle nuove funzionalità, sull'integrazione del Desktop, documentazione
e traduzione dei vari pacchetti. Si sono tenute varie sessioni per la correzione dei bug
che sono stati rimossi in gran parte, mentre ve ne sono altri che sono ancora presenti e che
attendono di essere rimossi per il rilascio finale. KDE 4.1 sta prendendo forma man mano.
Test e riscontri su questa versione saranno molto apprezzati e servono per fare in modo
che KDE 4.1 possa fare colpo.

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/desktop-folderview.png">
	<img src="/announcements/4/4.1-beta2/desktop-folderview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KDE 4.1 Beta 2 con il Desktop Plasma</em>
</div>
<br/>

<ul>
    <li>Linguaggi di binding per KDE 4.1 in vari linguaggi, come Python,
    Ruby
    </li>
    <li>Miglioramete per l'usabilità e supporto in Dolphin
    </li>
    <li>Miglioramenti su tutto Gwenview
    </li>
</ul>

Una lista più completa di funzionalità per KDE 4.1 è disponibile su
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Techbase</a>.

</p>

<h4>
  Linguaggi di binding
</h4>
<p align="justify">
Mentre la maggioranza delle applicazioni di KDE 4.1 sono scritte in C++, i linguaggi di binding
espongono le funzionalitò delle librerie di KDE a sviluppatori di applicazioni che preferiscono
un diverso linguaggio. KDE 4.1 ha un supporto a diversi altri linguaggi, come Python e Ruby.
L'applet di stampa che è stata aggiunta in KDE 4.1 è scritta in Python, ed è totalmente
trasparente all'utente.
</p>

<h4>
  Dolphin matura
</h4>
<p align="justify">
Dolphin, il gestore di file di KDE4 ha subito diverse migliorie.

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/dolphin-tagging.png">
	<img src="/announcements/4/4.1-beta2/dolphin-tagging_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Il Desktop Semantico con il suo sistema di etichettatura</em>
</div>
<br/>

I primi passi e pezzi del Desktop Semantico Sociale <a href="http://nepomuk.kde.org/">NEPOMUK</a>
stanno diventando visibili e applicabili in modo più diffuso. Il supporto per l'etichettatura in
Dolphin mostra la prima funzionalità in arrivo per questa tecnologia, sviluppata nell'ambito di un
programma di ricerca europeo.

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/dolphin-selection.png">
	<img src="/announcements/4/4.1-beta2/dolphin-selection_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Selezione dei file con un singolo clic</em>
</div>
<br/>

La selezione dei file è stata resa più semplice da un semplice pulsante "+",
visibile nell'angolo in alto a sinistra, che seleziona il file, anzicché aprirlo.
Questa modifica permette l'uso del gestore di file in modalità singolo clic molto più
semplice e previene aperture accidentali dei file, mentre diventa intuitivo e semplice da usare.<br />

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/dolphin-treeview.png">
	<img src="/announcements/4/4.1-beta2/dolphin-treeview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>La nuova vista ad albero in Dolphin</em>
</div>
<br/>

Molti utenti hanno richiesto una modalità di vista ad albero nella modalità della lista dettagliata.
La funzionalità è stata modificata e combinata con le migliorie del singolo clic per muovere e copiare
i file più velocemente.

</p>

<h4>
  Gwenview ripulito
</h4>
<p align="justify">
Il visualizzatore d'immagine predefinito di KDE4, Gwenview, è stato ripulito piuttosto bene. Le opzioni
come la rotazione e la vista a schermo pieno sono state messe nel contesto diretto dell'immagine,
rendendo l'interfaccia utente dell'applicazione meno ingombrante, minimizzando i movimenti del mouse e
quindi rendendo l'applicazione più intuitiva e semplice da usare.

</p>

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/gwenview-browse.png">
	<img src="/announcements/4/4.1-beta2/gwenview-browse_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Visione d'insieme di Gwenview di una cartella</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/gwenview.png">
	<img src="/announcements/4/4.1-beta2/gwenview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Gwenview con la sua barra delle anteprime</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/systemsettings-emoticons.png">
	<img src="/announcements/4/4.1-beta2/systemsettings-emoticons_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Il nuovo modulo delle Impostazioni di sistema emoticon</em>
</div>
<br/>

<h4>
  KDE 4.1 - Rilascio finale
</h4>
<p align="justify">
KDE 4.1 è pianificato per essere rilasciato il 29 Luglio 2008. Questo rilascio avviene esattamente sei mesi dopo il rilascio di KDE 4.0.
</p>

<h4>
  Scaricalo, avvialo, provalo
</h4>
<p align="justify">
  I volontari della comunità e i distributori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti precompilati per KDE 4.0.83 (4.1 Beta 2) per molte distribuzioni Linux, Mac OS X e Windows. Considerate il fatto che questi pacchetti non sono da ritenersi per un utilizzo quotidiano. Consultate il gestore di pacchetti della vostra distribuzione.

<h4>
  Compilare KDE 4.1 Beta 2 (4.0.83)
</h4>
<p align="justify">
  <a id="source_code"></a><em>Codice sorgente</em>.
  Il codice sorgente completo per KDE 4.0.83 può essere <a
  href="/info/4.0.83">scaricato liberamente</a>.
  Le istruzioni per la compilazione e l'installazione di KDE 4.0.83 si trovano nella <a href="/info/4.0.83">KDE 4.0.83 Info
  Page</a>, oppure su <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Supporta KDE
</h4>
<p align="justify">
 KDE è un progetto di <a href="http://www.gnu.org/philosophy/free-sw.it.html">Software Libero</a>
 che esiste e cresce solamente grazie all'aiuto di molti volontari che donano il loro tempo e
 impegno. KDE è sempre alla ricerca di nuovi volontari contribuenti che possano dare una mano
 in settori come la programmazione, la ricerca di bug, la scrittura della documentazione, la
 traduzione, la promozione, con donazioni, ecc... Tutti i contributi sono apprezzati e accettati.
 Per favore leggi la pagina <a href="/community/donations/">Supporting KDE</a> per ulteriori informazioni. </p>

<p align="justify">
Non vediamo l'ora di avere tue notizie!
</p>

<h4>A proposito di KDE 4</h4>
<p align="justify">
KDE 4.0 è un innovativo desktop di Software Libero che contiene molte applicazioni per un utilizzo
quotidiano come anche per applicazioni specifiche. Plasma è un nuovo gestore per il desktop sviluppato
per KDE 4, fornisce un'interfaccia intuitiva per l'interazione con il desktop e con le applicazioni.
Konqueror, il web browser, integra il web con il Desktop. Dolphin, il gestore file, Okular, il
visualizzatore dei documenti, e le Impostazioni di Sistema forniscono un desktop di base.
<br />
KDE è sviluppato sulle omonime librerie, che forniscono facilità di accesso alle risorse sulla rete
con KIO e avanzate capacità grafiche grazie alle Qt4. Phonon e Solid, che sono ugualmente parte delle
librerie di KDE, aggiungono un'infrastruttura multimediale e una migliore integrazione dell'hardware
con le applicazioni scritte per KDE.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
