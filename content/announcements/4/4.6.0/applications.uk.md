---
title: Контекстний перегляд у Dolphin
date: "2011-01-26"
hidden: true
---

<h2>KDE випущено набір програм 4.6.0</h2>
<p>
KDE з радістю повідомляє про випуск нових версій багатьох наших програм. Ігри, освітні програми і навіть невеличкі допоміжні програми стали потужнішими, але залишилися простими у користуванні. Нижче наведено лише деякі з нових можливостей випущеного сьогодні набору програм.
</p>
<h2>Контекстний перегляд у Dolphin</h2>
<p>
Інтерфейси пошуку Kfind і Dolphin уніфіковано у нову спрощену панель пошуку. Контекстний перегляд вперше продемонстровано у формі панелі фільтрування, за допомогою якої ви без проблем можете переглядати список проіндексованих файлів на основі вказаних вами метаданих. За допомогою нової бічної панелі ви зможете виконувати контекстний пошук, удосконалення звичайного режиму роботи файлів на основі додаткового фільтрування за метаданими.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a05.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a05.png" class="img-fluid" alt="За допомогою контекстного перегляду у Dolphin ви зможете скористатися багатьма фільтрами для пошуку файлів за метаданими">
	</a> <br/>
	<em>За допомогою контекстного перегляду у Dolphin ви зможете скористатися багатьма фільтрами для пошуку файлів за метаданими</em>
</div>
<br/>

<ul>
    <li>Користування режимом перегляду стовпчиками стало зручнішим. Ширину стовпчиків може бути визначено автоматично або вказано користувачем, можна позначати групи файлів простим обведенням їх контуру вказівником миші, для навігації сусідніми стовпчиками більше не потрібна горизонтальна смужка гортання.</li>
    <li>У дусі часу, з переведенням розробки KDE з SVN до Git, у новій версії Dolphin передбачено додаток роботи з Git, за допомогою якого можна оновлювати та надсилати дані до Git за допомогою графічного інтерфейсу. Звичайно ж, і додаток SVN нікуди не подівся.</li>
    <li>Різноманітні покращення у службових меню (<a href="http://ppenz.blogspot.com/2010/11/improved-service-menus.html">http://ppenz.blogspot.com/2010/11/improved-service-menus.html</a>)</li>
</ul>

<h2>Нова клієнтська частина SQL у Kate</h2>
У новому випуску Kate, потужного текстового редактора, ви зможете скористатися багатьма покращеннями і новими можливостями. Зокрема у новій версії можна:
<ul>
    <li>відновлювати незбережені вами дані локальних файлів під час наступного запуску Kate (підтримка файла резервних даних);</li>
    <li>постійне завантаження додатків;</li>
    <li>додавання скриптів до меню та прив’язка їх до клавіатурних скорочень.</li>
    <li>Новий<a href="http://kate-editor.org/2010/07/29/katesql-a-new-plugin-for-kate/">додаток для роботи з запитами SQL у Kate</a> надає можливість працювати у Kate у режимі клієнтської програми SQL. Передбачено підтримку багатьох форматів баз даних на основі модуля SQL Qt.</li>
    <li>Новий додаток GNU Debugger (GDB)</li>
    <li>Новий додаток підсвічування позначеного фрагмента тексту.</li>
</ul>

<h2>Поєднання графічних програм з соціальними мережами</h2>
<p>
У новій версії Gwenview, програми для перегляду зображень у KDE, ви зможете скористатися кнопкою «Оприлюднити», яка допоможе експортувати зображення до популярних служб зберігання фотографій та на сторінки соціальних мереж.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a03.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a03.png" class="img-fluid" alt="Gwenview може оприлюднювати зображення у багатьох популярних соціальних мережах">
	</a> <br/>
	<em>Gwenview може оприлюднювати зображення у багатьох популярних соціальних мережах</em>
</div>
<br/>

<p>
<b>KSnapshot</b> нової версії здатен створювати знімки довільно вказаних ділянок на екрані, здатен включати до знімків вказівник миші, а також має кнопку «Надіслати», за допомогою якої ви зможете негайно оприлюднити створений вами знімок.</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a04.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a04.png" class="img-fluid" alt="KSnapshot може експортувати знімки вікон безпосередньо на сайти служб зберігання зображень">
	</a> <br/>
	<em>KSnapshot може експортувати знімки вікон безпосередньо на сайти служб зберігання зображень</em>
</div>
<br/>

Серед випущених сьогодні програм KDE у значній частині ви знайдете нові можливості, а також виправлення численних вад. Крім того, останні покращення у платформі розробки KDE надали змогу підвищити швидкість роботи програм та їхню стабільність.

<h2>Marble допоможе вам потрапити куди треба, а KStars стане швидшим завдяки openGL</h2>
<p>
У Marble, віртуальному глобусі KDE, значно покращено підтримку планування маршрутів. У новій версії ви зможете звантажити потрібні вам карти і спланувати маршрут на місці без потреби у інтернет-з’єднанні. Версія програми для портативних пристроїв, <b>MarbleToGo</b>, стає дуже потужним покроковим навігатором. Зі знімками вікон та відео можна ознайомитися за адресою <a href="http://dot.kde.org/2010/11/10/kdes-marble-team-holds-first-contributor-sprepareprint">dot.kde.org</a>.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a01.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a01.png" class="img-fluid" alt="Додаткові можливості Marble з побудови маршрутів використовують дані з декількох вказаних вами служб">
	</a> <br/>
	<em>Додаткові можливості Marble з побудови маршрутів використовують дані з декількох вказаних вами служб</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a06.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a06.png" class="img-fluid" alt="Мобільна версія Marble є потужним інструментом навігації">
	</a> <br/>
	<em>Мобільна версія Marble є потужним інструментом навігації</em>
</div>
<br/>

<p>
У KStars, стільничному планетарії KDE, ви зможете скористатися додатковою підтримкою показу даних за допомогою openGL, що значно покращить швидкодію програми на обладнанні з підтримкою openGL.
</p>

<h2>Ігри KDE</h2>
<ul>
    <li><b>Klickety</b>, версія гри Clickomania, повертається до набору ігор KDE версії 4.6. Ця програма здатна, у режимі сумісності, замінити собою KSame («Ту саму гру»).</li>
    <li>За допомогою оболонки KGameRenderer розробникам вдалося уніфікувати використання тем у всіх іграх, що надасть іграм нового випуску одноріднішого вигляду і поведінки. На нову архітектуру було портовано близько десятка ігор KDE, що надало змогу зменшити вимоги до використання пам’яті та пришвидшити роботу на багатоядерних процесорах.</li>
    <li>За допомогою нового інструменту створення складанок Palapeli ви зможете бавитися з новими чудовими і неповторними шматочками складанки, ефект фаски робить вигляд нових шматочків набагато природнішим. Значно покращено зручність користування діалоговим вікном створення складанок Palapeli.</li>
    <li>У новій документації до <b>Kajongg</b> ви знайдете опис гри, призначений для гравців-початківців. Комп’ютерний гравець тепер грає значно краще, покращено зручність користування грою. Зануртеся у світ цієї давньої і захопливої гри!</li>
</ul>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a02.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a02.png" class="img-fluid" alt="Palapeli, гра у складанку KDE, здатна без проблем створювати складанки з вказаних вами зображень">
	</a> <br/>
	<em>Palapeli, гра у складанку KDE, здатна без проблем створювати складанки з вказаних вами зображень</em>
</div>
<br/>

<h4>Встановлення програм KDE</h4>

<p align="justify">
KDE, зокрема всі бібліотеки і програми, з яких складається середовище, доступні для безкоштовного звантаження за умов дотримання ліцензій на програмне забезпечення з відкритим кодом. Програмне забезпечення KDE працює на багатьох апаратних платформах, у багатьох операційних системах та з багатьма інструментами керування вікнами або стільничними середовищами. Окрім Linux та інших операційних систем на базі UNIX ви можете скористатися версіями більшості програм KDE у Microsoft Windows за допомогою сайта<a href="http://windows.kde.org">програмного забезпечення KDE у Windows</a> та версіями для Apple Mac OS X за допомогою сайта <a href="http://mac.kde.org/">програмного забезпечення KDE на Mac</a>. Експериментальні випуски програм KDE для різноманітних мобільних платформ, зокрема MeeGo, MS Windows Mobile і Symbian, можна знайти у інтернеті, але зараз у цих випусків немає супровідників.
<br />
Програмне забезпечення KDE можна отримати у форматі початкових кодів та різноманітних бінарних форматах за посиланнями на сайті <a
href="http://download.kde.org/stable/4.6.0/">http://download.kde.org</a>, а також отримати на <a href="http://www.kde.org/download/cdrom">компакт-диску</a>
 або разом з будь-якою з сучасних <a href="http://www.kde.org/download/distributions">поширених систем GNU/Linux та UNIX</a>.
</p>
<p align="justify">
  <a id="packages"><em>Пакунки</em></a>.
  Деякі з виробників дистрибутивів операційних систем Linux/UNIX люб’язно надали можливість користувачам своїх дистрибутивів звантажити бінарні пакунки 4.6.0 
для декількох версій дистрибутивів. У інших випадках такі бінарні версії було створено силами ентузіастів зі спільноти дистрибутива. <br />
  Деякі з таких бінарних пакунків доступні для вільного звантаження за адресою <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.6.0/">http://download.kde.org</a>.
  Додаткові бінарні пакунки, а також оновлення доступних пакунків можна буде звантажити найближчими днями.
<a id="package_locations"><em>Адреси для звантаження пакунків</em></a>.
Поточний список наявних бінарних пакунків, про які відомо проектові KDE, можна знайти на <a href="/info/4.6.0">4.6Інформаційній сторінці KDE</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Повні початкові коди 4.6.0 можна <a
href="http://download.kde.org/stable/4.6.0/src/">безкоштовно звантажити</a>.
Настанови щодо збирання і встановлення програмного забезпечення KDE 4.6.0
 можна знайти на <a href="/info/4.6.0#binary">4.6.0 Інформаційній сторінці KDE</a>.
</p>

<h4>
Вимоги до системи
</h4>
<p align="justify">
Для того, щоб скористатися всіма можливостями найсвіжіших випусків, ми наполегливо рекомендуємо вам встановити найсвіжішу версію Qt, зокрема поточну версію — 4.7.1. Встановлення цієї версії забезпечить стабільність роботи, оскільки покращення програмного забезпечення KDE засновано на покращенні коду бібліотек Qt.<br />
Роботу драйверів графічної підсистеми за певних умов може бути переведено у режим XRender, замість режиму OpenGL. Якщо у вас виникають проблеми з низькою швидкодією графічної підсистеми, має допомогти вимикання ефектів стільниці, все залежить від використаного драйвера і його налаштувань. З метою повного використання можливостей програмного забезпечення KDE ми також рекомендуємо встановити у систему найсвіжішу версію драйверів, оскільки це може значно покращити роботу системи як з огляду на додаткові можливості, так і з огляду на швидкодію і стабільність роботи. 
</p>

<h4>
Подяки
</h4>
<p>
Ці нотатки щодо випуску створено на основі повідомлень Vivek Prakash, Stefan Majewsky, Guillaume De Bure, Nikhil Marathe, Markus Slopianka, Stuart Jarvis, Jos Poortvliet, Nuno Pinheiro, Carl Symons, Marco Martin, Sebastian Kügler, Nick P та багатьох інших учасників команди з просування KDE.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Крім того, сьогодні випущено:</h2>

<h3>
<a href="../plasma">
Керуйте вашим робочим простором Плазми
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/4/4.6.0/images/plasma.png" class="app-icon float-left m-3" alt="Робочі простори Плазми KDE 4.6.0" />
</a>

<b>Робочі простори Плазми KDE</b> нової версії отримали нові можливості завдяки новій системі просторів дій, що спрощує прив’язку програм до певних просторів дій, призначених зокрема для виконання робочих чи розважальних завдань. Нова система керування живленням має спрощений інтерфейс налаштування і ще ширші можливості. KWin, програмою для керування вікнами у робочому просторі Плазми тепер можна керувати за допомогою скриптів, покращено також візуальні елементи стільниці. <b>Плазма для субноутбуків</b>, інтерфейс оптимізований для використання на мобільних пристроях, тепер значно швидший та простіший у користуванні на пристроях з сенсорним інтерфейсом. Докладніше про це можна дізнатися з <a href="../plasma">Оголошення про випуск робочого простору KDE 4.6</a>.

</p>

<h3>
<a href="../platform">
Мобільний профіль надає змогу значно полегшити платформу KDE
</a>
</h3>

<p>

<a href="../platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="Платформа розробки KDE 4.6.0"/>
</a>

<b>Платформа KDE</b>, на основі якої побудовано робочий простір Плазми та програми KDE, у новій версії надає нові можливості всім програмам KDE. Новий профіль збирання для мобільних пристроїв спрощує збирання програм для портативних пристроїв. Оболонка Плазми тепер підтримує створення віджетів на основі QML, декларативної мови Qt, на надає розробникам нові інтерфейси Javascript для взаємодії з даними. Nepomuk, технологія, що забезпечує роботу з метаданими та можливості з семантичного пошуку у програмах KDE, тепер має графічний інтерфейс для створення резервних копій даних та відновлення даних з резервних копій. Замість застарілого інтерфейсу HAL для роботи з обладнанням можна використовувати UPower, UDev і UDisks. Значно покращено підтримку Bluetooth. Покращено набір віджетів та стилів Oxygen. За допомогою нової теми Oxygen для програм GTK користувачі зможуть отримати однорідний вигляд програм робочого простору Плазми. Докладніше про це можна дізнатися з <a href="../platform">Оголошення про випуск платформи KDE 4.6</a>.

</p>
