---
title: Plasma Workspaces Put You in Control
date: "2011-01-26"
hidden: true
---

<h2>KDE Releases Workspaces 4.6.0</h2>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w10.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w10.png" class="img-fluid" alt="Plasma Desktop in 4.6.0">
	</a> <br/>
	<em>Plasma Desktop in 4.6.0</em>
</div>
<br/>

<p>
 KDE is happy to announce the immediate availability of Plasma Desktop and Plasma Netbook 4.6. The Plasma Workspaces have seen both polish in existing functionality as well as the introduction of significant new innovations extending Plasma's capabilities further towards a more semantic, task-driven workflow.
</p>
<p>
The <b>Activities </b>system has been redesigned making it easier to take advantage of them. By right clicking to the window title, you can now make applications and files part of an activity. Changing to this activity, the Plasma workspace will show you what you need when you need it. The process for adding, renaming and removing Activities has also been improved.</li>
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w01.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w01.png" class="img-fluid" alt="It is easy to associate applications with activities">
	</a> <br/>
	<em>It is easy to associate applications with activities</em>
</div>
<br/>

<p>
<b>Power management</b> has been revamped, with shorter and highly modular code (1/10th its previous size) which is easier to maintain and allows plugins to trigger power management features. Power settings are easier to understand and manage with the new configuration user interface. A new ‘Policy Agent’ allows screen inhibition features, such as notifications or the screen turning off while you're watching a movie
</p>
<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w02.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w02.png" class="img-fluid" alt="The new simplified power management interface">
	</a> <br/>
	<em>The new simplified power management interface</em>
</div>
<br/>

<b>KWin</b>, the window and compositing manager, is faster than ever and works better with more graphics drivers, due to significant optimizations that cache window settings and only refresh the needed areas of the screen. This brings:

<ul>
    <li>Significant <a href="http://blog.martin-graesslin.com/blog/2010/10/optimization-in-kwin-4-6/">performance optimizations</a></li>
    <li>Better detection of graphic driver capabilities.</li>
    <li>Present Windows effect – Windows can now be closed directly from this overview.</li>
</ul>

<p>
Other improvements include:
<ul>
    <li>Program start-up notifications are now handled more efficiently.</li>
    <li>Revamped notifications system.  It is now possible to have the notification popup in any point of the screen by just dragging it, the notification history and jobs popup looks cleaner, and running downloads have gained a download speed plot when the progress notification is expanded.</li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w04.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w04.png" class="img-fluid" alt="You can now drag notifications to a convenient place on the desktop">
	</a> <br/>
	<em>You can now drag notifications to a convenient place on the desktop</em>
</div>
<br/>

<p>
<ul>
    <li><a href="http://i158.photobucket.com/albums/t120/wdawn/sjelf-1.png">Shelf widget</a> - when in a panel: gained automatic sizing of the popup, shows the number of unread mails and online contacts</li>
    <li>The <b>digital clock</b> in the panel was revamped, and now features a shadowed text which improves readability with some backgrounds and themes.</li>
    <li>The <b>The taskbar</b> now has support for basic launchers: Just drag a program icon onto the task bar and it will become a quick launch entry, or pin a running app to the taskbar by right clicking it and using the new entry in the Advanced menu.</li>
    <li><b>Plasma Netbook</b> has been further optimized under the hood for a faster "Search and Launch" experience and more touch-friendly behaviour on the "Page One" newspaper activity.</li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w07.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w07.png" class="img-fluid" alt="The Plasma Netbook Search and Launch is faster">
	</a> <br/>
	<em>The Plasma Netbook Search and Launch is faster</em>
</div>
<br/>

<p>
A variety of other improvements apparent in the Plasma Workspaces include changes to the Oxygen icon theme and better integration of applications not built on the KDE Platform thanks to a completely rewritten Oxygen GTK theme.

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w06.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w06.png" class="img-fluid" alt="The new Oxygen GTK theme helps to integrate non-KDE applications in a Plasma workspace">
	</a> <br/>
	<em>The new Oxygen GTK theme helps to integrate non-KDE applications in a Plasma workspace</em>
</div>
<br/>

</p>

<h4>Installing Plasma</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.6.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.6.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.6.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.6.0">4.6 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.6.0 may be <a
href="http://download.kde.org/stable/4.6.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.6.0
  are available from the <a href="/info/4.6.0#binary">4.6.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.7.2. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>

<h4>
Credits
</h4>
<p>
These release notes have been compiled by Vivek Prakash, Stefan Majewsky, Guillaume De Bure, Nikhil Marathe, Markus Slopianka, Stuart Jarvis, Jos Poortvliet, Nuno Pinheiro, Carl Symons, Marco Martin, Sebastian Kügler, Nick P and many others in the KDE Promotion Team.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Released Today:</h2>

<h3>
<a href="../applications">
KDE’s Dolphin Adds Faceted Browsing
</a>
</h3>

<p>

<a href="../applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.6.0"/>
</a>
</a>
Many of the <b>KDE Application</b> teams have also released new versions. Particular highlights include improved routing capabilities in KDE’s virtual globe, Marble, and advanced filtering and searching using file metadata in the KDE file manager, Dolphin: Faceted Browsing. The KDE Games collection receives many enhancements and the image viewer Gwenview and screenshot program KSnapshot gain the ability to instantly share images to a number of popular social networking sites. For more details read the <a href="../applications">KDE Applications 4.6 announcement</a>.<br /><br />
</p>

<h3>
<a href="../platform">
Mobile Target Makes KDE Platform Lose Weight
</a>
</h3>

<p>

<a href="../platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.6.0"/>
</a>

The <b>KDE Platform</b>, on which the Plasma Workspaces and KDE Applications are built also gains new capabilities available to all KDE applications. The introduction of a "mobile build target" allows for easier deployment of applications on mobile devices. The Plasma framework gains support for writing desktop widgets in QML, the declarative Qt language, and provides new Javascript interfaces for interacting with data. Nepomuk, the technology behind metadata and semantic search in KDE applications, now provides a graphical interface to back up and restore data. UPower, UDev and UDisks can be used instead of the deprecated HAL. Bluetooth support is improved. The Oxygen widget and style set is improved and a new Oxygen theme for GTK applications allows them to seamlessly merge into the Plasma workspaces and look just like KDE applications. For more details read the <a href="../platform">KDE Platform 4.6 announcement</a>.

</p>
