---
title: KDE's Dolphin Brings Faceted Browsing
date: "2011-01-26"
hidden: true
---

<h2>KDE Releases Applications 4.6.0</h2>
<p>
 KDE is happy to announce the release of new versions of many of our applications. From games, education and even small utilities, these applications become more powerful, yet easy to use as they mature in this version. Below are just a few highlights from some of the new applications released today.
</p>
<h2>Dolphin Adds Faceted Browsing</h2>
<p>
The search interfaces of Kfind and Dolphin have been unified in a new, simplified search bar. Faceted browsing makes its public debut in the form of a Filter Panel that enables you to easily browse your indexed files using their metadata. A new sidebar allows for faceted searching, an enhancement to the traditional filebrowsing using metadata as additional filters.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a05.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a05.png" class="img-fluid" alt="Dolphin's faceted browsing lets you use multiple filters to find files by metadata">
	</a> <br/>
	<em>Dolphin's faceted browsing lets you use multiple filters to find files by metadata</em>
</div>
<br/>

<ul>
    <li>Column View usability improves. Columns width is now adjustable automatically or by the user, file selecting using a rubberband is now available and the horizontal scrollbar is no longer needed for navigating neighboring columns.</li>
    <li>To accompany the move of KDE development from SVN to Git, Dolphin features a new Git plugin, allowing updating and commiting from the GUI. The SVN plugin is of course still available.</li>
    <li>Various Improvents to the Service Menus (<a href="http://ppenz.blogspot.com/2010/11/improved-service-menus.html">http://ppenz.blogspot.com/2010/11/improved-service-menus.html</a>)</li>
</ul>

<h2>Kate Gains SQL Client</h2>
Kate, the powerful text-editor has gained a lot of polish and new features with this release. In particular, this new version offers the ability to:
<ul>
    <li>Recover unsaved data for local files on the next Kate startup (swap file support).</li>
    <li>Always load plugins.</li>
    <li>Add scripts to the menu and bind shortcuts.</li>
    <li>Kate's <a href="http://kate-editor.org/2010/07/29/katesql-a-new-plugin-for-kate/">new SQL Query plugin</a> brings features of a basic SQL client to Kate and supports a wide variety of databases through Qt's SQL module.</li>
    <li>A new GNU Debugger (GDB) Plugin</li>
    <li>A new Highlight Selected Text Plugin.</li>
</ul>

<h2>Graphics Applications Go Social</h2>
<p>
Gwenview, the KDE image viewer, gets a “Share” button to advertise its abilities to export pictures to popular photo sharing and social networking web sites.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a03.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a03.png" class="img-fluid" alt="Gwenview can share images to many popular social networking sites">
	</a> <br/>
	<em>Gwenview can share images to many popular social networking sites</em>
</div>
<br/>

<p>
<b>KSnapshot</b> can now lasso regions to snapshot, has an option to include the mouse pointer and also a “Send to” button to instantly share screenshots.</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a04.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a04.png" class="img-fluid" alt="KSnapshot can directly export screenshots to a number of 3rd party services">
	</a> <br/>
	<em>KSnapshot can directly export screenshots to a number of 3rd party services</em>
</div>
<br/>

The many other KDE applications updated today also receive new features and numerous bug fixes, while also gaining from the latest improvements in the KDE Platform to enhance speed and stability.

<h2>Marble Takes You Home, KStars Renders faster thanks to OpenGL</h2>
<p>
Marble, KDE's virtual globe, continues to shine with improved route planning support, now also allowing downloading of routing data. Its mobile version <b>MarbleToGo</b> is now a very capable turn-by-turn navigator. Pictures and videos are available on <a href="http://dot.kde.org/2010/11/10/kdes-marble-team-holds-first-contributor-sprint">dot.kde.org</a>, an illustrated feature guide can be found <a href="http://edu.kde.org/marble/current_1.0">here</a>.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a01.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a01.png" class="img-fluid" alt="Marble’s advanced routing can use a variety of configurable backends">
	</a> <br/>
	<em>Marble’s advanced routing can use a variety of configurable backends</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a06.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a06.png" class="img-fluid" alt="The mobile version of Marble is a capable personal navigation tool">
	</a> <br/>
	<em>The mobile version of Marble is a capable personal navigation tool</em>
</div>
<br/>

<p>
KStars, KDE's desktop planetarium has gained optional support for rendering using openGL, enhancing its performance on hardware with openGL capabilities.</s></li>
</p>

<h2>KDE Games</h2>
<ul>
    <li><b>Klickety</b>, an adaptation of the Clickomania game, makes a comeback in KDE Games 4.6, also replacing KSame through a compatibility mode.</li>
    <li>The KGameRenderer framework unifies theming across games, giving a more consistent and smooth look and feel. About a dozen KDE games have been ported to this new architecture, reducing memory usage and allowing them to take advantage of multicore processors.</li>
    <li>Palapeli's new puzzle slicer splits images into distinctive and authentic-looking pieces, a bevel effect makes pieces appear three-dimensional. Palapeli's “Create New Puzzle” dialog has seen usability improvements.</li>
    <li><b>Kajongg</b>'s improved documentation explains the game to beginners. More intelligent robot player, and improved usability and playability due to better tile-handling make this traditional game more fun to play.</li>
</ul>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a02.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a02.png" class="img-fluid" alt="Palapeli, the KDE puzzle game, makes it easy to create jigsaws from your own images">
	</a> <br/>
	<em>Palapeli, the KDE puzzle game, makes it easy to create jigsaws from your own images</em>
</div>
<br/>

<h4>Installing KDE Applications</h4>

<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.6.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.6.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.6.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.6.0">4.6 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.6.0 may be <a
href="http://download.kde.org/stable/4.6.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.6.0
  are available from the <a href="/info/4.6.0#binary">4.6.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.7.2. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>

<h4>
Credits
</h4>
<p>
These release notes have been compiled by Vivek Prakash, Stefan Majewsky, Guillaume De Bure, Nikhil Marathe, Markus Slopianka, Stuart Jarvis, Jos Poortvliet, Nuno Pinheiro, Carl Symons, Marco Martin, Sebastian Kügler, Nick P and many others in the KDE Promotion Team.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Released Today:</h2>

<h3>
<a href="../plasma">
Plasma Workspaces Put You in Control
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/4/4.6.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.6.0" />
</a>

The<b> KDE Plasma Workspaces</b> gain from a new Activities system, making it easier to associate applications with particular activities such as work or home tasks. Revised power management exposes new features but has a simpler configuration interface. KWin, the Plasma workspace window manager, receives a new scripting and the workspaces receive visual enhancements. <b>Plasma Netbook</b>, optimized for mobile computing devices receives speed enhancements and becomes easier to use via a touchscreen interface. For more details read the <a href="../plasma">KDE Plasma Workspaces 4.6 announcement</a>.

</p>

<h3>
<a href="../platform">
Mobile Target Makes KDE Platform Lose Weight
</a>
</h3>

<p>

<a href="../platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.6.0"/>
</a>

The <b>KDE Platform</b>, on which the Plasma Workspaces and KDE Applications are built also gains new capabilities available to all KDE applications. The introduction of a "mobile build target" allows for easier deployment of applications on mobile devices. The Plasma framework gains support for writing desktop widgets in QML, the declarative Qt language, and provides new Javascript interfaces for interacting with data. Nepomuk, the technology behind metadata and semantic search in KDE applications, now provides a graphical interface to back up and restore data. UPower, UDev and UDisks can be used instead of the deprecated HAL. Bluetooth support is improved. The Oxygen widget and style set is improved and a new Oxygen theme for GTK applications allows them to seamlessly merge into the Plasma workspaces and look just like KDE applications. For more details read the <a href="../platform">KDE Platform 4.6 announcement</a>.

</p>
