---
aliases:
- ../announce-4.6-beta2
date: '2010-12-08'
description: KDE Ships Second Test Release of KDE SC 4.6 Series
title: KDE Software Compilation 4.6 Beta2 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Software Compilation 4.6 Beta2 Released: Codename Caramel
</h3>

<p align="justify">
  <strong>
KDE Community Ships Second Beta Release of the 4.6 Free Desktop, Applications and
Development Platform
</strong>
</p>

<p align="justify">
Today, KDE has released the second beta version of what is to become KDE SC 4.6.0 in January 2011. KDE SC 4.6 Beta2 is targeted at testers and those that would like to have an early look at what's coming to their desktops and netbooks this winter. KDE is now firmly in beta mode, meaning that the primary focus is on fixing bugs and preparing the stable release. Since the release of the first beta two weeks ago 1318 bugs have been reported and 1176 bugs have been closed. Thank you to our testers and developers working on making the final release shine! More testing is in place, however, while the restless developers continue to create a rock-stable 4.6.0.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6-beta2/announce-4.6-beta2.png">
	<img src="/announcements/4/4.6-beta2/announce-4.6-beta2_thumb.png" class="img-fluid" alt="Activities Gallery in 4.6 Beta2">
	</a> <br/>
	<em>Activities Gallery in 4.6 Beta2</em>
</div>
<br/>

<p>
To find out more about the KDE Plasma desktop and applications, please also refer to the
<a href="/announcements/4.5/">4.5.0</a>,
<a href="/announcements/4.4/">4.4.0</a>,
<a href="/announcements/4.3/">4.3.0</a>,
<a href="/announcements/4.2/">4.2.0</a>,
<a href="/announcements/4.1/">4.1.0</a> and
<a href="/announcements/4.0/">4.0.0</a> release
notes.
</p>

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.5.85/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing KDE SC 4.6 Beta2 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.6 Beta2
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/unstable/4.5.85/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.5.85">KDE SC 4.6 Beta2 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.6 Beta2
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE SC 4.6 Beta2 may be <a
href="http://download.kde.org/unstable/4.5.85/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.6 Beta2
  are available from the <a href="/info/4.5.85#binary">KDE SC 4.6 Beta2 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}