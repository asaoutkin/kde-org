---
aliases:
- ../announce-4.2-rc
date: '2009-01-13'
description: KDE Community Ships Release Candidate for Next-Gen Leading Free Software
  Desktop.
title: KDE 4.2 RC Release Announcement
---

<p>FOR IMMEDIATE PUBLICATION</p>

<h3 align="center">
  KDE 4.2 RC Released for Final Testing
</h3>

<p align="justify">
  <strong>
KDE Commmunity Ships Release Candidate of KDE 4.2, codenamed "Cilense"
</strong>
</p>

<p align="justify">
 The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of <em>"Cilense"</em>,
(a.k.a KDE 4.2 Release Candidate), the only planned release candidate for the
KDE 4.2 desktop.
<em>Cilense</em> is aimed at testers and reviewers. It should provide a solid
ground to <a href="http://bugs.kde.org">report</a> last-minute bugs that
need to be tackled
before KDE 4.2.0 is released. Reviewers can use this release candidate
to get a first look at
the upcoming KDE 4.2 desktop which provides significant improvements all over
the desktop and applications. It is not recommended for everyday use, however.
<br />
KDE 4.2.0 will be released in January, 27th 2009, 6
months after KDE 4.1. KDE 4.2.0 will be followed up by a series of monthly
service updates and followed up by KDE 4.3.0 in summer 2009.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2-beta2/panel.png">
	<img src="/announcements/4/4.2-beta2/panel_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The Panel in KDE 4.2 Beta 2</em>
</div>
<br/>

<p>
Features and improvements of KDE 4.2 are covered in the
<a href="/announcements/4/4.2-beta2">4.2 Beta 2 release announcement</a>.
</p>
<p>
To find out more about the KDE 4 desktop and applications, please also refer to
the
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes.
<p />

<h4>
  Installing KDE 4.2 RC Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.2 RC,
 and in other cases community volunteers have done so.
  Some of these binary packages are available for free download via the <a
href="/info/4.1.96#binary">KDE 4.2 RC Info Page</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.1.96">KDE 4.2 RC Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.2 RC
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.1.96 may be <a
href="/info/4.1.96#desktop">freely downloaded</a>.
Instructions on compiling and installing KDE 4.1.96
  are available from the <a href="/info/4.1.96">KDE 4.2 RC Info
Page</a>.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}