---
title: Extended Development Platform
hidden: true
---

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../edu_games"><img src="/announcements/4/4.2.0/images/education-32.png" />Educational Applications and Games</a>      
        </td>
        <td align="right" width="50%">
                <a href="../guide">Overview<img src="/announcements/4/4.2.0/images/star-32.png" /></a>
        </td>
    </tr>
</table>


<p>The KDE development platform contains common UI widgets, but also 
libraries for interaction with hardware, multimedia, configuration and network 
transparency, among others. Some changes affect most or all the KDE applications. 
The most notable of these improvements include:</p>

<p>The <strong>'file open'</strong> and <strong>'file save'</strong> dialogs have been touched up. 
Using the same
infrastructure as Dolphin, they now also include a zoom slider and can show file
previews. Sharing such user interface elements ensures a smooth and consistent
user experience.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/file-open-dialog.png">
	<img src="/announcements/4/4.2.0/file-open-dialog_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The File Open dialog has been further improved</em>
</div>
<br/>


<p>The <strong>Oxygen artwork </strong>continues its path of refinement. In Plasma the panel has a
new (transparent) look. Window shadows now form a blue highlight around the
active window and wobble when you have the wobbly windows effect enabled. Subtle
improvements like the new progress bars and scroll bars can be found everywhere,
but the new wallpapers are probably most visible. Extending the already very
complete collection with some new high quality pieces of art they bring a fresh
look to your desktop.</p>


<h2>KDE on Windows and Mac</h2>

<p>The KDE packages for Windows and Mac are <strong>not fully mature yet</strong>, but getting
better. If you want, you can have a look. You can download the installer from <a
href="http://windows.kde.org">windows.kde.org</a>, choose <em>"Download Installer"</em>
from the download section and start the installer. Go through the dialogs by
using "Next". It is smart to choose a mirror in the server list close to you
to speed up the download of the KDE applications. After that you can choose
which KDE release you want - it is advisable to go for either the latest stable
or the latest unstable release. Next up will be a list of languages and software
you can choose to install. Choose your language by clicking the little box in
front of the line, and choose for example kdegraphics and kdeedu. Going further
clicking <em>"next" </em>will start downloading and installing the choosen applications
and languages. You can now find the KDE applications in a submenu in the Windows
Programs menu. Enjoy!</p>


<p>For KDE applications on the Apple Macintosh go to <a
href="http://mac.kde.org">mac.kde.org</a> and download the installer. After you
have run the installer, you can find the applications in Applications/KDE4. Be
on the lookout for updated packages for the Mac soon!</p>


<h2>Libraries &amp; development platform</h2>

<p>After Qt will be released under the terms of the <strong>LGPL</strong>, both
the KDE libraries and beneath it Qt are available under those more relaxed
licensing terms, making for a more compelling platform for commercial
software development.
</p>

<p>KDE 4.2 introduces experimental <strong>Python bindings support for the Marble widget</strong>
and related classes. This means it is possible for KDE applications written in
Python to make direct use of the Marble widget in their user interface to
display an interactive map and to overlay custom information and rendering on
top of it.</p>


<p>The KrossFalcon binding provides Kross-enabled KDE applications with a powerful,
light and extensive scripting engine, based on <a
href="http://www.falconpl.org">The Falcon Programming Language</a>. The time
needed to load Falcon scripts, and the fast reflection of QT objects into
language variables ensure performance and throughput far above what can be
achieved with other scripting engines. The Falcon binding does this without forfeiting the ability to build complex, stand-alone application-grade scripts. Falcon also provides
the ability to dynamically load and use other scripts and C libraries, that can
thus be seen as plug-ins. You are then fully able to build highly dynamic
applications, with a C++/Qt front-end and a flexible and pluggable engine. And
while libraries and bindings for Falcon are still few, but growing fast, the
language itself has everything you'd expect from a modern scripting language,
and much more.</p>


More highlights of the changes in KDE's development libraries are:
<ul>
    <li>
    With the help of CMake, the reduced library link 
    interface has been reduced, making start-up of applications faster
    </li>
    <li>
    Eigen 2, a C++ template library for linear algebra, such as vectors, matrices
     and related algorithms
    </li>
    <li>
    Sytem Settings module and several other KDE plugins can now be written in Python
    </li>
    <li>
    Phonon's VideoWidget now supports taking snapshots.
    </li>
    <li>
    Various improvements in the Kross Ruby and Python engines, easing
    development of add-ons in these languages
    </li>
    <li>
    Fish and sftp support are introduced in KDE on Windows, making 
    secure file transfer easier
    </li>
</ul>


<p>Documentation about many parts of KDE has been added to <a
href="http://techbase.kde.org">Techbase</a>. We are still a long way from
properly documenting all the new and improved libraries. You can ask for help
and information on the IRC channels and mailinglists. When you figured out how
something works, it would be greatly appreciated if you could turn this learning
experience into documentation for others to ease their learning process! Head on
to <a href="http://techbase.kde.org">Techbase</a> and help your fellow
programmer.</p>


