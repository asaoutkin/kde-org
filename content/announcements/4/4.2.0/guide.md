---
title: KDE 4.2 Visual Guide
hidden: true
---


<p>
Creating software in the open provides a challenge, even for a community with
over 12 years of experience. KDE 4.0 was meant to start the process of
participative development for the KDE 4 series. It was a starting point that
held in embryonic forms all the ambitions of the developer vision, much as a
first draft of a wikipedia page shows the ambition of the writer.
</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/desktop.png">
	<img src="/announcements/4/4.2.0/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The Desktop in KDE 4.2</em>
</div>
<br/>

<p>
With KDE 4.2 we are seeing the positive results of that beginning. Only one year
has passed since 4.0.0 was released and the embryonic concepts have been
blossoming.  Everywhere we look, we see substantial results.
62,460 commits were made by over 627 contributors to the KDE project during the 
six month KDE 4.2 development cycle.
<p />
KDE 4.2.0 is not the end, but another milestone along the road of KDE 4
development. This platform is designed and intended to keep on growing far into
the future, and the KDE Team would like to invite you to join us in this
fantastic journey. This visual guide highlights many of the improvements in KDE
4.2, and we hope that you will enjoy using this release.
</p>


<table  border="0" cellspacing="20" cellpadding="8">
<tr>
    <td width="32">
        <a href="../desktop"><img src="/announcements/4/4.2.0/images/desktop-32.png" /></a>
    </td>
    <td>
        <a href="../desktop"><strong>The desktop workspace</strong>: Plasma,
KWin, KRunner and new System Settings</a>
    </td>
</tr>
<tr>
    <td>
        <a href="../applications"><img src="/announcements/4/4.2.0/images/applications-32.png" /></a>
    </td>
    <td>
        <a href="../applications"><strong>Applications</strong>: Dolphin, Kate,
KMail and Gwenview</a>
    </td>
</tr>
<tr>
    <td>
        <a href="../edu_games"><img src="/announcements/4/4.2.0/images/education-32.png" /></a>
    </td>
    <td>
        <a href="../edu_games"><strong>Educational Applications and
Games:</strong> KStars, KBrunch and Parley and Killbots, Bomber, KDiamond,
Ksirk</a>
    </td>
</tr>
<tr>
    <td>
        <a href="../platform"><img src="/announcements/4/4.2.0/images/platform-32.png" /></a>
    </td>
    <td>
        <a href="../platform"><strong>Development Platform</strong>: Oxygen,
File dialogs, Python bindings, Falcon</a>
    </td>
</tr>
</table>

<p>
<strong>Credits</strong> for this Visual Guide go to Jos Poortvliet, Carl Symons, 
Luca Beltrame, Sebastian Kügler, the KDE Promo Team and several KDE developers who 
have all helped making this overview of new and "old" features in KDE 4.2 possible.
</p>
