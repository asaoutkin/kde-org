---
aliases:
- ../announce-4.14.2
date: '2014-10-14'
description: KDE Ships Applications and Platform 4.14.2.
title: KDE Ships Applications and Platform 4.14.2
---

October 14, 2014. Today KDE released updates for its Applications and Development Platform, the second in a series of monthly stabilization updates to the 4.14 series. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 35 recorded bugfixes include improvements to E-Mail client KMail, Umbrello UML Modeller, the document viewer Okular, the plot drawing application Kmplot and the file manager Dolphin.

{{% i18n_var "A more complete <a href='%[1]s'>list of changes</a> can be found in KDE's issue tracker." "https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2014-01-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=%[2]s.&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=" "4.14.2" %}}

{{% i18n_var "This release also includes <a href='%[1]s'>Plasma Workspaces %[2]s</a>." "https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2013-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.11.13&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=" "4.11.13" %}}

{{% i18n_var "To download source code or packages to install go to the <a href='/info/%[1]s.php'>%[1]s Info Page</a>. If you want to find out more about the 4.14 versions of KDE Applications and Development Platform, please refer to the <a href='/announcements/4.14/'>4.14 release notes</a>." "4.14.2" %}}

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

{{% i18n_var "KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/%[1]s/'>http://download.kde.org</a> or from any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today." "4.14.2" %}}

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.

#### {{% i18n_var "Installing %[1]s Binary Packages" "4.14.2" %}}

<em>Packages</em>.
{{% i18n_var "Some Linux/UNIX OS vendors have kindly provided binary packages of %[1]s for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks." "4.14.2" %}}

<em>Package Locations</em>.
{{% i18n_var "For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='/info/%[1]s.php#binary'>%[1]s Info Page</a>." "4.14.2" %}}

#### {{% i18n_var "Compiling %[1]s" "4.14.2" %}}

{{% i18n_var "The complete source code for %[1]s may be <a href='http://download.kde.org/stable/%[1]s/src/'>freely downloaded</a>. Instructions on compiling and installing %[1]s are available from the <a href='/info/%[1]s.php'>%[1]s Info Page</a>." "4.14.2" %}}

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}