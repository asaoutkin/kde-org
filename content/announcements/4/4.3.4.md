---
aliases:
- ../announce-4.3.4
date: '2009-12-01'
description: KDE Community Ships Fourth Update to KDE SC 4.3 Series.
title: KDE Software Compilation 4.3.4 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Software Compilation 4.3.4 Out Now: Codename "Cold"
</h3>

<p align="justify">
  <strong>
KDE Community Ships Fourth Translation and Service Release of the 4.3
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
 Today, KDE has released a new version of the KDE Software Compilation (KDE SC). <a href="/announcements/4/4.3.4">This month's edition of KDE SC</a> is a bugfix and translation update to KDE SC 4.3. KDE SC 4.3.4 is a recommended upgrade for everyone running KDE 4.3.3 or earlier versions.  As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. Users around the world will appreciate that KDE SC 4.3.4 is more completely translated. KDE 4 is already translated into more than 50 languages, with more to come.
</p>
<p>
KDE SC 4.3.4 has a number of improvements:

<ul>
    <li>A bugfix in Plasma's pixmap cache makes the workspace more responsive</li>
    <li>Okular, the document viewer improved stability in certain situations</li>
    <li>Marble, the desktop globe has seen some polish</li>
    <li>Passphrases with non-ASCII characters have been fixed in the KGpg encryption tool</li>
</ul>
The <a href="/announcements/changelogs/changelog4_3_3to4_3_4">changelog</a> has more, if not exhaustive, lists of the improvements since KDE SC 4.3.3.
</p>

<div class="text-center">
	<a href="/announcements/4/4.3.0/images/kde430-desktop.png">
	<img src="/announcements/4/4.3.0/images/kde430-desktop_thumb.png" class="img-fluid" alt="KDE 4.3.0 Release Notes">
	</a> <br/>
	<em>The KDE Plasma Desktop in 4.3</em>
</div>
<br/>

<p align="justify">
Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE SC 4.3.4, you can browse the Subversion log.
KDE SC 4.3.4 also ships a more complete set of translations for many of the 50+ supported languages.
<p />
To find out more about the KDE 4.3 desktop and applications, please refer to the
<a href="/announcements/4.3/">KDE 4.3.0</a>,
<a href="/announcements/4.2/">KDE 4.2.0</a>,
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE SC 4.3.4 is a recommended update for everyone running KDE 4.3.3 or earlier versions.
<p />

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.3.4/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing KDE SC 4.3.4 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.3.4
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.3.4/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.3.4">KDE SC 4.3.4 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.3.4
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE SC 4.3.4 may be <a
href="http://download.kde.org/stable/4.3.4/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.3.4
  are available from the <a href="/info/4.3.4#binary">KDE SC 4.3.4 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}