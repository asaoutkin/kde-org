---
aliases:
- ../4.2
date: '2009-01-27'
title: Anuncio de Lançamento do KDE 4.2.0
---

<h3 align="center">
  Comunidade KDE melhora a experiência do usuário com o KDE 4.2
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (Codename: <em>"A resposta"</em>) traz uma experiência melhorada para o ambiente de trabalho,
    aplicações e plataforma de desenvolvimento  </strong></p>

<p align="justify">
A <a href="http://www.kde.org/">Comunidade KDE</a> anuncia hoje a disponibilidade imediata de <em>"A resposta"</em>,
(t.c.c KDE 4.2.0), mais um passo na evolu&ccedil;&atilde;o do ambiente de trabalho  em Software Livre. O KDE 4.2 é desenvolvido sobre a tecnologia
introduzida com o KDE 4.0, em Janeiro de 2008. Após o lançamento do KDE 4.1, voltado para usuários casuais,
a Comunidade KDE est&aacute; confiante em finalmente ter desenvolvido um produto adequado para a
maioria dos usuários finais.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/desktop.png">
	<img src="/announcements/4/4.2.0/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The KDE 4.2 Desktop</em>
</div>
<br/>

<h3>
    &Aacute;rea de Trabalho melhorada</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
        Melhorias na interface de usu&aacute;rio do Plasma permitem organizar o seu espaço de trabalho facilmente.
        <strong>Applets novos e melhorados </strong> incluem um lançador rápido, informações do tempo,
        feeds de noticias, quadrinhos e compartilhamento de arquivos rápido via "pastebin". Applets plasma
        podem ser usados agora sobre o protetor de tela, para deixar uma nota enquanto o dono est&aacute; ausente
        por exemplo.
        O Plasma pode agir adionamente tamb&eacute;m como um <strong>desktop tradicional baseado em gerenciamento de arquivos</strong>.
        Previsualizações para os ícones de arquivos e localizações persistentes dos ícones foram adicionadas.<br />
        O painel Plasma <strong>agora agrupa tarefas</strong> e mostra m&uacute;ltiplas colunas. A
        bandeja do sistema melhorada agora <strong>mant&eacute;m sob controle tarefas longas</strong> como downloads. <strong>Notificações</strong> do sistema e tamb&eacute;m das aplicações
        são tamb&eacute;m agrupadas nesta &aacute;rea.
        Para salvar espaço, os ícones da bandeja do sistema agora podem ser escondidos, e todo o painel agora pode ser
        <strong>automaticamente escondido</strong> liberando mais o espaço de tela. Widgets podem ser mostrados
        tanto em pain&eacute;is como na &Aacute;rea de Trabalho.<br />
        </p>
  </li>
    <li>
        <p align="justify">
        KWin oferece um gerenciamento de janelsa harmonioso e eficiente. No KDE 4.2 ele emprega
        <strong>fisica de movimento</strong> para oferecer uma sensação natural para os velhos e
        <strong>novos efeitos</strong> como o  "Cubo" e a "L&acirc;mpada M&aacute;gica". O KWin somente habilita os
        efeitos em sua configuração padrão nos computadores que podem execut&aacute;-los.
        <strong>Uma configuração mais fácil</strong> libera o usuário para selecionar diferentes
        efeitos como o alternador de janelas, fazendo a mudança de janelas mais eficiente.        </p>
  </li>
    <li>
        <p align="justify">
        Novas  ferramentas de espa&ccedil;o de trabalho melhoram a produtividade. PowerDevil facilita
        a vida do usu&aacute;rio de port&aacute;teis trazendo uma <strong>gestão de energia</strong> moderna e não intrusiva
        para laptops e dispositivos móveis. Ark oferece <strong>extração e criação</strong> inteligente de arquivos,
        e as novas ferramentas de impressão permitem ao usuário <strong>gerenciar facilmente suas impressoras</strong>
        e trabalhos de impressão.        </p>
  </li>
</ul>
Além disso, suporte para diversas novas linguagens foi adicionado ao projeto, expandindo o número
de usuários usando o KDE na sua língua nativa em aproximadamente 700 milhões de pessoas.
Novas linguagens suportadas incluem Árabe, Islandês, Basco, Hebraico, Romeno, Tadjique e diversas
línguas Indianas (Bengali Índia, Gujarati, Kannada, Maithili, Marathi) indicando um crescimento de
popularidade nessa parte da Asia.
</p>

<div class="text-center">
	<em>A interface do  Plasma no KDE 4.2</em><br/>
<a href="http://blip.tv/file/get/Sebasje-ThePlasmaDesktopShellInKDE42312.ogv">Ogg Theora version</a></div>
<br/>

<h3>
    Aplicações avançam
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        O Gerenciamento de Arquivos tornou-se rápido e mais eficiente. O gerenciador de arquivos Dolphin agoras
        tem um slider para <strong>ajustar o tamanho dos ícones</strong> facilmente.
        Mais melhorias na interface do usuário incluem  <strong>dicas de contexto com previsualizações</strong>
        e um novo indicador de capacidade para os dispositivos de mídia removível. Estas
        mudanças também foram aplicadas para os <strong>di&aacute;logos de arquivos</strong> no KDE, tornando
        f&aacute;cil encontrar o arquivo certo.        </p>
  </li>
    <li>
        <p align="justify">
        A lista de e-mails do KMail foi retrabalhada por um estudante do Google Summer of Code.
        O usuário agora pode configurar a <strong>visualização de informações adicionais</strong>
        otimizando o fluxo do trabalho para cada pasta individual. Suporte para <strong>IMAP e
        outros protocolos</strong> também foi melhorado tornando o KMail mais rápido.
        </p>
    </li>
    <li>
        <p align="justify">
        Navegação Web torna-se melhor. O browser Konqueror melhora o suporte para
        <strong>scalable vector graphics (SVG)</strong> e recebe muitas melhorias
        de performance. Um <strong>novo di&aacute;logo de procura</strong> torna menos intrusiva a procura
        dentro de páginas web. Konqueror agora <strong>mostra seus favoritos</strong> na inicialização.
        </p>
    </li>
</ul>
</p>

<div class="text-center">
	<em>Gerenciamento de Janelas do KDE 4.2</em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagementInKDE42153.ogv">Ogg Theora version</a></div>
<br/>

<h3>
    Plataforma acelera desenvolvimento
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        <strong>Suporte para scripts melhorado</strong>. Widgets plasma agora podem ser escritos em
        JavaScript, Python e Ruby. Estes widgets podem ser distribuidos on-line via
        serviços web e ferramentas de colaboração on-line como OpenDesktop.org.
        GoogleGadgets agora podem ser usados no Plasma, e o suporte para os Mac OS X
        dashboard widgets foi melhorado.
        </p>
    </li>
    <li>
        <p align="justify">
        Vers&otilde;es preliminares de diversas aplicações KDE para <strong>Windows</strong> e <strong>Mac OS X</strong>
        estão disponíveis: algumas aplicações estão nos seus estágios iniciais de qualidade,
        enquanto outras precisam de algum trabalho adicional no conjunto de recursos implementado.
        O suporte para OpenSolaris está próximo e a um passo da ser classificado como estável.
        E o KDE4 no FreeBSD continua evoluindo para a maturidade.        </p>
  </li>
    <li>
        <p align="justify">
        Assim que o Qt 4.5 estiver dispon&iacute;vel  sob os termos da <strong>LGPL</strong>, ambas
        as bibliotecas  KDE e Qt serão beneficiadas sob os termos dessa licença
        mais relaxada, criando uma plataforma mais competitiva para o
        desenvolvimento de software.        </p>
  </li>
</ul>
</p>

<h4>
  Instalando o KDE 4.2.0
</h4>
<p align="justify">
 KDE, incluindo todas as suas bibliotecas e suas aplicação, estão disponíveis livremente
 sob os licen&ccedil;as livres. O KDE pode ser obtido em código-fonte e em diversos formatos binários em<a 
href="http://download.kde.org/stable/4.2.0/"> http://download.kde.org</a> e podem ser também obtido
 em <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
 ou com qualquer uma dos <a href="http://www.kde.org/download/distributions">principais sistemas GNU/Linux e UNIX</a>
 distribu&iacute;dos hoje.
</p>
<p align="justify">
  <em>Empacotadores</em>.
 Alguns distribuições de OS Linux/UNIX fornecem gentilmente pacotes binários do KDE 4.2.0
 para algumas versões de suas distribuições, e em outros casos voluntários de comunidades
 o fazem.
 Alguns desses pacotes binários estão disponíveis gratuitamente para download em <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">http://download.kde.org</a>.
  Pacotes adicionais binários, como também updates para os pacotes disponíveis,
serão liberados nas próximas semanas.</p>

<p align="justify">
  <a id="package_locations"><em>Localizações dos pacotes</em></a>.
  Para uma lista atualizada dos pacotes binários disponíveis e informados ao projeto, favor visitar a
  <a href="/info/4.2.0">Página de Informações</a>.</p>

<p align="justify">
Problemas de perfomance com os drivers binários da <em>NVidia</em> são <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">resolvidos</a> 
com a &uacute;ltima versão beta dos drivers disponíveis no s&iacute;tio web da NVidia.
</p>

<h4>
  Compilando o KDE 4.2.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  O Código-Fonte completo do KDE 4.2 pode ser <a
href="http://download.kde.org/stable/4.2.0/src/">baixado livremente</a>.
Instruções de compilação estão disponíveis na <a href="/info/4.2.0#binary">Página de Informações</a>.
</p>

<h4>
    Espalhe para o mundo
</h4>
<p align="justify">
O time KDE encoraja todos a espalhar este lan&ccedil;amento para o mundo atrav&eacute;s dos servi&ccedil;os da Web Social.
Envie artigos para websites, use canais como Orkut, delicious, digg, reddit, twitter,
identi.ca. Envie capturas de tela para serviços como Facebook, FlickR, ipernity e Picasa e
poste-os nos seus devidos grupos. Crie v&iacute;deos e envie para o YouTube, Blip.tv, Vimeo e outros.
  Não esqueça de colocar a tag <strong>kde42</strong></em> no seu material, assim fica mais fácil para
  todos encontrarem o material, e para o time KDE compilar os relatórios de mídia deste Anuncio de Lan&ccedil;amento do KDE 4.2.
Esta &eacute; a primeira vez que o time do KDE est&aacute; tentando um esfor&ccedil;o coordenado no uso de ferramentas sociais para a promo&ccedil;&atilde;o do projeto. Nos ajude a espalhar a mensagem, seja parte disso!</p>
<p>
Nos f&oacute;rums web, infome as pessoas sobre os novos recursos do KDE, ajude os outros a conhecer o novo ambiente de trabalho, e nos ajude a espalhar esta not&iacute;cia.</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}