---
aliases:
- ../announce-4.1-rc1
date: '2008-07-15'
description: KDE Community Ships Release Candidate of Major Update to Leading Free
  Software Desktop.
title: KDE 4.1 RC1 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Project Ships Release Candidate for KDE 4.1
</h3>

<p align="justify">
  <strong>
KDE Community Announces The Release Candidate for KDE 4.1</strong>
</p>

<p align="justify">
The <a href="http://www.kde.org/">KDE Community</a> today released the Release
Candidate for KDE 4.1. This release will is the last milestone towards KDE 4.1
due for final release on July 29th 2008, six months after the release of KDE 4.0.
</p>

<h4>
  <a id="changes">KDE 4.1-RC1</a>
</h4>
<p align="justify">
KDE 4.1-rc1 is meant
for testing the upcoming KDE 4.1 release and to filter out last-minute bugs. As
such, the KDE 4.1 is frozen for commits unless the patches have been reviewed
and are bugfix only. This way, regressions are kept at a bare minimum. For more information about what to expect in KDE 4.1, please refer to the announcement pages for
<a href="/announcements/4/4.1-beta1">Beta1</a> and
<a href="/announcements/4/4.1-beta2">Beta2</a>.
</p>

<h4>
  <a id="changes">Known problems</a>
</h4>
<p align="justify">
<ul>
    <li>Users of <strong>NVidia</strong> cards with the binary driver provided
    by NVidia might suffer from performance problems in window switching and
    resizing. We've made the NVidia engineers aware of those problems, no
    fixed NVidia driver has been released yet, however. You can find information
    how to improve graphics performance on
    <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a>,
    although we ultimately have to rely on NVidia to fix their driver.</li>
</ul>

</p>

<h4>
  Get it, run it, test it
</h4>
<p align="justify">
  Community volunteers and Linux/UNIX OS vendors have kindly provided binary packages
  of KDE 4.0.98 (RC1) for some Linux distributions, and Mac OS X and Windows.  Be
  aware that these packages are not considered ready for production use.  Check your
  operating system's software management system.
</p>
<h4>
  Compiling KDE 4.1 Release Candidate 1 (4.0.98)
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 4.0.98 may be <a
  href="/info/4.0.98">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.98
  are available from the <a href="/info/4.0.98">KDE 4.0.98 Info
  Page</a>, or on <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}