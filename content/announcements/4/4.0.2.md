---
aliases:
- ../announce-4.0.2
date: '2008-03-05'
description: KDE Community Ships Second Maintenance Update for Fourth Major Version
  for Leading Free Software Desktop.
title: KDE 4.0.2 Release Announcement
---

<h3 align="center">
  KDE Project Ships Second Translation and Service Release for Leading Free Software Desktop
</h3>

<p align="justify">
  <strong>
    KDE Community Ships Second Translation and Service Release of the 4.0
Free Desktop, Containing New Features in the Plasma Desktop And Numerous Bugfixes, 
Performance Improvements and Translation Updates
</strong>
</p>

<p align="justify">
The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of KDE 4.0.2, the second
bugfix and maintenance release for the latest generation of the most advanced and powerful
free desktop. KDE 4.0.2 ships with a basic
desktop and many other packages like administration, network, education,
utilities, multimedia, games, artwork, web development and more. KDE's
award-winning tools and applications are available in almost 49 languages.
</p>
<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.0.2/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  <a id="changes">Enhancements</a>
</h4>
<p align="justify">

KDE 4.0.2 is a maintenance release which provides corrections of problems
reported using the <a href="http://bugs.kde.org/">KDE bug tracking system</a>
and enhanced support for existing and new translations. For KDE 4.0.2, the
Documentation and Translation teams have made an exception to the usual policy of
not changing user-visible strings, so the Plasma developers could introduce some
new features for this KDE version.
<br />
Improvements in this release include, but are not limited to:

</p>
<ul>
    <li>
    New features in Plasma. The size and position of the panel can now be configured,
    the visibility of a number of option is now improved to makes it easier for new
    users to discover how Plasma works and what it provides.
    </li>
    <li>
    Farsi and Icelandic version of KDE 4.0.2 further improve localisation, other 
    translations have been improved further. KDE 4.0.2 is available in 49 languages.
    </li>
    <li>
    Repainting issues have been addressed in the KHTML webbrowser engine. KHTML now
    also supports more websites by accepting more non-HTML4-compliant documents.
    </li>
</ul>

<p align="justify">
Several stability issues have been fixed  in Kopete, KDE's instant messenger and Okular,
the document reader.</p>

<p align="justify">
 For a more detailed list of improvements since the KDE 4.0.1 release last month, 
 please refer to the <a href="/announcements/changelogs/changelog4_0_1to4_0_2">KDE
4.0.2 Changelog</a>.
</p>

<p align="justify">
 Additional information about the enhancements of the KDE 4.0.x release series
is available in the <a href="/announcements/4.0/">KDE 4.0
announcement</a> and in the <a href="/announcements/4/4.0.1">KDE 4.0.1
announcement</a>.
</p>

<h4>
  Installing KDE 4.0.2 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.0.2
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.2/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.0.2">KDE 4.0.2 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.0.2
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 4.0.2 may be <a
href="http://download.kde.org/stable/4.0.2/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.2
  are available from the <a href="/info/4.0.2#binary">KDE 4.0.2 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
project that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>About KDE 4</h4>
<p>
KDE 4.0 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop
shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The
Dolphin file manager, the Okular document reader and the System Settings control
center complete the basic desktop set. 
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}