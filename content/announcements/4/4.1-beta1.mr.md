---
aliases:
- ../announce-4.1-beta1.mr
date: '2008-05-27'
title: KDE 4.1 Beta1 Release Announcement
---

<h3 align="center">
  KDE प्रकल्प KDE 4.1 च्या प्रथम बीटाचे प्रकाशन करत आहे
</h3>

<p align="justify">
  <strong>
KDE मंडळ KDE 4.1 च्या प्रथम बीटा प्रकाशनाची घोषणा करत आहे</strong>
</p>

<p align="justify">
मे 27, 2008 (महाजाळ).  
<a href="http://www.kde.org/">KDE प्रकल्प</a> KDE 4.1 च्या प्रथम बीटा प्रकाशनाची घोषणा करत आहे.  बीटा 1 चाचणीकर्ता, मंडळ सदस्य व उत्साही वापरकर्त्यांकरीता त्रुटी ओळख व पुन्ह दाखलन या विषयांवर केंद्रीत आहे, व जेणेकरूण वापरकर्ता KDE 3 ऐवजी 4.1 चा पूर्णपणे वापर करू शकेल.  KDE 4.1 बीटा 1 विविध प्लॅटफॉर्मकरीता  बायनरी संकुल, व स्त्रोत संकुल स्वरूपात उपलब्ध आहे. KDE 4.1 चे अखेरचे प्रकाशन जुलै 2008 मध्ये प्रकाशीत केले जाईल.
</p>


<h4>
  <a id="changes">KDE 4.1 बीटा 1 विशेष</a>
</h4>

<p align="justify">
<ul>
    <li>डेस्कटॉप कार्यपध्दती व संयोजना वाढविण्यात आली आहे
    </li>
    <li>KDE व्यक्तिगत माहिती व्यवस्थापन संकुल KDE 4 करीता समाविष्ठ केले गेले आहे
    </li>
    <li>बरेच नविन व अलिकडील अनुप्रयोग समाविष्ठीत आहे
    </li>
</ul>
</p>

<h4>
  Plasma चा वाढीव वापर
</h4>
<p align="justify">
Plasma, नविन प्रणाली आहे जे मेन्यु, पटल व डेस्कटॉप निर्मीती करीता मदत पुरविते, व हळुवारपणे प्रधान्यता प्राप्त करीत आहे. बहु व पुन्ह आकार देण्याजोगी पटल करीता आता समर्थन पुरविला गेला आहे. अनुप्रयोग प्रारंभ मेन्यु, किकऑफ, यांस अनेक सुधारणांसह कार्यक्षम करण्यात आले आहे. कुशल वापरकर्त्यांना रन आदेश संवाद पासून अनुप्रयोग पटकन दाखल करण्यास, दस्तऐवज उघडण्यास व स्थळ करीता भेट देण्यास सहमती दिले गेली आहे. चौकट व्यवस्थापनाच्या कार्यक्षमतेत वाढ व कार्यस्थळ करीता देखणीय घटकं पुरविले गेले आहे, alt-tab गुणविशेष व हलणारे चौकट प्रभाव असे जरूरीचे गुणविशेष  समाविष्ठीत केले गेले आहे.
</p>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-krunner.png">
	<img src="/announcements/4/4.1-beta1/plasma-krunner-small.png" class="img-fluid" alt="The new KRunner alt-F2 dialog">
	</a> <br/>
	<em>The new KRunner alt-F2 dialog</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-panelcontroller.png">
	<img src="/announcements/4/4.1-beta1/plasma-panelcontroller-small.png" class="img-fluid" alt="Panel management returns">
	</a> <br/>
	<em>Panel management returns</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-coverswitch.png">
	<img src="/announcements/4/4.1-beta1/kwin-coverswitch-small.png" class="img-fluid" alt="KWin's Cover Switch effect">
	</a> <br/>
	<em>KWin's Cover Switch effect</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly1.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly1-small.png" class="img-fluid" alt="Windows that wobble">
	</a> <br/>
	<em>Windows that wobble</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly2.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly2-small.png" class="img-fluid" alt="More wobbling windows">
	</a> <br/>
	<em>More wobbling windows</em>
</div>
<br/>
<h4>
  Kontact पुनरागमन
</h4>
<p align="justify">
Kontact, KDE व्यक्तिगत माहिती व्यवस्थापक, व संबंधित साधन KDE 4 करीता पोर्ट केले गेले आहे व KDE 4.1 सह पहिल्यांदा प्रकाशीत केले जाईल. KDE 3 एन्टरप्राईज शाखे पासून बरेचशे गुणविशेष समाविष्ठ केले गेले आहे, 
ज्यामुळे Kontact चा वापर व्यापार संयजोना मध्ये जास्त पहायला मिळेल. नविन घटकांमध्ये KTimeTracker व टिपण्णी-लेखन करीता KJots सारखे घटक समाविष्ठीत आहे, नविन देखणीय रूप सह अनेक दिनदर्शिका व वेळक्षेत्र करीता उत्तम समर्थन व जास्त मजबूत ईमेल हाताळणी पुरविली गेली आहे.</p>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-calendar.png">
	<img src="/announcements/4/4.1-beta1/kontact-calendar-small.png" class="img-fluid" alt="Multiple calendars in use">
	</a> <br/>
	<em>Multiple calendars in use</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-kjots.png">
	<img src="/announcements/4/4.1-beta1/kontact-kjots-small.png" class="img-fluid" alt="The KJots outliner">
	</a> <br/>
	<em>The KJots outliner</em>
</div>
<br/>

<h4>
KDE 4 अनुप्रयोगांमध्ये वाढ
</h4>
<p align="justify">
KDE मंडळ अंतर्गत, बरेचशे अनुप्रयग आता KDE 4 करीता पोर्ट केले गेले आहे किंवा KDE 4 प्रकाशीत झाल्या पासून कार्यपध्दतीत लक्षणीय वाढ झाली आहे.  Dragon प्लेयर, एक मिडीया वादक, पहिल्यांदा दाखल करण्यात आले आहे.  KDE CD वादकाचे पुनरागमन झाले आहे. नविन छपाईयंत्र ऍपलेट Free Software डेस्कटॉपवर छपाईयंत्र कार्यक्षमता पुरवितो.  Konqueror ला वेब संचार सत्र, बदल पुन्ह प्राप्ती पध्दती, व उत्म संचारन करीता समर्थन प्राप्त झाले आहे.  Gwenview मध्ये पूर्ण-पडदा संवाद सह चित्र संचारन पध्दती जोडली गेली आहे.  Dolphin, फाइल व्यवस्थापकात, टॅब्ड् दर्शन जोडले गेले आहे, व KDE 3 वापरकर्ता द्वारे येथे प्रतिकृत करा, व संचयीका वृक्ष देखिल समाविष्ठ केले गेले आहे.  बरेचशे अनुप्रयोग, डेस्कटॉप व KDE शैक्षणीक अनुप्रयोग विषयी, आता खालिल अनुक्रम जसे की चिन्ह, सुत्रयोजना, नकाशा, व विषयाची माहिती Get New Stuff द्वारे मिळविली जाते, तसेच त्याचा संवादपट देखिल देखनीय  करण्यात आला आहे.  बरेचशे खेळ व उपअनुप्रयोगां मध्ये Zeroconf नेटवर्कींग जोडली गेली आहे, तसेच दूर्रस्थ प्रवेश व खेळ या माघची कामगिरीही पहायला मिळते.
</p>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dolphin-treeview.png">
		<img src="/announcements/4/4.1-beta1/dolphin-treeview-small.png" class="img-fluid" alt="Dolphin's tree view">
		</a> <br/>
		<em>Dolphin's tree view</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dragonplayer.png">
		<img src="/announcements/4/4.1-beta1/dragonplayer-small.png" class="img-fluid" alt="Dragon media player">
		</a> <br/>
		<em>Dragon media player</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kdiamond.png">
		<img src="/announcements/4/4.1-beta1/games-kdiamond-small.png" class="img-fluid" alt="KDiamond puzzle game">
		</a> <br/>
		<em>KDiamond puzzle game</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kubrick.png">
		<img src="/announcements/4/4.1-beta1/games-kubrick-small.png" class="img-fluid" alt="The 80s on your desktop">
		</a> <br/>
		<em>The 80s on your desktop</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/konqueror.png">
		<img src="/announcements/4/4.1-beta1/konqueror-small.png" class="img-fluid" alt="Konqueror browser">
		</a> <br/>
		<em>Konqueror browser</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/marble-openstreetmap.png">
		<img src="/announcements/4/4.1-beta1/marble-openstreetmap-small.png" class="img-fluid" alt="Marble showing OpenStreetMaps">
		</a> <br/>
		<em>Marble showing OpenStreetMaps</em>
	</div>
	<br/>
<h4>
संरचना मधिल शुध्दीकरण
</h4>
<p align="justify">
डेव्हलपअर्स् यांनी मुळ KDE लायब्ररी व संरचना प्रगत करण्यावर कार्य केले. KHTML ला स्त्रोत दाखलन पासून फायदा झाला, तर WebKit, उपघटकांस, KDE मध्ये OSX डॅशबोर्ड नियंत्रण घटकाच्या वापर करीता Plasma शी जोडले गेले. Qt 4.4 च्या कॅनवास गुणविशेषतील नियंत्रण घटकाचा वापर केल्यास Plasma आणखी स्थीर व हलके झाले.  KDE च्या विशेष एकवेळा-क्लिक आधारीत संवादपटात नविन निवडक कार्यपध्दती आहे जी गतिक व प्रवेशीय आहे.  Phonon, बहुप्लॅटफॉर्म मिडीया मांडणी, यांस उपशिर्षक आधार व GStreamer, DirectShow 9 व QuickTime बॅकएन्डचे समर्थन प्राप्त झाले. संजाळ व्यवस्थापन स्थर NetworkManager च्या विविध आवृत्ती करीता समर्थन पुरविला गेला आहे.  Free Desktop चे तत्व पाळले आहे म्हणजेच, बहु डेस्कटॉप प्रयत्न, जसे की पॉपअप सूचना संयोजना व freedesktop.org वरील डेस्कटॉप ओळखचिन्ह संयोजना करीता समर्थन पुरविले गेले आहे, त्यामुळेच डेस्कटॉप वरील KDE 4.1 सत्रा मध्ये अन्य अनुप्रयोगांना योग्यरित्या चालण्यास मदत प्राप्त झाली आहे.
</p>

<h4>
  KDE 4.1 अखेरचे प्रकाशन
</h4>
<p align="justify">
KDE 4.1 चे अखेरचे प्रकाशन जुलै 29, 2008 ला प्रकाशीत होईल.  हे प्रकाशन KDE 4.0 च्या प्रकाशन पासून सहा महिणे नंतर होत आहे.
</p>

<h4>
  मिळवा, चालवा, तपासणी करा
</h4>
<p align="justify">
मंडळातील प्रतिनिधी व Linux/UNIX OS विक्रेता यांनी विनम्रपणे अनेक Linux वितरण करीता व Mac OS X आणि Windows करीता KDE 4.0.80 (बीटा 1) चे बायनरी संकुल पुरविले आहे.  हे संकुल, वापरणी करीता तयार नसल्याने वापर करतेवेळी खबरदारी बाळगा.  तुमच्या वितरकाने दिलेल्या सॉफ्टवेअर व्यवस्थापन प्रणालीची तपासणी करा किंवा वितरण विषयी सूचनांकरीता खालिल लींक पहा:</p>

<ul>
<li><a href="http://fedoraproject.org/wiki/Releases/Rawhide">Fedora</a></li>
<li><em>Debian</em> चे KDE 4.1beta1 <em>चाचणी पध्दतीत आहे</em>.</li>
<li><em>Kubuntu</em> संकुल निर्मीती करत आहे.</li>
<li><a href="http://wiki.mandriva.com/en/2008.1_Notes#Testing_KDE_4">Mandriva</a></li>
<li><a href="http://en.opensuse.org/KDE4#KDE_4_UNSTABLE_Repository_--_Bleeding_Edge">openSUSE</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">Windows</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Mac_OS_X">Mac OS X</a></li>
</ul>

<h4>
  KDE 4.1 बीटा 1 कंपाई पध्दती (4.0.80)
</h4>
<p align="justify">
  <a id="source_code"></a><em>सोअर्स कोड</em>.
  KDE 4.0.80 चे पूर्ण सोअर्स कोड <a
  href="/info/4.0.80"> येथून मोफत डाऊनलोड करा</a>.
KDE 4.0.80 कंपाईल व प्रतिष्ठापन सूचनांकरीता
<a href="/info/4.0.80">KDE 4.0.80 माहिती पान</a>, किंवा <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a> येथे भेट द्या.
</p>

<h4>
  KDE समर्थन
</h4>
<p align="justify">
 KDE हा <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
प्रकल्प आहे ज्याची वाढ फक्त त्या प्रतिनिधींमुळे होते जे त्यांचा वेळ आणि मेहनत या प्रकल्पात देतात. KDE नेहमी नविन प्रतिनिधी व सहभागीय मदत करीता उत्सुक असते, मग ती कोडींग मधिल मदत असो, त्रुटी निर्धारण किंवा त्रुटी ओळख, दस्तऐवज लेखन, भाषांतरण, पदोन्नती, पैसा, इत्यादी मदत असो.  सर्व सहभागीय कार्यांची स्तुतीच नव्हे तर त्यांस स्वीकारलेही जाते. कृपया अधिक माहिती करीता <a href="/community/donations/">समर्थन करीताचे KDE पान</a> वाचा. </p>

<p align="justify">
आम्ही तुमच्याकडून लवरकरच प्रतिसादाची अपेक्षा करतो!
</p>

<h4>KDE 4 विषयी</h4>
<p align="justify">
KDE 4 नाविन्य Free Software डेस्कटॉप आहे ज्यात दैनंदिक व ठराविक वापर करीता अनेक अनुप्रयोग समाविष्ठीत आहे. Plasma KDE 4 करीता एक नविन डेस्कटॉप शेल आहे, जे डेस्कटॉप व अनुप्रयोग यांच्या अंतर्गत एक संवादपट आहे. Konqueror वेब ब्राऊजर, वेब ला डेस्कटॉपसह एकत्रीत करतो. तसेच Dolphin फाइल व्यवस्थापक, Okular दस्तऐवज वाचक व प्रणाली संयोजना नियंत्रण केंद्र मुळ डेस्कटॉप संच पूर्ण करतो.
<br />
KDE हे KDE लायब्ररी वर आधारीत आहे जे संजाळवरील स्त्रोत करीता KIO व Qt4 द्वारे प्रगत देखनीय क्षमतानुरूप स्त्रोतसाठी सोपे प्रवेश पुरवितो. Phonon व Solid, दोन्ही KDE लायब्ररीचे भाग आहेत जे सर्व KDE अनुप्रयोग करीता मल्टिमिडीया मांडणी व उत्तम हार्डवेअर एकाग्रता पुरवितात.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}