---
title: Plataforma KDE Platform – Melhor Interoperabilidade, Desenvolvimento Mais Fácil
date: "2012-12-05"
hidden: true
---

<p>
O KDE orgulhosamente anuncia o lançamento da Plataforma KDE 4.9. A Plataforma KDE é a base para o Espaço de Trabalho do Plasma e os Aplicativos do KDE. A Equipe KDE está trabalhando atualmente na próxima iteração da Plataforma de Desenvolvimento do KDE denominada "Frameworks 5". Enquanto o Frameworks 5 será em grande parte compatível a nível de código fonte, ele será baseado no Qt5. Ele fornecerá mais granularidade, tornando mais fácil usar partes da plataforma de maneira seletiva e reduzindo as dependências consideravelmente. Como resultado, a Plataforma KDE está praticamente congelada. A maior parte do esforço na Plataforma será na correção de erros e melhoria de desempenho.
</p>
<p>
Tem havido progresso na separação do QGraphicsView do Plasma para criar um caminho para um Plasma puramente baseado em QML e melhorias posteriores no suporte ao Qt Quick nas bibliotecas do KDE e no Espaço de Trabalho do Plasma.
</p>
<p>
Algum trabalho de baixo nível foi feito nas bibliotecas do KDE na área de rede. O acesso aos compartilhamentos de rede se tornou muito mais rápido para todos os aplicativos do KDE. Conexões NFS, Samba e SSHFS e KIO não mais contarão itens na pasta, tornando-as mais rápidas. O protocolo HTTP foi agilizado evitando-se acessos desnecessários ao servidor. Isto é especialmente notado em aplicativos de rede como o Korganizer, que está cerca de 20% mais rápido devido a esta correção. Existe também um armazenamento melhorado de senhas para compartilhamentos na rede.
</p>
<p>
Muitos erros críticos foram corrigidos no Nepomuk e Soprano, tornando a pesquisa mais rápida e confiável para Aplicativos e o Espaço de Trabalho compilado na Plataforma KDE 4.9. A melhoria na performance e estabilidade foram os principais objetivos destes projetos para este lançamento.
</p>

<h4>Instalando a Plataforma de Desenvolvimento KDE</h4>

<p align="justify">
O software do KDE, incluindo todas as bibliotecas e seus aplicativos, está disponível livremente sob licenças de Código Aberto. O software KDE é executado em várias configurações de hardware e arquiteturas de CPU como a ARM e a x86, sistemas operacionais e funciona com qualquer tipo de gerenciador de janelas e ambiente de desktop. Além do Linux e outros sistemas operacionais baseados em UNIX, você pode encontrar versões para o Microsoft Windows da maioria dos aplicativos do KDE no site <a href="http://windows.kde.org">Software KDE no Windows</a> e para versões do Apple Mac OS X no site <a href="http://mac.kde.org/">Software KDE no Mac</a>. Compilações experimentais dos aplicativos do KDE para diversas plataformas móveis como o MeeGo, MS Windows Mobile e Symbian podem ser encontradas na Web mas elas não são atualmente suportadas. O <a href="http://plasma-active.org">Plasma Active</a> é uma experiência de usuário para um grande espectro de dispositivos, como tablet e outros dispositivos móveis.
<br />
O software KDE pode ser obtido em código ou vários formatos binários a partir de <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a> e pode
também ser obtido em <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
ou com qualquer dos <a href="http://www.kde.org/download/distributions">principais 
sistemas GNU/Linux e UNIX</a> distribuídos hoje.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Alguns distribuidores do Linux/UNIX gentilmente fornecem pacotes binários do 4.9.0 
para algumas versões de suas distribuições, e em outros casos voluntários da comunidade têm
feito isto. <br />
  Alguns destes pacotes binários estão disponíveis para download livre no servidor KDE <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">http://download.kde.org</a>.
  Pacotes binários adicionais, bem como atualizações dos pacotes disponíveis, se tornarão disponíveis
nas próximas semanas.
<a id="package_locations"><em>Localizações dos pacotes</em></a>.
Para uma lista atual dos pacotes binários disponíveis os quais a Equipe de Lançamento do KDE foi informada,
por favor visite a <a href="/info/4.9.0">4.9 Página de
Informações</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  O código-fonte completo para o 4.9.0 pode ser <a href="/info/4.9.0">baixado livremente</a>.
Instruções para compilação e instalação do software KDE 4.9.0
  estão disponíveis na <a href="/info/4.9.0#binary">4.9.0 Página de Informações</a>.
</p>

<h4>
Requisitos do Sistema
</h4>
<p align="justify">
Para obter o melhor destes lançamentos, nós recomendamos usar uma versão recente do Qt, seja a 4.7.4 ou a 4.8.0. Isto é necessário para garantir uma experiência estável e com boa performance, bem como algumas melhorias feitas no software do KDE que foram construídas com base no framework do Qt.<br />
Para obter um uso completo das capacidades do software KDE, nós também recomendamos usar os últimos drivers gráficos para o seu sistema, uma vez que isso aprimora a experiência do usuário de maneira substancial, tanto nas funcionalidades opcionais como na performance e estabilidade geral.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Também anunciado hoje:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Espaço de Trabalho do Plasma – Principais Melhorias</a></h2>

<p>
Os destaques para o Espaço de Trabalho do Plasma incluem melhorias substanciais no Gerenciador de Arquivos Dolphin, no Emulador de Terminal X do Konsole, nas Atividades e no Gerenciador de Janelas KWin. Leia os detalhes no <a href="../plasma">'Anúncio do Espaço de Trabalho do Plasma'.</a>
</p>

<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Novidades e Melhorias nos Aplicativos do KDE</a></h2>

<p>
As novidades e melhorias nos Aplicativos do KDE lançados hoje incluem o Okular, o Kopete, o KDE PIM, os aplicativos educacionais e os jogos. Leia os detalhes no <a href="../applications">'Anúncio dos Aplicativos do KDE'</a>
</p>
