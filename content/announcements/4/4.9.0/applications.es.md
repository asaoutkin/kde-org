---
title: Las aplicaciones de KDE 4.9 están más pulidas y son más estables
date: "2012-12-05"
hidden: true
---

<p>
KDE se congratula de anunciar el lanzamiento de versiones mejoradas de varias aplicaciones populares. Esto incluye muchas importantes herramientas básicas y aplicaciones como Okular, Kopete, KDE PIM y aplicaciones y juegos educativos.
</p>
<p>
El visor de documentos de KDE, Okular, ya puede almacenar e imprimir notas con los documentos PDF. La búsqueda, los marcadores y la selección de texto se han mejorado. Okular se puede configurar para que un portátil no se duerma ni apague la pantalla durante una presentación, y también puede reproducir películas empotradas en archivos PDF. El visor de imágenes Gwenview presenta una nueva opción de navegación a pantalla completa, ha corregido un buen número de problemas y contiene mejoras menores.
</p>
<p>
Juk, el reproductor de música por omisión de KDE, proporciona integración con los perfiles de gustos musicales y la obtención de carátulas de last.fm, así como el uso de MPRIS2. Es capaz de leer las carátulas insertadas en los archivos MP4 y AAC. Dragon, el reproductor de vídeo de KDE, también funciona ahora con MPRIS2.
</p>
<p>
El versátil cliente de charla Kopete puede agrupar todos los usuarios sin conexión en un único grupo de «Usuarios sin conexión» y mostrar los cambios de estado de los contactos en las ventanas de charla. Posee una nueva opción para cambiar el nombre de un contacto y permite mostrar de forma personalizada los cambios de nombre en línea.
</p>
<p>
La aplicación de traducción Lokalize ha mejorado la búsqueda de mensajes dudosos y contiene un mejor apartado de búsqueda en archivos. Ahora también puede manejar archivos .TS. Umbrello posee diseños automáticos para los diagramas y puede exportar dibujos «graphviz dot». Okteta introduce un visor de perfiles, incluyendo un editor/gestor.
</p>
<h2>La suite Kontact</h2>
<p>
Kontact, la suite PIM más completa del mundo, ha recibido muchas correcciones de errores y mejoras de rendimiento. Esta versión introduce un asistente de importación para obtener preferencias, correos, filtros, calendarios y entradas de la libreta de contactos de Thunderbird y Evolution en KDE PIM. Contiene una herramienta que puede hacer copias de seguridad de los mensajes, la configuración y los metadatos, y restaurarlas. KTnef, el visor aislado de adjuntos TNEF, ha sido revivido de los archivos de KDE 3. Los recursos de Google se pueden integrar con KDE PIM, dando acceso a los usuarios a sus contactos y datos de calendario de Google.
</p>
<h2>Los programas educativos de KDE</h2>

<p>
KDE-Edu introduce Pairs, un nuevo juego de memoria. Rocs, la aplicación de teoría de grafos para estudiantes y profesores, ha ganado un buen número de mejoras. Los algoritmos se pueden ejecutar ahora paso a paso, el sistema de deshacer y cancelar la construcción funciona mejor y se permiten grafos superpuestos. Kstars ha mejorado la ordenación por hora de tránsito del meridiano y por hora de observación, y también la <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">obtención de imágenes</a> de Digital Sky Survey.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-pairs.png">
	<img src="/announcements/4/4.9.0/kde49-pairs-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<p>
Marble contiene optimizaciones de velocidad y ejecución multihilo, y también se ha mejorado su interfaz de usuario. Las extensiones de rutas de Marble incluyen ahora OSRM (Open Source Routing Machine), implementación de rutas en bicicleta y a pie, y un modelo de datos sin conexión para gestionar rutas y búsqueda de datos sin conexión. Marble puede mostrar posiciones de aviones en el simulador FlightGear.
</p>
<h2>Los juegos de KDE</h2>
<p>
Los juegos de KDE han sido actualizados. Kajongg, el juego de mahjong de KDE, se ha pulido bastante, e incluye consejos emergentes de juego, IA robótica mejorada y charla con los jugadores que estén en el mismo servidor (kajongg.org tiene uno ahora). KGoldrunner contiene un buen número de nuevos niveles (del colaborador Gabriel Miltschitzky) y KPatience retiene el historial del juego cuando se guarda. KSudoku contiene mejoras menores, como mejores pistas, así como siete nuevos rompecabezas de formas de dos dimensiones y tres de formas de tres dimensiones.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai.png">
	<img src="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>
</p>

<h4>Instalación de aplicaciones de KDE</h4>

<p align="justify">
El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href="http://windows.kde.org">KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href="http://mac.kde.org/">KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no tienen soporte. <a href="http://plasma-active.org">Plasma Active</a> es una experiencia de usuario para un amplio abanico de dispositivos, como tabletas y otro tipo de hardware 
móvil.
<br />
Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a>, 
y también en <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
o con cualquiera de los <a href="http://www.kde.org/download/distributions">principales
sistemas GNU/Linux y UNIX</a> de la actualidad.
</p>
<p align="justify">
  <a id="packages"><em>Paquetes</em></a>.
  Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 4.9.0 
para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad
los que lo han hecho. <br />
  Algunos de estos paquetes binarios están disponibles para su libre descarga en el sitio web de KDE <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">http://download.kde.org</a>.
  Paquetes binarios adicionales, así como actualizaciones de los paquetes ya disponibles,
estarán disponibles durante las próximas semanas.
<a id="package_locations"><em>Ubicación de los paquetes</em></a>.
Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha
sido notificado, visite la <a href="/info/4.9.0">Página de información sobre 4.9</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  La totalidad del código fuente de 4.9.0 se puede <a href="/info/4.9.0">descargar libremente</a>.
Dispone de instrucciones sobre cómo compilar e instalar el software de KDE 4.9.0
  en la <a href="/info/4.9.0#binary">Página de información sobre 4.9.0</a>.
</p>

<h4>
Requisitos del sistema
</h4>
<p align="justify">
Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, ya sea la 4.7.4 o la 4.8.0. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.<br />
Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>También se han anunciado hoy:</h2>
<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Espacios de trabajo Plasma 4.9 – Mejoras globales</a></h2>
<p>
Lo más destacado de los espacios de trabajo Plasma incluye mejoras sustanciales en el gestor de archivos Dolphin, el emulador de terminal de X Konsole, las Actividades y el gestor de ventanas KWin. Lea el <a href="../plasma">«Anuncio de los espacios de trabajo Plasma»</a>.
</p>

<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>Plataforma de KDE 4.9</a></h2>
<p>
El lanzamiento de la plataforma de KDE de hoy incluye corrección de errores, otras mejoras de calidad, redes y preparación para Frameworks 5.
</p>
