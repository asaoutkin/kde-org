---
title: Plasma Workspaces 4.9 – Core Component Improvements
date: "2012-12-05"
hidden: true
---

<p>
KDE is happy to announce the immediate availability of version 4.9 of both the Plasma Desktop and Plasma Netbook Workspaces. There have been improvements to existing Plasma Workspace functionality, and significant new features have been introduced as well.
</p>
<h2>Dolphin File Manager</h2>
<p>
KDE's powerful file manager Dolphin now includes back and forward buttons and in-line file renaming is back. Dolphin can show metadata such as ratings, tags, image and file sizes, author, date, and more as well as  grouping and sorting by metadata properties. The new Mercurial plugin handles this versioning system in the same convenient way git, SVN and CVS are supported, so users can do pulls, pushes and commits right from the file manager. The Dolphin User Interface has seen several smaller improvements, including a better Places panel, improved search support and synchronization with the terminal location.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-dolphin_.png">
	<img src="/announcements/4/4.9.0/kde49-dolphin_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Konsole X Terminal Emulator</h2>
<p>
The workhorse Konsole now has the ability to search for a text selection using KDE Web Shortcuts. It offers the 'Change Directory To' context option when a folder is dropped on the Konsole window. Users have more control for organizing terminal windows by <strong>detaching tabs</strong> and dragging them to create a new window with just that tab. Existing tabs can be cloned into new ones with the same profile. Visibility of the menu and tab bars can be controlled when starting Konsole. For those handy with scripting, tab titles can be changed through an escape sequence.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole1.png">
	<img src="/announcements/4/4.9.0/kde49-konsole1-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole2.png">
	<img src="/announcements/4/4.9.0/kde49-konsole2-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>KWin Window Manager</h2>
<p>
KDE's Window Manager KWin has seen a lot of work. Improvements include subtle changes like raising windows during window switching and help for Window Specific Settings, as well as more visible changes such as an improved KCM for box switching and better performance with Wobbly Windows. There are changes to make KWin deal better with Activities, including the addition of Activity-related window rules. There has been a general focus on improving KWin quality and performance.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-window-behaviour_settings.png">
	<img src="/announcements/4/4.9.0/kde49-window-behaviour_settings_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Activities</h2>
<p>
Activities are now integrated more thoroughly throughout the Workspaces. Files can be linked to Activities in Dolphin, Konqueror and Folder View. Folder View can also show only those files related to an Activity on the desktop or in a panel. An Activity KIO is new, and encryption for private activities is now possible.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-link-files-to-activities.png">
	<img src="/announcements/4/4.9.0/kde49-link-files-to-activities-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<p>
Workspaces introduce MPRIS2 support, with KMix having the ability to handle streams and a Plasma data engine for handling this music player control protocol. These changes tie in with MPRIS2 support in Juk and Dragon, KDE's music and video player.
</p>
<p>
There are many smaller changes in Workspaces, including several QML ports. The improved Plasma miniplayer includes a track properties dialog and better filtering. The Kickoff menu can now be used with only a keyboard. The Network Management plasmoid has seen layout and usability work. The Public Transport widget has also seen considerable changes.
</p>

<h4>Installing Plasma</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href="http://plasma-active.org">Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.9.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.9.0">4.9 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.9.0 may be <a href="/info/4.9.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.9.0
  are available from the <a href="/info/4.9.0#binary">4.9.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we recommend to use a recent version of Qt, either 4.7.4 or 4.8.0. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Announced Today:</h2>

<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.9"/>New and Improved KDE Applications 4.9</a></h2>
<p>
New and improved KDE Applications released today include Okular, Kopete, KDE PIM, educational applications and games. Read the complete <a href="../applications">'KDE Applications Announcement'</a>
</p>
<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.9"/> KDE Platform 4.9</a></h2>
<p>
Today’s KDE Platform release includes bugfixes, other quality improvements, networking, and preparation for Frameworks 5
</p>
