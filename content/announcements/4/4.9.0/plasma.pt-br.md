---
title: Espaço de Trabalho do Plasma – Principais Melhorias
date: "2012-12-05"
hidden: true
---

<p>
O KDE está feliz em anunciar a disponibilidade imediata da versão 4.9 do Espaço de Trabalho do Plasma para Desktops e Netbooks. Ocorreram muitas melhorias `as funcionalidades j´a existentes do Plasma e um número significante de novas funcionalidades também foi introduzido.
</p>
<h2>Gerenciador de Arquivos Dolphin</h2>
<p>
O poderoso gerenciador de arquivos Dolphin do KDE agora inclui botões para voltar e avançar e a função para renomear diretamente o arquivo está de volta. O Dolphin pode agora mostrar metadados como classificação, marcas, tamanho da imagem e arquivo, autor, data e mais, bem como agrupar e ordenar por propriedades dos metadados. O novo plugin Mercurial manipula o sistema de versões da maneira conveniente com suporte para o git, o SVN e o CVS, assim os usuários pode baixar, enviar e submeter alterações diretamente do gerenciador de arquivos. A Interface do Usuário do Dolphin teve diversas pequenas melhorias, incluindo um painel de Locais aprimorado, suporte à busca melhorado e sincronização com a localização do terminal.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-dolphin_.png">
	<img src="/announcements/4/4.9.0/kde49-dolphin_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Emulador de Terminal X do Konsole</h2>
<p>
O Konsole possui agora a habilidade de buscar por uma seleção de texto suado os Atalhos Web do KDE. Ele oferece a opção de contexto 'Mudar diretório para' quando uma pasta e solta na janela do Konsole. Os usuários tem um controle maior para organizar as janelas do terminal <strong>desacoplando abas</strong> e arrastando-as para criar uma nova janela com apenas aquela aba. As abas existentes pode ser clonadas em novas com o mesmo perfil. A visibilidade do menu de da barra de abas pode ser controlada ao iniciar o Kontole. Para aqueles acostumados a scripting, os títulos das abas podem ser mudados com uma sequência de escape.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole1.png">
	<img src="/announcements/4/4.9.0/kde49-konsole1-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole2.png">
	<img src="/announcements/4/4.9.0/kde49-konsole2-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Gerenciador de Janelas KWin</h2>
<p>
O Gerenciador de Janelas KWin do KDE presenciou muito trabalho. As melhorias incluem mudanças sutis como elevar janelas durante a alternância entre eles a ajuda para as Configurações específicas da janela, bem como mudanças mais visíveis como um KCM melhorado para a alternância em caixa e melhor desempenho para as janelas Wobbly. Existem mudanças que fazem com que o KWin lide melhor com as Atividades, incluindo a adição de regras de janelas relacionadas às Atividades. No período ocorreu um foco geral na melhoria da qualidade e desempenho do KWin.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-window-behaviour_settings.png">
	<img src="/announcements/4/4.9.0/kde49-window-behaviour_settings_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Atividades</h2>
<p>
As Atividades estão agora mais integradas por todo o Espaço de Trabalho. Arquivos podem ser ligados às Atividades no Dolphin, no Konqueror e na Visão de Pasta. A Visão de Pasta pode também mostrar somente os arquivos relacionados com uma Atividade na área de trabalho ou em um painel. Um KIO de Atividade é novo, e a criptografia para atividades privadas é agora possível.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-link-files-to-activities.png">
	<img src="/announcements/4/4.9.0/kde49-link-files-to-activities-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<p>
O Espaço de Trabalho introduziu o suporte ao MPRIS2, com o KMix tendo a capacidade de manipular streams e um motor de dados do Plasma para manipular este protocolo de controle de reprodutor de música. Estas mudanças amarram o suporte ao MPRIS2 no Juk e no Dragon, o reprodutor de vídeo e música do KDE.
</p>
<p>
Existem muitas outras pequenas mudanças no Espaço de Trabalho, incluindo diversos portes para o QML. O mini-reprodutor do Plasma inclui um diálogo de propriedades da faixa e melhor filtragem. O menu do Kickoff pode agora ser usado somente com o teclado. O plasmóide de Gerenciamento de Rede teve seu layout e usabilidade alterados. O widget de Transporte Público teve também mudanças consideráveis.
</p>

<h4>Instalando o Plasma</h4>

<p align="justify">
O software do KDE, incluindo todas as bibliotecas e seus aplicativos, está disponível livremente sob licenças de Código Aberto. O software KDE é executado em várias configurações de hardware e arquiteturas de CPU como a ARM e a x86, sistemas operacionais e funciona com qualquer tipo de gerenciador de janelas e ambiente de desktop. Além do Linux e outros sistemas operacionais baseados em UNIX, você pode encontrar versões para o Microsoft Windows da maioria dos aplicativos do KDE no site <a href="http://windows.kde.org">Software KDE no Windows</a> e para versões do Apple Mac OS X no site <a href="http://mac.kde.org/">Software KDE no Mac</a>. Compilações experimentais dos aplicativos do KDE para diversas plataformas móveis como o MeeGo, MS Windows Mobile e Symbian podem ser encontradas na Web mas elas não são atualmente suportadas. O <a href="http://plasma-active.org">Plasma Active</a> é uma experiência de usuário para um grande espectro de dispositivos, como tablet e outros dispositivos móveis.
<br />
O software KDE pode ser obtido em código ou vários formatos binários a partir de <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a> e pode
também ser obtido em <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
ou com qualquer dos <a href="http://www.kde.org/download/distributions">principais 
sistemas GNU/Linux e UNIX</a> distribuídos hoje.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Alguns distribuidores do Linux/UNIX gentilmente fornecem pacotes binários do 4.9.0 
para algumas versões de suas distribuições, e em outros casos voluntários da comunidade têm
feito isto. <br />
  Alguns destes pacotes binários estão disponíveis para download livre no servidor KDE <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">http://download.kde.org</a>.
  Pacotes binários adicionais, bem como atualizações dos pacotes disponíveis, se tornarão disponíveis
nas próximas semanas.
<a id="package_locations"><em>Localizações dos pacotes</em></a>.
Para uma lista atual dos pacotes binários disponíveis os quais a Equipe de Lançamento do KDE foi informada,
por favor visite a <a href="/info/4.9.0">4.9 Página de
Informações</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  O código-fonte completo para o 4.9.0 pode ser <a href="/info/4.9.0">baixado livremente</a>.
Instruções para compilação e instalação do software KDE 4.9.0
  estão disponíveis na <a href="/info/4.9.0#binary">4.9.0 Página de Informações</a>.
</p>

<h4>
Requisitos do Sistema
</h4>
<p align="justify">
Para obter o melhor destes lançamentos, nós recomendamos usar uma versão recente do Qt, seja a 4.7.4 ou a 4.8.0. Isto é necessário para garantir uma experiência estável e com boa performance, bem como algumas melhorias feitas no software do KDE que foram construídas com base no framework do Qt.<br />
Para obter um uso completo das capacidades do software KDE, nós também recomendamos usar os últimos drivers gráficos para o seu sistema, uma vez que isso aprimora a experiência do usuário de maneira substancial, tanto nas funcionalidades opcionais como na performance e estabilidade geral.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Também anunciado hoje:</h2>

<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Novidades e Melhorias nos Aplicativos do KDE</a></h2>

<p>
As novidades e melhorias nos Aplicativos do KDE lançados hoje incluem o Okular, o Kopete, o KDE PIM, os aplicativos educacionais e os jogos. Leia os detalhes no <a href="../applications">'Anúncio dos Aplicativos do KDE'</a>
</p>

<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>Plataforma KDE</a></h2>
<p>
O lançamento de hoje da Plataforma KDE inclui correções de erros, outras melhorias de qualidade, conectividade e preparação para o Framework 5
</p>
