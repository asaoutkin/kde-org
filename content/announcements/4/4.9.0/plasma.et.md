---
title: Plasma töötsoonid 4.9 – põhikomponentide täiustused
date: "2012-12-05"
hidden: true
---

<p>
KDE teatab rõõmuga nii Plasma töölaua kui ka Plasma Netbooki töötsooni versiooni 4.9 väljalaskmisest. Plasma töötsoonide senist funktsionaalsust on tublisti parandatud ning lisandunud on ka mitmeid märkimisväärseid võimalusi.
</p>
<h2>Failihaldur Dolphin</h2>
<p>
KDE võimsal failihalduril Dolphin on nüüd tagasi- ja edasiliikumise nupud ning taas saab ka failide nime otse muuta. Dolphin võib näidata metaandmeid, näiteks hinnangut, silte, pildi- ja failisuurust, autorit, kuupäeva ja veel mitmeid asju. Samuti saab faile rühmitada ja sortida vastavalt metaandmetele. Uus Mercuriali plugin võimaldab seda versioonihaldussüsteemi sama mugavalt kasutada nagu seni giti, SVN-i ja CVS-i, nii et kasutajad saavad oma faile alla või üles laadida otse failihaldurist. Dolphini kasutajaliides on saanud kerget lihvi, muu hulgas on täiustatud asukohtade paneeli, otsimist ja sünkroonimist terminaliga.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-dolphin_.png">
	<img src="/announcements/4/4.9.0/kde49-dolphin_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>X'i terminali emulaator Konsole</h2>
<p>
Igaks ülesandeks mihkel Konsole võimaldab nüüd otsida valitud teksti KDE veebikiirkorralduste abil. Kui lohistada Konsole peale kataloog, pakutakse võimalust terminalis kohe sellesse kataloogi liikuda. Kasutajad saavad paremini hallata terminaliaknaid <strong>kaarte lahti haakides</strong> ja neid mujale lohistades, millega luuakse uus aken ainult lohistatud kaardiga. Olemasolevaid kaarte saab kloonida sama profiili raames. Menüü- ja kaardiriba näitamist või peitmist saab määrata juba Konsolet käivitades. Kes teavad, mida peale hakata skriptidega, võivad kaartide nimetusi paojadaga muuta.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole1.png">
	<img src="/announcements/4/4.9.0/kde49-konsole1-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole2.png">
	<img src="/announcements/4/4.9.0/kde49-konsole2-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Aknahaldur KWin</h2>
<p>
KDE aknahalduriga KWin on palju vaeva nähtud. Täiustuste seas võib ära mainida selliseid pisikesi muudatusi, nagu akende esiletõstmine nende vahel lülitamise ajal või spetsiaalsete aknareeglite abitekstid, kuid mõned muudatused torkavad ka paremini silma, näiteks akende lülitamise täiustatud juhtimismoodul või võbelevate akende etem jõudlus. Mõned muudatused on aidanud parandada KWini ja tegevuste suhtlust, sealhulgas on lisatud tegevustega seotud aknareeglid. Ka üleüldiselt on üritatud eelkõige parandada KWini kvaliteeti ja jõudlust.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-window-behaviour_settings.png">
	<img src="/announcements/4/4.9.0/kde49-window-behaviour_settings_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Tegevused</h2>
<p>
Tegevused on nüüd põhjalikumalt töötsoonidesse lõimitud. Faile võib seostada tegevustega nii Dolphinis, Konqueroris kui ka kataloogivaates. Kataloogivaade võib näidata töölaual või paneelil ainult konkreetse tegevusega seotud faile. Uus asi on tegevuste KIO-moodul, samuti saab nüüd krüptida tegevusi, mida kasutaja ei taha teistele nähtavaks teha.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-link-files-to-activities.png">
	<img src="/announcements/4/4.9.0/kde49-link-files-to-activities-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<p>
Muude töötsoonide uuenduste hulka kuuluavd MPRIS2 toetus, KMixi oskus hakkama saada voogudega ning Plasma andmemootori toimetulek selle muusikamängija juhtprotokolliga. Need muudatused on seotud MPRIS2 toetuse lisandumisega KDE muusika- ja videomängijatele Juk ja Dragon.
</p>
<p>
Töötsoonides on samuti hulk väiksemaid muudatusi, sealhulgas mitu portimist QML peale. Täiustatud Plasma minipleier pakub radade omaduste dialoogi ja paremat filtreerimist. Kickoffi menüüd saab nüüd kasutada ka ainult klaviatuuri abil. Võrguhalduse plasmoidil on parandatud väljanägemist ja hõlbustatud kasutamist. Samamoodi on märkimisväärseid muudatusi üle elanud ühiskondliku transpordi vidin.
</p>

<h4>Plasma paigaldamine</h4>

<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral ja protsessoritel (näiteks ARM ja x86), operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta. <a href="http://plasma-active.org">Plasma Active</a> kujutab endast kasutajakogemust paljudele seadmetele, näiteks tahvelarvutid ja muud mobiilsed seadmed.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.9.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">http://download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4.9.0">4.9 infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.9.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.9.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.9.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4.9.0#binary">4.9.0 infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.4 või 4.8.0. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Täna ilmusid veel:</h2>

<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Uued ja täiustatud KDE rakendused 4.9</a></h2>
<p>
Täna ilmunud uute ja täiustatud KDE rakenduste seast väärivad märkimist Okular, Kopete, KDE PIM, õpirakendused ja mängud. Neist kõneleb lähemalt <a href="../applications">'KDE rakenduste teadaanne'</a>
</p>

<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>KDE platvorm 4.9</a></h2>

<p>
Tänane KDE platvormi väljalase sisaldab veaparandusi, muulaadseid kvaliteediparandusi, võrgutäiustusi ja valmistumist üleminekuks raamistikule 5
</p>
