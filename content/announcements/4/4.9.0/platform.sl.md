---
title: KDE-jeva razvojna platforma 4.9 – boljša povezljivost, preprostejše razvijanje
date: "2012-12-05"
hidden: true
---

<p>
KDE s ponosom najavlja izid razvojne platforme KDE Platform 4.9. Ta je osnova za delovna okolja Plasma in KDE-jeve programe. Skupnost KDE trenutno razvija naslednjo večjo izdajo KDE-jeve razvojne platforme, ki bo imenovana »KDE Frameworks 5«. Le-ta bo sicer v veliki meri združljiva na ravni izvorne kode in bo temeljila na ogrodju Qt 5. Ponujala bo jasno razdelitev na posamezne dele, kar bo olajšalo uporabo le določenih delov platforme in zmanjšalo medsebojne odvisnosti. To pomeni, da je bila v preteklih šestih mesecih KDE-jeva razvojna platforma 4 v večji meri zamrznjena. Večina dela je bila posvečenega odpravljanju napak in pohitritvam.
</p>
<p>
Napredovalo je ločevanje QGraphicsView od Plasme, s čimer se pripravlja prehod na Plasmo, ki bo povsem temeljila na tehnologiji Qt QML, ter izboljšave v podpori za Qt Quick v KDE-jevih knjižnicah in delovnih okoljih Plasma.
</p>
<p>
Nekaj dela v KDE-jevih knjižnicah je bilo opravljenega na področju omrežnega povezovanja. Dostopanje do virov, deljenih prek omrežja, je hitrejše za vse KDE-jeve programe. NFS, Samba in SSHFS povezave ter KIO nič več ne štejejo datotek in map, kar prinaša pohitritve. Protokol HTTP je ravno tako hitrejši, saj so bile odpravljene nepotrebne povratne izmenjave. Pohitritev je še najbolj opazna v programih, ki uporabljajo dostop do omrežja. KOrganizer je na primer hitrejši za približno 20 %. Izboljšano je tudi shranjevanje gesel za omrežne vire.
</p>
<p>
Odpravljene so bile večje napake v Nepomuku in Sopranu, zaradi česar je namizno iskanje hitrejše in zanesljivejše. Zmogljivostne izboljšave in povečanje stabilnosti sta bila glavna cilja teh projektov v tej izdaji.
</p>

<h4>Nameščanje KDE-jeve razvojne platforme</h4>

<p align="justify">
KDE-jeva programska oprema, ki vključuje vse knjižnice in programe, je prosto na voljo pod licencami za svobodno in odprtokodno prgramsko opremo. KDE-jeva programska oprema teče na raznih strojnih konfiguracijah in procesorskih arhitekturah kot so MIPS, ARM in x86. Deluje tudi na več operacijskih sistemih in s poljubnim upravljalnikom oken ali v poljubnem namiznem okolju. Poleg inačic za GNU/Linux in ostale operacijske sisteme temelječe na UNIX-u lahko večino programov dobite tudi za Windows (oglejte si stran projekta <a href="http://windows.kde.org/">KDE-jevih programov za Windows</a>) in Mac OS X (<a href="http://mac.kde.org/">KDE-jevi programi za Mac</a>). Na spletu je moč najti tudi razne preizkusne inačice KDE-jevih programov za mobilne platforme kot sta na primer MeeGo in Symbian. Te niso podprte. <a href="http://plasma-active.org/">Plasma Active</a> je uporabniška izkušnja za širši spekter naprav kot so tablični računalniki in druga mobilna strojna oprema.
<br />
KDE-jevo programsko opremo je v obliki izvorne kode in raznih binarnih formatih moč dobiti s strani <a
href="http://download.kde.org/stable/4.9.0/">download.kde.org</a>, na <a href="http://www.kde.org/download/cdrom">zgoščenki</a> ali kot del <a href="http://www.kde.org/download/distributions">večjih distribucij sistema GNU/Linux ali sistemov UNIX</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketi</em></a>.
  Nekateri ponudniki operacijskega sistema GNU/Linux in sistemov UNIX so dali na razpolago binarne pakete različice 4.9.0 za nekatere različice svojih distribucij. V drugih primeri so to storili prostovoljci. <br />
  Nekateri binarni paketi so za prost prenos na voljo s KDE-jeve strani <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">download.kde.org</a>.
  Dodatni binarni paketi in posodobitve obstoječih bodo na voljo v prihodnjih tednih.
<a id="package_locations"><em>Lokacije paketov</em></a>.
Za trenuten seznam razpoložljivih binarnih paketov, o katerih je bila skupnost KDE obveščena, obiščite <a href="/info/4.9.0">4.9to spletno stran</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Celotno izvorno kodo za različico 4.9.0 lahko <a href="/info/4.9.0">prosto prenesete</a>.
Navodila za prevajanje izvorne kode in nameščanje KDE-jeve programske opreme različice 4.9.0 so na voljo na
<a href="/info/4.9.0#binary">4.9.0tej spletni strani</a>.
</p>

<h4>
Sistemske zahteve
</h4>
<p align="justify">
Da bi ta različica delovala kar najbolje, priporočamo uporabo najnovejše različice knjižnice Qt. To je potrebno za stabilnost in hitrost, saj so bile nekatere izboljšave opravljene na nivoju ogrodja Qt.<br />
Za čimboljše doživetje pri uporabi vseh zmožnosti KDE-jevih programov priporočamo tudi uporabo najnovejših gonilnikov za grafično kartico. Najnovejši gonilniki lahko izboljšajo funkcionalnost, hitrost in stabilnost.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Druge današnje najave:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Delovna okolja KDE Plasma 4.9 – osnovne izboljšave</a></h2>

<p>
Med poudarke glede delovnih okolij Plasma spadajo precejšnje izboljšave upravljalnika datotek Dolphin, posnemovalnika terminala Konsole, dejavnosti in upravljalnika oken KWin. Za podrobnosti si oglejte <a href="../plasma">najavo za delovna okolja Plasma</a>.
</p>

<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Novi in izboljšani KDE-jevi programi 4.9</a></h2>

<p>
Med nove ali izboljšane KDE-jeve programe  spadajo, Okular, Kopete, Kontact, izobraževalni programi in igre. Za podrobnosti si oglejte <a href="../applications">najavo za KDE-jeve programe</a>.
</p>
