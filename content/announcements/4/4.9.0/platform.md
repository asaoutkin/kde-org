---
title: KDE Platform 4.9 – Better Interoperability, Easier Development
date: "2012-12-05"
hidden: true
---

<p>
KDE proudly announces the release of KDE Platform 4.9. KDE Platform is the foundation for Plasma Workspaces and KDE Applications. The KDE team is currently working on the next iteration of the KDE Development Platform referred to as "Frameworks 5". While Frameworks 5 will be mostly source compatible, it will be based on Qt5. It will provide more granularity, making it easier to use parts of the platform selectively and reducing dependencies considerably. As a result, the KDE Platform is mostly frozen. Most Platform efforts went into bugfixing and performance improvements.
</p>
<p>
There has been progress in separating QGraphicsView from Plasma to make way for a pure QML-based Plasma and further improvements to Qt Quick support in KDE libraries and Plasma Workspaces.
</p>
<p>
Some low-level work has been done in the KDE libraries in the area of networking. Accessing network shares has been made much faster for all KDE applications. NFS, Samba and SSHFS connections and KIO won't count items in folders anymore, making them faster. The HTTP protocol was also speeded up by preventing unnecessary server round trips. This is especially noticeable in networked applications like Korganizer, which is about 20% faster due to the fixes. There is also improved password storing for network shares.
</p>
<p>
Many critical bugs have been fixed in Nepomuk and Soprano, making desktop search faster and more reliable for Applications and Workspaces build on KDE Platform 4.9. Performance improvement and stability were the primary goals of these projects for this release.
</p>

<h4>Installing the KDE Development Platform</h4>

<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href="http://plasma-active.org">Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.9.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.9.0">4.9 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.9.0 may be <a href="/info/4.9.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.9.0
  are available from the <a href="/info/4.9.0#binary">4.9.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we recommend to use a recent version of Qt, either 4.7.4 or 4.8.0. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Announced Today:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.9" /> Plasma Workspaces 4.9 – Core Improvements</a></h2>
<p>
Highlights for Plasma Workspaces include substantial improvements to the Dolphin File Manager, Konsole X Terminal Emulator, Activities, and the KWin Window Manager. Read the complete <a href="../plasma">'Plasma Workspaces Announcement'.</a>
</p>
<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.9"/>New and Improved KDE Applications 4.9</a></h2>
<p>
New and improved KDE Applications released today include Okular, Kopete, KDE PIM, educational applications and games. Read the complete <a href="../applications">'KDE Applications Announcement'</a>
</p>
