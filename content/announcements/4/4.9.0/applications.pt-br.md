---
title: Novidades e Melhorias nos Aplicativos do KDE
date: "2012-12-05"
hidden: true
---

<p>
O KDE está feliz em anunciar o lançamento de versões melhoradas de diversos aplicativos populares. Isto inclui muitas ferramentas básicas importantes e aplicativos como o Okular, o Kopete, o KDE PIM, aplicativos educacionais e jogos.
</p>
<p>
O visualizador de documentos Okular do KDE pode agora armazenar e imprimir anotações com documentos PDF. A busca, marcação e seleção de texto foi melhorada. O Okular pode ser configurado de modo que o laptop não entre em modo de economia de energia ou desligue durante uma apresentação, e ele pode agora reproduzir filmes incorporados em arquivos PDF. O visualizador de imagens Gwenview fornece uma nova opção de navegação em tela cheia, além de diversas correções de erro e pequenas melhorias.
</p>
<p>
O Juk, o reprodutor de músicas padrão do KDE, traz o suporte ao last.fm com scrobbling e obtenção de capas bem como suporte ao MPRIS2. Ele irá ler a arte de capa embutida em arquivos MP4 e AAC. O Dragon, o reprodutor de vídeo do KDE, também funciona com o MPRIS2 agora.
</p>
<p>
O versátil cliente de bate-papo Kopete pode agrupar todos os usuários off-line em um único grupo "Usuários Offline" e mostrar a mudança de status do contato na janela de bate-papo. Existe uma nova opção para 'renomear' o contato que permite personalizar a exibição do nome na própria janela.
</p>
<p>
O aplicativo de tradução Lokalize melhorou a pesquisa de mensagens aproximadas e possui uma aba de pesquisa melhorada. Ele pode agora manipular arquivos .TS. O Umbrello posuui suporte à layout automático para diagramas e pode exportar desenhos para o graphviz. O Okteta introduziu perfis de visualização, incluindo um gerenciador/editor.
</p>
<h2>Suíte do Kontact</h2>
<p>
A mais completa suíte PIM do mundo, o Kontact, recebeu diversas correções de erro e melhorias de desempenho. Este lançamento introduz um assistente de importação para obter configurações, correio, filtros, calendários e entradas do livro de endereçoes do Thunderbird e Evolution para o KDE PIM. Existe uma ferramenta que pode criar cópias de segurança de mensagens de correio, configurações e meta-dados e restaurá-las. O KTnef, o visualizador independente de anexos TNEF, foi trazido de volta à vida dos arquivos do KDE 3. Recursos do Google podem ser integrados ao KDE PIM, fornecendo aos usuários acesso aos seus contatos e calendários do Google.
</p>
<h2>Educação no KDE</h2>

<p>
O KDE-Edu apresenta o Pairs, um novo jogo de memória. O Rocs, o aplicativo de teoria dos grafos para estudantes e professores, recebeu várias melhorias. Algoritmos pode agora ser executados passo-a-passo, o sistema de desfazer e cancelar construção funciona melhor, e camadas sobrepostas de grafos são agora suportadas. O Kstars possui uma ordenação melhorada da hora de trânsito do meridiano / hora de observação e melhor <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">obtenção de imagem</a> do Reconhecimento do Céu Digital.
<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-pairs.png">
	<img src="/announcements/4/4.9.0/kde49-pairs-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>
</p>
<p>
O Marble recebeu otimizações e suporte a threads, e sua interface foi aprimorada. As extensões de rotação do Marble agora incluem o OSRM (Open Source Routing Machine), suporte para rotas de ciclistas e pedestres, e um modelo de dados offline para gerenciar roteamento e busca de dados offline. O Marble pode agora mostrar posições de um avião voando no simulador FlightGear.
</p>
<h2>Jogos do KDE</h2>
<p>
Os Jogos do KDE foram atualizados. Muito polimento foi aplicado ao Kajongg, o jogo de Mahjongg do KDE, incluindo dicas de jogo, IA melhorada e bate-papo se os jogadores estiverem no mesmo servidor (kajongg.org agora possui um!). O KGoldrunner tem alguns novos níveis (uma contribuição de Gabriel Miltschitzky) e o KPatience retém o histórico do jogo ao salvar. O KSudoku teve pequenas melhorias como dicas melhores, bem como vários novos quebra-cabeças bi-dimensionais e três novas formas em 3-D.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai.png">
	<img src="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>
</p>

<h4>Instalando aplicativos do KDE</h4>

<p align="justify">
O software do KDE, incluindo todas as bibliotecas e seus aplicativos, está disponível livremente sob licenças de Código Aberto. O software KDE é executado em várias configurações de hardware e arquiteturas de CPU como a ARM e a x86, sistemas operacionais e funciona com qualquer tipo de gerenciador de janelas e ambiente de desktop. Além do Linux e outros sistemas operacionais baseados em UNIX, você pode encontrar versões para o Microsoft Windows da maioria dos aplicativos do KDE no site <a href="http://windows.kde.org">Software KDE no Windows</a> e para versões do Apple Mac OS X no site <a href="http://mac.kde.org/">Software KDE no Mac</a>. Compilações experimentais dos aplicativos do KDE para diversas plataformas móveis como o MeeGo, MS Windows Mobile e Symbian podem ser encontradas na Web mas elas não são atualmente suportadas. O <a href="http://plasma-active.org">Plasma Active</a> é uma experiência de usuário para um grande espectro de dispositivos, como tablet e outros dispositivos móveis.
<br />
O software KDE pode ser obtido em código ou vários formatos binários a partir de <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a> e pode
também ser obtido em <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
ou com qualquer dos <a href="http://www.kde.org/download/distributions">principais 
sistemas GNU/Linux e UNIX</a> distribuídos hoje.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Alguns distribuidores do Linux/UNIX gentilmente fornecem pacotes binários do 4.9.0 
para algumas versões de suas distribuições, e em outros casos voluntários da comunidade têm
feito isto. <br />
  Alguns destes pacotes binários estão disponíveis para download livre no servidor KDE <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">http://download.kde.org</a>.
  Pacotes binários adicionais, bem como atualizações dos pacotes disponíveis, se tornarão disponíveis
nas próximas semanas.
<a id="package_locations"><em>Localizações dos pacotes</em></a>.
Para uma lista atual dos pacotes binários disponíveis os quais a Equipe de Lançamento do KDE foi informada,
por favor visite a <a href="/info/4.9.0">4.9 Página de
Informações</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  O código-fonte completo para o 4.9.0 pode ser <a href="/info/4.9.0">baixado livremente</a>.
Instruções para compilação e instalação do software KDE 4.9.0
  estão disponíveis na <a href="/info/4.9.0#binary">4.9.0 Página de Informações</a>.
</p>

<h4>
Requisitos do Sistema
</h4>
<p align="justify">
Para obter o melhor destes lançamentos, nós recomendamos usar uma versão recente do Qt, seja a 4.7.4 ou a 4.8.0. Isto é necessário para garantir uma experiência estável e com boa performance, bem como algumas melhorias feitas no software do KDE que foram construídas com base no framework do Qt.<br />
Para obter um uso completo das capacidades do software KDE, nós também recomendamos usar os últimos drivers gráficos para o seu sistema, uma vez que isso aprimora a experiência do usuário de maneira substancial, tanto nas funcionalidades opcionais como na performance e estabilidade geral.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Também anunciado hoje:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Espaço de Trabalho do Plasma – Principais Melhorias</a></h2>

<p>
Os destaques para o Espaço de Trabalho do Plasma incluem melhorias substanciais no Gerenciador de Arquivos Dolphin, no Emulador de Terminal X do Konsole, nas Atividades e no Gerenciador de Janelas KWin. Leia os detalhes no <a href="../plasma">'Anúncio do Espaço de Trabalho do Plasma'.</a>
</p>

<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>Plataforma KDE</a></h2>
<p>
O lançamento de hoje da Plataforma KDE inclui correções de erros, outras melhorias de qualidade, conectividade e preparação para o Framework 5
</p>
