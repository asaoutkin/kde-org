---
title: KDE-jevi programi 4.9 so boljši in stabilnejši
date: "2012-12-05"
hidden: true
---

<p>
KDE z veseljem najavlja izid izboljšanih različic priljubljenih programov. Med te spadajo mnoga osnovna orodja in programi, kot so Okular, Kopete, Kontact ter izobraževalni programi in igre.
</p>
<p>
KDE-jev pregledovalnik dokumentov Okular lahko sedaj shrani in natisne zabeležke za dokumente PDF. Iskanje, zaznamki in izbiranje besedila so doživeli izboljšave. V načinu za predstavitve bo sedaj Okular preprečil ugasnitev zaslona ali prehod prenosnika v mirovanje. Okular po novem zna predvajati tudi video posnetke, ki so vgrajeni v dokumente PDF. Pregledovalnik slik Gwenview prinaša nov celozaslonski način za brskanje in precej popravkov in manjših izboljšav.
</p>
<p>
JuK, KDE-jev privzeti glasbeni predvajalnik prinaša podporo za objavljanje na last.fm, pridobivanje ovitkov z last.fm in podporo za protokol MPRIS2. Prikazati zna tudi ovitke, ki so vgrajeni v datotakah MP4 in AAC. KDE-jev predvajalnik videa Dragon je prav tako dobil podporo za MPRIS2
</p>
<p>
Vsestranski program za klepet Kopete zna po novem združiti vse stike brez povezave v svojo skupino in prikazati spremembe besedila stanja v oknih za klepet.
</p>
<p>
Program za računalniško podprto prevajanje Lokalize ima izboljšano iskanje ohlapnih prevodov in datotek. Z njim je sedaj možno prevajati tudi datoteke TS ogordja Qt. Umbrelo, modelirnik za UML, je dobil podporo za samodejno razporejanje diagramov in izvažanje risb v zapisu Graphviz dot. Urejevalnik binarnih datotek Okteta prinaša profile prikaza in upravljalnik zanje.
</p>
<h2>Zbrirka Kontact</h2>
<p>
Najcelovitejša zbirka programov za upravljanje osebnih podatkov Kontact je prejela veliko popravkov in pohitritev. Dodan je bil čarovnik za uvažanje nastavitev, e-pošte, filtrov, koledarjev in imenika iz programov Thunderbird ter Evolution. Novo je tudi orodje za ustvarjanje varnostnoh kopij e-pošte, nastavitev in metapodatkov. Vrnil se je program KTnef, ki je samostojen pregledovalnik prilog TNEF. Viri za Googlove storitve omogočajo uporabnikom dostop do stikov in koledarjev.
</p>
<h2>KDE-jevi izobraževalni programi</h2>

<p>
V zbirko izobraževalnih programov KDE Education je bila dodana igra za treniranje spomina Pairs. Rocs, program za učenje in raziskovanje teorije grafov, ima več izboljšav. Algoritme je sedaj moč izvajati po korakih, sistem za razveljavljanje deluje bolje, podprti pa so tudi prekrivni grafi. Namizni planetarij KStars ima izboljšano razvrščanje po času prehoda poldnevnika in času opazovanja ter boljšo podporo za pridobivanje slik projekta <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">Digitalized Sky Survey</a>.
<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-pairs.png">
	<img src="/announcements/4/4.9.0/kde49-pairs-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<p>
Nova različica namiznega globusa Marble vsebuje pohitritve in podporo za večjedrne procesorje.Izboljšave je doživel tudi uporabniški vmesnik. Med razširitvami za usmerjanje do cilja je tudi OSRM (Open Source Routing Machine), podpora za usmerjanje pešcev in kolesarjev ter zmožnost za usmerjanje in iskanje podatkov brez medmrežne povezave. Marble lahko prikazuje tudi položaje letal v simulatorju FlightGear.
</p>
<h2>KDE-jeve igre</h2>
<p>
Veliko izboljšav je doživel Kajongg, KDE-jeva igra mahjongga. Med izbojšavami so namigi za igro, izboljšana umetna inteligenca računalniških igralcev in klepet z igralci na istem strežniku (en izmed njih je kajongg.org). KGoldrunner ima več novih stopenj (prispeval jih je Gabriel Miltschitzky), KPatience pa shrani tudi zgodovino igre. KSudoku prikazuje boljše namige ter ponuja sedem novih dvorazsežnih ugank in tri nove trorazsežne uganke.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai.png">
	<img src="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>

<h4>Nameščanje KDE-jevih programov</h4>

<p align="justify">
KDE-jeva programska oprema, ki vključuje vse knjižnice in programe, je prosto na voljo pod licencami za svobodno in odprtokodno prgramsko opremo. KDE-jeva programska oprema teče na raznih strojnih konfiguracijah in procesorskih arhitekturah kot so MIPS, ARM in x86. Deluje tudi na več operacijskih sistemih in s poljubnim upravljalnikom oken ali v poljubnem namiznem okolju. Poleg inačic za GNU/Linux in ostale operacijske sisteme temelječe na UNIX-u lahko večino programov dobite tudi za Windows (oglejte si stran projekta <a href="http://windows.kde.org/">KDE-jevih programov za Windows</a>) in Mac OS X (<a href="http://mac.kde.org/">KDE-jevi programi za Mac</a>). Na spletu je moč najti tudi razne preizkusne inačice KDE-jevih programov za mobilne platforme kot sta na primer MeeGo in Symbian. Te niso podprte. <a href="http://plasma-active.org/">Plasma Active</a> je uporabniška izkušnja za širši spekter naprav kot so tablični računalniki in druga mobilna strojna oprema.
<br />
KDE-jevo programsko opremo je v obliki izvorne kode in raznih binarnih formatih moč dobiti s strani <a
href="http://download.kde.org/stable/4.9.0/">download.kde.org</a>, na <a href="http://www.kde.org/download/cdrom">zgoščenki</a> ali kot del <a href="http://www.kde.org/download/distributions">večjih distribucij sistema GNU/Linux ali sistemov UNIX</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketi</em></a>.
  Nekateri ponudniki operacijskega sistema GNU/Linux in sistemov UNIX so dali na razpolago binarne pakete različice 4.9.0 za nekatere različice svojih distribucij. V drugih primeri so to storili prostovoljci. <br />
  Nekateri binarni paketi so za prost prenos na voljo s KDE-jeve strani <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">download.kde.org</a>.
  Dodatni binarni paketi in posodobitve obstoječih bodo na voljo v prihodnjih tednih.
<a id="package_locations"><em>Lokacije paketov</em></a>.
Za trenuten seznam razpoložljivih binarnih paketov, o katerih je bila skupnost KDE obveščena, obiščite <a href="/info/4.9.0">4.9to spletno stran</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Celotno izvorno kodo za različico 4.9.0 lahko <a href="/info/4.9.0">prosto prenesete</a>.
Navodila za prevajanje izvorne kode in nameščanje KDE-jeve programske opreme različice 4.9.0 so na voljo na
<a href="/info/4.9.0#binary">4.9.0tej spletni strani</a>.
</p>

<h4>
Sistemske zahteve
</h4>
<p align="justify">
Da bi ta različica delovala kar najbolje, priporočamo uporabo najnovejše različice knjižnice Qt. To je potrebno za stabilnost in hitrost, saj so bile nekatere izboljšave opravljene na nivoju ogrodja Qt.<br />
Za čimboljše doživetje pri uporabi vseh zmožnosti KDE-jevih programov priporočamo tudi uporabo najnovejših gonilnikov za grafično kartico. Najnovejši gonilniki lahko izboljšajo funkcionalnost, hitrost in stabilnost.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Druge današnje najave:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Delovna okolja KDE Plasma 4.9 – osnovne izboljšave</a></h2>

<p>
Med poudarke glede delovnih okolij Plasma spadajo precejšnje izboljšave upravljalnika datotek Dolphin, posnemovalnika terminala Konsole, dejavnosti in upravljalnika oken KWin. Za podrobnosti si oglejte <a href="../plasma">najavo za delovna okolja Plasma</a>.
</p>

<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>KDE-jeva razvojna platforma 4.9</a></h2>
<p>
Današnja izdaja KDE-jeve razvojne platforme vsebuje popravke, druge izboljšave kakovosti in omrežnih zmožnosti ter priprave na ogrodje KDE Frameworks 5. Za podrobnosti si oglejte <a href="../platform">najavo za KDE-jevo razvojno platformo</a>.
</p>
