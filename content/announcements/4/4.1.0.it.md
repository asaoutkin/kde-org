---
aliases:
- ../4.1
date: '2008-07-29'
title: KDE 4.1 Release Announcement
---

<h3 style="text-align: center">
La comunità KDE è lieta di annunciare il rilascio di KDE 4.1.0
</h3>

<p style="text-align: center">
  <strong>
    Con la 4.1.0 KDE rilascia un desktop migliorato e molte applicazioni, il tutto dedicato a Uwe Thiem
  </strong>
</p>

<p style="text-align: justify">
La <a href="http://www.kde.org/">comunità KDE</a> rilascia oggi la versione 4.1.0. Questo è il secondo rilascio ampliativo per la serie 4, ed aggiunge nuove applicazioni e nuove funzionalità sviluppate sugli oramai famosi «Pilastri di KDE». KDE 4.1 è il primo rilascio che si accompagna ufficialmente con l'insieme di programmi di gestione delle informazioni personali KDE-PIM, contenente il programma di posta elettronica KMail, il calendario KOrganizer, l'aggregatore di notizie Akregator e il client per i gruppi di discussione KNode, assieme ad altri componenti minori integrati in Kontact, il contenitore che facilita l'utilizzo di tutti questi programmi. Andando avanti, Plasma, il nuovo ambiente desktop introdotto con KDE 4.0, è maturato al punto da poter sostituire il Desktop di KDE 3 per la maggior parte degli utenti. Inoltre, come per gli altri rilasci, molto lavoro è stato fatto per migliorare le strutture e le librerie sulle quali viene sviluppato KDE stesso.
<br />
Dirk Müller, uno dei responsabili dei rilasci di KDE accenna qualche cifra: <em>«Ci sono state 20803 modifiche al codice di KDE dalla 4.0 alla 4.1 assieme ad altre 15432 riguardanti le traduzioni. All'incirca 35000 di questi cambiamenti sono stati apportati nei rami di lavoro, alcune delle quali sono state apportate direttamente su KDE 4.1 non entrando neppure a far parte delle statistiche»</em>.
Müller inoltre ci confida che gli amministratori dei server di KDE hanno creato 166 nuovi account per sviluppatori sul server SVN di KDE.

<div class="text-center">
	<a href="/announcements/4/4.1.0/desktop.png">
	<img src="/announcements/4/4.1.0/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Il desktop di KDE 4.1</em>
</div>
<br/>

</p>
<p>
<strong>I miglioramenti fondamentali di KDE 4.1 sono:</strong>
<ul>
    <li>Il ritorno di KDE-PIM</li>
    <li>Plasma maturato</li>
    <li>Molte nuove applicazioni e infrastrutture migliorate</li>
</ul>

</p>

<h3>
  <a id="changes">Dedicato a Uwe Thiem</a>
</h3>
<p style="text-align: justify">
La comunità KDE dedica questo rilascio ad Uwe Thiem, un collaboratore di vecchia data, deceduto di recente a causa di alcuni problemi ai reni. La morte di Uwe è stata del tutto inattesa ed è stato uno shock per i suoi compagni e collaboratori. Letteralmente fino a pochi giorni dalla sua scomparsa Uwe ha contribuito a KDE, ma non solo attraverso la programmazione, anche giocando un ruolo fondamentale nell'educazione degli utenti all'utilizzo del software libero in Africa. Con la prematura scomparsa di Uwe KDE ha perso una parte insostituibile della comunità, nonché un amico. Il nostro pensiero va alla sua famiglia e a tutti coloro che hanno avuto la fortuna di conoscerlo.</p>

<h3>
  <a id="changes">Passato, presente e futuro</a>
</h3>
<p style="text-align: justify">
Nonostante KDE 4.1 punti ad essere il primo rilascio adatto ai normali utenti, alcune delle funzionalità che erano presenti in KDE 3.5 non sono ancora state reintrodotte. La squadra di KDE sta lavorando per la loro reintroduzione, e crede di poterle rendere disponibili in uno dei prossimi rilasci. Ma nonostante non ci sia la garanzia che tutte le funzionalità di KDE 3.5 vengano reintrodotte, la 4.1 costituisce comunque un ambiente di lavoro completo, potente e funzionale.<br /> Fate attenzione che alcune delle opzioni dell'interfaccia grafica sono state spostate in un contesto più appropriato, perciò siate sicuri aver cercato bene prima di segnalare che manca qualcosa.<br /> KDE 4.1 è un grosso passo avanti della serie KDE4, e la speranza è che si sia raggiunto uno stato di quiete per quanto riguarda le infrastrutture semplificando la strada per gli sviluppi futuri. KDE 4.2 è atteso per Gennaio 2009.
</p>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kdepim-screenie.png">
	<img src="/announcements/4/4.1.0/kdepim-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Il ritorno di KDE PIM</em>
</div>
<br/>

<h3>
  <a id="changes">Miglioramenti</a>
</h3>
<p style="text-align: justify">
Mentre le infrastrutture vanno stabilizzandosi in KDE 4.1, molta enfasi è stata messa sul lavoro svolto per le funzionalità più visibili dall'utente finale. Si continui a leggere per una lista dei miglioramenti di KDE 4.1. Informazioni ancora più dettagliate possono essere trovate sulla pagina degli <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Obbiettivi di KDE 4.1</a>
e nel
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">piano delle caratteristiche di KDE 4.1</a>.
</p>

<h4>
  Per gli utenti
</h4>

<ul>
    <li>
        <strong>KDE-PIM</strong> fa il suo ritorno con KDE 4.1, e contiene già tutte le applicazioni necessarie per la gestione delle informazioni personali come KMail, il client della posta elettronica, KOrganizer, il calendario, Akregator, il lettore delle fonti RSS e gli altri componenti sono nuovamente disponibili, ma in perfetto stile KDE 4.
    </li>
    <li>
        <strong>Dragon Player</strong>, che fa il suo ingresso come un facile lettore video.
    </li>
    <li>
        <strong>Okteta</strong>, il nuovo e completo programma per la gestione di file esadecimali.
    </li>
    <li>
        <strong>Step</strong>, il simulatore di fisica che ne facilita l'apprendimento in maniera semplice e divertente.
    </li>
    <li>
        <strong>KSystemLog</strong> aiuta a tenere traccia di quello che succede nel sistema.
    </li>
    <li>
        <strong>Nuovi giochi</strong> come KDiamond (un clone di Bejeweled), Kollision, KBreakOut
        e Kubrick faranno diventare irresistibile il volersi prendere una pausa dal lavoro.
    </li>
    <li>
        <strong>Lokalize</strong> aiuterà i traduttori a rendere KDE 4 disponibile nella tua lingua
        (se ancora non è fra le oltre 50 nelle quali KDE 4 è già disponibile).
    </li>
    <li>
        <strong>KSCD</strong>, il lettore di cd audio preferito dagli utenti, è risorto.
    </li>
</ul>

<p style="text-align: justify">
Molte risposte alle domande più comuni sono state raccolte nelle
<a href="http://www.kde.org/users/faq">FAQ per utenti finali di KDE 4</a>, che sono una buona lettura se si vuole saperne di più su KDE 4.
</p>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-screenie.png">
	<img src="/announcements/4/4.1.0/dolphin-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nuovo metodo di selezione per Dolphin</em>
</div>
<br/>

<ul style="text-align: justify">
    <li>
        <strong>Dolphin</strong>, il gestore di file predefinito per KDE 4 ha una nuova vista ad albero e il nuovo supporto per le schede. Una nuova e intuitiva modalità di selezione migliora l'usabilità per l'utente e le voci di menu contestuale «copia in...» e «muovi in...» rendono queste azioni facili e accessibili. Konqueror è sempre disponibile come alternativa a Dolphin, sfrutta infatti anch'esso molte delle funzionalità citate sopra. [<a href="#screenshots-dolphin">Schermata di Dolphin</a>]
    </li>
    <li>
        <strong>Konqueror</strong>, il browser web predefinito di KDE ora supporta la riapertura delle schede chiuse ed ha visto miglioramenti nella velocità di scorrimento delle pagine.
    </li>
    <li>
        <strong>Gwenview</strong>, il visualizzatore di immagini, ha una nuova modalità a schermo intero, una barra delle anteprime per l'accesso rapido alle altre foto, un sistema di annullamento delle azioni più intelligente e il supporto per la valutazione delle immagini. [<a href="#screenshots-gwenview">Schermata di Gwenview</a>]
    </li>
    <li>
        <strong>KRDC</strong>, il client per il desktop remoto di KDE, ora trova i desktop remoti nella rete locale automaticamente grazie all'utilizzo del protocollo ZeroConf.
    </li>
    <li>
        <strong>Marble</strong>, il mappamondo per il desktop KDE, ha ora l'integrazione a <a
        href="http://www.openstreetmap.org/">OpenStreetMap</a>, in modo da poter avere indicazioni stradali con mappe libere. [<a href="#screenshots-marble">Screenshot di Marble</a>]
    </li>
    <li>
        <strong>KSysGuard</strong> supporta il monitoraggio dei messaggi dei processi e l'avvio di applicazioni, rendendo superfluo l'utilizzo di un terminale per queste operazioni.
    </li>
    <li>
        <strong>KWin</strong>, con i suoi effetti grafici, è stato esteso e stabilizzato. Sono stati aggiunti gli effetti Coverswitch, per il passaggio da una applicazione all'altra, e le famose «Finestre tremolanti». [<a href="#screenshots-kwin">Schermata di KWin</a>]
    </li>
    <li>
        <strong>Plasma</strong> è cambiato molto, a partire dalle possibilità di configurazione del pannello che sono state ampliate. Il nuovo strumento di configurazione del pannello rende molto semplice la configurazione dello stesso dando un riscontro visivo immediato. Si possono anche aggiungere pannelli e spostarli attraverso uno o più schermi. Il plasmoide «Vista delle cartelle» dà la possibilità di gestire i file dal proprio desktop (Dando effettivamente la possibilità di vedere una o più directory direttamente dal desktop). Si possono avere nessuno, uno o diversi «Vista delle cartelle» sul desktop, dando accesso facile ai file con i quali si sta lavorando.
        [<a href="#screenshots-plasma">Schermata di Plasma</a>]
    </li>
</ul>

<h4>
  Per gli sviluppatori
</h4>

<ul style="text-align: justify">
    <li>
        L'infrastruttura <strong>Akonadi</strong> fornisce una maniera efficiente per l'archiviazione e la fruizione dei dati PIM, email e contatti attraverso le varie applicazioni. <a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> supporta la ricerca attraverso i dati e notifica le applicazioni dei cambiamenti effettuati.
    </li>
    <li>
        Le applicazioni KDE possono essere scritte in Python e Ruby. <strong>Le interfacce per questi linguaggi</strong> sono <a href="http://techbase.kde.org/Development/Languages">considerate</a> stabili, mature e utilizzabili per lo sviluppo di applicazioni.
    </li>
    <li>
        <strong>Libksane</strong> fornisce una facile via di accesso alle applicazioni per la scansione delle immagini come la nuova Skanlite.
    </li>
    <li>
        Un sistema condiviso di gestione delle <strong>faccine</strong> che possono essere usate da KMail e Kopete.
    </li>
    <li>
        Nuovo supporto <strong>Phonon</strong> per GStreamer, QuickTime e DirectShow9, che migliora le possibilità multimediali di KDE su Windows e Mac OS.</li>
</ul>

<h3>
  Nuove piattaforme
</h3>
<ul style="text-align: justify">
    <li>Il supporto a <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a> è in via di sviluppo. KDE più o meno funziona su OSOL, ma ci sono ancora dei grossi bug che devono essere risolti.</li>
    <li>I programmatori che sviluppano su <strong>Windows</strong> hanno la possibilità di <a href="http://windows.kde.org">scaricare</a> una anteprima delle applicazioni KDE per la loro piattaforma. Le librerie sono relativamente stabili anche se non tutte le loro capacità sono ancora disponibili per Windows. Alcune applicazioni funzionano abbastanza bene su Windows, altre potrebbero non funzionare così bene.</li>
    <li><strong>Mac OS X</strong> è un'altra delle piattaforme verso le quali KDE si affaccia per la prima volta.
    <a href="http://mac.kde.org">KDE su Mac</a> non è ancora pronto per l'uso quotidiano. Nonostante il supporto multimediale attraverso Phonon sia già disponibile, l'integrazione hardware e la ricerca non sono ancora complete.</li>
</ul>

<h3>
  Schermate
</h3>

<a id="screenshots-dolphin"></a>

<h4>Dolphin</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-treeview.png">
	<img src="/announcements/4/4.1.0/dolphin-treeview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>La nuova vista ad albero fornisce rapido accesso attraverso le cartelle. Si noti che questa funzione non è predefinita e va attivata.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-tagging.png">
	<img src="/announcements/4/4.1.0/dolphin-tagging_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nepomuk fornisce l'etichettatura e la valutazione per tutto KDE, e quindi anche per Dolphin</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-icons.png">
	<img src="/announcements/4/4.1.0/dolphin-icons_thumb.png" class="img-fluid">
	</a> <br/>
	<em>L'anteprima nelle icone e le barre informative forniscono un immediato riscontro visivo.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-filterbar.png">
	<img src="/announcements/4/4.1.0/dolphin-filterbar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Trova facilmente i tuoi file con la barra del filtro.</em>
</div>
<br/>

<a id="screenshots-gwenview"></a>

<h4>Gwenview</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-browse.png">
	<img src="/announcements/4/4.1.0/gwenview-browse_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Puoi navigare nelle cartelle contenenti immagini con Gwenview. Il menu contestuale ti permette di raggiungere facilmente le applicazioni più comuni.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-open.png">
	<img src="/announcements/4/4.1.0/gwenview-open_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Aprire file sul proprio disco fisso oppure in rete non sembrano due cose molto diverse grazie all'infrastruttura di KDE.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-thumbnailbar.png">
	<img src="/announcements/4/4.1.0/gwenview-thumbnailbar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>La nuova barra delle anteprime permette di navigare facilmente attraverso le immagini. Disponibile anche nella modalità a schermo intero.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-sidebar.png">
	<img src="/announcements/4/4.1.0/gwenview-sidebar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>La barra laterale di Gwenview permette di raggiungere facilmente informazioni aggiuntive e opzioni per la modifica delle immagini.</em>
</div>
<br/>

<a id="screenshots-marble"></a>

<h4>Marble</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/marble-globe.png">
	<img src="/announcements/4/4.1.0/marble-globe_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Marble, il mappamondo.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/marble-osm.png">
	<img src="/announcements/4/4.1.0/marble-osm_thumb.png" class="img-fluid">
	</a> <br/>
	<em>La nuova integrazione con OpenStreetMap fornisce annche informazioni sulle strade pubbliche.</em>
</div>
<br/>

<a id="screenshots-kwin"></a>

<h4>KWin</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-desktopgrid.png">
	<img src="/announcements/4/4.1.0/kwin-desktopgrid_thumb.png" class="img-fluid">
	</a> <br/>
	<em>La «Griglia Desktop» di KWin concretizza il concetto dei desktop virtuali e ti permette di trovare facilmente la finestra che stavi cercando.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-coverswitch.png">
	<img src="/announcements/4/4.1.0/kwin-coverswitch_thumb.png" class="img-fluid">
	</a> <br/>
	<em>L'effetto Coverswitch abbellisce il passaggio da una applicazione all'altra attraverso Alt+Tab. Si può attivare nelle impostazioni degli effetti del desktop.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-wobbly.png">
	<img src="/announcements/4/4.1.0/kwin-wobbly_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Anche KWin ha ora il famoso effetto delle finestre tremolanti (va attivato).</em>
</div>
<br/>

<a id="screenshots-plasma"></a>

<h4>Plasma</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/plasma-folderview.png">
	<img src="/announcements/4/4.1.0/plasma-folderview_thumb.png" class="img-fluid">
	</a> <br/> 
	<em>Il nuovo plasmoide «Vista delle cartelle» dà la possibilità di mostrare sul Desktop il contenuto di una cartella qualsiasi. Trascina una cartella sul desktop (con i widget sbloccati) per creare una «Vista delle cartelle». In questo modo si possono visualizzare non solo cartelle locali ma anche di rete.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/panel-controller.png">
	<img src="/announcements/4/4.1.0/panel-controller_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Il nuovo strumento per le impostazioni del pannello ti permette di modificare le dimensioni e le posizioni dei pannelli. Si può anche modificare la posizione di icone e applet.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/krunner-screenie.png">
	<img src="/announcements/4/4.1.0/krunner-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Con KRunner si possono avviare facilmente le applicazioni, creare messaggi di posta elettronica per i contatti e altre piccole cose.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/plasma-kickoff.png">
	<img src="/announcements/4/4.1.0/plasma-kickoff_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Il menu Kickoff è stato migliorato.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/switch-menu.png">
	<img src="/announcements/4/4.1.0/switch-menu_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Si può scegliere tra Kickoff e il menu tradizionale.</em>
</div>
<br/>

<h4>
  Problemi noti
</h4>
<ul style="text-align: justify">
    <li>Gli utenti delle schede <strong>NVidia</strong> che utilizzano il driver binario fornito dal produttore potranno riscontrare di problemi nella velocità di ridimensionamento delle finestre e durante il passaggio da una finestra all'altra. Abbiamo notificato il problema agli ingegneri NVidia, ma il problema non è stato ancora risolto. Informazioni su come migliorare le prestazioni si possono trovare su <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a>, anche se dobbiamo per forza di cose attendere che NVidia risolva il problema nei driver.</li>
</ul>

<h4>
  Scaricalo, avvialo, provalo
</h4>
<p style="text-align: justify">
  I volontari della comunità e i distributori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti precompilati per KDE 4.1.0 per molte distribuzioni Linux, Mac OS X e Windows. Controlla nel gestore di pacchetti della tua distribuzione.
</p>

<h4>
  Compilare KDE 4.1.0
</h4>
<p style="text-align: justify">
  <a id="source_code"></a><em>Codice sorgente</em>.
  Il codice sorgente completo per KDE 4.1.0 può essere <a href="http://www.kde.org/info/4.1.0">scaricato liberamente</a>. Le istruzioni per la compilazione e l'installazione di KDE 4.1.0 si trovano nella <a href="/info/4.1.0">Pagina informativa su KDE 4.1</a>, oppure su <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}