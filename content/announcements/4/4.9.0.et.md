---
aliases:
- ../4.9
- ../4.9.et
date: '2012-12-05'
title: KDE väljalase 4.9 – Claire Lotioni mälestuseks
---

<p>
<img src="/stuff/clipart/klogo-official-oxygen-128x128.png" class="float-left m-2" />KDE-l on rõõm teatada uusimatest väljalasetest, mis uuendavad oluliselt <a href="./plasma">KDE Plasma töötsoone</a>, <a href="./applications">KDE rakendusi</a> ja <a href="./platform">KDE platvormi</a>. Versioon 4.9 pakub rohkelt uusi võimalusi ning paranenud stabiilsust ja jõudlust.
</p>
<p>
Käesolev väljalase on pühendatud KDE kaasautori <a href="http://dot.kde.org/2012/05/20/remembering-claire-lotion">Claire Lotioni</a> mälestusele. Claire'i särav isiksus ja entusiasm innustasid paljusid kogukonna liikmeid ning tema teedrajav tegevus meie arendajate kokkusaamiste struktuuri, sisu ja sageduse alal muutis tõsiselt seda, kuidas me praegu käsitleme enda põhiülesannete täitmist. Selle ja muugi tegevusega jättis ta kustumatu jälje täna ilmavalgust nägevale tarkvarale ning me oleme talle äärmiselt tänulikud kõige eest, mida ta tegi.</p>
<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-desktop.png">
	<img src="/announcements/4/4.9.0/kde49-desktop-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>
<p>
KDE kvaliteedimeeskond loodi juba käesoleva aasta algupoolel eesmärgiga parandada KDE tarkvara üldist kvaliteeti ja stabiilsust. Erilist tähelepanu pöörati võrreldes eelmiste väljalasetega tagasiminekute tuvastamisele ja parandamisele. See oli tähtsaim eesmärk, sest vaid nii saab tagada, et iga väljalase on eelmisest parem.
</p>
<p>
Meeskond võttis samuti ette väljalasete märksa karmima testimise alates beetaversioonidest. Uued vabatahtlikud testijad said väljaõpet ning peeti mitu intensiivset testimispäeva. Tavapärase ülevaatliku testimise asemel anti testijatele ülesanne keskenduda konkreetsetele teemadele, mis olid võrreldes eelmiste väljalasetega muutunud. Mõne kriitilise tähtsusega komponendi puhul koostati ja käidi läbi põhjalikud kontrollnimekirjad. Meeskond avastas varakult hulga olulisi puudujääke ning aitas neid koos arendajatega parandada. Meeskond ise andis beeta- ja eelväljalasete kohta teada üle 160 vea, millest paljud said ka parandatud. Loomulikult andsid veel paljudest vigadest teada ka teised beeta- ja eelväljalasete kasutajad. Neist pingutustest oli palju abi, sest arendajad said nüüd koondada tähelepanu vigade parandamisele.
</p>
<p>
KDE kvaliteedimeeskonna panuse tõttu võib öelda, et 4.9 väljalasked on kõigi aegade parimad.
</p>
<p>
Üks veaparandus väärib erilist tähelepanu. Juba 2007. aastal teada antud Okulari viga oli kogunud ligemale 1100 häält, mis näitas, et tegu on paljudele kasutajatele tähtsa probleemiga. Nad kurtsid, et kuigi saavad teha märkmeid, ei saa nad neid salvestada ega trükkida. Paljude kommenteerijate ja Okulari IRC-kanalil sõna võtnud inimeste abil leidis Fabio D'Urso lahenduse, mis võimaldab Okulari PDF-dokumentide märkusi salvestada ja trükkida. Vea parandamine nõudis omajagu tööd KDE teekide kallal ja tähelepanu pööramist ka üldisele tarkvarakujundusele, et muude kui PDF-dokumentide kasutamine ei kannataks. See oli Fabiole esimene kogemus KDE arendajana ning selle taga seisis just tema isiklik kogemus: ta kohtas viga ja otsustas sellega midagi ette võtta.
</p>

<h2><a href="./plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Plasma töötsoonid 4.9 – põhikomponentide täiustused</a></h2>

<p>
Plasma töötsoonide peamistest uuendustest tasub esile tõsta olulisi täiustusi failihalduris Dolphin, X'i terminali emulaatoris Konsole, tegevustes ja aknahalduris Kwin. Neist kõneleb lähemalt <a href="./plasma">'Plasma töötsoonide teadaanne'.</a>
</p>

<h2><a href="./applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Uued ja täiustatud KDE rakendused 4.9</a></h2>
<p>
Täna ilmunud uute ja täiustatud KDE rakenduste seast väärivad märkimist Okular, Kopete, KDE PIM, õpirakendused ja mängud. Neist kõneleb lähemalt <a href="./applications">'KDE rakenduste teadaanne'</a>
</p>

<h2><a href="./platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>KDE platvorm 4.9</a></h2>

<p>
Tänane KDE platvormi väljalase sisaldab veaparandusi, muulaadseid kvaliteediparandusi, võrgutäiustusi ja valmistumist üleminekuks raamistikule 5
</p>
<h2>Levitage sõna ja jälgige toimuvat: silt "KDE" on oluline</h2>
<p>
KDE julgustab kõiki levitama sõna sotsiaalvõrgustikes. Edastage lugusid uudistesaitidele, kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter, identi.ca. Laadige ekraanipilte üles sellistesse teenustesse, nagu Facebook, Flickr, ipernity ja Picasa, ning postitage neid sobivatesse gruppidesse. Looge ekraanivideoid ja laadige need YouTube'i, Blip.tv-sse, Vimeosse või mujale. Ärge unustage lisada üleslaaditud materjalile silti "KDE", et kõik võiksid vajaliku materjali hõlpsamini üles leida ja KDE meeskond saaks koostada aruandeid KDE 4.9 väljalasete kajastamise kohta.
</p>
<p>
Kõike seda, mis toimub seoses väljalaskega sotsiaalvõrgustikes, võite jälgida KDE kogukonna otsevoos. See sait koondab reaalajas kõik, mis toimub identi.ca-s, twitteris, youtube'is, flickris, picasawebis, ajaveebides ja paljudes teistes sotsiaalvõrgustikes. Otsevoo leiab aadressilt <a href="http://buzz.kde.org">buzz.kde.org</a>.
</p>
<h2>Väljalaskepeod</h2>
<p>
Nagu ikka, korraldavad KDE kogukonna liikmed kõikjal maailmas väljalaskepidusid. Mõned on juba praegu ette teada, aga neid tuleb kindlasti rohkemgi. Nende kohta annab aimu meie <a href="http://community.kde.org/Promo/Events/Release_Parties/4.9">pidude nimekiri</a>. Kõik on oodatud osalema! Pidudelt leiab nii head seltskonda, vaimustavaid kõnesid kui ka veidi süüa-juua. See on suurepärane võimalus saada teada, mis toimub KDE-s, lüüa ise kaasa või ka lihtsalt kohtuda teiste kasutajate ja arendajatega.
</p>
<p>
Me innustame inimesi pidusid korraldama. Neil on lõbus viibida nii peoperemehe kui ka külalisena! Uuri lähemalt <a href="http://community.kde.org/Promo/Events/Release_Parties">näpunäiteid, kuidas pidu korraldada</a>.
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.9/&amp;title=KDE%20releases%20version%204.9%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.9/" data-text="#KDE releases version 4.9 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.9/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.9%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.9/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde49"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde49"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde49"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde49"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2>Väljalasketeadetest</h2>
<p>
Käesolevad väljalasketeated koostasid Jos Poortvliet, Carl Symons, Valorie Zimmerman, Jure Repinc, David Edmundson ja teised KDE propageerimismeeskonna ning laiemagi kogukonna liikmed. Eesti keelde tõlkis need Marek Laane. Väljalasketeated tõstavad esile kõige tähtsamaid muudatusi suures arendustöös, mida KDE tarkvara on viimasel poolel aastal üle elanud.
</p>

<h4>KDE toetamine</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify"> KDE e.V. uus <a
href="http://jointhegame.kde.org/">toetajaliikme programm</a> on
nüüd avatud. 25 euro eest kvartalis võite omalt poolt kindlustada,
et KDE rahvusvaheline kogukond kasvab ja areneb ning suudab valmistada
maailmaklassiga vaba tarkvara.</p>

<p>&nbsp;</p>
