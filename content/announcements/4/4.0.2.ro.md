---
aliases:
- ../announce-4.0.2.ro
date: '2008-03-05'
title: KDE 4.0.2 Release Announcement
---

<h3 align="center">
  Proiectul KDE lansează a doua ediție de traduceri și îmbunătățiri pentru cel mai popular mediu de birou liber</h3>

<p align="justify">
  <strong>
    Comunitatea KDE lansează a doua ediție de traduceri și îmbunătățiri pentru versiunea 4.0 a popularului mediu de birou, conținînd posibilități noi ale biroului Plasma și numeroase corecții, 
îmbunătățiri ale performanței și traduceri actualizate
</strong>
</p>

<p align="justify">
 <a href="http://www.kde.org/">Comunitatea KDE</a> a anunțat azi disponibilitatea imediată a KDE 4.0.2, a doua ediție de mentenanță și corecție pentru ultima generație a celui mai avansat și puternic mediu de birou liber. 
KDE 4.0.2 vine cu un birou de bază și multe alte pachete de administrare, rețea, educație, utilitare, multimedia, jocuri, grafică, dezvoltare web și multe altele. 
 Renumitele unelte și aplicații KDE sînt disponibile în aproximativ 50 de limbi.
</p>
<p align="justify">
 KDE, inclusiv toate bibliotecile și aplicațiile sale, este disponibil gratuit sub licențele Open Source.
 KDE poate fi obținut sub formă de cod-sursă și în diverse formate binare de la
 <ahref="http://download.kde.org/stable/4.0.2/">http://download.kde.org</a> și, de asemenea, pe <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
 sau oricare din <a href="http://www.kde.org/download/distributions">principalele sisteme GNU/Linux și UNIX</a> livrate în ziua de azi.
</p>

<h4>
  <a id="changes">Îmbunătățiri</a>
</h4>
<p align="justify">

KDE 4.0.1 e o versiune de mentenanță, care aduce corecții problemelor raportate în <a href="http://bugs.kde.org/">Sistemul KDE de urmărire a erorilor</a>
și îmbunătățește suportul pentru traducerile deja existente, cît și pentru cele noi. Pentru KDE 4.0.2,
echipele de Documentație și Traducere au făcut o excepție de la regula generală de a nu modifica șirurile vizibile, astfel încît dezvoltatorii Plasma au putut
introduce cîteva posibilități noi în această versiune KDE.
<br />
Îmbunătățirile acestei ediții includ, dar nu se limitează la:

</p>
<ul>
    <li>
    Posibilități noi în Plasma. Mărimea și poziția panoului pot fi de acum configurate,
    vizibilitatea cîtorva opțiuni este accentuată pentru a stimula noii utilizatori să descopere modul de lucru și funcționalitatea Plasma.
    </li>
    <li>
    Versiunile de KDE în limbile farsi și islandeză și-au îmbunătățit localizarea, alte traduceri fiind îmbunătățite în continuare. 
KDE 4.0.2 este disponibil în 50 de limbi.
    </li>
    <li>

    Defectele de redesenare ale motorului de navigare web KHTML au fost luate în vizor. Acum, KHTML

susține mai multe situri web, acceptînd mai multe tipuri de documente ce nu sînt conforme cu standardul HTML4.
</li>

</ul>

<p align="justify">
Numeroase deficiențe de stabilitate au fost remediate în Kopete (mesagerul instant KDE) și Okular,
 cititorul de documente.</p>

<p align="justify">
 Pentru o listă detaliată a îmbunătățirilor de la ultima ediție KDE, consultați <a href="/announcements/changelogs/changelog4_0_1to4_0_2">Jurnalul modificărilor KDE
4.0.2</a>.
</p>

<p align="justify">

Informații suplimentare despre îmbunătățirile edițiilor din seria KDE 4.0.x sînt disponibile în <a href="/announcements/4/4.0.ro">Anunțul de lansare KDE 4.0</a>
și în <a href="/announcements/4/4.0.1.ro">Anunțul de lansare KDE 4.0.1</a>.

</p>

<h4>
   Instalarea pachetelor binare KDE 4.0.2
</h4>
<p align="justify">
  <em>Distribuitori</em>.
  Unii producători de sisteme de operare Linux/UNIX au avut amabilitatea de a oferi pachete binare pentru KDE 4.0.2 la unele versiuni ale distribuțiilor acestora,
 iar în alte cazuri voluntari din comunitate au făcut acest lucru.
 Unele din aceste pachete binare sînt disponibile pentru descărcare liberă de pe 
<ahref="http://download.kde.org/binarydownload.html?url=/stable/4.0.2/">http://download.kde.org</a>.
 Pachetele binare suplimentare, la fel ca și actualizările pachetelor deja existente,
 vor deveni disponibile în cursul săptămînilor viitoare.

</p>

<p align="justify">
  <a id="package_locations"><em>Amplasările pachetelor</em></a>.
  Pentru o listă actuală cu toate pachetele binare despre care Proiectul KDE a fost
 informat, vizitați <a href="/info/4.0.2">Pagina de informare KDE 4.0.2</a>.
</p>

<h4>
  Compilarea KDE 4.0.2
</h4>
<p align="justify">
  <a id="source_code"></a><em>Cod-sursă</em>.
  Codul-sursă complet pentru KDE 4.0.2 poate fi <ahref="http://download.kde.org/stable/4.0.2/src/">descărcat liber</a>.
 Instrucțiuni de compilare și instalare KDE 4.0.2
 sînt disponibile pe <a href="/info/4.0.2#binary">Pagina de informare KDE 4.0.2</a>.

</p>

<h4>
  Sprijină KDE
</h4>
<p align="justify">
 KDE este un proiect <a href="http://www.gnu.org/philosophy/free-sw.html">liber</a>
 care există și crește numai datorită ajutorului din partea a numeroși voluntari care își
pun la dispoziție timpul și efortul. KDE se află într-o continuă căutare de noi voluntari și
contribuții, fie că este vorba de programare, depanare sau raportarea erorilor, întocmirea documentației,
traducere, promovare, bani, etc. Toate contribuțiile sînt înalt apreciate și acceptate cu nerăbdare.
 Vă rugăm să citiți pagina <ahref="/community/donations/">Susținerea KDE</a> pentru mai multe informații.</p>

<p align="justify">
Așteptăm vești de la dumneavoastră!
</p>

<h4>Despre KDE 4</h4>
<p>
KDE 4.0 este un mediu de birou liber, inovator, ce conține o mulțime de aplicații atît pentru utilizarea de zi cu zi,
 cît și în scopuri specifice. Plasma este noul înveliș al mediului de birou dezvoltat pentru KDE 4 care asigură
 o interfață intuitivă, ce facilitează interacțiunea cu mediul de birou și aplicațiile. Navigatorul Konqueror integrează mediul web cu cel de birou.
 Gestionarul de fișiere Dolphin, vizualizatorul de documente Okular și Centrul de control al setărilor de sistem completează mediul de birou.
<br />
KDE este construit pe baza Bibliotecilor KDE, care asigură un acces facil la resursele din rețea prin intermediul
 interfeței KIO și a capacităților vizuale avansate oferite de Qt4. Phonon și Solid, la rîndul lor componente ale 
Bibliotecilor KDE, adaugă un cadru de lucru multimedia și o mai bună integrare a echipamentelor fizice pentru toate 
aplicațiile KDE.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}