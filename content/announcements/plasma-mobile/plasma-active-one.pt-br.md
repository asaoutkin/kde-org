---
aliases:
- ../plasma-active-one
title: "9.10.11: Plasma Active One disponível !"
date: "2011-10-09"
---

<p align="justify"><em>
Um dispositivo móvel deve ser mais que uma simples coleção de aplicativos. Ele deve refletir quem você é. O Plasma Active traz o melhor para o seu tablet, a partir da novíssima tecnologia de Activities baseadas em touch-screen.
</em></p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/activity.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/activity.png" class="img-fluid" alt="A vacation planning Activity">
	</a> <br/>
	<em>A vacation planning Activity</em>
</div>
<br/>

<p align="justify">
No dia 09 de Outubro de 2011 o primeiro lançamento do <a href="http://plasma-active.org/">Plasma Active</a> se torna publicamente disponível. A interface touch screen do Plasma Active One é muito mais que um lançador de aplicativos. Assim que o seu dispositivo é ligado, ao invés da exibição tradicional do grid de aplicações, é exibido o gerenciador de Activities apresentando o seu projeto, tarefa ou idéia atual. Com as Activities, você pode reunir todos os documentos, pessoas, sites web, conteúdo multimídia e widgets, relacionados a um tópico, em um único lugar, construíndo visões personalizadas e interativas da sua vida.
</p>

<p align="justify">
Com o Plasma Active as possibilidades são ilimitadas. Você pode adicionar quantos elementos você quiser a uma Activity, com o auxílio da sua funcionalidade de scroll ilimitado. Você pode criar quantas Activities quiser e alternar entre elas via touch-screen, utilizando o Activity Switcher.
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/activity-add-content.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/activity-add-content.png" class="img-fluid" alt="Add content to your Activity">
	</a> <br/>
	<em>Add content to your Activity</em>
</div>
<br/>

<p align="justify">
Você escolhe o objetivo de uma Activity: planejar suas férias, trabalhar em um projeto da escola ou do trabalho, organizar as fotos da festa bacana do fim de semana, manter informações sobre suas redes sociais ou organizar seus sites preferidos de notícias. Crie uma Activity, informe o seu nome e seu papel de parede e comece a adicionar seus apetrechos ou diretamente através do botão "Adicionar" ou usando a interface Share Like Connect.
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/share-like-connect.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/share-like-connect.png" class="img-fluid" alt="Sharing a picture using Share Like Connect">
	</a> <br/>
	<em>Sharing a picture using Share Like Connect</em>
</div>
<br/>

<p align="justify">
Share Like Connect é um grupo de três pequenos ícones sempre disponíveis no painel superior. Eles permitem que você rapidamente conecte o que você está fazendo a qualquer Activity - sites web ou imagens sendo visualizados, documentos, etc. Use o Share Like Connect para registrar seus bookmarks. Envie facilmente o que você está visualizando, por e-mail, para pessoas da sua lista de contatos.
</p>
<p align="justify">
Complementando as Activities, o Plasma Active disponibiliza um lançador rápido e eficiente de aplicações e inclui, por padrão, uma variedade de aplicativos para você usar e se divertir. Basta puxar o painel superior para baixo para tornar a área de seleção de aplicações (Application Peek) visível. A seleção de aplicações apresenta as aplicações atualmente em execução, de modo que você pode facilmente alternar ou fechar os aplicativos. Puxando a seleção de aplicações um pouco mais para baixo faz com que o lançador de aplicações (Application Launcher) fique visível. Ele apresenta uma lista de ícones similar às outras interfaces para tablets e smartphones. A barra de busca integrada permite que você facilmente busque por aplicações instaladas no sistema. 
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/peek-and-launch.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/peek-and-launch.png" class="img-fluid" alt="Peek and Launch allow you to switch between apps and open new ones">
	</a> <br/>
	<em>Peek and Launch allow you to switch between apps and open new ones</em>
</div>
<br/>

<p align="justify">
Quando uma aplicação é iniciada ela automaticamente se conecta à Activity atual. Aplicações relacionadas são mantidas em grupo e fora da sua visão quando você alterna para uma Activity diferente. O Plasma Active traz produtividade imediata para você. Várias aplicações estão por padrão incluidas - navegador web, visualizador de imagens, player de áudio e vídeo (Bangarang Active), visualizador de documentos com funcionalidades simples de edição (Calligra Active [beta]), total controle de e-mail e calendários (Kontact Touch) e diversos jogos divertidos. Estas aplicações são totalmente otimizadas para a interface touch screen. Centenas de outras aplicações estão gratuitamente disponíveis através dos repositórios on-line, embora a maioria delas não estejam ainda otimizadas para touch-screen.
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/kontact-touch.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/kontact-touch.png" class="img-fluid" alt="Kontact Touch adds scalable email and groupware to Plasma Active">
	</a> <br/>
	<em>Kontact Touch adds scalable email and groupware to Plasma Active</em>
</div>
<br/>

<p align="justify">
Plasma Active é projetado e construído de forma aberta e é lançado sob a forma de software gratuito e de código aberto. Por trás das idéias do Plasma Active está uma comunidade vibrante de participantes e empresas trabalhando em conjunto por um único ideal: a criação de aplicações incríveis para os dispositivos atuais. Não há lock-ins proprietários ou segredos na frente dos esforços de projeto e desenvolvimento, somente uma fantástica experiência de usuário que você pode usar, se divertir e participar.
</p>
<h2>Como Obter o Plasma Active One</h2>
<p align="justify">
Imagens nas versões instalável e live estão <a href="http://community.kde.org/Plasma/Active/Installation#Live_Images">disponíveis para download</a>. Instruções detalhadas de instalação podem ser obtidas na página <a href="http://community.kde.org/Plasma/Active/Installation">Instalação do Plasma Active</a>. Tipicamente a instalação consiste simplesmente na inserção de um pendrive USB bootável no tablet, uma reinicialização e seguir as instruções na tela. 
</p>

<p align="justify">
É também possível rodar o Plasma Active em uma máquina virtual para propósitos de teste e avaliação. Para maiores informações, bem como os problemas dessa abordagem, por favor refira-se à página <a href="http://community.kde.org/Plasma/Active/Installation#Running_a_Plasma_Active_in_a_Virtual_Machine">Plasma Active em máquinas virtuais</a>. Recomendamos fortemente, entretanto, a execução do Plasma Active em um tablet para melhor experiência. 
</p>

<h2>O Futuro do Plasma Active</h2>
<p align="justify">
O <strong>Plasma Active One<strong> é mais um passo da longa jornada que a equipe KDE Plasma criou desde o início do projeto em 2007, originalmente para criação de um novo shell desktop. E não chegamos no fim desta caminhada. O Plasma Active Two será lançado em meados de Dezembro de 2011 com o Plasma Active Three programado para ser lançado no terceiro trimestre de 2012. 
</p>
<p align="justify">
O <strong>Plasma Active Two</strong> terá suporte a recomendações automatizadas a Activities, permitindo que o Plasma Active ajude você a coletar e adicionar informações e documentos relevantes às suas Activities. Teremos também a visualização, filtragem e ordenação melhorados de todos os tipos de mídias e informações acessíveis no Plasma Active. Outras funcionalidades serão também desenvolvidas para o Share Like Connect baseado em plugins. Esta versão irá também conter melhorias relacionadas a estabilidade e desempenho, bem como correção de bugs. 
</p>
<p align="justify">
O <strong>Plasma Active Three</strong> será outro lançamento importante onde focaremos nos desafios de segurança de dispositivos, de modo a manter os seus dados seguros e sob controle. Na versão três, começaremos a implementação de forms factors adicionais, para suportar settop boxes e handhelds. O Plasma é construído com a filosofia de "interfaces altamente modularizadas, re-utilizáveis e re-integráveis". Isto nos permite mover rapidamente de um tipo de dispositivo para outro, mantendo sempre uma ótima experiência de usuário. Pretendemos explorar ao máximo esta flexibilidade nas futuras versões.
</p>
<p align="justify">
Em ciclos de desenvolvimento futuros, um maior número de aplicativos Active e widgets estarão disponíveis. Desenvolvedores de aplicações já existentes estão convidados a se juntarem a nós na "ativação" de suas aplicações para suportar touch-screen e outros form factors. As melhores e mais úteis aplicações serão lançadas nas próximas versões do Plasma Active. Um sistema fácil para download de outras aplicações estará também disponível.
</p>
<p align="justify">
Planejamos também incluir o ARM como arquitetura oficialmente suportada, em adição às plataformas Intel já contempladas. Este esforço está sendo executado em conjunto com as tarefas de redução do footprint do sistema operacional necessário para rodar o Plasma Active. Adicionalmente, estamos no processo de desenvolvimento de uma especificação clara e bem documentada sobre os requisitos subjacentes de sistema operacional necessários para uma boa execução do Plasma Active.
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/web-browser.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/web-browser.png" class="img-fluid" alt="Plasma Active's touch-friendly web browser">
	</a> <br/>
	<em>Plasma Active's touch-friendly web browser</em>
</div>
<br/>

<h2>Como Contribuir com o Plasma Active</h2>
<p align="justify">
Plasma Active é um projeto aberto: todo o projeto, planejamento e desenvolvimento é feito em um ambiente de trabalho baseado em consenso. Colaboradores da comunidade e empresas de criação de dispositivos ou aplicativos são bem vindos e capazes de participar da sua própria forma. Se você deseja criar novos widgets, add-ons para o Share Like Connect, novas aplicações, suportar novos dispositivos ou criar uma experiência de usuário totalmente nova usando o Plasma Active, estaremos sempre prontos para lhe receber. Esforços para testes, estabilização, análise de usabilidade e projeto gráfico para os softwares já existentes são também áreas de potencial colaboração.
</p>
<p align="justify">
As bibliotecas utilizadas pelo Plasma Active são licenciadas sob a LGPL, permitindo uma variedade de opções de licenciamento e possibilidades de extensão e configuração. Você pode contactar a equipe do Plasma Active na nossa <a href="https://mail.kde.org/mailman/listinfo/active">lista de discussão</a> ou no canal #active do irc.freenode.net. Existe também o <a href="http://forum.kde.org/viewforum?f=211">fórum KDE de auxílio ao usuário</a>. Solicitações privadas e de imprensa podem ser enviadas a Sebastian Kügler (sebas@kde.org) e Eva Brucherseifer (eva.brucherseifer@basyskom.de).
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/blinken.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/blinken.png" class="img-fluid" alt="Blinken is a fun and touch-friendly game for children">
	</a> <br/>
	<em>Blinken is a fun and touch-friendly game for children</em>
</div>
<br/>

<h2>Compatibilidade de Hardware</h2>

<p align="justify">
As imagens do Plasma Active One foram testadas e são suportadas nos dispositivos WeTab, ViewSonic ViewPad, ExoPC e Lenovo Ideapad. Mais informações sobre compatibilidade de dispositivos podem ser encontradas <a href="http://community.kde.org/Plasma/Active/Devices">aqui</a>. O Plasma Active deve funcionar bem em outros dispositivos baseados em Intel. O suporte a dispositivos ARM está ainda em desenvolvimento. Entretanto, o Plasma Active foi instalado e executado com sucesso no Nokia N900 e outros dispositivos ARM.
</p>

<h2>Compatibilidade de Software</h2>

<p align="justify">
Praticamente qualquer software disponível para a plataforma Linux pode ser executado no Plasma Active. Recomendamos aplicações baseadas no Qt e no KDE visto que elas apresentam uma melhor integração com o Plasma Active One, o qual é também baseado no KDE Platform 4.7 e Qt 4.7. Outras aplicações, entretanto, executam e funcionam da mesma forma que funcionariam em qualquer outro sistema baseado no Linux. Isso inclui aplicações em linha de comando, acessíveis através do terminal com suporte a touch já presente no Plasma Active.
</p>
<p align="justify">
O Plasma Active é compatível com os workspaces do <a href="https://www.kde.org/workspaces/plasmadesktop/">Plasma Desktop</a> e <a href="https://www.kde.org/workspaces/plasmanetbook/">Plasma Netbook</a>. São todos baseados no mesmo framework, compartilhando mais de 95% do código-fonte. Esta é uma abordagem radicalmente diferente para similaridade de  interfaces entre dispositivos. A maioria dos outros produtos para tablets ou são isolados (compartilhando pouco ou nada com outras interfaces de usuário) ou tentam utilizar interfaces desktop em displays pequenos. As interfaces baseadas no Plasma, entretanto, são criadas para tipos específicos de dispositivos mantendo ainda a grande maioria do código-fonte e esforços de engenharia. Consequentemente, widgets e aplicações escritas para o Plasma Desktop ou Netbook podem ser executadas normalmente em dispositivos com o Plasma Active. 
</p>

<h2>Agradecimentos</h2>

<p align="justify">
O Plasma Active One não seria possível sem o inacreditável apoio da comunidade Plasma e sem o suporte financeiro e de desenvolvimento da <a href="http://www.basyskom.com">basysKom</a> e <a href="http://www.open-slx.de/">open-slx</a>. O  Plasma Active é construído com base nos excelentes frameworks do KDE e Qt, nos dando excelentes pilares para iniciar qualquer trabalho. Estamos muito felizes em conviver com milhares de pessoas que vêm colaborado com as comunidades KDE e Qt ao longo dos últimos 15 anos. Agradecemos também aos esforços que resultaram na criação do MeeGo OS e do <a href="http://openbuildservice.org/">Open Build Service</a>, fundamentais para a finalização e lançamento do Plasma Active One.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
