---
title: Kirigami 1.1
release: kirigami-1.1
description: KDE Ships Kirigami 1.1.
date: 2016-09-26
---

Monday, 26 September 2016. After weeks of development and two small bugfix releases, we are happy to announce the first Kirigami minor release, version 1.1.

The Menu class features some changes and fixes which give greater control over the action triggered by submenus and leaf nodes in the menu tree. Submenus now know which entry is their parent, and allow the submenu's view to be reset when the application needs it to.

The OverlaySheet now allows to embed ListView and GridView instances in it as well.

The Drawer width now is standardized so all applications look coherent from one another and the title now elides if it doesn't fit. We also introduced the GlobalDrawer.bannerClicked signal to let applications react to banner interaction.

SwipeListItem has been polished to make sure its contents can fit to the space they have and we introduced the Separator component.

A nice fix for desktop Kirigami applications: The application window now has a default shortcut to close the application, depending on the system preferences, commonly Ctrl+Q.

Naturally this release also contains smaller fixes and general polishing.

As announced <a href="https://www.kde.org/announcements/plasma-5.7.95.php\">recently</a>, Plasma 5.8 will ship a revamped version of Discover based on Kirigami. As such we expect that all major distributions will ship Kirigami packages by then if they are not already doing so.

## Source Download

The source code for Kirigami releases is available for download from <a href='http://download.kde.org/stable/kirigami/'>download.kde.org</a>.

More information is on the <a href='https://techbase.kde.org/Kirigami'>Kirigami wiki page</a>.

GPG signatures are available alongside the source code for
verification. They are signed by release manager <a
href='https://sks-keyservers.net/pks/lookup?op=vindex&search=0xeaaf29b42a678c20'>Marco Martin with 0xeaaf29b42a678c20</a>.

## Feedback

{{% i18n_var "You can give us feedback and get updates on %[1]s or %[2]s or %[3]s." "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='/announcements/buttons/facebook-logo.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>" "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='/announcements/buttons/twitter-logo.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>" "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='/announcements/buttons/googleplus-logo.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" %}}

{{% i18n_var "Discuss Plasma 5 on the <a href='%[1]s'>KDE Forums Plasma 5 board</a>." "https://forum.kde.org/viewforum.php?f=289" %}}

{{% i18n_var "You can provide feedback direct to the developers via the <a href='%[1]s'>#Plasma IRC channel</a>, <a href='%[2]s'>Plasma-devel mailing list</a> or report issues via <a href='%[3]s'>bugzilla</a>. If you like what the team is doing, please let them know!" "irc://#plasma@freenode.net" "https://mail.kde.org/mailman/listinfo/plasma-devel" "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided" %}}

Your feedback is greatly appreciated.

## Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
