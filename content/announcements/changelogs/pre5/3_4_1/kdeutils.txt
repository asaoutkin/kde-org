2005-03-21 02:04 +0000 [r399390]  thiago

	* ark/compressedfile.cpp, ark/arkwidget.cpp: Backporting the
	  changes that fixed bugs #102014 and #102027.
	  arkwidget.cpp:1.96:1.97 compressedfile.cpp:1.25:1.26

2005-03-31 00:29 +0000 [r402062]  kniederk

	* kcalc/ChangeLog, kcalc/kcalc.cpp, kcalc/kcalc.h: GUI: included
	  last changes from HEAD, which were all minor (Mem-Buttons)

2005-04-05 15:31 +0000 [r403328]  binner

	* kgpg/listkeys.cpp: Bug 101245: No "Import Missing Signatures From
	  Keyserver" for own key BUG:101245

2005-04-17 13:24 +0000 [r406069]  rizsanyi

	* kdelirc/irkick/klircclient.cpp: Fix reading of remote data when
	  it contains many buttons - above a certain limit the buttons did
	  not fit into the socket buffer and the code only read till the
	  buffer did not get empty Now it waits a short timeout and if new
	  buttons arrive it continues reading. This is not the best
	  solution to the problem, but it is satisfactory and the better
	  ones would require code reorganization. BUGS:103668

2005-04-22 13:30 +0000 [r407136]  mlaurent

	* kdelirc/kcmlirc/kcmlirc.cpp: Backport launch irkick

2005-04-24 23:01 +0000 [r407638]  kniederk

	* kcalc/ChangeLog, kcalc/kcalc.cpp: GUI: Fix BUG 99817.

2005-04-24 23:12 +0000 [r407641]  kniederk

	* kcalc/ChangeLog: Fixed date.

2005-04-30 00:20 +0000 [r408731]  kniederk

	* kcalc/ChangeLog, kcalc/kcalc_button.cpp: Fix BUG 100316

2005-05-07 14:27 +0000 [r410313]  goutte

	* kfloppy/TODO (removed): Remove obsolete to-do (Partial backport
	  of revision 410311)

2005-05-07 14:52 +0000 [r410322]  goutte

	* kfloppy/floppy.h, kfloppy/floppy.cpp: Remove obsolete code,
	  private member variables or QString casts (Backport of revision
	  410095 and revision 410125)

2005-05-07 15:15 +0000 [r410325]  goutte

	* kfloppy/floppy.cpp: Do not allow the user to choose "Auto-detect"
	  on BSD Reason: the KFloppy code for BSD cannot auto-detect the
	  floppy size and fdformat on BSD does not seem to be able to do it
	  either. (Partial backport of revision 410087) (BSD only)

2005-05-07 15:38 +0000 [r410333]  goutte

	* kfloppy/format.cpp: Improve processing of the error message of
	  the program fdformat (Partial backports from revisions: 410059,
	  410082, 410148) (Linux only)

2005-05-07 15:54 +0000 [r410338]  goutte

	* kfloppy/floppy.cpp: - Disable the volume name line edit if the
	  volume name checkbox is unchecked (Backport of revision 410108) -
	  Initialize correctly the values comming from the config files
	  (The added user visible strings exists already in the file.)
	  (Partial backport of revision 410148)

2005-05-10 10:32 +0000 [r411876]  binner

	* kdeutils.lsm: update lsm for release

2005-05-12 07:21 +0000 [r412621]  binner

	* kcharselect/kcharselectui.rc: No toolbar -> no "Configure
	  Toolbars..." (105430)

2005-05-12 11:31 +0000 [r412702]  binner

	* kcharselect/kcharselectui.rc, kcharselect/kcharselectdia.cc: The
	  correct fix for PREV

2005-05-15 01:28 +0000 [r413967]  aseigo

	* kjots/KJotsMain.cpp: backport BR#105501 by Jaison Lee
	  CCMAIL:lee.jaison@gmail.com CCBUGS:105501

2005-05-19 12:58 +0000 [r415728]  fedemar

	* ark/arkwidget.cpp: Backport my fix for bug 99279 (Dead lock in
	  "Add to archive" action when providing invalid archive format).

2005-05-22 01:42 +0000 [r416616]  henrique

	* ark/main.cpp, ark/extractdlg.h, ark/arkwidget.cpp,
	  ark/extractdlg.cpp: * Backporting the fix for bug #105787

2005-05-22 08:04 +0000 [r416656]  binner

	* kcalc/version.h: version++

