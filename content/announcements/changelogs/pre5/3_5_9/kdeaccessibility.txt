2007-11-05 17:23 +0000 [r733157]  lunakl

	* branches/KDE/3.5/kdeaccessibility/kmag/kmag.cpp,
	  branches/KDE/3.5/kdeaccessibility/kmag/kmagselrect.cpp: Fix
	  handling positions with Xinerama. Also, setting things like
	  window type after a window is shown is somewhat late.

2008-02-13 09:52 +0000 [r774460]  coolo

	* branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm: 3.5.9

