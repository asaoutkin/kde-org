------------------------------------------------------------------------
r1017447 | sebas | 2009-08-30 17:32:11 +0000 (Sun, 30 Aug 2009) | 2 lines

build on arm

------------------------------------------------------------------------
r1017797 | ewoerner | 2009-08-31 14:45:44 +0000 (Mon, 31 Aug 2009) | 4 lines

Backport r1017796
Fix opendesktop widget always starting with fixed size (just noticed 205457 has already been fixed)
BUG:202937

------------------------------------------------------------------------
r1018094 | fabo | 2009-09-01 08:07:17 +0000 (Tue, 01 Sep 2009) | 2 lines

backport accessories-dictionary icon renaming.

------------------------------------------------------------------------
r1019099 | sebas | 2009-09-02 20:49:50 +0000 (Wed, 02 Sep 2009) | 2 lines

Fix build on ARM

------------------------------------------------------------------------
r1019179 | scripty | 2009-09-03 04:12:15 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019660 | ewoerner | 2009-09-04 09:12:29 +0000 (Fri, 04 Sep 2009) | 5 lines

Backport r1019653
Don't fail if the downloaded avatar image is corrupt
CCBUG:206101
Note: this is not intended as a fix for 206101 and probably doesn't fix that bug but only hides the effect

------------------------------------------------------------------------
r1020020 | scripty | 2009-09-05 03:22:04 +0000 (Sat, 05 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1020915 | sebas | 2009-09-07 15:23:44 +0000 (Mon, 07 Sep 2009) | 2 lines

Fix build on ARM

------------------------------------------------------------------------
r1021388 | scripty | 2009-09-09 03:39:04 +0000 (Wed, 09 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1022729 | jbrouault | 2009-09-12 11:44:41 +0000 (Sat, 12 Sep 2009) | 4 lines

backport of r1013632
BUG:194302
BUG:203949

------------------------------------------------------------------------
r1022793 | ivan | 2009-09-12 15:20:22 +0000 (Sat, 12 Sep 2009) | 3 lines

Small Lancelot/Air theme fixes


------------------------------------------------------------------------
r1023373 | annma | 2009-09-14 16:48:31 +0000 (Mon, 14 Sep 2009) | 2 lines

fix default picture as the svg one is blurred

------------------------------------------------------------------------
r1024456 | scripty | 2009-09-16 16:16:48 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024566 | bram | 2009-09-16 20:33:49 +0000 (Wed, 16 Sep 2009) | 2 lines

Respect the step interval setting, don't default to 1 second.

------------------------------------------------------------------------
r1025925 | scripty | 2009-09-20 03:20:09 +0000 (Sun, 20 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1025985 | ivan | 2009-09-20 11:20:57 +0000 (Sun, 20 Sep 2009) | 3 lines

Fixed bug 207904


------------------------------------------------------------------------
r1026976 | scripty | 2009-09-23 03:18:18 +0000 (Wed, 23 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1027178 | annma | 2009-09-23 13:28:01 +0000 (Wed, 23 Sep 2009) | 2 lines

don't use decimals

------------------------------------------------------------------------
r1027408 | scripty | 2009-09-24 03:15:47 +0000 (Thu, 24 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1028024 | ewoerner | 2009-09-25 14:35:50 +0000 (Fri, 25 Sep 2009) | 4 lines

Backport r1028022
change library name from attica to ocs, fixes loading of wrong catalog
CCBUG:208422

------------------------------------------------------------------------
r1028050 | ivan | 2009-09-25 17:04:32 +0000 (Fri, 25 Sep 2009) | 3 lines

fixed bug 208400


------------------------------------------------------------------------
r1028739 | scripty | 2009-09-28 03:06:52 +0000 (Mon, 28 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1029410 | aseigo | 2009-09-29 17:15:46 +0000 (Tue, 29 Sep 2009) | 3 lines

ulong instead of uint; patch by chrissalch
CCBUG:207327

------------------------------------------------------------------------
