2008-03-20 21:53 +0000 [r788160]  tohojo

	* branches/KDE/3.5/kdeutils/superkaramba/src/disksensor.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/disksensor.h: Fix a
	  bug in the superkaramba disk sensor which would cause the values
	  to overflow for large disks (> 2TB).

2008-04-03 14:49 +0000 [r793293]  mueller

	* branches/KDE/3.5/kdeutils/ark/ark.desktop: add
	  application/x-zip-compressed

2008-05-26 21:14 +0000 [r813022]  tohojo

	* branches/KDE/3.5/kdeutils/superkaramba/src/richtextlabel.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/clickarea.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/richtextlabel.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/graph.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/meter.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/imagelabel.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/clickarea.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/clickmap.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/bar.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/textlabel.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/graph.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/imagelabel.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/clickmap.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/bar.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/textlabel.h: Updated
	  Meter::setValue, ::maxValue and ::minValue to use long instead of
	  int. This allows for using large values to draw bars, so e.g.
	  bars of the disk sensor work when using both %u and %up formats.
	  Related to previous update of DiskSensor to use long instead of
	  int, to allow for hard drives >2TB.

2008-06-08 08:19 +0000 [r818284-818283]  dakon

	* branches/KDE/3.5/kdeutils/kgpg/kgpginterface.cpp: Don't delete
	  sender in slot, call deleteLater() instead. This should fix a
	  bunch of crashes on key operations. Backport of r818278

	* branches/KDE/3.5/kdeutils/kgpg/keyinfowidget.cpp: Fix memleak
	  Delete process class when it's done.

2008-06-15 10:32 +0000 [r820742]  kossebau

	* branches/KDE/3.5/kdeutils/khexedit/lib/kbufferranges.cpp,
	  branches/KDE/3.5/kdeutils/khexedit/lib/kbytesedit.cpp,
	  branches/KDE/3.5/kdeutils/khexedit/parts/kbytesedit/kbyteseditwidget.h,
	  branches/KDE/3.5/kdeutils/khexedit/lib/kbytesedit.h: fixed: build
	  on OpenSolaris, where names like DS, CS etc. are predefined in
	  system headers (Solaris Express B78, GCC 3.4.3) Thanks for the
	  patch, Moinak. I only have no idea if there ever will be a 3.5.10
	  release, for now you have to rely on the 3.5 branch. BUG:156494

2008-06-21 08:57 +0000 [r822717]  dakon

	* branches/KDE/3.5/kdeutils/kgpg/keyservers.h,
	  branches/KDE/3.5/kdeutils/kgpg/listkeys.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/keyservers.cpp: Fix exporting
	  multiple keys to keyserver at once BUG:154528

2008-06-29 22:54 +0000 [r826185]  anagl

	* branches/KDE/3.5/kdeutils/kfloppy/KFloppy.desktop,
	  branches/KDE/3.5/kdeutils/khexedit/khexedit.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/thinkpad/kmilo_thinkpad.desktop,
	  branches/KDE/3.5/kdeutils/kwallet/konfigurator/kwalletmanager_show.desktop,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/applnk/pcmcia.desktop,
	  branches/KDE/3.5/kdeutils/khexedit/parts/kbytesedit/kbyteseditwidget.desktop,
	  branches/KDE/3.5/kdeutils/kgpg/kgpg.desktop,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/applnk/klaptopdaemon.desktop,
	  branches/KDE/3.5/kdeutils/superkaramba/src/superkaramba.desktop,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/applnk/laptop.desktop,
	  branches/KDE/3.5/kdeutils/kdf/kcmdf.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/kmilo_kvaio/kcmkvaio/kvaio.desktop,
	  branches/KDE/3.5/kdeutils/khexedit/parts/kpart/khexedit2part.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/delli8k/kmilo_delli8k.desktop,
	  branches/KDE/3.5/kdeutils/kedit/KEdit.desktop,
	  branches/KDE/3.5/kdeutils/kdelirc/irkick/irkick.desktop,
	  branches/KDE/3.5/kdeutils/kdf/kdf.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/kmilod/kmilod.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/thinkpad/kcmthinkpad/thinkpad.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/demo/kmilo_demo.desktop,
	  branches/KDE/3.5/kdeutils/kcharselect/KCharSelect.desktop,
	  branches/KDE/3.5/kdeutils/ark/ark_part.desktop,
	  branches/KDE/3.5/kdeutils/charselectapplet/kcharselectapplet.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/kmilo_kvaio/kmilo_kvaio.desktop,
	  branches/KDE/3.5/kdeutils/kdf/kwikdisk.desktop,
	  branches/KDE/3.5/kdeutils/kjots/Kjots.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/powerbook/kmilo_powerbook.desktop,
	  branches/KDE/3.5/kdeutils/kdelirc/kcmlirc/kcmlirc.desktop,
	  branches/KDE/3.5/kdeutils/ktimer/ktimer.desktop,
	  branches/KDE/3.5/kdeutils/kregexpeditor/kregexpeditor.desktop,
	  branches/KDE/3.5/kdeutils/kcalc/kcalc.desktop,
	  branches/KDE/3.5/kdeutils/ark/ark.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/asus/kmilo_asus.desktop,
	  branches/KDE/3.5/kdeutils/kregexpeditor/kregexpeditorgui.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/generic/kmilo_generic.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/kmilod/kmilopluginsvc.desktop,
	  branches/KDE/3.5/kdeutils/kwallet/kwalletmanager.desktop,
	  branches/KDE/3.5/kdeutils/kwallet/konfigurator/kwallet_config.desktop,
	  branches/KDE/3.5/kdeutils/kwallet/kwalletmanager-kwalletd.desktop,
	  branches/KDE/3.5/kdeutils/kwallet/konfigurator/kwalletconfig.desktop,
	  branches/KDE/3.5/kdeutils/kmilo/powerbook2/kmilo_powerbook.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-07-10 16:10 +0000 [r830494]  dakon

	* branches/KDE/3.5/kdeutils/kgpg/keyservers.cpp: CCBUG:139774 Fix
	  this also for 3.5

2008-08-19 19:48 +0000 [r849607]  coolo

	* branches/KDE/3.5/kdeutils/kdeutils.lsm: update for 3.5.10

