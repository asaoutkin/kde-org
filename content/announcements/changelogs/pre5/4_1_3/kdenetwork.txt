2008-09-25 18:50 +0000 [r864859]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/yahoo/yahooeditaccount.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp:
	  backport r864858 - remove nested event loops from Yahoo

2008-09-26 06:12 +0000 [r864938]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/msn/msnnotifysocket.cpp:
	  Backport r864936: Make safe lookup of contacts using data
	  obtained from the wire.

2008-09-26 08:12 +0000 [r864952]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/msn/msnsocket.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/msn/msnsocket.h:
	  Backport r864950,864951 - Replace char * operations with
	  QByteArray. Fixes valgrind errors reported at
	  https://bugzilla.novell.com:443/show_bug.cgi?id=360333

2008-09-26 14:45 +0000 [r865058]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/gwaccount.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/ui/gwsearch.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/ui/gwsearch.h:
	  Merge -r865057 - various memory management cleanups

2008-09-26 19:03 +0000 [r865119-865118]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/ui/icquserinfowidget.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/icqaccount.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/ui/icqsearchdialog.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/ui/icqsearchdialog.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/icqcontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/ui/icquserinfowidget.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/icqaccount.cpp:
	  Backport commit 864907. We can't create new ICQ contact for
	  UserInfo because it will overwrite contact from contact list and
	  bad things may happen like duplicated contacts in contact list.

	* branches/KDE/4.1/kdenetwork/kopete/libkopete/kopeteaccount.cpp:
	  Backport commit 864908. Show warning if we are trying to
	  overwrite already created contact. Also don't add it to contact
	  hash.

2008-09-26 21:09 +0000 [r865150]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/aim/aimaccount.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/icqprotocol.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/oscarprivacyengine.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/oscaraccount.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/icqaccount.cpp:
	  Backport commit 865149. Use contacts().value instead of
	  contacts()[] because [] adds null pointers. The
	  contacts().contains() isn't reliable because the contacts() can
	  contain 0 pointer.

2008-09-27 06:59 +0000 [r865226-865225]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/conferencetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/joinconferencetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/gwmessagemanager.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontacttask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/deleteitemtask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/coreprotocol.cpp:
	  Backport r845443 - string fixes - to groupwise branch to ease
	  syncing branches in future

	* branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/deleteitemtask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/stream.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/requesttask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createconferencetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/rejectinvitetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/joinconferencetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/task.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/securestream.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/sendmessagetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontactinstancetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/joinchattask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/coreprotocol.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/searchchattask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/deleteitemtask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/qcatlshandler.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/stream.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/updatefoldertask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/leaveconferencetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontacttask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/joinconferencetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tlshandler.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/statustask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getstatustask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/privacyitemtask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/requesttask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/safedelete.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/joinchattask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/setstatustask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createfoldertask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/connectiontask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/modifycontactlisttask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/searchchattask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/updateitemtask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/logintask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/sendinvitetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontactinstancetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/conferencetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/bytestream.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/qcatlshandler.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontacttask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/connector.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/movecontacttask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/statustask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getstatustask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/safedelete.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/leaveconferencetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getchatsearchresultstask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tlshandler.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/updatecontacttask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/connectiontask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/modifycontactlisttask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createfoldertask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/setstatustask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/privacyitemtask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/updateitemtask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/searchusertask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/bytestream.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/conferencetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/keepalivetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/typingtask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/logintask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/sendinvitetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/pollsearchresultstask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/gwclientstream.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/eventtask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/connector.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getchatsearchresultstask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/movecontacttask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/task.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/securestream.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getdetailstask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/sendmessagetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createconferencetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/rejectinvitetask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/coreprotocol.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/updatecontacttask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/keepalivetask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/searchusertask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/updatefoldertask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/typingtask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/eventtask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/pollsearchresultstask.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getdetailstask.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/gwclientstream.h:
	  Backport r845829 - copyright header fixes - to groupwise branch
	  to ease syncing branches in future

2008-09-27 07:36 +0000 [r865231]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/rtf.cc,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/rtf.ll:
	  Backport r835363 - ebn fixes - to groupwise branch to ease
	  syncing branches in future

2008-09-27 07:41 +0000 [r865233]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/gwcontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/responseprotocol.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/ui/gweditaccountwidget.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/gwerror.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/gwerror.h,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getdetailstask.cpp:
	  Backport r842122 - don't crash on mmeeks' contact data - to
	  groupwise branch to ease syncing branches in future

2008-09-27 07:47 +0000 [r865235]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/gwcontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/aim/aimcontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/aim/icqcontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/aimcontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/jabber/jabbercontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/msn/msncontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/libkopete/kopeteaccountmanager.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/yahoo/yahoocontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/libkopete/kopetepluginmanager.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/icq/icqcontact.cpp:
	  Backport r856995 - enable Kiosk support for protocol actions to
	  4.1 branch

2008-09-27 07:56 +0000 [r865238-865237]  wstephens <wstephens@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/ui/gwsearch.cpp:
	  Backport r865128 - fix
	  https://bugzilla.novell.com/show_bug.cgi?id=396381

	* branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/gwcontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/userdetailsmanager.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/gwmessagemanager.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/getdetailstask.cpp:
	  Backport r865154 - fix
	  https://bugzilla.novell.com/show_bug.cgi?id=404147

2008-09-27 09:31 +0000 [r865258]  uwolfer <uwolfer@localhost>:

	* branches/KDE/4.1/kdenetwork/krdc/floatingtoolbar.cpp,
	  branches/KDE/4.1/kdenetwork/krdc/floatingtoolbar.h,
	  branches/KDE/4.1/kdenetwork/krdc/mainwindow.cpp: Backport: SVN
	  commit 865253 by uwolfer: Prevent possible race condition. Remove
	  ugly timer based hack. Thanks to Ben Klopfenstein for the input
	  on this issue.

2008-09-27 15:57 +0000 [r865446]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdenetwork/doc/krdc/index.docbook,
	  branches/KDE/4.1/kdenetwork/doc/kopete/index.docbook,
	  branches/KDE/4.1/kdenetwork/doc/kget/index.docbook,
	  branches/KDE/4.1/kdenetwork/doc/kppp/accounting.docbook:
	  documentation backport for 4.1.3 from trunk

2008-09-29 20:39 +0000 [r866039]  uwolfer <uwolfer@localhost>:

	* branches/KDE/4.1/kdenetwork/kget/core/kget.cpp,
	  branches/KDE/4.1/kdenetwork/kget/conf/dlgdirectories.cpp,
	  branches/KDE/4.1/kdenetwork/kget/ui/newtransferdialog.cpp:
	  Backport (parts of): SVN commit 866036 by uwolfer: Improve
	  stability of new transfer dialog (and friends...): * fix url ->
	  string -> url converstions * use deleteLater on QObjects * use
	  some const iterators * cache expensive results #171864

2008-09-29 21:11 +0000 [r866054]  uwolfer <uwolfer@localhost>:

	* branches/KDE/4.1/kdenetwork/kget/core/kget.cpp,
	  branches/KDE/4.1/kdenetwork/kget/conf/dlgdirectories.cpp:
	  SVN_SILENT fix compile

2008-09-30 20:14 +0000 [r866423]  woebbe <woebbe@localhost>:

	* branches/KDE/4.1/kdenetwork/kget/core/kget.cpp: compile (remove
	  strange whitespaces)

2008-10-01 17:04 +0000 [r866679]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/jabber/jabbergroupcontact.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/jabber/jabbergroupmembercontact.cpp:
	  backport 866673: make groupchat messages "importance low" so that
	  they don't trigger notification.

2008-10-03 16:47 +0000 [r867499]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/jabber/jabbergroupcontact.cpp:
	  Backport r867498: Fix lines being missing when joining a jabber
	  group chat with history replayed: all the lines said by people
	  who are not in the chat anymore would be missing.

2008-10-07 20:36 +0000 [r868988]  uwolfer <uwolfer@localhost>:

	* branches/KDE/4.1/kdenetwork/krdc/rdp/rdphostpreferences.cpp:
	  Backport: SVN commit 868986 by uwolfer: Fix issue that custom
	  resolutions do not get restored correctly. Fixes BUG: 172331

2008-10-11 11:55 +0000 [r869875]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/libkopete/kopetepluginmanager.cpp:
	  Backport commit 869874. Fix bug 172011: kopete crash during KDE
	  logout. CCBUG: 172011

2008-10-12 06:12 +0000 [r870249]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdenetwork/kppp/DB/Provider/Switzerland/.directory,
	  branches/KDE/4.1/kdenetwork/kppp/DB/Provider/Austria/.directory,
	  branches/KDE/4.1/kdenetwork/kppp/DB/Provider/Belarus/.directory:
	  SVN_SILENT made messages (.desktop file)

2008-10-14 06:17 +0000 [r871194]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/kopete/kopete.notifyrc,
	  branches/KDE/4.1/kdenetwork/krfb/krfb.notifyrc,
	  branches/KDE/4.1/kdenetwork/kopete/plugins/urlpicpreview/kopete_urlpicpreview.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-14 21:11 +0000 [r871414]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/libkopete/private/kopeteviewmanager.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/libkopete/kopetebehaviorsettings.kcfg:
	  Backport part of commit 871405. Fix bug 167976: Systray icon
	  shouldn't animate for existing chat windows. As we cannot change
	  GUI in KDE 4.1 you have to add/change [Behavior]
	  animateOnMessageWithOpenChat=false in
	  ~/.kde/share/config/kopeterc file to make it behave as in KDE 3.
	  CCBUG: 167976

2008-10-16 06:31 +0000 [r871937]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdenetwork/knewsticker/plasma-knewsticker-default.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-16 19:43 +0000 [r872280]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/libkopete/kopeteaccount.cpp:
	  Backport fix for bug 172985: Kopete crashed suddenly The
	  d->contacts can have empty entires if [] is used for lookup.
	  CCBUG: 172985

2008-10-18 09:13 +0000 [r872809-872808]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/kopete/chatwindow/chatmessagepart.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Backport SVN commit 872704. Fix bug 172485 Kopete has full CPU
	  load (freezes) when receiving a long line. CCBUG: 172485

	* branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp,
	  branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/liboscar/client.h:
	  Backport commits 871882 and 872433 Fix crash on login. BUG:
	  172997

2008-10-18 09:19 +0000 [r872811]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/libkopete/kopeteversion.h:
	  Bump version for KDE 4.1.3

2008-10-21 06:51 +0000 [r874287]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdenetwork/kppp/Kppp.desktop,
	  branches/KDE/4.1/kdenetwork/kppp/logview/kppplogview.desktop,
	  branches/KDE/4.1/kdenetwork/krfb/kinetd/kinetd.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-24 19:45 +0000 [r875552]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/jabber/jabberbasecontact.cpp:
	  Backport commit 875550. Don't put avatar into avatar manager if
	  contactId is empty. CCBUG: 173136

2008-10-26 10:08 +0000 [r876012]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp:
	  Backport commit 876011. Remove only devices with matching udi.
	  Don't increase i when device was removed from vector. CCBUG:
	  173530

2008-10-26 10:31 +0000 [r876034]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/aimcontactbase.cpp:
	  Backport commit 876033. Strip last BR before we add closing style
	  tags. CCBUG: 146380

2008-10-26 11:43 +0000 [r876051]  rjarosz <rjarosz@localhost>:

	* branches/KDE/4.1/kdenetwork/kopete/protocols/oscar/aimcontactbase.cpp:
	  Backport commit 876050.

2008-10-29 00:21 +0000 [r877255]  gkiagia <gkiagia@localhost>:

	* branches/KDE/4.1/kdenetwork/krfb/main.cpp,
	  branches/KDE/4.1/kdenetwork/krfb/krfbserver.cpp,
	  branches/KDE/4.1/kdenetwork/krfb/krfbserver.h: Backport r877254.
	  Integrate the rfb event system with qt's event loop instead of
	  running a custom event loop in KrfbServer. This fixes the major
	  problem that modal dialogs were closing immediately after they
	  were shown, which caused huge usability issues. CCBUG: 167955

2008-10-29 15:32 +0000 [r877455]  gkiagia <gkiagia@localhost>:

	* branches/KDE/4.1/kdenetwork/krfb/main.cpp: Backport r877454. Show
	  the ManageInvitationsDialog on startup, so that users are not
	  confused by the absence of a main window. CCBUG: 167953

