commit 98fe41427651ec34485e9879af09712f75628797
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Apr 28 15:18:23 2011 +0200

    bump version to 4.6.3

commit 34d8ae956174b38266d591e027b33ea5a9fc4c2e
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Apr 28 09:03:16 2011 +0200

    fix minor memory leak

commit 8dda138dcb7b75ccb699f53976f379193815dc56
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Apr 28 09:02:49 2011 +0200

    avoid passing random garbage to unlink()

commit b8ec6652f8b518bd4704101c24308e531e62cc90
Author: Burkhard Lück <lueck@hube-lueck.de>
Date:   Tue Apr 26 21:02:56 2011 +0200

    fix wrong file extension for kmplot
    REVIEW:101059
    (cherry picked from commit c61c931e5415d99bcb23815f03a7bc53ab24885b)

commit eb2fe7da7f82759ce156f7e81b42eff5689a5267
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Apr 26 17:43:28 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 30d8f05f9db791bf346f1cb33efba31e25dc9e51
Author: David Faure <faure@kde.org>
Date:   Tue Apr 26 11:49:17 2011 +0200

    Polish the last fix here, so that the debug output makes sense.

commit 32fffc6767d5f93796c804ea5528e404698a0455
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Mon Apr 25 22:06:24 2011 -0400

    On redirection, update the proxy script URL so that it avoids a catch-22:
    proxy url retreival require a proxy url! Fixes a long long standing bug.
    
    BUG: 123356
    FIXED-IN: 4.6.3

commit 0f7cf4634300a9d5af3c1498a3efd6320fd590fb
Author: Stephen Kelly <steveire@gmail.com>
Date:   Tue Apr 26 01:22:30 2011 +0200

    While processing changes in the source, don't process selections.
    
    Because the proxy hadn't processed the rowsInserted or rowsRemoved
    signal yet, it was creating corrupt internal data.
    
    Possible crash fix. Please retest.
    
    BUG: 253108

commit 1317e6e22057d6080ab4279cfd50330cdd43b522
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Mon Apr 25 11:48:18 2011 -0400

    Fixed the revert made by commit 870e619624851dc892cac234ac0c91cccbc00ae9:
      - kill() should be performed on m_slaveOnHold and not the null slave variable.
      - Reset the variables that contain the slave-on-hold information once it has
        been used or discarded.

commit 06d0f719e8d7d270317ce06e368c9818f0b718a4
Author: Dominik Haumann <dhdev@gmx.de>
Date:   Mon Apr 25 15:38:23 2011 +0200

    clear layout cache after document close
    
    CCBUG: 271250

commit c5712ce0e3661293d05e87baf60626dedb63e3b9
Author: Michel Ludwig <michel.ludwig@kdemail.net>
Date:   Mon Apr 25 13:25:13 2011 +0200

    Add support for IEEEeqnarrary(box) and other amsmath matrix environments.
    
    Backport of Kate commit b1936990a96d8f48072e19bc6000c17c5cf77e87.

commit 44873e1b04ad8a1d23d1dc442cbb29ee685756b2
Author: Andre Woebbeking <Woebbeking@kde.org>
Date:   Mon Apr 25 10:52:06 2011 +0200

    Don't crash in heldSlaveForJob(), slave can be 0.
    
    CCMAIL:adawit@kde.org

commit 3da71bac6705e796f15a40de1f4734e4090234b6
Author: Jacopo De Simoi <wilderkde@gmail.com>
Date:   Sun Apr 24 18:31:45 2011 -0400

    Check if the crypto device still needs to be locked

commit 41daae1f17d42045e793ddd12c27aa1f3db38d3a
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Apr 24 14:08:33 2011 -0400

    Make it compile

commit 5bc40921e72dd26e2228145b03591a9feb675666
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Apr 24 13:41:52 2011 -0400

    Implemented the required copy constructor and assignment operator for ExtraFields since it used
    in a Qt container (QMap).
    
    CCBUG: 256712
    CCBUG: 198396

commit 4a346543ebe073a11a3d2ef57927fb4795b01474
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Apr 24 13:58:38 2011 -0400

    Revert myself yet again: m_kioJob is a QPointer, no need to set it to NULL in putOnHold.

commit 8d58ddf37cf61742fbe14989a87d0771d7141a83
Author: David Faure <faure@kde.org>
Date:   Sun Apr 24 19:48:41 2011 +0200

    fix compilation with strict iterators

commit 870e619624851dc892cac234ac0c91cccbc00ae9
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Apr 24 04:52:12 2011 -0400

    Reverted most of the put on hold related changes, commit abc9b6de33ced386c15deeb6d0245d428ff278f,
    except for the flip of the two if statements in Scheduler::heldSlaveForJob.

commit 3d3cc32d906177229af45bfb4f2ed4a6b415ba87
Author: Rafael Fernández López <ereslibre@ereslibre.es>
Date:   Sun Apr 24 00:00:45 2011 +0200

    Correctly set right margin, since viewportWidth removes too many space, and when starting from the right
    we need to remove only the left part of the margin, so the viewportWidth is balanced.

commit 4d693434978659a7aec3e0fffc0c2dc6a6a306ea
Author: Rafael Fernández López <ereslibre@ereslibre.es>
Date:   Sat Apr 23 22:15:46 2011 +0200

    compile++

commit bea45a78c40368a333197d13e443671c23e45505
Author: Rafael Fernández López <ereslibre@ereslibre.es>
Date:   Sat Apr 23 20:40:18 2011 +0200

    Take into account RightToLeft environments, and adapt two things:
    * Visual rect for items, that need to start from the very right and flow to the left.
    * Binary search to find items fast. The search needed to be adapted to reflect this new case.
    
    Following changes on drawers that need to be adapted on this kind of environments.
    
    CCBUG: 238508

commit 46194b596212449b1b3beb26e8ece68fb8ac7a15
Author: Jacopo De Simoi <wilderkde@gmail.com>
Date:   Sat Apr 23 12:59:03 2011 -0400

    Don't broadcast teardownDone until it's really done
    
    this is a companion commit to bdf3cc8ac328e907e88de36d698bba6a7f0e2c7f
    which did the same with setup;

commit 3b563e079e37612c527806ef60f5e0f687fd6238
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Fri Apr 22 19:00:12 2011 -0400

    Fixed decompression of HTTP documents that were compressed using "deflate-http".

commit fbd20b80b9ea391738415919a24d7d2833332cc8
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Apr 22 16:35:15 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 36a8e02457e554db188d0d83e8ad24cfc9a0dc86
Author: Nicolas Lécureuil <neoclust.kde@free.fr>
Date:   Fri Apr 22 10:50:09 2011 +0200

    Fix crash
    CCBUG:234564

commit 98a285a65665d42f45e9460f5b150cc84c0e9b86
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue Apr 19 00:43:30 2011 -0400

    Do not automatically preserve username information on redirection
    
    REVIEW:101153

commit 6315b732cd8c775892d442c5709349e0f66d1f12
Author: Ozan Çağlayan <ozan@pardus.org.tr>
Date:   Thu Apr 21 10:33:46 2011 +0300

    Really wait until the slave filesystem is really mounted
    
    Backport:
    When you first attempt to mount an encrypted container and provide
    the passphrase, udisks doesn't correctly wait for mount completion.
    
    Fix this so that dolphin points the mounted slave filesystem instead
    of the Home directory.
    
    REVIEW:101160
    BUG:271156
    FIXED-IN:4.6.3
    (cherry picked from commit bdf3cc8ac328e907e88de36d698bba6a7f0e2c7f)

commit 8954ca32a46ceb03d81136cc4b17873865fe82d9
Author: Jeremy Whiting <jpwhiting@kde.org>
Date:   Wed Apr 20 22:15:34 2011 -0600

    Attempt to remove installed files during uninstall if they exist or are
    sym links.
    Fixes bug where symlinks to removed files are not deleted.
    (cherry picked from commit 07bbb143ff8847907d4f2e65fe12d1c905a4122b)

commit eac6ba46194a7f06555a81b7087363beb3a97a83
Author: Marco Martin <notmart@gmail.com>
Date:   Wed Apr 20 20:34:52 2011 +0200

    use Item width and height as minimumSize
    
    massively not optimal. however qml doesn't have concept of size hints unfortunately

commit a4abf22c79b517e989a50a21c5a80e72714aad53
Author: Marco Martin <notmart@gmail.com>
Date:   Wed Apr 20 18:10:02 2011 +0200

    fix popup position
    
    backport:
    if item is in an hidden Dialog, try to find the parent applet and use it instead of item
    this fixes the device notifier popup placement

commit 7e24c016c9716843b017560e2fca830ecd6f85bc
Author: Christoph Feck <christoph@maxiom.de>
Date:   Wed Apr 20 16:58:38 2011 +0200

    Fix month names appearing multiple times
    
    Patch by Sergey Ivanov, thanks!
    BUG: 271347
    FIXED-IN: 4.6.3
    (cherry picked from commit 9f7171aa55664bcdd0e03d5a39d8d5491ba607c8)

commit 041df3a9de29663407c7c4c2d1b5c49a1124c7eb
Author: Ludovic Grossard <grossard@kde.org>
Date:   Tue Apr 19 21:54:17 2011 +0200

    added a french translator

commit ff96d8cbba80300d3d4f75bd89f15872dfb59d5e
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Mon Apr 18 15:41:38 2011 -0400

    readTimeout is in secs. Convert to msecs before use

commit 9d4edae7f953574ede09aeb6cf91fed8a897d1ed
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Apr 17 22:55:26 2011 -0400

    Publishing ioslaves that are on hold is now automatically handled when calling
    KIO::Integration::AccessManager::putReplyOnHold.

commit 003d0fdf4a8a9744bb0b02e6b1f7acddb1afa5f9
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Apr 17 21:44:13 2011 -0400

    - Automatically call KIO::Scheduler::publishSlaveOnHold whenever putOnHold
      is called.
    
    - Since the job is useless without its ioslave, emit the finished signal when
      putOnHold is called.
    
    - Minor code clean up.

commit 5c65f468d016961e7e0bf877e56d475bc302f552
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Apr 17 21:23:27 2011 -0400

    Mostly revert the changes I made with commit d27c820581673040e80c46087b34b76e9fc9ed79.
    
    I completely misunderstood the main concept behind put slave on hold. With the exception
    of corner-case that can only be solved at the application level, putting slaves on hold
    should work correctly once this partial revert is commited.
    
    CCBUG:123121
    CCBUG:148307
    REVIEW:6271

commit a3297d274843c22ee8f5c4ede64f9c62311ade37
Author: Volker Krause <vkrause@kde.org>
Date:   Sun Apr 17 13:17:13 2011 +0200

    Consider data: URLs local in KIO::AccessManager.
    
    Currently KIO::AccessManager blocks retrieval of embedded data: URLs if
    external references are disabled. This does not match the behavior of KHTML
    and breaks for example the display of sender photos/logos in KMail.
    
    REVIEW: 101140

commit 37b27f7f4dcc2f4d46e46808acff1396027f1331
Author: Dominik Haumann <dhdev@gmx.de>
Date:   Sat Apr 16 13:35:36 2011 +0200

    backport hl updates

commit 89e6eb60dc9a66b317a893bf6790febd50fb8810
Author: Dominik Haumann <dhdev@gmx.de>
Date:   Sat Apr 16 13:33:49 2011 +0200

    do not make matches bold, as non-fixed font jumps around
    
    CCBUG: 270861

commit 1855d5963c4fdbc96456dc7ce661b5bccc795472
Author: Alex Fiestas <afiestas@kde.org>
Date:   Fri Apr 15 17:04:33 2011 +0200

    Add KAuthorize hooks into KFileItemAction openwith methods
    
    REVIEW:101082

commit 4441b6a9961482c91f6a67620a32120c99778291
Author: Alex Fiestas <afiestas@kde.org>
Date:   Mon Apr 11 15:06:27 2011 +0200

    Add KAuthorize containment_context_menu into createToolBox
    
    Having the posibility of avoid the context_menu while we're
    allowing the cashew has no sense. So let's use the same action
    to lockdown both.

commit a1d3e6924c1473d94cb365e05e154e10fa649f0c
Author: Patrick von Reth <patrick.vonreth@gmail.com>
Date:   Fri Jan 28 11:35:14 2011 +0000

    fixed mingw debug build
    
    svn path=/trunk/KDE/kdelibs/; revision=1217740
    (cherry picked from commit 551c1ece9f6fff5e2efa91fe4a56c640d65f67d6)

commit 5249326cb7b098df2f65331fbabd9f31f0ac1220
Merge: 72cbad1 a84566a
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Fri Apr 15 01:55:51 2011 -0300

    Merge branch 'KDE/4.6' of git://anongit.kde.org/kdelibs into KDE/4.6

commit a84566af139b3606baaaa38fa3afe23c615b2615
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Apr 13 15:32:20 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit d65c7a8454ce9ab99b06c57dc2b63a0acfdc75a9
Author: Christophe Giboudeaux <cgiboudeaux@gmx.com>
Date:   Tue Apr 12 13:41:44 2011 +0200

    Find Hunspell 1.3
    (cherry picked from commit a519a5ba326635c889f64ea7aeb6d7e7251e6667)

commit 72cbad117d0abb4b47a5d00754ea465eda6c06b5
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sun Apr 10 18:34:17 2011 -0300

    Fix Solid::Networking::status() returning Solid::Networking::Unknown
    sometimes.
    
    BUG: 270538
    FIXED-IN: 4.6.3

commit 5df712e88439ac304ecb49618e263bdc00d7609d
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sat Apr 9 16:32:28 2011 -0400

    Properly handle ERR_IS_DIRECTORY. Fixes the bug where clicking on an FTP
    link on a web page, e.g. ftp://ftp.kde.org/pub, does not show the contents
    of the FTP directory.

commit 95fb8be6a9265f7b0c7edc7045f95f0f03e8ed55
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sat Apr 9 20:07:00 2011 +0200

    similar work around in mousePressEvent as we have in contextMenuEvent

commit 9e46e4f0f65b90a409d409fa6e7e6a0f97b8b4c6
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sat Apr 9 00:08:16 2011 +0200

    tage the transform into consideration

commit 5fc269a124fee0b8fc385f3a0a54f84da6c13522
Author: Marco Martin <notmart@gmail.com>
Date:   Fri Apr 8 19:03:36 2011 +0200

    initialize scrollbarVisible
    
    BUG:270387

commit e62e101ee05f630b4b6f6705c756dbb1bbb8a05f
Author: Albert Astals Cid <aacid@kde.org>
Date:   Fri Apr 8 17:55:37 2011 +0100

    Initialize preShowStatus
    
    acked by Marco

commit 759b8e173de490ca05fc5d165538e73d80cab79d
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Fri Apr 8 12:08:11 2011 -0400

    Make sure HTTP headers are set even when the ioslave does not send content-type information.
    This fixes the issue where kdewebkit sometimes does not seem to finish loading a page.

commit b3968e2c370bcc5347d83aa7fc16f806e58617b0
Author: Sebastian Trueg <trueg@kde.org>
Date:   Thu Apr 7 09:51:29 2011 +0200

    Gracefully handle an invalid manager parameter.
    
    Instead of crashing when given a 0 manager parameter use the default
    global manager in the constructors of Nepomuk::Resource.

commit 3c5a9fcc23c004fb2fc1ec9abd61fcdb593e007a
Author: Jeremy Whiting <jpwhiting@kde.org>
Date:   Thu Apr 7 09:09:17 2011 -0600

    Call createNewApplicationPalette from createApplicationPalette.
    Add @since to apidox.

commit d6f5914e7928c11430cac3f95515ba6a6d42a3ba
Author: Jeremy Whiting <jpwhiting@kde.org>
Date:   Thu Apr 7 07:08:23 2011 -0600

    Added KGlobalSettings::createNewApplicationPalette that doesn't cache.

commit 0f7b8c4551357d143ad59a02f033287bc07f4942
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Wed Apr 6 10:04:22 2011 -0400

    Cosmetic change: removed a comment that is no longer valid.

commit 78678e3b0ba2ea7124f3a9347632ea3bd6a786e4
Author: Sebastian Trueg <trueg@kde.org>
Date:   Wed Apr 6 15:11:29 2011 +0200

    Only replace slashes in query titles with the fraction slash.
    
    This is what KIO::encodeFileName does. But since we do not want to link
    to KIO we copy the code.
    This results in nicer query titles without any percent-encoding.

commit 8375433c1875471ed0d04b3e2026fe7f6795fe6c
Author: Sebastian Trueg <trueg@kde.org>
Date:   Wed Apr 6 14:51:35 2011 +0200

    Percent-encode the query title.
    
    This fixes the problem where clicking on a property value in Dolphin's
    information panel throws an "invalid protocol" error.

commit 58d3f14a3f8825d7a7ec9b966bd68d883ddee95c
Author: Andre Woebbeking <Woebbeking@kde.org>
Date:   Wed Apr 6 00:06:16 2011 +0200

    compile

commit ba22a1d02382503ca95832a149155e83632c36f6
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue Apr 5 17:46:58 2011 -0400

    Do not trim whitespaces from filenames.
    
    BUG:88575
    REVIEW:100943
    FIXED-IN:4.6.2

commit a4f5b32e1efa097e7816819f50400498cb7e6903
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue Apr 5 16:20:47 2011 -0400

    Correctly recover mimetype information from a KIO::Stat job

commit d3c779920ddbf514e220b6e2e8e1d169d76de1e8
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Mon Apr 4 23:02:39 2011 -0300

    Expands %i to --icon <icon name> instead of -icon <icon name> in
    .desktop files.
    
    BUG: 270107
    FIXED-IN: 4.6.3
