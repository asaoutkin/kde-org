------------------------------------------------------------------------
r1168107 | dakon | 2010-08-26 07:19:11 +0100 (dj, 26 ago 2010) | 1 line

bump KGpg version number for release
------------------------------------------------------------------------
r1168054 | kossebau | 2010-08-26 01:04:29 +0100 (dj, 26 ago 2010) | 2 lines

backport of 1168053: changed: update magic windowstate config string to new tools

------------------------------------------------------------------------
r1167879 | lueck | 2010-08-25 16:21:58 +0100 (dc, 25 ago 2010) | 1 line

backport from trunk: fix X-DocPath and install dir to match autogen.sh's CMakeLists.txt
------------------------------------------------------------------------
r1167427 | teran | 2010-08-24 16:32:36 +0100 (dt, 24 ago 2010) | 5 lines

fix for erroneous use of delete on a pointer which was new[]'d.

backport of commit #1167426.


------------------------------------------------------------------------
r1167424 | teran | 2010-08-24 16:19:17 +0100 (dt, 24 ago 2010) | 7 lines

fix for bug #247931. Apparently the mpz_tdiv_q_2exp function rounds and doesn't just truncate. Since there doesn't seem to
be a function which does this and truncates, we instead use the versions which round up or down depending on if the 
value to shift is positive or negative, appears to yield the expected results now.

BUG:24793


------------------------------------------------------------------------
r1167251 | mmrozowski | 2010-08-24 07:37:33 +0100 (dt, 24 ago 2010) | 1 line

[CMake] Fix broken logic in printer-applet (respect -DINSTALL_PRINTER_APPLET=TRUE)
------------------------------------------------------------------------
r1167162 | dakon | 2010-08-23 21:12:24 +0100 (dl, 23 ago 2010) | 6 lines

backport the fixes to allow KGpg working with keys stored on smartcards

backport of 1167158, 1167159 and 1167161

BUGS:139965,248598,248833

------------------------------------------------------------------------
r1166790 | rkcosta | 2010-08-22 23:28:12 +0100 (dg, 22 ago 2010) | 7 lines

Make sure our KProcess is terminated in failOperation().

If we don't, finished() may trigger some deletions and the remaining
KProcess events might not get delivered in time.

BUG: 248336
FIXED-IN: 4.5.1
------------------------------------------------------------------------
r1166789 | rkcosta | 2010-08-22 23:28:08 +0100 (dg, 22 ago 2010) | 3 lines

Const'ify the QString here.

SVN_SILENT
------------------------------------------------------------------------
r1166771 | rkcosta | 2010-08-22 22:29:21 +0100 (dg, 22 ago 2010) | 5 lines

Use QLatin1String for the constraints, as they're only ASCII text.

Backport of r1166770.

SVN_SILENT
------------------------------------------------------------------------
r1166664 | kudryashov | 2010-08-22 13:04:50 +0100 (dg, 22 ago 2010) | 3 lines

Display feature log if dependency is found

Also make INSTALL_PRINTER_APPLET visible to cmake gui.
------------------------------------------------------------------------
r1166589 | dakon | 2010-08-22 09:31:03 +0100 (dg, 22 ago 2010) | 5 lines

fix another crash because of calling functions of an object in deletion when refreshing keys

BUG:248604
FIXEDIN:4.5.1

------------------------------------------------------------------------
r1166479 | rkcosta | 2010-08-22 00:26:44 +0100 (dg, 22 ago 2010) | 7 lines

Do not always check if we have a valid subfolder value.

It only makes sense to check for that if extraction into a subfolder has
been enabled.

BUG: 248638
FIXED-IN: 4.5.1
------------------------------------------------------------------------
r1165999 | teran | 2010-08-20 15:31:10 +0100 (dv, 20 ago 2010) | 5 lines

backport of commit #1165987

fixes pasting of values with the MSB set in non-decimal modes


------------------------------------------------------------------------
r1165710 | scripty | 2010-08-20 03:33:20 +0100 (dv, 20 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1164871 | dakon | 2010-08-17 21:21:42 +0100 (dt, 17 ago 2010) | 7 lines

gpg should not try to open a terminal

CCBUG:248161
FIXEDIN:4.5.1

backport of r1164870

------------------------------------------------------------------------
r1164369 | rkcosta | 2010-08-16 17:18:18 +0100 (dl, 16 ago 2010) | 12 lines

Support different 7z archive delimiters.

For some weird reason, 7z has decided to change the archive
delimiter from "----" to "--" from version 9.04 to 9.13.

We now detect both correctly.

Backport of r1164368.

BUG: 247628
FIXED-IN: 4.5.1

------------------------------------------------------------------------
r1164332 | mmrozowski | 2010-08-16 15:54:55 +0100 (dl, 16 ago 2010) | 1 line

Add macro_log_feature for find_package(KDEWorkspace) - patch by Yury G. Kudryashov
------------------------------------------------------------------------
r1163743 | rkcosta | 2010-08-15 00:46:37 +0100 (dg, 15 ago 2010) | 6 lines

Fix build.

Some old variable declarations weren't removed after some commits were
merged.

Backport of r1155797.
------------------------------------------------------------------------
r1163742 | rkcosta | 2010-08-15 00:46:33 +0100 (dg, 15 ago 2010) | 3 lines

Make emitEntryFromArchive() more readable, const'ify where possible.

Backport of r1155724.
------------------------------------------------------------------------
r1163740 | rkcosta | 2010-08-15 00:46:29 +0100 (dg, 15 ago 2010) | 8 lines

Stop leaking archive_entry pointers.

 * In addFiles(), we don't need to manually create a new archive_entry,
   as it can be created and properly freed inside writeFile().
 * In deleteFiles(), there is no need to manually allocate memory for an
   archive_entry either.

Backport of r1155721.
------------------------------------------------------------------------
r1163739 | rkcosta | 2010-08-15 00:46:26 +0100 (dg, 15 ago 2010) | 3 lines

Always check the return of archive_read_support_{compression,format}_all

Backport of r1155720.
------------------------------------------------------------------------
r1163738 | rkcosta | 2010-08-15 00:46:22 +0100 (dg, 15 ago 2010) | 6 lines

Wrap our archive* pointers with QScopedPointer.

This helps us clean up the code, as we don't need to keep track of the
exact places we need to free our pointers to avoid leaks.

Backport of r1155718.
------------------------------------------------------------------------
r1163737 | rkcosta | 2010-08-15 00:46:18 +0100 (dg, 15 ago 2010) | 3 lines

Simplify rootNode creation by passing a default value to QHash::value.

Backport of r1155717.
------------------------------------------------------------------------
r1163732 | rkcosta | 2010-08-15 00:22:41 +0100 (dg, 15 ago 2010) | 5 lines

Give a parent to JobTracker.

Backport of r1163721.

CCBUG: 237728
------------------------------------------------------------------------
r1163731 | rkcosta | 2010-08-15 00:22:37 +0100 (dg, 15 ago 2010) | 12 lines

Remove the finished() slot reimplementation.

It called unregisterJob() and did many things it also did.
unregisterJob() calls resetUi(), finished() calls it after 1.5 seconds.

Since the default registerJob() implementation already connects a KJob's
finished() signal to the unregisterJob() slot, there's little sense in
keeping this duplicated code.

Backport of r1163720.

CCBUG: 237728
------------------------------------------------------------------------
r1163730 | rkcosta | 2010-08-15 00:22:33 +0100 (dg, 15 ago 2010) | 3 lines

Call the parent unregisterJob() implementation before anything else.

Backport of r1163719.
------------------------------------------------------------------------
r1163729 | rkcosta | 2010-08-15 00:22:30 +0100 (dg, 15 ago 2010) | 3 lines

Make JobTracker's height smaller.

Backport of r1163718.
------------------------------------------------------------------------
r1163728 | rkcosta | 2010-08-15 00:22:26 +0100 (dg, 15 ago 2010) | 5 lines

Remove the currentJob() method from JobTracker.

It was not being used anywhere, so we're better off without it.

Backport of r1163717.
------------------------------------------------------------------------
r1163727 | rkcosta | 2010-08-15 00:22:22 +0100 (dg, 15 ago 2010) | 3 lines

Remove unused include.

Backport of r1163716.
------------------------------------------------------------------------
r1163726 | rkcosta | 2010-08-15 00:22:18 +0100 (dg, 15 ago 2010) | 3 lines

Forward declare KJob.

Backport of r1163715.
------------------------------------------------------------------------
r1163725 | rkcosta | 2010-08-15 00:22:14 +0100 (dg, 15 ago 2010) | 3 lines

Hint that widget() is virtual.

Backport of r1163714.
------------------------------------------------------------------------
r1163724 | rkcosta | 2010-08-15 00:22:10 +0100 (dg, 15 ago 2010) | 3 lines

registerJob and unregisterJob are slots, declare them as such.

Backport of r1163713.
------------------------------------------------------------------------
r1163723 | rkcosta | 2010-08-15 00:22:06 +0100 (dg, 15 ago 2010) | 5 lines

Remove dead line.

Backport of r1163712.

SVN_SILENT
------------------------------------------------------------------------
r1163722 | rkcosta | 2010-08-15 00:21:39 +0100 (dg, 15 ago 2010) | 3 lines

Explicitly link to KDE4_KDEUI_LIBS.

Backport of r1163711.
------------------------------------------------------------------------
r1163683 | kossebau | 2010-08-14 20:53:45 +0100 (ds, 14 ago 2010) | 2 lines

backport of 1163675: fixed: kdelirc is now kremotecontrol

------------------------------------------------------------------------
r1163682 | kossebau | 2010-08-14 20:47:36 +0100 (ds, 14 ago 2010) | 2 lines

backport of 1163680: fixed: homepage is now kremotecontrol, no longer kdelirc

------------------------------------------------------------------------
r1163663 | kossebau | 2010-08-14 19:39:00 +0100 (ds, 14 ago 2010) | 2 lines

backport of 1163660: fixed: kdelirc is now kremotecontrol

------------------------------------------------------------------------
r1162263 | kossebau | 2010-08-11 19:54:14 +0100 (dc, 11 ago 2010) | 1 line

update version number
------------------------------------------------------------------------
r1161792 | kossebau | 2010-08-10 21:33:37 +0100 (dt, 10 ago 2010) | 2 lines

backport of 1161791: fixed: disable SetBytesPerGroup action if there is no view, crashed otherwise if selected

------------------------------------------------------------------------
r1160404 | scripty | 2010-08-08 03:09:30 +0100 (dg, 08 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1160302 | dakon | 2010-08-07 18:44:39 +0100 (ds, 07 ago 2010) | 7 lines

fix setting owner trust to ultimate

BUG:244288
FIXEDIN:4.5.1

forward-port of r1160301

------------------------------------------------------------------------
