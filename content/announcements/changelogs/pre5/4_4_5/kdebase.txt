------------------------------------------------------------------------
r1131785 | scripty | 2010-05-29 14:00:48 +1200 (Sat, 29 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1132175 | scripty | 2010-05-30 14:19:05 +1200 (Sun, 30 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1132414 | hpereiradacosta | 2010-05-31 04:15:33 +1200 (Mon, 31 May 2010) | 4 lines

Backport r1132411
Do not trigger widget grab on paintEvent
CCBUG: 220621

------------------------------------------------------------------------
r1132543 | orlovich | 2010-05-31 11:58:06 +1200 (Mon, 31 May 2010) | 7 lines

Make the stupid slow spinny blocking startup of nspluginviewer less stupid,
(and hence significantly less slow) by not relying on a broken #ifdef which is 
pointless anyway since kdefakes provides the needed function.

The particular #ifdef also needs to be cleaned up in other spots..
todolisted since I'll need an outside review for that.

------------------------------------------------------------------------
r1132570 | scripty | 2010-05-31 14:07:56 +1200 (Mon, 31 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1133000 | fredrik | 2010-06-01 09:12:52 +1200 (Tue, 01 Jun 2010) | 5 lines

Backport r1132999:

Really fix the crash when changing preview settings and the applet
is on the panel.

------------------------------------------------------------------------
r1133012 | nlecureuil | 2010-06-01 10:04:01 +1200 (Tue, 01 Jun 2010) | 4 lines

Backport of commit 1122490
FIXED-IN: 4.4.5
BUG: 240032

------------------------------------------------------------------------
r1133014 | hindenburg | 2010-06-01 10:23:59 +1200 (Tue, 01 Jun 2010) | 6 lines

Refresh the screen after the user manually drag-n-drops a tab.

I will forward port to 4.5.1 when that branch is open.

CCBUG: 164099

------------------------------------------------------------------------
r1133020 | hindenburg | 2010-06-01 10:38:21 +1200 (Tue, 01 Jun 2010) | 4 lines

Fix issue where the tab bar reappears after a split-view.

CCBUG: 176260

------------------------------------------------------------------------
r1133024 | hindenburg | 2010-06-01 10:44:16 +1200 (Tue, 01 Jun 2010) | 4 lines

Fix issue where the menu status for View menubar is not correct.

CCBUG: 181345

------------------------------------------------------------------------
r1133025 | hindenburg | 2010-06-01 10:44:51 +1200 (Tue, 01 Jun 2010) | 1 line

update version
------------------------------------------------------------------------
r1133027 | hindenburg | 2010-06-01 10:53:25 +1200 (Tue, 01 Jun 2010) | 4 lines

Remove the shortcut to send all output to current windows.  This might catch people off guard if they accidently hit it.  People who want it can just re-assign it back.

CCBUG: 238373

------------------------------------------------------------------------
r1133073 | scripty | 2010-06-01 14:44:22 +1200 (Tue, 01 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1133542 | nlecureuil | 2010-06-02 10:53:37 +1200 (Wed, 02 Jun 2010) | 4 lines

Fix crash when moving widget from panel to desktop
FIXED-IN: 4.4.5
BUG:240399

------------------------------------------------------------------------
r1133564 | scripty | 2010-06-02 13:59:26 +1200 (Wed, 02 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1133843 | trueg | 2010-06-03 04:35:29 +1200 (Thu, 03 Jun 2010) | 1 line

Backport: ref() and unref() were actually replaced. Fixed that
------------------------------------------------------------------------
r1133984 | scripty | 2010-06-03 14:06:45 +1200 (Thu, 03 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1134074 | nlecureuil | 2010-06-03 22:54:49 +1200 (Thu, 03 Jun 2010) | 3 lines

Fix crash in kwin
BUG: 240464

------------------------------------------------------------------------
r1134110 | graesslin | 2010-06-04 00:52:04 +1200 (Fri, 04 Jun 2010) | 5 lines

Fix crash when trying to group a window without decorations.
BUG: 222816
FIXED-IN: 4.4.5


------------------------------------------------------------------------
r1134346 | scripty | 2010-06-04 14:19:42 +1200 (Fri, 04 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1134456 | nlecureuil | 2010-06-04 23:23:30 +1200 (Fri, 04 Jun 2010) | 4 lines

Make sure menu doesn't crash if the application where we right click have been deleted
BUG: 227601
FIXED-IN: 4.4.5

------------------------------------------------------------------------
r1134494 | hindenburg | 2010-06-05 00:58:51 +1200 (Sat, 05 Jun 2010) | 4 lines

Save character encoding to session management.

CCBUG: 221450

------------------------------------------------------------------------
r1134578 | hindenburg | 2010-06-05 04:27:57 +1200 (Sat, 05 Jun 2010) | 5 lines

Ignore Konsole's message "Undecodable sequence: \\001b(hex)[?1034h" that new ncurses/xterm has in their terminfo.
I believe 1034h deals with activating 8bitinput.

CCBUG: 183244

------------------------------------------------------------------------
r1134702 | scripty | 2010-06-05 14:01:39 +1200 (Sat, 05 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1134758 | freininghaus | 2010-06-05 19:47:28 +1200 (Sat, 05 Jun 2010) | 8 lines

Backport of commit 1128599 to the 4.4 branch:

Disable the automatic error handling in the DolphinDirLister, the error message
is shown in Dolphin instead.

CCBUG: 229505


------------------------------------------------------------------------
r1134825 | beschow | 2010-06-06 00:39:57 +1200 (Sun, 06 Jun 2010) | 1 line

header cleanup
------------------------------------------------------------------------
r1134826 | beschow | 2010-06-06 00:40:01 +1200 (Sun, 06 Jun 2010) | 1 line

backport: autocomplete directories
------------------------------------------------------------------------
r1134979 | nlecureuil | 2010-06-06 11:13:36 +1200 (Sun, 06 Jun 2010) | 4 lines

Backport commit 1134975
CCBUG: 232424
FIXED-IN: 4.4.5

------------------------------------------------------------------------
r1134995 | scripty | 2010-06-06 13:54:43 +1200 (Sun, 06 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1135159 | orlovich | 2010-06-07 03:53:47 +1200 (Mon, 07 Jun 2010) | 10 lines

Merged revision:r1135153 | orlovich | 2010-06-06 11:32:15 -0400 (Sun, 06 Jun 2010) | 9 lines

Fix enabling/disabling of web shortcut editting buttons when the item is selected
in double-click mode --- or via keyboard.

Based on analysis & patch by Gard Spreemann (user name gspreemann at the host name gmail, tld com);
I just used yet-another different signal from the suggestion.

BUG: 220077
BUG: 232855
------------------------------------------------------------------------
r1135229 | jlayt | 2010-06-07 09:38:09 +1200 (Mon, 07 Jun 2010) | 8 lines

Fix HU locale date/time formats per POSIX LC_TIME

Backport of revision 1135228

BUG: 229102



------------------------------------------------------------------------
r1135396 | nlecureuil | 2010-06-07 19:18:50 +1200 (Mon, 07 Jun 2010) | 3 lines

Backport commit 1122528
CCBUG: 235922

------------------------------------------------------------------------
r1135572 | cfeck | 2010-06-08 04:37:12 +1200 (Tue, 08 Jun 2010) | 5 lines

Fix browser service not reset to KIO for empty exec line (backport r1135569)

CCBUG: 240677
FIXED-IN: 4.4.5

------------------------------------------------------------------------
r1135708 | rkcosta | 2010-06-08 12:05:02 +1200 (Tue, 08 Jun 2010) | 8 lines

Backport r1135707.

Implement a dummy version of parse() for FreeBSD 8.0+ so that we don't have a missing symbol.

Additionally, implement a dummy version of parseSys(), even though it will never be called currently, and add some Q_UNUSED() calls to make -pedantic happier.

CCBUG: 240884

------------------------------------------------------------------------
r1135731 | scripty | 2010-06-08 14:32:42 +1200 (Tue, 08 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1135756 | nlecureuil | 2010-06-08 19:18:24 +1200 (Tue, 08 Jun 2010) | 3 lines

Remove unneeded deps at build time
BUG: 221501

------------------------------------------------------------------------
r1135984 | nlecureuil | 2010-06-09 03:22:32 +1200 (Wed, 09 Jun 2010) | 5 lines

Fix crash when moving widget from panel to desktop
BUG: 241099
FIXED-IN: 4.4.5


------------------------------------------------------------------------
r1136130 | scripty | 2010-06-09 14:04:00 +1200 (Wed, 09 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1136822 | graesslin | 2010-06-11 08:59:22 +1200 (Fri, 11 Jun 2010) | 5 lines

If presentwindows does not get activated, we have to revert the tabbox state or next activation is not possible.
BUG: 240730
FIXED-IN: 4.4.5


------------------------------------------------------------------------
r1136888 | scripty | 2010-06-11 14:10:03 +1200 (Fri, 11 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1137263 | graesslin | 2010-06-12 18:55:41 +1200 (Sat, 12 Jun 2010) | 6 lines

Make icon sizes 64x64 and 128x128 available in KWin and use it in TabBox for large icon modes.
So no more ugly upscaling.
BUG: 241384
FIXED-IN: 4.4.5


------------------------------------------------------------------------
r1137464 | scripty | 2010-06-13 14:00:33 +1200 (Sun, 13 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1137688 | scripty | 2010-06-14 14:05:06 +1200 (Mon, 14 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1137786 | pino | 2010-06-14 21:33:23 +1200 (Mon, 14 Jun 2010) | 4 lines

add the 'status' category at sizes 16, 22, 32, and 64

CCBUG: 241414

------------------------------------------------------------------------
r1137857 | schmidt-domine | 2010-06-15 01:55:05 +1200 (Tue, 15 Jun 2010) | 2 lines

Backported bugfix in servicemenuinstallation

------------------------------------------------------------------------
r1138032 | scripty | 2010-06-15 14:45:53 +1200 (Tue, 15 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1138422 | scripty | 2010-06-16 14:08:10 +1200 (Wed, 16 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1138867 | scripty | 2010-06-17 13:56:16 +1200 (Thu, 17 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1139147 | gladhorn | 2010-06-18 01:17:34 +1200 (Fri, 18 Jun 2010) | 4 lines

make kdebase-runtime compile with attica >= 0.1.4
patch by Thibaut Gridel <tgridel@free.fr>


------------------------------------------------------------------------
r1139149 | wstephens | 2010-06-18 01:18:59 +1200 (Fri, 18 Jun 2010) | 3 lines

Fix broken connection preventing backport from working.
CCBUG: 235066

------------------------------------------------------------------------
r1139154 | wstephens | 2010-06-18 01:31:16 +1200 (Fri, 18 Jun 2010) | 7 lines

Backport r1113069

"Approach adding the default [widgets to the system tray] in a different
way."

This way actually works.

------------------------------------------------------------------------
r1139205 | sandsmark | 2010-06-18 04:35:16 +1200 (Fri, 18 Jun 2010) | 5 lines

Remove annoying "Cannot load plugin" error message from the KDE Phonon Platform plugin

backport of commit 1139199 by thiago.


------------------------------------------------------------------------
r1140341 | graesslin | 2010-06-21 00:14:43 +1200 (Mon, 21 Jun 2010) | 6 lines

Ensure that a decoration is created before we try to shade the client. This fixes a crash when auto-grouping is enabled and a client should be added to a shaded group.
It still crashes when kwin is restarted with a shade group (same backtrace, but needs a different fix).
BUG: 242206
FIXED-IN: 4.4.5


------------------------------------------------------------------------
r1140344 | graesslin | 2010-06-21 00:23:04 +1200 (Mon, 21 Jun 2010) | 4 lines

Backport rev 1140343: When restarting kwin and there is a shaded group, set shade is called before a decoration is created. Catch this case, so that kwin doesn't crash.
CCBUG: 242206


------------------------------------------------------------------------
r1140377 | djarvie | 2010-06-21 02:22:45 +1200 (Mon, 21 Jun 2010) | 2 lines

Bug 224868: fix crash reading zone.tab if it is corrupted and very large.

------------------------------------------------------------------------
r1140446 | jacopods | 2010-06-21 05:59:18 +1200 (Mon, 21 Jun 2010) | 4 lines

backport fix for bogus free space value in some cases

CCBUG:223925

------------------------------------------------------------------------
r1140502 | jacopods | 2010-06-21 09:37:18 +1200 (Mon, 21 Jun 2010) | 3 lines

backport r1140495
CCBUG:228706

------------------------------------------------------------------------
r1140577 | scripty | 2010-06-21 14:09:36 +1200 (Mon, 21 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1140633 | jacopods | 2010-06-21 20:30:08 +1200 (Mon, 21 Jun 2010) | 4 lines

Revert last commit, I'll see if I can find a proper way to backport.

CCBUG:223925

------------------------------------------------------------------------
r1141033 | scripty | 2010-06-22 14:46:16 +1200 (Tue, 22 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141600 | scripty | 2010-06-23 14:07:08 +1200 (Wed, 23 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141968 | woebbe | 2010-06-24 10:10:22 +1200 (Thu, 24 Jun 2010) | 2 lines

-pedantic

------------------------------------------------------------------------
r1142451 | scripty | 2010-06-25 14:19:23 +1200 (Fri, 25 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1142808 | mueller | 2010-06-26 06:42:40 +1200 (Sat, 26 Jun 2010) | 2 lines

KDE 4.4.5

------------------------------------------------------------------------
