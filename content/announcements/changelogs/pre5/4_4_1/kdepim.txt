------------------------------------------------------------------------
r1085106 | scripty | 2010-02-04 11:21:53 +0000 (Thu, 04 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1085147 | mlaurent | 2010-02-04 12:56:17 +0000 (Thu, 04 Feb 2010) | 3 lines

Backport:
add icon from trunk

------------------------------------------------------------------------
r1085219 | tmcguire | 2010-02-04 17:00:33 +0000 (Thu, 04 Feb 2010) | 10 lines

Backport r1085218 by tmcguire from trunk to the 4.4 branch:

I don't understand why, but the CollectionDialog is disabled, even after enabling the CollectionRequester.

Work around this by not disabling CollectionRequester. There is a chance the collection fetch takes some time,
so there can now be a race between the resource and the user setting the collection.

CCBUG: 225496


------------------------------------------------------------------------
r1085426 | mlaurent | 2010-02-05 09:13:03 +0000 (Fri, 05 Feb 2010) | 2 lines

Fix kolab issue4059

------------------------------------------------------------------------
r1085511 | mlaurent | 2010-02-05 11:46:16 +0000 (Fri, 05 Feb 2010) | 2 lines

Backport fix enable/disable action

------------------------------------------------------------------------
r1085536 | mlaurent | 2010-02-05 12:39:22 +0000 (Fri, 05 Feb 2010) | 6 lines

Backport:
Fix a bug when we select an item after don't select an item
and reselect first item.

MERGE: e4/e5/3.5

------------------------------------------------------------------------
r1085537 | mlaurent | 2010-02-05 12:42:07 +0000 (Fri, 05 Feb 2010) | 2 lines

fix compile :) yes kde 4.5 is different from 4.4

------------------------------------------------------------------------
r1085552 | mlaurent | 2010-02-05 13:10:56 +0000 (Fri, 05 Feb 2010) | 2 lines

Fix crash when there is not selection

------------------------------------------------------------------------
r1085557 | mlaurent | 2010-02-05 13:20:19 +0000 (Fri, 05 Feb 2010) | 2 lines

Fix enable/disable remove button

------------------------------------------------------------------------
r1085667 | mlaurent | 2010-02-05 18:01:34 +0000 (Fri, 05 Feb 2010) | 2 lines

Fix caption

------------------------------------------------------------------------
r1085687 | mlaurent | 2010-02-05 18:44:27 +0000 (Fri, 05 Feb 2010) | 3 lines

Backport:
Be sure to set read only status

------------------------------------------------------------------------
r1086319 | osterfeld | 2010-02-06 21:30:56 +0000 (Sat, 06 Feb 2010) | 3 lines

don't crash when no selection model exists (as there is no feed
selected)
BUG:224555
------------------------------------------------------------------------
r1086320 | osterfeld | 2010-02-06 21:30:59 +0000 (Sat, 06 Feb 2010) | 1 line

prevent line-breaks in the article list
------------------------------------------------------------------------
r1086321 | osterfeld | 2010-02-06 21:31:02 +0000 (Sat, 06 Feb 2010) | 2 lines

strip HTML in titles, don't escape it.
BUG:211937
------------------------------------------------------------------------
r1086814 | djarvie | 2010-02-07 23:14:41 +0000 (Sun, 07 Feb 2010) | 1 line

--warnings
------------------------------------------------------------------------
r1087372 | mlaurent | 2010-02-08 22:06:41 +0000 (Mon, 08 Feb 2010) | 6 lines

Backport:
Fix bug #225934
add a clear button 
CCMAIL: 225934


------------------------------------------------------------------------
r1087718 | mlaurent | 2010-02-09 12:57:14 +0000 (Tue, 09 Feb 2010) | 4 lines

Backport:
fix i18n.
MERGE: e4/e5 I think

------------------------------------------------------------------------
r1087778 | dfaure | 2010-02-09 14:44:07 +0000 (Tue, 09 Feb 2010) | 4 lines

Fix the lack of akonadiserver startup when kontact is restored by session management,
and fix the lack of a mainwindow appearing at all in some other cases (kmailpart
waiting for akonadi forever), as discussed with Volker.

------------------------------------------------------------------------
r1087861 | lueck | 2010-02-09 17:27:01 +0000 (Tue, 09 Feb 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1087968 | mlaurent | 2010-02-09 22:18:39 +0000 (Tue, 09 Feb 2010) | 3 lines

Backport:
fix extract messages

------------------------------------------------------------------------
r1088299 | smartins | 2010-02-10 13:43:46 +0000 (Wed, 10 Feb 2010) | 8 lines

When deleting all filters, don't delete the one currently set in KCal::Calendar because, as said in kcal/calendar.h, the calendar 
takes ownership of it and will free it in the destructor.

BUG: 226095
BUG: 222683

MERGE: trunk probably, didn't try to reproduce the crash yet.

------------------------------------------------------------------------
r1088434 | smartins | 2010-02-10 20:25:04 +0000 (Wed, 10 Feb 2010) | 4 lines

Backport r1081140 by smartins from trunk to the 4.4 branch

"QuickAddTodo line" should respect the selected categories in the combo.

------------------------------------------------------------------------
r1088438 | smartins | 2010-02-10 20:44:26 +0000 (Wed, 10 Feb 2010) | 6 lines

Backport r1081122 by smartins from trunk to the 4.4 branch:

Unclutter to-do view by only showing the progress dialog on to-dos which use it.

http://reviewboard.kde.org/r/2731/

------------------------------------------------------------------------
r1088440 | smartins | 2010-02-10 20:51:38 +0000 (Wed, 10 Feb 2010) | 6 lines

Backport r1081695 by smartins from trunk to the 4.4 branch:

Don't flicker so much when closing the prefs dialog.

The view was being set twice.

------------------------------------------------------------------------
r1088443 | smartins | 2010-02-10 21:07:14 +0000 (Wed, 10 Feb 2010) | 4 lines

Backport r1082634 by smartins from trunk to the 4.4 branch:

Initialize mChanger pointer in the constuctor, otherwise we can sometimes get a segfault when doing inline editing.

------------------------------------------------------------------------
r1088449 | smartins | 2010-02-10 21:22:33 +0000 (Wed, 10 Feb 2010) | 4 lines

Backport r1084242 by smartins from trunk to the 4.4 branch:

Refactor agenda layout management, fixes a crash and some flickering issues.

------------------------------------------------------------------------
r1088483 | lueck | 2010-02-10 22:51:22 +0000 (Wed, 10 Feb 2010) | 1 line

update screenshot
------------------------------------------------------------------------
r1088489 | weilbach | 2010-02-10 23:37:56 +0000 (Wed, 10 Feb 2010) | 2 lines

Backport for 225124.

------------------------------------------------------------------------
r1088490 | weilbach | 2010-02-10 23:38:41 +0000 (Wed, 10 Feb 2010) | 2 lines

Backport cleanups.

------------------------------------------------------------------------
r1089560 | tstaerk | 2010-02-13 12:09:49 +0000 (Sat, 13 Feb 2010) | 4 lines

Increase performance for ical files with many todos.
Backporting 1086027.
CCBUGS:216854

------------------------------------------------------------------------
r1089589 | tstaerk | 2010-02-13 13:14:48 +0000 (Sat, 13 Feb 2010) | 3 lines

Increase version number.
CCBUGS:216854

------------------------------------------------------------------------
r1089591 | tstaerk | 2010-02-13 13:20:31 +0000 (Sat, 13 Feb 2010) | 4 lines

Add quotation marks around task names in csv export.
Backporting 1079158


------------------------------------------------------------------------
r1089830 | momeny | 2010-02-14 07:01:43 +0000 (Sun, 14 Feb 2010) | 3 lines

Backport of r1089826
A little fix around AddBlog wizard

------------------------------------------------------------------------
r1089844 | tstaerk | 2010-02-14 08:30:10 +0000 (Sun, 14 Feb 2010) | 4 lines

Recalculate total times and total session times after drag&drop.
Backporting 1076230 and 1076233.
CCBUGS:220059

------------------------------------------------------------------------
r1089864 | tstaerk | 2010-02-14 09:59:11 +0000 (Sun, 14 Feb 2010) | 4 lines

Do not add desktop 0 (first) as autotracking-desktop when creating a new
task with --addtask. Backport of 1089862
CCBUGS:225707

------------------------------------------------------------------------
r1090033 | mlaurent | 2010-02-14 14:14:30 +0000 (Sun, 14 Feb 2010) | 2 lines

const'ify

------------------------------------------------------------------------
r1090045 | mlaurent | 2010-02-14 14:38:36 +0000 (Sun, 14 Feb 2010) | 2 lines

Enabled/disabled buttons when we don't  select an item

------------------------------------------------------------------------
r1090091 | mlaurent | 2010-02-14 16:31:18 +0000 (Sun, 14 Feb 2010) | 3 lines

Fix disable item when we select a search folder.
Not necessary to merge in trunk

------------------------------------------------------------------------
r1090108 | mlaurent | 2010-02-14 17:09:20 +0000 (Sun, 14 Feb 2010) | 2 lines

Don't allow to rename a read only folder (bug found on my kdab account)

------------------------------------------------------------------------
r1090110 | mlaurent | 2010-02-14 17:19:26 +0000 (Sun, 14 Feb 2010) | 2 lines

Remove layout warning

------------------------------------------------------------------------
r1090342 | scripty | 2010-02-15 04:26:05 +0000 (Mon, 15 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1090597 | darioandres | 2010-02-15 15:49:00 +0000 (Mon, 15 Feb 2010) | 11 lines

- Backport to 4.4branch of:
SVN commit 1087128 by ervin:

Really no need to call disconnect() twice (as ImapResource does it on
error as well on ImapAccount when the connection is lost during the
capabilities check.

It will be fixed on KDE SC 4.4.1

CCBUG: 220384

------------------------------------------------------------------------
r1090813 | scripty | 2010-02-16 04:33:32 +0000 (Tue, 16 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1090851 | tokoe | 2010-02-16 08:44:29 +0000 (Tue, 16 Feb 2010) | 4 lines

Backport fix for #226562

CCMAIL:tsdgeos@terra.es

------------------------------------------------------------------------
r1090856 | mlaurent | 2010-02-16 09:23:03 +0000 (Tue, 16 Feb 2010) | 2 lines

Add shortcut to search text in readerwin same as in kmail

------------------------------------------------------------------------
r1090946 | mlaurent | 2010-02-16 11:45:31 +0000 (Tue, 16 Feb 2010) | 3 lines

Fix enable/disable combobox
MERGE: e4/e5/ 3.5 ?

------------------------------------------------------------------------
r1091593 | scripty | 2010-02-17 04:24:27 +0000 (Wed, 17 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1091742 | mlaurent | 2010-02-17 12:42:41 +0000 (Wed, 17 Feb 2010) | 4 lines

Fix bug #KONT-81
When we make a search and open search folder, and re make a search
we don't want to rename it and lose previous search

------------------------------------------------------------------------
r1091745 | mlaurent | 2010-02-17 12:53:30 +0000 (Wed, 17 Feb 2010) | 3 lines

Backport:
Add missing i18n

------------------------------------------------------------------------
r1091927 | mlaurent | 2010-02-17 22:09:13 +0000 (Wed, 17 Feb 2010) | 2 lines

Fix extract messages

------------------------------------------------------------------------
r1092431 | mlaurent | 2010-02-18 22:45:14 +0000 (Thu, 18 Feb 2010) | 2 lines

Fix caption.

------------------------------------------------------------------------
r1092497 | scripty | 2010-02-19 04:24:47 +0000 (Fri, 19 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1092550 | mlaurent | 2010-02-19 09:19:50 +0000 (Fri, 19 Feb 2010) | 3 lines

This function was never implemented
=> remove it from config

------------------------------------------------------------------------
r1092911 | tmcguire | 2010-02-19 18:04:45 +0000 (Fri, 19 Feb 2010) | 5 lines

Backport r1073089 by tmcguire from trunk to the 4.4 branch:

Speed up sending mails by querying contacts by nickname instead of getting all.


------------------------------------------------------------------------
r1093311 | djarvie | 2010-02-20 15:53:46 +0000 (Sat, 20 Feb 2010) | 2 lines

Better encapsulation

------------------------------------------------------------------------
r1093692 | schwarzer | 2010-02-21 11:37:57 +0000 (Sun, 21 Feb 2010) | 10 lines

fix xml markup

<nl> -> <nl/>
mistyped markup is shown in the UI message

This fuzzies 2 message for translators.

CCMAIL: kde-i18n-doc@kde.org


------------------------------------------------------------------------
r1093838 | djarvie | 2010-02-21 15:06:52 +0000 (Sun, 21 Feb 2010) | 3 lines

Disable calendars containing only the wrong alarm types for the calendar type.
This adds 5 new translatable strings which are needed to notify the user.

------------------------------------------------------------------------
r1093870 | tstaerk | 2010-02-21 16:44:43 +0000 (Sun, 21 Feb 2010) | 15 lines

Backporting 1093863

First find out what task to delete, then pop up windows asking the user.
Reason is: When a window pops up and ktimetracker has "Track active
applications" enabled, this window will create a new task, start it and
select it. So we need the right order: First remember what task is
selected - then pop up window asking for confirmation.

At the same time, I removed the code to create several tasks at once as
I can no longer select several task in ktimetracker's QTreeWidget. The
code is a lot clearer so.

CCBUGS:227746


------------------------------------------------------------------------
r1093871 | tstaerk | 2010-02-21 16:51:40 +0000 (Sun, 21 Feb 2010) | 8 lines

Backporting 1075317
Migration from X (XGetInPutFocUs) to KWindowSystem. No idea why the old
code stopped working. Purpose: Track time of tasks by title of focus
window.

CCBUGS:220160


------------------------------------------------------------------------
r1093903 | djarvie | 2010-02-21 18:00:43 +0000 (Sun, 21 Feb 2010) | 3 lines

Disable calendars containing only the wrong alarm types for the calendar type.
(Omitted from commit 1093838.)

------------------------------------------------------------------------
r1093923 | tmcguire | 2010-02-21 18:22:19 +0000 (Sun, 21 Feb 2010) | 10 lines

Don't remove the autosave file after recovering.
It will be properly cleaned up in cleanupAutoSave() anyway, which is only called
after successful sending or saving as draft.

This fixes mail loss when KMail crashes before the first autosave kicks in, after
recovering a message.

BUG: 159634
MERGE: 3.5, trunk

------------------------------------------------------------------------
r1093976 | tmcguire | 2010-02-21 20:23:02 +0000 (Sun, 21 Feb 2010) | 10 lines

Disable the message box about Nepomuk not working.

I get at least one of those on every KDE startup, which is very annoying.
Please fix the race condition of this error message, Akonadi should start
Nepomuk properly (see bug 224668)

BUG: 218206
BUG: 217111


------------------------------------------------------------------------
r1094069 | scripty | 2010-02-22 04:34:13 +0000 (Mon, 22 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1094300 | cgiboudeaux | 2010-02-22 13:43:24 +0000 (Mon, 22 Feb 2010) | 1 line

Prepare for 4.4.1
------------------------------------------------------------------------
r1094353 | tokoe | 2010-02-22 16:07:28 +0000 (Mon, 22 Feb 2010) | 2 lines

Backport bugfix #228064 (search bar does not work for contact groups)

------------------------------------------------------------------------
r1094501 | tmcguire | 2010-02-22 21:25:17 +0000 (Mon, 22 Feb 2010) | 8 lines

Remove the slow KabcBridge::categories().
Instead, populate the category list with Nepomuk tags. This should work, since all categories in contacts
get indexed as tags by the Nepomuk contact feeder.

MERGE: none (already in trunk)

BUG: 224037

------------------------------------------------------------------------
r1094835 | dfaure | 2010-02-23 10:56:03 +0000 (Tue, 23 Feb 2010) | 3 lines

Ensure the progress-widget overlay has a background rather than being transparent.
BUG: 228117

------------------------------------------------------------------------
r1095135 | tmcguire | 2010-02-23 18:35:51 +0000 (Tue, 23 Feb 2010) | 5 lines

Backport r1094866 by tmcguire from trunk to the 4.4 branch:

Fix this statement which was broken with one of the coding style / parenthesis fixes commit.


------------------------------------------------------------------------
r1095362 | scripty | 2010-02-24 04:27:20 +0000 (Wed, 24 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1096207 | mlaurent | 2010-02-26 08:56:09 +0000 (Fri, 26 Feb 2010) | 4 lines

Backport fix bug #228504
MERGE: e4/e5/ 3.5 ?
CCBUG: 228504

------------------------------------------------------------------------
r1096471 | dfaure | 2010-02-26 17:58:12 +0000 (Fri, 26 Feb 2010) | 2 lines

Fix porting bug which broke freebusy triggering: the default port is -1 in Qt4 (due to QUrl), not 0.

------------------------------------------------------------------------
