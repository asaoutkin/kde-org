---
aliases:
- /announcements/plasma-5.5.3-5.5.4-changelog
hidden: true
plasma: true
title: Plasma 5.5.4 Complete Changelog
type: fulllog
version: 5.5.4
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Check whether parent has altered background to decide of tabbar's background in document mode. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f422975c719f0a57ac6d28c28fbe39c2b8e80cee'>Commit.</a>
- - better handling of custom property for isMenuTitle. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f563c54fd47b8bdffeabc51a16bb1adbe3667b55'>Commit.</a>
- Moved all abstract scrollarea polishing to polishScrollArea. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=23c5f3b75edb2da22e60849df57950fc63b6a622'>Commit.</a>
- - Removed palette helper. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2cae9359188af85ac884e2709c17592bc67b0e73'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356561'>#356561</a>. Fixes bug <a href='https://bugs.kde.org/356343'>#356343</a>

### <a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a>

- Fix UI translation of libdiscover by fixing translation domain. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=155382e17488e83ac1cce070e863e4bb87ca0ce2'>Commit.</a>
- Prevent misleading notification. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=2ccd19a67fbd716093b31cf60bcd7c5c3643c960'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357613'>#357613</a>
- Fix crash. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=811cca260f389271c330be43e068aa70f7165ba7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357791'>#357791</a>

### <a name='khelpcenter' href='http://quickgit.kde.org/?p=khelpcenter.git'>KHelpCenter</a>

- Use KLocalizedString::setApplicationDomain. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=51166d2bb70332ce5a6d48bbd70c3ca368322085'>Commit.</a>

### <a name='kmenuedit' href='http://quickgit.kde.org/?p=kmenuedit.git'>KMenuEdit</a>

- Make the translators tab of the About dialog appear. <a href='http://quickgit.kde.org/?p=kmenuedit.git&amp;a=commit&amp;h=b53c0997f10c74f79f76a951da27f164f0717ea8'>Commit.</a>

### <a name='ksysguard' href='http://quickgit.kde.org/?p=ksysguard.git'>KSysGuard</a>

- Make the translators tab of the About dialog appear. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=104d86bc7faea83ed0c12411ab2d45bcf825d04f'>Commit.</a>

### <a name='kwayland' href='http://quickgit.kde.org/?p=kwayland.git'>KWayland</a>

- Update the version number of org_kde_plasma_surface to 2. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=53c371fb8f049e51dde74b346af8e4f070d1a284'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126819'>#126819</a>. Fixes bug <a href='https://bugs.kde.org/358136'>#358136</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Use new Qt flag to disable high DPI scaling on X. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=90112ef0cd6ed33f28b657f59d1cd3196077e955'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357896'>#357896</a>. Code review <a href='https://git.reviewboard.kde.org/r/126810'>#126810</a>
- Skip SWAP_BEHAVIOR_PRESERVED for supportsBufferAge. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e641022bf9482a11209577b5654cd43231be0755'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356992'>#356992</a>
- [backends/drm] Set mode when changing from/to a gbm buffer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a18177cc24d20bca02f60a95b67f2dcbd1ee8afc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357543'>#357543</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Cancel window highlight when opening the context menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5e95ef677ea5dd09137dcf7b88847b436ad572b3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353096'>#353096</a>
- Fix font preview colors. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9977edac12d6bec56f8d9cf7f97529177ca842eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/336089'>#336089</a>. Code review <a href='https://git.reviewboard.kde.org/r/126713'>#126713</a>
- Fix shrinking panel on top/left/right edge. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2f07fdd5d34673f856b8f8401ab8447000254aaa'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126743'>#126743</a>. Fixes bug <a href='https://bugs.kde.org/357835'>#357835</a>
- Resolve focus fighting between search field and grid. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8d534f94a6de944dd2f99a1b0f0026933f56c5bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357821'>#357821</a>
- [Task Manager] Don't show on which virtual desktop a window is on if there is just one. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f1ac92c5ee6de29721753d4fdd67f09a0d4dd824'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126669'>#126669</a>
- Add missing appletInterface prop. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=12a64ed82308b9a368d42f641fb23da8b18c3000'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357638'>#357638</a>
- Fix inconsistent margins, add missing listview margin, drop non-scalable measurements. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=aa99711fe71235ba6a1e4431c43d690673457e26'>Commit.</a>
- Fix favorites scripting in Kickoff. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=456100e0f9770534f1bfbb79183f598733936961'>Commit.</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Workaround broken bindings when enabling/disabling devices using rfkill. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=fc1a6599de071ad50df9dc063067a1a652583c0b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126795'>#126795</a>. Fixes bug <a href='https://bugs.kde.org/358028'>#358028</a>
- Make sure we show correct icon when a VPN connection with type of generic gets default route. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=87c78dd5e8176b3ab79b1660396dcfb3c114fb80'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126714'>#126714</a>. Fixes bug <a href='https://bugs.kde.org/357816'>#357816</a>

### <a name='plasma-sdk' href='http://quickgit.kde.org/?p=plasma-sdk.git'>Plasma SDK</a>

- Remove Encoding keys from .desktop files. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=b825652be2f857add0b2197526bddadb879bf214'>Commit.</a>
- Fix crash. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=f7bfb62e0190a72ebf99b50b5fb9af30341c7ca1'>Commit.</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Check client geom exists in clicks. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=988a7bef7e84a4a2fc6a2f6ec6145e093dfb84f8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/'>#</a>
- [notifications] Replace the icon on the button to expand the running job. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=994de033983c32fca24f09465cf9b98b3e28f568'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358282'>#358282</a>
- Keep disabling Qt's high DPI when on Qt 5.6. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bb3e7c47f4a5d0104756c515e2fa1920b5c726b1'>Commit.</a> See bug <a href='https://bugs.kde.org/357896'>#357896</a>
- [KSMServer] Dismiss logout dialog when clicking outside. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3dce39fd21eb36a1d912a299fad7fc3e2a4a7c72'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124586'>#124586</a>. Fixes bug <a href='https://bugs.kde.org/357143'>#357143</a>
- [notifications] Force the max height of the text item to be 0 when no text. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4f05c553f07eee25cda8d92ee6a7c298bb5f9860'>Commit.</a>
- [notifications] Also place the popup directly without animation if y == 0. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7079e8c20ac127290b25961b420f634f98a9f5b1'>Commit.</a>
- [notifications] Place the popup directly when it is displayed. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c1a7d61fbab7620f9f92f25ae82e96b9bf3a589c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126668'>#126668</a>
- Make the translators tab in the about dialog appear. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=47e0866c813063c6134c96499ba014751ede966c'>Commit.</a>
- Fix double-click in Widget Explorer not running config initialization scripts. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a4c6151e41d0c282495a34785c3aca1893a91828'>Commit.</a>
- Check for null geometry in client window. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6232362cca7021e5b436d267e07f9d6875a20a4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355463'>#355463</a>
- [Device Notifier] Improve legibility of device status label. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=abb010cae5c6c0093aec21b81585fbebfc46faa3'>Commit.</a>
- [notifications] Ensure the applet gets correct screen geometry when loaded. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b806d726ccfc2507bb7636029e60a9b02b6da3a3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357652'>#357652</a>
- [notifications] Replace the mainItem's Layout.max/minWidth with fixed width. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=03cf4b2ba8810323e633e4e6708419c7be3a5030'>Commit.</a>
- [notifications] Ensure the screen position selector works after state change. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=32ace708bdaa5bf0494274661aacc5beff527cdc'>Commit.</a>

### <a name='polkit-kde-agent-1' href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git'>polkit-kde-agent-1</a>

- Properly set modality on AuthDialog. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=2abe1fe105d0b7843d90365d39677f6043c72684'>Commit.</a>

### <a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a>

- Don't consider Unknown output to be an external monitor. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=fefe2878f5c204fa01306e9e6c60045295ef66a0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126721'>#126721</a>. See bug <a href='https://bugs.kde.org/357868'>#357868</a>