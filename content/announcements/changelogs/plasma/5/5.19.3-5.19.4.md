---
aliases:
- /announcements/plasma-5.19.3-5.19.4-changelog
hidden: true
title: Plasma 5.19.4 Complete Changelog
---

<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Check if we successfully restored input focus. <a href='https://commits.kde.org/kwin/892d2ad128e53e5ad4d3ec70c87a4f802f10e023'>Commit.</a> See bug <a href='https://bugs.kde.org/424223'>#424223</a></li>
<li>Grab all possible keyboard modifiers for window commands. <a href='https://commits.kde.org/kwin/dccf0ee456c05ecfac9773f9e1fce4dee2d57b10'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424272'>#424272</a></li>
<li>KCM KWin Options setting ActiveMouseScreen set proper default value. <a href='https://commits.kde.org/kwin/c853f8313a213a0f7d0f98f662f7e98849d547ca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424389'>#424389</a></li>
<li>Resize maximised windows upon workspace change. <a href='https://commits.kde.org/kwin/b3c7d2b3f050e4a8e2c62303e419cac63bbdbc2a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423596'>#423596</a></li>
<li>Partially revert a0c4a8e766a2160. <a href='https://commits.kde.org/kwin/9c0b1096e2affec5f53045ed464574977c8e7207'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424223'>#424223</a></li>
<li>Don't perform MouseActivateRaiseAndPassClick for topmost windows. <a href='https://commits.kde.org/kwin/4c31eea31ce8d773b696362be698a09dfdc220aa'>Commit.</a> </li>
<li>[virtualkeyboard] Fix the qtvirtualkeyboard with Qt 5.15. <a href='https://commits.kde.org/kwin/030cf0c57f4478f78c115962f4e15b0bf42600fe'>Commit.</a> </li>
<li>[scripts/videowall] Reenable the config dialog. <a href='https://commits.kde.org/kwin/6cf2eb5428359ffc76ccad06ecefc491f15c0205'>Commit.</a> </li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Correctly replace spaces with nothing. <a href='https://commits.kde.org/libksysguard/75c2d512a4249f3076ba695acc0025ea7026b067'>Commit.</a> </li>
<li>Fix presets loading. <a href='https://commits.kde.org/libksysguard/44b1e41ad801e0d7a4a7ee7b6aa19279d9421afc'>Commit.</a> </li>
<li>Delete the face config ui when face gets switched. <a href='https://commits.kde.org/libksysguard/5992fb4c2ee5a6ee72432a766e33980529f79ab7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423071'>#423071</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[kcms/desktoppath] Use folder dialogs instead of file dialogs. <a href='https://commits.kde.org/plasma-desktop/f9e56dc705c48702ffe2fd26377bdfefdfcb27d1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424438'>#424438</a></li>
<li>[kcm cursortheme] Also clear default theme when resetting. <a href='https://commits.kde.org/plasma-desktop/a31cdfa4a55306d5b9acda6ff17a1c33e5f1682f'>Commit.</a> </li>
<li>Notify about changes when changing Global Theme. <a href='https://commits.kde.org/plasma-desktop/24211d51b223c1bb30a86f2c83a2331022dc8b69'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421745'>#421745</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Make hotspot configuration dialog bigger. <a href='https://commits.kde.org/plasma-nm/42806c6c82511b705a204334cab4c52c66a9f8b9'>Commit.</a> </li>
<li>Remove (seemingly debug) warning statement from passworddialog. <a href='https://commits.kde.org/plasma-nm/374226b438c5e8c484fc2c4f642bb4d565856cb4'>Commit.</a> </li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>Reset password field when the user clicks Ok. <a href='https://commits.kde.org/plasma-vault/5e6a53ba55fd60ace3adbd4ca57f90a5cd44992f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424063'>#424063</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Emit countChanged when we get a new source model. <a href='https://commits.kde.org/plasma-workspace/6b10bf28265250ae9438d2db03db628ce4ff8ad7'>Commit.</a> </li>
<li>[wallpaper] Avoid using pluginId for indexing package indexes. <a href='https://commits.kde.org/plasma-workspace/cbeae8e16eee3ba6fd66246805fa362292cc6e48'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423987'>#423987</a></li>
<li>Revert "Fix broken ENV variables for detailed settings". <a href='https://commits.kde.org/plasma-workspace/bd8aa17588233dd6f8927c17aef193f6e1df5e58'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423995'>#423995</a></li>
<li>[applet/systemtray] Regression: all applets in config are shown as disabled. <a href='https://commits.kde.org/plasma-workspace/30b26d1165839a94b0156542e3d3f5e356aea0cd'>Commit.</a> </li>
<li>Hide face config button if the face can't config. <a href='https://commits.kde.org/plasma-workspace/6c68d9cb78f32c29ae533f95c6c07dc2fd1893ae'>Commit.</a> </li>
<li>Only open KCM in systemsettings if it can be displayed. <a href='https://commits.kde.org/plasma-workspace/5bc6d83ae083632fa4eef21a03b1ef59dd9a852e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423612'>#423612</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Split args in RunScript again. <a href='https://commits.kde.org/powerdevil/3946eff8b298da05e0567886fd39283c04da8fef'>Commit.</a> </li>
</ul>