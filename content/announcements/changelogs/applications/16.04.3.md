---
aliases:
- ../../fulllog_applications-16.04.3
hidden: true
title: KDE Applications 16.04.3 Full Log Page
type: fulllog
version: 16.04.3
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix preserving local-only flags during Item merging. <a href='http://commits.kde.org/akonadi/1c43951cc1216683bbf6b66e453277a8cfe7d4e8'>Commit.</a> </li>
<li>Fix read-after-free in CollectionStatistics. <a href='http://commits.kde.org/akonadi/6f32336be990362c7f74d17f6225bc7345242f6c'>Commit.</a> </li>
<li>Update queryAttribute. Patch ok'ed by Dan. <a href='http://commits.kde.org/akonadi/f8b7f95048e247e70e630dfcbe56024ce952e369'>Commit.</a> </li>
<li>Re-enable SearchJobTest, finish testModifySearch(). <a href='http://commits.kde.org/akonadi/fe3c4fd57bbb960de58a76589146d2da1334ca30'>Commit.</a> </li>
<li>PersistentSearchAttribute: fix deserialization of parameters. <a href='http://commits.kde.org/akonadi/9fb47bb2620610a3d4c398ef1385184b128c8654'>Commit.</a> </li>
<li>PersistentSearchAttribute: change 'remote' default to false. <a href='http://commits.kde.org/akonadi/f59584d96c4c97116a713b3aae1037f0108112bd'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Fix starting Ark from Plasma 5.7. <a href='http://commits.kde.org/ark/132743e572fb7550cbfbbe8db848f2a28a0001cb'>Commit.</a> </li>
<li>Archivemodel: check for ListJob errors. <a href='http://commits.kde.org/ark/97eb1b3598dcc684c3cc7ec410b8a2e85cf551c8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361757'>#361757</a></li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Show]</a></h3>
<ul id='ulcantor' style='display: none'>
<li>Import Sage LaTeX macros when the output. <a href='http://commits.kde.org/cantor/abc5d99184c3591e93028476d7f17f7bdb710b09'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/312738'>#312738</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>[DBusInterface] Use %U instead of %u to indicate multiple URLs are supported. <a href='http://commits.kde.org/dolphin/6bda60f1dd2708996d260c7d867f227e032f9185'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128306'>#128306</a></li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Show]</a></h3>
<ul id='uleventviews' style='display: none'>
<li>Port away from KLocale (which was used for use12Clock()). <a href='http://commits.kde.org/eventviews/376da8f1c6d537f387d905ab4466ec6d7a6c8df6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357556'>#357556</a></li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Tab Switcher Plugin: Make sure the height fits all items. <a href='http://commits.kde.org/kate/60fb10d22f17513f8179d4960fb4b2c5d765dea4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354019'>#354019</a></li>
<li>Correctly restore view config of all split views. <a href='http://commits.kde.org/kate/f4b885a89869abb96e1e536c769f396d2372c527'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353852'>#353852</a>. Code review <a href='https://git.reviewboard.kde.org/r/128163'>#128163</a></li>
</ul>
<h3><a name='kde-baseapps' href='https://cgit.kde.org/kde-baseapps.git'>kde-baseapps</a> <a href='#kde-baseapps' onclick='toggle("ulkde-baseapps", this)'>[Show]</a></h3>
<ul id='ulkde-baseapps' style='display: none'>
<li>Cmake: use variable for knewstuff3 library. <a href='http://commits.kde.org/kde-baseapps/3eab13bc1e7fbe6c61bc156f7d6342298678dbcd'>Commit.</a> </li>
<li>Set cmake_min_req to match kdelibs policy and enable newer cmake policies. <a href='http://commits.kde.org/kde-baseapps/a74f99409d463837094c996c19e5f90fe92dea04'>Commit.</a> </li>
</ul>
<h3><a name='kde-runtime' href='https://cgit.kde.org/kde-runtime.git'>kde-runtime</a> <a href='#kde-runtime' onclick='toggle("ulkde-runtime", this)'>[Show]</a></h3>
<ul id='ulkde-runtime' style='display: none'>
<li>Cmake: fix add_definitions according to new policy. <a href='http://commits.kde.org/kde-runtime/823d2fbd9cbc146e007510a41b4820fb8b9e81b8'>Commit.</a> </li>
<li>Set cmake_min_req to match kdelibs policy and enable newer cmake policies. <a href='http://commits.kde.org/kde-runtime/5a1d89e006ca69c6bf5351a796f7da57a6f96582'>Commit.</a> </li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Show]</a></h3>
<ul id='ulkdebugsettings' style='display: none'>
<li>Hide menu entry. <a href='http://commits.kde.org/kdebugsettings/edb153a257031a192f15b1384be0b40e763e3cbd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364300'>#364300</a></li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Support percentage unit specification in border radius. <a href='http://commits.kde.org/kdelibs/44a7dcf01dff8c055cb1f218574ece1500fb7e4f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365145'>#365145</a></li>
<li>Backport 4a51dd6 from kio to fix CI. <a href='http://commits.kde.org/kdelibs/8010a3abc9672be30c31d4bb790960dbfc952d77'>Commit.</a> </li>
<li>Try harder to skip this test if ktimezoned is missing. <a href='http://commits.kde.org/kdelibs/2c150e63451f4f4a66f9c60ca8085b59084dc349'>Commit.</a> </li>
<li>Skip test if ktimezoned isn't available. <a href='http://commits.kde.org/kdelibs/5948c9b1c364df959a45c0a5b6724483564987d3'>Commit.</a> </li>
<li>Removed prefixed version of background and border radius properties. <a href='http://commits.kde.org/kdelibs/93ec0e260382eabe2162de323026dca546c6eab2'>Commit.</a> </li>
<li>Fixes in 4-values shorthand constructor function. <a href='http://commits.kde.org/kdelibs/acc9b257f61efbbd44b68c72046d9b58521fbbb2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365007'>#365007</a></li>
<li>Create string objects only if they will be used. <a href='http://commits.kde.org/kdelibs/e01a2f91f79df3504825f8e05bcbca4e4a5878a0'>Commit.</a> </li>
<li>Fix applying inherit value for outline shorthand property. <a href='http://commits.kde.org/kdelibs/dd30e70269e554c5c20c53135a286756be05fdf7'>Commit.</a> </li>
<li>Fix c++11 build. <a href='http://commits.kde.org/kdelibs/0f5f0d5db4e7afc72cf8c767149b5bc333342785'>Commit.</a> </li>
<li>Fix reproducibility in builds by ensuring utf-8 encoding. <a href='http://commits.kde.org/kdelibs/d6655e34f05516b9e857e8b2e1a9238c33875924'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128102'>#128102</a></li>
<li>Do not use C-style casts into void*. <a href='http://commits.kde.org/kdelibs/d746370814a40a6d5bd56ae5dc13909aec9d62f4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/325055'>#325055</a></li>
<li>Fix compilation with gcc6. <a href='http://commits.kde.org/kdelibs/e36051fd7cf46093bf96cb5eb987102a605be52a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128160'>#128160</a></li>
<li>Handle initial and inherit for border radius. <a href='http://commits.kde.org/kdelibs/b4b561b9bfc0e93e80c7b084ff6a3607d1921d8d'>Commit.</a> </li>
<li>Discard property if we caught an invalid length|percent as background-size. <a href='http://commits.kde.org/kdelibs/930d2e2100f381c36940834575d70c87b0d5b857'>Commit.</a> </li>
<li>CssText must output comma separated values for those properties. <a href='http://commits.kde.org/kdelibs/b493ca778f907c6fba6a7f018cbebca1b32dd630'>Commit.</a> </li>
<li>Fix parsing background-clip in shorthand. <a href='http://commits.kde.org/kdelibs/b1ddfe44f1360065960e150389075bfbc9f0e7f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364158'>#364158</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Revert OpenGL change that might cause startup crash on some config. <a href='http://commits.kde.org/kdenlive/778ab830174e91d163159cafaa200e2bbc873862'>Commit.</a> See bug <a href='https://bugs.kde.org/364278'>#364278</a></li>
<li>Fix shadow offset in title editor. <a href='http://commits.kde.org/kdenlive/c9fbd3f56709ae578e2fb80a1948f4f892129334'>Commit.</a> See bug <a href='https://bugs.kde.org/364584'>#364584</a></li>
<li>Fix crash on reverse clip job (backport from master). <a href='http://commits.kde.org/kdenlive/f96ed884857cd47f224c51e6373611dded21d074'>Commit.</a> </li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Fix Bug 358679 - kmail5 crash on reload when config to empty trash on exit. <a href='http://commits.kde.org/kdepim/544f8cd337d7179cf51cd908f012431243760ad1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358679'>#358679</a></li>
<li>Remove deprecated calls. <a href='http://commits.kde.org/kdepim/b3f5d116d88d61f2f33ba0c1a36e434f32f806e5'>Commit.</a> </li>
<li>Update version. <a href='http://commits.kde.org/kdepim/1f4f85606ef0f0082a01d1de91ebaa9cb81f615c'>Commit.</a> </li>
<li>Bug 359163: Use current email address preference setting. <a href='http://commits.kde.org/kdepim/99b6b56dc21716ad872d268d3902f5836aee588d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359163'>#359163</a></li>
<li>Guard against null dereference. <a href='http://commits.kde.org/kdepim/3fd3b4766d84a28d88c73a67040b8d7bc2fa0dc8'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Show]</a></h3>
<ul id='ulkdepim-addons' style='display: none'>
<li>Fix crash when ShortUrlEnginePlugin cannot be loaded. <a href='http://commits.kde.org/kdepim-addons/2272b07dcead734fcdbec4e571ee2e005baecbd9'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Fix save/load. <a href='http://commits.kde.org/kdepim-runtime/545dac4da3da5006cac72198729f5d061d158b06'>Commit.</a> </li>
<li>Rename variable. <a href='http://commits.kde.org/kdepim-runtime/8d1dc0eb28840b89d9ae7f67bea9964fbb5aacc1'>Commit.</a> </li>
<li>Remove debug. <a href='http://commits.kde.org/kdepim-runtime/5736f0379ebe63428e6853fb3d9c093554cb5299'>Commit.</a> </li>
<li>Fix check enable/disable value. <a href='http://commits.kde.org/kdepim-runtime/f29100ed11f4e24617249241e0343748993786c5'>Commit.</a> </li>
<li>Reorganize code. <a href='http://commits.kde.org/kdepim-runtime/184af370016e23c9bb114d8285317c540da268c9'>Commit.</a> </li>
<li>Rename class. <a href='http://commits.kde.org/kdepim-runtime/5715a8db2a97dab575eb6b6c1ba42110fe88a1d2'>Commit.</a> </li>
<li>Try to fix load status. <a href='http://commits.kde.org/kdepim-runtime/ce37e82a5ea208de5f73e3f0297cfee2626eabc1'>Commit.</a> </li>
<li>Create test application. It will more easy to fix bug. <a href='http://commits.kde.org/kdepim-runtime/abd6515298e4b582192d6e6690885314d4a47fd3'>Commit.</a> </li>
<li>Add tests subdirectory. <a href='http://commits.kde.org/kdepim-runtime/9e8985dcb5f782e867c6c7d980d9c7ca9e381484'>Commit.</a> </li>
</ul>
<h3><a name='khangman' href='https://cgit.kde.org/khangman.git'>khangman</a> <a href='#khangman' onclick='toggle("ulkhangman", this)'>[Show]</a></h3>
<ul id='ulkhangman' style='display: none'>
<li>Set the minimum KF5 version to 5.15. <a href='http://commits.kde.org/khangman/ad92fc145351297cf297e2280681593ba0070e65'>Commit.</a> </li>
</ul>
<h3><a name='khelpcenter' href='https://cgit.kde.org/khelpcenter.git'>khelpcenter</a> <a href='#khelpcenter' onclick='toggle("ulkhelpcenter", this)'>[Show]</a></h3>
<ul id='ulkhelpcenter' style='display: none'>
<li>Clean the status bar message from the HTML part. <a href='http://commits.kde.org/khelpcenter/9179039c028e396afb8018a5cf18f14505a0ecda'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361932'>#361932</a></li>
<li>Bump version (there is a fix). <a href='http://commits.kde.org/khelpcenter/817e40d2e62abc0badd4b533d34eea418b18812a'>Commit.</a> </li>
<li>Switch to search widget only on search. <a href='http://commits.kde.org/khelpcenter/3f886c79f2be70f984406901fbba0b70616f851e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363830'>#363830</a></li>
<li>Applications, Manpages search providers by default. <a href='http://commits.kde.org/khelpcenter/de377fc5028863042cecfdbfbe08ab2eda8b3c8b'>Commit.</a> </li>
</ul>
<h3><a name='kimap' href='https://cgit.kde.org/kimap.git'>kimap</a> <a href='#kimap' onclick='toggle("ulkimap", this)'>[Show]</a></h3>
<ul id='ulkimap' style='display: none'>
<li>Fix crash when server returns non-standard EXPUNGE response. <a href='http://commits.kde.org/kimap/8933e1dcfe7686683c5d804b968dfc4a3af40f34'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364045'>#364045</a></li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Show]</a></h3>
<ul id='ulkleopatra' style='display: none'>
<li>Pedantic. <a href='http://commits.kde.org/kleopatra/c7bd960529b001364f80c23f901543f79869daff'>Commit.</a> </li>
<li>Fix Bug 364296 - Send certificate by mail: Wrong attachment path. <a href='http://commits.kde.org/kleopatra/60dc0bc4d6449e384d88a8e398261e1bb98f0a04'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364296'>#364296</a></li>
</ul>
<h3><a name='kmahjongg' href='https://cgit.kde.org/kmahjongg.git'>kmahjongg</a> <a href='#kmahjongg' onclick='toggle("ulkmahjongg", this)'>[Show]</a></h3>
<ul id='ulkmahjongg' style='display: none'>
<li>Fix endless loop when configured layout cannot be loaded. <a href='http://commits.kde.org/kmahjongg/22600c69edbf59bc83377e3b96e3899ba935b1a3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361132'>#361132</a>. Code review <a href='https://git.reviewboard.kde.org/r/128279'>#128279</a></li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 16.04.3. <a href='http://commits.kde.org/kopete/0c470bae4062cbca0c3b0915702ffb2a2c4cdd59'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Show]</a></h3>
<ul id='ulkpimtextedit' style='display: none'>
<li>Fix hide/show replace widget when we close widget. <a href='http://commits.kde.org/kpimtextedit/92c36fde6c0692cb60cfe8eea6d0fa4de6f5e0f0'>Commit.</a> </li>
</ul>
<h3><a name='krdc' href='https://cgit.kde.org/krdc.git'>krdc</a> <a href='#krdc' onclick='toggle("ulkrdc", this)'>[Show]</a></h3>
<ul id='ulkrdc' style='display: none'>
<li>Fix bookmarks migration from the KDE4 version. <a href='http://commits.kde.org/krdc/7d9ad0152dbc47c30f261c9d4c026e01bea31b7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361823'>#361823</a>. Code review <a href='https://git.reviewboard.kde.org/r/128281'>#128281</a></li>
<li>Pass a proper version string to KAboutData. <a href='http://commits.kde.org/krdc/c6fdda25fe1903999268716a5dcbe748f11c4dbd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128282'>#128282</a></li>
</ul>
<h3><a name='ksystemlog' href='https://cgit.kde.org/ksystemlog.git'>ksystemlog</a> <a href='#ksystemlog' onclick='toggle("ulksystemlog", this)'>[Show]</a></h3>
<ul id='ulksystemlog' style='display: none'>
<li>Make kiologfilereader test actually do something. <a href='http://commits.kde.org/ksystemlog/8283de282f103501ae8d0108db1c52feddfc331c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128328'>#128328</a></li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Show]</a></h3>
<ul id='ullibksieve' style='display: none'>
<li>Revert "Fix Bug 328246 - Manage Sieve scripts feature broken". <a href='http://commits.kde.org/libksieve/abc329d2b4d3a8efb489b0aa8dfb9cc2d2da9472'>Commit.</a> </li>
<li>Fix conflict shortcut. <a href='http://commits.kde.org/libksieve/c865a238487f0723538ad387173d7d18b1ebc87d'>Commit.</a> </li>
<li>Fix crash when server doesn't support "date". <a href='http://commits.kde.org/libksieve/9034c1d076fcb4d052b61b6a5c634e71b5b9aba9'>Commit.</a> </li>
<li>Fix layout warning. <a href='http://commits.kde.org/libksieve/9bf6d23d4021596af75cc09aa28cd72d30128f64'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Disable registration of marblepart as filehandler, currently broken. <a href='http://commits.kde.org/marble/75a3cfd58aee6cc3edba0bcb191fdec2776b355e'>Commit.</a> </li>
<li>Fix parsing of geo coordinates in locale encoding (+ extend unit test). <a href='http://commits.kde.org/marble/1b1de18783cab8a0589cf3768221f189a2dd1705'>Commit.</a> </li>
<li>Fix bicycle tour API request. <a href='http://commits.kde.org/marble/5d5031c79ceec2c6f0bfa14aa4b78186559925b3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363587'>#363587</a></li>
<li>Do not crash when toggling projections. <a href='http://commits.kde.org/marble/880aafbac65be7e5dd30e5207084c22a7bbecc8b'>Commit.</a> </li>
<li>Move win/mac installation files to common dir. Split iss files. <a href='http://commits.kde.org/marble/2ae0a9568a5ef4a32958c11d27d32799376a1eac'>Commit.</a> </li>
<li>Use a Marble start menu folder, install website link. <a href='http://commits.kde.org/marble/d7704fb34edd24dfbe2260d408fcf78797d4375a'>Commit.</a> </li>
<li>Sync windows installer version. <a href='http://commits.kde.org/marble/5ecc187374ab5c34c6329708bd77e2f98e7661cf'>Commit.</a> </li>
<li>Install import libraries on Windows to lib/. <a href='http://commits.kde.org/marble/922b1b65108b0d3d12d1b66b05272c1b922d7d5a'>Commit.</a> </li>
<li>Fix linker error when using OpenCV 3.1. <a href='http://commits.kde.org/marble/af52a8a2c8ce8a36c7b901ad8ebc4b54d733a103'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Fix Identifier. Remove not support expanded bcc address. <a href='http://commits.kde.org/messagelib/9761598000055612ca09a74f7ed21d2017d8e392'>Commit.</a> </li>
<li>Don't create big icon. <a href='http://commits.kde.org/messagelib/9261d195c5c3598852f36308549730e252fc34a9'>Commit.</a> </li>
<li>Fix icon size. <a href='http://commits.kde.org/messagelib/c7ea32b497f88f9d9637926486b8bebadbee4c05'>Commit.</a> </li>
<li>Fix Bug 364872 - Adblock not working because list(s) can't be saved on disk. <a href='http://commits.kde.org/messagelib/6f209856ea4ac67b74465126387f7111a949bbc3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364872'>#364872</a></li>
<li>Fix mem leak. <a href='http://commits.kde.org/messagelib/3076ce908d4f142dc0e9f9cbb9c16ed58d9a6ba4'>Commit.</a> </li>
<li>Fix create file (missing '/') when dnd between composer. <a href='http://commits.kde.org/messagelib/d9acbb4d4618b0b265e0cbd694347b16bfc88a92'>Commit.</a> </li>
<li>Load vcard plugin. <a href='http://commits.kde.org/messagelib/7c889402a30071b10512815bab69f65a230e4363'>Commit.</a> See bug <a href='https://bugs.kde.org/362958'>#362958</a></li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Bump version to 0.19.3. <a href='http://commits.kde.org/okteta/d339737bd4997f2386618212a52ebba7af5cff11'>Commit.</a> </li>
<li>Fix broken usage of url.path() for local file paths. <a href='http://commits.kde.org/okteta/ae3680ab8a2405139f1c56ee847a75a73be93b0c'>Commit.</a> </li>
</ul>
<h3><a name='parley' href='https://cgit.kde.org/parley.git'>parley</a> <a href='#parley' onclick='toggle("ulparley", this)'>[Show]</a></h3>
<ul id='ulparley' style='display: none'>
<li>Google images do not work anymore (comment out this section of docs). <a href='http://commits.kde.org/parley/c6f9b7916e51950ef2904ac171a2503dff1e22dd'>Commit.</a> </li>
<li>Remove the google_images.py script: Google image search API is not available anymore. <a href='http://commits.kde.org/parley/45c5fb4fd3c6635fe9ae8b37b367eb8bb22125ac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364373'>#364373</a>. Code review <a href='https://git.reviewboard.kde.org/r/128229'>#128229</a></li>
</ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Show]</a></h3>
<ul id='ulpimcommon' style='display: none'>
<li>Remove duplicate margin. <a href='http://commits.kde.org/pimcommon/bb8377dee22336fb7bb2b3764d49d9f87881093e'>Commit.</a> </li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Show]</a></h3>
<ul id='ulstep' style='display: none'>
<li>Port i18n for stepcore to kf5. <a href='http://commits.kde.org/step/e3d58a87794d40ec325560a3196c366a9f1228f9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128286'>#128286</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'Umbrello crash'. <a href='http://commits.kde.org/umbrello/fa1e78b334a494f7b820bd85f7dcdb4a17b636f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364322'>#364322</a></li>
</ul>