---
aliases:
- ../../fulllog_applications-15.04.3
hidden: true
title: KDE Applications 15.04.3 Full Log Page
type: fulllog
version: 15.04.3
---

<h3><a name='bomber' href='https://cgit.kde.org/bomber.git'>bomber</a> <a href='#bomber' onclick='toggle("ulbomber", this)'>[Show]</a></h3>
<ul id='ulbomber' style='display: none'>
<li>Fix translate it. <a href='http://commits.kde.org/bomber/20c059c8f533f228ba3983253a7ed78c9c2b0f09'>Commit.</a> </li>
</ul>
<h3><a name='granatier' href='https://cgit.kde.org/granatier.git'>granatier</a> <a href='#granatier' onclick='toggle("ulgranatier", this)'>[Show]</a></h3>
<ul id='ulgranatier' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/granatier/e8065cd72b6b05cdadc0a639f84031ed71907192'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Make saving thumbnails work again. <a href='http://commits.kde.org/gwenview/94d4453ee8fdefb182e4730b0417e42ed56c3fb2'>Commit.</a> </li>
</ul>
<h3><a name='katomic' href='https://cgit.kde.org/katomic.git'>katomic</a> <a href='#katomic' onclick='toggle("ulkatomic", this)'>[Show]</a></h3>
<ul id='ulkatomic' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/katomic/a4e6e917f162b5e0788d4e9ebe86e43e7c207f64'>Commit.</a> </li>
</ul>
<h3><a name='kblackbox' href='https://cgit.kde.org/kblackbox.git'>kblackbox</a> <a href='#kblackbox' onclick='toggle("ulkblackbox", this)'>[Show]</a></h3>
<ul id='ulkblackbox' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/kblackbox/cfc7080667dd570748497a2c177941156ee38fa3'>Commit.</a> </li>
</ul>
<h3><a name='kblocks' href='https://cgit.kde.org/kblocks.git'>kblocks</a> <a href='#kblocks' onclick='toggle("ulkblocks", this)'>[Show]</a></h3>
<ul id='ulkblocks' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/kblocks/f7da8b031e4b7e23d039c7550b5fbf16c9bbc11a'>Commit.</a> </li>
</ul>
<h3><a name='kbounce' href='https://cgit.kde.org/kbounce.git'>kbounce</a> <a href='#kbounce' onclick='toggle("ulkbounce", this)'>[Show]</a></h3>
<ul id='ulkbounce' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/kbounce/23e7b688f5632287f7e5c13cf94e9bd1d7953814'>Commit.</a> </li>
</ul>
<h3><a name='kbreakout' href='https://cgit.kde.org/kbreakout.git'>kbreakout</a> <a href='#kbreakout' onclick='toggle("ulkbreakout", this)'>[Show]</a></h3>
<ul id='ulkbreakout' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/kbreakout/337be1ab568ed63dae26fb737e918a2ec9f45db0'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Update kdoctools/customization/ca. <a href='http://commits.kde.org/kdelibs/facbfd3defc7b8b5ce2a3d734faee9cfc04b184a'>Commit.</a> </li>
<li>Update Brazilian Portuguese translation. <a href='http://commits.kde.org/kdelibs/bc9d29d4887aa7849c3e0b724864e053fce0c11c'>Commit.</a> </li>
<li>Update Brazilian Portuguese translation. <a href='http://commits.kde.org/kdelibs/51dbde8591c8a86451abf170d2b7606a922f143b'>Commit.</a> </li>
<li>Update customization/ru. <a href='http://commits.kde.org/kdelibs/e043a1fa3494ec706f20e8f9c6eebca16082dc52'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix autosave not working when opening project file from command line or click in file browser. <a href='http://commits.kde.org/kdenlive/28b3cc7b5141fcc2fed84433b9fe9851c35b7e08'>Commit.</a> See bug <a href='https://bugs.kde.org/348674'>#348674</a></li>
<li>Re-open development for 15.04.3 release. <a href='http://commits.kde.org/kdenlive/a3e6a47fc78ff1276e31313a246025c0756c8ed0'>Commit.</a> </li>
<li>Fix zoom broken after context menu shown in timeline. <a href='http://commits.kde.org/kdenlive/0ccca1e07077773d402265761a0d168b66d7b4f3'>Commit.</a> See bug <a href='https://bugs.kde.org/348671'>#348671</a></li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Change OpenPGP key defaults to match GnuPG's. <a href='http://commits.kde.org/kdepim/61c335853895e50bec7c749dcd505200e96d0521'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124180'>#124180</a></li>
<li>Do not try to get audit log for erroneous contexts. <a href='http://commits.kde.org/kdepim/be886ddf9397ab1ee40a075a1ee34c5c42dea038'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349180'>#349180</a>. Code review <a href='https://git.reviewboard.kde.org/r/124175'>#124175</a></li>
<li>Don't signal as scam email when we have kmail:showAuditLog. <a href='http://commits.kde.org/kdepim/e0fc73a261d9bdd9092841b0b41caf227098e494'>Commit.</a> </li>
<li>Fix Bug 349180 - resizing attached images corrupts the images. <a href='http://commits.kde.org/kdepim/7b928f3b5f76520960357616e737a9b2f9777d78'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349180'>#349180</a></li>
<li>Fix Bug 349180 - resizing attached images corrupts the images. <a href='http://commits.kde.org/kdepim/b9d88a5f1b2d2a814572437c32b456cceb22fa23'>Commit.</a> See bug <a href='https://bugs.kde.org/349180'>#349180</a></li>
<li>[OS X] avoid crashing KNode when unsubscribing from a group. <a href='http://commits.kde.org/kdepim/217000009a9cb538bda53e14bbe8315014fb2b14'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124061'>#124061</a></li>
<li>ToLower is not necessary indeed. <a href='http://commits.kde.org/kdepim/210cc2a1281528c4f1edd8d5f9d9fec242534e17'>Commit.</a> </li>
<li>Don't duplicate identity when we resend email/reedit message. <a href='http://commits.kde.org/kdepim/60c71d1ff1cd05af3c16d373dc45692cb0a27290'>Commit.</a> </li>
<li>Use "name.contains(str, Qt::CaseInsensitive)" it would be faster. <a href='http://commits.kde.org/kdepim/c84abce107ee47bd5e4707165e86bf14a6002a06'>Commit.</a> </li>
<li>Fix replace action. <a href='http://commits.kde.org/kdepim/475e54749ada3241e304d38a9ee59ef412fe3de0'>Commit.</a> </li>
<li>Increase version for new release. <a href='http://commits.kde.org/kdepim/e537a78e719b78c2b3d0da2e758fe20d5da35f38'>Commit.</a> </li>
<li>Fix enable/disable checkbox. <a href='http://commits.kde.org/kdepim/b185c1791881c6582b1ab221ac75e467868114af'>Commit.</a> </li>
<li>ExtractEmailAddress can return an empty string if email is not correct. <a href='http://commits.kde.org/kdepim/1199495cab40da680aa4ffc70c69c5ddf3340142'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Pre-validation of the collections propfind response. <a href='http://commits.kde.org/kdepim-runtime/b3bfa11aae9b6d7696bae7de0bf5afdc79a91fba'>Commit.</a> See bug <a href='https://bugs.kde.org/341998'>#341998</a></li>
<li>Add simple conflict handling. <a href='http://commits.kde.org/kdepim-runtime/44d38d363d6ff7b4f8c892becfb58a0339dbef0e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/335090'>#335090</a>. Fixes bug <a href='https://bugs.kde.org/338570'>#338570</a></li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/ab0e811039780fa0a7f20750db14def1684c151e'>Commit.</a> </li>
</ul>
<h3><a name='kdepimlibs' href='https://cgit.kde.org/kdepimlibs.git'>kdepimlibs</a> <a href='#kdepimlibs' onclick='toggle("ulkdepimlibs", this)'>[Show]</a></h3>
<ul id='ulkdepimlibs' style='display: none'>
<li>ResourceBase: explain what the resource should do in doSetOnline(false). <a href='http://commits.kde.org/kdepimlibs/ea518aef354afbcd0ec9f5c1be4344418535cc31'>Commit.</a> </li>
<li>Make sure that email is not empty. <a href='http://commits.kde.org/kdepimlibs/6dd93871d5b39298ac831edfe990660d9ea3dc5b'>Commit.</a> </li>
<li>Make sure that text is not empty. <a href='http://commits.kde.org/kdepimlibs/3d1461d5dc6cdded0ec6eb0ba627b8dcb3c31144'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepimlibs/f4e9b2dcd917a3499cbe31dd10ac3c4deaee857e'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kdepimlibs/bc5ef3db60a164448da07d70416303748050765a'>Commit.</a> </li>
<li>Add unittest for Addressee::parseEmailAddress to compare with new version in kcontact. <a href='http://commits.kde.org/kdepimlibs/8eefa22d73e6259767b68abdfbe5db96ea749fba'>Commit.</a> </li>
</ul>
<h3><a name='kdiamond' href='https://cgit.kde.org/kdiamond.git'>kdiamond</a> <a href='#kdiamond' onclick='toggle("ulkdiamond", this)'>[Show]</a></h3>
<ul id='ulkdiamond' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/kdiamond/7d33581fe714bc768e91d2a25db9d4bde33219a3'>Commit.</a> </li>
</ul>
<h3><a name='kfourinline' href='https://cgit.kde.org/kfourinline.git'>kfourinline</a> <a href='#kfourinline' onclick='toggle("ulkfourinline", this)'>[Show]</a></h3>
<ul id='ulkfourinline' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/kfourinline/9cf3a395513c53abe40271bf5bdafba234540a80'>Commit.</a> </li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Show]</a></h3>
<ul id='ulkig' style='display: none'>
<li>Explicitly Set the Default Save Path for Exporters. <a href='http://commits.kde.org/kig/a344e65304c5fe903159646fdd9b636b4965ef76'>Commit.</a> </li>
<li>Fix Layout and Behavior of Options Dialog. <a href='http://commits.kde.org/kig/afcca29b19f15e5b99992c848681bfc6073e3aba'>Commit.</a> </li>
</ul>
<h3><a name='killbots' href='https://cgit.kde.org/killbots.git'>killbots</a> <a href='#killbots' onclick='toggle("ulkillbots", this)'>[Show]</a></h3>
<ul id='ulkillbots' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/killbots/b5a2237b7b26af225c390768cdcf1bb6a8548cfb'>Commit.</a> </li>
</ul>
<h3><a name='kiten' href='https://cgit.kde.org/kiten.git'>kiten</a> <a href='#kiten' onclick='toggle("ulkiten", this)'>[Show]</a></h3>
<ul id='ulkiten' style='display: none'>
<li>Include QObject header to fix build with Qt 5.5. <a href='http://commits.kde.org/kiten/47a7c12fc27d6b95467c8a9994f9c12371b6422d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123945'>#123945</a></li>
</ul>
<h3><a name='kjumpingcube' href='https://cgit.kde.org/kjumpingcube.git'>kjumpingcube</a> <a href='#kjumpingcube' onclick='toggle("ulkjumpingcube", this)'>[Show]</a></h3>
<ul id='ulkjumpingcube' style='display: none'>
<li>Include QObject header to fix build with Qt 5.5. <a href='http://commits.kde.org/kjumpingcube/87b8c912740e59de1c7dcbe8ddfc6bdd4eb5bc33'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123947'>#123947</a></li>
<li>Fix translation. <a href='http://commits.kde.org/kjumpingcube/7f57affc3ab77d2b1c3c9588a44a4459642ef5b5'>Commit.</a> </li>
</ul>
<h3><a name='klines' href='https://cgit.kde.org/klines.git'>klines</a> <a href='#klines' onclick='toggle("ulklines", this)'>[Show]</a></h3>
<ul id='ulklines' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/klines/3c5323f716dd067fee34b9412305fa72d2eaab56'>Commit.</a> </li>
</ul>
<h3><a name='kmines' href='https://cgit.kde.org/kmines.git'>kmines</a> <a href='#kmines' onclick='toggle("ulkmines", this)'>[Show]</a></h3>
<ul id='ulkmines' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/kmines/fc403c0c16a214f7d27b4275ea2c86cd06fbd4e3'>Commit.</a> </li>
</ul>
<h3><a name='kompare' href='https://cgit.kde.org/kompare.git'>kompare</a> <a href='#kompare' onclick='toggle("ulkompare", this)'>[Show]</a></h3>
<ul id='ulkompare' style='display: none'>
<li>Add QtPlugin header to fix build with Qt 5.5. <a href='http://commits.kde.org/kompare/0f65e84190013583e8171ca2bf8641b234e020d3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123948'>#123948</a></li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Libjingle: Fix compilation with mediastreamer >= 2.9. <a href='http://commits.kde.org/kopete/a1c1876db703618327f44aa31209e066df86cdd8'>Commit.</a> </li>
<li>Libjingle: Fix expression for port variable. <a href='http://commits.kde.org/kopete/cb2a9f34894002fadb643dca9f812396c4910878'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348356'>#348356</a></li>
<li>Libjingle: Do not drop rtp packets with invalid ssrc. <a href='http://commits.kde.org/kopete/22554d99c8bf8e6b7e7c9bd305d09a4ef69a02fd'>Commit.</a> </li>
<li>Libjingle: Regenerate linphonemediaengine patches. <a href='http://commits.kde.org/kopete/4dbdd0c3f631f08590d0a3515de52e426941c14e'>Commit.</a> </li>
<li>Libjingle: Revert "Fix handling non rtcp packets in libjingle". <a href='http://commits.kde.org/kopete/3b8969d9304178e3d0287f8845fb6b74778b5319'>Commit.</a> </li>
<li>Libjingle: Fix selecting voice codec. <a href='http://commits.kde.org/kopete/dfc424a2f96249b223886979f4f3c8a5ff28e88a'>Commit.</a> </li>
<li>Libjingle: Disable oRTP debug messages in non-debug mode. <a href='http://commits.kde.org/kopete/2a4fc9fd386160e95282a81c2dc0a7b0386eefe5'>Commit.</a> </li>
<li>Libjingle: Fix mute support. <a href='http://commits.kde.org/kopete/119eaaf0699e5d8b42b19b3b84a01a6305ea011e'>Commit.</a> </li>
<li>Libjingle: Fix processing control rtcp packets. <a href='http://commits.kde.org/kopete/64d374b7142ca757248ae5d0986a5f35ceb55542'>Commit.</a> </li>
<li>Libjingle: Set rtp ssrc stream parameter correctly. <a href='http://commits.kde.org/kopete/7b04b688c5a034ec0483abd5991313ba095c3e14'>Commit.</a> </li>
<li>Libjingle: Enable SSL verification only for google server. <a href='http://commits.kde.org/kopete/e54feb425846b9c7cbb796ca088f00937e3ec609'>Commit.</a> </li>
</ul>
<h3><a name='ksquares' href='https://cgit.kde.org/ksquares.git'>ksquares</a> <a href='#ksquares' onclick='toggle("ulksquares", this)'>[Show]</a></h3>
<ul id='ulksquares' style='display: none'>
<li>Fix translation. <a href='http://commits.kde.org/ksquares/b726c5b8444c9df1f6031eb17962144cbc737cbc'>Commit.</a> </li>
</ul>
<h3><a name='ktp-contact-list' href='https://cgit.kde.org/ktp-contact-list.git'>ktp-contact-list</a> <a href='#ktp-contact-list' onclick='toggle("ulktp-contact-list", this)'>[Show]</a></h3>
<ul id='ulktp-contact-list' style='display: none'>
<li>Use QPalette::ColorGroup from the option rather than always QPalette::Active. <a href='http://commits.kde.org/ktp-contact-list/de4d38c734576cd6ea8e259bef60b29027ddd103'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349523'>#349523</a></li>
</ul>
<h3><a name='libkexiv2' href='https://cgit.kde.org/libkexiv2.git'>libkexiv2</a> <a href='#libkexiv2' onclick='toggle("ullibkexiv2", this)'>[Show]</a></h3>
<ul id='ullibkexiv2' style='display: none'>
<li>Namespace from ACDSee added. <a href='http://commits.kde.org/libkexiv2/f2c866b5ab264300d414415c1c2303fae73812d8'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Version bump to 0.21.3 (stable release). <a href='http://commits.kde.org/marble/8e00020daed1435c8c923c2c9abc0e12d1b3b1f1'>Commit.</a> </li>
<li>Fix wrong decoding (longitude calculation) of some MIC-E positions. <a href='http://commits.kde.org/marble/a7482b5381f22640add20ba1b190e60d7715b400'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348209'>#348209</a></li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Bump version to 0.16.3. <a href='http://commits.kde.org/okteta/a1f4ef2046623e61b901470372e4c38f140d3b6f'>Commit.</a> </li>
<li>Fix hidden last row of bytes by horizontal scrollbar with vertical scrollbar on. <a href='http://commits.kde.org/okteta/817ca042c66d1c01abd143c714c20b494b388fc9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343809'>#343809</a></li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Show]</a></h3>
<ul id='ulstep' style='display: none'>
<li>Add forward declaration of QIODevice. <a href='http://commits.kde.org/step/dfa5fdd634ac3c88fb5e32aa44035eb7bf826642'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123949'>#123949</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Only apply UIState related settings displayed in the style page to the scene. <a href='http://commits.kde.org/umbrello/0b7dc1040a1a1bbaec9795a221ed5fd7f29fadb9'>Commit.</a> </li>
<li>Coverige check CID 71451: Big parameter passed by value (PASS_BY_VALUE). <a href='http://commits.kde.org/umbrello/61083fe3fd4a178684193cc45238f316ba9a08fb'>Commit.</a> </li>
<li>Fix 'Export docbook or xhtml fails to prompt user for name'. <a href='http://commits.kde.org/umbrello/eb4a0585fe942f3a3882694e05499d229fe3aa0b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/206898'>#206898</a></li>
<li>Coverige check CID 71433: Logically dead code (DEADCODE). <a href='http://commits.kde.org/umbrello/2d442e9343b998f1241252e2b496259ee3cb073c'>Commit.</a> </li>
<li>Coverity check CID 71467: Logically dead code (DEADCODE). <a href='http://commits.kde.org/umbrello/406eb4b7fb129bfc196f5fbb6246ed0f623b9429'>Commit.</a> </li>
<li>Fix 'Crash on opening second xmi file'. <a href='http://commits.kde.org/umbrello/849f7a41efdc624b3096b1d7232d61a0ca7cada7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349259'>#349259</a></li>
<li>Fix gcc 4.8.1 compile error: ‘>>’ should be ‘> >’ within a nested template argument list. <a href='http://commits.kde.org/umbrello/d796909291f26b1059c081209910ee99c0017dab'>Commit.</a> </li>
<li>Fixup of 01633284efa85e5ca39626d70bb39f658206e8c4. <a href='http://commits.kde.org/umbrello/3da9b7a3fb2697c25010c4848e622561b08d3c04'>Commit.</a> </li>
<li>Fix crash caused by dangling pointers in UMLPackage::m_objects on document close. <a href='http://commits.kde.org/umbrello/4c3098fbcb73d64ef860bed1f771fc03d9826385'>Commit.</a> </li>
<li>Fix 'SVG export image does not look like the actual diagram'. <a href='http://commits.kde.org/umbrello/af8679befd1251475a4bcfdab5e6582a88d065d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349234'>#349234</a></li>
<li>Fix 'EPS export produces empty image'. <a href='http://commits.kde.org/umbrello/99ba6ce98482f965a5639a06ec7309b13c58ec22'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348848'>#348848</a></li>
<li>Update version to 2.16.3 (KDE Applications version 15.04.3). <a href='http://commits.kde.org/umbrello/2c0cefc8a2f978619c6338c16af9c524e6a1ccc5'>Commit.</a> </li>
<li>Coverige check CID 71523: Structurally dead code (UNREACHABLE). <a href='http://commits.kde.org/umbrello/19d8ea555dc6f3f03f5811fad6b95ff664899fa5'>Commit.</a> </li>
<li>Coverige check CID 71525: Structurally dead code (UNREACHABLE). <a href='http://commits.kde.org/umbrello/889a4ca1540d0ab1652b1515905e80c95ef31550'>Commit.</a> </li>
<li>Coverige check CID 71408: Logically dead code (DEADCODE). <a href='http://commits.kde.org/umbrello/7b2a25fbd17d2cb3f5b11507ede38b5bb002c64f'>Commit.</a> </li>
<li>Coverige check CID 71421: Logically dead code (DEADCODE). <a href='http://commits.kde.org/umbrello/b9988d1d93822ed207b6009d4cee4e0131ccbca1'>Commit.</a> </li>
<li>Coverige check CID 71423: Logically dead code (DEADCODE). <a href='http://commits.kde.org/umbrello/4d7c6144c1838a66572c2464769b2493a7fa493f'>Commit.</a> </li>
<li>Coverige check CID 71424: Logically dead code (DEADCODE). <a href='http://commits.kde.org/umbrello/2d485ca6c65b7a2581c1e70e3822ce8748a28300'>Commit.</a> </li>
<li>Coverity check CID 71437: Logically dead code (DEADCODE). <a href='http://commits.kde.org/umbrello/8a12fb3d515d37dd8a831bb154b4dbf788750b8f'>Commit.</a> </li>
<li>Fix bug not generating setter for static class attributes using old c++ codegenerator (CID 71454). <a href='http://commits.kde.org/umbrello/f654714ece6aa5ab7bdb293037dfeba9d71fff98'>Commit.</a> </li>
<li>Fix not working "Shift"+"Key_Right" and "Shift"+"Key_Left" shortcut in non tabbed diagram mode (CID 71474). <a href='http://commits.kde.org/umbrello/7835b6b292c42db370307e2ce06c84f1b3873ead'>Commit.</a> </li>
<li>Coverity check @b3d0c70, CID 88346: Logically dead code (DEADCODE). <a href='http://commits.kde.org/umbrello/f7c8ccdf02e57a5541477784c4e285616cbe56d0'>Commit.</a> </li>
<li>Coverity check CID 95917:  Error handling issues  (CHECKED_RETURN). <a href='http://commits.kde.org/umbrello/70431bf63376926f8831bc8d4bcc9229927fbab5'>Commit.</a> </li>
<li>Coverity check CID 95919:  Error handling issues  (CHECKED_RETURN). <a href='http://commits.kde.org/umbrello/765439d275f55d7c625a26101b20c908836bdd80'>Commit.</a> </li>
<li>Coverity check CID 71527: Uninitialized pointer field (UNINIT_CTOR). <a href='http://commits.kde.org/umbrello/5a9f235ecb53191f1028463e2d09c4475a426832'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Fix bug 'All diagram export fails with "use folder"'. <a href='http://commits.kde.org/umbrello/b63e082aada5eb1f69eaf455270766815f6edc95'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348687'>#348687</a></li>
<li>Add script for updating CMakeLists.txt patch level. <a href='http://commits.kde.org/umbrello/ba18916bc52924fd95f22ec4d8aeaa7bd34caba7'>Commit.</a> </li>
<li>Update version to 2.16.2 (KDE Applications version 15.04.2). <a href='http://commits.kde.org/umbrello/9b21f697640ab27c36af80d8402a679903ea050a'>Commit.</a> </li>
</ul>