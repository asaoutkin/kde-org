---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE Ships KDE Applications 16.12.3
layout: application
title: KDE Ships KDE Applications 16.12.3
version: 16.12.3
---

{{% i18n_var "March 9, 2017. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../16.12.0" %}}

More than 20 recorded bugfixes include improvements to kdepim, ark, filelight, gwenview, kate, kdenlive, okular, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.30" %}}