---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE Ships KDE Applications 16.12.0
layout: application
title: KDE Ships KDE Applications 16.12.0
version: 16.12.0
---

December 15, 2016. Today, KDE introduces KDE Applications 16.12, with an impressive array of upgrades when it comes to better ease of access, the introduction of highly useful functionalities and getting rid of some minor issues, bringing KDE Applications one step closer to offering you the perfect setup for your device.

{{% i18n_var "<a href='%[1]s'>Okular</a>, <a href='%[2]s'>Konqueror</a>, <a href='%[3]s'>KGpg</a>, <a href='%[4]s'>KTouch</a>, <a href='%[5]s'>Kalzium</a> and more (<a href='%[6]s'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release." "https://okular.kde.org/" "https://konqueror.org/" "https://www.kde.org/applications/utilities/kgpg/" "https://www.kde.org/applications/education/ktouch/" "https://www.kde.org/applications/education/kalzium/" "https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based" %}}

{{% i18n_var "In the continued effort to make applications easier to build standalone, we have split the kde-baseapps, kdepim and kdewebdev tarballs. You can find the newly created tarballs at <a href='%[1]s'>the Release Notes document</a>" "https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split" %}}

We have discontinued the following packages: kdgantt2, gpgmepp and kuser. This will help us focus on the rest of the code.

### Kwave sound editor joins KDE Applications!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Kwave</a> is a sound editor, it can record, play back, import and edit many sorts of audio files including multi channel files. Kwave includes some plugins to transform audio files in several ways and presents a graphical view with a complete zoom and scroll capability." "http://kwave.sourceforge.net/" %}}

### The World as your wallpaper

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble now includes both a Wallpaper and a Widget for Plasma that show the time on top of a satellite view of the earth, with real-time day/night display. These used to be available for Plasma 4; they have now been updated to work on Plasma 5.

{{% i18n_var "You can find more information on <a href='%[1]s'>Friedrich W. H. Kossebau's blog</a>." "https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/" %}}

### Emoticons galore!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

KCharSelect has gained the ability to show the Unicode Emoticons block (and other SMP symbol blocks).

It also gained a Bookmarks menu so you can favorite all your loved characters.

### Math is better with Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor has a new backend for Julia, giving its users the ability to use the latest progress in scientific computing.

{{% i18n_var "You can find more information on <a href='%[1]s'>Ivan Lakhtanov's blog</a>." "https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html" %}}

### Advanced archiving

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark has several new features:

- Files and folders can now be renamed, copied or moved within the archive
- It's now possible to select compression and encryption algorithms when creating archives
- Ark can now open AR files (e.g. Linux \*.a static libraries)

{{% i18n_var "You can find more information on <a href='%[1]s'>Ragnar Thomsen's blog</a>." "https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/" %}}

### And more!

Kopete got support for X-OAUTH2 SASL authentication in jabber protocol and fixed some problems with the OTR encryption plugin.

{{% i18n_var "Kdenlive has a new Rotoscoping effect, support for downloadable content and an updated Motion Tracker. It also provides <a href='%[1]s'>Snap and AppImage</a> files for easier installation." "https://kdenlive.org/download/" %}}

KMail and Akregator can use Google Safe Browsing to check if a link being clicked is malicious. Both have also added back printing support (needs Qt 5.8).

### Aggressive Pest Control

More than 130 bugs have been resolved in applications including Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark, Kdenlive and more!

### Full Changelog