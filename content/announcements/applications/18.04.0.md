---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE Ships KDE Applications 18.04.0
layout: application
title: KDE Ships KDE Applications 18.04.0
version: 18.04.0
---

April 19, 2018. KDE Applications 18.04.0 are now released.

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

## What's new in KDE Applications 18.04

### System

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

{{% i18n_var "The first major release in 2018 of <a href='%[1]s'>Dolphin</a>, KDE's powerful file manager, features many  improvements to its panels:" "https://www.kde.org/applications/system/dolphin/" %}}

- The 'Places' panel's sections can now be hidden if you prefer not to display them, and a new 'Network' section is now available to hold entries for remote locations.
- The 'Terminal' panel can be docked to any side of the window, and if you try to open it without Konsole installed, Dolphin will show a warning and help you install it.
- HiDPI support for the 'Information' panel has been improved.

The folder view and the menus have been updated as well:

- The Trash folder now displays an 'Empty Trash' button.
- A 'Show Target' menu item has been added to help finding symlink targets.
- Git integration has been enhanced, as the context menu for git folders now displays two new actions for 'git log' and 'git merge'.

Further improvements include:

- A new shortcut has been introduced giving you the option to open the Filter Bar simply by hitting the slash (/) key.
- You can now sort and organize photos by the date they were taken.
- The drag-and-drop of many small files within Dolphin became faster, and users can now undo batch rename jobs.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

{{% i18n_var "To make working on the command line even more enjoyable, <a href='%[1]s'>Konsole</a>, KDE's terminal emulator application, can now look prettier:" "https://www.kde.org/applications/system/konsole/" %}}

- You can download color schemes via KNewStuff.
- The scrollbar blends in better with the active color scheme.
- By default the tab bar is shown only when needed.

Konsole's contributors did not stop there, and introduced many new features:

- A new read-only mode and a profile property to toggle copying text as HTML have been added.
- Under Wayland, Konsole now supports the drag-and-drop menu.
- Several improvements took place in regards to the ZMODEM protocol: Konsole can now handle the zmodem upload indicator B01, it will show the progress while transferring data, the Cancel button in the dialog now works as it should, and transferring of bigger files is better supported by reading them into memory in 1MB chunks.

Further improvements include:

- Mouse wheel scrolling with libinput has been fixed, and cycling through shell history when scrolling with the mouse wheel is now prevented.
- Searches are refreshed after changing the search match regular expression option and when pressing 'Ctrl' + 'Backspace' Konsole will match xterm behaviour.
- The '--background-mode' shortcut has been fixed.

### Multimedia

{{% i18n_var "<a href='%[1]s'>JuK</a>, KDE's music player, now has Wayland support. New UI features include the ability to hide the menu bar and having a visual indication of the currently playing track. While docking to the system tray is disabled, JuK will no longer crash when attempting to quit via the window 'close' icon and the user interface will remain visible. Bugs regarding JuK autoplaying unexpectedly when resuming from sleep in Plasma 5 and handling the playlist column have also been fixed." "https://juk.kde.org/" %}}

{{% i18n_var "For the 18.04 release, contributors of <a href='%[1]s'>Kdenlive</a>, KDE's non-linear video editor, focused on maintenance:" "https://kdenlive.org/" %}}

- Clip resizing no longer corrupts fades and keyframes.
- Users of display scaling on HiDPI monitors can enjoy crisper icons.
- A possible crash on startup with some configurations has been fixed.
- MLT is now required to have version 6.6.0 or later, and compatibility is improved.

#### Graphics

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

{{% i18n_var "Over the last months, contributors of <a href='%[1]s'>Gwenview</a>, KDE's image viewer and organizer, worked on a plethora of improvements. Highlights include:" "https://www.kde.org/applications/graphics/gwenview/" %}}

- Support for MPRIS controllers has been added so you can now control full screen slideshows via KDE Connect, your keyboard's media keys, and the Media Player plasmoid.
- The thumbnail hover buttons can now be turned off.
- The Crop tool received several enhancements, as its settings are now remembered when switching to another image, the shape of the selection box can now be locked by holding down the 'Shift' or 'Ctrl' keys and it can also be locked to the aspect ratio of the currently displayed image.
- In full screen mode, you can now exit by using the 'Escape' key and the color palette will reflect your active color theme. If you quit Gwenview in this mode, this setting will be remembered and the new session will start in full screen as well.

Attention to detail is important, so Gwenview has been polished in the following areas:

- Gwenview will display more human-readable file paths in the 'Recent Folders' list, show the proper context menu for items in the 'Recent Files' list, and correctly forget everything when using the 'Disable History' feature.
- Clicking on a folder in the sidebar allows toggling between Browse and View modes and remembers the last used mode when switching between folders, thus allowing faster navigation in huge image collections.
- Keyboard navigation has been further streamlined by properly indicating keyboard focus in Browse mode.
- The 'Fit Width' feature has been replaced with a more generalized 'Fill' function.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Even smaller enhancements often can make user's workflows more enjoyable:

- To improve consistency, SVG images are now enlarged like all other images when 'Image View → Enlarge Smaller Images' is turned on.
- After editing an image or undoing changes, image view and thumbnail won't get out of sync anymore.
- When renaming images, the filename extension will be unselected by default and the 'Move/Copy/Link' dialog now defaults to showing the current folder.
- Lots of visual papercuts were fixed, e.g. in the URL bar, for the full screen toolbar, and for the thumbnail tooltip animations. Missing icons were added as well.
- Last but not least, the full screen hover button will directly view the image instead of showing the folder's content, and the advanced settings now allow for more control over the ICC color rendering intent.

#### Office

{{% i18n_var "In <a href='%[1]s'>Okular</a>, KDE's universal document viewer, PDF rendering and text extraction can now be cancelled if you have poppler version 0.63 or higher, which means that if you have a complex PDF file and you change the zoom while it's rendering it will cancel immediately instead of waiting for the render to finish." "https://www.kde.org/applications/graphics/okular/" %}}

You will find improved PDF JavaScript support for AFSimple_Calculate, and if you have poppler version 0.64 or higher, Okular will support PDF JavaScript changes in read-only state of forms.

{{% i18n_var "Management of booking confirmation emails in <a href='%[1]s'>KMail</a>, KDE's powerful email client, has been significantly enhanced to support train bookings and uses a Wikidata-based airport database for showing flights with correct timezone information. To make things easier for you, a new extractor has been implemented for emails not containing structured booking data." "https://www.kde.org/applications/internet/kmail/" %}}

Further improvements include:

- The message structure can again be shown with the new 'Expert' plugin.
- A plugin has been added in the Sieve Editor for selecting emails from the Akonadi database.
- The text search in the editor has been improved with regular expression support.

#### Utilities

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

{{% i18n_var "Improving the user interface of <a href='%[1]s'>Spectacle</a>, KDE's versatile screenshot tool, was a major focus area:" "https://www.kde.org/applications/graphics/spectacle/" %}}

- The bottom row of buttons has received an overhaul, and now displays a button to open the Settings window and a new 'Tools' button that reveals methods to open the last-used screenshot folder and launch a screen recording program.
- The last-used save mode is now remembered by default.
- The window size now adapts to the aspect ratio of the screenshot, resulting in a more pleasant and space-efficient screenshot thumbnail.
- The Settings window has been substantially simplified.

In addition, users will be able to simplify their workflows with these new features:

- When a specific window has been captured, its title can be automatically added to the name of the screenshot file.
- The user can now choose whether or not Spectacle automatically quits after any save or copy operation.

Important bug fixes include:

- Drag-and-drop to Chromium windows now works as expected.
- Repeatedly using the keyboard shortcuts to save a screenshot does not result in an ambiguous shortcut warning dialog anymore.
- For rectangular region screenshots, the bottom edge of the selection can be adjusted more accurately.
- Improved reliability of capturing windows touching screen edges when compositing is turned off.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

{{% i18n_var "With <a href='%[1]s'>Kleopatra</a>, KDE's certificate manager and universal crypto GUI, Curve 25519 EdDSA keys can be generated when used with a recent version of GnuPG. A 'Notepad' view has been added for text based crypto actions and you can now sign/encrypt and decrypt/verify directly in the application. Under the 'Certificate details' view you will now find an export action, which you can use to export as text to copy and paste. What's more, you can import the result via the new 'Notepad' view." "https://www.kde.org/applications/utilities/kleopatra/" %}}

{{% i18n_var "In <a href='%[1]s'>Ark</a>, KDE's graphical file compression/decompression tool with support for multiple formats, it is now possible to stop compressions or extractions while using the libzip backend for ZIP archives." "https://www.kde.org/applications/utilities/ark/" %}}

### Applications joining the KDE Applications release schedule

{{% i18n_var "KDE's webcam recorder <a href='%[1]s'>Kamoso</a> and backup program <a href='%[2]s'>KBackup</a> will now follow the Applications releases. The instant messenger <a href='%[3]s'>Kopete</a> is also being reintroduced after being ported to KDE Frameworks 5." "https://userbase.kde.org/Kamoso" "https://www.kde.org/applications/utilities/kbackup/" "https://www.kde.org/applications/internet/kopete/" %}}

### Applications moving to their own release schedule

{{% i18n_var "The hex editor <a href='%[1]s'>Okteta</a> will have its own release schedule after a request from its maintainer." "https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore" %}}

### Bug Stomping

More than 170 bugs have been resolved in applications including the Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello and more!

### Full Changelog
