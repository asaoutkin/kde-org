---
title: Release Service 20.12.1 Full Log Page
type: fulllog
version: 20.12.1
release: true
releaseDate: 2021-01
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ NotificationSubscriber: fix regression introduced by my commit 61ae4984. [Commit.](http://commits.kde.org/akonadi/883d991d4a16f48ee26c5fe44181c9b05c7740cb) 
{{< /details >}}
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Calendarbase.cpp - handleUidChange(): reload memory calendar. [Commit.](http://commits.kde.org/akonadi-calendar/e07ddead6a46d60420aba118562c857c8d6a1e18) 
{{< /details >}}
{{< details id="akonadi-contacts" title="akonadi-contacts" link="https://commits.kde.org/akonadi-contacts" >}}
+ Fix msvc build on https://build.kde.org. [Commit.](http://commits.kde.org/akonadi-contacts/a636dab45855dd86c5ddd1659afbe6e2cf38e887) 
+ Fix Bug 430119 - Confirm add address on OK. [Commit.](http://commits.kde.org/akonadi-contacts/ca8ef1ce945f31a2f11f32b2ea91dc5fabc03425) Fixes bug [#430119](https://bugs.kde.org/430119)
+ Activate signal/slot. [Commit.](http://commits.kde.org/akonadi-contacts/5752645a2f704ae46571631f4f7644a58609ce8c) 
+ Continue to implement "check if address is created". [Commit.](http://commits.kde.org/akonadi-contacts/214712fda13e4b2860cbf5a085f1a16cca5a5f5a) 
+ Start to fix 430119. [Commit.](http://commits.kde.org/akonadi-contacts/5bd2d4d3c778047b4690eb3970a3f16098500a46) See bug [#430119](https://bugs.kde.org/430119)
{{< /details >}}
{{< details id="akregator" title="akregator" link="https://commits.kde.org/akregator" >}}
+ Remove setRunExecutables(true), it serves no purpose with a text/html mimetype. [Commit.](http://commits.kde.org/akregator/3d1a994588825d72fa2ed8a9925d7e9b34e9d1eb) 
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Added 44px, 150px and 310px versions of Cantor's icon. Required for the. [Commit.](http://commits.kde.org/cantor/8d9109b0c30e0cb278c6096accb12461c53c6258) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix vertical scroll with horizontal component not being recognized properly. [Commit.](http://commits.kde.org/dolphin/303740c68af83f45be1bdea56a250397a83efa6a) Fixes bug [#430532](https://bugs.kde.org/430532)
+ DolphinContextMenu: drop wrong assert. [Commit.](http://commits.kde.org/dolphin/125c7aba09cea4ce0f7a1e0106e4eec0f1aa8c9f) 
+ Fix crash when device with capacitybar is dragged. [Commit.](http://commits.kde.org/dolphin/ef093154046f91a59bc7e0fe1f22b590f45a0b0b) Fixes bug [#430441](https://bugs.kde.org/430441)
+ Use setShowOpenOrExecuteDialog(true) with OpenUrlJob. [Commit.](http://commits.kde.org/dolphin/c03b43b4a1132ca7bc6db4a1583bc8bd1578b44f) 
+ Fix access url navigator while creating new tab in filename search view. [Commit.](http://commits.kde.org/dolphin/c95d7fae79a309ed7f2df393fa20257cb20d54a8) Fixes bug [#429628](https://bugs.kde.org/429628). Fixes bug [#430434](https://bugs.kde.org/430434)
+ Fix KIO warning when URL is empty. [Commit.](http://commits.kde.org/dolphin/da636bf513dcd15fbf7d8be13dd9315ed73311e9) 
+ Servicemenuinstaller: Remove file if it already exists. [Commit.](http://commits.kde.org/dolphin/a4a7b5e8af46a8dd44b08fae497cec73bc73579f) 
+ Fix PlacesItemModelTest, 2nd try. [Commit.](http://commits.kde.org/dolphin/c453393d357f094bc48d61061bf818cfe9d1c4ec) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Do not mishandle the force file system option. [Commit.](http://commits.kde.org/elisa/a90eeb04729656804a472f071874649b70223311) Fixes bug [#424901](https://bugs.kde.org/424901)
+ Always enable the remove button for playlist entries. [Commit.](http://commits.kde.org/elisa/e995cd2d602a71eb0ebac954c327bbf738c80a77) 
+ When adding a directory from files view, follow sort order. [Commit.](http://commits.kde.org/elisa/22324f49b3010e18a642257a9254976229045a42) 
+ Properly build URLs for local files that may have special characters. [Commit.](http://commits.kde.org/elisa/a4bc625654590eaa9d492d29c29faecf0f7446f2) 
+ Properly enqueue files from file browser to playlist. [Commit.](http://commits.kde.org/elisa/710275fd7f7efc977f542f59061a0599a089b8e7) Fixes bug [#429465](https://bugs.kde.org/429465)
+ Clean dead signal in ViewManager. [Commit.](http://commits.kde.org/elisa/da6e97819d3536cc43130212101598cc87601caa) 
+ Add some categorized debug output in player control code. [Commit.](http://commits.kde.org/elisa/ef6edac69f3ef2322411bbc6479b21d3c84e0d5c) 
+ Remove unused code in MPRIS interface class. [Commit.](http://commits.kde.org/elisa/9f9a2d1f0fb4b0bf262e55408aed37d77e46f73d) 
+ Emit Seeked on MPRIS2 interface when player really seeked. [Commit.](http://commits.kde.org/elisa/8315d92e3add2e6f8db0b0f6bccfb1f98c0f9515) Fixes bug [#428957](https://bugs.kde.org/428957)
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Revert merge request 14. [Commit.](http://commits.kde.org/eventviews/64ed63fcd25a18924b396dd358daf60762b6a929) 
+ Make To-do List popup menu match other menus. [Commit.](http://commits.kde.org/eventviews/e24f5e6171a0354bb4badf634bf7cb53cc9c4119) 
+ Agendaviews: Display recurrence w/o main incidence. [Commit.](http://commits.kde.org/eventviews/8e354e8524605f8db3aa5ad5134413da53c65e9b) 
+ Incidencetreemodel: Improve debug ouput. [Commit.](http://commits.kde.org/eventviews/74bff6a9d7341c51f65545f69a90882c6945262e) 
+ Fix display of multi-day events from non-local time zones. [Commit.](http://commits.kde.org/eventviews/710794ae1e3d747d36ac6ac559aaa6f2f063e02b) Fixes bug [#429007](https://bugs.kde.org/429007)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Do not restore the JPG quality after a "Save" operation. [Commit.](http://commits.kde.org/gwenview/1e309832d203bcb9f3dad4d2d8ff4e0e4e182ea9) 
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Handle changes to start dates of recurring to-dos. [Commit.](http://commits.kde.org/incidenceeditor/5c9c45bb8dadaa113c88ddf863eb8460df8f43c3) Fixes bug [#430651](https://bugs.kde.org/430651)
+ Fix off-by-one error in yearly recurrence rule pull-down. [Commit.](http://commits.kde.org/incidenceeditor/3eef1ba70b006dae2f1342dcaf54d4ebc4702824) 
+ IncidenceAttachment: don't allow running executables. [Commit.](http://commits.kde.org/incidenceeditor/2d53e5593fac79c7ddbe4ff6036bc4830302e55b) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Properly handle journey section begins/ends when setting platform names. [Commit.](http://commits.kde.org/itinerary/f0722277110e752cd16c9414fc39a89ff6e9f893) 
+ Make tests independent of locale configuration. [Commit.](http://commits.kde.org/itinerary/74e54bc8b73d48bb769f998a60dae7d11c58f297) 
+ Align transfer and journey query pages. [Commit.](http://commits.kde.org/itinerary/945f832d9f37e94c60fa940b06f40cc389622a3a) 
+ Port timeline delegates away from QtQuick layouts. [Commit.](http://commits.kde.org/itinerary/83544ce1cf0424dc3b5c21920e6b30841aa39397) 
+ Explicitly anchor address label to the left. [Commit.](http://commits.kde.org/itinerary/70821b3be79a737948735d50cf1edf9abe33a119) 
+ Fix opening the indoor map for elements that aren't location changes. [Commit.](http://commits.kde.org/itinerary/5d538009cf0e8b7d0e25d402e4404923491deb8e) 
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Fix infinite loop when clearing a DVD Video project. [Commit.](http://commits.kde.org/k3b/e116121108ced2136cc31492e4661eec41071916) Fixes bug [#259164](https://bugs.kde.org/259164)
{{< /details >}}
{{< details id="kaccounts-integration" title="kaccounts-integration" link="https://commits.kde.org/kaccounts-integration" >}}
+ Load translations. [Commit.](http://commits.kde.org/kaccounts-integration/c52677d68bfbf8203b376c9fd2fba18913403690) 
{{< /details >}}
{{< details id="kaccounts-providers" title="kaccounts-providers" link="https://commits.kde.org/kaccounts-providers" >}}
+ [*cloud] Don't manually close the window when finished. [Commit.](http://commits.kde.org/kaccounts-providers/f36a78d121d9ee0ddc51c7d6d182143e8721e035) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Alter priority for removing duplicate Akonadi resources. [Commit.](http://commits.kde.org/kalarm/4aca027e7b7456a430bb05c6d4ad306c2fda49fa) 
+ Remove unused mailcommon dependency. [Commit.](http://commits.kde.org/kalarm/4658689a24be5e6a8777cd4ae9bbb197f4fb885f) 
+ Revert accidentally committed change. [Commit.](http://commits.kde.org/kalarm/be00192bbf897e9674ff78d54c7f1f17cbbda200) 
+ Show correct alarm columns in main window on first run. [Commit.](http://commits.kde.org/kalarm/f32c26fa99b3dc9994657b75d201bf663aa028c5) 
+ Ensure that build uses file resource option. [Commit.](http://commits.kde.org/kalarm/b0a3f2a50d94abec73c8cba726b5c3a3f809ea15) 
{{< /details >}}
{{< details id="kalarmcal" title="kalarmcal" link="https://commits.kde.org/kalarmcal" >}}
+ Fix RFC3339 fractional seconds handling. [Commit.](http://commits.kde.org/kalarmcal/163e7d0d9d72c5e72a2ef337a6884bbf5c443804) 
{{< /details >}}
{{< details id="kamoso" title="kamoso" link="https://commits.kde.org/kamoso" >}}
+ Highlight the border of the selected effect. [Commit.](http://commits.kde.org/kamoso/5c51e23b705193fb84184653f301cffa468c8026) 
{{< /details >}}
{{< details id="kcalutils" title="kcalutils" link="https://commits.kde.org/kcalutils" >}}
+ Use consistent icons for events and to-dos. [Commit.](http://commits.kde.org/kcalutils/a336183a869dd372ab1b1844bf17f203c94337a5) 
+ Simplify invitation style. [Commit.](http://commits.kde.org/kcalutils/fb29a666e73714dabe26cab8352d4ba4b55b4200) 
+ Fix autotests/testincidenceformatter. [Commit.](http://commits.kde.org/kcalutils/d21fd9500f351d5d84991f11a00c81b0cd04fccf) 
+ Fix autotests/testtodotooltip. [Commit.](http://commits.kde.org/kcalutils/f5a456ac261551341704d78a5176a6ce146af10c) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Plugins/battery: add battery charge info to the update signal. [Commit.](http://commits.kde.org/kdeconnect-kde/fce8bc1e53e1ff30b4bf24febb373f4da10345b5) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix crash on copy subtitle (not implemented yet). [Commit.](http://commits.kde.org/kdenlive/e6d6934ff907eeaf678226812afaabf71479458f) 
+ Ensure jobs for timeline clips/tracks are properly canceled when the clip/track is deleted, fix crash on audio align deleted clip. [Commit.](http://commits.kde.org/kdenlive/fd20f747b46ab525b45c784f2027214a43f2d642) 
+ Fix crash if the clip of an audio align job is deleted during calculations. [Commit.](http://commits.kde.org/kdenlive/0c2a2eaec2442e60459c0ec676fa8ef0e2e563ad) 
+ Fix possible crash dragging clip in timeline from a file manager. [Commit.](http://commits.kde.org/kdenlive/8c53fd0f9b229f9ac1953c12296db61076733102) 
+ Various display adjustments for compositions and clips. [Commit.](http://commits.kde.org/kdenlive/02c7aef3a102bbad199fbcfed35f9f3d9eeee195) 
+ Reset config should also delete xmlui config file. [Commit.](http://commits.kde.org/kdenlive/6e5a4534ce3b9586d25db3056dccfe95c9e3a7a1) 
+ Fix disabling proxy loses some clip properties. [Commit.](http://commits.kde.org/kdenlive/b365d6c7e5f0f4cfb0cb7f285b0c8f9403800280) 
+ Fix tests. [Commit.](http://commits.kde.org/kdenlive/338ef95354cc6552f1fa5642fdb3874e7a0d9cf3) 
+ Fix some regressions in keyframe move. [Commit.](http://commits.kde.org/kdenlive/687dea161c072b5d173fcd769148f48b91358aba) 
+ Undo/redo on clip monitor set in/out point. [Commit.](http://commits.kde.org/kdenlive/8f6fe0ce4cdf35c29cc766cc394ce3c11d134abf) 
+ Don't snap on subtitles when track is hidden. [Commit.](http://commits.kde.org/kdenlive/f265f7bbc3914c555f84cc882cd9763af6238f1b) 
+ Add option to delete all effects in selected clip/s. [Commit.](http://commits.kde.org/kdenlive/daf52f0c521acbdeea7f1d90499ee37a35806db0) 
+ Fix some more xml parameters by Eugen Mohr. [Commit.](http://commits.kde.org/kdenlive/109a0954e55094c5c216382101241484bba1bc7a) 
+ Fix crash when all audio streams of a clip were disabled. [Commit.](http://commits.kde.org/kdenlive/23f256c77db23a3d63db3ea792b54ea4353f0a37) Fixes bug [#429997](https://bugs.kde.org/429997)
+ Fix some broken effects descriptions, spotted by Eugen Mohr. [Commit.](http://commits.kde.org/kdenlive/24113cfedd650fe7e6ede5c31a186a3012b19cc5) 
+ Reduce latency on forwards/backwards play. [Commit.](http://commits.kde.org/kdenlive/7e314223b97afb4ce646fbb07e46370198f73324) 
+ Fix the integer value of effect parameter's checkbox. Fixes #880. [Commit.](http://commits.kde.org/kdenlive/e8a3af8121a1f98d4811fb5ed945cb74e2557d89) 
+ Fix various typos spotted by Kunda Ki. [Commit.](http://commits.kde.org/kdenlive/237ae40b1f1d27d9fdf0c0ab119cef5ae88d7474) 
+ Automatically update title clip name when we edit a duplicate title. [Commit.](http://commits.kde.org/kdenlive/a15c84131903476db4200d134340681c14ca90dc) 
+ Add option to not pause the playback while seeking. [Commit.](http://commits.kde.org/kdenlive/ebaf8d94ec5d3f07978c3da6fd2839192aa842d1) 
+ Fix some crashes with locked subtitle track. [Commit.](http://commits.kde.org/kdenlive/ec0ff97c478385111d32515d47f8b4160911ed4c) 
+ Fix qml deprecation warning. [Commit.](http://commits.kde.org/kdenlive/e21a4d11469965c1d6f06461834f12eea289a817) 
+ Fix track effects applying only on first playlist. [Commit.](http://commits.kde.org/kdenlive/4fdb0382395d8bd34b91a791f221fe80e15fca9a) 
+ Fix timeline vertical scrolling too fast. [Commit.](http://commits.kde.org/kdenlive/de46c150025f38e30426e11b51f5ddca3aa9e514) 
+ Fix clip move incorrectly rejected. [Commit.](http://commits.kde.org/kdenlive/7623b16b435f0e79195ef2c668c0b76934c5ff14) 
+ Fix regression with crash in effect stack. [Commit.](http://commits.kde.org/kdenlive/8199a9886f32bdeeef14078e7156fc202656e6d1) 
+ Add preliminary support to copy a keyframe param value to other selected keyframes. [Commit.](http://commits.kde.org/kdenlive/9afc4a5549b78f76405933fc2b3b9ce00973570b) 
+ Move timeline tooltips in statusbar. [Commit.](http://commits.kde.org/kdenlive/ee1d1a970a2696b11330fab96ffc2432d765da62) 
+ Add normalizers to MLT thumbcreator, fixing Kdeinit crash. [Commit.](http://commits.kde.org/kdenlive/a2cb2600670142c647485e2a3f1e06d7199d0b7a) See bug [#430122](https://bugs.kde.org/430122)
+ Effectstack: Add duplicate keyframe(s) button. [Commit.](http://commits.kde.org/kdenlive/2d6f0afeee138fd58d777840dea2227b78fcd906) 
+ Effectstack: select multiple keyframes by shift-click + drag (like in timeline). [Commit.](http://commits.kde.org/kdenlive/eb7c898dd2f39a21ab67760daef8acbf56a722c7) 
+ Improve grabbing of keyframes in effect stack. [Commit.](http://commits.kde.org/kdenlive/59cac4915c126525056d164a96b5f64f06dd4f9d) 
+ Initial implementation of grouped keyframe operation (move/delete). Select multiple keyframes with CTRL+click. [Commit.](http://commits.kde.org/kdenlive/0edef2b0b52a7353354187aacec3e4c3bcf89af0) 
+ When calculating a folder hash (to find a missing slideshow), take into accound the file hash of 2 files inside the folder. [Commit.](http://commits.kde.org/kdenlive/b89f854bf13fb7a2db9f2c9f85b0cf1a54862761) 
+ Ensure subtitle track buttons are hidden when the track is hidden. [Commit.](http://commits.kde.org/kdenlive/1d722f44a488e755bacd047501184a86f342a96b) 
+ Fix project profile creation dialog not updating properties on profile selection. [Commit.](http://commits.kde.org/kdenlive/da8d326a27db5bdfd16e66a5be73de67d544a6c0) 
+ Don't change Bin horizontal scrolling when focusing an item. [Commit.](http://commits.kde.org/kdenlive/901014fc1cdc67205d9d6a733e27e908488b071a) 
+ Fix composition unselected on move. [Commit.](http://commits.kde.org/kdenlive/c8739be233b99c70c0d88d45df25541e2daabbfa) 
+ Fix unwanted keyframe move on keyframe widget seek. [Commit.](http://commits.kde.org/kdenlive/177a89746f44311b2ae3a71b50f71d6424160e0c) 
+ Don't snap on subtitles when locked. [Commit.](http://commits.kde.org/kdenlive/b558390f6e6f2a56053f99ace532e5c1640b8b39) 
+ Show/lock subtitle track now correctly uses undo/redo. [Commit.](http://commits.kde.org/kdenlive/0fe2d82e2d204dc7f68c736f982fba6c1ce52a6e) 
+ Restor subtitle track state (hidden/locked) on project opening. [Commit.](http://commits.kde.org/kdenlive/263af4bfacbb30a92a02cdad9d195bf9bce3f466) 
+ Fix qmlt typo. [Commit.](http://commits.kde.org/kdenlive/8d33fe6381538beeb279b14f5b59bf867fc0bc54) 
+ Fix color picker offset, live preview of picked color in the button. [Commit.](http://commits.kde.org/kdenlive/cb718f860f29007af594d612609a8174c7032b5a) 
+ Implement subtitle track lock. [Commit.](http://commits.kde.org/kdenlive/49f477047dfb031ab08b209d5b5c16aa4de8bfdf) 
+ Add hide and lock (in progress) of subtitle track. [Commit.](http://commits.kde.org/kdenlive/bd13f3b8627714ccd5c75453d790442b0cf0f91e) 
+ Zoom effect keyframe on CTRL + wheel, add option to move selected keyframe to current cursor position. [Commit.](http://commits.kde.org/kdenlive/8d53e900326b290608dcdaa0e7d59227755c6eaa) 
+ Add "unused clip" filter in Project Bin. [Commit.](http://commits.kde.org/kdenlive/026a554ebe2e53ab30bd6f09f7fab153398c1bd0) Fixes bug [#430035](https://bugs.kde.org/430035)
+ Another small fix for image sequence on project opening. [Commit.](http://commits.kde.org/kdenlive/1f05e18a1949adac57f8be6db4a8e63e97ddf3a0) 
{{< /details >}}
{{< details id="kfourinline" title="kfourinline" link="https://commits.kde.org/kfourinline" >}}
+ Fix broken load/save due to use of outdated ":<kwin4>". [Commit.](http://commits.kde.org/kfourinline/d3633c7b8dbd5f5b55bde163e12656f5fb1ac686) 
{{< /details >}}
{{< details id="kget" title="kget" link="https://commits.kde.org/kget" >}}
+ Org.kde.kget.appdata.xml: add <content_rating>. [Commit.](http://commits.kde.org/kget/838baac9716565d4071cc880deaa06c58413bf88) 
{{< /details >}}
{{< details id="kig" title="kig" link="https://commits.kde.org/kig" >}}
+ Scripting: PyEval_CallObject -> PyObject_CallObject. [Commit.](http://commits.kde.org/kig/7f13da34997f1d4bbec19e857a89325fe66964d1) 
+ Scripting: fix Python initialization. [Commit.](http://commits.kde.org/kig/62e2c9b9ac2a88081f86c46b14f82e63f21c7e2d) 
+ Prevent  Deletion of Option Dialog. [Commit.](http://commits.kde.org/kig/8ea5219728cc6e727329a514f69ca69bca9f47db) Fixes bug [#422665](https://bugs.kde.org/422665)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Handle yet another TER ticket variant. [Commit.](http://commits.kde.org/kitinerary/ccec0ce9e5e86c8b9400aab809597098887eaae5) 
{{< /details >}}
{{< details id="klickety" title="klickety" link="https://commits.kde.org/klickety" >}}
+ Fix broken connection from "New" action to newGame slot. [Commit.](http://commits.kde.org/klickety/3d178427de7c275f3958d0f586bb869ff24401c4) Fixes bug [#430362](https://bugs.kde.org/430362)
{{< /details >}}
{{< details id="klines" title="klines" link="https://commits.kde.org/klines" >}}
+ Fix keyboard navigation focus not being shown. [Commit.](http://commits.kde.org/klines/6211dc0d6bc905101e59cf08e44e8382f5a5a1c4) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Correct columns size with highlightScrolledLines. [Commit.](http://commits.kde.org/konsole/457e590c26add6edafcae4a6c10d1d9277ed4818) Fixes bug [#429600](https://bugs.kde.org/429600)
+ Only link konsoleprofile to konsoleprivate. [Commit.](http://commits.kde.org/konsole/3f2b2d9df4ca74baa0d263c36dc70d95c149b221) Fixes bug [#430492](https://bugs.kde.org/430492)
+ Revert 'Fix bold character color paint'. [Commit.](http://commits.kde.org/konsole/08aa65e32427f8dc49fbfd6b1d8997431b72dab2) 
{{< /details >}}
{{< details id="kontrast" title="kontrast" link="https://commits.kde.org/kontrast" >}}
+ Release service versionning change. [Commit.](http://commits.kde.org/kontrast/ceed61db75a0e6621e76dd152bd2322f354cddaa) 
+ Fix favorite page layout on mobile. [Commit.](http://commits.kde.org/kontrast/e1f9fcd7735a069b838e7baba97c6ddb1a6ca390) 
+ Fix broken sql model. [Commit.](http://commits.kde.org/kontrast/a8169eec73d6fa25c16b4cef9c06ebca0763bb84) 
+ Add correct version in AppStream File. [Commit.](http://commits.kde.org/kontrast/b981903f109b3f4219265d9183ce70ea75a307ff) 
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ Fix shell scripts to also find qdbus-qt5. [Commit.](http://commits.kde.org/konversation/ca6b67df7ee1ee53a367e4fa73f2b6bcb06260c3) 
+ Handle \ in nicks when opening new query in ircview. [Commit.](http://commits.kde.org/konversation/eb63cf307ec9e1de4b1a09835fd36e57c189df68) Fixes bug [#431045](https://bugs.kde.org/431045)
+ Use SSL for the server created by default. [Commit.](http://commits.kde.org/konversation/aa0bfea0d0163d6d7f532cf6ea6dd7d982959c4d) 
+ Don't allow spaces in ident and nick inputs. [Commit.](http://commits.kde.org/konversation/47733e88b76822f61cd9fcc6ff0434ebf6223823) Fixes bug [#422146](https://bugs.kde.org/422146)
+ Fix missed handling of a irc:// url cmdl arg on app start. [Commit.](http://commits.kde.org/konversation/ea484d1b6042c593c3da247a2449f2ad9e37c629) Fixes bug [#391698](https://bugs.kde.org/391698)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Recognize selection of different dates of a recurring incidence. [Commit.](http://commits.kde.org/korganizer/5738c83ff002a662b797478385ed8cd9abdd9e64) 
+ Use the same icons in the Actions menu as are used in the pop-up menu. [Commit.](http://commits.kde.org/korganizer/37506deb2bd6af5ead9ebd00befc08af9db1b759) 
+ Do not double-delete. [Commit.](http://commits.kde.org/korganizer/07353f82fc9095a21479c26fb84159372dacf1fa) 
+ Collectionview: Show correct tooltip. [Commit.](http://commits.kde.org/korganizer/5ed6f0a7bba99562415860678fc8f70e6b93682f) 
{{< /details >}}
{{< details id="kpat" title="kpat" link="https://commits.kde.org/kpat" >}}
+ Restore KPat Theme downloads. [Commit.](http://commits.kde.org/kpat/6cf28db18487aed808187540dfe8ee9124088f96) 
{{< /details >}}
{{< details id="kspaceduel" title="kspaceduel" link="https://commits.kde.org/kspaceduel" >}}
+ Fix auto-rotating spaceships. [Commit.](http://commits.kde.org/kspaceduel/2466e364aea117658ea8344c008fc7a7a26fabe2) 
{{< /details >}}
{{< details id="ktorrent" title="ktorrent" link="https://commits.kde.org/ktorrent" >}}
+ Fix a bug in torrent content filtering. [Commit.](http://commits.kde.org/ktorrent/e9edd771f5231aba455e6ebe87ed30e61f1104ff) 
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ Don't install Forward header KExtHighscore for no longer existing header. [Commit.](http://commits.kde.org/libkdegames/06b3b64c8641b7ff5c3f3909008927ca7c5061c6) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Add Protected Header tests for SignEncryptJob. [Commit.](http://commits.kde.org/messagelib/d6c60a78808383ac25be735100a8322b57a7369f) 
+ Messagecomposer: Move protected headers to signed part. [Commit.](http://commits.kde.org/messagelib/87bc9316422ac2293fabbe88f64a59fb251dd5bd) 
+ Fix[messagecomposer]: Do copy all mail headers instad of reference them. [Commit.](http://commits.kde.org/messagelib/3a7114399b105cbe159355f600b9d3e08ec10fcb) Fixes bug [#427091](https://bugs.kde.org/427091)
+ Fix Bug 431153 - Crash in MessageComposer::MessageFactoryNG::createForwardDigestMIME() when trying to forward a group of emails. [Commit.](http://commits.kde.org/messagelib/3ab9029f79f1eb0d6dd99b262edcbcc8f21ff57b) Fixes bug [#431153](https://bugs.kde.org/431153)
+ Storagemodel.cpp - collectionForId() improve. [Commit.](http://commits.kde.org/messagelib/92b5dda0d1f4148a9372df01d40d8bbeedcc09ba) 
+ Remove setRunExecutables(true), it serves no purpose with a text/html mimetype. [Commit.](http://commits.kde.org/messagelib/4eee60f210d82bfdee1cae728710085fb7e7c9b5) 
+ Cleanup EncryptedJobTest. [Commit.](http://commits.kde.org/messagelib/369d185032d0e88422ec26894d8ef65779a16e84) 
+ FIX[messagecomposer/cryptocomposertests]: Use EncryptJob exec instead of async variant to pass tests. [Commit.](http://commits.kde.org/messagelib/f600fba6345f98bc39f404e06713af4e8a5de8d3) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix opening markdown files. [Commit.](http://commits.kde.org/okular/9e23338b002cd14d8a9ad191526a7950e72d97e9) Fixes bug [#430538](https://bugs.kde.org/430538)
+ Make search work on documents that describe Å as A + ◌̊. [Commit.](http://commits.kde.org/okular/d112682611cd38fe7f28943f688648b5862c7582) Fixes bug [#430243](https://bugs.kde.org/430243)
+ Fix crash with some broken PDF files. [Commit.](http://commits.kde.org/okular/3e86c9a56c0c312ac4f4eaf09b14edcb7651d1cc) 
+ Pdf: Fix small memory leak. [Commit.](http://commits.kde.org/okular/54209751b523f7d700e68774c05d6f86e6967571) 
+ Clang-tidy: Disable performance-no-automatic-move. [Commit.](http://commits.kde.org/okular/bc85160d7f2727b1911339a4bc1965f401d8fc2a) 
+ Make the CI pass with new clang-tidy. [Commit.](http://commits.kde.org/okular/d1c2eed1b6aaaa6f7416237ed148d23b10e47373) 
+ Document::openRelativeFile: Double check the url changed when we asked to. [Commit.](http://commits.kde.org/okular/4f2c2b27147b6ed410f5aaf68f351726e07822e4) Fixes bug [#429924](https://bugs.kde.org/429924)
{{< /details >}}
{{< details id="parley" title="parley" link="https://commits.kde.org/parley" >}}
+ Fix location for file open dialog. [Commit.](http://commits.kde.org/parley/45ac67f7adfc514122f5d31550e89a17447400a2) 
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Fix crash with old data (before my fixing about simple quote). [Commit.](http://commits.kde.org/pimcommon/96956b946ed94dd333b072000a8abe8824aadf1d) 
+ Add more autotest. [Commit.](http://commits.kde.org/pimcommon/2bae517191260dd21a185beacc419b36d78f0f7b) 
+ Add autotest for loading quote. [Commit.](http://commits.kde.org/pimcommon/6fc492e1e1d6325c29fb90e8df88503bc1457003) 
+ Start to implement loading autotest. [Commit.](http://commits.kde.org/pimcommon/190d60d63d1359da6b28db70c1be72b37d3f19db) 
+ Fix Bug 430345 - kmail suppress "apostrophe" in french. [Commit.](http://commits.kde.org/pimcommon/0faf03a521630f02ebafb72fa966c9152433c060) Fixes bug [#430345](https://bugs.kde.org/430345)
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Changed signal of Annotate button in main window from pressed to clicked in order to let annotation mode appear after button is released. [Commit.](http://commits.kde.org/spectacle/d1fd8b179762cbb9f0696fb66a676fe6ff551428) 
+ We do not need progress info when checking if a file exists. [Commit.](http://commits.kde.org/spectacle/647583a395195f60cf10de8bd1a6b77bc9447d6e) Fixes bug [#430173](https://bugs.kde.org/430173)
+ Remove dead code variant. [Commit.](http://commits.kde.org/spectacle/9d7d1a6438ffef19f18ba679efe0d13bfc944562) 
{{< /details >}}
{{< details id="svgpart" title="svgpart" link="https://commits.kde.org/svgpart" >}}
+ Add appdata file. [Commit.](http://commits.kde.org/svgpart/0862fa667215287d30528708c1cb34a0f19362b4) 
{{< /details >}}
