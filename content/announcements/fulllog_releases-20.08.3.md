---
release: true
releaseDate: 2020-11
title: Release Service 20.08.3 Full Log Page
type: fulllog
version: 20.08.3
hidden: true
---
{{< details id="akonadi" title="akonadi" href="https://commits.kde.org/akonadi" >}}
+ Bug 427358 - akonadi_ical_resource stuck in InvalideCacheForCollection. [Commit.](http://commits.kde.org/akonadi/b2e4ee76d5831df075349a5a597c371a4d9c1460) Fixes bug [#427358](https://bugs.kde.org/427358)
{{< /details >}}
{{< details id="akregator" title="akregator" href="https://commits.kde.org/akregator" >}}
+ Fix Bug 428449 - wrong tab order in "edit feed" dialog. [Commit.](http://commits.kde.org/akregator/35c18c72ce9f7cf5f5fad42211caceb1d3352dff) Fixes bug [#428449](https://bugs.kde.org/428449)
{{< /details >}}
{{< details id="gwenview" title="gwenview" href="https://commits.kde.org/gwenview" >}}
+ Fix typo in KIPI plugins URL. [Commit.](http://commits.kde.org/gwenview/888fa4274d95d7f92683db6d3c3a028fd2e17f27) Fixes bug [#421015](https://bugs.kde.org/421015)
+ Set visibility after setting parent. [Commit.](http://commits.kde.org/gwenview/e737554c9faff3be27083497c26675155bcf8122) Fixes bug [#427569](https://bugs.kde.org/427569)
{{< /details >}}
{{< details id="kaccounts-integration" title="kaccounts-integration" href="https://commits.kde.org/kaccounts-integration" >}}
+ [plugins/kio-webdav] Fix realm extraction. [Commit.](http://commits.kde.org/kaccounts-integration/7f03f218d5420769b62b7e7221795dbf935c5517) 
+ Fix account details page (especially on dark themes). [Commit.](http://commits.kde.org/kaccounts-integration/41b91d296cfa139ba1f7ff1f985ca2e9ec62160e) Fixes bug [#422190](https://bugs.kde.org/422190)
+ [kcm] Show account creation error in the UI. [Commit.](http://commits.kde.org/kaccounts-integration/afa18ab77a33a9237115f22a0b8eb6ecd37167cc) See bug [#420280](https://bugs.kde.org/420280)
{{< /details >}}
{{< details id="kalarm" title="kalarm" href="https://commits.kde.org/kalarm" >}}
+ Warn user if no active alarm calendar is enabled. [Commit.](http://commits.kde.org/kalarm/7672bae7358c48462e0ac6ea4264675b825ca54c) 
+ Bug 427722: Prevent resources being disabled on logout. [Commit.](http://commits.kde.org/kalarm/23eae65d7150bbc726d17092eacf2ad1f1d80f10) 
{{< /details >}}
{{< details id="kanagram" title="kanagram" href="https://commits.kde.org/kanagram" >}}
+ Drop unused Qt5OpenGL. [Commit.](http://commits.kde.org/kanagram/0b9ae7cc4ab5ebe6bd65833783ed4b4e5cc8cb38) 
{{< /details >}}
{{< details id="kate" title="kate" href="https://commits.kde.org/kate" >}}
+ Lspclient: only consider user configuration contents changes as change. [Commit.](http://commits.kde.org/kate/d02dc1f832ae46c96d72f45c33626a208e04d81f) Fixes bug [#427084](https://bugs.kde.org/427084)
+ Lspclient: correctly unregister the registered TextHintProvider. [Commit.](http://commits.kde.org/kate/1e7b6051a1de21ffa6d7281a5fd9e7771ee4a9f9) Fixes bug [#427338](https://bugs.kde.org/427338)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" href="https://commits.kde.org/kdeconnect-kde" >}}
+ Revert "[sms] Restore compatibility with current Android release". [Commit.](http://commits.kde.org/kdeconnect-kde/0f32462e260e1b75fcb54ea3eb434cc085517815) 
+ Set window icons for GUI executables. [Commit.](http://commits.kde.org/kdeconnect-kde/18031e97a909f5e285c10409c70edd723ef39e02) 
+ [sms] Restore compatibility with current Android release. [Commit.](http://commits.kde.org/kdeconnect-kde/4266834052983a788bb12a579da18fd35832d763) Fixes bug [#427266](https://bugs.kde.org/427266)
+ Work around SMS not being sent from cli. [Commit.](http://commits.kde.org/kdeconnect-kde/674e826bb60fc9fae104ec7bd5db031187a45b2d) 
+ Update kdeconnect-cli send-sms interface. This patch fixes the BUG: 425731. [Commit.](http://commits.kde.org/kdeconnect-kde/180c87353ba52b79a177943047d9de0d32605a17) 
+ Fix missing kcm dependency for runcommand module. [Commit.](http://commits.kde.org/kdeconnect-kde/795bb0434a08572a54339408d7fa2df56a0a4ccd) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" href="https://commits.kde.org/kdenlive" >}}
+ Fix on monitor displayed fps with high fps values. [Commit.](http://commits.kde.org/kdenlive/c29638fc49dc0d5fce695a02681f3743fc702d34) 
+ Ensure timeline ruler is correctly updated on profile switch. [Commit.](http://commits.kde.org/kdenlive/edef6318cad39932c0253cb1df621e814a63aa91) 
+ When switching project profile and there is only 1 clip in timeline, update the timeline clip duration accordingly to profile change. [Commit.](http://commits.kde.org/kdenlive/82dd806cd433cb735c0c4fadc758ba6c470ef11c) 
+ When switching project profile and there is only 1 clip in timeline, update the timeline clip duration accordingly to profile change. [Commit.](http://commits.kde.org/kdenlive/da8ea89891e117e796a608243b5551769b98aa93) 
+ Project archiving: check after each file if archiving works, add option to use zip instead of tar.gz. [Commit.](http://commits.kde.org/kdenlive/1989120b658c661cc34456e235cf77c367c17f8a) See bug [#421565](https://bugs.kde.org/421565)
+ Fix opening project files with missing version number. [Commit.](http://commits.kde.org/kdenlive/e5287c0df33acbd83778b7671dc162db2040c95d) See bug [#420494](https://bugs.kde.org/420494)
+ Fix duplicated audio from previous commit. [Commit.](http://commits.kde.org/kdenlive/c8fe98b33af714fcf721ee5f5dc5ba19327d697b) 
+ Fix playlist clips have no audio regression. [Commit.](http://commits.kde.org/kdenlive/1f9279333724b995a25235a985ff8335c55ca67d) 
+ Fix keyframeable effect params left enabled when selecting a clip, leading to possible crash. [Commit.](http://commits.kde.org/kdenlive/a4aef884d9ad7316b2232ac2cde9c3138954e621) 
+ Don't allow removing the only keyframe in an effect (was possible from the on monitor toolbar and crashing). [Commit.](http://commits.kde.org/kdenlive/cce8bcc4389ea7808f312eff80af01cc64d1f453) 
+ Fix crash inserting zone over grouped clips in same track. [Commit.](http://commits.kde.org/kdenlive/e2f9b806b8dc4ea9848ecc284c824285b07a6ac6) 
+ Fix previous commit. [Commit.](http://commits.kde.org/kdenlive/3b9595bd99b1d9e8b39322324b74eee7c3aab655) 
+ Check ffmpeg setting points to a file, not just isn't empty. [Commit.](http://commits.kde.org/kdenlive/b7ab436a3c66eff2aa090a46096c92e8dc33ed26) 
+ Qtcrop effect: make radius animated. [Commit.](http://commits.kde.org/kdenlive/f4f92defbee1b9914e718e64864c742a0ad364b9) 
+ Render widget: avoid misuse of parallel processing. [Commit.](http://commits.kde.org/kdenlive/a155503246c0bbe8f64c5332727a41d5c568dfbf) 
+ Fix resizing clip loses focus if mouse cursor did not get outside of clip boundaries. [Commit.](http://commits.kde.org/kdenlive/6ec0e30868a69bbd893864771f9f0090a8550a33) 
+ Fix rounding error sometimes hiding last keyframe in effectstack. [Commit.](http://commits.kde.org/kdenlive/86266b4ae27d8753e61cf977564f132537854482) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" href="https://commits.kde.org/kdepim-addons" >}}
+ Fix nullptr access on pkpass parsing failures. [Commit.](http://commits.kde.org/kdepim-addons/c40638b7e5f7a16733aa9f7d6d2daf25763c9619) Fixes bug [#428358](https://bugs.kde.org/428358)
+ Use KActionMenu and use setDelayed(false). [Commit.](http://commits.kde.org/kdepim-addons/0e83bb94b19f86a69c6f708b82b4f44451eda0fb) 
+ Add missing remove label. [Commit.](http://commits.kde.org/kdepim-addons/7a44eaaeee5d68783ae36693a53813160e26397c) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" href="https://commits.kde.org/kio-extras" >}}
+ Fish: Make makeTimeFromLs() actually take the strings into account. [Commit.](http://commits.kde.org/kio-extras/798d95824844af27dab617e28a501a1a384a6b21) 
+ Fish: Use the right result code on 'sh' command submission. [Commit.](http://commits.kde.org/kio-extras/e329876690eb3aa49227b0f18d5f856e02cfbf8e) 
+ Fish: Fix 'sh' implementations for LIST and STAT commands. [Commit.](http://commits.kde.org/kio-extras/f1346d827073c98e91c515d236c3c4f06bedde31) 
+ Fish: Wrap lines in 'sh' implementations for LIST and STAT commands. [Commit.](http://commits.kde.org/kio-extras/dae1c6f0834f71c00df7c03e56d246ff7a854ae3) 
+ Smb: no discovery cap on file listing. [Commit.](http://commits.kde.org/kio-extras/c58fdc63aeab10db329cd5a6167254543684c1a2) Fixes bug [#427644](https://bugs.kde.org/427644)
{{< /details >}}
{{< details id="kiten" title="kiten" href="https://commits.kde.org/kiten" >}}
+ Remove wrong and unneded i18n calls. [Commit.](http://commits.kde.org/kiten/2fdfdb6f20525dd3f9d679dad7d86261cf885cff) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" href="https://commits.kde.org/kitinerary" >}}
+ Be a bit less strict when applying the year from the context date. [Commit.](http://commits.kde.org/kitinerary/7f17fca2d64497544402e7909f8e7a439e16c6cd) 
+ Guard against SNCF tickets having 1st/2nd leg barcode on 3rd leg page. [Commit.](http://commits.kde.org/kitinerary/fd5c8270486d80f9a4a839af35ae76fe31799747) 
+ Fix second leg class extraction from SNCF ticket barcodes. [Commit.](http://commits.kde.org/kitinerary/333e98d64618b3fff927efd4af15dde6ee03469d) 
+ Extract English SNCF tickets. [Commit.](http://commits.kde.org/kitinerary/e67baf9074449f1946f08fad14a4b6ea9b00a129) 
+ Avoid NaN comparisons. [Commit.](http://commits.kde.org/kitinerary/06d1c15591f33d055a36657622402d25462bb3ab) 
+ Require C++17. [Commit.](http://commits.kde.org/kitinerary/8a13ede910ff022f92ad35e92ed68d75f351b62d) 
{{< /details >}}
{{< details id="kmail" title="kmail" href="https://commits.kde.org/kmail" >}}
+ Add missing remove text. [Commit.](http://commits.kde.org/kmail/80d8a0c213801758819f4ed1c1a6b03c5bd01b9f) 
+ Fix Bug 427889 - Missing text when removing charset encodings. [Commit.](http://commits.kde.org/kmail/13ab2578b3ed3aa5e39c94c5cc7a0120bbbbfae8) Fixes bug [#427889](https://bugs.kde.org/427889)
+ Fix Bug 427697 - KMail did not apply data from mailto information. [Commit.](http://commits.kde.org/kmail/a4715e7efc0909c6e37dbbcf21c4309c769a1fd9) Fixes bug [#427697](https://bugs.kde.org/427697)
+ Fix encoding problem with `kmail  --msg $PWD/file.txt`. [Commit.](http://commits.kde.org/kmail/ec37fe67706515cc851ca25e0e2a5aa1b21dbb75) 
{{< /details >}}
{{< details id="konqueror" title="konqueror" href="https://commits.kde.org/konqueror" >}}
+ Repair running desktop files and executables from konqueror. [Commit.](http://commits.kde.org/konqueror/84655a33200c114309fc69500ca74fbd28e48696) 
{{< /details >}}
{{< details id="konquest" title="konquest" href="https://commits.kde.org/konquest" >}}
+ Add homepage and oars. [Commit.](http://commits.kde.org/konquest/770b17f2c034ef808bdb924745ccaae877bb620b) 
{{< /details >}}
{{< details id="konsole" title="konsole" href="https://commits.kde.org/konsole" >}}
+ Fix Maximize current Terminal (Keyboard vs Mouse). [Commit.](http://commits.kde.org/konsole/45733c04b5712b44ca9dd89e55da9c20fe0f152c) Fixes bug [#418498](https://bugs.kde.org/418498)
+ Fix URL hint numbering. [Commit.](http://commits.kde.org/konsole/0516650a5cfd887d9586aebf2f693f435b60e137) 
+ Fix 'Alt+C' closing split view. [Commit.](http://commits.kde.org/konsole/13e9195bc5ae249f0dac8a14f7b285d4041b1f11) Fixes bug [#424654](https://bugs.kde.org/424654)
+ For "Manage Profiles" always open the "Profiles" page in settings. [Commit.](http://commits.kde.org/konsole/e694358a1cde0f1cedeef5698d632691c492c199) Fixes bug [#428206](https://bugs.kde.org/428206)
+ Fix duplicates link-related entries in context menu. [Commit.](http://commits.kde.org/konsole/532b7b965956f5ad04e90b07fbcc4fcc00885bdb) Fixes bug [#426808](https://bugs.kde.org/426808)
{{< /details >}}
{{< details id="messagelib" title="messagelib" href="https://commits.kde.org/messagelib" >}}
+ Fix typo :). [Commit.](http://commits.kde.org/messagelib/8a3042b9dc8d7135454431199c49c7a48c523ad6) 
+ Fix Bug 391030 (anarchotaoist) - KMail does not print images in emails. [Commit.](http://commits.kde.org/messagelib/9a8bbe19ea11efd5bbd721fc6e986682b9d3b2a9) Fixes bug [#391030](https://bugs.kde.org/391030)
+ Fix Bug 427792 - Crash on exit. [Commit.](http://commits.kde.org/messagelib/dd57ef3079388a9311d2d2d51885d7b2769d1547) Fixes bug [#427792](https://bugs.kde.org/427792)
+ Fix Bug 427697 - KMail did not apply data from mailto information. [Commit.](http://commits.kde.org/messagelib/6e5baafc27748d870ab37f2fcbbfb63e53cc121c) Fixes bug [#427697](https://bugs.kde.org/427697)
{{< /details >}}
{{< details id="okular" title="okular" href="https://commits.kde.org/okular" >}}
+ Do not disable flick if cursor has been wrapped. [Commit.](http://commits.kde.org/okular/08c4d4f712deb6e33665c7207701083932881329) Fixes bug [#420556](https://bugs.kde.org/420556)
+ Fix wrong memory access that may cause crash. [Commit.](http://commits.kde.org/okular/82590da11a11510a8a7281e73847cd3dd497020d) 
+ Fix middle click on sidebar tab bar unloads the document. [Commit.](http://commits.kde.org/okular/924a5fb9fcb23a88d3b18bfd3c64f6102a482b88) Fixes bug [#426613](https://bugs.kde.org/426613)
+ Support more than latin1 chars on --find command line option. [Commit.](http://commits.kde.org/okular/af526819937e31e525718ed93fbb65905e80d26b) Fixes bug [#427596](https://bugs.kde.org/427596)
+ Fix fast scrolling with Shift+Scroll. [Commit.](http://commits.kde.org/okular/777ac37ea21212c4d924c75be5cee502ad5820c0) Fixes bug [#420889](https://bugs.kde.org/420889)
+ Fix cursor not updated after clicking internal link. [Commit.](http://commits.kde.org/okular/8b008b02fc31e20f7c74ab2c25d34160756c7a92) Fixes bug [#421437](https://bugs.kde.org/421437)
+ Fix scrolling with scrollbar when the slider is not clicked. [Commit.](http://commits.kde.org/okular/17c266993deda05713171f824669cb4e09a6ee65) Fixes bug [#421159](https://bugs.kde.org/421159)
+ Complete TextSelectorEngine if nothing selected. [Commit.](http://commits.kde.org/okular/c8cf651335732170aa803c1af538c716c9b17e10) Fixes bug [#446658](https://bugs.kde.org/446658)
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" href="https://commits.kde.org/pimcommon" >}}
+ Fix initialize variable. [Commit.](http://commits.kde.org/pimcommon/10a36f3ab5b2621d6bcdd4fd5b413c08087a95cf) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" href="https://commits.kde.org/spectacle" >}}
+ ExportManager: Marked saved files as recent files. [Commit.](http://commits.kde.org/spectacle/5bd7e9b83a092b46531191d6122482d5a5a6bcf2) Fixes bug [#424205](https://bugs.kde.org/424205)
+ QuickEditor: Use XCB API to position the editor window. [Commit.](http://commits.kde.org/spectacle/f078d56371836486927b4c0138dd2ca4bc6e3ae2) 
{{< /details >}}
{{< details id="umbrello" title="umbrello" href="https://commits.kde.org/umbrello" >}}
+ Cmake: Add fallback in case QTQCHDIR was not found for KF5 build variant. [Commit.](http://commits.kde.org/umbrello/b2fd2bf58bbea512de1f37897bbc480986f62826) See bug [#427070](https://bugs.kde.org/427070)
+ Fix "file cannot create directory: /qch. Maybe need administrative privileges". [Commit.](http://commits.kde.org/umbrello/d4b77bfe0a54c5f10b63020c19b1150209cbad60) Fixes bug [#427070](https://bugs.kde.org/427070)
+ Add cmake command line option BUILD_QCH to support building without QCH support if requested. [Commit.](http://commits.kde.org/umbrello/8fdb410cd678fe5b018db7a094daa940263a8349) See bug [#427070](https://bugs.kde.org/427070)
{{< /details >}}
{{< details id="yakuake" title="yakuake" href="https://commits.kde.org/yakuake" >}}
+ Consider renaming tabs via DBus interactive. This allows it to override a previouly interactively set title. [Commit.](http://commits.kde.org/yakuake/e1b2ed353e6118b9f9c8289ac5a6725135dfab73) Fixes bug [#428400](https://bugs.kde.org/428400)
{{< /details >}}
