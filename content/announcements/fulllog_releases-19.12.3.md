---
release: true
releaseDate: 2020-03
title: Release Service 19.12.3 Full Log Page
type: fulllog
version: 19.12.3
hidden: true
---

<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Hide]</a></h3>
<ul id='ulakonadi-calendar' style='display: block'>
<li>Calendarbase.cpp - don't crash in non-debug builds. <a href='http://commits.kde.org/akonadi-calendar/b88acc73bf8dfaa29119b79289c6d8ea61964c26'>Commit.</a> See bug <a href='https://bugs.kde.org/417794'>#417794</a></li>
</ul>
<h3><a name='audiocd-kio' href='https://cgit.kde.org/audiocd-kio.git'>audiocd-kio</a> <a href='#audiocd-kio' onclick='toggle("ulaudiocd-kio", this)'>[Hide]</a></h3>
<ul id='ulaudiocd-kio' style='display: block'>
<li>Fix compilation with Qt 5.15. <a href='http://commits.kde.org/audiocd-kio/6835890ac2f87037ba088f20f34928da08b04425'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix minor bug with unavailable save button, when first runned entry have finished with error. <a href='http://commits.kde.org/cantor/dbbc3c4e27cacbafaeef1efdc408663b756c1815'>Commit.</a> </li>
<li>Fix problem with missing backend icon for created sessions. <a href='http://commits.kde.org/cantor/4f40d69c9292bbb2184e7f292fcb3455cfdb17f2'>Commit.</a> </li>
<li>[Sage] Fix bug with unworking loaded worksheets. <a href='http://commits.kde.org/cantor/83575703c3170f8be58b809b419ae52789839335'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415893'>#415893</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix typo in comment. <a href='http://commits.kde.org/dolphin/af45eb0af5219819f76b624162c75da83d763657'>Commit.</a> </li>
<li>Fix files not being highlighted if directory of file is already open. <a href='http://commits.kde.org/dolphin/fef7eebc6e3c27e6653bfd5d8a1abec191dc9835'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417230'>#417230</a></li>
</ul>
<h3><a name='elisa' href='https://cgit.kde.org/elisa.git'>elisa</a> <a href='#elisa' onclick='toggle("ulelisa", this)'>[Hide]</a></h3>
<ul id='ulelisa' style='display: block'>
<li>Set TextInput::selectByMouse to true to fix text selection. <a href='http://commits.kde.org/elisa/502ac397592decfdf4be18ea97f91a6ac7c19bc6'>Commit.</a> </li>
<li>Limit the rate with which progress is sent via MPRIS. <a href='http://commits.kde.org/elisa/ec5197521066d64ec0d2409a0af9388095c2130c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416179'>#416179</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix loading of remote files (i.e https). <a href='http://commits.kde.org/gwenview/9973a097d30a91dd50a4d23388b0b2b00eeeb626'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Incidencedialog.cpp - fallback to default collection. <a href='http://commits.kde.org/incidenceeditor/729c12d21e8d02e665a1837489ee5dad9ecec566'>Commit.</a> </li>
<li>Incidencedialog.cpp - restore last used collection. <a href='http://commits.kde.org/incidenceeditor/f0177161c696ed4475bb8761186d330a34039eb9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411191'>#411191</a></li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix libsoundfiledecoder with files with non whole second length. <a href='http://commits.kde.org/k3b/183d20b1aa544c221592eb32b9743dc287ae0e79'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418079'>#418079</a></li>
<li>K3bFFMpegFile: Support files with more than one stream. <a href='http://commits.kde.org/k3b/24d966d4fa065ab1176b329d0689673a54eea12c'>Commit.</a> </li>
<li>Fix color of tracks in Media Info. <a href='http://commits.kde.org/k3b/a267d05832db355db3316a0c84d095d9def66305'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389428'>#389428</a></li>
<li>Accept WAV files in K3bLibsndfileDecoderFactory::canDecode. <a href='http://commits.kde.org/k3b/2491cc70f9ffb129cf49633cbdc4f0d77789677d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399056'>#399056</a></li>
</ul>
<h3><a name='kaccounts-integration' href='https://cgit.kde.org/kaccounts-integration.git'>kaccounts-integration</a> <a href='#kaccounts-integration' onclick='toggle("ulkaccounts-integration", this)'>[Hide]</a></h3>
<ul id='ulkaccounts-integration' style='display: block'>
<li>Unbreak the Accounts KCM. <a href='http://commits.kde.org/kaccounts-integration/67fc8d83b71f2b1304615ea087023ef66a9c9d5d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415267'>#415267</a></li>
<li>[CreateAccount job] Never set an empty name when creating an account. <a href='http://commits.kde.org/kaccounts-integration/4600d32a1c063db62c371b422f26263d1e991b40'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414219'>#414219</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Replace call to deprecated QWidget::showMinimized(). <a href='http://commits.kde.org/kalarm/7b7c49781480c27763d4151410c69fdbcd62c9c3'>Commit.</a> </li>
<li>Bug 417108: fix failure of command line options requiring calendar access. <a href='http://commits.kde.org/kalarm/9d0308396a74e64dbbd67a7b162f0d025c16123f'>Commit.</a> </li>
</ul>
<h3><a name='kalarmcal' href='https://cgit.kde.org/kalarmcal.git'>kalarmcal</a> <a href='#kalarmcal' onclick='toggle("ulkalarmcal", this)'>[Hide]</a></h3>
<ul id='ulkalarmcal' style='display: block'>
<li>Fix pri file generation. <a href='http://commits.kde.org/kalarmcal/a2d76e7a0925c4d51ef5e5fa37ebba16ca99612b'>Commit.</a> </li>
</ul>
<h3><a name='kamoso' href='https://cgit.kde.org/kamoso.git'>kamoso</a> <a href='#kamoso' onclick='toggle("ulkamoso", this)'>[Hide]</a></h3>
<ul id='ulkamoso' style='display: block'>
<li>Fix running qtvideosink_autotest after requiring >= ECM 5.38. <a href='http://commits.kde.org/kamoso/b31656f9df73f9e375ce8425ed4ae5ccd5f501bf'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Try to add enough large icons for Windows (256/512 px). <a href='http://commits.kde.org/kate/57f60ab132f800ea3e8bf9aa546d0c62fa3e531e'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix clip monitor ruler not always adjusting to correct length. <a href='http://commits.kde.org/kdenlive/c0768145d5355a3f057b4339355448bebf750abe'>Commit.</a> </li>
<li>Fix paste speed clip broken on comma locale. <a href='http://commits.kde.org/kdenlive/3ed3b9ae4001d83c3707b8a36ef18d4f9eb72a0e'>Commit.</a> See bug <a href='https://bugs.kde.org/418121'>#418121</a></li>
<li>Fix 1 frame offset in fade out. <a href='http://commits.kde.org/kdenlive/9fa9218c6a437527a50caa0415fd2a94035adf3d'>Commit.</a> See bug <a href='https://bugs.kde.org/416811'>#416811</a></li>
<li>Fix markers drawn outside clip. <a href='http://commits.kde.org/kdenlive/6eb18fd88b96a77383eff61865b523e1d4c754b6'>Commit.</a> </li>
<li>Fix crash on insert track. Related to #573. <a href='http://commits.kde.org/kdenlive/473c11e946a6c0293338bcfe573169c6174c450d'>Commit.</a> </li>
<li>Fix changing of title clip duration broken. <a href='http://commits.kde.org/kdenlive/b0917833f4fd92a7009e73b4b7f27bda04345cbd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417505'>#417505</a></li>
<li>Fix org.kde.kdenlive.appdata.xml. <a href='http://commits.kde.org/kdenlive/a27bf515f308b2938576c77cb90ebed8c42bedb2'>Commit.</a> </li>
<li>Filter effects in current category only. <a href='http://commits.kde.org/kdenlive/966add0e02f3b806fcf3158c2f245bb1ac1d2cbc'>Commit.</a> </li>
<li>Fix recent change breaking effects with jobs (like motion tracker). <a href='http://commits.kde.org/kdenlive/625766617b47d06f7aeda17c2a059792e6f9e4af'>Commit.</a> </li>
<li>Update appdata for 19.12.3. <a href='http://commits.kde.org/kdenlive/1d97bad18b6fc77dd170553679542013b8d6ffbd'>Commit.</a> </li>
<li>Fix audio mixer balance cannot be changed after project opening. <a href='http://commits.kde.org/kdenlive/3c17c4ee3fc02643c037cc0c26c3fe35dd452f0d'>Commit.</a> </li>
<li>Fix clip fades cannot be inserted after undoing. <a href='http://commits.kde.org/kdenlive/81f67bd98cde6dcfa5c5189802de5c8b588d3f73'>Commit.</a> </li>
<li>Fix cannot update render filename. <a href='http://commits.kde.org/kdenlive/60b9c1affbc0191a5d4dbb4741c924e7afa393ea'>Commit.</a> </li>
<li>Fix pasted clips with negative speed have wrong in/out. <a href='http://commits.kde.org/kdenlive/f3546b3fa82cd3c671e1384f8d4d74e4b9bc1c85'>Commit.</a> See bug <a href='https://bugs.kde.org/417143'>#417143</a></li>
<li>Fix dropping effect on monitor. <a href='http://commits.kde.org/kdenlive/9fb411c76a933d18cc6f99a520b831c5810f86bb'>Commit.</a> </li>
<li>Spelling fixes (by Patrick Matthäi). <a href='http://commits.kde.org/kdenlive/15f4abf922f2a641cf7ca23f3ab901fdfd9c9419'>Commit.</a> </li>
<li>Fix rotoscoping broken in some circumstances on cut clips. <a href='http://commits.kde.org/kdenlive/fc8be512323824b0979c7dfd569265780a929073'>Commit.</a> </li>
<li>Some updates for AppImage rubberband (not automatically included, needs some manual patching). <a href='http://commits.kde.org/kdenlive/bbc4d01a4859206ae45df0f02cfe9ad3aef350e7'>Commit.</a> </li>
<li>Add vamp-sdk to AppImage scripts. <a href='http://commits.kde.org/kdenlive/18b09a3434a30c3ff33cca20515d5183eeb034f0'>Commit.</a> </li>
<li>Add rubberband to AppImage scripts. <a href='http://commits.kde.org/kdenlive/9be4cf86d083fdc1e59312a31d03d3e3cf51d516'>Commit.</a> </li>
<li>Fix monitor fullscreen in some cases and don't lose focus (broke shortcuts). <a href='http://commits.kde.org/kdenlive/5d2b4cc5671165a34763ae26f82769cd1d95275c'>Commit.</a> </li>
<li>Update org.kde.kdenlive.appdata.xml. <a href='http://commits.kde.org/kdenlive/c5f05bbf2853f8962e4d67d69371f901837b66cd'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix grantlee i18n stuff not being translated. <a href='http://commits.kde.org/kdepim-addons/abd13bf95f0aaf9e84ad34b23a42b3cf41c018a8'>Commit.</a> </li>
</ul>
<h3><a name='khelpcenter' href='https://cgit.kde.org/khelpcenter.git'>khelpcenter</a> <a href='#khelpcenter' onclick='toggle("ulkhelpcenter", this)'>[Hide]</a></h3>
<ul id='ulkhelpcenter' style='display: block'>
<li>Reload all kind of KHC addresses when the font size changed. <a href='http://commits.kde.org/khelpcenter/ea387e967d8d8cf8724e491d436d382f1e5c41b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417222'>#417222</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Correctly disable UDS_ACCESS being set. <a href='http://commits.kde.org/kio-extras/01e442c7514024a5cb48b1b364faef17eb0046ad'>Commit.</a> </li>
<li>Smb: enable anonymous and domain extra field in authinfo. <a href='http://commits.kde.org/kio-extras/9d52cd9c9144cf56a3011c40b1d55c9bd853c6eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/327484'>#327484</a></li>
<li>Smb: disable mode bits getting forwarded to KIO. <a href='http://commits.kde.org/kio-extras/718abcb69b78385123dd7a05a66a656af772d581'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414482'>#414482</a></li>
<li>Smb: map ECONNABORTED to ERR_CONNECTION_BROKEN. <a href='http://commits.kde.org/kio-extras/2075e76389ccd914fdbf86ebe83c4ba01367e915'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415436'>#415436</a></li>
<li>Smb: install smb as both smb:// and cifs://. <a href='http://commits.kde.org/kio-extras/a5f51238fab75c407c53dd6ec348937c08b6022d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/327295'>#327295</a></li>
<li>Smb: set the share comment as UDS_COMMENT for the UI to display. <a href='http://commits.kde.org/kio-extras/dddafae55363f5d9c51263b4c8a741287dd986c5'>Commit.</a> See bug <a href='https://bugs.kde.org/105086'>#105086</a></li>
<li>Smb: do not map uid and gid. <a href='http://commits.kde.org/kio-extras/25a02064dbeea618e22dc2c0ba853b609f124f2c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/212801'>#212801</a></li>
<li>Smb: fix free space calculation. <a href='http://commits.kde.org/kio-extras/6832817f311bfa106ee971ae51bac288843af08b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/298801'>#298801</a></li>
<li>Smb: support more advanced hidden file flag. <a href='http://commits.kde.org/kio-extras/e2eadb7555a6320b2e2b5a20248940d2f78d9823'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/307353'>#307353</a></li>
<li>Smb: call smb_cutime on the correct url to actually set mtime properly. <a href='http://commits.kde.org/kio-extras/aaedd983729f85129a91d861e1e4c2cc6ad084cb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356651'>#356651</a></li>
<li>Smb: retain atime properly. <a href='http://commits.kde.org/kio-extras/7f47119b22d526fd2ffd9451a9513dfb260a6706'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410624'>#410624</a></li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Handle language variants in the Vueling extractor. <a href='http://commits.kde.org/kitinerary/1f1815a43399a9428814a40c996dd09b3bb7b538'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Remove temporary directory. <a href='http://commits.kde.org/kmail/3191bf9e0dbb93a1e417328b7350748623484d74'>Commit.</a> </li>
<li>Remove temporary file. <a href='http://commits.kde.org/kmail/095a9fc0da8d85bb77784416302f82886df7e0ee'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>Src/akonadicollectionview.cpp - Fix "Use as Default Calendar". <a href='http://commits.kde.org/korganizer/de87c84720662d6fc7143bc06c48d4315df1ad7c'>Commit.</a> </li>
<li>Alarmdialog.cpp - fix initial suspend starting values. <a href='http://commits.kde.org/korganizer/9777deba88e46e5a6a29567416174f3cd6f18584'>Commit.</a> </li>
<li>Calendarview.cpp - consider currenttime for default start time. <a href='http://commits.kde.org/korganizer/6175050fcb1342893963c5fca2eac51efaa2dd4a'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Fix another fallout of a recent ECM change. <a href='http://commits.kde.org/kpimtextedit/48e4750021727af3e00738d07f8bc3dcdc7a29c8'>Commit.</a> </li>
</ul>
<h3><a name='kreversi' href='https://cgit.kde.org/kreversi.git'>kreversi</a> <a href='#kreversi' onclick='toggle("ulkreversi", this)'>[Hide]</a></h3>
<ul id='ulkreversi' style='display: block'>
<li>Port deprecated IconSize. <a href='http://commits.kde.org/kreversi/a12b79859d1ad74034c11e4c6c44d12d31b2de1f'>Commit.</a> </li>
</ul>
<h3><a name='krfb' href='https://cgit.kde.org/krfb.git'>krfb</a> <a href='#krfb' onclick='toggle("ulkrfb", this)'>[Hide]</a></h3>
<ul id='ulkrfb' style='display: block'>
<li>PW framebuffer: send correct type over DBus. <a href='http://commits.kde.org/krfb/a62ef07e9d7d71ada719e677d9b425e987f11041'>Commit.</a> </li>
</ul>
<h3><a name='kteatime' href='https://cgit.kde.org/kteatime.git'>kteatime</a> <a href='#kteatime' onclick='toggle("ulkteatime", this)'>[Hide]</a></h3>
<ul id='ulkteatime' style='display: block'>
<li>Add missing emit. <a href='http://commits.kde.org/kteatime/8152bc0fe0cc2b3b42adc7ff702507e14ee8d302'>Commit.</a> </li>
</ul>
<h3><a name='ktnef' href='https://cgit.kde.org/ktnef.git'>ktnef</a> <a href='#ktnef' onclick='toggle("ulktnef", this)'>[Hide]</a></h3>
<ul id='ulktnef' style='display: block'>
<li>Fix another typo causing errors with ECM. <a href='http://commits.kde.org/ktnef/48dcfeb7b7d1b391d5e208595bb38eecc8973774'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Hide]</a></h3>
<ul id='ulktouch' style='display: block'>
<li>Homescreen: Fix Layout of Lesson Locked Notice. <a href='http://commits.kde.org/ktouch/3ef3fa87420a67d3d55baf78fc2746410b81cb80'>Commit.</a> </li>
</ul>
<h3><a name='libkcddb' href='https://cgit.kde.org/libkcddb.git'>libkcddb</a> <a href='#libkcddb' onclick='toggle("ullibkcddb", this)'>[Hide]</a></h3>
<ul id='ullibkcddb' style='display: block'>
<li>Fix compilation with Qt 5.15. <a href='http://commits.kde.org/libkcddb/ffe81b780fddc9daa51172f7f4152ba1b930ea02'>Commit.</a> </li>
</ul>
<h3><a name='libkdegames' href='https://cgit.kde.org/libkdegames.git'>libkdegames</a> <a href='#libkdegames' onclick='toggle("ullibkdegames", this)'>[Hide]</a></h3>
<ul id='ullibkdegames' style='display: block'>
<li>Fix compilation with Qt 5.15. <a href='http://commits.kde.org/libkdegames/9ac419d13ad7d22ae29619fbc31603631c23338e'>Commit.</a> </li>
<li>Fix using 0 for a flags. <a href='http://commits.kde.org/libkdegames/6bb919618c96057b2cd0312ec7e3da41f76dd47f'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Fix typo in event status. <a href='http://commits.kde.org/libkgapi/48f487f06e62cba61b35a87aca4bdb0bce5ba638'>Commit.</a> </li>
</ul>
<h3><a name='libksane' href='https://cgit.kde.org/libksane.git'>libksane</a> <a href='#libksane' onclick='toggle("ullibksane", this)'>[Hide]</a></h3>
<ul id='ullibksane' style='display: block'>
<li>Fix compilation with Qt 5.15. <a href='http://commits.kde.org/libksane/4377fa6648b60abc80eb438604d58a72231286c9'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix setting translation domain on the Grantlee localizer. <a href='http://commits.kde.org/messagelib/139ef5a0f6249f8f877c7b6a3457f120acbb0f4e'>Commit.</a> </li>
<li>Fix using "Qt" in search line. Test flag was incorrect before. <a href='http://commits.kde.org/messagelib/997cfd53e177b4ca9246e1eaee5d29859ecfc08f'>Commit.</a> </li>
<li>Initialize value in header class. <a href='http://commits.kde.org/messagelib/e50fa31f636f5db19ef4dd72ca4cc08fe78558fe'>Commit.</a> </li>
<li>Fix pri file generation. <a href='http://commits.kde.org/messagelib/fdb80d10a27065518539198f8629cb1e8d6beb78'>Commit.</a> </li>
<li>Replace \n by '_' in filename. otherwise mail will be broken. <a href='http://commits.kde.org/messagelib/47d804c32fc41d802612c374ac23fa61ee3f4aff'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fix re-rendering of images when using partial updates. <a href='http://commits.kde.org/okular/81c005710cdbb23f9ed9ebcc4aee4db69d33434d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418086'>#418086</a></li>
<li>Fix rendering of stamps with non 100% opacity. <a href='http://commits.kde.org/okular/ff7dff1d62dfae190e88a4571c7392cea7670fca'>Commit.</a> </li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Hide]</a></h3>
<ul id='ulpim-data-exporter' style='display: block'>
<li>Add strech here. <a href='http://commits.kde.org/pim-data-exporter/7183d0dfa91412a71d1118df88fbb7f125f2a256'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Use the global xml catalog to replace external docbook.xsl uri. <a href='http://commits.kde.org/umbrello/ffd5f5c786c939a52c9207b2c34fcde9d2926098'>Commit.</a> See bug <a href='https://bugs.kde.org/417520'>#417520</a></li>
<li>Use a more reliable way on Windows to find the application's installation root. <a href='http://commits.kde.org/umbrello/3059fa8486fe3ba51e64b8d2ea33a6d3e6c86b5a'>Commit.</a> See bug <a href='https://bugs.kde.org/417520'>#417520</a></li>
<li>Fix finding docbook stylesheet by using xml catalog support. <a href='http://commits.kde.org/umbrello/05f582a0ffae75be72da2588b9f320c3647b37c9'>Commit.</a> See bug <a href='https://bugs.kde.org/417520'>#417520</a></li>
<li>Fixed error message 'Can't save an empty diagram' with Docbook and Xhtml export. <a href='http://commits.kde.org/umbrello/78c5b1ff737ff31fe6a4564e6d7b1fc3dfb8b8e2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417784'>#417784</a></li>
<li>Correction of the path setting for the Docbook and Xhtml export. <a href='http://commits.kde.org/umbrello/4c9269461cd55806ae886c2bbd8294090a328d2c'>Commit.</a> See bug <a href='https://bugs.kde.org/417520'>#417520</a></li>
<li>Fix KF5 issue 'Exporter searches for XSLT file in nonexistent directory'. <a href='http://commits.kde.org/umbrello/aa66d92440698ad1aa8981482b24eb2f7939cb53'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417520'>#417520</a></li>
<li>Fix '404 on help for settings in Umbrello. <a href='http://commits.kde.org/umbrello/735d0e93a2b6eff20b22801f4beee39f4104d73f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410906'>#410906</a></li>
<li>Cmake: remove annoying suggest-override warning. <a href='http://commits.kde.org/umbrello/f6bf8c890ec16e1fe8eb743f1b44d24b80244f75'>Commit.</a> </li>
<li>Fix using wrong name for slotInterfaceRequired() in class WorkToolBar. <a href='http://commits.kde.org/umbrello/2807dcca8996b22222972e4ff699d1d4046329f3'>Commit.</a> See bug <a href='https://bugs.kde.org/115269'>#115269</a></li>
</ul>

